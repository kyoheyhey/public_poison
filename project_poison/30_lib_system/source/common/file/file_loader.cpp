﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "file/file_loader.h"
#include "file/file_utility.h"


POISON_BGN

CFileLoader::~CFileLoader()
{
	Clear();
}


///-------------------------------------------------------------------------------------------------
/// ファイルロード
b8		CFileLoader::Load(const c8* _path, void* _buf, const u32 _size)
{
	// 事前クリア
	Clear();
	m_pPath = _path;

	// ファイル読み込み、破棄
	{
		// ファイルオートクローズクラス
		struct AUTO_CLOSE_HANDLE
		{
			AUTO_CLOSE_HANDLE(const FILE_HANDLE _handle)
				: handle(_handle){}
			~AUTO_CLOSE_HANDLE(){ if( handle != FILE_HANDLE_INVALID ){ CFileUtility::Close( handle ); } }

			FILE_HANDLE& operator()() { return handle; }

			FILE_HANDLE handle;
		};

		// ファイルオープン
		AUTO_CLOSE_HANDLE handle( CFileUtility::Open(_path) );
		if( handle() == FILE_HANDLE_INVALID )
		{
			PRINT_ERROR( "*****[ERROR] CFileLoader::Load() Failed Open File (%s)\n", _path);
			return false;
		}

		// ファイルサイズ取得
		s32 fsize = 0;
		u32 status = CFileUtility::GetFileSize(handle(), &fsize);
		if (status != CFileUtility::FU_STATUS_OK)
		{
			return false;
		}
		m_uSize = SCast<u32>(fsize);

		// ファイルバッファ確保
		libAssert(_buf == NULL || _size <= 0);
		m_pBuffer = _buf;
		// バッファの方が小さければそれに合わせる
		if (_size < m_uSize)
		{
			m_uSize = _size;
		}

		// ファイルロード、失敗
		if (CFileUtility::Read(handle(), m_pBuffer, m_uSize) != CFileUtility::STATUS_READED)
		{
			PRINT_ERROR("*****[ERROR] CFileLoader::Load() ファイル読み込み失敗(%s)\n", _path );
			Clear();	
			return false;
		}
	}
	return (m_pBuffer != NULL);
}

b8		CFileLoader::Load(const c8* _path, const MEM_HANDLE _hdl)
{
	// 事前クリア
	Clear();
	m_pPath = _path;

	// ファイル読み込み、破棄
	{
		// ファイルオートクローズクラス
		struct AUTO_CLOSE_HANDLE
		{
			AUTO_CLOSE_HANDLE(const FILE_HANDLE _handle)
				: handle(_handle) {}
			~AUTO_CLOSE_HANDLE() { if (handle != FILE_HANDLE_INVALID) { CFileUtility::Close(handle); } }

			FILE_HANDLE& operator()() { return handle; }

			FILE_HANDLE handle;
		};

		// ファイルオープン
		AUTO_CLOSE_HANDLE handle(CFileUtility::Open(_path));
		if (handle() == FILE_HANDLE_INVALID)
		{
			PRINT_ERROR("*****[ERROR] CFileLoader::Load() Failed Open File (%s)\n", _path);
			return false;
		}

		// ファイルサイズ取得
		s32 fsize = 0;
		u32 status = CFileUtility::GetFileSize(handle(), &fsize);
		if (status != CFileUtility::FU_STATUS_OK)
		{
			return false;
		}
		m_uSize = SCast<u32>(fsize);

		// ファイルバッファ確保
		m_memHdl = _hdl;
		m_pBuffer = MEM_MALLOC_ALIGN(m_memHdl, CFileUtility::GetFileBufferAlign(), m_uSize);
		// 失敗
		if (m_pBuffer == NULL)
		{
			PRINT_ERROR("*****[ERROR] CFileLoader::Load() メCFileUtilityモリ確保失敗[%d](%s)\n", m_uSize, _path);
			return false;
		}

		// ファイルロード、失敗
		if (CFileUtility::Read(handle(), m_pBuffer, m_uSize) != CFileUtility::STATUS_READED)
		{
			PRINT_ERROR("*****[ERROR] CFileLoader::Load() ファイル読み込み失敗(%s)\n", _path);
			Clear();
			return false;
		}
	}

	return (m_pBuffer != NULL);
}

///-------------------------------------------------------------------------------------------------
/// クリア
void	CFileLoader::Clear(b8 _free_memory)
{
	m_pPath = NULL;
	if(m_memHdl != MEM_HDL_INVALID && _free_memory)
	{
		// メモリ解放
		MEM_FREE(m_memHdl, m_pBuffer);
	}
	m_memHdl = MEM_HDL_INVALID;
	m_pBuffer = NULL;
	m_uSize = 0;
}


///-------------------------------------------------------------------------------------------------
/// ファイルが存在するか
b8		CFileLoader::IsExistFile(const c8* _path)
{
	return CFileUtility::IsExistFile(_path);
}


///-------------------------------------------------------------------------------------------------
/// ファイルサイズ取得
s32		CFileLoader::CheckFileSize(const c8* _path)
{
	// ファイルオープン
	FILE_HANDLE handle = CFileUtility::Open(_path);
	// ファイルが存在
	if( handle == FILE_HANDLE_INVALID ){ return -1; }

	// ファイルサイズ取得
	s32 fsize = 0;
	u32 status = CFileUtility::GetFileSize(handle, &fsize);
	if (status != CFileUtility::FU_STATUS_OK)
	{
		return -1;
	}

	// ファイルクローズ
	CFileUtility::Close(handle);

	return SCast<s32>(fsize);
}

POISON_END

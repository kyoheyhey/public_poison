﻿
//*************************************************************************************************
// バイナリパラメータファイル読み込み処理クラス
// 拡張スクリプトインタープリタクラス
//*************************************************************************************************

#define SI_INLINE_INSTANCE	///< ※インライン展開用のコードをインライン化せずに実体化させる為のマクロ　※#include "script_interpreter.h"より先に書く事！


///-------------------------------------------------------------------------------------------------
/// include
#include "script/script_interpreter.h"
#include <math.h>
#include <string.h>


//-------------------------------------------------------------------------------------------------
// define
#define SI_USE_DEBUG_PRINT

#ifdef SI_USE_EASY_ALLOCATOR
#include SI_INCLUDE_EASY_ALLOCATOR
#endif//SI_USE_EASY_ALLOCATOR

#ifdef SI_USE_MEMORY_STAFF
#include SI_INCLUDE_MEMORY_STAFF
#endif//SI_USE_MEMORY_STAFF

#ifdef SI_USE_TEXT_BUFF
#include SI_INCLUDE_TEXT_BUFF
#endif//SI_USE_TEXT_BUFF

#ifdef SI_USE_TEXT_LIB
#include SI_INCLUDE_TEXT_LIB
#endif//SI_USE_TEXT_LIB


POISON_BGN

// 度<->ラジアン変換
#define meToRadian( degree ) ( 0.017453292f * degree ) ///< PI*degree/180.0f
#define meToDegree( radian ) ( 57.29577951f * radian ) ///< 180.0f*radian/mgPI

// プリント文
#ifdef SI_DELETE_PRINT	//< PRINT文を無効化
#define SI_PRINT_ERROR(...)
#define SI_PRINT_WARNING(...)
#define SI_PRINT(...)
#else//SI_DELETE_PRINT
#ifdef SI_USE_DEBUG_PRINT	//< DEBUG_PRINT系関数を使用
#define SI_PRINT_ERROR		PRINT_ERROR
#define SI_PRINT_WARNING	PRINT_WARNING
//	#define SI_PRINT			PRINT
#define SI_PRINT(...)
#else//SI_USE_DEBUG_PRINT	//< 標準関数を使用
#define SI_PRINT_ERROR		lxpPrintSJIS
#define SI_PRINT_WARNING	lxpPrintSJIS
#define SI_PRINT			LXP_PRINT_SCRIPT_LV2
#endif//SI_USE_DEBUG_PRINT
#endif//SI_DELETE_PRINT

// テキスト解析用バッファサイズ
#ifdef SI_DEFAULT_STACK_SIZE_LARGE
	// 大
	static const u32 PARAM_COUNT_MAX = 128;			///< パラメータの最大数
static const u32 PARAM_TEXT_BUFF_SIZE = 4 * 1024;	///< テキストバッファの最大長（一行の解析に必要なバッファの総量）　※外部からのバッファ受け渡しによる増量が可能_
static const u32 PARAM_WORK_BUFF_SIZE = 4 * 1024;	///< 一つのパラメータの最大長　※外部からのバッファ受け渡しによる増量が可能_
#else//SI_DEFAULT_STACK_SIZE_LARGE
#ifdef SI_DEFAULT_STACK_SIZE_SMALL
	// 小
	static const u32 PARAM_COUNT_MAX = 128;			///< パラメータの最大数
static const u32 PARAM_TEXT_BUFF_SIZE = 1 * 1024;	///< テキストバッファの最大長（一行の解析に必要なバッファの総量）　※外部からのバッファ受け渡しによる増量が可能_
static const u32 PARAM_WORK_BUFF_SIZE = 1 * 1024;	///< 一つのパラメータの最大長　※外部からのバッファ受け渡しによる増量が可能_
#else//SI_DEFAULT_STACK_SIZE_SMALL(SI_DEFAULT_STACK_SIZE_STADARD)
	// 中
	static const u32 PARAM_COUNT_MAX = 128;			///< パラメータの最大数
static const u32 PARAM_TEXT_BUFF_SIZE = 2 * 1024;	///< テキストバッファの最大長（一行の解析に必要なバッファの総量）　※外部からのバッファ受け渡しによる増量が可能_
static const u32 PARAM_WORK_BUFF_SIZE = 2 * 1024;	///< 一つのパラメータの最大長　※外部からのバッファ受け渡しによる増量が可能_
#endif//SI_DEFAULT_STACK_SIZE_SMALL
#endif//SI_DEFAULT_STACK_SIZE_LARGE

// データサイズ
static const s32 SI_TAG_PARAM_BIN_HEADER_SIZE = 16;			///< sizeof(TAG_PARAM_BIN_HEADER);
static const s32 SI_TAG_PARAM_BIN_CRC_RECORD_HEADER_SIZE = 5;	///< sizeof(TAG_PARAM_BIN_CRC_RECORD::HEADER);
static const s32 SI_TAG_PARAM_BIN_CRC_RECORD_VALUE_SIZE = 4;	///< sizeof(TAG_PARAM_BIN_CRC_RECORD::PARAM_VALUE);

// アラインメント計算
#define GET_BLOCK_COUNT(total_size, block_size)			((total_size / block_size) + ((total_size % block_size) > 0))
#define GET_BYTE_COUNT(total_size)						GET_BLOCK_COUNT(total_size, 8)
#define GET_ALIGNMENT_OVER_SIZE(size, alignment_size)	(size % alignment_size)
#define GET_ALIGNMENT_FILL_SIZE(size, alignment_size)	((alignment_size - GET_ALIGNMENT_OVER_SIZE(size, alignment_size)) % alignment_size)
#define GET_ALIGNMENTED_SIZE(size, alignment_size)		(size + GET_ALIGNMENT_FILL_SIZE(size, alignment_size))
#define GET_ALIGNMENTED_SIZE_STD(size)					GET_ALIGNMENTED_SIZE(size, SI_RECORD_PARAM_ALIGNMENT)

// Statics
u32					CScriptInterpreter::s_TagParamCountGlobal = 0;
SI_TAG_PARAM*		CScriptInterpreter::s_TagParamTopGlobal = NULL;
SI_EXCEPTION_FUNC	CScriptInterpreter::s_ExceptionCallbackDefault = NULL;

///-------------------------------------------------------------------------------------------------
/// レコードヘッダーサイズを計算（バイナリ形式）
inline u32	CScriptInterpreter::CalcRecordHeaderSizeForBin() const
{
	const s32 record_header_bytes = SI_TAG_PARAM_BIN_CRC_RECORD_HEADER_SIZE;
	//const s32 record_header_bytes_alignmented = GET_ALIGNMENTED_SIZE_STD(record_header_bytes);
	//return record_header_bytes_alignmented;
	return record_header_bytes;//レコードヘッダーではアラインメントを調整しない
}

///-------------------------------------------------------------------------------------------------
/// レコード前半サイズを計算（バイナリ形式）
/// ※レコードヘッダー情報＋型情報のサイズ
inline u32	CScriptInterpreter::CalcHalfRecordSizeForBin(const SI_TAG_PARAM_BIN_RECORD* record) const
{
	const s32 record_header_size = this->CalcRecordHeaderSizeForBin();

	const s32 param_count = record->header.param_count;
	const s32 param_type_bits = SI_RECORD_PARAM_TYPE_BITS * param_count;
	const s32 param_type_bytes = GET_BYTE_COUNT(param_type_bits);

	const s32 half_record_bytes = record_header_size + param_type_bytes;
	const s32 half_record_bytes_alignmented = GET_ALIGNMENTED_SIZE_STD(half_record_bytes);

	return half_record_bytes_alignmented;
}

///-------------------------------------------------------------------------------------------------
/// レコード全体サイズを計算（バイナリ形式）
/// ※レコードヘッダー情報＋型情報＋値情報のサイズ
inline u32	CScriptInterpreter::CalcRecordSizeForBin(const SI_TAG_PARAM_BIN_RECORD* record) const
{
	const s32 half_record_size = this->CalcHalfRecordSizeForBin(record);

	const s32 param_count = record->header.param_count;
	const s32 param_value_bytes = SI_TAG_PARAM_BIN_CRC_RECORD_VALUE_SIZE * param_count;

	const s32 record_bytes = half_record_size + param_value_bytes;
	const s32 record_bytes_alignmented = GET_ALIGNMENTED_SIZE_STD(record_bytes);

	return record_bytes_alignmented;
}

///-------------------------------------------------------------------------------------------------
/// レコード位置を計算（バイナリ形式）
inline const SI_TAG_PARAM_BIN_RECORD*	CScriptInterpreter::CalcByteOffsetedRecordForBin(const void* base_ptr, const s32 offset) const
{
	const c8* record_ptr = RCast<const c8*>(base_ptr) + offset;
	const SI_TAG_PARAM_BIN_RECORD* record = RCast<const SI_TAG_PARAM_BIN_RECORD*>(record_ptr);
	return record;
}

///-------------------------------------------------------------------------------------------------
/// 最初のレコード位置を計算（バイナリ形式）
inline const SI_TAG_PARAM_BIN_RECORD*	CScriptInterpreter::CalcFirstRecordForBin() const
{
	const s32 file_header_size = SI_TAG_PARAM_BIN_HEADER_SIZE;
	const SI_TAG_PARAM_BIN_RECORD* first_record = this->CalcByteOffsetedRecordForBin(this->m_FileBuff, file_header_size);
	return first_record;
}

///-------------------------------------------------------------------------------------------------
/// 次のレコード位置を計算（バイナリ形式）
const SI_TAG_PARAM_BIN_RECORD*	CScriptInterpreter::CalcNextRecordForBin(const SI_TAG_PARAM_BIN_RECORD* record) const
{
	const s32 record_size = this->CalcRecordSizeForBin(record);
	const SI_TAG_PARAM_BIN_RECORD* next_record = this->CalcByteOffsetedRecordForBin(record, record_size);
	return next_record;
}

///-------------------------------------------------------------------------------------------------
/// レコードの型情報の先頭位置を計算（バイナリ形式）
//inline const SI_TAG_PARAM_BIN_RECORD::PARAM_TYPE*	CScriptInterpreter::CalcParamTypeTopForBin(const SI_TAG_PARAM_BIN_RECORD* record) const
inline const u8*	CScriptInterpreter::CalcParamTypeTopForBin(const SI_TAG_PARAM_BIN_RECORD* record) const
{
	const s32 record_header_size = this->CalcRecordHeaderSizeForBin();

	const c8* param_type_top_ptr = RCast<const c8*>(record) + record_header_size;
	//const SI_TAG_PARAM_BIN_RECORD::PARAM_TYPE* param_type_top     = RCast<const SI_TAG_PARAM_BIN_RECORD::PARAM_TYPE*>(param_type_top_ptr);
	const u8* param_type_top = RCast<const u8*>(param_type_top_ptr);

	return param_type_top;
}

///-------------------------------------------------------------------------------------------------
/// レコードの値情報の先頭位置を計算（バイナリ形式）
inline const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE*	CScriptInterpreter::CalcParamValueTopForBin(const SI_TAG_PARAM_BIN_RECORD* record) const
{
	const s32 half_record_size = this->CalcHalfRecordSizeForBin(record);

	const c8* param_value_top_ptr = RCast<const c8*>(record) + half_record_size;
	const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_value_top = RCast<const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE*>(param_value_top_ptr);

	return param_value_top;
}

///-------------------------------------------------------------------------------------------------
/// 初期化
b8		CScriptInterpreter::Initialize()
{
	// タグ設定
	this->m_TagParamCount = 0;
	this->m_TagParamTop = NULL;
	this->m_TagParamCache = NULL;

	// 拡張パラメータ
	this->m_ExtParam = NULL;

	// 例外タグコールバック
	this->m_ExceptionCallback = s_ExceptionCallbackDefault;

	// バイナリファイル
	this->m_FileBuff = NULL;
	this->m_FileName = NULL;

	// バイナリファイルサイズ
	this->m_FileSize = 0;

	// スクリプトファイル種別
	this->m_ScriptFileType = SI_FILE_TYPE_BIN_OR_SJIS;

	// ファイルヘッダー情報
	this->m_Header = NULL;

	// 基本データ（行データ）
	this->m_Record = NULL;

	// 文字列データ
	this->m_Text = NULL;

	// 複写された文字列データ
	this->m_DuplicatedText = NULL;

#ifdef SI_USE_TEXT_BUFF
	// 文字列データを自動複写用バッファ
	this->m_TextBuffForAutoCopy = NULL;
#endif//SI_USE_TEXT_BUFF
	this->m_PlainTextBuffForAutoCopy = NULL;
	this->m_PlainTextBuffSizeForAutoCopy = 0;
	this->m_PlainTextBuffUsedSizeForAutoCopy = 0;

	// 現在の処理レコード
	this->m_CurrentRecord = NULL;

	// 現在の処理レコードインデックス
	this->m_CurrentRecordIndex = 0;

	// デバッグ情報
	this->m_DebugInfo = NULL;
	this->m_DebugInfoRecordTop = NULL;
	this->m_DebugInfoTextTop = NULL;

	// ファイル情報
	this->m_FileInfo = NULL;

	// 例外タグを無視
	this->m_IgnoreExceptionTag = false;

	// バイナリデータ
	this->m_IsBinData = false;

	// デバッグ情報有無
	this->m_HasDebugInfo = false;

	// ファイル情報有無
	this->m_HasFileInfo = false;

	// テキスト形式データ向けレコード情報格納用バッファ
	this->m_ParamCountMaxForText = 0;
	this->m_RecordInfoForText = NULL;
	this->m_ParamTypeInfoForText = NULL;
	this->m_ParamValueInfoForText = NULL;
	this->m_ParamTextBuffSizeForText = 0;
	this->m_ParamTextBuffForText = NULL;
	this->m_ParamWorkBuffSizeForText = 0;
	this->m_ParamWorkBuffForText = NULL;
	this->m_ParamExtTextBuffSizeForText = 0;
	this->m_ParamExtTextBuffForText = NULL;
	this->m_ParamExtWorkBuffSizeForText = 0;
	this->m_ParamExtWorkBuffForText = NULL;
	this->m_CurrParsePos = 0;
	this->m_CurrParsePtr = NULL;
	this->m_CurrParseLineNo = 0;
	this->m_CurrParsePosOnLineNo = 0;

	return true;
}

///-------------------------------------------------------------------------------------------------
/// タグ情報の設定
b8	CScriptInterpreter::SetTagGlobal(SI_TAG_PARAM* tag_param_top, u16& tag_param_count, b8& tag_param_is_sorted)
{
	const b8 result = InitTagTable(tag_param_top, tag_param_count, tag_param_is_sorted);
	if (!result) {
		return false;
	}
	s_TagParamCountGlobal = tag_param_count;
	s_TagParamTopGlobal = tag_param_top;
	return true;
}

///-------------------------------------------------------------------------------------------------
/// タグ情報の設定
b8	CScriptInterpreter::SetTag(SI_TAG_PARAM* tag_param_top, u16& tag_param_count, b8& tag_param_is_sorted)
{
	const b8 result = InitTagTable(tag_param_top, tag_param_count, tag_param_is_sorted);
	if (!result) {
		return false;
	}
	this->m_TagParamCount = tag_param_count;
	this->m_TagParamTop = tag_param_top;
	this->m_TagParamCache = tag_param_top;
	return true;
}

///-------------------------------------------------------------------------------------------------
/// タグ情報の事前初期化
b8	CScriptInterpreter::InitTagTable(SI_TAG_PARAM* tag_param_top, u16& tag_param_count, b8& tag_param_is_sorted)
{
	if (!tag_param_top) {
		return false;
	}

	// タグ情報数の計測と重複チェック
	if (tag_param_count == 0 && !tag_param_is_sorted)
	{
		const SI_TAG_PARAM* tag_param = tag_param_top;
		while (tag_param->tag_id_crc)
		{
#ifdef SI_DEBUG_MODE
			const SI_TAG_PARAM* tag_param_check = tag_param_top;
			for (u32 index = 0; index < tag_param_count; ++index, ++tag_param_check)
			{
				if (tag_param->tag_id_crc == tag_param_check->tag_id_crc)
				{
					// SI_PRINT_ERRORと漢字は使えない（main関数実行前に呼ばれる可能性がある為）
					PRINT("***** [error!] CScriptInterpreter::InitTagTables() SI_TAG_PARAM Missmatched. No.%d-tag and No.%d-tag is same CRC. (CRC=0x%08x)\n", index, tag_param_count + 1, SCast<u32>(tag_param->tag_id_crc));
					SI_ASSERT_FAIL_SILENT();
					break;
				}
			}
#endif//SI_DEBUG_MODE
			++tag_param;
			++tag_param_count;
		}
	}

	// タグ情報を昇順にソート
	if (!tag_param_is_sorted)
	{
		s32 index_begin = 0;
		s32 index_end = tag_param_count - 1;
		while (1)
		{
			{
				b8 is_exchanged = false;
				SI_TAG_PARAM* tag_param_prev = tag_param_top + index_begin;
				SI_TAG_PARAM* tag_param = tag_param_top + index_begin + 1;
				for (s32 index = index_begin + 1; index <= index_end; ++index, ++tag_param)
				{
					if (tag_param_prev->tag_id_crc > tag_param->tag_id_crc)
					{
						is_exchanged = true;
						SI_TAG_PARAM tag_param_tmp = *tag_param;
						*tag_param = *tag_param_prev;
						*tag_param_prev = tag_param_tmp;
					}
					tag_param_prev = tag_param;
				}
				if (!is_exchanged)
					break;
				--index_end;
				if (index_begin >= index_end)
					break;
			}
			{
				b8 is_exchanged = false;
				SI_TAG_PARAM* tag_param_next = tag_param_top + index_end;
				SI_TAG_PARAM* tag_param = tag_param_top + index_end - 1;
				for (s32 index = index_end - 1; index >= index_begin; --index, --tag_param)
				{
					if (tag_param->tag_id_crc > tag_param_next->tag_id_crc)
					{
						is_exchanged = true;
						SI_TAG_PARAM tag_param_tmp = *tag_param;
						*tag_param = *tag_param_next;
						*tag_param_next = tag_param_tmp;
					}
					tag_param_next = tag_param;
				}
				if (!is_exchanged)
					break;
				++index_begin;
				if (index_begin >= index_end)
					break;
			}
		}
		tag_param_is_sorted = true;
	}

	return true;
}

///-------------------------------------------------------------------------------------------------
/// 拡張パラメータをセット
void	CScriptInterpreter::SetExtParam(void* ext_param)
{
	this->m_ExtParam = ext_param;
}

///-------------------------------------------------------------------------------------------------
/// パラメータ解析用拡張テキストバッファをセット（テキスト形式時専用）
void	CScriptInterpreter::SetParamExtTextBuff(const u32 size, c8* buff)
{
	this->m_ParamExtTextBuffSizeForText = size;
	this->m_ParamExtTextBuffForText = buff;
}

///-------------------------------------------------------------------------------------------------
/// パラメータ解析用拡張ワークバッファをセット（テキスト形式時専用）
void	CScriptInterpreter::SetParamExtWorkBuff(const u32 size, c8* buff)
{
	this->m_ParamExtWorkBuffSizeForText = size;
	this->m_ParamExtWorkBuffForText = buff;
}

///-------------------------------------------------------------------------------------------------
/// スクリプトの設定
b8	CScriptInterpreter::SetScript(const void* file_buff, const u32 file_size, const c8* _file_name, const SI_FILE_TYPE script_file_type)
{
//	//拡張パラメータ
//	this->m_ExtParam = NULL;

	// ファイル名
	this->m_FileName = _file_name;

	// ファイルバッファ
	this->m_FileBuff = file_buff;

	// ファイルサイズ
	this->m_FileSize = file_size;

	// スクリプトファイル種別
	this->m_ScriptFileType = script_file_type;

	// ファイルヘッダー情報
	this->m_Header = NULL;

	// 基本データ（行データ）
	this->m_Record = NULL;

	// 文字列データ
	this->m_Text = NULL;

	// 複写された文字列データ
	this->m_DuplicatedText = NULL;

#if 0
#ifdef SI_USE_TEXT_BUFF
	// 文字列データを自動複写用バッファ
	this->m_TextBuffForAutoCopy = NULL;
#endif//SI_USE_TEXT_BUFF
	this->m_PlainTextBuffForAutoCopy = NULL;
	this->m_PlainTextBuffSizeForAutoCopy = 0;
	this->m_PlainTextBuffUsedSizeForAutoCopy = 0;
#endif // 0

	// 現在の処理レコード
	this->m_CurrentRecord = NULL;

	// 現在の処理レコードインデックス
	this->m_CurrentRecordIndex = 0;

	// デバッグ情報
	this->m_DebugInfo = NULL;
	this->m_DebugInfoRecordTop = NULL;
	this->m_DebugInfoTextTop = NULL;

	// ファイル情報
	this->m_FileInfo = NULL;

	// テキスト形式データ向けレコード情報格納用バッファ
	this->m_ParamCountMaxForText = 0;
	this->m_RecordInfoForText = NULL;
	this->m_ParamTypeInfoForText = NULL;
	this->m_ParamValueInfoForText = NULL;
	this->m_ParamTextBuffSizeForText = 0;
	this->m_ParamTextBuffForText = NULL;
	//this->m_ExtParamTextBuffSizeForText = 0;
	//this->m_ExtParamTextBuffForText = NULL;
	this->m_CurrParsePos = 0;
	this->m_CurrParsePtr = NULL;
	this->m_CurrParseLineNo = 0;
	this->m_CurrParsePosOnLineNo = 0;

	// バイナリデータ判定
	static const c8* TOOL_NAME = "\x001t2b\x0fe";
	b8 is_broken_data = false;
	this->m_IsBinData = false;
	this->m_HasDebugInfo = false;
	this->m_HasFileInfo = false;
	b8 is_match_file_size_and_bin_header_info = false;
	b8 is_match_file_size_and_bin_header_info_with_file_info = false;
	b8 is_match_file_size_and_bin_header_info_with_debug_info = false;
	b8 is_match_file_size_and_bin_header_info_with_debug_info_and_file_info = false;
	b8 is_consistented_debug_info = false;
	b8 is_match_tool_name_on_file_info = false;
	if (this->m_FileSize >= sizeof(SI_TAG_PARAM_BIN_HEADER))
	{
		const SI_TAG_PARAM_BIN_HEADER* header = RCast<const SI_TAG_PARAM_BIN_HEADER*>(this->m_FileBuff);
		const u32 base_file_size = (header->text_data_file_offset + header->text_data_total_size + (16 - 1)) & (~(16 - 1));
		if (base_file_size == this->m_FileSize)
		{
			is_match_file_size_and_bin_header_info = true;

			// バイナリデータ（デバッグ情報なし、ファイル情報なし）
			this->m_IsBinData = true;
			SI_PRINT("CScriptInterpreter::SetScript() スクリプト解析：バイナリデータ（デバッグ情報なし、ファイル情報なし）\n");
		}
		else if (base_file_size + sizeof(SI_TAG_PARAM_BIN_FILE_INFO) == this->m_FileSize)
		{
			is_match_file_size_and_bin_header_info_with_file_info = true;

			const SI_TAG_PARAM_BIN_FILE_INFO* file_info = RCast<const SI_TAG_PARAM_BIN_FILE_INFO*>(RCast<const c8*>(this->m_FileBuff) + this->m_FileSize - sizeof(SI_TAG_PARAM_BIN_FILE_INFO));
			if (memcmp(file_info->tool_name, TOOL_NAME, sizeof(file_info->tool_name)) == 0)
			{
				is_match_tool_name_on_file_info = true;

				// バイナリデータ（デバッグ情報なし、ファイル情報あり）
				this->m_IsBinData = true;
				this->m_HasFileInfo = true;
				SI_PRINT("CScriptInterpreter::SetScript() スクリプト解析：バイナリデータ（デバッグ情報なし、ファイル情報あり）\n");

				// ファイル情報
				this->m_FileInfo = file_info;
			}
			else
			{
#if 0
				// 破損データ
				is_broken_data = true;
				SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 破損データ：バイナリデータのファイル情報が正しくありません。\n");
				SI_ASSERT_FAIL_SILENT();
#endif // 0
				// テキストデータの可能性あり
			}
		}
		else if (base_file_size + sizeof(SI_TAG_PARAM_BIN_DEBUG) <= this->m_FileSize)
		{
			const SI_TAG_PARAM_BIN_DEBUG* debug_info = RCast<const SI_TAG_PARAM_BIN_DEBUG*>(RCast<const c8*>(this->m_FileBuff) + base_file_size);
			const SI_TAG_PARAM_BIN_DEBUG::HEADER* debug_header = &debug_info->header;
			const u32 debug_info_size = (debug_header->text_offset + debug_header->total_text_size + (16 - 1)) & (~(16 - 1));
			if (debug_header->total_size == debug_info_size)
			{
				is_consistented_debug_info = true;

				if (base_file_size + debug_info_size == this->m_FileSize)
				{
					is_match_file_size_and_bin_header_info_with_debug_info = true;

					// バイナリデータ（デバッグ情報あり、ファイル情報なし）
					this->m_IsBinData = true;
					this->m_HasDebugInfo = true;
					SI_PRINT("CScriptInterpreter::SetScript() スクリプト解析：バイナリデータ（デバッグ情報あり、ファイル情報なし）\n");

					// デバッグ情報
					this->m_DebugInfo = debug_info;
					this->m_DebugInfoRecordTop = RCast<const SI_TAG_PARAM_BIN_DEBUG::RECORD*>(RCast<const c8*>(debug_header) + sizeof(SI_TAG_PARAM_BIN_DEBUG::HEADER));
					this->m_DebugInfoTextTop = RCast<const SI_TAG_PARAM_BIN_DEBUG::TEXT*>(RCast<const c8*>(debug_header) + debug_header->text_offset);
				}
				else if (base_file_size + debug_info_size + sizeof(SI_TAG_PARAM_BIN_FILE_INFO) == this->m_FileSize)
				{
					is_match_file_size_and_bin_header_info_with_debug_info_and_file_info = true;

					const SI_TAG_PARAM_BIN_FILE_INFO* file_info = RCast<const SI_TAG_PARAM_BIN_FILE_INFO*>(RCast<const c8*>(this->m_FileBuff) + this->m_FileSize - sizeof(SI_TAG_PARAM_BIN_FILE_INFO));
					if (memcmp(file_info->tool_name, TOOL_NAME, sizeof(file_info->tool_name)) == 0)
					{
						is_match_tool_name_on_file_info = true;

						// バイナリデータ（デバッグ情報あり、ファイル情報あり）
						this->m_IsBinData = true;
						this->m_HasDebugInfo = true;
						this->m_HasFileInfo = true;
						SI_PRINT("CScriptInterpreter::SetScript() スクリプト解析：バイナリデータ（デバッグ情報あり、ファイル情報あり）\n");

						// デバッグ情報
						this->m_DebugInfo = debug_info;
						this->m_DebugInfoRecordTop = RCast<const SI_TAG_PARAM_BIN_DEBUG::RECORD*>(RCast<const c8*>(debug_header) + sizeof(SI_TAG_PARAM_BIN_DEBUG::HEADER));
						this->m_DebugInfoTextTop = RCast<const SI_TAG_PARAM_BIN_DEBUG::TEXT*>(RCast<const c8*>(debug_header) + debug_header->text_offset);

						// ファイル情報
						this->m_FileInfo = file_info;
					}
					else
					{
#if 0
						// 破損データ
						is_broken_data = true;
						SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 破損データ：バイナリデータのファイル情報が正しくありません。（デバッグ情報は正しい）\n");
						SI_ASSERT_FAIL_SILENT();
#endif // 0
						// テキストデータの可能性あり
					}
				}
				else
				{
#if 0
					// 破損データ
					is_broken_data = true;
					SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 破損データ：バイナリデータのデバッグ情報が正しくありません。\n");
					SI_ASSERT_FAIL_SILENT();
#endif // 0
					// テキストデータの可能性あり
				}
			}
			else
			{
#if 0
				// 破損データ
				is_broken_data = true;
				SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 破損データ：バイナリデータのデバッグ情報ヘッダーが正しくありません。\n");
				SI_ASSERT_FAIL_SILENT();
#endif // 0
				// テキストデータの可能性あり
			}
		}
		else
		{
			if (base_file_size + sizeof(SI_TAG_PARAM_BIN_FILE_INFO) <= this->m_FileSize)
			{
				const SI_TAG_PARAM_BIN_FILE_INFO* file_info = RCast<const SI_TAG_PARAM_BIN_FILE_INFO*>(RCast<const c8*>(this->m_FileBuff) + this->m_FileSize - sizeof(SI_TAG_PARAM_BIN_FILE_INFO));
				if (memcmp(file_info->tool_name, TOOL_NAME, sizeof(file_info->tool_name)) == 0)
				{
					is_match_tool_name_on_file_info = true;
#if 0
					// 破損データ　※バイナリデータヘッダーに基づくデータサイズの整合は取れないが、バイナリファイル情報はある
					is_broken_data = true;
					SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 破損データ：バイナリデータのヘッダーが正しくありません。\n");
					SI_ASSERT_FAIL_SILENT();
#endif // 0
					// テキストデータの可能性あり（？）
				}
				else
				{
					// テキストデータの可能性あり
				}
			}
			else
			{
				// テキストデータの可能性あり
			}
		}
	}
	else
	{
		// テキストデータの可能性あり
	}

	// テキストデータチェック
	if (!this->m_IsBinData && !is_broken_data)
	{
		// 最初の16バイトにバイナリ文字が含まれているかチェック（タブ、改行文字を除く）
		b8 has_binary_char = false;
		if (is_match_tool_name_on_file_info)
		{
			has_binary_char = true;
		}
		else
		{
			const s8* buff_p = RCast<const s8*>(this->m_FileBuff);
			for (u32 pos = 0; pos < sizeof(SI_TAG_PARAM_BIN_HEADER) && pos < this->m_FileSize; ++pos, ++buff_p)
			{
				const s8 c = *buff_p;
				if (c >= '\0' && c < ' ' && c != '\t' && c != '\r' && c != '\n')
				{
					has_binary_char = true;
					break;
				}
			}
		}
		if (has_binary_char)
		{
			// 破損データ
			is_broken_data = true;
#ifndef POISON_RELEASE
			SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 破損データ：バイナリファイルが壊れています。\n");
			if (is_match_file_size_and_bin_header_info) {
				SI_PRINT_ERROR("               ※部分的に正しい状態：バイナリデータヘッダーのファイルサイズ情報、実際のファイルサイズは一致しています。\n");
			}
			if (is_match_file_size_and_bin_header_info_with_file_info) {
				SI_PRINT_ERROR("               ※部分的に正しい状態：バイナリデータヘッダーのファイルサイズ情報＋ファイル情報のサイズ、実際のファイルサイズは一致しています。\n");
			}
			if (is_match_file_size_and_bin_header_info_with_debug_info) {
				SI_PRINT_ERROR("               ※部分的に正しい状態：バイナリデータヘッダーのファイルサイズ情報＋デバッグ情報のサイズ、実際のファイルサイズは一致しています。\n");
			}
			if (is_match_file_size_and_bin_header_info_with_debug_info_and_file_info) {
				SI_PRINT_ERROR("               ※部分的に正しい状態：バイナリデータヘッダーのファイルサイズ情報＋デバッグ情報のサイズ＋ファイル情報のサイズと、実際のファイルサイズは一致しています。\n");
			}
			if (is_consistented_debug_info) {
				SI_PRINT_ERROR("               ※部分的に正しい状態：デバッグ情報部が存在し、整合性が取れています。\n");
			}
			if (is_match_tool_name_on_file_info) {
				SI_PRINT_ERROR("               ※部分的に正しい状態：ファイル情報が存在し、正しい値が設定されています。\n");
			}
			SI_ASSERT_FAIL_SILENT();
#endif // POISON_RELEASE
		}
		else
		{
			// テキストデータ確定
			SI_PRINT("CScriptInterpreter::SetScript() スクリプト解析：テキストデータ\n");
		}
	}

	if (this->m_IsBinData)
	{
		// ファイルヘッダー情報
		const c8* header_ptr = RCast<const c8*>(this->m_FileBuff);
		this->m_Header = RCast<const SI_TAG_PARAM_BIN_HEADER*>(header_ptr);

		// 基本データ（行データ）
		this->m_Record = this->CalcFirstRecordForBin();

		// 文字列データ
		const c8* text_ptr = RCast<const c8*>(this->m_FileBuff) + this->m_Header->text_data_file_offset;
		this->m_Text = RCast<const SI_TAG_PARAM_BIN_TEXT*>(text_ptr);

		// 複写された文字列データ
		this->m_DuplicatedText = NULL;
	}

	return true;
}

///-------------------------------------------------------------------------------------------------
/// スクリプト実行
b8	CScriptInterpreter::Run()
{
	if (this->m_IsBinData) {
		return this->RunForBin();
	}
	return this->RunForText();
}
b8	CScriptInterpreter::Run(const b8 ignore_exception_tag)
{
	this->m_IgnoreExceptionTag = ignore_exception_tag;
	return this->Run();
}

///-------------------------------------------------------------------------------------------------
/// 指定したタグのレコードまでスキップ
/// ※for_next =	true  ... 次のタグ解析では指定のタグのコールバックが発生する
///					false ... 指定のタグまでスキップする為、次のタグ解析では指定のタグの次のタグのコールバックが発生する
b8	CScriptInterpreter::Skip(const hash32 tag_id_crc, const b8 for_next)
{
	if (this->m_IsBinData) {
		return this->SkipForBin(tag_id_crc, for_next);
	}
	return this->SkipForText(tag_id_crc, for_next);
}

///-------------------------------------------------------------------------------------------------
/// 最後のレコードまでスキップ
/// ※次のタグ解析では最後のタグのコールバックが発生する
b8	CScriptInterpreter::SkipEnd()
{
	if (this->m_IsBinData) {
		return this->SkipEndForBin();
	}
	return this->SkipEndForText();
}

///-------------------------------------------------------------------------------------------------
/// 全文字列データのメモリへの複写（バイナリ形式時専用）
/// ※これを事前にやっておくと、コールバック関数には複写された領域の文字列ポインタが渡される
#ifdef SI_USE_EASY_ALLOCATOR
const c8*	CScriptInterpreter::CopyTextAll(IEasyAllocatorPtrProxy allocator, u32& text_buff_size)
{
	if (this->m_IsBinData) {
		return this->CopyTextAllForBin(allocator, text_buff_size);
	}
	return NULL;
}
const c8*	CScriptInterpreter::CopyTextAll(IEasyAllocatorPtrProxy allocator)
{
	if (this->m_IsBinData) {
		return this->CopyTextAllForBin(allocator);
	}
	return NULL;
}
#endif//SI_USE_EASY_ALLOCATOR
#ifdef SI_USE_MEMORY_STAFF
const c8*	CScriptInterpreter::CopyTextAll(CMemoryStaff* mem_man, u32& text_buff_size)
{
	if (this->m_IsBinData) {
		return this->CopyTextAllForBin(mem_man, text_buff_size);
	}
	return NULL;
}
const c8*	CScriptInterpreter::CopyTextAll(CMemoryStaff* mem_man)
{
	if (this->m_IsBinData) {
		return this->CopyTextAllForBin(mem_man);
	}
	return NULL;
}
#endif//SI_USE_MEMORY_STAFF
#ifdef SI_USE_TEXT_BUFF
const c8*	CScriptInterpreter::CopyTextAll(CTextBuff* text_buff, u32& text_buff_size)
{
	if (this->m_IsBinData) {
		return this->CopyTextAllForBin(text_buff, text_buff_size);
	}
	return NULL;
}
const c8*	CScriptInterpreter::CopyTextAll(CTextBuff* text_buff)
{
	if (this->m_IsBinData) {
		return this->CopyTextAllForBin(text_buff);
	}
	return NULL;
}
const c8*	CScriptInterpreter::AddTextAll(CTextBuff* text_buff, u32& text_buff_size)
{
	if (this->m_IsBinData) {
		return this->AddTextAllForBin(text_buff, text_buff_size);
	}
	return NULL;
}
const c8*	CScriptInterpreter::AddTextAll(CTextBuff* text_buff)
{
	if (this->m_IsBinData) {
		return this->AddTextAllForBin(text_buff);
	}
	return NULL;
}
#endif//SI_USE_TEXT_BUFF
const c8*	CScriptInterpreter::CopyTextAll(c8* text_buff, const u32 text_buff_size_max, u32& text_buff_size)
{
	if (this->m_IsBinData) {
		return this->CopyTextAllForBin(text_buff, text_buff_size_max, text_buff_size);
	}
	return NULL;
}

#ifdef SI_USE_TEXT_BUFF
///-------------------------------------------------------------------------------------------------
/// 全文字列データを解析の際に自動的にメモリへ複写（テキスト形式時専用）
b8	CScriptInterpreter::SetTextBuffForAutoCopy(CTextBuff* text_buff, const b8 is_reuse_text)
{
	if (!this->m_IsBinData) {
		return this->SetTextBuffForAutoCopyForText(text_buff, is_reuse_text);
	}
	return false;
}
#endif//SI_USE_TEXT_BUFF

///-------------------------------------------------------------------------------------------------
/// スクリプト実行（バイナリ形式）
b8	CScriptInterpreter::RunForBin()
{
	if (!this->m_IsBinData) {
		return false;
	}
	if (this->m_TagParamCount <= 0 || !this->m_FileBuff || this->m_FileSize <= 0) {
		return false;
	}

	// 現在の処理レコード
	this->m_CurrentRecord = NULL;

	// 現在の処理レコードインデックス
	this->m_CurrentRecordIndex = 0;

	s32 record_count = 0;
	while (1)
	{
		const SI_TAG_PARAM_BIN_RECORD* record = this->GetNextRecordForBin(true);
		if (!record) {
			break;
		}
		++record_count;
	}

	return true;
}

///-------------------------------------------------------------------------------------------------
/// 次のレコード取得（バイナリ形式）
const SI_TAG_PARAM_BIN_RECORD*	CScriptInterpreter::GetNextRecordForBin(const b8 callback)
{
	if (!this->m_IsBinData) {
		return NULL;
	}
	if (!this->m_FileBuff || this->m_FileSize <= 0 || !this->m_Header) {
		return NULL;
	}

	if (this->m_CurrentRecordIndex >= this->m_Header->record_count) {
		return NULL;
	}

	if (!this->m_CurrentRecord)
	{
		// 最初のレコードを取得
		this->m_CurrentRecord = this->m_Record;
	}
	else
	{
		// 次のレコードを取得
		this->m_CurrentRecord = this->CalcNextRecordForBin();
	}

	// コールバック
	if (this->m_CurrentRecord && callback)
	{
		const hash32 tag_id_crc = this->m_CurrentRecord->header.tag_id_crc;
		const s32 param_count = this->m_CurrentRecord->header.param_count;
		const SI_TAG_PARAM* tag_param = this->SearchTagParam(tag_id_crc);

		b8 allow_callback = false;
		if (tag_param)
		{
			//if (tag_param->func)
			{
				allow_callback = true;
			}
		}
		else
		{
			if (!this->m_IgnoreExceptionTag)
			{
				if (this->m_ExceptionCallback)
				{
					allow_callback = true;
				}
				else
				{
					const c8* tag_id = this->SearchTagIDForBin(tag_id_crc);
					if (tag_id)
					{
						SI_PRINT_WARNING("+++++ [warning!] CScriptInterpreter::GetNextRecordForBin() タグ解析ワーニング：不明なタグが指定されています。\n");
						SI_PRINT_WARNING("                 （タグID=\"%s\", CRC=0x%08x, レコード番号=%d）\n", tag_id, SCast<u32>(tag_id_crc), this->m_CurrentRecordIndex);
					}
					else
					{
						SI_PRINT_WARNING("+++++ [warning!] CScriptInterpreter::GetNextRecordForBin() タグ解析ワーニング：不明なタグが指定されています。\n");
						SI_PRINT_WARNING("                 （タグID(CRC)=0x%08x, レコード番号=%d）\n", SCast<u32>(tag_id_crc), this->m_CurrentRecordIndex);
					}
					SI_ASSERT_FAIL_SILENT();
				}
			}
		}
		if (allow_callback)
		{
			// 型情報取得
			//const TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* param_type = this->CalcParamTypeTopForBin(this->m_CurrentRecord);
			const u8* param_type = this->CalcParamTypeTopForBin(this->m_CurrentRecord);

			// 値情報取得
			const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_value = this->CalcParamValueTopForBin(this->m_CurrentRecord);

			// コールバック
			SI_STACK_CORE st_core(this, param_type, param_value, param_count, this->m_ExtParam);
#ifdef SI_DEBUG_MODE
			{
#ifdef SI_VERBOSE_DEBUG_MODE
				const c8* tag_id = this->SearchTagIDForBin(tag_id_crc);
#else//SI_VERBOSE_DEBUG_MODE
				const c8* tag_id = NULL;
#endif//SI_VERBOSE_DEBUG_MODE
				st_core.SetDebugInfo(true, tag_id, tag_id_crc, 0, this->m_CurrentRecordIndex);
			}
#endif//SI_DEBUG_MODE
			SI_STACK st(st_core);
			if (tag_param)
			{
				tag_param->func(st);
			}
			else
			{
				this->m_ExceptionCallback(tag_id_crc, st);
			}
		}
	}

	++ this->m_CurrentRecordIndex;

	return this->m_CurrentRecord;
}

///-------------------------------------------------------------------------------------------------
/// 指定したタグのレコードまでスキップ（バイナリ形式）
///	※for_next =	true  ... 次のタグ解析では指定のタグのコールバックが発生する
///					false ... 指定のタグまでスキップする為、次のタグ解析では指定のタグの次のタグのコールバックが発生する
b8	CScriptInterpreter::SkipForBin(const hash32 tag_id_crc, const b8 for_next)
{
	if (!this->m_IsBinData) {
		return false;
	}
	while (1)
	{
		const s32						prev_record_index = this->m_CurrentRecordIndex;
		const SI_TAG_PARAM_BIN_RECORD*	prev_record = this->m_CurrentRecord;
		const SI_TAG_PARAM_BIN_RECORD*	record = this->GetNextRecordForBin(false);
		if (!record) {
			break;
		}
		if (record->header.tag_id_crc == tag_id_crc)
		{
			if (for_next)
			{
				this->m_CurrentRecordIndex = prev_record_index;
				this->m_CurrentRecord = prev_record;
			}
			return true;
			//break;
		}
	}
	return false;
}

///-------------------------------------------------------------------------------------------------
/// 最後のレコードまでスキップ（バイナリ形式）
/// ※次のタグ解析では最後のタグのコールバックが発生する
b8	CScriptInterpreter::SkipEndForBin()
{
	if (!this->m_IsBinData) {
		return false;
	}
	while (1)
	{
		const s32						prev_record_index = this->m_CurrentRecordIndex;
		const SI_TAG_PARAM_BIN_RECORD*	prev_record = this->m_CurrentRecord;
		const SI_TAG_PARAM_BIN_RECORD*	record = this->GetNextRecordForBin(false);
		if (!record)
		{
			this->m_CurrentRecordIndex = prev_record_index;
			this->m_CurrentRecord = prev_record;
			return true;
			//break;
		}
	}
	//return false;
}

///-------------------------------------------------------------------------------------------------
/// 全文字列データのメモリへの複写（バイナリ形式）
/// ※これを事前にやっておくと、コールバック関数には複写された領域の文字列ポインタが渡される
#ifdef SI_USE_EASY_ALLOCATOR
const c8*	CScriptInterpreter::CopyTextAllForBin(IEasyAllocatorPtrProxy allocator, u32& text_buff_size)
{
	if (!this->m_IsBinData) {
		return NULL;
	}
	if (isNull(allocator) || !this->m_Text) {
		return NULL;
	}

	if (this->m_Text->text[0] == '\0') {
		return NULL;
	}

	const c8* text_ptr = this->GetOriginalText();
	text_buff_size = this->GetTextDataSize();
	if (text_buff_size <= 0) {
		return NULL;
	}

	this->m_DuplicatedText = RCast<c8*>(allocator->Allocate(text_buff_size));
	if (this->m_DuplicatedText) {
		memcpy(this->m_DuplicatedText, text_ptr, text_buff_size);
	}
	else {
		text_buff_size = 0;
	}

	return this->m_DuplicatedText;
}
const c8*	CScriptInterpreter::CopyTextAllForBin(IEasyAllocatorPtrProxy allocator)
{
	u32 text_buff_size = 0;
	return this->CopyTextAllForBin(allocator, text_buff_size);
}
#endif//SI_USE_EASY_ALLOCATOR
#ifdef SI_USE_MEMORY_STAFF
const c8*	CScriptInterpreter::CopyTextAllForBin(CMemoryStaff* mem_man, u32& text_buff_size)
{
	if (!this->m_IsBinData) {
		return NULL;
	}
	if (!mem_man || !this->m_Text) {
		return NULL;
	}

	if (this->m_Text->text[0] == '\0') {
		return NULL;
	}

	const c8* text_ptr = this->GetOriginalText();
	text_buff_size = this->GetTextDataSize();
	this->m_DuplicatedText = RCast<c8*>(mem_man->Allocate(text_buff_size));
	if (this->m_DuplicatedText) {
		memcpy(this->m_DuplicatedText, text_ptr, text_buff_size);
	}
	else {
		text_buff_size = 0;
	}

	return this->m_DuplicatedText;
}
const c8*	CScriptInterpreter::CopyTextAllForBin(CMemoryStaff* mem_man)
{
	u32 text_buff_size = 0;
	return this->CopyTextAllForBin(mem_man, text_buff_size);
}
#endif//SI_USE_MEMORY_STAFF
#ifdef SI_USE_TEXT_BUFF
const c8*	CScriptInterpreter::CopyTextAllForBin(CTextBuff* text_buff, u32& text_buff_size)
{
	if (!this->m_IsBinData) {
		return NULL;
	}
	if (!text_buff || !this->m_Text) {
		return NULL;
	}

	if (this->m_Text->text[0] == '\0') {
		return NULL;
	}

	const c8* text_ptr = this->GetOriginalText();
	text_buff_size = this->GetTextDataSize();
	this->m_DuplicatedText = CCast<c8*>(text_buff->CopyAll(text_ptr, text_buff_size, this->GetTextNodeCount(), false));
	if (!this->m_DuplicatedText) {
		text_buff_size = 0;
	}

	return this->m_DuplicatedText;
}
const c8*	CScriptInterpreter::CopyTextAllForBin(CTextBuff* text_buff)
{
	u32 text_buff_size = 0;
	return this->CopyTextAllForBin(text_buff, text_buff_size);
}
const c8*	CScriptInterpreter::AddTextAllForBin(CTextBuff* text_buff, u32& text_buff_size)
{
	if (!this->m_IsBinData) {
		return NULL;
	}
	if (!text_buff || !this->m_Text) {
		return NULL;
	}

	if (this->m_Text->text[0] == '\0') {
		return NULL;
	}

	const c8* text_ptr = this->GetOriginalText();
	text_buff_size = this->GetTextDataSize();
	this->m_DuplicatedText = CCast<c8*>(text_buff->AddTextSet(text_ptr, text_buff_size, this->GetTextNodeCount()));
	if (!this->m_DuplicatedText) {
		text_buff_size = 0;
	}

	return this->m_DuplicatedText;
}
const c8*	CScriptInterpreter::AddTextAllForBin(CTextBuff* text_buff)
{
	u32 text_buff_size = 0;
	return this->AddTextAllForBin(text_buff, text_buff_size);
}
#endif//SI_USE_TEXT_BUFF
const c8*	CScriptInterpreter::CopyTextAllForBin(c8* text_buff, const u32 text_buff_size_max, u32& text_buff_size)
{
	if (!this->m_IsBinData) {
		return NULL;
	}
	if (!text_buff || !this->m_Text) {
		return NULL;
	}

	if (this->m_Text->text[0] == '\0') {
		return NULL;
	}

	text_buff_size = this->GetTextDataSize();
	if (text_buff_size > text_buff_size_max)
	{
		//PRINT_CRITICAL("***** [error!] CScriptInterpreter::SetScript() テキストバッファをコピーするメモリがありません。(%d/%d)\n", text_buff_size, text_buff_size_max);
		//ASSERT_FAIL_SILENT();
		return NULL;
	}
	memcpy(text_buff, this->GetOriginalText(), text_buff_size);
	this->m_DuplicatedText = text_buff;

	return this->m_DuplicatedText;
}

///-------------------------------------------------------------------------------------------------
/// 文字列を取得（バイナリ形式）
const c8*	CScriptInterpreter::GetTextForBin(const u32 offset) const
{
	if (!this->m_IsBinData) {
		return NULL;
	}
	if (!this->m_FileBuff || this->m_FileSize <= 0 || !this->m_Header || !this->m_Text) {
		return NULL;
	}

	if (SCast<u32>(offset) == 0xffffffff) {
		return "";
	}
	if (SCast<u32>(offset) & 0x80000000) {
		return NULL;
	}

	if (offset >= this->m_Header->text_data_total_size) {
		return NULL;
	}

	const c8* str = NULL;
	if (this->m_DuplicatedText) {
		str = this->m_DuplicatedText + offset;
	}
	else {
		str = this->m_Text->text + offset;
	}

	return str;
}

///-------------------------------------------------------------------------------------------------
/// タグCRCからタグID（文字列）を取得（バイナリ形式）　※デバッグ用
const c8*	CScriptInterpreter::SearchTagIDForBin(const hash32 tag_id_crc) const
{
	if (!this->m_DebugInfo || !this->m_DebugInfoRecordTop || !this->m_DebugInfoTextTop) {
		return NULL;
	}
	const u32 count = this->m_DebugInfo->header.node_count;
	const SI_TAG_PARAM_BIN_DEBUG::RECORD* record = this->m_DebugInfoRecordTop;
	for (u32 index = 0; index < count; ++index, ++record)
	{
		if (record->tag_id_crc == tag_id_crc) {
			return this->m_DebugInfoTextTop->text + record->text_offset;
		}
	}
	return NULL;
}

///-------------------------------------------------------------------------------------------------
/// スクリプト実行（テキスト形式）
b8	CScriptInterpreter::RunForText()
{
	if (this->m_IsBinData) {
		return false;
	}
	if (this->m_TagParamCount <= 0 || !this->m_FileBuff || this->m_FileSize <= 0) {
		return false;
	}

	// テキスト形式データ向けレコード情報格納用バッファをセットアップ
	SI_TAG_PARAM_BIN_RECORD record_info;
	u8 param_type_info[PARAM_COUNT_MAX / SI_RECORD_PARAM_TYPE_NUM_OF_BYTE];
	SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE param_value_info[PARAM_COUNT_MAX];
	c8 param_text_buff[PARAM_TEXT_BUFF_SIZE];
	c8 param_work_buff[PARAM_WORK_BUFF_SIZE];
	this->SetupRecordBuffForText(PARAM_COUNT_MAX, &record_info, param_type_info, param_value_info, PARAM_TEXT_BUFF_SIZE, param_text_buff, PARAM_WORK_BUFF_SIZE, param_work_buff);

	// 現在の処理レコード
	this->m_CurrentRecord = NULL;

	// 現在の処理レコードインデックス
	this->m_CurrentRecordIndex = 0;

	// 現在の解析位置
	this->m_CurrParsePos = 0;
	this->m_CurrParsePtr = RCast<const c8*>(this->m_FileBuff);
	this->m_CurrParseLineNo = 1;
	this->m_CurrParsePosOnLineNo = 1;

	s32 record_count = 0;
	while (1)
	{
		const SI_TAG_PARAM_BIN_RECORD* record = this->GetNextRecordForText(true);
		if (!record) {
			break;
		}
		++record_count;
	}

	return true;
}

///-------------------------------------------------------------------------------------------------
/// レコード情報格納用バッファをセットアップ（テキスト形式）
void	CScriptInterpreter::SetupRecordBuffForText(const u32 param_count_max, SI_TAG_PARAM_BIN_RECORD* record_info, u8* param_type_info, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_value_info, const u32 param_text_buff_size, c8* param_text_buff, const u32 param_work_buff_size, c8* param_work_buff)
{
	this->m_ParamCountMaxForText = param_count_max;
	this->m_RecordInfoForText = record_info;
	this->m_ParamTypeInfoForText = param_type_info;
	this->m_ParamValueInfoForText = param_value_info;
	this->m_ParamTextBuffSizeForText = param_text_buff_size;
	this->m_ParamTextBuffForText = param_text_buff;
//	this->m_ParamExtTextBuffSizeForText = 0;
//	this->m_ParamExtTextBuffForText = NULL;
	this->m_ParamWorkBuffSizeForText = param_work_buff_size;
	this->m_ParamWorkBuffForText = param_work_buff;
//	this->m_ParamExtWorkBuffSizeForText = 0;
//	this->m_ParamExtWorkBuffForText = NULL;
}

///-------------------------------------------------------------------------------------------------
/// 次のレコード取得（テキスト形式）
const SI_TAG_PARAM_BIN_RECORD*	CScriptInterpreter::GetNextRecordForText(const b8 callback)
{
	if (this->m_IsBinData) {
		return NULL;
	}
	if (!this->m_FileBuff || this->m_FileSize <= 0) {
		return NULL;
	}
	if (!this->m_CurrParsePtr || this->m_CurrParsePtr >= RCast<const c8*>(this->m_FileBuff) + this->m_FileSize) {
		return NULL;
	}

	u32 param_text_buff_size = (this->m_ParamExtTextBuffForText ? this->m_ParamExtTextBuffSizeForText : this->m_ParamTextBuffSizeForText);
	c8* param_text_buff = (this->m_ParamExtTextBuffForText ? this->m_ParamExtTextBuffForText : this->m_ParamTextBuffForText);

	u32 param_work_buff_size = (this->m_ParamExtWorkBuffForText ? this->m_ParamExtWorkBuffSizeForText : this->m_ParamWorkBuffSizeForText);
	c8* param_work_buff = (this->m_ParamExtWorkBuffForText ? this->m_ParamExtWorkBuffForText : this->m_ParamWorkBuffForText);

	if (this->m_ParamCountMaxForText == 0 ||
		this->m_RecordInfoForText == NULL ||
		this->m_ParamTypeInfoForText == NULL ||
		this->m_ParamValueInfoForText == NULL ||
		param_text_buff_size == 0 ||
		param_text_buff == NULL ||
		param_work_buff_size == 0 ||
		param_work_buff == NULL)
	{
		SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() テキスト解析の為のバッファ割り当てが不分です。\n");
		SI_ASSERT_FAIL_SILENT();
		return NULL;
	}

	memset(this->m_RecordInfoForText, 0, sizeof(SI_TAG_PARAM_BIN_RECORD));
	memset(this->m_ParamTypeInfoForText, 0, sizeof(u8) * SI_CALC_RECORD_PARAM_TYPE_BYTE_NUM(this->m_ParamCountMaxForText));
	memset(this->m_ParamValueInfoForText, 0, sizeof(SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE) * this->m_ParamCountMaxForText);
	u32 param_count = 0;

	enum PURSE_PHASE
	{
		PURSE_PHASE_TAG = 0,
		PURSE_PHASE_PARAM,
		PURSE_PHASE_PARAM_TEXT,
		PURSE_PHASE_COMMENT_C,
		PURSE_PHASE_COMMENT_CPP,
		PURSE_PHASE_COMMENT_SHARP,
		PURSE_PHASE_ERROR
	};
	enum PURSE_ERROR
	{
		PURSE_ERROR_NONE = 0,
		PURSE_ERROR_NO_TAG,
		PURSE_ERROR_ILLEGAL_CHARACTER_ON_TAG,
		PURSE_ERROR_WORK_BUFFER_OVERFLOW_ON_TAG,
		PURSE_ERROR_ILLEGAL_CHARACTER_ON_PARAM,
		PURSE_ERROR_ILLEGAL_CHARACTER_OR_FUNC_ON_PARAM,
		PURSE_ERROR_OVERFLOW_ON_PARAM,
		PURSE_ERROR_BROKEN_SYNTAX_FOR_FUNC,
		PURSE_ERROR_EMPTY_ON_PARAM,
		PURSE_ERROR_WORK_BUFFER_OVERFLOW_ON_PARAM,
		PURSE_ERROR_TEXT_BUFFER_OVERFLOW_ON_PARAM,
		PURSE_ERROR_EXT_TEXT_BUFFER_OVERFLOW_ON_PARAM,
		PURSE_ERROR_ILLEGAL_PARAM_ON_FUNC,
		PURSE_ERROR_OVERFLOW_PARAM_ON_FUNC,
		PURSE_ERROR_EMPTY_ON_FUNC_PARAM,
		PURSE_ERROR_NO_PARAM_ON_FUNC,
		PURSE_ERROR_NOT_IMPLEMENTED_ON_FUNC,
		PURSE_ERROR_PARAMTER_OVERFLOW
	};
	enum PURSE_FUNC
	{
		PURSE_FUNC_NONE = 0,
		PURSE_FUNC_CRC,
		//PURSE_FUNC_CRC2,
		PURSE_FUNC_TORADIAN,
		PURSE_FUNC_TODEGREE,
		PURSE_FUNC_TOINT,
		PURSE_FUNC_TOFLOAT,
		PURSE_FUNC_SQR,
		PURSE_FUNC_SUM,
		PURSE_FUNC_AVE
	};
	enum PURSE_PARAM_SUBTYPE
	{
		PURSE_PARAM_SUBTYPE_NONE = 0,
		PURSE_PARAM_SUBTYPE_INT_HEX,
		PURSE_PARAM_SUBTYPE_INT_SIGN,
		PURSE_PARAM_SUBTYPE_INT_ZERO
	};

#ifdef SI_DEBUG_MODE
	b8 is_already_PURSE_ERROR_PARAMTER_OVERFLOW = false;
	b8 is_already_PURSE_ERROR_TEXT_BUFFER_OVERFLOW_ON_PARAM = false;
	b8 is_already_PURSE_ERROR_EXT_TEXT_BUFFER_OVERFLOW_ON_PARAM = false;
#endif//SI_DEBUG_MODE

	PURSE_PHASE purse_phase = PURSE_PHASE_TAG;
	PURSE_ERROR purse_error = PURSE_ERROR_NONE;
	u32 param_text_buff_pos = 0;
	c8* param_text_buff_p = param_text_buff;
	u32 param_work_buff_pos = 0;
	c8* param_work_buff_p = param_work_buff;

#ifdef SI_DEBUG_MODE
	u32 tag_id_row_debug = 0;
	c8 tag_id_debug[64];
	tag_id_debug[0] = '\0';
#endif//SI_DEBUG_MODE
	hash32 tag_id_crc = 0;
	const SI_TAG_PARAM* tag_param = NULL;
	b8 allow_callback = false;

	u32 line_no_for_tag = 0;
	u32& line_no = this->m_CurrParseLineNo;
	u32& pos_on_line = this->m_CurrParsePosOnLineNo;
	b8 has_tag = false;
	c8 quotation = '\0';
	SI_STACK_TYPE param_type = SI_TYPE_UNKNOWN;
	PURSE_PARAM_SUBTYPE param_subtype = PURSE_PARAM_SUBTYPE_NONE;
	b8 param_with_space = false;
	b8 is_finish = false;
	b8 is_top_of_line = true;
	PURSE_FUNC param_func = PURSE_FUNC_NONE;
	const c8* param_func_name = NULL;
	u32 param_func_param_index_begin = 0;
	u32 param_func_text_buff_pos_begin = 0;
	b8 param_func_has_bracket = false;
	b8 param_func_is_call = false;
	b8 has_error = false;

	while (!is_finish && this->m_CurrParsePos < this->m_FileSize)
	{
		const c8 c = *this->m_CurrParsePtr;
		if (!c) {
			break;
		}
		const c8 c_next = this->m_CurrParsePos + 1 < this->m_FileSize ? *(this->m_CurrParsePtr + 1) : '\0';
		u32 pos_plus = 1;
		if (c != ' ' && c != '\t') {
			is_top_of_line = false;
		}

		b8 is_cr = false;
		b8 is_normal_chara = false;
		b8 is_add_chara_to_work = false;
		b8 is_add_param = false;
		b8 is_pickup_tag = false;
		b8 is_show_error = false;
		c8 c_for_work = '\0';
		switch (purse_phase)
		{
		//------------------------------
		// タグ取得フェーズ
		case PURSE_PHASE_TAG:
		{
			switch (c)
			{
			// クォーテーション
			// ・タグ検出済み ... 文字列取得フェーズへ
			// ・タグ未検出   ... エラー
			case '\'':
			case '\"':
			{
				if (!has_tag)
				{
					purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_TAG;
					is_show_error = true;
					is_add_chara_to_work = true;
				}
				purse_phase = PURSE_PHASE_PARAM_TEXT;
				quotation = c;
			}
			break;
			// 空白orタブ
			// ・タグ検出済み ... パラメータ取得フェーズへ
			// ・タグ未検出   ... スキップ
			case ' ':
			case '\t':
			{
				if (has_tag)
				{
					is_pickup_tag = true;
				}
			}
			break;
			// セミコロン
			// ・タグ検出済み ... レコード検出処理終了 → コールバック実行
			// ・タグ未検出   ... エラー
			case ';':
			{
				if (has_tag)
				{
					is_pickup_tag = true;
					is_finish = true;
				}
				else
				{
					purse_error = PURSE_ERROR_NO_TAG;
					is_show_error = true;
				}
			}
			break;
			// コメント開始検出①
			// ・「//」 ... コメントスキップフェーズへ
			// ・「/*」 ... コメントスキップフェーズへ
			// ・その他 ... エラー
			case '/':
			{
				if (c_next == '/')
				{
					++pos_plus;
					purse_phase = PURSE_PHASE_COMMENT_CPP;
				}
				else if (c_next == '*')
				{
					++pos_plus;
					purse_phase = PURSE_PHASE_COMMENT_C;
				}
				else
				{
					purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_TAG;
					is_show_error = true;
					is_add_chara_to_work = true;
				}
			}
			break;
			// コメント開始検出②
			// ・行頭   ... コメントスキップフェーズへ
			// ・その他 ... エラー
			case '#':
			{
				if (is_top_of_line)
				{
					purse_phase = PURSE_PHASE_COMMENT_SHARP;
				}
				else
				{
					purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_TAG;
					is_show_error = true;
					is_add_chara_to_work = true;
				}
			}
			break;
			// 改行
			// ・タグ検出済み ... パラメータ取得フェーズへ
			// ・タグ未検出   ... スキップ
			case '\r':
			{
				if (c_next == '\n') {
					++pos_plus;
				}
			}
			//break;
			case '\n':
			{
				is_cr = true;
				if (has_tag) {
					is_pickup_tag = true;
				}
			}
			break;
			// その他
			// ・英数字 ... タグ文字列抽出
			// ・その他 ... エラー
			default:
			{
				is_normal_chara = true;
				if ((c >= 'a' && c <= 'z') ||
					(c >= 'A' && c <= 'Z') ||
					(c >= '0' && c <= '9') ||
					c == '_' || c == '-')
				{
					if (!has_tag)
					{
						has_tag = true;
						param_work_buff_p = param_work_buff;
						param_work_buff_pos = 0;
						line_no_for_tag = line_no;
					}
					is_add_chara_to_work = true;
				}
				else
				{
					purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_TAG;
					is_show_error = true;
					is_add_chara_to_work = true;
				}
			}
			break;
			}
		}
		break;

		//------------------------------
		// パラメータ取得フェーズ
		case PURSE_PHASE_PARAM:
		{
			switch (c)
			{
				// クォーテーション
				// ・パラメータ未抽出状態 ... 文字列パラメータ取得フェーズへ
				// ・その他               ... エラー
			case '\'':
			case '\"':
			{
				if (param_type == SI_TYPE_UNKNOWN)
				{
					if (param_func != PURSE_FUNC_NONE && !param_func_has_bracket)
					{
						purse_error = PURSE_ERROR_BROKEN_SYNTAX_FOR_FUNC;
						is_show_error = true;

						param_func = PURSE_FUNC_NONE;
						param_count = param_func_param_index_begin;
						param_text_buff_pos = param_func_text_buff_pos_begin;
						param_func_has_bracket = false;
						param_func_is_call = false;
					}
					else
					{
						purse_phase = PURSE_PHASE_PARAM_TEXT;
						quotation = c;
						param_type = SI_STRING_DIRECT;
						param_subtype = PURSE_PARAM_SUBTYPE_NONE;
					}
				}
				else
				{
					purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_PARAM;
					is_show_error = true;
					is_add_chara_to_work = true;
				}
			}
			break;
			// 空白orタブ
			// ・パラメータ未抽出状態 ... スキップ
			// ・その他               ... 空白検出
			case ' ':
			case '\t':
			{
				if (param_type != SI_TYPE_UNKNOWN)
				{
					param_with_space = true;
				}
			}
			break;
			// カンマ
			// ・パラメータ未抽出状態 ... エラー
			// ・その他               ... パラメータ抽出
			case ',':
			{
				if (param_type == SI_TYPE_UNKNOWN)
				{
					if (param_func != PURSE_FUNC_NONE && !param_func_has_bracket)
					{
						purse_error = PURSE_ERROR_BROKEN_SYNTAX_FOR_FUNC;
						is_show_error = true;

						param_func = PURSE_FUNC_NONE;
						param_count = param_func_param_index_begin;
						param_text_buff_pos = param_func_text_buff_pos_begin;
						param_func_has_bracket = false;
						param_func_is_call = false;
					}
					else
					{
						purse_error = PURSE_ERROR_EMPTY_ON_PARAM;
						is_show_error = true;
					}
				}
				else
				{
					is_add_param = true;
				}
			}
			break;
			// セミコロン
			// ・パラメータ未抽出状態 ... エラー
			// ・その他               ... パラメータ抽出＆レコード検出処理終了 → コールバック実行
			case ';':
			{
				if (param_func != PURSE_FUNC_NONE)
				{
					purse_error = PURSE_ERROR_BROKEN_SYNTAX_FOR_FUNC;
					is_show_error = true;

					param_func = PURSE_FUNC_NONE;
					param_count = param_func_param_index_begin;
					param_text_buff_pos = param_func_text_buff_pos_begin;
					param_func_has_bracket = false;
					param_func_is_call = false;
				}
				else
				{
					if (param_type == SI_TYPE_UNKNOWN)
					{
						purse_error = PURSE_ERROR_EMPTY_ON_PARAM;
						is_show_error = true;
					}
					else
					{
						is_add_param = true;
					}
				}
				is_finish = true;
			}
			break;
			// コメント開始検出①
			// ・「//」 ... コメントスキップフェーズへ
			// ・「/*」 ... コメントスキップフェーズへ
			// ・その他 ... エラー
			case '/':
			{
				if (c_next == '/')
				{
					++pos_plus;
					purse_phase = PURSE_PHASE_COMMENT_CPP;
				}
				else if (c_next == '*')
				{
					++pos_plus;
					purse_phase = PURSE_PHASE_COMMENT_C;
				}
				else
				{
					purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_PARAM;
					is_show_error = true;
					is_add_chara_to_work = true;
				}
			}
			break;
			// コメント開始検出②
			// ・行頭   ... コメントスキップフェーズへ
			// ・その他 ... エラー
			case '#':
			{
				if (is_top_of_line)
				{
					purse_phase = PURSE_PHASE_COMMENT_SHARP;
				}
				else
				{
					purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_PARAM;
					is_show_error = true;
					is_add_chara_to_work = true;
				}
			}
			break;
			// 改行
			// ・パラメータ未抽出状態 ... スキップ
			// ・その他               ... 空白検出扱い
			case '\r':
			{
				if (c_next == '\n') {
					++pos_plus;
				}
			}
			//break;
			case '\n':
			{
				is_cr = true;
				if (param_type != SI_TYPE_UNKNOWN)
				{
					param_with_space = true;
				}
			}
			break;
			// 開始括弧（関数の引数開始）
			case '(':
			{
				if (param_func != PURSE_FUNC_NONE && !param_func_has_bracket)
				{
					param_func_has_bracket = true;
				}
				else
				{
					purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_PARAM;
					is_show_error = true;
					is_add_chara_to_work = true;
				}
			}
			break;
			// 終了括弧（関数の引数終了）
			case ')':
			{
				if (param_func != PURSE_FUNC_NONE && param_func_has_bracket)
				{
					if (param_type != SI_TYPE_UNKNOWN)
					{
						is_add_param = true;
					}
					else
					{
						if (param_func_param_index_begin < param_count)
						{
							purse_error = PURSE_ERROR_EMPTY_ON_FUNC_PARAM;
							is_show_error = true;
						}
					}
					param_func_is_call = true;
				}
				else
				{
					purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_PARAM;
					is_show_error = true;
					is_add_chara_to_work = true;
				}
			}
			break;
			// その他
			// ・空白検出後 ... エラー
			// ・数字       ... パラメータ種別不明時なら整数と解釈してバッファリング、整数時か小数時ならバッファリング、それ以外ならエラー
			// ・'-'        ... パラメータ種別不明時なら整数と解釈してバッファリング、それ以外ならエラー
			// ・'.'        ... パラメータ種別不明時もしくは整数時ならパラメータは小数と解釈してバッファリング、それ以外ならエラー
			// ・その他     ... 関数またはエラー
			default:
			{
				if (param_with_space)
				{
					if (param_type == SI_INT)
					{
						if (param_work_buff[0] == '-' && param_work_buff_pos == 1)
						{
							param_with_space = false;
						}
					}
				}
				if (param_with_space)
				{
					purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_PARAM;
					is_show_error = true;
					is_add_chara_to_work = true;
				}
				else
				{
					if (c >= '0' && c <= '9')
					{
						if (param_type == SI_TYPE_UNKNOWN)
						{
							if (param_func != PURSE_FUNC_NONE && !param_func_has_bracket)
							{
								purse_error = PURSE_ERROR_BROKEN_SYNTAX_FOR_FUNC;
								is_show_error = true;

								param_func = PURSE_FUNC_NONE;
								param_count = param_func_param_index_begin;
								param_text_buff_pos = param_func_text_buff_pos_begin;
								param_func_has_bracket = false;
								param_func_is_call = false;
							}
							else
							{
								param_type = SI_INT;
								if (c == '0' && (c_next == 'x' || c_next == 'X'))
								{
									param_subtype = PURSE_PARAM_SUBTYPE_INT_HEX;
									++pos_plus;
								}
								else
								{
									if (c == '0') {
										param_subtype = PURSE_PARAM_SUBTYPE_INT_ZERO;
									}
									else {
										param_subtype = PURSE_PARAM_SUBTYPE_NONE;
									}
									c_for_work = c;
									is_add_chara_to_work = true;
								}
							}
						}
						else if (param_type == SI_INT || param_type == SI_FLOAT)
						{
							// ※001 や 00.2 などの '0' が余分に付いた表記をエラー扱いするならこの箇所を有効化する
#if 1
							if (param_type == SI_INT && param_subtype == PURSE_PARAM_SUBTYPE_INT_ZERO)
							{
								purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_PARAM;
								is_show_error = true;
								is_add_chara_to_work = true;
							}
							else
#endif								
							{
								c_for_work = c;
								is_add_chara_to_work = true;
								if (param_subtype == PURSE_PARAM_SUBTYPE_INT_SIGN)
								{
									if (c == '0') {
										param_subtype = PURSE_PARAM_SUBTYPE_INT_ZERO;
									}
									else {
										param_subtype = PURSE_PARAM_SUBTYPE_NONE;
									}
								}
								else if (param_subtype == PURSE_PARAM_SUBTYPE_INT_ZERO) {
									param_subtype = PURSE_PARAM_SUBTYPE_NONE;
								}
							}
						}
						else
						{
							purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_PARAM;
							is_show_error = true;
							is_add_chara_to_work = true;
						}
					}
					else if (c == '-')
					{
						if (param_type == SI_TYPE_UNKNOWN)
						{
							if (param_func != PURSE_FUNC_NONE && !param_func_has_bracket)
							{
								purse_error = PURSE_ERROR_BROKEN_SYNTAX_FOR_FUNC;
								is_show_error = true;

								param_func = PURSE_FUNC_NONE;
								param_count = param_func_param_index_begin;
								param_text_buff_pos = param_func_text_buff_pos_begin;
								param_func_has_bracket = false;
								param_func_is_call = false;
							}
							else
							{
								param_type = SI_INT;
								param_subtype = PURSE_PARAM_SUBTYPE_INT_SIGN;
								c_for_work = c;
								is_add_chara_to_work = true;
							}
						}
						else
						{
							purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_PARAM;
							is_show_error = true;
							is_add_chara_to_work = true;
						}
					}
					else if (c == '.')
					{
						if (param_type == SI_TYPE_UNKNOWN)
						{
							if (param_func != PURSE_FUNC_NONE && !param_func_has_bracket)
							{
								purse_error = PURSE_ERROR_BROKEN_SYNTAX_FOR_FUNC;
								is_show_error = true;

								param_func = PURSE_FUNC_NONE;
								param_count = param_func_param_index_begin;
								param_text_buff_pos = param_func_text_buff_pos_begin;
								param_func_has_bracket = false;
								param_func_is_call = false;
							}
							else
							{
								param_type = SI_FLOAT;
								param_subtype = PURSE_PARAM_SUBTYPE_NONE;
								c_for_work = c;
								is_add_chara_to_work = true;
							}
						}
						else if (param_type == SI_INT && param_subtype != PURSE_PARAM_SUBTYPE_INT_HEX)
						{
							param_type = SI_FLOAT;
							param_subtype = PURSE_PARAM_SUBTYPE_NONE;
							c_for_work = c;
							is_add_chara_to_work = true;
						}
						else
						{
							purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_PARAM;
							is_show_error = true;
							is_add_chara_to_work = true;
						}
					}
					else
					{
						if (param_type == SI_INT && param_subtype == PURSE_PARAM_SUBTYPE_INT_HEX && ((c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F')))
						{
							c_for_work = c;
							is_add_chara_to_work = true;
						}
						else if (param_type == SI_TYPE_UNKNOWN)
						{
							if (param_func != PURSE_FUNC_NONE && !param_func_has_bracket)
							{
								purse_error = PURSE_ERROR_BROKEN_SYNTAX_FOR_FUNC;
								is_show_error = true;

								param_func = PURSE_FUNC_NONE;
								param_count = param_func_param_index_begin;
								param_text_buff_pos = param_func_text_buff_pos_begin;
								param_func_has_bracket = false;
								param_func_is_call = false;
							}
							else
							{
								if (param_func == PURSE_FUNC_NONE)
								{
									// 関数照合
									if (strncmp(this->m_CurrParsePtr, "meGenerateNameHash", 18) == 0) { param_func = PURSE_FUNC_CRC;      pos_plus = 18; param_func_name = "meGenerateNameHash"; }
									else if (strncmp(this->m_CurrParsePtr, "CRC32", 5) == 0) { param_func = PURSE_FUNC_CRC;      pos_plus = 5; param_func_name = "CRC32"; }
									else if (strncmp(this->m_CurrParsePtr, "CRC", 3) == 0) { param_func = PURSE_FUNC_CRC;      pos_plus = 3; param_func_name = "CRC"; }
									else if (strncmp(this->m_CurrParsePtr, "Hash", 4) == 0) { param_func = PURSE_FUNC_CRC;      pos_plus = 4; param_func_name = "Hash"; }
									else if (strncmp(this->m_CurrParsePtr, "NameHash", 8) == 0) { param_func = PURSE_FUNC_CRC;      pos_plus = 8; param_func_name = "NameHash"; }
									//else if(strncmp(this->m_CurrParsePtr, "CEncode::CRC32",     14) == 0){ param_func = PURSE_FUNC_CRC2;     pos_plus = 14; param_func_name = "CEncode::CRC32";     }
									else if (strncmp(this->m_CurrParsePtr, "ToRadian", 8) == 0) { param_func = PURSE_FUNC_TORADIAN; pos_plus = 8; param_func_name = "ToRadian"; }
									else if (strncmp(this->m_CurrParsePtr, "ToDegree", 8) == 0) { param_func = PURSE_FUNC_TODEGREE; pos_plus = 8; param_func_name = "ToDegree"; }
									else if (strncmp(this->m_CurrParsePtr, "ToInt", 5) == 0) { param_func = PURSE_FUNC_TOINT;    pos_plus = 5; param_func_name = "ToInt"; }
									else if (strncmp(this->m_CurrParsePtr, "ToFloat", 7) == 0) { param_func = PURSE_FUNC_TOFLOAT;  pos_plus = 7; param_func_name = "ToFloat"; }
									else if (strncmp(this->m_CurrParsePtr, "Sqrt", 4) == 0) { param_func = PURSE_FUNC_SQR;      pos_plus = 4; param_func_name = "Sqrt"; }
									else if (strncmp(this->m_CurrParsePtr, "Sqr", 3) == 0) { param_func = PURSE_FUNC_SQR;      pos_plus = 3; param_func_name = "Sqr"; }
									else if (strncmp(this->m_CurrParsePtr, "Sum", 3) == 0) { param_func = PURSE_FUNC_SUM;      pos_plus = 3; param_func_name = "Sum"; }
									else if (strncmp(this->m_CurrParsePtr, "Average", 7) == 0) { param_func = PURSE_FUNC_AVE;      pos_plus = 7; param_func_name = "Average"; }
									else if (strncmp(this->m_CurrParsePtr, "Ave", 3) == 0) { param_func = PURSE_FUNC_AVE;      pos_plus = 3; param_func_name = "Ave"; }
									if (param_func != PURSE_FUNC_NONE)
									{
										// 関数名前方一致の為、更なる照合
										if (this->m_CurrParsePos + pos_plus >= this->m_FileSize)
										{
											// 関数照合失敗：ファイルの終端に達した
											purse_error = PURSE_ERROR_BROKEN_SYNTAX_FOR_FUNC;
											is_show_error = true;
											is_add_chara_to_work = true;
											pos_plus = this->m_FileSize - this->m_CurrParsePos;
										}
										else
										{
											const c8 c_func_name_next = *(this->m_CurrParsePtr + pos_plus);
											//const c8 c_func_name_next2 = '\0';
											if (this->m_CurrParsePos + pos_plus + 1 < this->m_FileSize)
											{
												//*(this->m_CurrParsePtr + pos_plus + 1) = 0;
											}
											if (c_func_name_next == ' ' || c_func_name_next == '\t' || c_func_name_next == '\r' || c_func_name_next == '\n' || c_func_name_next == '(')//|| (c_func_name_next == '/' && (c_func_name_next2 == '/' || c_func_name_next2 == '*')))
											{
												// 関数照合成功
												param_func_param_index_begin = param_count;
												param_func_text_buff_pos_begin = param_text_buff_pos;
												param_func_has_bracket = false;
												param_func_is_call = false;
											}
											else
											{
												// 関数照合失敗：関数名の全体一致に失敗
												purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_OR_FUNC_ON_PARAM;
												is_show_error = true;
												is_add_chara_to_work = true;
											}
										}
									}
									else
									{
										// 関数照合失敗：関数名の前方一致に失敗
										purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_OR_FUNC_ON_PARAM;
										is_show_error = true;
										is_add_chara_to_work = true;
									}
								}
								else
								{
									purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_PARAM;
									is_show_error = true;
									is_add_chara_to_work = true;
								}
							}
						}
						else
						{
							purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_PARAM;
							is_show_error = true;
							is_add_chara_to_work = true;
						}
					}
				}
			}
			break;
			}
		}
		break;

		//------------------------------
		// 文字列パラメータ取得フェーズ
		case PURSE_PHASE_PARAM_TEXT:
		{
			switch (c)
			{
				// クォーテーション
				// ・シングルクォーテーション文字列取得フェーズ ... パラメータ取得フェーズへ
				// ・その他                                     ... パラメータ文字列に追加
			case '\'':
			case '\"':
			{
				if (c == quotation)
				{
					quotation = '\0';
					if (purse_error == PURSE_ERROR_NONE) {
						purse_phase = PURSE_PHASE_PARAM;
					}
					else {
						purse_phase = PURSE_PHASE_ERROR;
					}
				}
				else
				{
					c_for_work = c;
					is_add_chara_to_work = true;
				}
			}
			break;
			// エスケープシーケンス
			// ・"\r\n" ... '\n' をパラメータ文字列に追加
			// ・"\r"   ... '\n' をパラメータ文字列に追加
			// ・"\n"   ... '\n' をパラメータ文字列に追加
			// ・"\t"   ... '\t' をパラメータ文字列に追加
			// ・その他 ... '\'  をパラメータ文字列に追加
			case '\\':
			{
				if (c_next == 'r')
				{
					const c8 c_next2 = this->m_CurrParsePos + 2 < this->m_FileSize ? *(this->m_CurrParsePtr + 2) : '\0';
					const c8 c_next3 = this->m_CurrParsePos + 3 < this->m_FileSize ? *(this->m_CurrParsePtr + 3) : '\0';
					if (c_next2 == '\\' && c_next3 == 'n')
					{
						pos_plus += 3;
					}
					else
					{
						++pos_plus;
					}
					c_for_work = '\n';
					is_add_chara_to_work = true;
				}
				else if (c_next == 'n')
				{
					++pos_plus;
					c_for_work = '\n';
					is_add_chara_to_work = true;
				}
				else if (c_next == 't')
				{
					++pos_plus;
					c_for_work = '\t';
					is_add_chara_to_work = true;
				}
				else if (c_next == '\\' || c_next == '\'' || c_next == '\"')
				{
					++pos_plus;
					c_for_work = c_next;
					is_add_chara_to_work = true;
				}
				else
				{
					c_for_work = c;
					is_add_chara_to_work = true;
				}
			}
			break;
			// 改行
			// ・'\n' をパラメータ文字列に追加
			case '\r':
			{
				if (c_next == '\n') {
					++pos_plus;
				}
			}
			//break;
			case '\n':
			{
				is_cr = true;
				c_for_work = '\n';
				is_add_chara_to_work = true;
			}
			break;
			// その他の英数字・漢字
			// ・パラメータ文字列に追加
			default:
			{
				is_normal_chara = true;
				is_add_chara_to_work = true;
			}
			break;
			}
		}
		break;

		//------------------------------
		// コメントスキップフェーズ
		case PURSE_PHASE_COMMENT_C:
		case PURSE_PHASE_COMMENT_CPP:
		case PURSE_PHASE_COMMENT_SHARP:
		{
			switch (c)
			{
				// コメント終了検出①
				// ・「*/」 ... /*から始まっている場合はタグ解析フェーズかパラメータ解析フェーズへ
				// ・その他 ... スキップ
			case '*':
			{
				if (purse_phase == PURSE_PHASE_COMMENT_C)
				{
					if (c_next == '/')
					{
						++pos_plus;
						if (purse_error == PURSE_ERROR_NONE)
						{
							if (has_tag)
							{
								purse_phase = PURSE_PHASE_PARAM;
								if (param_type != SI_TYPE_UNKNOWN) {
									param_with_space = true;
								}
							}
							else {
								purse_phase = PURSE_PHASE_TAG;
							}
						}
						else
						{
							purse_phase = PURSE_PHASE_ERROR;
						}
					}
					else
					{
						is_normal_chara = true;
					}
				}
				else
				{
					is_normal_chara = true;
				}
			}
			break;
			// コメント終了検出②：改行
			// ・// か # から始まっている場合はタグ解析フェーズかパラメータ解析フェーズへ
			// ・その他 ... スキップ
			case '\r':
			{
				if (c_next == '\n') {
					++pos_plus;
				}
			}
			//break;
			case '\n':
			{
				is_cr = true;
				if (purse_phase == PURSE_PHASE_COMMENT_CPP ||
					purse_phase == PURSE_PHASE_COMMENT_SHARP)
				{
					if (purse_error == PURSE_ERROR_NONE)
					{
						if (has_tag)
						{
							purse_phase = PURSE_PHASE_PARAM;
							if (param_type != SI_TYPE_UNKNOWN) {
								param_with_space = true;
							}
						}
						else {
							purse_phase = PURSE_PHASE_TAG;
						}
					}
					else
					{
						purse_phase = PURSE_PHASE_ERROR;
					}
				}
			}
			break;
			// その他
			// ・スキップ
			default:
			{
				is_normal_chara = true;
			}
			break;
			}
		}
		break;

		//------------------------------
		// エラースキップフェーズ
		case PURSE_PHASE_ERROR:
		{
			switch (c)
			{
				// セミコロン
				// ・レコード検出処理終了
			case ';':
			{
				is_finish = true;
			}
			break;
			// コメント開始検出①
			// ・「//」 ... コメントスキップフェーズへ
			// ・「/*」 ... コメントスキップフェーズへ
			// ・その他 ... スキップ
			case '/':
			{
				if (c_next == '/')
				{
					++pos_plus;
					purse_phase = PURSE_PHASE_COMMENT_CPP;
				}
				else if (c_next == '*')
				{
					++pos_plus;
					purse_phase = PURSE_PHASE_COMMENT_C;
				}
			}
			break;
			// コメント開始検出②
			// ・行頭   ... コメントスキップフェーズへ
			case '#':
			{
				if (is_top_of_line)
				{
					purse_phase = PURSE_PHASE_COMMENT_SHARP;
				}
			}
			break;
			// クォーテーション
			// ・文字列取得フェーズへ
			case '\'':
			{
				purse_phase = PURSE_PHASE_PARAM_TEXT;
				quotation = c;
			}
			break;
			// 改行
			// ・スキップ
			case '\r':
			{
				if (c_next == '\n') {
					++pos_plus;
				}
			}
			//break;
			case '\n':
			{
				is_cr = true;
			}
			break;
			// その他
			// ・スキップ
			default:
			{
				is_normal_chara = true;
			}
			break;
			}
		}
		break;
		}

		// 解析バッファを進めるキャラ数を取得
		u32 add_work_len = 1;
		if (is_normal_chara)
		{
#ifdef SI_USE_TEXT_LIB
			if (this->m_ScriptFileType == SI_FILE_TYPE_SJIS)
			{
				pos_plus = CTextLib::GetBytesOfSJISChar(c);
			}
			else if (this->m_ScriptFileType == SI_FILE_TYPE_UTF8)
			{
				pos_plus = CTextLib::GetBytesOfUTF8Char(c);
			}
#else//SI_USE_TEXT_LIB
			if (this->m_ScriptFileType == SI_FILE_TYPE_SJIS)
			{
				const u32 u_c = (c) & 0xff;
				pos_plus = ((u_c >= 0x81 && u_c <= 0x9f) || (u_c >= 0xe0 && u_c <= 0xff) ? 2 : 1);
			}
			else if (this->m_ScriptFileType == SI_FILE_TYPE_UTF8)
			{
				pos_plus = ((c & 0xf8) == 0xf0 ? 4 : (c & 0xf0) == 0xe0 ? 3 : (c & 0xe0) == 0xc0 ? 2 : 1);
			}
#endif//SI_USE_TEXT_LIB
			add_work_len = pos_plus;
		}

		// ワークに文字列を追加
		if (is_add_chara_to_work)
		{
			if (c_for_work != '\0')
			{
				if (param_work_buff_pos == param_work_buff_size - 1)
				{
					if (purse_phase == PURSE_PHASE_TAG) {
						purse_error = PURSE_ERROR_WORK_BUFFER_OVERFLOW_ON_TAG;
					}
					else {
						purse_error = PURSE_ERROR_WORK_BUFFER_OVERFLOW_ON_PARAM;
					}
					is_show_error = true;
				}
				else
				{
					*(param_work_buff_p++) = c_for_work;
					++param_work_buff_pos;
				}
			}
			else
			{
				if (is_show_error && (purse_error == PURSE_ERROR_ILLEGAL_CHARACTER_ON_TAG))
				{
					const c8* c_tmp_p = this->m_CurrParsePtr + pos_plus;
					while (this->m_CurrParsePos + pos_plus < this->m_FileSize)
					{
						const c8 c_tmp = *(c_tmp_p++);
						const c8 c_tmp2 = *(c_tmp_p);
						if (c_tmp == ' ' || c_tmp == '\t' || c_tmp == '\r' || c_tmp == '\n' || c_tmp == ';' || (c_tmp == '/' && (c_tmp2 == '/' || c_tmp2 == '*'))) {
							break;
						}
						++pos_plus;
					}
					add_work_len = pos_plus;
				}
				else if (is_show_error && (purse_error == PURSE_ERROR_ILLEGAL_CHARACTER_ON_PARAM || purse_error == PURSE_ERROR_ILLEGAL_CHARACTER_OR_FUNC_ON_PARAM))
				{
					const c8* c_tmp_p = this->m_CurrParsePtr + pos_plus;
					while (this->m_CurrParsePos + pos_plus < this->m_FileSize)
					{
						const c8 c_tmp = *(c_tmp_p++);
						const c8 c_tmp2 = *(c_tmp_p);
						if (c_tmp == ' ' || c_tmp == '\t' || c_tmp == '\r' || c_tmp == '\n' || c_tmp == ',' || c_tmp == ';' || c_tmp == '(' || (c_tmp == '/' && (c_tmp2 == '/' || c_tmp2 == '*'))) {
							break;
						}
						++pos_plus;
					}
					add_work_len = pos_plus;
				}
				const c8* c_for_work_p = this->m_CurrParsePtr;
				for (u32 add_pos = 0; add_pos < add_work_len; ++add_pos, ++c_for_work_p)
				{
					if (param_work_buff_pos == param_work_buff_size - 1)
					{
						if (purse_phase == PURSE_PHASE_TAG) {
							purse_error = PURSE_ERROR_WORK_BUFFER_OVERFLOW_ON_TAG;
						}
						else {
							purse_error = PURSE_ERROR_WORK_BUFFER_OVERFLOW_ON_PARAM;
						}
						is_show_error = true;
						break;
					}
					else
					{
						*(param_work_buff_p++) = *c_for_work_p;
						++param_work_buff_pos;
					}
				}
			}
		}

		if (is_pickup_tag || is_add_param || is_show_error)
		{
			*param_work_buff_p = '\0';
		}

		// タグ抽出
		if (is_pickup_tag)
		{
			const c8* tag_id = param_work_buff;
			tag_id_crc = SI_CRC_FUNC(tag_id);
			if (tag_id_crc != 0 && tag_id_crc != 0xffffffff)
			{
				this->m_RecordInfoForText->header.tag_id_crc = tag_id_crc;
				this->m_RecordInfoForText->header.param_count = 0;
				tag_param = this->SearchTagParam(tag_id_crc);
				allow_callback = false;
				if (callback)
				{
					if (tag_param)
					{
						//if(tag_param->func)
						{
							allow_callback = true;
						}
					}
					else
					{
						if (!this->m_IgnoreExceptionTag)
						{
							if (this->m_ExceptionCallback)
							{
								allow_callback = true;
							}
							else
							{
								SI_PRINT_WARNING("+++++ [warning!] CScriptInterpreter::GetNextRecordForText() タグ解析ワーニング：不明なタグが指定されています。\n");
								SI_PRINT_WARNING("                 （タグID=\"%s\", CRC=0x%08x, レコード番号=%d, 行=%d）\n", tag_id, SCast<u32>(tag_id_crc), this->m_CurrentRecordIndex, line_no_for_tag);
								SI_ASSERT_FAIL_SILENT();
							}
						}
					}
				}
			}
#ifdef SI_DEBUG_MODE
			tag_id_row_debug = this->m_CurrParseLineNo;
			//strcpy_s(tag_id_debug, sizeof(tag_id_debug), tag_id);
			size_t len = strlen(tag_id); if (len >= sizeof(tag_id_debug)) { len = sizeof(tag_id_debug) - 1; }
			memcpy(tag_id_debug, tag_id, len);
			tag_id_debug[len] = 0;
#endif//SI_DEBUG_MODE
			purse_phase = PURSE_PHASE_PARAM;
			param_with_space = false;
			param_type = SI_TYPE_UNKNOWN;
			param_work_buff_p = param_work_buff;
			param_work_buff_pos = 0;
			is_pickup_tag = false;
		}

		// パラメータ抽出
		const u32 param_index = param_count;
		if (is_add_param)
		{
			const c8* param = param_work_buff;
			if (param_index >= this->m_ParamCountMaxForText)
			{
				purse_error = PURSE_ERROR_PARAMTER_OVERFLOW;
				is_show_error = true;
			}
			else
			{
				SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE& value = this->m_ParamValueInfoForText[param_index];
				switch (param_type)
				{
				case SI_INT:
				{
					if (param_subtype == PURSE_PARAM_SUBTYPE_INT_HEX)
					{
						if (param[0] == '\0')
						{
							purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_PARAM;
							is_show_error = true;
							is_add_chara_to_work = true;
						}
						else
						{
							s32 val = 0;
							u32 param_len = 0;
							const c8* param_p = param;
							while (1)
							{
								const c8 param_c = *(param_p++);
								if (param_c == '\0') {
									break;
								}
								if (param_c >= '0' && param_c <= '9') {
									val = (val << 4) | (param_c - '0');
								}
								else if (param_c >= 'a' && param_c <= 'f') {
									val = (val << 4) | (param_c - 'a' + 10);
								}
								else if (param_c >= 'A' && param_c <= 'F') {
									val = (val << 4) | (param_c - 'A' + 10);
								}
								else
								{
									purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_PARAM;
									is_show_error = true;
									val = 0;
									break;
								}
								++param_len;
								if (param_len == 9)
								{
									purse_error = PURSE_ERROR_OVERFLOW_ON_PARAM;
									is_show_error = true;
									val = 0;
									break;
								}
							}
							value.i = val;
						}
					}
					else
					{
						s32 val = 0;
						b8 sign = false;
						c8 param_c_prev = '\0';
						const c8* param_p = param;
						if (*param_p == '-')
						{
							sign = true;
							param_c_prev = '-';
							param_p++;
						}
						while (1)
						{
							const c8 param_c = *(param_p++);
							if (param_c == '\0')
							{
								if (param_c_prev == '-')
								{
									purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_PARAM;
									is_show_error = true;
									val = 0;
									sign = false;
								}
								break;
							}
							if (param_c >= '0' && param_c <= '9')
							{
								s32 val_prev = val;
								val = val * 10 + (param_c - '0');
								if (val < val_prev)
								//if(val & 0x80000000)
								{
									purse_error = PURSE_ERROR_OVERFLOW_ON_PARAM;
									is_show_error = true;
									val = 0;
									sign = false;
									break;
								}
							}
							else
							{
								purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_PARAM;
								is_show_error = true;
								val = 0;
								sign = false;
								break;
							}
							param_c_prev = param_c;
						}
						if (sign) {
							val = -val;
						}
						value.i = val;
					}
				}
				break;
				case SI_FLOAT:
				{
					s32 val_dec = 0;
					u32 val_fra = 0;
					u32 val_fra_size = 1;
					b8 val_is_fra = false;
					b8 sign = false;
					c8 param_c_prev = '\0';
					const c8* param_p = param;
					if (*param_p == '-')
					{
						sign = true;
						param_c_prev = '-';
						param_p++;
					}
					while (1)
					{
						const c8 param_c = *(param_p++);
						if (param_c == '\0')
						{
							if (param_c_prev == '-' || param_c_prev == '.')
							{
								purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_PARAM;
								is_show_error = true;
								val_dec = 0;
								val_fra = 0;
								sign = false;
							}
							break;
						}
						if (param_c >= '0' && param_c <= '9')
						{
							if (val_is_fra)
							{
								u32 val_fra_prev = val_fra;
								val_fra = val_fra * 10 + (param_c - '0');
								if (val_fra < val_fra_prev)
								//if(val_fra & 0x80000000)
								{
									purse_error = PURSE_ERROR_OVERFLOW_ON_PARAM;
									is_show_error = true;
									val_dec = 0;
									val_fra = 0;
									sign = false;
									break;
								}
								val_fra_size *= 10;
							}
							else
							{
								s32 val_dec_prev = val_dec;
								val_dec = val_dec * 10 + (param_c - '0');
								if (val_dec < val_dec_prev)
								//if(val_dec & 0x80000000)
								{
									purse_error = PURSE_ERROR_OVERFLOW_ON_PARAM;
									is_show_error = true;
									val_dec = 0;
									val_fra = 0;
									sign = false;
									break;
								}
							}
						}
						else if (param_c == '.')
						{
							val_is_fra = true;
						}
						else
						{
							purse_error = PURSE_ERROR_ILLEGAL_CHARACTER_ON_PARAM;
							is_show_error = true;
							val_dec = 0;
							val_fra = 0;
							sign = false;
							break;
						}
						param_c_prev = param_c;
					}
					if (val_fra > 0 && val_fra_size > 0) {
						value.f = SCast<f32>(val_dec) + SCast<f32>(val_fra) / SCast<f32>(val_fra_size);
					}
					else {
						value.f = SCast<f32>(val_dec);
					}
					if (sign) {
						value.f = -value.f;
					}
				}
				break;
				case SI_STRING_DIRECT:
				{
					const size_t str_len = strlen(param);
					const size_t str_size = str_len + 1;
					if (param_text_buff_pos + str_size <= param_text_buff_size)
					{
						//value.str_direct = NULL;
#ifdef SI_USE_TEXT_BUFF
						if (param_func == PURSE_FUNC_NONE && this->m_TextBuffForAutoCopy)
						{
							u32 text_size = 0;
							u32 added_text_size = 0;
							value.str_direct = this->m_TextBuffForAutoCopy->AddText(param, this->m_IsReuseAutoCopyText, &text_size, &added_text_size);
							if (text_size != added_text_size)
							{
								purse_error = PURSE_ERROR_EXT_TEXT_BUFFER_OVERFLOW_ON_PARAM;
								is_show_error = true;
							}
						}
						else
#endif//SI_USE_TEXT_BUFF
						{
							value.str_offset = -1;
						}
						if (param_func == PURSE_FUNC_NONE && this->m_PlainTextBuffForAutoCopy && this->m_PlainTextBuffSizeForAutoCopy > 0)
						{
							const u32 remain_size = this->m_PlainTextBuffSizeForAutoCopy - this->m_PlainTextBuffUsedSizeForAutoCopy;
							if (remain_size < str_size)
							{
								purse_error = PURSE_ERROR_EXT_TEXT_BUFFER_OVERFLOW_ON_PARAM;
								is_show_error = true;
							}
							else
							{
								c8* str_buff = this->m_PlainTextBuffForAutoCopy + this->m_PlainTextBuffUsedSizeForAutoCopy;
								memcpy(str_buff, param, str_size);
								value.str_offset = this->m_PlainTextBuffUsedSizeForAutoCopy;
								this->m_PlainTextBuffUsedSizeForAutoCopy += static_cast<u32>(str_size);
							}
						}
						if (value.str_offset < 0)
						{
							value.str_offset = param_text_buff_pos;
							memcpy(param_text_buff_p, param, str_size);
							param_text_buff_p += str_size;
							param_text_buff_pos += static_cast<u32>(str_size);
						}
					}
					else
					{
						param_type = SI_INT;
						value.i = 0;
						purse_error = PURSE_ERROR_TEXT_BUFFER_OVERFLOW_ON_PARAM;
						is_show_error = true;
					}
				}
				break;
				default:
					value.i = 0;
					break;
				}
				{
					const u32 index = param_index / SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
					const u32 index_on_byte = param_index % SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
					const u32 shift_bits = index_on_byte * SI_RECORD_PARAM_TYPE_BITS;
					const u8 type_bits = SI_RECORD_PARAM_TYPE_BITS_MASK << shift_bits;
					const u8 type_mask = ~type_bits;
					u8& type = this->m_ParamTypeInfoForText[index];
					type = (type & type_mask) | ((param_type << shift_bits) & type_bits);
				}
				++param_count;
			}
			purse_phase = PURSE_PHASE_PARAM;
			param_with_space = false;
			param_type = SI_TYPE_UNKNOWN;
			param_work_buff_p = param_work_buff;
			param_work_buff_pos = 0;
			is_add_param = false;
		}

		// 関数呼び出し
		if (param_func_is_call)
		{
			const u32 param_func_param_count = param_count - param_func_param_index_begin;
			param_count = param_func_param_index_begin;
			param_text_buff_pos = param_func_text_buff_pos_begin;
			if (purse_error == PURSE_ERROR_NONE)
			{
				switch (param_func)
				{
				case PURSE_FUNC_CRC:
				{
					if (param_func_param_count >= 1)
					{
						const u32 param_func_param_index = param_func_param_index_begin;
						const u32 index = param_func_param_index / SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
						const u32 index_on_byte = param_func_param_index % SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
						const u32 shift_bits = index_on_byte * SI_RECORD_PARAM_TYPE_BITS;
						const u8 type_bits = SI_RECORD_PARAM_TYPE_BITS_MASK << shift_bits;
						const u8 type_mask = ~type_bits;
						u8& type = this->m_ParamTypeInfoForText[index];
						const u8 param_func_param_type = (type >> shift_bits)& SI_RECORD_PARAM_TYPE_BITS_MASK;
						if (param_func_param_type == SI_STRING_DIRECT)
						{
							SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE& value = this->m_ParamValueInfoForText[param_func_param_index_begin];
							value.ui = SI_CRC_FUNC(this->GetTextForText(value.str_offset));
							type = (type & type_mask) | ((SI_INT << shift_bits) & type_bits);
							++param_count;
							if (param_func_param_count > 1)
							{
								purse_error = PURSE_ERROR_OVERFLOW_PARAM_ON_FUNC;
								is_show_error = true;
							}
							param_type = SI_INT;
						}
						else
						{
							purse_error = PURSE_ERROR_ILLEGAL_PARAM_ON_FUNC;
							is_show_error = true;
						}
					}
					else
					{
						purse_error = PURSE_ERROR_NO_PARAM_ON_FUNC;
						is_show_error = true;
					}
				}
				break;
#if 0
				case PURSE_FUNC_CRC2:
				{
					if(param_func_param_count >= 1)
					{
						const u32 param_func_param_index = param_func_param_index_begin;
						const u32 index         = param_func_param_index / SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
						const u32 index_on_byte = param_func_param_index % SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
						const u32 shift_bits    = index_on_byte * SI_RECORD_PARAM_TYPE_BITS;
						const u8 type_bits      = SI_RECORD_PARAM_TYPE_BITS_MASK << shift_bits;
						const u8 type_mask      = ~type_bits;
						u8& type = this->m_ParamTypeInfoForText[index];
						const u8 param_func_param_type = (type >> shift_bits) & SI_RECORD_PARAM_TYPE_BITS_MASK;
						if(param_func_param_type == SI_STRING_DIRECT)
						{
							SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE& value = this->m_ParamValueInfoForText[param_func_param_index_begin];
							value.i = CEncode::CRC32(value.str_direct);
							type = (type & type_mask) | ((SI_INT << shift_bits) & type_bits);
							++ param_count;
							if(param_func_param_count > 1)
							{
								purse_error = PURSE_ERROR_OVERFLOW_PARAM_ON_FUNC;
								is_show_error = true;
							}
							param_type = SI_INT;
						}
						else
						{
							purse_error = PURSE_ERROR_ILLEGAL_PARAM_ON_FUNC;
							is_show_error = true;
						}
					}
					else
					{
						purse_error = PURSE_ERROR_NO_PARAM_ON_FUNC;
						is_show_error = true;
					}
				}
				break;
#endif // 0
				case PURSE_FUNC_TORADIAN:
				{
					if (param_func_param_count >= 1)
					{
						const u32 param_func_param_index = param_func_param_index_begin;
						const u32 index = param_func_param_index / SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
						const u32 index_on_byte = param_func_param_index % SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
						const u32 shift_bits = index_on_byte * SI_RECORD_PARAM_TYPE_BITS;
						const u8 type_bits = SI_RECORD_PARAM_TYPE_BITS_MASK << shift_bits;
						const u8 type_mask = ~type_bits;
						u8& type = this->m_ParamTypeInfoForText[index];
						const u8 param_func_param_type = (type >> shift_bits)& SI_RECORD_PARAM_TYPE_BITS_MASK;
						if (param_func_param_type == SI_INT || param_func_param_type == SI_FLOAT)
						{
							SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE& value = this->m_ParamValueInfoForText[param_func_param_index_begin];
							f32 value_f = 0.f;
							if (param_func_param_type == SI_INT) {
								value_f = SCast<f32>(value.i);
							}
							else {
								value_f = value.f;
							}
							value.f = meToRadian(value_f);
							type = (type & type_mask) | ((SI_FLOAT << shift_bits) & type_bits);
							++param_count;
							if (param_func_param_count > 1)
							{
								purse_error = PURSE_ERROR_OVERFLOW_PARAM_ON_FUNC;
								is_show_error = true;
							}
							param_type = SI_FLOAT;
						}
						else
						{
							purse_error = PURSE_ERROR_ILLEGAL_PARAM_ON_FUNC;
							is_show_error = true;
						}
					}
					else
					{
						purse_error = PURSE_ERROR_NO_PARAM_ON_FUNC;
						is_show_error = true;
					}
				}
				break;
				case PURSE_FUNC_TODEGREE:
				{
					if (param_func_param_count >= 1)
					{
						const u32 param_func_param_index = param_func_param_index_begin;
						const u32 index = param_func_param_index / SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
						const u32 index_on_byte = param_func_param_index % SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
						const u32 shift_bits = index_on_byte * SI_RECORD_PARAM_TYPE_BITS;
						const u8 type_bits = SI_RECORD_PARAM_TYPE_BITS_MASK << shift_bits;
						const u8 type_mask = ~type_bits;
						u8& type = this->m_ParamTypeInfoForText[index];
						const u8 param_func_param_type = (type >> shift_bits)& SI_RECORD_PARAM_TYPE_BITS_MASK;
						if (param_func_param_type == SI_INT || param_func_param_type == SI_FLOAT)
						{
							SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE& value = this->m_ParamValueInfoForText[param_func_param_index_begin];
							f32 value_f = 0.f;
							if (param_func_param_type == SI_INT) {
								value_f = SCast<f32>(value.i);
							}
							else {
								value_f = value.f;
							}
							value.f = meToDegree(value_f);
							type = (type & type_mask) | ((SI_FLOAT << shift_bits) & type_bits);
							++param_count;
							if (param_func_param_count > 1)
							{
								purse_error = PURSE_ERROR_OVERFLOW_PARAM_ON_FUNC;
								is_show_error = true;
							}
							param_type = SI_FLOAT;
						}
						else
						{
							purse_error = PURSE_ERROR_ILLEGAL_PARAM_ON_FUNC;
							is_show_error = true;
						}
					}
					else
					{
						purse_error = PURSE_ERROR_NO_PARAM_ON_FUNC;
						is_show_error = true;
					}
				}
				break;
				case PURSE_FUNC_TOINT:
				{
					if (param_func_param_count >= 1)
					{
						const u32 param_func_param_index = param_func_param_index_begin;
						const u32 index = param_func_param_index / SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
						const u32 index_on_byte = param_func_param_index % SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
						const u32 shift_bits = index_on_byte * SI_RECORD_PARAM_TYPE_BITS;
						const u8 type_bits = SI_RECORD_PARAM_TYPE_BITS_MASK << shift_bits;
						const u8 type_mask = ~type_bits;
						u8& type = this->m_ParamTypeInfoForText[index];
						const u8 param_func_param_type = (type >> shift_bits)& SI_RECORD_PARAM_TYPE_BITS_MASK;
						if (param_func_param_type == SI_INT || param_func_param_type == SI_FLOAT)
						{
							SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE& value = this->m_ParamValueInfoForText[param_func_param_index_begin];
							if (param_func_param_type == SI_FLOAT) {
								value.i = SCast<s32>(value.f);
							}
							type = (type & type_mask) | ((SI_INT << shift_bits) & type_bits);
							++param_count;
							if (param_func_param_count > 1)
							{
								purse_error = PURSE_ERROR_OVERFLOW_PARAM_ON_FUNC;
								is_show_error = true;
							}
							param_type = SI_INT;
						}
						else
						{
							purse_error = PURSE_ERROR_ILLEGAL_PARAM_ON_FUNC;
							is_show_error = true;
						}
					}
					else
					{
						purse_error = PURSE_ERROR_NO_PARAM_ON_FUNC;
						is_show_error = true;
					}
				}
				break;
				case PURSE_FUNC_TOFLOAT:
				{
					if (param_func_param_count >= 1)
					{
						const u32 param_func_param_index = param_func_param_index_begin;
						const u32 index = param_func_param_index / SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
						const u32 index_on_byte = param_func_param_index % SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
						const u32 shift_bits = index_on_byte * SI_RECORD_PARAM_TYPE_BITS;
						const u8 type_bits = SI_RECORD_PARAM_TYPE_BITS_MASK << shift_bits;
						const u8 type_mask = ~type_bits;
						u8& type = this->m_ParamTypeInfoForText[index];
						const u8 param_func_param_type = (type >> shift_bits)& SI_RECORD_PARAM_TYPE_BITS_MASK;
						if (param_func_param_type == SI_INT || param_func_param_type == SI_FLOAT)
						{
							SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE& value = this->m_ParamValueInfoForText[param_func_param_index_begin];
							if (param_func_param_type == SI_INT) {
								value.f = SCast<f32>(value.i);
							}
							type = (type & type_mask) | ((SI_FLOAT << shift_bits) & type_bits);
							++param_count;
							if (param_func_param_count > 1)
							{
								purse_error = PURSE_ERROR_OVERFLOW_PARAM_ON_FUNC;
								is_show_error = true;
							}
						}
						else
						{
							purse_error = PURSE_ERROR_ILLEGAL_PARAM_ON_FUNC;
							is_show_error = true;
						}
						param_type = SI_FLOAT;
					}
					else
					{
						purse_error = PURSE_ERROR_NO_PARAM_ON_FUNC;
						is_show_error = true;
					}
				}
				break;
				case PURSE_FUNC_SQR:
				{
					if (param_func_param_count >= 1)
					{
						const u32 param_func_param_index = param_func_param_index_begin;
						const u32 index = param_func_param_index / SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
						const u32 index_on_byte = param_func_param_index % SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
						const u32 shift_bits = index_on_byte * SI_RECORD_PARAM_TYPE_BITS;
						const u8 type_bits = SI_RECORD_PARAM_TYPE_BITS_MASK << shift_bits;
						const u8 type_mask = ~type_bits;
						u8& type = this->m_ParamTypeInfoForText[index];
						const u8 param_func_param_type = (type >> shift_bits)& SI_RECORD_PARAM_TYPE_BITS_MASK;
						if (param_func_param_type == SI_INT || param_func_param_type == SI_FLOAT)
						{
							SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE& value = this->m_ParamValueInfoForText[param_func_param_index_begin];
							f32 value_f = 0.f;
							if (param_func_param_type == SI_INT) {
								value_f = SCast<f32>(value.i);
							}
							else {
								value_f = value.f;
							}
							value.f = sqrtf(value_f);
							type = (type & type_mask) | ((SI_FLOAT << shift_bits) & type_bits);
							++param_count;
							if (param_func_param_count > 1)
							{
								purse_error = PURSE_ERROR_OVERFLOW_PARAM_ON_FUNC;
								is_show_error = true;
							}
						}
						else
						{
							purse_error = PURSE_ERROR_ILLEGAL_PARAM_ON_FUNC;
							is_show_error = true;
						}
						param_type = SI_FLOAT;
					}
					else
					{
						purse_error = PURSE_ERROR_NO_PARAM_ON_FUNC;
						is_show_error = true;
					}
				}
				break;
				case PURSE_FUNC_SUM:
				{
					if (param_func_param_count >= 1)
					{
						s32 param_func_param_count_rest = SCast<s32>(param_func_param_count);
						u32 param_func_param_index = param_func_param_index_begin;
						SI_STACK_TYPE param_func_result_type = SI_INT;
						SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE param_func_result;
						param_func_result.i = 0;
						while (param_func_param_count_rest > 0)
						{
							const u32 index = param_func_param_index / SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
							const u32 index_on_byte = param_func_param_index % SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
							const u32 shift_bits = index_on_byte * SI_RECORD_PARAM_TYPE_BITS;
							//const u8 type_bits      = SI_RECORD_PARAM_TYPE_BITS_MASK << shift_bits;
							//const u8 type_mask      = ~type_bits;
							u8& type = this->m_ParamTypeInfoForText[index];
							const u8 param_func_param_type = (type >> shift_bits)& SI_RECORD_PARAM_TYPE_BITS_MASK;
							SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE& value = this->m_ParamValueInfoForText[param_func_param_index];
							if (param_func_param_type == SI_INT)
							{
								if (param_func_result_type == SI_INT) {
									param_func_result.i += value.i;
								}
								else {
									param_func_result.f += SCast<f32>(value.i);
								}
							}
							else if (param_func_param_type == SI_FLOAT)
							{
								if (param_func_result_type == SI_INT) {
									param_func_result.f = SCast<f32>(param_func_result.i) + value.f;
									param_func_result_type = SI_FLOAT;
								}
								else {
									param_func_result.f += value.f;
								}
							}
							else
							{
								purse_error = PURSE_ERROR_ILLEGAL_PARAM_ON_FUNC;
								is_show_error = true;
								break;
							}
							++param_func_param_index;
							--param_func_param_count_rest;
						}
						if (!is_show_error)
						{
							const u32 index = param_func_param_index_begin / SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
							const u32 index_on_byte = param_func_param_index_begin % SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
							const u32 shift_bits = index_on_byte * SI_RECORD_PARAM_TYPE_BITS;
							const u8 type_bits = SI_RECORD_PARAM_TYPE_BITS_MASK << shift_bits;
							const u8 type_mask = ~type_bits;
							u8& type = this->m_ParamTypeInfoForText[index];
							SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE& value = this->m_ParamValueInfoForText[param_func_param_index_begin];
							value = param_func_result;
							type = (type & type_mask) | ((param_func_result_type << shift_bits) & type_bits);
							++param_count;
							param_type = param_func_result_type;
						}
					}
					else
					{
						purse_error = PURSE_ERROR_NO_PARAM_ON_FUNC;
						is_show_error = true;
					}
				}
				break;
				case PURSE_FUNC_AVE:
				{
					if (param_func_param_count >= 1)
					{
						s32 param_func_param_count_rest = SCast<s32>(param_func_param_count);
						u32 param_func_param_index = param_func_param_index_begin;
						SI_STACK_TYPE param_func_result_type = SI_INT;
						SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE param_func_result;
						param_func_result.i = 0;
						while (param_func_param_count_rest > 0)
						{
							const u32 index = param_func_param_index / SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
							const u32 index_on_byte = param_func_param_index % SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
							const u32 shift_bits = index_on_byte * SI_RECORD_PARAM_TYPE_BITS;
							//const u8 type_bits      = SI_RECORD_PARAM_TYPE_BITS_MASK << shift_bits;
							//const u8 type_mask      = ~type_bits;
							u8& type = this->m_ParamTypeInfoForText[index];
							const u8 param_func_param_type = (type >> shift_bits)& SI_RECORD_PARAM_TYPE_BITS_MASK;
							SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE& value = this->m_ParamValueInfoForText[param_func_param_index];
							if (param_func_param_type == SI_INT)
							{
								if (param_func_result_type == SI_INT) {
									param_func_result.i += value.i;
								}
								else {
									param_func_result.f += SCast<f32>(value.i);
								}
							}
							else if (param_func_param_type == SI_FLOAT)
							{
								if (param_func_result_type == SI_INT) {
									param_func_result.f = SCast<f32>(param_func_result.i) + value.f;
									param_func_result_type = SI_FLOAT;
								}
								else {
									param_func_result.f += value.f;
								}
							}
							else
							{
								purse_error = PURSE_ERROR_ILLEGAL_PARAM_ON_FUNC;
								is_show_error = true;
								break;
							}
							++param_func_param_index;
							--param_func_param_count_rest;
						}
						if (!is_show_error)
						{
							const u32 index = param_func_param_index_begin / SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
							const u32 index_on_byte = param_func_param_index_begin % SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
							const u32 shift_bits = index_on_byte * SI_RECORD_PARAM_TYPE_BITS;
							const u8 type_bits = SI_RECORD_PARAM_TYPE_BITS_MASK << shift_bits;
							const u8 type_mask = ~type_bits;
							u8& type = this->m_ParamTypeInfoForText[index];
							SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE& value = this->m_ParamValueInfoForText[param_func_param_index_begin];
							f32 value_f = 0.f;
							if (param_func_result_type == SI_INT) {
								value_f = SCast<f32>(param_func_result.i);
							}
							else {
								value_f = param_func_result.f;
							}
							value.f = value_f / SCast<f32>(param_func_param_count);
							type = (type & type_mask) | ((SI_FLOAT << shift_bits) & type_bits);
							++param_count;
							param_type = SI_FLOAT;
						}
					}
					else
					{
						purse_error = PURSE_ERROR_NO_PARAM_ON_FUNC;
						is_show_error = true;
					}
				}
				break;
				default:
				{
					purse_error = PURSE_ERROR_NOT_IMPLEMENTED_ON_FUNC;
					is_show_error = true;
				}
				break;
				}
			}

			param_func = PURSE_FUNC_NONE;
			//param_func_param_index_begin = 0;
			//param_func_text_buff_pos_begin = 0;
			param_func_has_bracket = false;
			param_func_is_call = false;
		}

		//エラー表示
		if (is_show_error)
		{
			has_error = true;
#ifdef SI_DEBUG_MODE
			switch (purse_error)
			{
			case PURSE_ERROR_NO_TAG:
				SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 破損データ：タグがありません。\n");
				SI_PRINT_ERROR("               （行=%d, 位置=%d）\n", line_no, pos_on_line);
				break;
			case PURSE_ERROR_ILLEGAL_CHARACTER_ON_TAG:
				SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 破損データ：タグに不正な文字が含まれています。\n");
				SI_PRINT_ERROR("               （タグID=\"%s\", 行=%d, 位置=%d）\n", param_work_buff, line_no, pos_on_line);
				break;
			case PURSE_ERROR_WORK_BUFFER_OVERFLOW_ON_TAG:
				SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() タグが長すぎます。（ワークバッファサイズ=%d）\n", param_work_buff_size);
				SI_PRINT_ERROR("               （タグID=\"%s\", 行=%d, 位置=%d）\n", param_work_buff, line_no, pos_on_line);
				break;
			case PURSE_ERROR_ILLEGAL_CHARACTER_ON_PARAM:
				SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 破損データ：パラメータに不正な文字が含まれています。\n");
				SI_PRINT_ERROR("               （タグID=\"%s\", パラメータ番号=%d, パラメータ=\"%s\", 行=%d, 位置=%d）\n", tag_id_debug, param_index, param_work_buff, line_no, pos_on_line);
				break;
			case PURSE_ERROR_ILLEGAL_CHARACTER_OR_FUNC_ON_PARAM:
				SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 破損データ：パラメータに不正な文字が含まれているか、未対応の関数が指定されています。\n");
				SI_PRINT_ERROR("               （タグID=\"%s\", パラメータ番号=%d, パラメータ=\"%s\", 行=%d, 位置=%d）\n", tag_id_debug, param_index, param_work_buff, line_no, pos_on_line);
				break;
			case PURSE_ERROR_OVERFLOW_ON_PARAM:
				SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 破損データ：パラメータの値もしくは値の桁数が範囲を超えています。\n");
				SI_PRINT_ERROR("               （タグID=\"%s\", パラメータ番号=%d, パラメータ=\"%s\", 行=%d, 位置=%d）\n", tag_id_debug, param_index, param_work_buff, line_no, pos_on_line);
				break;
			case PURSE_ERROR_EMPTY_ON_PARAM:
				SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 破損データ：パラメータが空欄の箇所があります。\n");
				SI_PRINT_ERROR("               （タグID=\"%s\", 行=%d, 位置=%d）\n", tag_id_debug, line_no, pos_on_line);
				break;
			case PURSE_ERROR_BROKEN_SYNTAX_FOR_FUNC:
				SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 破損データ：関数の呼び出し方が正しくありません。\n");
				SI_PRINT_ERROR("               （タグID=\"%s\", パラメータ番号=%d, 関数=\"%s\", 行=%d, 位置=%d）\n", tag_id_debug, param_func_param_index_begin, param_func_name, line_no, pos_on_line);
				break;
			case PURSE_ERROR_ILLEGAL_PARAM_ON_FUNC:
				SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 破損データ：関数のパラメータが正しい型ではありません。\n");
				SI_PRINT_ERROR("               （タグID=\"%s\", パラメータ番号=%d, 関数=\"%s\", 行=%d, 位置=%d）\n", tag_id_debug, param_func_param_index_begin, param_func_name, line_no, pos_on_line);
				break;
			case PURSE_ERROR_OVERFLOW_PARAM_ON_FUNC:
				SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 破損データ：関数のパラメータが多すぎます。\n");
				SI_PRINT_ERROR("               （タグID=\"%s\", パラメータ番号=%d, 関数=\"%s\", 行=%d, 位置=%d）\n", tag_id_debug, param_func_param_index_begin, param_func_name, line_no, pos_on_line);
				break;
			case PURSE_ERROR_EMPTY_ON_FUNC_PARAM:
				SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 破損データ：関数のパラメータが空欄の箇所があります。\n");
				SI_PRINT_ERROR("               （タグID=\"%s\", パラメータ番号=%d, 関数=\"%s\", 行=%d, 位置=%d）\n", tag_id_debug, param_func_param_index_begin, param_func_name, line_no, pos_on_line);
				break;
			case PURSE_ERROR_NO_PARAM_ON_FUNC:
				SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 破損データ：関数のパラメータがありません。\n");
				SI_PRINT_ERROR("               （タグID=\"%s\", パラメータ番号=%d, 関数=\"%s\", 行=%d, 位置=%d）\n", tag_id_debug, param_func_param_index_begin, param_func_name, line_no, pos_on_line);
				break;
			case PURSE_ERROR_NOT_IMPLEMENTED_ON_FUNC:
				SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 破損データ：未実装の関数が使用されています。\n");
				SI_PRINT_ERROR("               （タグID=\"%s\", パラメータ番号=%d, 関数=\"%s\", 行=%d, 位置=%d）\n", tag_id_debug, param_func_param_index_begin, param_func_name, line_no, pos_on_line);
				break;
			case PURSE_ERROR_WORK_BUFFER_OVERFLOW_ON_PARAM:
				SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 一つのパラメータが長すぎます。（ワークバッファサイズ=%d）\n", param_work_buff_size);
				SI_PRINT_ERROR("               （タグID=\"%s\", パラメータ番号=%d, パラメータ=\"%s\", 行=%d, 位置=%d）\n", tag_id_debug, param_index, param_work_buff, line_no, pos_on_line);
				break;
			case PURSE_ERROR_TEXT_BUFFER_OVERFLOW_ON_PARAM:
				if (!is_already_PURSE_ERROR_TEXT_BUFFER_OVERFLOW_ON_PARAM)
				{
					SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 一行に含まれる文字列パラメータの総量が長すぎます。（テキストバッファサイズ=%d）\n", param_text_buff_size);
					SI_PRINT_ERROR("               （タグID=\"%s\", パラメータ番号=%d, パラメータ=\"%s\", 行=%d, 位置=%d）\n", tag_id_debug, param_index, param_work_buff, line_no, pos_on_line);
					is_already_PURSE_ERROR_TEXT_BUFFER_OVERFLOW_ON_PARAM = true;
				}
				break;
			case PURSE_ERROR_EXT_TEXT_BUFFER_OVERFLOW_ON_PARAM:
				if (!is_already_PURSE_ERROR_EXT_TEXT_BUFFER_OVERFLOW_ON_PARAM)
				{
					SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 文字列パラメータの総量が長すぎます。もしくは、与えられたテキスト用バッファの空き容量が小さすぎます。\n");
					SI_PRINT_ERROR("               （タグID=\"%s\", パラメータ番号=%d, パラメータ=\"%s\", 行=%d, 位置=%d）\n", tag_id_debug, param_index, param_work_buff, line_no, pos_on_line);
					is_already_PURSE_ERROR_EXT_TEXT_BUFFER_OVERFLOW_ON_PARAM = true;
				}
				break;
			case PURSE_ERROR_PARAMTER_OVERFLOW:
				if (!is_already_PURSE_ERROR_PARAMTER_OVERFLOW)
				{
					SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() パラメータが多すぎます。（最大パラメータ数=%d）\n", this->m_ParamCountMaxForText);
					SI_PRINT_ERROR("               （タグID=\"%s\", パラメータ番号=%d, 行=%d, 位置=%d）\n", tag_id_debug, param_index, line_no, pos_on_line);
					is_already_PURSE_ERROR_PARAMTER_OVERFLOW = true;
				}
				break;
			default:
				SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 破損データ：不明なエラーです（エラー番号=%d）。\n", purse_error);
				SI_PRINT_ERROR("               （行=%d, 位置=%d）\n", line_no, pos_on_line);
				break;
			}
#endif//SI_DEBUG_MODE
			SI_ASSERT_FAIL_SILENT();
			purse_phase = PURSE_PHASE_ERROR;
			is_show_error = false;
		}

		// 解析位置を進める
		this->m_CurrParsePtr += pos_plus;
		this->m_CurrParsePos += pos_plus;
		if (is_cr)
		{
			++line_no;
			pos_on_line = 1;
			is_top_of_line = true;
		}
		else
		{
			pos_on_line += pos_plus;
		}
	}
#ifdef SI_DEBUG_MODE
	if (has_tag && !is_finish)
	{
		SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetScript() 破損データ：終端 ';' がありません。\n");
		SI_PRINT_ERROR("               （タグID=\"%s\", 行=%d, 位置=%d）\n", tag_id_debug, line_no, pos_on_line);
		SI_ASSERT_FAIL_SILENT();
	}
#endif//SI_DEBUG_MODE

	if (!has_error && this->m_RecordInfoForText->header.tag_id_crc == 0) {
		return NULL;
	}

	// コールバック
	this->m_RecordInfoForText->header.param_count = SCast<u8>(param_count);
	this->m_CurrentRecord = this->m_RecordInfoForText;
	if (tag_id_crc != 0 && allow_callback && purse_phase != PURSE_PHASE_ERROR)
	{
		// 型情報取得
		//const TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* param_type = this->m_ParamTypeInfoForText;
		const u8* param_type_txt = this->m_ParamTypeInfoForText;

		// 値情報取得
		const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_value = this->m_ParamValueInfoForText;

		// コールバック
		SI_STACK_CORE st_core(this, param_type_txt, param_value, param_count, this->m_ExtParam);
#ifdef SI_DEBUG_MODE
		st_core.SetDebugInfo(false, tag_id_debug, tag_id_crc, SCast<u16>(tag_id_row_debug), SCast<u16>(this->m_CurrentRecordIndex));
#endif//SI_DEBUG_MODE
		SI_STACK st(st_core);
		if (tag_param)
		{
			tag_param->func(st);
		}
		else
		{
			this->m_ExceptionCallback(tag_id_crc, st);
		}
	}

	++this->m_CurrentRecordIndex;

	return this->m_CurrentRecord;
}

///-------------------------------------------------------------------------------------------------
/// 指定したタグのレコードまでスキップ（テキスト形式）
/// ※for_next =	true  ... 次のタグ解析では指定のタグのコールバックが発生する
///					false ... 指定のタグまでスキップする為、次のタグ解析では指定のタグの次のタグのコールバックが発生する
b8	CScriptInterpreter::SkipForText(const hash32 tag_id_crc, const b8 for_next)
{
	if (this->m_IsBinData) {
		return false;
	}
	while (1)
	{
		const s32                       prev_record_index = this->m_CurrentRecordIndex;
		const SI_TAG_PARAM_BIN_RECORD* prev_record = this->m_CurrentRecord;
		const SI_TAG_PARAM_BIN_RECORD* record = this->GetNextRecordForText(false);
		if (!record) {
			break;
		}
		if (record->header.tag_id_crc == tag_id_crc)
		{
			if (for_next)
			{
				this->m_CurrentRecordIndex = prev_record_index;
				this->m_CurrentRecord = prev_record;
			}
			return true;
			//break;
		}
	}
	return false;
}

///-------------------------------------------------------------------------------------------------
/// 最後のレコードまでスキップ（テキスト形式）
/// ※次のタグ解析では最後のタグのコールバックが発生する
b8	CScriptInterpreter::SkipEndForText()
{
	if (this->m_IsBinData) {
		return false;
	}
	while (1)
	{
		const s32                       prev_record_index = this->m_CurrentRecordIndex;
		const SI_TAG_PARAM_BIN_RECORD* prev_record = this->m_CurrentRecord;
		const SI_TAG_PARAM_BIN_RECORD* record = this->GetNextRecordForText(false);
		if (!record)
		{
			this->m_CurrentRecordIndex = prev_record_index;
			this->m_CurrentRecord = prev_record;
			return true;
			//break;
		}
	}
	//return false;
}

#ifdef SI_USE_TEXT_BUFF
///-------------------------------------------------------------------------------------------------
/// 全文字列データを解析の際に自動的にメモリへ複写（テキスト形式時専用）
b8	CScriptInterpreter::SetTextBuffForAutoCopyForText(CTextBuff* text_buff, const b8 is_reuse_text)
{
	if (this->m_IsBinData) {
		return false;
	}
	this->m_TextBuffForAutoCopy = text_buff;
	this->m_IsReuseAutoCopyText = is_reuse_text;
	return true;
}
#endif//SI_USE_TEXT_BUFF

///-------------------------------------------------------------------------------------------------
/// テキストオフセット取得処理
const c8*	CScriptInterpreter::GetTextForText(const u32 offset) const
{
	if (this->m_IsBinData) {
		return NULL;
	}
	if (!this->m_FileBuff || this->m_FileSize <= 0) {
		return NULL;
	}

	if (SCast<u32>(offset) == 0xffffffff) {
		return "";
	}
	if (SCast<u32>(offset) & 0x80000000) {
		return NULL;
	}

	const c8* str = NULL;
	if (this->m_PlainTextBuffForAutoCopy)
	{
		str = this->m_PlainTextBuffForAutoCopy + offset;
	}
	else
	{
		c8* param_text_buff = (this->m_ParamExtTextBuffForText ? this->m_ParamExtTextBuffForText : this->m_ParamTextBuffForText);
		str = param_text_buff + offset;
	}

	return str;
}

#if 0
///-------------------------------------------------------------------------------------------------
/// 外部定義されたパラメータの値をセットする　※基本的に使用禁止
//void	CScriptInterpreter::SetParamType(       TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* param_types,                                                const s32 param_index, const SI_STACK_TYPE param_type)
void  	CScriptInterpreter::SetParamType(       u8*                                param_types,                                                const s32 param_index, const SI_STACK_TYPE param_type)
{
	const s32 index         = param_index / SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
	const s32 index_on_byte = param_index % SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
	const s32 shift_bits    = index_on_byte * SI_RECORD_PARAM_TYPE_BITS;
	const u8 type_bits      = SI_RECORD_PARAM_TYPE_BITS_MASK << shift_bits;
	const u8 type_mask      = ~type_bits;
	u8& type = param_types[index];
	type = (type & type_mask) | ((param_type << shift_bits) & type_bits);
}
//void	CScriptInterpreter::SetParamValueInt(   TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* param_types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_values, const s32 param_index, const s32  param_value)
void  	CScriptInterpreter::SetParamValueInt(   u8*                                param_types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_values, const s32 param_index, const s32  param_value)
{
	SetParamType(param_types, param_index, SI_INT);
	param_values[param_index].i = param_value;
}
//void	CScriptInterpreter::SetParamValueFloat( TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* param_types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_values, const s32 param_index, const f32 param_value)
void  	CScriptInterpreter::SetParamValueFloat( u8*                                param_types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_values, const s32 param_index, const f32 param_value)
{
	SetParamType(param_types, param_index, SI_FLOAT);
	param_values[param_index].f = param_value;
}
//void	CScriptInterpreter::SetParamValueString(TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* param_types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_values, const s32 param_index, const c8*   param_value)
void  	CScriptInterpreter::SetParamValueString(u8*                                param_types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_values, const s32 param_index, const c8*   param_value)
{
	SetParamType(param_types, param_index, SI_STRING_DIRECT);
	param_values[param_index].str_direct = param_value;
}
#endif // 0


///-------------------------------------------------------------------------------------------------
/// SI3関数：ダミー　※何もしない
s32	CScriptInterpreter::_si_DUMMY(SI_STACK st)
{
	return 1;
}

///-------------------------------------------------------------------------------------------------
/// タグ情報の検索・内部処理
const SI_TAG_PARAM*	CScriptInterpreter::SearchTagParamInner(hash32 tag_id_crc, u32 tag_count, const SI_TAG_PARAM* tag_param_top) const
{
	if (tag_count <= 0) {
		return NULL;
	}

	//バイナリサーチ準備
	s32 search_index_min = 0;
	s32 search_index_max = tag_count - 1;

	//バイナリサーチ
	{
		while (search_index_min <= search_index_max)
		{
			const s32 search_index_range = search_index_max - search_index_min + 1;
			const s32 search_index = search_index_min + (search_index_range / 2);
			const SI_TAG_PARAM* search_tag_param = tag_param_top + search_index;
			const hash32 search_tag_id_crc = search_tag_param->tag_id_crc;
			if (tag_id_crc == search_tag_id_crc)
			{
				return search_tag_param;
				//					break;
			}
			else if (tag_id_crc < search_tag_id_crc) {
				search_index_max = search_index - 1;
			}
			else {//if(tag_id_crc > search_tag_id_crc)
				search_index_min = search_index + 1;
			}
		}
	}

	return NULL;
}

///-------------------------------------------------------------------------------------------------
/// タグ情報の検索
const SI_TAG_PARAM*	CScriptInterpreter::SearchTagParam(hash32 tag_id_crc)
{
	if (m_TagParamCache->tag_id_crc == tag_id_crc) {
		// キャッシュされたタグ情報を返す
		return m_TagParamCache;
	}
	const SI_TAG_PARAM* tag_param = SearchTagParamInner(tag_id_crc, m_TagParamCount, m_TagParamTop);
	if (!tag_param) {
		// 見つからなければ、全体設定から探索
		tag_param = SearchTagParamInner(tag_id_crc, s_TagParamCountGlobal, s_TagParamTopGlobal);
	}
	if (tag_param) {
		// タグは連続する場合が多いので、発見したタグはキャッシュする
		m_TagParamCache = tag_param;
	}

	return tag_param;
}

///-------------------------------------------------------------------------------------------------
/// パラメータ（スタック）のカレントインデックスを操作

#if defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
///-------------------------------------------------------------------------------------------------
/// スタック操作エラーを表示
void	SI_STACK::PrintStackOpeError(const s32 ope) const
{
	SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetTag() スタック操作エラー：スタックはこれ以上操作できません。\n");
	SI_PRINT_ERROR("               （スタック数=%d, 現在のインデックス=%d, インデックス増減値=%d）\n", this->m_Core.m_Count, this->m_CurrIndex, ope);
	SI_PRINT_ERROR("               ");
	if (this->m_Core.m_DebugInfo.m_IsBinary)
	{
		if (this->m_Core.m_DebugInfo.m_TagID)
		{
			SI_PRINT_ERROR("（タグID=\"%s\", CRC=0x%08x, レコード番号=%d）\n", this->m_Core.m_DebugInfo.m_TagID, SCast<u32>(this->m_Core.m_DebugInfo.m_TagIDCRC), this->m_Core.m_DebugInfo.m_RecordIndex);
		}
		else
		{
			SI_PRINT_ERROR("（タグID(CRC)=0x%08x, レコード番号=%d）\n", SCast<u32>(this->m_Core.m_DebugInfo.m_TagIDCRC), this->m_Core.m_DebugInfo.m_RecordIndex);
		}
	}
	else
	{
		SI_PRINT_ERROR("（タグID=\"%s\", CRC=0x%08x, レコード番号=%d, 行=%d）\n", this->m_Core.m_DebugInfo.m_TagID, SCast<u32>(this->m_Core.m_DebugInfo.m_TagIDCRC), this->m_Core.m_DebugInfo.m_RecordIndex, this->m_Core.m_DebugInfo.m_Row);
	}
	SI_ASSERT_FAIL_SILENT();
}
//スタックアクセスエラーを表示
void	SI_STACK::PrintStackAccessError() const
{
	SI_PRINT_ERROR("***** [error!] CScriptInterpreter::SetTag() スタックアクセスエラー：範囲外のスタックにアクセスしようとしています。\n");
	SI_PRINT_ERROR("               （スタック数=%d, 現在のインデックス=%d）\n", this->m_Core.m_Count, this->m_CurrIndex);
	SI_PRINT_ERROR("               ");
	if (this->m_Core.m_DebugInfo.m_IsBinary)
	{
		if (this->m_Core.m_DebugInfo.m_TagID)
		{
			SI_PRINT_ERROR("（タグID=\"%s\", CRC=0x%08x, レコード番号=%d）\n", this->m_Core.m_DebugInfo.m_TagID, SCast<u32>(this->m_Core.m_DebugInfo.m_TagIDCRC), this->m_Core.m_DebugInfo.m_RecordIndex);
		}
		else
		{
			SI_PRINT_ERROR("（タグID(CRC)=0x%08x, レコード番号=%d）\n", SCast<u32>(this->m_Core.m_DebugInfo.m_TagIDCRC), this->m_Core.m_DebugInfo.m_RecordIndex);
		}
	}
	else
	{
		SI_PRINT_ERROR("（タグID=\"%s\", CRC=0x%08x, レコード番号=%d, 行=%d）\n", this->m_Core.m_DebugInfo.m_TagID, SCast<u32>(this->m_Core.m_DebugInfo.m_TagIDCRC), this->m_Core.m_DebugInfo.m_RecordIndex, this->m_Core.m_DebugInfo.m_Row);
	}
	SI_ASSERT_FAIL_SILENT();
}
#endif//defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)

///-------------------------------------------------------------------------------------------------
/// スタック操作

///-------------------------------------------------------------------------------------------------
/// スタックの基本情報をセット
//SI_STACK_CORE::SI_STACK_CORE(CScriptInterpreter* si, const TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* param_type_top, const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_value_top, const u32 count, void* ext_param) :
SI_STACK_CORE::SI_STACK_CORE(CScriptInterpreter* si, const u8* param_type_top, const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_value_top, const u32 count, void* ext_param) :
	m_SI(si),
	m_ParamTypeTop(param_type_top),
	m_ParamValueTop(param_value_top),
	m_Count(count),
	m_ExtParam(ext_param)
{
}
#if 0
//void	SI_STACK_CORE::MakeStack(CScriptInterpreter* si, const TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* param_type_top, const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_value_top, const u32 count, void* ext_param)
void	SI_STACK_CORE::MakeStack(CScriptInterpreter* si, const u8*                                param_type_top, const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_value_top, const u32 count, void* ext_param)
{
	this->m_SI = si;
	this->m_ParamTypeTop = param_type_top;
	this->m_ParamValueTop = param_value_top;
	this->m_Count = count;
	this->m_ExtParam = ext_param;
#ifdef SI_DEBUG_MODE
	this->m_DebugInfo.m_IsBinary = false;
	this->m_DebugInfo.m_TagID = NULL;
	this->m_DebugInfo.m_TagIDCRC = 0;
	this->m_DebugInfo.m_Row = 0;
#endif//SI_DEBUG_MODE
}
#endif // 0
#ifdef SI_DEBUG_MODE
///-------------------------------------------------------------------------------------------------
/// デバッグ情報をセット
void	SI_STACK_CORE::SetDebugInfo(const b8 is_binary, const c8* tag_id, const hash32 tag_id_crc, const u16 row, const u32 record_idex)
{
	this->m_DebugInfo.m_IsBinary = is_binary;
	this->m_DebugInfo.m_TagID = tag_id;
	this->m_DebugInfo.m_TagIDCRC = tag_id_crc;
	this->m_DebugInfo.m_Row = row;
	this->m_DebugInfo.m_RecordIndex = record_idex;
}
#endif//SI_DEBUG_MODE
#if 0
//スタック基本情報をセット　※外部からの情報を扱う場合に使用（基本的に使用禁止）
//void SI_STACK_CORE::MakeStackEx(CScriptInterpreter* si, const TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* param_type_top, const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_value_top, const u32 count, void* ext_param)
void SI_STACK_CORE::MakeStackEx(CScriptInterpreter* si, const u8*                                param_type_top, const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_value_top, const u32 count, void* ext_param)
{
	this->MakeStack(si, param_type_top, param_value_top, count, ext_param);
}
#endif // 0

///-------------------------------------------------------------------------------------------------
/// スタックに値種別をセット
//void	SI_STACK_MAKER_BASE::SetParamType(TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* types, const s32 param_index, const SI_STACK_TYPE param_type)
void	SI_STACK_MAKER_BASE::SetParamType(u8* types, const s32 param_index, const SI_STACK_TYPE param_type)
{
	const s32 index = param_index / SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
	const s32 index_on_byte = param_index % SI_RECORD_PARAM_TYPE_NUM_OF_BYTE;
	const s32 shift_bits = index_on_byte * SI_RECORD_PARAM_TYPE_BITS;
	const u8 type_bits = SI_RECORD_PARAM_TYPE_BITS_MASK << shift_bits;
	const u8 type_mask = ~type_bits;
	u8& type = types[index];
	type = (type & type_mask) | ((param_type << shift_bits) & type_bits);
}

///-------------------------------------------------------------------------------------------------
/// スタックに値をセット
//b8	SI_STACK_MAKER_BASE::SetParamValueBaseInt(TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* values, const u32 param_count, const s32 param_index, const s32 param_value)
b8	SI_STACK_MAKER_BASE::SetParamValueBaseInt(u8* types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* values, const u32 param_count, const s32 param_index, const s32 param_value)
{
	if (param_index >= 0 && param_index >= SCast<s32>(param_count)) {
		return false;
	}
	SetParamType(types, param_index, SI_INT);
	values[param_index].i = param_value;
	return true;
}
//b8	SI_STACK_MAKER_BASE::SetParamValueBaseFloat(TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* values, const u32 param_count, const s32 param_index, const f32 param_value)
b8	SI_STACK_MAKER_BASE::SetParamValueBaseFloat(u8* types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* values, const u32 param_count, const s32 param_index, const f32 param_value)
{
	if (param_index >= 0 && param_index >= SCast<s32>(param_count)) {
		return false;
	}
	SetParamType(types, param_index, SI_FLOAT);
	values[param_index].f = param_value;
	return true;
}
//b8	SI_STACK_MAKER_BASE::SetParamValueBaseString(TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* values, const u32 param_count, const s32 param_index, const c8* param_value)
b8	SI_STACK_MAKER_BASE::SetParamValueBaseString(u8* types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* values, const u32 param_count, const s32 param_index, const c8* param_value, c8* _text_buff, u32* _use_text_buff_size)
{
	if (param_index >= 0 && param_index >= SCast<s32>(param_count)) {
		return false;
	}
	if (_text_buff == NULL) {
		return false;
	}
	if (param_value == NULL) {
		return false;
	}
	SetParamType(types, param_index, SI_STRING_DIRECT);
	u32 text_len = SCast<u32>(strlen(param_value)) + 1;
	::memcpy(_text_buff + *_use_text_buff_size, param_value, text_len);
	values[param_index].str_offset = *_use_text_buff_size;
	*_use_text_buff_size += text_len;
	return true;
}


POISON_END


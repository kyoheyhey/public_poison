﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include <ctype.h>
#include "text/text_utility.h"


POISON_BGN

namespace
{
	static const u16 CONTROL_CHAR_MAXNUM = 7;
	static const c8 REPLACE_CHAR[CONTROL_CHAR_MAXNUM] = { 0x07, 0x08, 0x10, 0x11, 0x12, 0x14, 0x16 };
	static const c8 CONTROL_CHAR[CONTROL_CHAR_MAXNUM]   = { '&', '=', '[', '{', '<', '%', '\\' };
}

#define CALL_TEXT_TYPE_METHOD(__method,__type, ...) { \
if (__type == TEXT_TYPE::TEXT_TYPE_UTF8){ return __method<CStrUTF8>(__VA_ARGS__); } \
		else if (__type == TEXT_TYPE::TEXT_TYPE_SJIS){ return __method<CStrSJIS>(__VA_ARGS__); } \
		else { return __method<CStrASCII>(__VA_ARGS__); }}

// 文字コード変換
static u32 CodeUTF8toASCII(u32 _char, u32 _error);
static const c8* CodeUTF8toASCII(c8* _buff, const c8* _string, u32 _error);
static u32 CodeUTF8toSJIS(u32 _char, u32 _error);
static const c8* CodeUTF8toSJIS(c8* _buff, const c8* _string, u32 _error);
static u32 CodeUTF8toUTF8(u32 _char, u32 _error);
static const c8* CodeUTF8toUTF8(c8* _buff, const c8* _string, u32 _error);

static u32 CodeSJIStoASCII(u32 _char, u32 _error);
static const c8* CodeSJIStoASCII(c8* _buff, const c8* _string, u32 _error);
static u32 CodeSJIStoSJIS(u32 _char, u32 _error);
static const c8* CodeSJIStoSJIS(c8* _buff, const c8* _string, u32 _error);
static u32 CodeSJIStoUTF8(u32 _char, u32 _error);
static const c8* CodeSJIStoUTF8(c8* _buff, const c8* _string, u32 _error);

static u32 CodeASCIItoASCII(u32 _char, u32 _error);
static const c8* CodeASCIItoASCII(c8* _buff, const c8* _string, u32 _error);
static u32 CodeASCIItoSJIS(u32 _char, u32 _error);
static const c8* CodeASCIItoSJIS(c8* _buff, const c8* _string, u32 _error);
static u32 CodeASCIItoUTF8(u32 _char, u32 _error);
static const c8* CodeASCIItoUTF8(c8* _buff, const c8* _string, u32 _error);


///*************************************************************************************************
///	汎用文字列操作用ユーティリティクラス
///*************************************************************************************************

CTextUtility::CTextUtility()
{
}
CTextUtility::~CTextUtility()
{
}

// 変換処理がないため一旦削除
#if 0

// SJISの文字列をUTF8の文字列に変換する
size_t	CTextUtility::StringSJIStoUTF8(c8* utf8, const size_t utf8_size, const c8* sjis)
{
	size_t	sjis_size	= strlen(sjis)+1;
	size_t	utf8_size_	= utf8_size;

	SJISstoUTF8s(reinterpret_cast<const u8*>(sjis), &sjis_size, reinterpret_cast<u8*>(utf8), &utf8_size_);

	return utf8_size_;
}

#endif

//-------------------------------------------------------------------------------------------------
// Shift-JISの文字をUTF-8の文字に変換（１文字）
u32		CTextUtility::SJIStoUTF8(const u32 sjis_code)
{
	if(sjis_code == '\0')
	{
		return '\0';
	}
	u32		utf8_code = 0;

#if 0
	c8		utf8[5] = {0x00, 0x00, 0x00, 0x00, 0x00};
	size_t	utf8_len = 5;
	if(!(sjis_code & 0xff80))
	{
		return sjis_code;
	}
	else if(!(sjis_code & 0xff00))
	{
		const	c8	sjis[2]		= {sjis_code, 0x00};
		size_t		sjis_len	= 1;
		ConvSJIStoUTF8( sjis, &sjis_len, utf8, &utf8_len );
	}
	else
	{
		const	c8	sjis[3]		= {((sjis_code >> 8) & 0xff), (sjis_code & 0xff), 0x00};
		size_t		sjis_len	= 2;
		ConvSJIStoUTF8( sjis, &sjis_len, utf8, &utf8_len );
	}
	if(utf8_len == 1)
	{
		utf8_code = static_cast<u32>(utf8[0]);
	}
	else if(utf8_len == 2)
	{
		utf8_code =	(static_cast<u32>(utf8[0]) << 8) |
					(static_cast<u32>(utf8[1]) << 0);
	}
	else if(utf8_len == 3)
	{
		utf8_code =	(static_cast<u32>(utf8[0]) << 16) |
					(static_cast<u32>(utf8[1]) <<  8) |
					(static_cast<u32>(utf8[2]) <<  0);
	}
	else if(utf8_len == 4)
	{
		utf8_code =	(static_cast<u32>(utf8[0]) << 24) |
					(static_cast<u32>(utf8[1]) << 16) |
					(static_cast<u32>(utf8[2]) <<  8) |
					(static_cast<u32>(utf8[3]) <<  0);
	}
	else//if(utf8_len == 0)
	{
		utf8_code = '?';
	}
#endif
	utf8_code = ConvSJIStoUTF8(sjis_code);
	return utf8_code;
}

//-------------------------------------------------------------------------------------------------
// UTF-8の文字をShift-JISに変換（１文字）
u32		CTextUtility::UTF8toSJIS(const u32 utf8_code)
{
	return ConvUTF8toSJIS(utf8_code);
}


//*************************************************************************************************	
// SJIS処理
//*************************************************************************************************	

//-------------------------------------------------------------------------------------------------
// Shift-JISでの全角／半角文字判定　※１バイト文字～２バイト文字を、符号なし３２ビット長のコードに拡張したコードで判定
b8		CTextUtility::IsSJISCharHan(const u32 c) // 半角文字判定
{
	if(!(c & 0xff00))
		return false;
	return IsSJISChar1of1(static_cast<c8>(c & 0xff));
}

//-------------------------------------------------------------------------------------------------
// Shift-JISでの文字列処理
u32		CTextUtility::GetBytesOfSJISChar(const c8 c)
{
	if(IsSJISChar1of2(c))	return 2;
	return 1;
}
u32		CTextUtility::GetBytesOfSJISChar(const c8* text_p)
{
	const u32 bytes = GetBytesOfSJISChar(*(text_p+0));
	if(bytes > 1 && !*(text_p+1)) return 1;
	return bytes;
}
u32		CTextUtility::GetCharOfSJISChar(const c8* text_p, u32& bytes)
{
	const c8 c = *text_p;
	if(!c)
	{
		bytes = 0;
		return 0;
	}
	else if(c == '\n')
	{
		bytes = 1;
		return '\n';
	}
	else if(c == '\r')
	{
		bytes = 1;
		const c8 c2 = *(text_p + 1);
		if(c2 == '\n')
		{
			bytes = 2;
			return '\n';
		}
		return '\n';
	}
	else if(c == '\\')
	{
		const c8 c2 = *(text_p + 1);
		if(c2 == 'n')
		{
			bytes = 2;
			return '\n';
		}
#if 0
		else if(c2 == 'r')
		{
			const c8 c3 = *(text_p + 2);
			if(c3 == '\\')
			{
				const c8 c4 = *(text_p + 3);
				if(c4 == 'n')
				{
					bytes = 4;
					return '\n';
				}
			}
			bytes = 2;
			return '\n';
		}
		else if(c2 == 't')
		{
			bytes = 2;
			return '\t';
		}
#endif // 0
	}
	bytes  = GetBytesOfSJISChar(text_p);
	const u32 index = bytes - 1;
	if(index & 0xfffffffe)	return ILLEGAL_CHAR_CODE;
	
	typedef u32 (*func_p)(const c8* text_p);
	static func_p func_table[] =
	{
		GetCharOfSJISChar1,
		GetCharOfSJISChar2,
	};
	return func_table[index](text_p);
}
u32		CTextUtility::GetCharOfSJISCharNG(const c8* text_p)
{
	return ILLEGAL_CHAR_CODE;
}
u32		CTextUtility::GetCharOfSJISChar1(const c8* text_p)
{
	return ((static_cast<u32>(*(text_p+0)) <<  0) & 0x000000ff);
}
u32		CTextUtility::GetCharOfSJISChar2(const c8* text_p)
{
	return ((static_cast<u32>(*(text_p+0)) <<  8) & 0x0000ff00) |
		   ((static_cast<u32>(*(text_p+1)) <<  0) & 0x000000ff);
}

//-------------------------------------------------------------------------------------------------
// 改行コードを検索する（戻り値は発見した改行コードのアドレス）
const c8*	CTextUtility::SearchCRLFCodeSJIS(const c8* _text, u32* crlf_code_bytes)
{
	if(!_text) { return NULL; }
	const c8	*pt	= _text;
	while( *pt )
	{
		u32 bytes=0;
		const u32 code = CTextUtility::GetCharOfSJISChar(pt, bytes);
		// 改行
		if(code == '\n')
		{
			if(crlf_code_bytes)
				*crlf_code_bytes = bytes;
			return pt;
		}
		else if(code == '\r')
		{
			u32 bytes_next=0;
			const u32 code_next = CTextUtility::GetCharOfSJISChar(pt+bytes, bytes_next);
			if(code_next == '\n')
			{
				if(crlf_code_bytes)
					*crlf_code_bytes = bytes+bytes_next;
				return pt;
			}
			else if(code_next == '\0')
			{
				break;
			}
			else
			{
				if(crlf_code_bytes)
					*crlf_code_bytes = bytes;
				return pt;
			}
		}
		else if(code == '\\')
		{
			u32 bytes_next=0;
			const u32 code_next = CTextUtility::GetCharOfSJISChar(pt+bytes, bytes_next);
			if(code_next == 'n')
			{
				if(crlf_code_bytes)
					*crlf_code_bytes = bytes+bytes_next;
				return pt;
			}
			else if(code_next == '\0')
			{
				break;
			}
			else
			{
				pt += bytes;
			}
		}
		else
		{
			pt += bytes;
		}
	}

	return NULL;
}

b8		CTextUtility::EraseLastCRLFCodeSJIS(c8* _buff, const c8* _text)
{
	if(!_text) { return false; }
	size_t len = strlen(_text);
	c8 c_1 = 0;
	c8 c_2 = 0;
	b8 adjusted = false;
	if(len >= 2)
	{
		c_2 = *(_text+len-2);
		c_1 = *(_text+len-1);
		if((c_2 == '\r' && c_1 == '\n') ||
		   (c_2 == '\\' && c_1 == 'n'))
		{
			len -= 2;
			adjusted = true;
		}
	}
	if(!adjusted && len >= 1)
	{
		c_1 = *(_text+len-1);
		if(c_1 == '\r' || c_1 == '\n')
		{
			len --;
			adjusted = true;
		}
	}
	strncpy_s(_buff, len, _text, len);
	*(_buff+len) = '\0';
//	return true;
	return adjusted;
}

u16		CTextUtility::GetCRLFCodeNumSJIS(const c8* _text)
{
	if(!_text) { return 0; }
	const c8	*pt	= _text;
	u16 lines=0;
	while( *pt )
	{
		u32 bytes=0;
		const u32 code = CTextUtility::GetCharOfSJISChar(pt, bytes);
		// 改行
		if(code == '\n')
		{
			lines++;
			pt += bytes;
		}
		else if(code == '\r')
		{
			u32 bytes_next=0;
			const u32 code_next = CTextUtility::GetCharOfSJISChar(pt+bytes, bytes_next);
			if(code_next == '\n')
			{
				lines ++;
				pt += (bytes+bytes_next);
			}
			else if(code_next == '\0')
			{
				break;
			}
			else
			{
				lines++;
				pt += bytes;
			}
		}
		else if(code == '\\')
		{
			u32 bytes_next=0;
			const u32 code_next = CTextUtility::GetCharOfSJISChar(pt+bytes, bytes_next);
			if(code_next == 'n')
			{
				lines ++;
				pt += (bytes+bytes_next);
			}
			else if(code_next == '\0')
			{
				break;
			}
			else
			{
				pt += bytes;
			}
		}
		else
		{
			pt += bytes;
		}
	}

	return lines;
}

//-------------------------------------------------------------------------------------------------
// 文字数を取得する
s32		CTextUtility::GetLengthSJIS(const c8* _text)
{
	s32 len = 0;

	const c8* p = _text;
	while( *p )
	{
		if( IsSJISChar1of1( *p ) )	p++;
		else						p += 2;
		len++;
	}
	return len;
}

s32		CTextUtility::CopyTextSJIS(c8* _buff, const c8* _text, const u32 _offset, const u32 _len)
{
	s32 l = GetLengthSJIS( _text );

	if( l <= (s32)_len )
	{
		strcpy_safe( _buff, _len, _text );
		return l;
	}
	else 
	{
		u32 offset_cnt    = 0;
		u32 len_cnt       = 0;
		u32 byte_cnt      = 0;
		const c8* text_p   = _text;
		const c8* text_top = _text;
		while( 1 )
		{
			const c8 c = *text_p;
			if(!c)
				break;
			const u32 bytes = GetBytesOfSJISChar(text_p);
			byte_cnt += bytes;
			text_p   += bytes;
			
			if(offset_cnt < _offset)
			{
				offset_cnt ++;
				text_top = text_p;
			}
			else
			{
				len_cnt++;
				if( _len <= len_cnt )
					break;
			}
		}
		memset( _buff, 0,        byte_cnt + 1 );
		memcpy( _buff, text_top, byte_cnt     );

		return _len;
	}
}

s32		CTextUtility::ExChangeNumTextSJIS(c8* _buff, const u32 _bufSize , u32 _value , u32 _digit)
{
	u32	val		=	_value;
	u32	div		=	1;
	u32	dig_lim	=	_digit-1;
	for(u32 i=0; i<dig_lim; i++ )	div *= 10;
//	CHR 33359,    0, 271,  63,16, 3,23,0; // '０'
	u32	index	=	0;
	for(u32 i=0;i<=dig_lim;i++){
		c8*	text_p	=_buff	+	index;
		b8	skip	=false;
		u32	div_num	=	val	/	div;
				val		=	val	-	div_num	*	div;
				div		=	div	/	10;

		if( index == 0 ){
			if( div_num == 0 )
				skip = true;
		}

		if(	i	==	dig_lim)
			skip=	false;

		if(	!skip	)
		{
			u16 str_code = SCast<u16>(0x824f + div_num);
			memcpy( text_p , &str_code , sizeof(u16) );
			index	+=	2;
			if(	index >= _bufSize )
			{
				memset( _buff , 0x00 , _bufSize );
				return	false;
			}
		}
	}
	return	false;
}

s32		CTextUtility::ElideTextSJIS(c8* _buff, const c8* _text, const u32 _offset, const u32 _len, const c8* _ellipsis, const b8 _is_reverse)
{
	const s32 text_len = GetLengthSJIS(_text);
	s32 len = CopyTextSJIS(_buff, _text, _offset, _len);
	if( len < text_len )
	{
		if(!_is_reverse)
		{
			strcat_s(_buff, _len, _ellipsis);
		}
		else
		{
			_buff += sprintf_safe(_buff, _len, "%s", _ellipsis);
			CopyTextSJIS(_buff, _text, _offset, _len);
		}
		len += GetLengthSJIS(_ellipsis);
	}
	return len;
}

// UTF-8での全角／半角文字判定　※マルチバイト文字の２バイト目を渡した場合の挙動は未定義です。必ず１バイト目として判定します。
b8		CTextUtility::IsUTF8CharHan(const u32 c)
{
	if (c == 0x00) {
		return 0;
	}
	if(!(c & 0xffffff80))
	{
		return true;
	}
	else if((c >= 0xefbda1 && c <= 0xefbdbf) ||
	        (c >= 0xefbe80 && c <= 0xefbe90))
	{
		return true;
	}

	return false;
}

// UTF-8で何バイトの文字か
u32		CTextUtility::GetBytesOfUTF8Char(c8 c)
{
	if(c == 0x00) { return 1; }
	if(!(c & 0x80)) { return 1; }
	c <<= 1; if(!(c & 0x80)) return 1;
	c <<= 1; if(!(c & 0x80)) return 2;
	c <<= 1; if(!(c & 0x80)) return 3;
	c <<= 1; if(!(c & 0x80)) return 4;
	return 1;
}
// UTF-8で何バイトの文字か（ポインタ版）
u32		CTextUtility::GetBytesOfUTF8Char(const c8* text_p)
{
	const u32 bytes = GetBytesOfUTF8Char(*(text_p+0));
	if(     bytes > 1 && !*(text_p+1))  return 1;
	else if(bytes > 2 && !*(text_p+2))  return 2;
	else if(bytes > 3 && !*(text_p+3))  return 3;
	return bytes;
}
// UTF-8で何バイトの文字か
u32		CTextUtility::GetBytesOfUTF8Char(const u32 _code)
{
	if (_code & 0xff000000){ return 4; }
	else if (_code & 0x00ff0000){ return 3; }
	else if (_code & 0x0000ff00){ return 2; }
	else if (_code & 0x000000ff){ return 1; }

	return 1;
}

//-------------------------------------------------------------------------------------------------
// UTF-8での文字コードを返す
u32		CTextUtility::GetCharOfUTF8Char(const c8* text_p, u32& bytes)
{
	const c8 c = *text_p;
	if(!c)
	{
		bytes = 0;
		return 0;
	}
	else if(c == '\n')
	{
		bytes = 1;
		return '\n';
	}
	else if(c == '\r')
	{
		bytes = 1;
		const c8 c2 = *(text_p + 1);
		if(c2 == '\n')
		{
			bytes = 2;
			return '\n';
		}
		return '\n';
	}
	else if(c == '\\')
	{
		const c8 c2 = *(text_p + 1);
		if(c2 == 'n')
		{
			bytes = 2;
			return '\n';
		}
#if 0
		else if(c2 == 'r')
		{
			const c8 c3 = *(text_p + 2);
			if(c3 == '\\')
			{
				const c8 c4 = *(text_p + 3);
				if(c4 == 'n')
				{
					bytes = 4;
					return '\n';
				}
			}
			bytes = 2;
			return '\n';
		}
		else if(c2 == 't')
		{
			bytes = 2;
			return '\t';
		}
#endif // 0
	}
	bytes = GetBytesOfUTF8Char(text_p);
	const u32 index = bytes - 1;
	if(index & 0xfffffffc) { return ILLEGAL_CHAR_CODE; }
	
	typedef u32 (*func_p)(const c8* text_p);
	static func_p func_table[] =
	{
		GetCharOfUTF8Char1,
		GetCharOfUTF8Char2,
		GetCharOfUTF8Char3,
		GetCharOfUTF8Char4,
	};
	return func_table[index]( text_p );
}
// UTF-8でＮＧの文字コードを返す
u32		CTextUtility::GetCharOfUTF8CharNG(const c8* text_p)
{
	return ILLEGAL_CHAR_CODE;
}
// UTF-8で文字コードを返す（１バイト）
u32		CTextUtility::GetCharOfUTF8Char1(const c8* text_p)
{
	return ((static_cast<u32>(*(text_p+0)) <<  0) & 0x000000ff);
}
// UTF-8で文字コードを返す（２バイト）
u32		CTextUtility::GetCharOfUTF8Char2(const c8* text_p)
{
	return ((static_cast<u32>(*(text_p+0)) <<  8) & 0x0000ff00) |
		   ((static_cast<u32>(*(text_p+1)) <<  0) & 0x000000ff);
}
// UTF-8で文字コードを返す（３バイト）
u32		CTextUtility::GetCharOfUTF8Char3(const c8* text_p)
{
	return ((static_cast<u32>(*(text_p+0)) << 16) & 0x00ff0000) |
		   ((static_cast<u32>(*(text_p+1)) <<  8) & 0x0000ff00) |
		   ((static_cast<u32>(*(text_p+2)) <<  0) & 0x000000ff);
}
// UTF-8で文字コードを返す（４バイト）
u32		CTextUtility::GetCharOfUTF8Char4(const c8* text_p)
{
	return ((static_cast<u32>(*(text_p+0)) << 24) & 0xff000000) |
		   ((static_cast<u32>(*(text_p+1)) << 16) & 0x00ff0000) |
		   ((static_cast<u32>(*(text_p+2)) <<  8) & 0x0000ff00) |
		   ((static_cast<u32>(*(text_p+3)) <<  0) & 0x000000ff);
}

//-------------------------------------------------------------------------------------------------
// バイナリを１６進文字列化
static inline c8	bin2hex_c8(const s32 bin)
{
	if(bin >= 0 && bin <= 9) { return SCast<c8>('0' + bin); }
	else if(bin >= 10 && bin <= 15) { return SCast<c8>('a' + bin - 10); }
	return '?';
}
c8*		CTextUtility::bin2hex(c8* dst, const u8* bin, const s32 bin_size)
{
	if(!dst) { return NULL; }
	if(!bin) { return NULL; }
	c8* bin_p = (c8*)bin;
	for(s32 col = 0; col < bin_size; col ++)
	{
		c8 c = *(bin_p ++);
		const s32 hi = ((s32)(c >> 4) & 0x0f);
		const s32 lo = ((s32)(c >> 0) & 0x0f);
		*(dst ++) = bin2hex_c8(hi);
		*(dst ++) = bin2hex_c8(lo);
	}
	*(dst ++) = '\0';
	return dst;
}

//-------------------------------------------------------------------------------------------------
// １６進文字列をバイナリ化
static inline s32	hex2bin_int(const s32 hex)
{
	if(hex >= '0' && hex <= '9') { return hex - '0'; }
	else if(hex >= 'a' && hex <= 'f') { return hex - 'a' + 10; }
	else if(hex >= 'A' && hex <= 'F') { return hex - 'A' + 10; }
	return 0;
}
u8*		CTextUtility::hex2bin(u8* dst, const c8* hex, const s32 hex_size)
{
	if(!dst) { return NULL; }
	if(!hex) { return NULL; }
	c8* hex_p = (c8*)hex;
	for(s32 col = ((hex_size%2) ? -1 : 0) ; col < hex_size; col += 2)
	{
		s32 hi = col >= 0 ? hex2bin_int((s32)*(hex_p ++)) : 0;
		s32 lo = hex2bin_int((s32)*(hex_p ++));
		*(dst ++) = SCast<u8>((hi << 4) | (lo << 0));
	}
	return dst;
}

//-------------------------------------------------------------------------------------------------
// 文字長の取得（UTF-8）
s32		CTextUtility::GetLengthUTF8(const c8* _text)
{
	s32 len = 0;
	const c8* p = _text;

	while( *p )
	{
		if( IsUTF8Char1of1( *p ) )			p++;
		else if( IsUTF8Char1of2( *p ) )		p += 2;
		else if( IsUTF8Char2After( *p ) )	p += 2;
		else if( IsUTF8Char1of3( *p ) )		p += 3;
		else if( IsUTF8Char1of4( *p ) )		p += 4;
		len++;
	}
	return len;
}

//-------------------------------------------------------------------------------------------------
// 文字列の先頭から指定の文字数だけコピー（UTF-8）
s32		CTextUtility::CopyTextUTF8(c8* _buff, const c8* _text, const u32 _offset, const u32 _len)
{
	s32 l = GetLengthUTF8( _text );

	if( l <= (s32)_len )
	{
		strcpy_safe(_buff, _len, _text);
		return l;
	}
	else 
	{
		u32 offset_cnt    = 0;
		u32 len_cnt       = 0;
		u32 byte_cnt      = 0;
		const c8* text_p   = _text;
		const c8* text_top = _text;
		while( 1 )
		{
			const c8 c = *text_p;
			if(!c) { break; }
			const u32 bytes = GetBytesOfUTF8Char(text_p);
			byte_cnt += bytes;
			text_p   += bytes;
			
			if(offset_cnt < _offset)
			{
				offset_cnt ++;
				text_top = text_p;
			}
			else
			{
				len_cnt++;
				if( _len <= len_cnt ) { break; }
			}
		}
		memset( _buff, 0,        byte_cnt + 1 );
		memcpy( _buff, text_top, byte_cnt     );

		return _len;
	}
}

//-------------------------------------------------------------------------------------------------
// 改行コードを検索する（UTF-8）
const c8*	CTextUtility::SearchCRLFCodeUTF8(const c8* _text, u32* crlf_code_bytes)
{
	if(!_text) { return NULL; }
	const c8* pt = _text;
	while( *pt )
	{
		u32 bytes=0;
		const u32 code = CTextUtility::GetCharOfUTF8Char(pt, bytes);
		// 改行
		if(code == '\n')
		{
			if(crlf_code_bytes)
				*crlf_code_bytes = bytes;
			return pt;
		}
		else if(code == '\r')
		{
			u32 bytes_next=0;
			const u32 code_next = CTextUtility::GetCharOfUTF8Char(pt+bytes, bytes_next);
			if(code_next == '\n')
			{
				if(crlf_code_bytes)
					*crlf_code_bytes = bytes+bytes_next;
				return pt;
			}
			else if(code_next == '\0')
			{
				break;
			}
			else
			{
				if(crlf_code_bytes)
					*crlf_code_bytes = bytes;
				return pt;
			}
		}
		else if(code == '\\')
		{
			u32 bytes_next=0;
			const u32 code_next = CTextUtility::GetCharOfUTF8Char(pt+bytes, bytes_next);
			if(code_next == 'n')
			{
				if(crlf_code_bytes)
					*crlf_code_bytes = bytes+bytes_next;
				return pt;
			}
			else if(code_next == '\0')
			{
				break;
			}
			else
			{
				pt += bytes;
			}
		}
		else
		{
			pt += bytes;
		}
	}

	return NULL;
}

//-------------------------------------------------------------------------------------------------
// テキストの最後が改行コードだった場合に削除する（UTF8）
b8	CTextUtility::EraseLastCRLFCodeUTF8(c8* _buff, const c8* _text )
{
	if(!_text) { return false; }
	size_t len = strlen(_text);
	c8 c_1 = 0;
	c8 c_2 = 0;
	b8 adjusted = false;
	if(len >= 2)
	{
		c_2 = *(_text+len-2);
		c_1 = *(_text+len-1);
		if((c_2 == '\r' && c_1 == '\n') ||
		   (c_2 == '\\' && c_1 == 'n'))
		{
			len -= 2;
			adjusted = true;
		}
	}
	if(!adjusted && len >= 1)
	{
		c_1 = *(_text+len-1);
		if(c_1 == '\r' || c_1 == '\n')
		{
			len --;
			adjusted = true;
		}
	}
	strncpy_s(_buff, len, _text, len);
	*(_buff+len) = '\0';
	return adjusted;
}


//-------------------------------------------------------------------------------------------------	
// テキストの改行コードの数を調べる（UTF8）
u16		CTextUtility::GetCRLFCodeNumUTF8( const c8* _text )
{
	if(!_text) { return 0; }
	const c8	*pt	= _text;
	u16 lines=0;
	while( *pt )
	{
		u32 bytes=0;
		const u32 code = CTextUtility::GetCharOfUTF8Char(pt, bytes);
		// 改行
		if(code == '\n')
		{
			lines++;
			pt += bytes;
		}
		else if(code == '\r')
		{
			u32 bytes_next=0;
			const u32 code_next = CTextUtility::GetCharOfUTF8Char(pt+bytes, bytes_next);
			if(code_next == '\n')
			{
				lines ++;
				pt += (bytes+bytes_next);
			}
			else if(code_next == '\0')
			{
				break;
			}
			else
			{
				lines++;
				pt += bytes;
			}
		}
		else if(code == '\\')
		{
			u32 bytes_next=0;
			const u32 code_next = CTextUtility::GetCharOfUTF8Char(pt+bytes, bytes_next);
			if(code_next == 'n')
			{
				lines ++;
				pt += (bytes+bytes_next);
			}
			else if(code_next == '\0')
			{
				break;
			}
			else
			{
				pt += bytes;
			}
		}
		else
		{
			pt += bytes;
		}
	}

	return lines;
}

//*************************************************************************************************	
// UTF8処理
//*************************************************************************************************	

//-------------------------------------------------------------------------------------------------	
// 数字情報を全角文字列に変換する（UTF-8）
s32		CTextUtility::ExChangeNumTextUTF8(c8* _buff, const u32 _bufSize , u32 _value , u32 _digit)
{
	u32	val		=	_value;
	u32	div		=	1;
	u32	dig_lim	=	_digit-1;
	for(u32 i=0; i< dig_lim; i++ ) { div *= 10; }
//	CHR 33359,    0, 271,  63,16, 3,23,0; // '０'
	u32	index	=	0;
	for(u32 i=0;i<=dig_lim;i++)
	{
		c8*	text_p	=_buff	+	index;
		b8	skip	=false;
		u32	div_num	=	val	/	div;
		val =	val - div_num * div;
		div =	div / 10;

		if( index == 0 )
		{
			if( div_num == 0 ) { skip = true; }
		}

		if(	i	==	dig_lim){ skip=	false; }
		if(	!skip	)
		{
			u16 str_code = SCast<u16>(0xff10 + div_num);
			memcpy( text_p , &str_code , sizeof(u16) );
			index	+=	2;
			if(	index >= _bufSize )
			{
				memset( _buff , 0x00 , _bufSize );
				return	false;
			}
		}
	}
	return	false;
}

//-------------------------------------------------------------------------------------------------	
// 文字列を指定の省略記号に置き換える（UTF-8）
s32		CTextUtility::ElideTextUTF8(c8* _buff, const c8* _text, const u32 _offset, const u32 _len, const c8* _ellipsis, const b8 _is_reverse)
{
	const s32 text_len = GetLengthUTF8(_text);
	s32 len = CopyTextUTF8(_buff, _text, _offset, _len);
	if( len < text_len )
	{
		if(!_is_reverse)
		{
			strcat_s(_buff, _len, _ellipsis);
		}
		else
		{
			_buff += sprintf_safe(_buff, _len, "%s", _ellipsis);
			CopyTextUTF8(_buff, _text, _offset, _len);
		}
		len += GetLengthUTF8(_ellipsis);
	}
	return len;
}

//-------------------------------------------------------------------------------------------------	
// '\n'と'\t'を半角スペースに置き換える
void	CTextUtility::ReplaceUnnecessaryChar(c8* _dst)
{
	while( *_dst )
	{
		u32 bytes=0;
		const u32 code = CTextUtility::GetCharOfUTF8Char(_dst, bytes);
		const c8 c = *_dst;
		//	\n or \t
		if(code == '\n')
		{
			*_dst = ' ';
			_dst++;
			*_dst = ' ';
			_dst++;
		}
		else if(c == '\\')
		{
			const c8 c2 = *(_dst + 1);
			if(c2 == 't'){
				*_dst = ' ';
				_dst++;
				*_dst = ' ';
				_dst++;
			}
			else
			{
				_dst += bytes;
			}
		}
		else
		{
			_dst += bytes;
		}
	}
}

//-------------------------------------------------------------------------------------------------	
// 制御文字を置き換える
void	CTextUtility::ReplaceControlChar(c8* _dst)
{
	while( *_dst )
	{
		u32 bytes=0;
		CTextUtility::GetCharOfUTF8Char(_dst, bytes);
		const c8 c = *_dst;

		b8 is_find = false;

		for(s32 i = 0; i < CONTROL_CHAR_MAXNUM; i++)
		{
			if(c == CONTROL_CHAR[i])
			{
				*_dst = REPLACE_CHAR[i];
				_dst++;
				is_find = true;
				break;
			}
		}

		if(is_find == false)
		{
			_dst += bytes;
		}
	}
}

//-------------------------------------------------------------------------------------------------	
// 制御文字を元に戻す
void	CTextUtility::ReturnsControlChar(c8* _dst)
{
	while( *_dst )
	{
		u32 bytes=0;
		CTextUtility::GetCharOfUTF8Char(_dst, bytes);
		const c8 c = *_dst;

		b8 is_find = false;

		for(s32 i = 0; i < CONTROL_CHAR_MAXNUM; i++)
		{
			if(c == REPLACE_CHAR[i])
			{
				*_dst = CONTROL_CHAR[i];
				_dst++;
				is_find = true;
				break;
			}
		}

		if(is_find == false)
		{
			_dst += bytes;
		}
	}
}

//-------------------------------------------------------------------------------------------------	
// 制御文字を元に戻す
void	CTextUtility::ReturnsControlChar(u32& _dst)
{
	for(s32 i = 0; i < CONTROL_CHAR_MAXNUM; i++)
	{
		if(_dst == (u32)REPLACE_CHAR[i])
		{
			_dst = CONTROL_CHAR[i];
			break;
		}
	}
}

//-------------------------------------------------------------------------------------------------	
/// 文字列を数値化する処理
s32		CTextUtility::GetStringToNum(const c8* _text, const c8** _text_list, const s32* _num_list)
{
	if(!_text || !_text_list || !_num_list)
		return 0;
	const c8** _text_p = _text_list;
	const s32*   _num_p  = _num_list;
	while(*_text_p)
	{
		if(strcmp(*_text_p, _text) == 0) { return *_num_p; }
		_text_p ++;
		_num_p ++;
	}
	return 0;
}

//-------------------------------------------------------------------------------------------------	
// UTF-8 & LF改行として正当な文字列かどうかをチェック
u32		CTextUtility::TestStringIsUtf8LF(const c8 *_text, u32 _len)
{
	const c8 *p = _text;
	u32 seq = 0;
	u32 last_seq_start = 0;
	for(u32 i = 0; i < _len; i++, p++){
		if(seq > 0)
		{
			if(!IsUTF8Char2After(*p)){ return last_seq_start; }
			seq--;
			continue;
		}
		if(IsUTF8Char1of1(*p)){
			if(*p == 0x13){
				// CRは存在してはならない
				return i;
			}
			last_seq_start = i;
			seq = 0;
			continue;
		}
		else
			if (IsUTF8Char1of2(*p)){
				last_seq_start = i;
				seq = 1;
				continue;
			}
			else
				if (IsUTF8Char1of3(*p)){
					last_seq_start = i;
					seq = 2;
					continue;
				}
				else
					if (IsUTF8Char1of4(*p)){
						last_seq_start = i;
						seq = 3;
						continue;
					}
					else
					{
						return i;
					}
	}
	if (seq > 0){
		// 終端がおかしい。マルチバイトの途中で切れている
		return last_seq_start;
	}
	return _len;
}

//-------------------------------------------------------------------------------------------------
// タグを文字列に置換する
b8		CTextUtility::ReplaceTag(c8* _dest, const u32 _dest_length, const c8* _src, const TAG_WORD* _tag_word, u32 _tag_num, TEXT_TYPE _text_code)
{
	CALL_TEXT_TYPE_METHOD(ReplaceTag, _text_code, _dest, _dest_length, _src, _tag_word, _tag_num);
}

//-------------------------------------------------------------------------------------------------	
// タグを解析してコールバックを呼ぶ
b8		CTextUtility::CallTag(c8* _dest, u32 _dest_length, const c8* _src, const CALL_TAG_INFO& _tag_info, void* _ptr, TEXT_TYPE _text_code)
{
	CALL_TEXT_TYPE_METHOD(CallTag, _text_code, _dest, _dest_length, _src, _tag_info, _ptr);
}

//-------------------------------------------------------------------------------------------------	
// 文字検索
const c8*	CTextUtility::SearchChar(const c8* _str, u32 _search_char, u32 _length, TEXT_TYPE _text_code)
{
	CALL_TEXT_TYPE_METHOD(SearchChar, _text_code, _str, _search_char, _length);
}

//-------------------------------------------------------------------------------------------------	
// タグを検索して先頭ポインタと文字数を返す
const c8*	CTextUtility::CheckTag(const c8* _str, u32 _tag_begin, u32 _tag_end, u32& _tag_length, u32& _arg_length, TEXT_TYPE _text_code)
{
	CALL_TEXT_TYPE_METHOD(CheckTag, _text_code, _str, _tag_begin, _tag_end, _tag_length, _arg_length);
}

//-------------------------------------------------------------------------------------------------	
// 文字列を指定の文字で区切り,先頭と長さを返す
const c8*	CTextUtility::StrSplit(const c8* _str, u32 _max_length, const  u32* _split, u32 _split_num, u32& _arg_length, TEXT_TYPE _text_code)
{
	CALL_TEXT_TYPE_METHOD(StrSplit, _text_code, _str, _max_length, _split, _split_num, _arg_length);
}

//-------------------------------------------------------------------------------------------------	
// 特定の文字(複数)と一致するか
b8		CTextUtility::StrCompare(const c8* _str, const  u32* _split, u32 _split_num, TEXT_TYPE _text_code)
{
	CALL_TEXT_TYPE_METHOD(StrCompare, _text_code, _str, _split, _split_num);
}

//-------------------------------------------------------------------------------------------------
// 文字コード変換（一文字）
u32		CTextUtility::ConvTextCode(TEXT_TYPE _src_type, TEXT_TYPE _dest_type, u32 _char, u32 _error)
{
	if (_src_type == TEXT_TYPE::TEXT_TYPE_UTF8)
	{
		switch (_dest_type)
		{
		case TEXT_TYPE::TEXT_TYPE_UTF8:
			return CodeUTF8toUTF8(_char, _error);
		case TEXT_TYPE::TEXT_TYPE_SJIS:
			return CodeUTF8toSJIS(_char, _error);
		default:
			return CodeUTF8toASCII(_char, _error);
		}
	}
	else if (_src_type == TEXT_TYPE::TEXT_TYPE_SJIS)
	{
		switch (_dest_type)
		{
		case TEXT_TYPE::TEXT_TYPE_UTF8:
			return CodeSJIStoUTF8(_char, _error);
		case TEXT_TYPE::TEXT_TYPE_SJIS:
			return CodeSJIStoSJIS(_char, _error);
		default:
			return CodeSJIStoASCII(_char, _error);
		}
	}
	else
	{
		switch (_dest_type)
		{
		case TEXT_TYPE::TEXT_TYPE_UTF8:
			return CodeASCIItoUTF8(_char, _error);
		case TEXT_TYPE::TEXT_TYPE_SJIS:
			return CodeASCIItoSJIS(_char, _error);
		default:
			return CodeASCIItoASCII(_char, _error);
		}
	}
}

//-------------------------------------------------------------------------------------------------
// 文字コード変換（文字列）
const c8*	CTextUtility::ConvTextCode(TEXT_TYPE _src_type, TEXT_TYPE _dest_type, const c8* _string, c8* _buff, u32 _error)
{
	if (_src_type == TEXT_TYPE::TEXT_TYPE_UTF8)
	{
		switch (_dest_type)
		{
		case TEXT_TYPE::TEXT_TYPE_UTF8:
			return CodeUTF8toUTF8(_buff, _string, _error);
		case TEXT_TYPE::TEXT_TYPE_SJIS:
			return CodeUTF8toSJIS(_buff, _string, _error);
		default:
			return CodeUTF8toASCII(_buff, _string, _error);
		}
	}
	else if (_src_type == TEXT_TYPE::TEXT_TYPE_SJIS)
	{
		switch (_dest_type)
		{
		case TEXT_TYPE::TEXT_TYPE_UTF8:
			return CodeSJIStoUTF8(_buff, _string, _error);
		case TEXT_TYPE::TEXT_TYPE_SJIS:
			return CodeSJIStoSJIS(_buff, _string, _error);
		default:
			return CodeSJIStoASCII(_buff, _string, _error);
		}
	}
	else
	{
		switch (_dest_type)
		{
		case TEXT_TYPE::TEXT_TYPE_UTF8:
			return CodeASCIItoUTF8(_buff, _string, _error);
		case TEXT_TYPE::TEXT_TYPE_SJIS:
			return CodeASCIItoSJIS(_buff, _string, _error);
		default:
			return CodeASCIItoASCII(_buff, _string, _error);
		}
	}
}

//-------------------------------------------------------------------------------------------------	
// 何バイトの文字か
u32		CTextUtility::GetBytesOfChar(const TEXT_TYPE _type, const c8 _c)
{
	switch (_type)
	{
	case TEXT_TYPE::TEXT_TYPE_UTF8:
		return GetBytesOfUTF8Char(_c);
	case TEXT_TYPE::TEXT_TYPE_SJIS:
		return GetBytesOfSJISChar(_c);
	default:
		return 1;
	}
}

//-------------------------------------------------------------------------------------------------
// 整数型の数値をカンマ付文字列に変換
void	CTextUtility::Converts32ToStrCommaNum(c8* _ret, s32 _num)
{
	if (!_ret){ return;	}
	s32	digit = 0;			  // 書き込んだ桁数
	c8	*ptr = _ret;		  // 文字配列の先頭にポインタセット
	u32	absValue = abs(_num); // 数値の絶対値取得
	
	do // while( 0 != absValue );
	{
		*ptr++ = "0123456789"[absValue % 10];
		absValue = absValue / 10;
		digit++;

		if ((0 != absValue) && (0 == (digit % 3))) *ptr++ = ',';
	} while (0 != absValue);

	if (_num < 0) *ptr++ = '-';
	*ptr = '\0';

	ReverseString(_ret);
}

//-------------------------------------------------------------------------------------------------
// 文字列の並びを逆にする
void	CTextUtility::ReverseString(c8* _str)
{
	if (!_str){ return; }
	size_t length	= strlen(_str);			// 文字列長取得
	c8* former		= _str;					// 先頭→半分までの前半を移動していくポインタ
	c8* latter		= &_str[length - 1];	// 末尾→半分までの後半を移動していくポインタ

	for (u32 i = 0; i < (length / 2); i++)
	{
		c8 keepChara = *former;				// 文字入れ替え時の一時保持用変数
		*former = *latter;
		*latter = keepChara;
		former++;
		latter--;
	}
}


//-------------------------------------------------------------------------------------------------	
// UTF8からASCIIへの文字コード変換（一文字）
u32		CodeUTF8toASCII(u32 _char, u32 _error)
{
	// アスキーコードでないなら'?'に変換
	if (_char & 0xff80)
	{
		return _error;	//< '?'
	}

	return _char;
}

// UTF8からASCIIへの文字コード変換（文字列）
const c8*	CodeUTF8toASCII(c8* _buff, const c8* _string, u32 _error)
{
	CStrUTF8 utf8 = _string;
	c8* ascii_buff = _buff;

	while (*utf8)
	{
		u32 code = CodeUTF8toASCII(*utf8, _error);
		*ascii_buff = SCast<c8>(code);
		++utf8;
	}

	return _buff;
}

//-------------------------------------------------------------------------------------------------
// UTF8からSJISへの文字コード変換（一文字）
u32		CodeUTF8toSJIS(u32 _char, u32 _error)
{
	u32 out = ConvUTF8toSJIS(_char);

	return out == 63 ? _error : out;
}

// UTF8からSJISへの文字コード変換（文字列）
const c8*	CodeUTF8toSJIS(c8* _buff, const c8* _string, u32 _error)
{
	CStrUTF8 utf8 = _string;
	c8* sjis_buff = _buff;

	while (*utf8)
	{
		u32 code = CodeUTF8toSJIS(*utf8, _error);
		u32 byte = CTextUtility::GetBytesOfSJISChar(utf8);

		for (u32 idx = 0; idx < byte; ++idx)
		{
			u32 shift = 8 * (byte - idx - 1);
			*sjis_buff = (code >> shift) & 0x000000ff;
			++sjis_buff;
		}
		++utf8;
	}

	return _buff;
}

//-------------------------------------------------------------------------------------------------
// UTF8からUTF8への文字コード変換（一文字）
u32		CodeUTF8toUTF8(u32 _char, u32 _error)
{
	return _char;
}

// UTF8からUTF8への文字コード変換（文字列）
const c8*	CodeUTF8toUTF8(c8* _buff, const c8* _string, u32 _error)
{
	size_t strLen = strlen(_buff);
	strcpy_safe(_buff, strLen, _string);
	return _buff;
}

//-------------------------------------------------------------------------------------------------	
// SJISからASCIIへの文字コード変換（一文字）
u32		CodeSJIStoASCII(u32 _char, u32 _error)
{
	// アスキーコードでないなら'?'に変換
	if (_char & 0xff80)
	{
		return _error;
	}

	return _char;
}

// SJISからASCIIへの文字コード変換（文字列）
const c8*	CodeSJIStoASCII(c8* _buff, const c8* _string, u32 _error)
{
	CStrUTF8 sjis = _string;
	c8* ascii_buff = _buff;

	while (*sjis)
	{
		u32 code = CodeUTF8toASCII(*sjis, _error);
		*ascii_buff = SCast<c8>(code);
		++sjis;
	}

	return _buff;
}

//-------------------------------------------------------------------------------------------------	
// SJISからSJISへの文字コード変換（一文字）
u32		CodeSJIStoSJIS(u32 _char, u32 _error)
{
	return _char;
}

// SJISからSJISへの文字コード変換（文字列）
const c8*	CodeSJIStoSJIS(c8* _buff, const c8* _string, u32 _error)
{
	size_t strLen = strlen(_buff);
	strcpy_safe(_buff, strLen, _string);
	return _buff;
}

//-------------------------------------------------------------------------------------------------	
// SJISからUTF8への文字コード変換（一文字）
u32		CodeSJIStoUTF8(u32 _char, u32 _error)
{
	u32 out = ConvSJIStoUTF8(_char);

	return out == 63 ? _error : out;
}

// SJISからUTF8への文字コード変換（文字列）
const c8*	CodeSJIStoUTF8(c8* _buff, const c8* _string, u32 _error)
{
	CStrUTF8 sjis = _string;
	c8* sjis_buff = _buff;

	while (*sjis)
	{
		u32 code = CodeSJIStoUTF8(*sjis, _error);
		u32 byte = CTextUtility::GetBytesOfUTF8Char(code);

		for (u32 idx = 0; idx < byte; ++idx)
		{
			u32 shift = 8 * (byte - idx - 1);
			*sjis_buff = (code >> shift) & 0x000000ff;
			++sjis_buff;
		}
		++sjis;
	}

	return _buff;
}

//-------------------------------------------------------------------------------------------------	
// ASCIIからASCIIへの文字コード変換（一文字）
u32		CodeASCIItoASCII(u32 _char, u32 _error)
{
	return _char;
}

// ASCIIからASCIIへの文字コード変換（文字列）
const c8*	CodeASCIItoASCII(c8* _buff, const c8* _string, u32 _error)
{
	size_t strLen = strlen(_buff);
	strcpy_safe(_buff, strLen, _string);
	return _buff;
}

//-------------------------------------------------------------------------------------------------	
// ASCIIからSJISへの文字コード変換（一文字）
u32		CodeASCIItoSJIS(u32 _char, u32 _error)
{
	return _char;
}

// ASCIIからSJISへの文字コード変換（文字列）
const c8*	CodeASCIItoSJIS(c8* _buff, const c8* _string, u32 _error)
{
	size_t strLen = strlen(_buff);
	strcpy_safe(_buff, strLen, _string);
	return _buff;
}

//-------------------------------------------------------------------------------------------------	
// ASCIIからUTF8への文字コード変換（一文字）
u32	CodeASCIItoUTF8(u32 _char, u32 _error)
{
	return _char;
}

// ASCIIからUTF8への文字コード変換（文字列）
const c8*	CodeASCIItoUTF8(c8* _buff, const c8* _string, u32 _error)
{
	size_t strLen = strlen(_buff);
	strcpy_safe(_buff, strLen, _string);
	return _buff;
}


//-------------------------------------------------------------------------------------------------
// 有効
inline const u32	PoisonValue_IsEnable(const c8* _val) { return ((_val != NULL) && ((*_val) != '\0')); }
inline const u32	PoisonValue_IsEnable(const s32 _val) { return (_val >= 0); }
inline const u32	PoisonValue_IsEnable(const f32 _val) { return (((*(u32*)(&_val)) & 0x7fffffff) != 0x7fffffff); }
inline const u32	PoisonValue_IsEnable(const u32 _val) { return (_val != 0); }
inline const u32	PoisonValue_IsEnable(const void* _val) { return (_val != NULL); }

// 比較
const s32	PoisonValue_Compare(const c8* _dst, const c8* _src);
inline const s32	PoisonValue_Compare(const s32 _dst, const s32 _src) { return _dst - _src; }
inline const s32	PoisonValue_Compare(const f32 _dst, const f32 _src) { return (_dst == _src) ? 0 : (_dst<_src) ? -1 : 1; }
inline const s32	PoisonValue_Compare(const u32 _dst, const u32 _src) { return (_dst == _src) ? 0 : (_dst<_src) ? -1 : 1; }
inline const s32	PoisonValue_Compare(const void* _dst, const void* _src) { return (_dst == _src) ? 0 : ((size_t)_dst<(size_t)_src) ? -1 : 1; }

//-------------------------------------------------------------------------------------------------
// 先頭文字か
inline const u32	PoisonUTF8_IsHead(const u8 _c) { return ((_c & 0xc0) != 0x80); }
inline const u32	PoisonUTF8_IsHead(const c8 _c) { return PoisonUTF8_IsHead((u8)_c); }

// 拡張文字か
inline const u32	PoisonUTF8_IsExtra(const u8 _c) { return ((_c & 0xc0) == 0x80); }
inline const u32	PoisonUTF8_IsExtra(const c8 _c) { return PoisonUTF8_IsExtra((u8)_c); }

// シングルバイト文字か
inline const u32	PoisonUTF8_IsSingle(const c8 _c) { return (_c >= 0); }
inline const u32	PoisonUTF8_IsSingle(const u8 _c) { return PoisonUTF8_IsSingle((c8)_c); }

// マルチバイト文字か
inline const u32	PoisonUTF8_IsMulti(const c8 _c) { return (_c<0); }
inline const u32	PoisonUTF8_IsMulti(const u8 _c) { return PoisonUTF8_IsMulti((c8)_c); }

//-------------------------------------------------------------------------------------------------
// 文字列同士を比較
const s32	PoisonString_Compare(const c8 *_dst, const c8 *_src);
const s32	PoisonUTF8_Compare(const c8 *_dst, const c8 *_src);

//-------------------------------------------------------------------------------------------------
// 配列関数
template<typename _Type, class _Data>
const s32	PoisonArray_Compare(_Type _dst, _Data &_src)
{
	return PoisonValue_Compare(_dst, _src.GetManage());
}

//-------------------------------------------------------------------------------------------------
// ソート済み配列から対象のデータを取得する
template<typename _Type, class _Data, class _Comp>
inline _Data*	__PoisonSort_SearchPart(_Type _mngr, _Data *_list, u32 _num, _Comp _comp)
{
	//if((!_list)||(_num<=0))	{	return NULL;	}
	if (_num <= 0) { return NULL; }

	if (!PoisonValue_IsEnable(_mngr)) { return NULL; }

	s32	ret = -1;
	u32	ofs = 0;
	do
	{
		const u32	add = (_num - 1) >> 1;
		const u32	tmp = ofs + add;
		const s32	cmp = _comp(_mngr, _list[tmp]);
		if (cmp <= 0)
		{
			_num = add;
			ret = (cmp != 0) ? ret : (s32)tmp;
		}
		else
		{
			ofs = tmp + 1;
			_num -= add + 1;
		}
	} while (_num>0);

	//PoisonAssertWIN((ret<0) || (_comp(_mngr, _list[ret]) == 0));

	return (ret<0) ? NULL : _list + ret;
}

//-------------------------------------------------------------------------------------------------
template<typename _Type, class _Data>
inline _Data*	PoisonArrayVAL_SearchPart(_Type _mngr, _Data *_list, u32 _num)
{
	return __PoisonSort_SearchPart(_mngr, _list, _num, PoisonArray_Compare<_Type, _Data>);
}

//	比較
const s32	PoisonValue_Compare(const c8* _dst, const c8* _src) { return PoisonString_Compare(_dst, _src); }


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//	文字コードリスト
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// ハーフ ⇒ ワイド
struct	POISON_HALF_WIDE
{
	u16	m_half;	// ハーフ
	u16	m_wide;	// ワイド

	const s32	GetManage(void)	const { return (s32)m_half; }
};

//-------------------------------------------------------------------------------------------------

#define POISON_HALF_WIDE_NUM	(96)

//-------------------------------------------------------------------------------------------------
static const u16	Poison_uncd_half_wide[POISON_HALF_WIDE_NUM << 1] =
{
	0x0020,0x3000, 0x0021,0xFF01, 0x0022,0xFF02, 0x0023,0xFF03, 0x0024,0xFF04, 0x0025,0xFF05, 0x0026,0xFF06, 0x0027,0xFF07, 0x0028,0xFF08, 0x0029,0xFF09, 0x002a,0xFF0A,								//	半角スペース ! " # $ % & ' ( )

	0x002a,0xFF0A, 0x002b,0xFF0B, 0x002c,0xFF0C, 0x002d,0xFF0D, 0x002e,0xFF0E, 0x002f,0xFF0F,																											//	* + , - . /
	
	0x0030,0xFF10, 0x0031,0xFF11, 0x0032,0xFF12, 0x0033,0xFF13, 0x0034,0xFF14, 0x0035,0xFF15, 0x0036,0xFF16, 0x0037,0xFF17, 0x0038,0xFF18, 0x0039,0xFF19,												//	0～9

	0x003a,0xFF1A, 0x003b,0xFF1B, 0x003c,0xFF1C, 0x003d,0xFF1D, 0x003e,0xFF1E, 0x003f,0xFF1F, 0x0040,0xFF20,																							//	: ; < = > ? @

	0x0041,0xFF21, 0x0042,0xFF22, 0x0043,0xFF23, 0x0044,0xFF24, 0x0045,0xFF25, 0x0046,0xFF26, 0x0047,0xFF27, 0x0048,0xFF28, 0x0049,0xFF29, 0x004a,0xFF2A, 0x004b,0xFF2B, 0x004c,0xFF2C, 0x004d,0xFF2D,	//	A～Z
	0x004e,0xFF2E, 0x004f,0xFF2F, 0x0050,0xFF30, 0x0051,0xFF31, 0x0052,0xFF32, 0x0053,0xFF33, 0x0054,0xFF34, 0x0055,0xFF35, 0x0056,0xFF36, 0x0057,0xFF37, 0x0058,0xFF38, 0x0059,0xFF39, 0x005a,0xFF3A,

	0x005b,0xFF3B, 0x005c,0xFF3C, 0x005d,0xFF3D, 0x005e,0xFF3E, 0x005F,0xFF3F, 0x0060,0xFF40,																											//	[ \ ]  ^ _ `

	0x0061,0xFF41, 0x0062,0xFF42, 0x0063,0xFF43, 0x0064,0xFF44, 0x0065,0xFF45, 0x0066,0xFF46, 0x0067,0xFF47, 0x0068,0xFF48, 0x0069,0xFF49, 0x006a,0xFF4A, 0x006b,0xFF4B, 0x006c,0xFF4C, 0x006d,0xFF4D, 	//	a～z
	0x006e,0xFF4e, 0x006f,0xFF4f, 0x0070,0xFF50, 0x0071,0xFF51, 0x0072,0xFF52, 0x0073,0xFF53, 0x0074,0xFF54, 0x0075,0xFF55, 0x0076,0xFF56, 0x0077,0xFF57, 0x0078,0xFF58, 0x0079,0xFF59, 0x007a,0xFF5a,

	0x007b,0xFF5B, 0x007c,0xFF5C, 0x007d,0xFF5D, 0x00A5,0xFFE5,																																			// { } | 
};

static const c16	__PoisonUNCD_HalfToWide(const c16 _code)
{
	const POISON_HALF_WIDE	*tmp = PoisonArrayVAL_SearchPart((s32)_code, (const POISON_HALF_WIDE*)Poison_uncd_half_wide, POISON_HALF_WIDE_NUM);
	return (tmp) ? tmp->m_wide : _code;
}



//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//	文字列操作クラス
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	//	
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//*************************************************************************************************
// UTF8
//*************************************************************************************************
class	PoisonCStrUTF8
{
public:
	//----------------------------------------------------------------------------------------------
	// シングル文字か
	static inline const u32	IsSingle(const c8 *_str)
	{
		return PoisonUTF8_IsSingle(*_str);
	}

	//----------------------------------------------------------------------------------------------
	// マルチ文字か
	static inline const u32	IsMulti(const c8 *_str)
	{
		return PoisonUTF8_IsMulti(*_str);
	}

	//----------------------------------------------------------------------------------------------
	// 次の文字まで進める
	static inline const b8	NextByString(const c8* &_str)
	{
		if (!(*_str)) { return false; }
		if (PoisonUTF8_IsExtra(*_str))
		{
			// 途中のコードのとき
			_str++;
			while (PoisonUTF8_IsExtra(*_str))
			{
				_str++;
			}
		}
		else {
			_str += LengthByString(_str);
		}
		return true;
	}

	//----------------------------------------------------------------------------------------------
	// 文字列から必要バイト数を求める
	static inline const size_t	LengthByString(const c8 *_str)
	{
		static const u8	num[128] =
		{
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,	//	0000000? - 0001111?
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,	//	0010000? - 0011111?
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,	//	0100000? - 0101111?
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,	//	0110000? - 0111111?
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	//	1000000? - 1001111?
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	//	1010000? - 1011111?
			2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,	//	1100000? - 1101111?
			3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 6, 0,	//	1110000? - 1111111?
		};
		return num[(*(const u8*)_str) >> 1];
	}

	//----------------------------------------------------------------------------------------------
	// 文字列を文字コードに変換
	static inline const c32	StringToCode(const c8* &_str)
	{
		if (!PoisonUTF8_IsHead(*_str))
		{
			// 途中のコードのとき
			return 0;
		}
		else
		{
			if (PoisonUTF8_IsSingle(*_str))
			{
				// シングルバイト
				const c32	ret = (c32)(*_str);
				_str++;
				return ret;
			}
			else
			{
				// マルチバイト
				u8	bit = 0x40;
				u8	chr = *(const u8*)_str;

				u32	msk = 0x3f;
				c32	ret = *(const u8*)_str;
				_str++;

				while ((*_str) && ((chr&bit) != 0))
				{
					ret <<= 6;
					ret |= (*(const u8*)_str) & 0x3f;

					msk <<= 5;
					msk |= 0x1f;

					bit >>= 1;
					_str++;
				}

				return (c32)(ret&msk);
			}
		}
	}

	//----------------------------------------------------------------------------------------------
	// 文字コードを文字列に変換
	static inline const u32	CodeToString(c8* &_str, const c32 _chr, const size_t _max)
	{
		if (_chr == 0) { return false; }

		if (_chr<0x00010000)
		{
			if (_chr<0x00000080)
			{
				// 7bit
				if (_max <= 1) { return false; }
				_str[0] = (u8)_chr;
				_str++;
			}
			else if (_chr<0x00000800)
			{
				// 11bit
				if (_max <= 2) { return false; }
				_str[0] = (u8)(0xc0 | ((_chr >> 6)));
				_str[1] = (u8)(0x80 | ((_chr & 0x3f)));
				_str += 2;
			}
			else
			{
				// 16bit
				if (_max <= 3) { return false; }
				_str[0] = (u8)(0xe0 | ((_chr >> 12)));
				_str[1] = (u8)(0x80 | ((_chr >> 6) & 0x3f));
				_str[2] = (u8)(0x80 | ((_chr & 0x3f)));
				_str += 3;
			}
		}
		else
		{
			if (_chr<0x00200000)
			{
				// 21bit
				if (_max <= 4) { return false; }
				_str[0] = (u8)(0xf0 | ((_chr >> 18)));
				_str[1] = (u8)(0x80 | ((_chr >> 12) & 0x3f));
				_str[2] = (u8)(0x80 | ((_chr >> 6) & 0x3f));
				_str[3] = (u8)(0x80 | ((_chr & 0x3f)));
				_str += 4;
			}
			else if (_chr<0x04000000)
			{
				// 26bit
				if (_max <= 5) { return false; }
				_str[0] = (u8)(0xf8 | ((_chr >> 24)));
				_str[1] = (u8)(0x80 | ((_chr >> 18) & 0x3f));
				_str[2] = (u8)(0x80 | ((_chr >> 12) & 0x3f));
				_str[3] = (u8)(0x80 | ((_chr >> 6) & 0x3f));
				_str[4] = (u8)(0x80 | ((_chr & 0x3f)));
				_str += 5;
			}
			else
			{
				// 31bit
				if (_max <= 6) { return false; }
				_str[0] = (u8)(0xfc | ((_chr >> 30)));
				_str[1] = (u8)(0x80 | ((_chr >> 24) & 0x3f));
				_str[2] = (u8)(0x80 | ((_chr >> 18) & 0x3f));
				_str[3] = (u8)(0x80 | ((_chr >> 12) & 0x3f));
				_str[4] = (u8)(0x80 | ((_chr >> 6) & 0x3f));
				_str[5] = (u8)(0x80 | ((_chr & 0x3f)));
				_str += 6;
			}
		}

		return true;
	}

	//----------------------------------------------------------------------------------------------
	// ハーフ⇒ワイド
	static inline const c32	HalfToWide(const c32 _chr)
	{
		return __PoisonUNCD_HalfToWide((c16)_chr);
	}
};

//*************************************************************************************************
// 文字列操作
//*************************************************************************************************
template<class S>
class	PoisonCStrCTRL
{
private:
	//----------------------------------------------------------------------------------------------
	// 次の文字まで進める
	static inline const b8	__Next(const c8* &_str)
	{
		return S::NextByString(_str);
	}

	//----------------------------------------------------------------------------------------------
	// 文字列を文字コードに変換
	static inline const c32	__StringToCode(const c8* &_str)
	{
		return S::StringToCode(_str);
	}

	// 文字コードを文字列に変換
	static inline const u32	__CodeToString(c8* &_str, const c32 _chr, const size_t _max = (size_t)(-1))
	{
		return S::CodeToString(_str, _chr, _max);
	}

	//----------------------------------------------------------------------------------------------
	// １文字コピー
	static inline const b8	__CopyChar(c8* &_dst, const c8* &_src, const size_t _max = (size_t)(-1))
	{
		if (!(*_src)) { return false; }

		const c8	*src = _src;
		if (!S::NextByString(src)) { return false; }

		size_t	len = src - _src;
		if (len >= _max) { return false; }

		while (len>0)
		{
			len--;
			*_dst = *_src;
			_src++;
			_dst++;
		}

		return true;
	}

	//----------------------------------------------------------------------------------------------
	// シングル文字か
	static inline const u32	__IsSingle(const c8 *_str)
	{
		return S::IsSingle(_str);
	}

	//----------------------------------------------------------------------------------------------
	// マルチ文字か
	static inline const u32	__IsMulti(const c8 *_str)
	{
		return S::IsMulti(_str);
	}

public:
	//----------------------------------------------------------------------------------------------
	static inline const b8	GetCode(const c8 *_str, c32 &_chr, size_t &_num)
	{
		if (_str)
		{
			const c8	*str = _str;
			_chr = __StringToCode(str);
			_num = str - _str;
			if (_num == 0)
			{
				// Alert("文字列の先頭を取得できませんでした。既定の文字コードか確認してください\n");
				return false;
			}
			return true;
		}
		else {
			_chr = 0;
			_num = 0;
			return false;
		}
	}

	//----------------------------------------------------------------------------------------------
	// 比較
	static inline const s32	Compare(const c8 *_dst, const c8 *_src)
	{
		if ((!_dst) || (!_src)) { return 0; }

		while ((*_dst) && (*_src))
		{
			const s32	cmp = (s32)__StringToCode(_dst) - (s32)__StringToCode(_src);
			if (cmp != 0) { return cmp; }
		}

		return (s32)__StringToCode(_dst) - (s32)__StringToCode(_src);
	}

	//----------------------------------------------------------------------------------------------
	// ハーフ ⇒ ワイド
	static inline const size_t	HalfToWide(c8 *_dst, const c8 *_src, const size_t _max)
	{
		const size_t	pos = _max + (size_t)_dst;
		const c8		*src = _src;
		c8				*dst = _dst;
		while (*src)
		{
			if (!S::CodeToString(dst, S::HalfToWide(__StringToCode(src)), pos - (size_t)dst)) { break; }
		}

		*dst = '\0';

		return dst - _dst;
	}

	//----------------------------------------------------------------------------------------------
	// ルビ削除
	static inline const size_t	DeleteRubi(c8 *_str_dst, const c8 *_str_src, const size_t _max = (size_t)(-1))
	{
		if (!_str_dst) { return 0; }
		if (_max <= 0) { return 0; }

		if ((!_str_src) || (!(*_str_src)))
		{
			*_str_dst = NULL;
			return 0;
		}

		size_t	ret = 0;

		const size_t	pos = _max + (size_t)_str_dst;
		const c8		*src = _str_src;
		c8				*dst = _str_dst;
		while (*src)
		{
			if ((__IsSingle(src)) && (*src == '['))
			{
				const c8	*sep = SearchCharN(src + 1, '/');
				const c8	*end = SearchCharN(src + 1, ']');
				if ((sep) && (end) && (sep<end))
				{
					src++;

					while (src<sep)
					{
						if (!__CopyChar(dst, src, pos - (size_t)dst)) { break; }
					}

					src = end + 1;
					ret++;
					continue;
				}
			}

			if (!__CopyChar(dst, src, pos - (size_t)dst)) { break; }
		}

		*dst = NULL;

		return ret;
	}

	//----------------------------------------------------------------------------------------------
	// ルビ本文削除
	static inline const size_t	DeleteRubiBody(c8 *_str_dst, const c8 *_str_src, const size_t _max = (size_t)(-1))
	{
		if (!_str_dst) { return 0; }
		if (_max <= 0) { return 0; }

		if ((!_str_src) || (!(*_str_src)))
		{
			*_str_dst = NULL;
			return 0;
		}

		size_t	ret = 0;

		const size_t	pos = _max + (size_t)_str_dst;
		const c8		*src = _str_src;
		c8				*dst = _str_dst;
		while (*src)
		{
			if ((__IsSingle(src)) && (*src == '['))
			{
				const c8	*start = src;
				const c8	*sep = SearchCharN(src + 1, '/');
				const c8	*end = SearchCharN(src + 1, ']');
				if ((start) && (sep) && (start<sep))
				{
					src = sep + 1;

					while (src<end)
					{
						if (!__CopyChar(dst, src, pos - (size_t)dst)) { break; }
					}

					src = end + 1;
					ret++;
					continue;
				}
			}

			if (!__CopyChar(dst, src, pos - (size_t)dst)) { break; }
		}

		*dst = NULL;

		return ret;
	}

	//----------------------------------------------------------------------------------------------
	// 文字検索
	static inline const c8*		SearchCharN(const c8 *_str, const c8 _val)
	{
		if ((!_str) || (!(*_str))) { return NULL; }
		if (!_val) { return NULL; }

		do
		{
			if (__IsSingle(_str))
			{
				if ((*_str) == _val) { return _str; }
				_str++;
			}
			else {
				__Next(_str);
			}
		} while (*_str);

		return NULL;
	}
};

typedef	PoisonCStrCTRL<PoisonCStrUTF8>	Poison_str_utf8;


//-------------------------------------------------------------------------------------------------
// 文字列同士を比較
const s32	PoisonString_Compare(const c8 *_dst, const c8 *_src)
{
	return PoisonUTF8_Compare(_dst, _src);
}
const s32	PoisonUTF8_Compare(const c8 *_dst, const c8 *_src) { return Poison_str_utf8::Compare(_dst, _src); }

//-------------------------------------------------------------------------------------------------
// 文字列内の半角を全角に変換
const size_t	CTextUtility::UTF8_HalfToWide(c8 *_dst, const c8 *_src, const size_t _max) { return Poison_str_utf8::HalfToWide(_dst, _src, _max); }


//-------------------------------------------------------------------------------------------------
// 文字コード取得
const b8	CTextUtility::GetStringCodeUTF8(const c8 *_str, c32 &_chr, size_t &_num) { return Poison_str_utf8::GetCode(_str, _chr, _num); }

//-------------------------------------------------------------------------------------------------
// 文字列内の指定文字を前から検索
const c8*	CTextUtility::SearchN(const c8 *_str, const c8 _val) { return Poison_str_utf8::SearchCharN(_str, _val); }

//-------------------------------------------------------------------------------------------------
// ルビ本文文字列を文字列から削除
const size_t	CTextUtility::DeleteRubiBody(c8 *_str_dst, const c8 *_str_src) { return Poison_str_utf8::DeleteRubiBody(_str_dst, _str_src); }



POISON_END

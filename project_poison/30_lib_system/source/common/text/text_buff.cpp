﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "text/text_buff.h"

POISON_BGN
///-------------------------------------------------------------------------------------------------
/// constant
const c8* CTextBuff::NULL_TEXT = "";	// 空文字列


///*************************************************************************************************
///	テキストバッファ管理クラス
///*************************************************************************************************
CTextBuff::CTextBuff()
	: m_buffReal(NULL)
	, m_buff(NULL)
	, m_buffSize(0)
	, m_usedBuffSize(0)
	, m_textCount(0)
	, m_isBuffFixed(false)
	, m_alignmentSize(1)
{
//	Initialize();
}

CTextBuff::~CTextBuff()
{
//	Finalize();
}

///-------------------------------------------------------------------------------------------------
/// 初期化
void	CTextBuff::Initialize()
{
	SetBuff(NULL, 0);
}

///-------------------------------------------------------------------------------------------------
/// 終了
void	CTextBuff::Finalize()
{
#ifdef DIRTY_VAL_CLEAR
	Clear(true, DIRTY_VAL_CLEAR);
#endif//DIRTY_VAL_CLEAR
	SetBuff(NULL, 0);
}
	
///-------------------------------------------------------------------------------------------------
/// バッファをセット
b8		CTextBuff::SetBuff(c8* _buff, const u32 _buff_size, const u8 _alignment_size)
{
	if(!_buff)
	{
		m_buffReal      = NULL;
		m_buff          = NULL;
		m_alignmentSize = 1;
		m_buffSize      = 0;
		m_usedBuffSize  = 0;
		m_textCount     = 0;
		m_isBuffFixed   = false;
		return true;
	}
	
	m_buffSize    = _buff_size;
	m_textCount   = 0;
	m_isBuffFixed = (m_buffSize > 0);
	
	if(_alignment_size <= 1)
	{
		m_buff = m_buffReal = _buff;
		m_alignmentSize = 1;
		m_usedBuffSize  = 0;
	}
	else
	{
		m_buffReal      = _buff;
		m_alignmentSize = _alignment_size;
		const u64 buff_i     = RCast<u64>(_buff);
		const u32 over_align = (buff_i % m_alignmentSize);
		if(over_align == 0)
		{
			m_usedBuffSize = 0;
			m_buff         = m_buffReal;
		}
		else
		{
			m_usedBuffSize = (m_alignmentSize - over_align);
			if(m_buffSize < m_usedBuffSize)
				m_usedBuffSize = m_buffSize;
			m_buff = m_buffReal + m_usedBuffSize;
			memset(m_buffReal, 0, m_usedBuffSize);
		}
	}
	if (m_buffSize - m_usedBuffSize > 0) {
		*m_buff = '\0';
	}
	
	return true;
}

///-------------------------------------------------------------------------------------------------
/// クリア
b8		CTextBuff::Clear(const b8 _fill_memory, const c8 _fill_data)
{
	if (!m_buff) {
		return false;
	}
	
	const u64 buff_real_addr = RCast<u64>(m_buffReal);
	const u64 buff_addr      = RCast<u64>(m_buff);
	
	m_usedBuffSize = SCast<u32>(buff_addr - buff_real_addr);
	m_textCount    = 0;
	
	if (_fill_memory && m_isBuffFixed) {
		memset(m_buffReal, _fill_data, m_buffSize);
	}
	
	if (m_usedBuffSize > 0) {
		memset(m_buffReal, 0x00, m_usedBuffSize);
	}
	
	return true;
}

///-------------------------------------------------------------------------------------------------
/// バッファサイズを確定する
b8	CTextBuff::Fix()
{
	if (!m_buff) {
		return false;
	}
	if (m_isBuffFixed) {
		return false;
	}
	m_isBuffFixed = true;
	return true;
}

///-------------------------------------------------------------------------------------------------
/// 文字列を追加
const c8*	CTextBuff::AddText(const c8* _text, const b8 _reuse, u32* _text_size, u32* _added_text_size)
{
	if (_text_size) {
		*_text_size = 0;
	}
	if (_added_text_size) {
		*_added_text_size = 0;
	}

	if (!m_buff) {
		return NULL;
	}
	if (!_text) {
		return NULL;
	}
	if (_text[0] == '\0') {
		return NULL_TEXT;
	}
	if (IsFull()) {
		return NULL;
	}

	const u32 text_size = static_cast<u32>(strlen(_text));
	if (_text_size) {
		*_text_size = text_size;
	}
	
	if(_reuse)
	{
		const c8* found_text = FindText(_text);
		if(found_text)
		{
			const u32 found_text_size = static_cast<u32>(strlen(found_text));
			if(_added_text_size)
				*_added_text_size = found_text_size;
			return found_text;
		}
	}
	
	const u32 remain_size = GetRemainSize();
	if(remain_size <= 0 || (remain_size == 1 && text_size >= 1))
	{
		PRINT_ERROR("*****[ERROR][TextBuff] メモリ不足につき、文字列が登録できませんでした。(%d/%d \"%s\")\n", remain_size - 1, text_size, _text);
		libAssert(0);
		return NULL;
	}
	u32 add_text_size = text_size;
	if(remain_size <= text_size)
	{
		add_text_size = remain_size - 1;
		PRINT_ERROR("*****[ERROR][TextBuff] メモリ不足につき、文字列が途中で切れた状態で登録されました。(%d/%d \"%s\")\n", remain_size - 1, text_size, _text);
		libAssert(0);
	}
	if (_added_text_size) {
		*_added_text_size = add_text_size;
	}
	
	c8* next_p = CCast<c8*>(GetUsedBuffNext());
	memcpy(next_p, _text, add_text_size);
	c8* term_p = next_p + add_text_size;
	*term_p = '\0';
	add_text_size ++;
	
	const u32 over_align = add_text_size % m_alignmentSize;
	const u32 add_text_size_org = add_text_size;
	if (over_align > 0) {
		add_text_size += (m_alignmentSize - over_align);
	}
	if (add_text_size > remain_size) {
		add_text_size = remain_size;
	}
	const u32 align_fill_size = add_text_size_org - add_text_size;
	if (align_fill_size > 0) {
		memset(term_p, 0, align_fill_size);
	}
	m_usedBuffSize += add_text_size;
	
	m_textCount ++;
	
	return next_p;
}

///-------------------------------------------------------------------------------------------------
/// 文字列セットをまるごと追加する
const c8*	CTextBuff::AddTextSet(const c8* _text_set, const u32 _text_set_size, const u32 _text_node_count)
{
	const u32 remain_size = GetRemainSize();
	if(remain_size < _text_set_size)
	{
		PRINT_ERROR("*****[ERROR][TextBuff] メモリ不足につき、文字列セットが登録できませんでした。(remain=%d/%d)\n", remain_size, _text_set_size);
		libAssert(0);
		return NULL;
	}
	
	c8* next_p = CCast<c8*>(GetUsedBuffNext());
	memcpy(next_p, _text_set, _text_set_size);
//	c8* term_p = next_p + _text_set_size;
//	*term_p = '\0';
	m_usedBuffSize += _text_set_size;
	m_textCount += _text_node_count;
	
	return next_p;
}

///-------------------------------------------------------------------------------------------------
/// 文字列バッファをコピーする
const c8*	CTextBuff::CopyAll(const c8* _text, const u32 _text_size, const u32 _text_node_count, const b8 _ignore_to_not_enough_buff)
{
	if (!m_buff) {
		return NULL;
	}
	
	if (!_text || _text_size == 0) {
		return m_buff;
	}
	
	u32 text_size = _text_size;
	if(m_buffSize > 0)
	{
		if(text_size > m_buffSize)
		{
			if(!_ignore_to_not_enough_buff)
			{
				PRINT_ERROR("*****[ERROR][TextBuff] メモリ不足につき、文字列セットがコピーできませんでした。(%d/%d)\n", m_buffSize, text_size);
				libAssert(0);
				return NULL;
			}
			text_size = m_buffSize - 1;
		}
	}
	memcpy(m_buff, _text, text_size);
	m_buff[text_size] = '\0';
	m_usedBuffSize = SCast<u32>(_text_size + (RCast<u64>(m_buff) - RCast<u64>(m_buffReal)));
	m_textCount = _text_node_count;
	
	return m_buff;
}

///-------------------------------------------------------------------------------------------------
/// 文字列を検索する
const c8*	CTextBuff::GetText(const u32 _num, const c8* _start_pos)
{
	if (!m_buff) {
		return NULL;
	}
	
	const u64 buff_addr_end = RCast<u64>(m_buffReal) + m_usedBuffSize - 1;
	
	const c8* start_pos = m_buff;
	if(_start_pos)
	{
		const u64 buff_addr_top  = RCast<u64>(m_buff);
		const u64 start_pos_addr = RCast<u64>(_start_pos);
		if (start_pos_addr < buff_addr_top || start_pos_addr > buff_addr_end) {
			return NULL;
		}
		start_pos = _start_pos;
	}
	
	const c8* search_pos = start_pos;
	for(u32 count = 0; count < _num; count ++)
	{
		if (RCast<u64>(search_pos) > buff_addr_end) {
			return NULL;
		}
		const size_t text_size = strlen(search_pos);
		search_pos += (text_size + 1);
		while (!*(search_pos) && RCast<u64>(search_pos) <= buff_addr_end) {
			search_pos++;
		}
	}
	if (RCast<u64>(search_pos) > buff_addr_end) {
		return NULL;
	}
	
	return search_pos;
}

///-------------------------------------------------------------------------------------------------
/// 文字列を検索する
/// ※指定の文字列と全く同一の文字列があれば、そのポインタを返す。
const c8*	CTextBuff::FindText(const c8* _text)
{
	if (!_text) {
		return NULL;
	}
	if (!m_buff) {
		return NULL;
	}
	if (_text[0] == '\0') {
		return NULL_TEXT;
	}
	
	const u64 buff_addr_end = RCast<u64>(m_buffReal) + m_usedBuffSize - 1;
	
	const c8* search_pos = m_buff;
	while(RCast<u64>(search_pos) <= buff_addr_end)
	{
		if (strcmp(search_pos, _text) == 0) {
			return search_pos;
		}
		const size_t text_size = strlen(search_pos);
		search_pos += (text_size + 1);
		while (!*(search_pos) && RCast<u64>(search_pos) <= buff_addr_end) {
			search_pos++;
		}
	}
	
	return NULL;
}

POISON_END


﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "resource/shader_library.h"



POISON_BGN


///*************************************************************************************************
/// Fxリソース
///*************************************************************************************************
CResFx::CResFx()
{
}

CResFx::~CResFx()
{
}

///-------------------------------------------------------------------------------------------------
///	解放処理
void	CResFx::Release()
{
	for (u32 i = 0; i < m_passNum; ++i)
	{
		m_pPassShader[i].Release();
		m_pPassShader[i].~PassShader();
	}
	for (u32 i = 0; i < m_tecNum; ++i)
	{
		m_pTecShader[i].Release();
		m_pTecShader[i].~TecShader();
	}
	MEM_SAFE_FREE(CShaderUtility::GetMemHdl(), m_pPassShader);
	MEM_SAFE_FREE(CShaderUtility::GetMemHdl(), m_pTecShader);
	m_passNum = 0;
	m_tecNum = 0;
	m_shader.Release();
}


///*************************************************************************************************
///	リソースシェーダクラス
///*************************************************************************************************
CResShader::CResShader()
{
}

CResShader::~CResShader()
{
}

///-------------------------------------------------------------------------------------------------
///	解放処理
void	CResShader::Release()
{
	for (u32 i = 0; i < m_tecNum; ++i)
	{
		for (u32 j = 0; j < m_pTec[i].comboNum; ++j)
		{
			MEM_SAFE_FREE(CShaderUtility::GetMemHdl(), m_pTec[i].pCombo[j].pPass);
		}
		MEM_SAFE_FREE(CShaderUtility::GetMemHdl(), m_pTec[i].pAttr);
		MEM_SAFE_FREE(CShaderUtility::GetMemHdl(), m_pTec[i].pCombo);
	}
	MEM_SAFE_FREE(CShaderUtility::GetMemHdl(), m_pTec);
	m_tecNum = 0;
}

///-------------------------------------------------------------------------------------------------
///	ソース構築済みか
b8		CResShader::IsEnable() const
{
	return (m_pTec != NULL);
}

///-------------------------------------------------------------------------------------------------
///	生成
b8		CResShader::Create(CResFx* _pFxRes, const VTX_DECL* _pDecl, u32 _declNum) const
{
	if (!IsEnable()) { return false; }

	// パスシェーダ数カウント
	u32 passCount = 0;
	u32 attrCount = 0;
	u32 comboCount = 0;
	for (u32 i = 0; i < m_tecNum; ++i)
	{
		for (u32 j = 0; j < m_pTec[i].comboNum; ++j)
		{
			passCount += m_pTec[i].pCombo[j].passNum;
		}
		attrCount += m_pTec[i].attrNum;
		comboCount += m_pTec[i].comboNum;
	}
	// パスシェーダ生成
	_pFxRes->m_pPassShader = MEM_MALLOC_TYPES(CShaderUtility::GetMemHdl(), PassShader, passCount);
	libAssert(_pFxRes->m_pPassShader);
	_pFxRes->m_passNum = passCount;
	// パスシェーダ構築
	PassShader* pPass = _pFxRes->m_pPassShader;
	for (u32 i = 0; i < m_tecNum; ++i)
	{
		for (u32 j = 0; j < m_pTec[i].comboNum; ++j)
		{
			for (u32 k = 0; k < m_pTec[i].pCombo[j].passNum; ++k, ++pPass)
			{
				const PASS& pass = m_pTec[i].pCombo[j].pPass[k];
				new(pPass) PassShader;
				PassShader::INIT_CRC_DESC desc;
				desc.vsNameCrc = pass.vsNameCrc;
				desc.gsNameCrc = pass.gsNameCrc;
				desc.psNameCrc = pass.psNameCrc;
				if (!pPass->Create(_pDecl, _declNum, desc, m_name))
				{
					return false;
				}
			}
		}
	}

	// 一時メモリに生成
	TecShader::ATTR* pAttr = PCast<TecShader::ATTR*>(alloca(sizeof(TecShader::ATTR) * attrCount));
	TecShader::COMBO* pCombo = PCast<TecShader::COMBO*>(alloca(sizeof(TecShader::COMBO) * comboCount));
	libAssert(pAttr && pCombo);

	// テクニックシェーダ生成
	_pFxRes->m_pTecShader = MEM_MALLOC_TYPES(CShaderUtility::GetMemHdl(), TecShader, m_tecNum);
	libAssert(_pFxRes->m_pTecShader);
	_pFxRes->m_tecNum = m_tecNum;

	// テクニックシェーダ構築
	passCount = 0;
	TecShader* pTec = _pFxRes->m_pTecShader;
	for (u32 i = 0; i < m_tecNum; ++i, ++pTec)
	{
		new(pTec) TecShader;
		for (u32 j = 0; j < m_pTec[i].attrNum; ++j)
		{
			pAttr[j].nameCrc = m_pTec[i].pAttr[j].nameCrc;
			pAttr[j].bit = m_pTec[i].pAttr[j].bit;
		}
		for (u32 j = 0; j < m_pTec[i].comboNum; ++j)
		{
			pCombo[j].combobit = m_pTec[i].pCombo[j].bit;
			pCombo[j].pPass = &_pFxRes->m_pPassShader[passCount];
			passCount += m_pTec[i].pCombo[j].passNum;
		}

		TecShader::INIT_DESC desc;
		desc.pAttrs = pAttr;
		desc.attrNum = m_pTec[i].attrNum;
		desc.pCombos = pCombo;
		desc.comboNum = m_pTec[i].comboNum;
		if (!pTec->Create(desc))
		{
			return false;
		}

		pAttr += m_pTec[i].attrNum;
		pCombo += m_pTec[i].comboNum;
	}

	// エフェクトシェーダ構築
	FxShader::INIT_DESC desc;
	desc.pTecShader = _pFxRes->m_pTecShader;
	desc.tecNum = _pFxRes->m_tecNum;
	if (!_pFxRes->m_shader.Create(desc, m_name))
	{
		return false;
	}

	return _pFxRes->IsEnable();
}




///*************************************************************************************************
///	シェーダライブラリ
///*************************************************************************************************

CShaderLibrary::CShaderLibrary()
	: m_memHdl(MEM_HDL_INVALID)
{
}
CShaderLibrary::~CShaderLibrary()
{
}

//-------------------------------------------------------------------------------------------------
///	初期化
b8		CShaderLibrary::Initialize(MEM_HANDLE _memHdl)
{
	m_memHdl = _memHdl;
	return true;
}

//-------------------------------------------------------------------------------------------------
///	終了
void	CShaderLibrary::Finalize()
{
	m_vsList.Finalize();
	m_gsList.Finalize();
	m_psList.Finalize();
	m_fxList.Finalize();
	m_resList.Finalize();
	m_pError = NULL;
}

///-------------------------------------------------------------------------------------------------
///	VS取得・検索
const VShader*	CShaderLibrary::GetVShader(u16 _idx) const
{
	if (_idx >= m_vsList.GetMaxNum()) { return NULL; }
	return &m_vsList.Get(_idx)->GetShader();
}
const VShader*	CShaderLibrary::SearchVShader(hash32 _nameCrc) const
{
	const ResVS* pRes = m_vsList.Search(_nameCrc);
	return pRes ? &pRes->GetShader() : NULL;
}

///-------------------------------------------------------------------------------------------------
///	GS取得・検索
const GShader*	CShaderLibrary::GetGShader(u16 _idx) const
{
	if (_idx >= m_gsList.GetMaxNum()) { return NULL; }
	return &m_gsList.Get(_idx)->GetShader();
}
const GShader*	CShaderLibrary::SearchGShader(hash32 _nameCrc) const
{
	const ResGS* pRes = m_gsList.Search(_nameCrc);
	return pRes ? &pRes->GetShader() : NULL;
}

///-------------------------------------------------------------------------------------------------
///	PS取得・検索
const PShader*	CShaderLibrary::GetPShader(u16 _idx) const
{
	if (_idx >= m_psList.GetMaxNum()) { return NULL; }
	return &m_psList.Get(_idx)->GetShader();
}
const PShader*	CShaderLibrary::SearchPShader(hash32 _nameCrc) const
{
	const ResPS* pRes = m_psList.Search(_nameCrc);
	return pRes ? &pRes->GetShader() : NULL;
}

///-------------------------------------------------------------------------------------------------
///	Fx取得・検索＆生成
const FxShader*	CShaderLibrary::GetFxShader(u16 _idx) const
{
	if (_idx >= m_fxList.GetMaxNum()) { return NULL; }
	return &m_fxList.Get(_idx)->GetShader();
}
const FxShader*	CShaderLibrary::SearchAndCreateFxShader(hash32 _nameCrc, const VTX_DECL* _pDecl, u32 _declNum)
{
	hash32 hash = FxShader::ConvAttrCrc(_nameCrc, _pDecl, _declNum);
	const CResFx* pResFx = m_fxList.Search(hash);
	if (pResFx == NULL)
	{
		const CResShader* pResShader = m_resList.Search(_nameCrc);
		if (pResShader)
		{
			CResFx* pEditResFx = m_fxList.Create(hash);
			if (pEditResFx)
			{
				if (pResShader->Create(pEditResFx, _pDecl, _declNum))
				{
					pResFx = pEditResFx;
					PRINT("□■□■ FxShader Create [%s]\n", pResShader->GetName());
				}
			}
		}
	}

	return pResFx ? &pResFx->GetShader() : NULL;
}


///-------------------------------------------------------------------------------------------------
///	VS追加
VShader*	CShaderLibrary::CreateVShader(hash32 _nameCrc)
{
	ResVS* pRes = m_vsList.Create(_nameCrc);
	return pRes ? &pRes->GetShader() : NULL;
}
VShader*	CShaderLibrary::CreateAndSearchVShader(hash32 _nameCrc)
{
	ResVS* pRes = m_vsList.SearchAndCreate(_nameCrc);
	return pRes ? &pRes->GetShader() : NULL;
}

///-------------------------------------------------------------------------------------------------
///	GS追加
GShader*	CShaderLibrary::CreateGShader(hash32 _nameCrc)
{
	ResGS* pRes = m_gsList.Create(_nameCrc);
	return pRes ? &pRes->GetShader() : NULL;
}
GShader*	CShaderLibrary::CreateAndSearchGShader(hash32 _nameCrc)
{
	ResGS* pRes = m_gsList.SearchAndCreate(_nameCrc);
	return pRes ? &pRes->GetShader() : NULL;
}

///-------------------------------------------------------------------------------------------------
///	PS追加
PShader*	CShaderLibrary::CreatePShader(hash32 _nameCrc)
{
	ResPS* pRes = m_psList.Create(_nameCrc);
	return pRes ? &pRes->GetShader() : NULL;
}
PShader*	CShaderLibrary::CreateAndSearchPShader(hash32 _nameCrc)
{
	ResPS* pRes = m_psList.SearchAndCreate(_nameCrc);
	return pRes ? &pRes->GetShader() : NULL;
}

///-------------------------------------------------------------------------------------------------
///	リソースシェーダ追加
CResShader*	CShaderLibrary::CreateResShader(hash32 _nameCrc)
{
	return m_resList.Create(_nameCrc);
}



POISON_END

﻿
///-------------------------------------------------------------------------------------------------
/// define
//#define USE_TEXT_SOURCE_SHADER


///-------------------------------------------------------------------------------------------------
/// include
#include "resource/shader_loader.h"
#include "resource/shader_library.h"

#include "file/file_loader.h"
#include "script/script_interpreter.h"

#include "text/text_utility.h"

POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///*************************************************************************************************
///	シェーダローダー
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// シェーダパス変換
const c8*	ConvShaderPath(const c8* _pPath)
{
	// 拡張子変更
	{
		c8* pExt = CCast<c8*>(GetFileExt(_pPath));
		hash32 extCrc = CRC32(pExt);
		if (SCRC32("vfxo") == extCrc)
		{
#if defined( LIB_GFX_OPENGL )
#ifdef USE_TEXT_SOURCE_SHADER
			pExt[0] = 'v';
			pExt[1] = 'e';
			pExt[2] = 'r';
			pExt[3] = 't';
			pExt[4] = '\0';
#else
			pExt[0] = 'v';
			pExt[1] = 's';
			pExt[2] = 'x';
			pExt[3] = '\0';
#endif // USE_TEXT_SOURCE_SHADER
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
			pExt[0] = 'v';
			pExt[1] = 's';
			pExt[2] = 'b';
			pExt[3] = '\0';
#endif // LIB_GFX_DX11
		}
		else if (SCRC32("gfxo") == extCrc)
		{
#if defined( LIB_GFX_OPENGL )
#ifdef USE_TEXT_SOURCE_SHADER
			pExt[0] = 'g';
			pExt[1] = 'e';
			pExt[2] = 'o';
			pExt[3] = 'm';
			pExt[4] = '\0';
#else
			pExt[0] = 'g';
			pExt[1] = 's';
			pExt[2] = 'x';
			pExt[3] = '\0';
#endif // USE_TEXT_SOURCE_SHADER
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
			pExt[0] = 'g';
			pExt[1] = 's';
			pExt[2] = 'b';
			pExt[3] = '\0';
#endif // LIB_GFX_DX11
		}
		else if (SCRC32("pfxo") == extCrc)
		{
#if defined( LIB_GFX_OPENGL )
#ifdef USE_TEXT_SOURCE_SHADER
			pExt[0] = 'f';
			pExt[1] = 'r';
			pExt[2] = 'a';
			pExt[3] = 'g';
			pExt[4] = '\0';
#else
			pExt[0] = 'p';
			pExt[1] = 's';
			pExt[2] = 'x';
			pExt[3] = '\0';
#endif // USE_TEXT_SOURCE_SHADER
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
			pExt[0] = 'p';
			pExt[1] = 's';
			pExt[2] = 'b';
			pExt[3] = '\0';
#endif // LIB_GFX_DX11
		}
		else if (SCRC32("cfxo") == extCrc)
		{
#if defined( LIB_GFX_OPENGL )
#ifdef USE_TEXT_SOURCE_SHADER
			pExt[0] = 'c';
			pExt[1] = 'o';
			pExt[2] = 'm';
			pExt[3] = 'p';
			pExt[4] = '\0';
#else
			pExt[0] = 'c';
			pExt[1] = 's';
			pExt[2] = 'x';
			pExt[3] = '\0';
#endif // USE_TEXT_SOURCE_SHADER
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
			pExt[0] = 'c';
			pExt[1] = 's';
			pExt[2] = 'b';
			pExt[3] = '\0';
#endif // LIB_GFX_DX11
		}
	}

	// タグ変更
	{
		u32 tagLen = 0;
		u32 argLen = 0;
		c8* pTag = CCast<c8*>(CTextUtility::CheckTag(_pPath, '<', '>', tagLen, argLen, TEXT_TYPE::TEXT_TYPE_SJIS));
		if (tagLen >= 5)
		{
#if defined( LIB_GFX_OPENGL )
#ifdef USE_TEXT_SOURCE_SHADER
			static const c8* s_gfxTag = "glsl";
#else
			static const c8* s_gfxTag = "spirv";
#endif // USE_TEXT_SOURCE_SHADER
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
			static const c8* s_gfxTag = "hlsl";
#endif // LIB_GFX_DX11
			u32 gfxTagLen = SCast<u32>(strlen(s_gfxTag));
			//strncpy_s(pTag, tagLen, s_gfxTag, gfxTagLen);
			memcpy(pTag, s_gfxTag, sizeof(c8) * gfxTagLen);
			for (u32 i = gfxTagLen; i < tagLen; ++i)
			{
				pTag[i] = '/';
			}
		}
	}
	return _pPath;
}

///-------------------------------------------------------------------------------------------------
/// シェーダ名取得
hash32	GetShaderNameCrc(const c8* _pPath)
{
	// ファイルパスからファイル名取得
	const u32 uPathLen = SCast<u32>(strlen(_pPath));
	const c8* pFileName = GetFileName(_pPath, uPathLen);
	const c8* pFileExt = GetFileExt(_pPath, uPathLen);

	// ファイル名コピー
	c8 nameBuff[64];
	strcpy_s(nameBuff, pFileName);
	nameBuff[pFileExt - pFileName - 1] = 0;
	// CRC32変換
	return CRC32(nameBuff);
}
hash32	GetShaderNameCrc(c8* _fileName, u32 _nameLen, const c8* _pPath)
{
	// ファイルパスからファイル名取得
	const u32 uPathLen = SCast<u32>(strlen(_pPath));
	const c8* pFileName = GetFileName(_pPath, uPathLen);
	const c8* pFileExt = GetFileExt(_pPath, uPathLen);

	// ファイル名コピー
	strcpy_s(_fileName, _nameLen, pFileName);
	_fileName[pFileExt - pFileName - 1] = 0;
	// CRC32変換
	return CRC32(_fileName);
}


///-------------------------------------------------------------------------------------------------
/// 読み込み時にのみ使用する情報
struct RUN_LIST_INFO_ST
{
	CShaderLibrary* pRes = NULL;
	CLinkList<CFileLoader*>	vsLoadList;
	CLinkList<CFileLoader*>	gsLoadList;
	CLinkList<CFileLoader*>	psLoadList;
	CLinkList<CFileLoader*>	csLoadList;
	CLinkList<CFileLoader*>	fxLoadList;
	MEM_HANDLE hdl;
	b8 isAsync = false;

	void		Release(CLinkList<CFileLoader*>& _list)
	{
		CLinkList<CFileLoader*>::CItr& pItr = _list.Begin();
		while (pItr != _list.End())
		{
			CFileLoader* pLoader = (*pItr);
			pLoader->Clear();
			MEM_DELETE(hdl, pLoader);
			++pItr;
		}
		_list.Finalize();
	}

	void		Finalize()
	{
		Release(vsLoadList);
		Release(gsLoadList);
		Release(psLoadList);
		Release(csLoadList);
		Release(fxLoadList);
	}
};

///-------------------------------------------------------------------------------------------------
/// 解析用クラス
class CShaderListDefInner
{
public:
	///-------------------------------------------------------------------------------------------------
	// シェーダ数タグ
	static s32 Tag_SHADER_NUM(SI_STACK _st)
	{
		RUN_LIST_INFO_ST* pRunInfo = PCast<RUN_LIST_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CShaderLibrary* pLibrary = pRunInfo->pRes;

		if (siGetStackRemain(_st) <= 0) { return 1; }
		s32 vsNum = siGetStackInt_inc(_st);

		if (siGetStackRemain(_st) <= 0) { return 1; }
		s32 gsNum = siGetStackInt_inc(_st);

		if (siGetStackRemain(_st) <= 0) { return 1; }
		s32 psNum = siGetStackInt_inc(_st);

		if (siGetStackRemain(_st) <= 0) { return 1; }
		s32 csNum = siGetStackInt_inc(_st);

		b8 ret = true;
		ret &= pLibrary->m_vsList.Initialize(MEM_ALLOCATOR(pLibrary->m_memHdl), vsNum);
		ret &= pLibrary->m_gsList.Initialize(MEM_ALLOCATOR(pLibrary->m_memHdl), gsNum);
		ret &= pLibrary->m_psList.Initialize(MEM_ALLOCATOR(pLibrary->m_memHdl), psNum);
		//ret &= pLibrary->m_csList.Initialize(MEM_ALLOCATOR(pLibrary->m_memHdl), csNum);

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// エフェクトシェーダ数タグ
	static s32 Tag_FXSHADER_NUM(SI_STACK _st)
	{
		RUN_LIST_INFO_ST* pRunInfo = PCast<RUN_LIST_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CShaderLibrary* pLibrary = pRunInfo->pRes;

		if (siGetStackRemain(_st) <= 0) { return 1; }
		s32 resNum = siGetStackInt_inc(_st);

		if (siGetStackRemain(_st) <= 0) { return 1; }
		s32 fxNum = resNum * siGetStackInt_inc(_st);

		b8 ret = true;
		ret &= pLibrary->m_resList.Initialize(MEM_ALLOCATOR(pLibrary->m_memHdl), resNum);
		ret &= pLibrary->m_fxList.Initialize(MEM_ALLOCATOR(pLibrary->m_memHdl), fxNum);

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// 非同期タグ
	static s32 Tag_ASYNC_LOAD(SI_STACK _st)
	{
		RUN_LIST_INFO_ST* pRunInfo = PCast<RUN_LIST_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CShaderLibrary* pLibrary = pRunInfo->pRes;

		if (siGetStackRemain(_st) <= 0) { return 1; }
		pRunInfo->isAsync = (siGetStackInt_inc(_st) != 0);

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// バーテックスシェーダタグ
	static s32 Tag_LOAD_VS_BGN(SI_STACK _st)
	{
		RUN_LIST_INFO_ST* pRunInfo = PCast<RUN_LIST_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CShaderLibrary* pLibrary = pRunInfo->pRes;

		return 1;
	}

	static s32 Tag_LOAD_VS(SI_STACK _st)
	{
		RUN_LIST_INFO_ST* pRunInfo = PCast<RUN_LIST_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CShaderLibrary* pLibrary = pRunInfo->pRes;

		// ファイルパス取得
		if (siGetStackRemain(_st) <= 0) { return 1; }
		const c8* pPath = siGetStackString_inc(_st);
		// パス変換
		pPath = ConvShaderPath(pPath);

		// ファイルロード
		CFileLoader* pLoader = MEM_NEW(pRunInfo->hdl)CFileLoader;
		libAssert(pLoader);
		if (!pLoader->Load(pPath, pRunInfo->hdl)) {
			PRINT_ERROR("*****[ERROR] Not Found Vertex Shader [%s]\n", pPath);
			return 0;
		}
		pRunInfo->vsLoadList.PushBack(pLoader);
		
		// シェーダ生成
		const c8* fileName = GetFileName(pPath);
		hash32 shaderNameCrc = GetShaderNameCrc(fileName);
		VShader* pShader = pLibrary->CreateVShader(shaderNameCrc);
		if (pShader)
		{
#ifdef USE_TEXT_SOURCE_SHADER
			if (!pShader->Compile(PCast<const c8*>(pLoader->GetBuffer()), pLoader->GetSize(), fileName))
#else
			if (!pShader->Create(pLoader->GetBuffer(), pLoader->GetSize(), fileName))
#endif // USE_TEXT_SOURCE_SHADER
			{
				PRINT_ERROR("*****[ERROR] Create Vertex Shader [%s]\n", pPath);
				return 0;
			}
		}

		return 1;
	}

	static s32 Tag_LOAD_VS_END(SI_STACK _st)
	{
		RUN_LIST_INFO_ST* pRunInfo = PCast<RUN_LIST_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CShaderLibrary* pLibrary = pRunInfo->pRes;

		// 初期化済みなら
		if (pLibrary->m_vsList.GetMaxNum() > 0)
		{
			// ローダー破棄
			pRunInfo->Release(pRunInfo->vsLoadList);
			return 1;
		}

		b8 ret = true;
		u32 shaderNum = pRunInfo->vsLoadList.GetNum();
		ret &= pLibrary->m_vsList.Initialize(MEM_ALLOCATOR(pLibrary->m_memHdl), shaderNum);

		CLinkList<CFileLoader*>::CItr& pItr = pRunInfo->vsLoadList.Begin();
		while (pItr != pRunInfo->vsLoadList.End())
		{
			CFileLoader* pLoader = (*pItr);

			// シェーダ生成
			const c8* pPath = pLoader->GetPath();
			const c8* fileName = GetFileName(pPath);
			hash32 shaderNameCrc = GetShaderNameCrc(fileName);
			VShader* pShader = pLibrary->CreateVShader(shaderNameCrc);
			if (pShader)
			{
#ifdef USE_TEXT_SOURCE_SHADER
				if (!pShader->Compile(PCast<const c8*>(pLoader->GetBuffer()), pLoader->GetSize(), fileName))
#else
				if (!pShader->Create(pLoader->GetBuffer(), pLoader->GetSize(), fileName))
#endif // USE_TEXT_SOURCE_SHADER
				{
					PRINT_ERROR("*****[ERROR] Create Vertex Shader [%s]\n", pPath);
					return 0;
				}
			}

			++pItr;
		}

		// ローダー破棄
		pRunInfo->Release(pRunInfo->vsLoadList);

		return 1;
	}


	///-------------------------------------------------------------------------------------------------
	// ジオメトリシェーダタグ
	static s32 Tag_LOAD_GS_BGN(SI_STACK _st)
	{
		RUN_LIST_INFO_ST* pRunInfo = PCast<RUN_LIST_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CShaderLibrary* pLibrary = pRunInfo->pRes;

		return 1;
	}

	static s32 Tag_LOAD_GS(SI_STACK _st)
	{
		RUN_LIST_INFO_ST* pRunInfo = PCast<RUN_LIST_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CShaderLibrary* pLibrary = pRunInfo->pRes;

		// ファイルパス取得
		if (siGetStackRemain(_st) <= 0) { return 1; }
		const c8* pPath = siGetStackString_inc(_st);
		// パス変換
		pPath = ConvShaderPath(pPath);

		// ファイルロード
		CFileLoader* pLoader = MEM_NEW(pRunInfo->hdl)CFileLoader;
		libAssert(pLoader);
		if (!pLoader->Load(pPath, pRunInfo->hdl)) {
			PRINT_ERROR("*****[ERROR] Not Found Geometory Shader [%s]\n", pPath);
			return 0;
		}
		pRunInfo->gsLoadList.PushBack(pLoader);

		// シェーダ生成
		const c8* fileName = GetFileName(pPath);
		hash32 shaderNameCrc = GetShaderNameCrc(fileName);
		GShader* pShader = pLibrary->CreateGShader(shaderNameCrc);
		if (pShader)
		{
#ifdef USE_TEXT_SOURCE_SHADER
			if (!pShader->Compile(PCast<const c8*>(pLoader->GetBuffer()), pLoader->GetSize(), fileName))
#else
			if (!pShader->Create(pLoader->GetBuffer(), pLoader->GetSize(), fileName))
#endif // USE_TEXT_SOURCE_SHADER
			{
				PRINT_ERROR("*****[ERROR] Create Geometory Shader [%s]\n", pPath);
				return 0;
			}
		}
		return 1;
	}

	static s32 Tag_LOAD_GS_END(SI_STACK _st)
	{
		RUN_LIST_INFO_ST* pRunInfo = PCast<RUN_LIST_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CShaderLibrary* pLibrary = pRunInfo->pRes;

		// 初期化済みなら
		if (pLibrary->m_gsList.GetMaxNum() > 0)
		{
			// ローダー破棄
			pRunInfo->Release(pRunInfo->gsLoadList);
			return 1;
		}

		b8 ret = true;
		u32 shaderNum = pRunInfo->gsLoadList.GetNum();
		ret &= pLibrary->m_gsList.Initialize(MEM_ALLOCATOR(pLibrary->m_memHdl), shaderNum);

		CLinkList<CFileLoader*>::CItr& pItr = pRunInfo->gsLoadList.Begin();
		while (pItr != pRunInfo->gsLoadList.End())
		{
			CFileLoader* pLoader = (*pItr);

			// シェーダ生成
			const c8* pPath = pLoader->GetPath();
			const c8* fileName = GetFileName(pPath);
			hash32 shaderNameCrc = GetShaderNameCrc(fileName);
			GShader* pShader = pLibrary->CreateGShader(shaderNameCrc);
			if (pShader)
			{
#ifdef USE_TEXT_SOURCE_SHADER
				if (!pShader->Compile(PCast<const c8*>(pLoader->GetBuffer()), pLoader->GetSize(), fileName))
#else
				if (!pShader->Create(pLoader->GetBuffer(), pLoader->GetSize(), fileName))
#endif // USE_TEXT_SOURCE_SHADER
				{
					PRINT_ERROR("*****[ERROR] Create Geometory Shader [%s]\n", pPath);
					return 0;
				}
			}

			++pItr;
		}

		// ローダー破棄
		pRunInfo->Release(pRunInfo->gsLoadList);

		return 1;
	}


	///-------------------------------------------------------------------------------------------------
	// ピクセルシェーダタグ
	static s32 Tag_LOAD_PS_BGN(SI_STACK _st)
	{
		RUN_LIST_INFO_ST* pRunInfo = PCast<RUN_LIST_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CShaderLibrary* pLibrary = pRunInfo->pRes;

		return 1;
	}

	static s32 Tag_LOAD_PS(SI_STACK _st)
	{
		RUN_LIST_INFO_ST* pRunInfo = PCast<RUN_LIST_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CShaderLibrary* pLibrary = pRunInfo->pRes;

		// ファイルパス取得
		if (siGetStackRemain(_st) <= 0) { return 1; }
		const c8* pPath = siGetStackString_inc(_st);
		// パス変換
		pPath = ConvShaderPath(pPath);

		// ファイルロード
		CFileLoader* pLoader = MEM_NEW(pRunInfo->hdl)CFileLoader;
		libAssert(pLoader);
		if (!pLoader->Load(pPath, pRunInfo->hdl)) {
			PRINT_ERROR("*****[ERROR] Not Found Pixel Shader [%s]\n", pPath);
			return 0;
		}
		pRunInfo->psLoadList.PushBack(pLoader);

		// シェーダ生成
		const c8* fileName = GetFileName(pPath);
		hash32 shaderNameCrc = GetShaderNameCrc(fileName);
		//c8 nameBuff[32];
		//hash32 shaderNameCrc = GetShaderNameCrc(nameBuff, ARRAYOF(nameBuff), GetFileName(pPath));
		PShader* pShader = pLibrary->CreatePShader(shaderNameCrc);
		if (pShader)
		{
#ifdef USE_TEXT_SOURCE_SHADER
			if (!pShader->Compile(PCast<const c8*>(pLoader->GetBuffer()), pLoader->GetSize(), fileName))
#else
			if (!pShader->Create(pLoader->GetBuffer(), pLoader->GetSize(), fileName))
#endif // USE_TEXT_SOURCE_SHADER
			{
				PRINT_ERROR("*****[ERROR] Create Pixel Shader [%s]\n", pPath);
				return 0;
			}
		}
		return 1;
	}

	static s32 Tag_LOAD_PS_END(SI_STACK _st)
	{
		RUN_LIST_INFO_ST* pRunInfo = PCast<RUN_LIST_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CShaderLibrary* pLibrary = pRunInfo->pRes;

		// 初期化済みなら
		if (pLibrary->m_psList.GetMaxNum() > 0)
		{
			// ローダー破棄
			pRunInfo->Release(pRunInfo->psLoadList);
			return 1;
		}

		b8 ret = true;
		u32 shaderNum = pRunInfo->psLoadList.GetNum();
		ret &= pLibrary->m_psList.Initialize(MEM_ALLOCATOR(pLibrary->m_memHdl), shaderNum);

		CLinkList<CFileLoader*>::CItr& pItr = pRunInfo->psLoadList.Begin();
		while (pItr != pRunInfo->psLoadList.End())
		{
			CFileLoader* pLoader = (*pItr);

			// シェーダ生成
			const c8* pPath = pLoader->GetPath();
			const c8* fileName = GetFileName(pPath);
			hash32 shaderNameCrc = GetShaderNameCrc(fileName);
			PShader* pShader = pLibrary->CreatePShader(shaderNameCrc);
			if (pShader)
			{
#ifdef USE_TEXT_SOURCE_SHADER
				if (!pShader->Compile(PCast<const c8*>(pLoader->GetBuffer()), pLoader->GetSize(), fileName))
#else
				if (!pShader->Create(pLoader->GetBuffer(), pLoader->GetSize(), fileName))
#endif // USE_TEXT_SOURCE_SHADER
				{
					PRINT_ERROR("*****[ERROR] Create Vertex Pixel [%s]\n", pPath);
					return 0;
				}
			}

			++pItr;
		}

		// ローダー破棄
		pRunInfo->Release(pRunInfo->psLoadList);

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// コンピュートシェーダタグ
	static s32 Tag_LOAD_CS_BGN(SI_STACK _st)
	{
		RUN_LIST_INFO_ST* pRunInfo = PCast<RUN_LIST_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CShaderLibrary* pLibrary = pRunInfo->pRes;

		return 1;
	}

	static s32 Tag_LOAD_CS(SI_STACK _st)
	{
		RUN_LIST_INFO_ST* pRunInfo = PCast<RUN_LIST_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CShaderLibrary* pLibrary = pRunInfo->pRes;

		// ファイルパス取得
		if (siGetStackRemain(_st) <= 0) { return 1; }
		const c8* pPath = siGetStackString_inc(_st);
		// パス変換
		pPath = ConvShaderPath(pPath);

		// ファイルロード
		CFileLoader* pLoader = MEM_NEW(pRunInfo->hdl)CFileLoader;
		libAssert(pLoader);
		if (!pLoader->Load(pPath, pRunInfo->hdl)) {
			PRINT_ERROR("*****[ERROR] Not Found Compute Shader [%s]\n", pPath);
			return 0;
		}
		pRunInfo->csLoadList.PushBack(pLoader);

		return 1;
	}

	static s32 Tag_LOAD_CS_END(SI_STACK _st)
	{
		RUN_LIST_INFO_ST* pRunInfo = PCast<RUN_LIST_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CShaderLibrary* pLibrary = pRunInfo->pRes;

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// エフェクトシェーダタグ
	static s32 Tag_LOAD_FX_BGN(SI_STACK _st)
	{
		RUN_LIST_INFO_ST* pRunInfo = PCast<RUN_LIST_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CShaderLibrary* pLibrary = pRunInfo->pRes;

		return 1;
	}

	static s32 Tag_LOAD_FX(SI_STACK _st)
	{
		RUN_LIST_INFO_ST* pRunInfo = PCast<RUN_LIST_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CShaderLibrary* pLibrary = pRunInfo->pRes;

		// ファイルパス取得
		if (siGetStackRemain(_st) <= 0) { return 1; }
		const c8* pPath = siGetStackString_inc(_st);

		// ファイルロード
		CFileLoader* pLoader = MEM_NEW(pRunInfo->hdl)CFileLoader;
		libAssert(pLoader);
		if (!pLoader->Load(pPath, pRunInfo->hdl)) {
			PRINT_ERROR("*****[ERROR] Not Found Technic Shader [%s]\n", pPath);
			return 0;
		}
		pRunInfo->fxLoadList.PushBack(pLoader);

		// シェーダ生成
		const c8* fileName = GetFileName(pPath);
		hash32 shaderNameCrc = GetShaderNameCrc(fileName);
		CResShader* pShader = pLibrary->CreateResShader(shaderNameCrc);
		if (pShader)
		{
			if (!CShaderLoader::BuildResource(pShader, pLoader->GetBuffer(), pLoader->GetSize(), pRunInfo->hdl))
			{
				PRINT_ERROR("*****[ERROR] Create Fx Shader [%s]\n", pPath);
				return 0;
			}
			// 名前保持
			pShader->SetName(fileName);
		}
		return 1;
	}

	static s32 Tag_LOAD_FX_END(SI_STACK _st)
	{
		RUN_LIST_INFO_ST* pRunInfo = PCast<RUN_LIST_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CShaderLibrary* pLibrary = pRunInfo->pRes;

		// FXシェーダの倍率数
		if (siGetStackRemain(_st) <= 0) { return 1; }
		s32 fxShaderMulNum = siGetStackInt_inc(_st);

		// 初期化済みなら
		if (pLibrary->m_fxList.GetMaxNum() > 0) { return 1; }

		b8 ret = true;
		u32 shaderNum = pRunInfo->fxLoadList.GetNum();
		ret &= pLibrary->m_resList.Initialize(MEM_ALLOCATOR(pLibrary->m_memHdl), shaderNum);
		ret &= pLibrary->m_fxList.Initialize(MEM_ALLOCATOR(pLibrary->m_memHdl), shaderNum * fxShaderMulNum);

		CLinkList<CFileLoader*>::CItr& pItr = pRunInfo->fxLoadList.Begin();
		while (pItr != pRunInfo->fxLoadList.End())
		{
			CFileLoader* pLoader = (*pItr);

			// シェーダ生成
			const c8* pPath = pLoader->GetPath();
			const c8* fileName = GetFileName(pPath);
			hash32 shaderNameCrc = GetShaderNameCrc(fileName);
			CResShader* pShader = pLibrary->CreateResShader(shaderNameCrc);
			if (pShader)
			{
				if (!CShaderLoader::BuildResource(pShader, pLoader->GetBuffer(), pLoader->GetSize(), pRunInfo->hdl))
				{
					PRINT_ERROR("*****[ERROR] Create Fx Shader [%s]\n", pPath);
					return 0;
				}
				// 名前保持
				pShader->SetName(fileName);
			}

			++pItr;
		}

		// ローダー破棄
		pRunInfo->Release(pRunInfo->fxLoadList);

		return 1;
	}

};



///-------------------------------------------------------------------------------------------------
/// 読み込み時にのみ使用する情報
struct ATTR_INFO
{
	const c8*	attrName = NULL;
	hash32		attrCrc;
	u16			bit = 0;
};
struct PASS_INFO
{
	hash32	vsNameCrc;
	hash32	gsNameCrc;
	hash32	psNameCrc;
};
struct COMBO_INFO
{
	CLinkList<PASS_INFO>	passInfoList;
	u16						bit = 0;
};
struct TEC_INFO
{
	CLinkList<ATTR_INFO>	attrInfoList;
	CLinkList<COMBO_INFO>	comboInfoList;
	const c8* tecName = NULL;
};
struct RUN_FX_INFO_ST
{
	CResShader*			pRes = NULL;
	CLinkList<TEC_INFO>	tecInfoList;
	MEM_HANDLE			hdl;


	void		Finalize()
	{
		CLinkList<TEC_INFO>::CItr tecItr = tecInfoList.Begin();
		while (tecItr != tecInfoList.End())
		{
			TEC_INFO& rTec = (*tecItr);

			rTec.attrInfoList.Finalize();

			CLinkList<COMBO_INFO>::CItr comboItr = rTec.comboInfoList.Begin();
			while (comboItr != rTec.comboInfoList.End())
			{
				(*comboItr).passInfoList.Finalize();
				++comboItr;
			}
			rTec.comboInfoList.Finalize();

			++tecItr;
		}
		tecInfoList.Finalize();
	}
};

///-------------------------------------------------------------------------------------------------
/// 解析用クラス
class CShaderFxDefInner
{
public:
	///-------------------------------------------------------------------------------------------------
	// テクニック開始タグ
	static s32 Tag_TEC_BGN(SI_STACK _st)
	{
		RUN_FX_INFO_ST* pRunInfo = PCast<RUN_FX_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CResShader* pRes = pRunInfo->pRes;

		TEC_INFO tecInfo;
		// テクニック名取得
		if (siGetStackRemain(_st) <= 0) { return 1; }
		tecInfo.tecName = siGetStackString_inc(_st);
		tecInfo.attrInfoList.Initialize(pRunInfo->hdl);
		tecInfo.comboInfoList.Initialize(pRunInfo->hdl);
		pRunInfo->tecInfoList.PushBack(tecInfo);

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// テクニック終了タグ
	static s32 Tag_TEC_END(SI_STACK _st)
	{
		RUN_FX_INFO_ST* pRunInfo = PCast<RUN_FX_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CResShader* pRes = pRunInfo->pRes;

#ifndef POISON_RELEASE
		// コンボシェーダのビットがかぶってないかチェック
		CLinkList<TEC_INFO>::CItr tecItr = pRunInfo->tecInfoList.ReBegin();
		TEC_INFO& rTecInfo = (*tecItr);
		CLinkList<COMBO_INFO>::CItr comboItr = rTecInfo.comboInfoList.Begin();
		while (comboItr != rTecInfo.comboInfoList.End())
		{
			CLinkList<COMBO_INFO>::CItr compItr = comboItr;
			++compItr;
			while (compItr != rTecInfo.comboInfoList.End())
			{
				libAssert((*comboItr).bit != (*compItr).bit);
				++compItr;
			}
			++comboItr;
		}
#endif // !POISON_RELEASE


		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// 属性開始タグ
	static s32 Tag_ATTR_BGN(SI_STACK _st)
	{
		RUN_FX_INFO_ST* pRunInfo = PCast<RUN_FX_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CResShader* pRes = pRunInfo->pRes;

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// 属性タグ
	static s32 Tag_ATTR(SI_STACK _st)
	{
		RUN_FX_INFO_ST* pRunInfo = PCast<RUN_FX_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CResShader* pRes = pRunInfo->pRes;

		CLinkList<TEC_INFO>::CItr itr = pRunInfo->tecInfoList.ReBegin();
		TEC_INFO& rTecInfo = (*itr);

		ATTR_INFO attrInfo;
		// 属性名
		if (siGetStackRemain(_st) <= 0) { return 1; }
		attrInfo.attrName = siGetStackString_inc(_st);
		attrInfo.attrCrc = CRC32(attrInfo.attrName);
		attrInfo.bit = (1 << rTecInfo.attrInfoList.GetNum());
		rTecInfo.attrInfoList.PushBack(attrInfo);

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// 属性終了タグ
	static s32 Tag_ATTR_END(SI_STACK _st)
	{
		RUN_FX_INFO_ST* pRunInfo = PCast<RUN_FX_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CResShader* pRes = pRunInfo->pRes;

#ifndef POISON_RELEASE
		// 属性名がかぶってないかチェック
		CLinkList<TEC_INFO>::CItr tecItr = pRunInfo->tecInfoList.ReBegin();
		TEC_INFO& rTecInfo = (*tecItr);
		CLinkList<ATTR_INFO>::CItr attrItr = rTecInfo.attrInfoList.Begin();
		while (attrItr != rTecInfo.attrInfoList.End())
		{
			CLinkList<ATTR_INFO>::CItr compItr = attrItr;
			++compItr;
			while (compItr != rTecInfo.attrInfoList.End())
			{
				libAssert((*attrItr).attrCrc != (*compItr).attrCrc);
				libAssert((*attrItr).bit != (*compItr).bit);
				++compItr;
			}
			++attrItr;
		}
#endif // !POISON_RELEASE

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// コンボ開始タグ
	static s32 Tag_COMBO_BGN(SI_STACK _st)
	{
		RUN_FX_INFO_ST* pRunInfo = PCast<RUN_FX_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CResShader* pRes = pRunInfo->pRes;

		CLinkList<TEC_INFO>::CItr itr = pRunInfo->tecInfoList.ReBegin();
		TEC_INFO& rTecInfo = (*itr);

		COMBO_INFO comboInfo;
		// コンボインデックス
		if (siGetStackRemain(_st) <= 0) { return 1; }
		comboInfo.bit = siGetStackInt_inc(_st);
		comboInfo.passInfoList.Initialize(pRunInfo->hdl);
		rTecInfo.comboInfoList.PushBack(comboInfo);

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// コンボ終了タグ
	static s32 Tag_COMBO_END(SI_STACK _st)
	{
		RUN_FX_INFO_ST* pRunInfo = PCast<RUN_FX_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CResShader* pRes = pRunInfo->pRes;

		if (siGetStackRemain(_st) <= 0) { return 1; }

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// パス開始タグ
	static s32 Tag_PASS_BGN(SI_STACK _st)
	{
		RUN_FX_INFO_ST* pRunInfo = PCast<RUN_FX_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CResShader* pRes = pRunInfo->pRes;

		CLinkList<TEC_INFO>::CItr tecItr = pRunInfo->tecInfoList.ReBegin();
		TEC_INFO& rTecInfo = (*tecItr);
		CLinkList<COMBO_INFO>::CItr comboItr = rTecInfo.comboInfoList.ReBegin();
		COMBO_INFO& rComboInfo = (*comboItr);
		PASS_INFO passInfo;
		rComboInfo.passInfoList.PushBack(passInfo);

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// パス終了タグ
	static s32 Tag_PASS_END(SI_STACK _st)
	{
		RUN_FX_INFO_ST* pRunInfo = PCast<RUN_FX_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CResShader* pRes = pRunInfo->pRes;

#ifndef POISON_RELEASE
		// 頂点シェーダとピクセルシェーダがあるかチェック
		CLinkList<TEC_INFO>::CItr tecItr = pRunInfo->tecInfoList.ReBegin();
		TEC_INFO& rTecInfo = (*tecItr);
		CLinkList<COMBO_INFO>::CItr comboItr = rTecInfo.comboInfoList.ReBegin();
		COMBO_INFO& rComboInfo = (*comboItr);
		CLinkList<PASS_INFO>::CItr passItr = rComboInfo.passInfoList.ReBegin();
		PASS_INFO& rPassInfo = (*passItr);
		libAssert(rPassInfo.vsNameCrc != hash32::Invalid && rPassInfo.psNameCrc != hash32::Invalid)
#endif // !POISON_RELEASE

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// 頂点シェーダタグ
	static s32 Tag_VS(SI_STACK _st)
	{
		RUN_FX_INFO_ST* pRunInfo = PCast<RUN_FX_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CResShader* pRes = pRunInfo->pRes;

		CLinkList<TEC_INFO>::CItr tecItr = pRunInfo->tecInfoList.ReBegin();
		TEC_INFO& rTecInfo = (*tecItr);
		CLinkList<COMBO_INFO>::CItr comboItr = rTecInfo.comboInfoList.ReBegin();
		COMBO_INFO& rComboInfo = (*comboItr);
		CLinkList<PASS_INFO>::CItr passItr = rComboInfo.passInfoList.ReBegin();
		PASS_INFO& rPassInfo = (*passItr);
		// シェーダ名取得
		if (siGetStackRemain(_st) <= 0) { return 1; }
		const c8* pName = siGetStackString_inc(_st);
		rPassInfo.vsNameCrc = CRC32(pName);

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// ジオメトリシェーダタグ
	static s32 Tag_GS(SI_STACK _st)
	{
		RUN_FX_INFO_ST* pRunInfo = PCast<RUN_FX_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CResShader* pRes = pRunInfo->pRes;

		CLinkList<TEC_INFO>::CItr tecItr = pRunInfo->tecInfoList.ReBegin();
		TEC_INFO& rTecInfo = (*tecItr);
		CLinkList<COMBO_INFO>::CItr comboItr = rTecInfo.comboInfoList.ReBegin();
		COMBO_INFO& rComboInfo = (*comboItr);
		CLinkList<PASS_INFO>::CItr passItr = rComboInfo.passInfoList.ReBegin();
		PASS_INFO& rPassInfo = (*passItr);
		// シェーダ名取得
		if (siGetStackRemain(_st) <= 0) { return 1; }
		const c8* pName = siGetStackString_inc(_st);
		rPassInfo.gsNameCrc = CRC32(pName);

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// ピクセルシェーダタグ
	static s32 Tag_PS(SI_STACK _st)
	{
		RUN_FX_INFO_ST* pRunInfo = PCast<RUN_FX_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CResShader* pRes = pRunInfo->pRes;

		CLinkList<TEC_INFO>::CItr tecItr = pRunInfo->tecInfoList.ReBegin();
		TEC_INFO& rTecInfo = (*tecItr);
		CLinkList<COMBO_INFO>::CItr comboItr = rTecInfo.comboInfoList.ReBegin();
		COMBO_INFO& rComboInfo = (*comboItr);
		CLinkList<PASS_INFO>::CItr passItr = rComboInfo.passInfoList.ReBegin();
		PASS_INFO& rPassInfo = (*passItr);
		// シェーダ名取得
		if (siGetStackRemain(_st) <= 0) { return 1; }
		const c8* pName = siGetStackString_inc(_st);
		rPassInfo.psNameCrc = CRC32(pName);

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// タグ
	static s32 Tag_STATE(SI_STACK _st)
	{
		RUN_FX_INFO_ST* pRunInfo = PCast<RUN_FX_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		CResShader* pRes = pRunInfo->pRes;

		if (siGetStackRemain(_st) <= 0) { return 1; }

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// リソースシェーダ構築
	static b8	BuildResShader(CResShader* _pRes, const RUN_FX_INFO_ST* _pRun)
	{
		_pRes->m_pTec = MEM_MALLOC_TYPES(CShaderUtility::GetMemHdl(), CResShader::TECHNIC, _pRun->tecInfoList.GetNum());
		if (_pRes->m_pTec == NULL) {
			libAssert(0);
			return false;
		}
		_pRes->m_tecNum = _pRun->tecInfoList.GetNum();
		CLinkList<TEC_INFO>::CItr tecItr = _pRun->tecInfoList.Begin();
		CResShader::TECHNIC* pTec = _pRes->m_pTec;
		for (u32 tecIdx = 0; tecIdx < _pRes->m_tecNum; ++tecIdx, ++pTec)
		{
			TEC_INFO& rTec = (*tecItr);
			new(pTec) CResShader::TECHNIC;
			// 属性生成（※ある場合だけ）
			pTec->pAttr = MEM_MALLOC_TYPES(CShaderUtility::GetMemHdl(), CResShader::ATTR, rTec.attrInfoList.GetNum());
			if (pTec->pAttr)
			{
				pTec->attrNum = rTec.attrInfoList.GetNum();
				CLinkList<ATTR_INFO>::CItr attrItr = rTec.attrInfoList.Begin();
				CResShader::ATTR* pAttr = pTec->pAttr;
				for (u32 attrIdx = 0; attrIdx < pTec->attrNum; ++attrIdx, ++pAttr)
				{
					new(pAttr) CResShader::ATTR;
					ATTR_INFO& rAttr = (*attrItr);
					pAttr->nameCrc = rAttr.attrCrc;
					pAttr->bit = rAttr.bit;
					++attrItr;
				}
				libAssert(attrItr == rTec.attrInfoList.End());
			}

			// コンボ生成
			pTec->pCombo = MEM_MALLOC_TYPES(CShaderUtility::GetMemHdl(), CResShader::COMBO, rTec.comboInfoList.GetNum());
			if (pTec->pCombo == NULL) {
				libAssert(0);
				return false;
			}
			pTec->comboNum = rTec.comboInfoList.GetNum();
			CLinkList<COMBO_INFO>::CItr comboItr = rTec.comboInfoList.Begin();
			CResShader::COMBO* pCombo = pTec->pCombo;
			for (u32 comboIdx = 0; comboIdx < pTec->comboNum; ++comboIdx, ++pCombo)
			{
				COMBO_INFO& rCombo = (*comboItr);
				new(pCombo) CResShader::COMBO;
				pCombo->bit = rCombo.bit;

				// パス生成
				pCombo->pPass = MEM_MALLOC_TYPES(CShaderUtility::GetMemHdl(), CResShader::PASS, rCombo.passInfoList.GetNum());
				if (pCombo->pPass == NULL) {
					libAssert(0);
					return false;
				}
				pCombo->passNum = rCombo.passInfoList.GetNum();
				CLinkList<PASS_INFO>::CItr passItr = rCombo.passInfoList.Begin();
				CResShader::PASS* pPass = pCombo->pPass;
				for (u32 passIdx = 0; passIdx < pCombo->passNum; ++passIdx, ++pPass)
				{
					PASS_INFO rPass = (*passItr);
					new(pPass) CResShader::PASS;
					pPass->vsNameCrc = rPass.vsNameCrc;
					pPass->gsNameCrc = rPass.gsNameCrc;
					pPass->psNameCrc = rPass.psNameCrc;
					++passItr;
				}
				libAssert(passItr == rCombo.passInfoList.End());

				++comboItr;
			}
			libAssert(comboItr == rTec.comboInfoList.End());

			++tecItr;
		}
		libAssert(tecItr == _pRun->tecInfoList.End());
		return true;
	}
};



///-------------------------------------------------------------------------------------------------
/// 設定ファイル解析関数リスト
SI_TAG_TABLE_INSTANCE_BEGIN(s_shaderListBuildTagList)[] =
{
	SI_TAG_C2(SHADER_NUM,	CShaderListDefInner, Tag_),
	SI_TAG_C2(FXSHADER_NUM,	CShaderListDefInner, Tag_),
	SI_TAG_C2(ASYNC_LOAD,	CShaderListDefInner, Tag_),
	SI_TAG_C2(LOAD_VS_BGN,	CShaderListDefInner, Tag_),
	SI_TAG_C2(LOAD_VS,		CShaderListDefInner, Tag_),
	SI_TAG_C2(LOAD_VS_END,	CShaderListDefInner, Tag_),
	SI_TAG_C2(LOAD_GS_BGN,	CShaderListDefInner, Tag_),
	SI_TAG_C2(LOAD_GS,		CShaderListDefInner, Tag_),
	SI_TAG_C2(LOAD_GS_END,	CShaderListDefInner, Tag_),
	SI_TAG_C2(LOAD_PS_BGN,	CShaderListDefInner, Tag_),
	SI_TAG_C2(LOAD_PS,		CShaderListDefInner, Tag_),
	SI_TAG_C2(LOAD_PS_END,	CShaderListDefInner, Tag_),
	SI_TAG_C2(LOAD_CS_BGN,	CShaderListDefInner, Tag_),
	SI_TAG_C2(LOAD_CS,		CShaderListDefInner, Tag_),
	SI_TAG_C2(LOAD_CS_END,	CShaderListDefInner, Tag_),
	SI_TAG_C2(LOAD_FX_BGN,	CShaderListDefInner, Tag_),
	SI_TAG_C2(LOAD_FX,		CShaderListDefInner, Tag_),
	SI_TAG_C2(LOAD_FX_END,	CShaderListDefInner, Tag_),

	SI_TAG_TERM()
};
SI_TAG_TABLE_INSTANCE_END(s_shaderListBuildTagList);

SI_TAG_TABLE_INSTANCE_BEGIN(s_shaderFxBuildTagList)[] =
{
	SI_TAG_C2(TEC_BGN,		CShaderFxDefInner, Tag_),
	SI_TAG_C2(TEC_END,		CShaderFxDefInner, Tag_),
	SI_TAG_C2(ATTR_BGN,		CShaderFxDefInner, Tag_),
	SI_TAG_C2(ATTR,			CShaderFxDefInner, Tag_),
	SI_TAG_C2(ATTR_END,		CShaderFxDefInner, Tag_),
	SI_TAG_C2(COMBO_BGN,	CShaderFxDefInner, Tag_),
	SI_TAG_C2(COMBO_END,	CShaderFxDefInner, Tag_),
	SI_TAG_C2(PASS_BGN,		CShaderFxDefInner, Tag_),
	SI_TAG_C2(PASS_END,		CShaderFxDefInner, Tag_),
	SI_TAG_C2(VS,			CShaderFxDefInner, Tag_),
	SI_TAG_C2(GS,			CShaderFxDefInner, Tag_),
	SI_TAG_C2(PS,			CShaderFxDefInner, Tag_),
	SI_TAG_C2(STATE,		CShaderFxDefInner, Tag_),

	SI_TAG_TERM()
};
SI_TAG_TABLE_INSTANCE_END(s_shaderFxBuildTagList);


//-------------------------------------------------------------------------------------------------
///@brief	コマンドファイルからメモリ定義ファイル生成
///@return						成功か
///@param[in]	_pRes			生成リソース
///@param[in]	_fileBuf		コマンドファイルバッファ
///@param[in]	_fileSize		コマンドファイルサイズ
///@param[in]	_workMemHdl		一時アロケータ
///@note					
b8	CShaderLoader::BuildResource(CShaderLibrary* _pRes, const void* _fileBuf, u32 _fileSize, MEM_HANDLE _workMemHdl)
{
	// 無効
	if (_pRes == NULL || _fileBuf == NULL || _fileSize <= 0)
	{
		return false;
	}

	RUN_LIST_INFO_ST runInfo;
	runInfo.pRes = _pRes;
	runInfo.hdl = _workMemHdl;
	runInfo.vsLoadList.Initialize(_workMemHdl);
	runInfo.gsLoadList.Initialize(_workMemHdl);
	runInfo.psLoadList.Initialize(_workMemHdl);
	runInfo.csLoadList.Initialize(_workMemHdl);
	runInfo.fxLoadList.Initialize(_workMemHdl);

	// スクリプトインタプリタを作成
	CScriptInterpreter si;
	si.SetTag(SI_SET_TAG_PARAM(s_shaderListBuildTagList));
	si.SetExtParam(&runInfo);
	si.SetScript(_fileBuf, _fileSize);

	// 解析
	b8 succeeded = si.Run();

	// 解放
	runInfo.Finalize();

	return succeeded;
}


b8	CShaderLoader::BuildResource(CResShader* _pRes, const void* _fileBuf, u32 _fileSize, MEM_HANDLE _workMemHdl)
{
	// 無効
	if (_pRes == NULL || _fileBuf == NULL || _fileSize <= 0)
	{
		return false;
	}

	RUN_FX_INFO_ST runInfo;
	runInfo.pRes = _pRes;
	runInfo.hdl = _workMemHdl;
	runInfo.tecInfoList.Initialize(_workMemHdl);

	// スクリプトインタプリタを作成
	CScriptInterpreter si;
	si.SetTag(SI_SET_TAG_PARAM(s_shaderFxBuildTagList));
	si.SetExtParam(&runInfo);
	si.SetScript(_fileBuf, _fileSize);

	// 解析
	b8 succeeded = si.Run();

	// リソースシェーダ構築
	succeeded &= CShaderFxDefInner::BuildResShader(_pRes, &runInfo);

	// 解放
	runInfo.Finalize();

	return succeeded;
}


POISON_END

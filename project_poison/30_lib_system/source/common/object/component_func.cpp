﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "object/component_func.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///*************************************************************************************************
///	
///*************************************************************************************************

CComponentFunc*	CComponentFunc::s_pTop[] = {};
u32				CComponentFunc::s_num[] = {};

CComponentFunc::CComponentFunc(const c8* _className, STEP_TYPE _stepType, s32 _priority)
{
	s32 priority[STEP_TYPE::STEP_TYPE_NUM];
	for (u32 i = 0; i < STEP_TYPE::STEP_TYPE_NUM; i++)
	{
		priority[i] = _priority;
	}
	new(this)CComponentFunc(_className, _stepType, priority);
}
CComponentFunc::CComponentFunc(const c8* _className, STEP_TYPE _stepType, const s32* _pPriority)
{
	for (u32 typeIdx = 0; typeIdx < STEP_TYPE::STEP_TYPE_NUM; ++typeIdx)
	{
		STEP_TYPE_IDX stepTypeIdx = SCast<STEP_TYPE_IDX>(typeIdx);
		STEP_TYPE stepType = SCast<STEP_TYPE>(1u << stepTypeIdx);
		if (_stepType & stepType)
		{
			AddCompFunc(this, stepTypeIdx, _pPriority[typeIdx]);
		}
	}
	m_pTypeName = _className;
	m_typeNameCrc = CRC32(_className);
	m_stepType = _stepType;

}

CComponentFunc::~CComponentFunc()
{
	for (u32 typeIdx = 0; typeIdx < STEP_TYPE::STEP_TYPE_NUM; ++typeIdx)
	{
		STEP_TYPE_IDX stepTypeIdx = SCast<STEP_TYPE_IDX>(typeIdx);
		if (m_stepType & (1u << stepTypeIdx))
		{
			DelCompFunc(this, stepTypeIdx);
		}
	}
}

///-------------------------------------------------------------------------------------------------
/// コンポーネント追加
void		CComponentFunc::AddComp(CComponent* _pComp)
{
	libAssert(_pComp);
	// コンポーネントリストに追加
	if (m_pCompTop)
	{
		m_pCompEnd->SetNext(_pComp);
		_pComp->SetPrev(m_pCompEnd);
	}
	else
	{
		m_pCompTop = _pComp;
	}
	m_pCompEnd = _pComp;
	m_compNum++;
}

///-------------------------------------------------------------------------------------------------
/// コンポーネント削除
void		CComponentFunc::DelComp(CComponent* _pComp)
{
	libAssert(_pComp);
	libAssert(m_compNum > 0);

#ifdef POISON_DEBUG
	// ちゃんとリストにある中のコンポーネントかチェック
	{
		b8 check = false;
		CComponent* pComp = GetCompTop();
		while (pComp)
		{
			if (pComp == _pComp)
			{
				check = true;
				break;
			}
			pComp = pComp->GetNext();
		}
		libAssert(check);
		if (!check) { return; }
	}
#endif // POISON_DEBUG

	CComponent* pPrev = _pComp->GetPrev();
	CComponent* pNext = _pComp->GetNext();
	if (m_pCompTop == _pComp)
	{
		m_pCompTop = pNext;
	}
	if (m_pCompEnd == _pComp)
	{
		m_pCompEnd = pPrev;
	}
	if (pPrev)
	{
		pPrev->SetNext(pNext);
	}
	if (pNext)
	{
		pNext->SetPrev(pPrev);
	}
	m_compNum--;
}


///-------------------------------------------------------------------------------------------------
/// コンポーネント関数追加
void		CComponentFunc::AddCompFunc(CComponentFunc* _pCompFunc, STEP_TYPE_IDX _stepTypeIdx, s32 _priority)
{
	libAssert(_pCompFunc);
	// コンポーネント関数リストに追加
	if (s_pTop[_stepTypeIdx])
	{
		CComponentFunc* pCompFunc = s_pTop[_stepTypeIdx];
		while (pCompFunc && pCompFunc->GetNext(_stepTypeIdx))
		{
			// プライオリティソート
			if (pCompFunc->GetPriority(_stepTypeIdx) < _priority)
			{
				break;
			}
			pCompFunc = pCompFunc->GetNext(_stepTypeIdx);
		}
		pCompFunc->SetNext(_pCompFunc, _stepTypeIdx);
	}
	else
	{
		s_pTop[_stepTypeIdx] = _pCompFunc;
	}
	// プライオリティ設定
	_pCompFunc->SetPriority(_priority, _stepTypeIdx);
	s_num[_stepTypeIdx]++;
}

///-------------------------------------------------------------------------------------------------
/// コンポーネント関数削除
void		CComponentFunc::DelCompFunc(CComponentFunc* _pCompFunc, STEP_TYPE_IDX _stepTypeIdx)
{
	libAssert(_pCompFunc);
	libAssert(GetNum(_stepTypeIdx) > 0);

	CComponentFunc* pCompFunc = s_pTop[_stepTypeIdx];
	if (pCompFunc == _pCompFunc)
	{
		// 先頭ならその次を先頭に切り替える
		s_pTop[_stepTypeIdx] = pCompFunc->GetNext(_stepTypeIdx);
	}
	else
	{
		while (pCompFunc)
		{
			CComponentFunc* pPrev = pCompFunc;
			pCompFunc = pCompFunc->GetNext(_stepTypeIdx);
			// 自分と同じのが見つかるまでループ
			if (pCompFunc == _pCompFunc)
			{
				pPrev->SetNext(pCompFunc->GetNext(_stepTypeIdx), _stepTypeIdx);
				break;
			}
		}
	}
	s_num[_stepTypeIdx]--;
}


POISON_END


﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "object/component.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///*************************************************************************************************
///	
///*************************************************************************************************

CComponent::CComponent()
{
}

CComponent::~CComponent()
{
}

///-------------------------------------------------------------------------------------------------
/// オブジェクトとリンクしたときのコールバック
void	CComponent::OnLinkObject()
{
	CComponentFunc* pCompFunc = GetCompnentFunc();
	libAssert(pCompFunc);
	pCompFunc->AddComp(this);
}

///-------------------------------------------------------------------------------------------------
/// 終了
void	CComponent::Finalize()
{
	CComponentFunc* pCompFunc = GetCompnentFunc();
	libAssert(pCompFunc);
	pCompFunc->DelComp(this);
}

///-------------------------------------------------------------------------------------------------
/// ステップ関数取得
template<>
CComponent::STEP_FUNC	CComponent::GetStepFunc<CComponent::PRE_STEP>()
{
	return &CComponent::PreStep;
}
template<>
CComponent::STEP_FUNC	CComponent::GetStepFunc<CComponent::STEP>()
{
	return &CComponent::Step;
}
template<>
CComponent::STEP_FUNC	CComponent::GetStepFunc<CComponent::POST_STEP>()
{
	return &CComponent::PostStep;
}
template<>
CComponent::STEP_FUNC	CComponent::GetStepFunc<CComponent::PREPARE_DRAW>()
{
	return &CComponent::PrepareDraw;
}
template<>
CComponent::STEP_FUNC	CComponent::GetStepFunc<CComponent::DRAW>()
{
	return &CComponent::Draw;
}


///-------------------------------------------------------------------------------------------------
/// 構築の関数取得
template<>
CComponent::BUILD_FUNC	CComponent::GetBuildFunc<CComponent::PRE_STEP>()
{
	return &CComponent::PreBuild;
}
template<>
CComponent::BUILD_FUNC	CComponent::GetBuildFunc<CComponent::STEP>()
{
	return &CComponent::Build;
}
template<>
CComponent::BUILD_FUNC	CComponent::GetBuildFunc<CComponent::POST_STEP>()
{
	return &CComponent::PostBuild;
}
template<>
CComponent::BUILD_FUNC	CComponent::GetBuildFunc<CComponent::PREPARE_DRAW>()
{
	return NULL;
}
template<>
CComponent::BUILD_FUNC	CComponent::GetBuildFunc<CComponent::DRAW>()
{
	return NULL;
}

POISON_END

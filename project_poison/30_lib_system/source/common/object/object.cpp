﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "object/object.h"

#include "object/property.h"
#include <functional>


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant
CObject*	CObject::s_pRoot = NULL;					///< ルートオブジェクト
CObject*	CObject::s_pDeleteObjectTop = NULL;			///< 削除予約リストトップ
CObject*	CObject::s_pDeleteObjectEnd = NULL;			///< 削除予約リストエンド


///*************************************************************************************************
///	オブジェクトクラス
///*************************************************************************************************
CObject::CObject()
{
}

CObject::~CObject()
{
}

///-------------------------------------------------------------------------------------------------
/// 終了
void	CObject::Finalize()
{
	// プロパティ破棄
	DeletePropertyAll();
}

///-------------------------------------------------------------------------------------------------
/// 子供追加
void	CObject::AddChild(CObject* _obj)
{
	if (m_pChildTop)
	{
		// 末っ子にする
		m_pChildEnd->m_pBrotherNext = _obj;
		_obj->m_pBrotherPrev = m_pChildEnd;
	}
	else
	{
		// 長男
		m_pChildTop = _obj;
	}
	m_pChildEnd = _obj;
	_obj->m_pParent = this;
	m_childNum++;
}

///-------------------------------------------------------------------------------------------------
/// 子供削除
void	CObject::DelChild(CObject* _obj)
{
	libAssert(_obj->m_pParent == this);
	libAssert(m_childNum > 0);

	CObject* pBrotherPrev = _obj->m_pBrotherPrev;
	CObject* pBrotherNext = _obj->m_pBrotherNext;

	// 長男なら次男に譲る
	if (m_pChildTop == _obj)
	{
		m_pChildTop = pBrotherNext;
	}
	// 末っ子なら上の兄弟に譲る
	if (m_pChildEnd == _obj)
	{
		m_pChildEnd = pBrotherPrev;
	}
	// 兄弟の縁削除
	if (pBrotherNext)
	{
		pBrotherNext->m_pBrotherPrev = pBrotherPrev;
	}
	if (pBrotherPrev)
	{
		pBrotherPrev->m_pBrotherNext = pBrotherNext;
	}
	m_childNum--;
}


//-------------------------------------------------------------------------------------------------
//@brief	次プロパティ取得
CProperty*			CObject::GetNextProperty(CProperty* _pProp)
{
	return _pProp->GetObjLinkNext();
}


///-------------------------------------------------------------------------------------------------
/// プロパティ生成
CProperty*	CObject::CreateProperty(hash32 _propNameCrc, MEM_HANDLE _memHdl/* = MEM_HDL_INVALID*/)
{
	CProperty* pProp = NULL;
	CPropertyCreator* pPropCreater = CPropertyCreator::Search(_propNameCrc);
	if (pPropCreater)
	{
		if (_memHdl == MEM_HDL_INVALID) { _memHdl = m_memHdl; }
		pProp = CPropertyCreator::Create(_memHdl, pPropCreater);
		if (pProp)
		{
			pProp->SetObject(this);
			AddLinkProperty(pProp);
			pProp->OnLinkObject();
		}
	}
	return pProp;
}

///-------------------------------------------------------------------------------------------------
/// プロパティ検索
CProperty*	CObject::SearchProperty(hash32 _propNameCrc)
{
	CProperty* pProp = m_pPropTop;
	while (pProp)
	{
		if (pProp->GetTypeNameCrc() == _propNameCrc)
		{
			return pProp;
		}
		pProp = pProp->GetObjLinkNext();
	}
	return NULL;
}

///-------------------------------------------------------------------------------------------------
/// プロパティ検索＆生成
CProperty*	CObject::SearchAndCreateProperty(hash32 _propNameCrc, MEM_HANDLE _memHdl/* = MEM_HDL_INVALID*/)
{
	CProperty* pProp = SearchProperty(_propNameCrc);
	if (pProp == NULL)
	{
		pProp = CreateProperty(_propNameCrc, _memHdl);
	}
	return pProp;
}

///-------------------------------------------------------------------------------------------------
/// プロパティ破棄
void		CObject::DeleteProperty(CProperty* _pProp)
{
	DelLinkProperty(_pProp);
	_pProp->SetObject(NULL);
	CPropertyCreator::Delete(_pProp);
}

void		CObject::DeletePropertyAll()
{
	CProperty* pProp = m_pPropTop;
	while (pProp)
	{
		CProperty* pNext = pProp->GetObjLinkNext();
		// 削除
		DeleteProperty(pProp);
		pProp = pNext;
	}
}


///-------------------------------------------------------------------------------------------------
/// プロパティに対してのメッセージ送信
b8			CObject::SendMessage(hash32 _crc, const void* _argv[]/* = NULL*/, u32 _argc/* = 0*/) const
{
	b8 ret = true;	// １つでもfalseがあれば
	CProperty* pProp = m_pPropTop;
	while (pProp)
	{
		// メッセージ送信
		if (pProp->IsEnableOnMessage())
		{
			ret &= pProp->OnMessage(_crc, _argv, _argc);
		}

		pProp = pProp->GetObjLinkNext();
	}
	return ret;
}

b8			CObject::SendMessage(hash32 _crc, const void* _argv[]/* = NULL*/, u32 _argc/* = 0*/)
{
	b8 ret = true;	// １つでもfalseがあれば
	CProperty* pProp = m_pPropTop;
	while (pProp)
	{
		// メッセージ送信
		if (pProp->IsEnableOnMessage())
		{
			ret &= pProp->OnMessage(_crc, _argv, _argc);
		}

		pProp = pProp->GetObjLinkNext();
	}
	return ret;
}


///-------------------------------------------------------------------------------------------------
/// プロパティリンク追加
void		CObject::AddLinkProperty(CProperty* _pProp)
{
	libAssert(_pProp);
	// プロパティリストに追加
	if (m_pPropTop)
	{
		m_pPropEnd->SetObjLinkNext(_pProp);
		_pProp->SetObjLinkPrev(m_pPropEnd);
	}
	else
	{
		m_pPropTop = _pProp;
	}
	m_pPropEnd = _pProp;
	m_propNum++;
}

///-------------------------------------------------------------------------------------------------
/// プロパティリンク削除
void		CObject::DelLinkProperty(CProperty* _pProp)
{
	libAssert(_pProp);
	libAssert(m_propNum > 0);

	CProperty* pPrev = _pProp->GetObjLinkPrev();
	CProperty* pNext = _pProp->GetObjLinkNext();
	if (m_pPropTop == _pProp)
	{
		m_pPropTop = pNext;
	}
	if (m_pPropEnd == _pProp)
	{
		m_pPropEnd = pPrev;
	}
	if (pPrev)
	{
		pPrev->SetObjLinkNext(pNext);
	}
	if (pNext)
	{
		pNext->SetObjLinkPrev(pPrev);
	}
	m_propNum--;
}


///-------------------------------------------------------------------------------------------------
/// 行列取得
const CMtx34&	CObject::GetLocalMtx()
{
	return m_srt.GetCalcMtx(m_mtx);
}

///*************************************************************************************************
///	オブジェクトユーティリティクラス
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// 初期化
b8			CObjectUtility::Initialize()
{
	static CObject s_root;
	s_root.SetName("root");
	s_root.SetNameCrc(CRC32("root"));
	s_root.SetIgnoreParentFlag(true);
	s_root.SetIgnoreParentDelete(true);
	// ルートオブジェクト設定
	CObject::s_pRoot = &s_root;
	return true;
}

///-------------------------------------------------------------------------------------------------
/// 終了
void		CObjectUtility::Finalize()
{
	// 再帰削除関数
	std::function<void(CObject*)> Recursive = [&](CObject* _obj)
	{
		// 子孫共々根絶やしに
		CObject* pChild = _obj->m_pChildTop;
		while (pChild)
		{
			CObject* pNext = pChild->m_pBrotherNext;

			// 再帰
			Recursive(pChild);

			pChild = pNext;
		}

		if (_obj != CObject::s_pRoot)
		{
			// 独り立ちする
			_obj->m_pParent->DelChild(_obj);
			FinDelete(_obj);
		}
	};
	Recursive(CObject::s_pRoot);
}

///-------------------------------------------------------------------------------------------------
/// オブジェクト作成
CObject*	CObjectUtility::Create(MEM_HANDLE _memHdl, const c8* _name, CObject* _parent/* = NULL*/)
{
	// todo: 生成処理
	CObject* pObj = MEM_MALLOC_TYPE(_memHdl, CObject);	// 一旦指定のメモリハンドルで
	new(pObj) CObject;
	pObj->m_memHdl = _memHdl;
	// 名前設定
	pObj->SetName(_name);
	pObj->SetNameCrc(CRC32(_name));

	// 子供として転生
	if (_parent == NULL)
	{
		_parent = CObject::s_pRoot;
	}
	libAssert(_parent);
	_parent->AddChild(pObj);

	return pObj;
}

///-------------------------------------------------------------------------------------------------
/// 最終破棄
void		CObjectUtility::FinDelete(CObject* _obj)
{
	// 終了処理
	_obj->Finalize();
	// todo: 削除処理
	_obj->~CObject();
	MEM_FREE(_obj->m_memHdl, _obj);	// 一旦指定のメモリハンドルで
}

///-------------------------------------------------------------------------------------------------
/// オブジェクト削除
void		CObjectUtility::Delete(CObject* _obj, b8 _isImediate/* = false*/)
{
	libAssert(CObject::s_pRoot != _obj);

	// 子孫共々根絶やしに
	CObject* pChild = _obj->m_pChildTop;
	while (pChild)
	{
		CObject* pNext = pChild->m_pBrotherNext;
		// 親と連動して削除
		if (!pChild->IsIgnoreParentDelete())
		{
			// 子削除
			Delete(pChild, _isImediate);
		}
		// 祖父母の子供になる
		else
		{
			libAssert(_obj->m_pParent);
			_obj->DelChild(pChild);
			_obj->m_pParent->AddChild(pChild);
		}

		pChild = pNext;
	}

	// 独り立ちする
	_obj->m_pParent->DelChild(_obj);

	if (_isImediate)
	{
		FinDelete(_obj);
	}
	else
	{
		// 削除リストに追加
		if (CObject::s_pDeleteObjectTop)
		{
			CObject::s_pDeleteObjectEnd->m_pDeleteNext = _obj;
		}
		else
		{
			CObject::s_pDeleteObjectTop = _obj;
		}
		CObject::s_pDeleteObjectEnd = _obj;

		// 削除フラグ設定
		_obj->SetFlag(CObject::FLAG_DELETE, true);
	}
}

///-------------------------------------------------------------------------------------------------
/// 削除リストに登録されてるオブジェクト削除
void		CObjectUtility::DeleteExecute()
{
	// 削除リスト総なめ
	CObject* pObj = CObject::s_pDeleteObjectTop;
	while (pObj)
	{
		CObject* pNext = pObj->m_pDeleteNext;

		// 破棄
		FinDelete(pObj);

		pObj = pNext;
	}
	CObject::s_pDeleteObjectTop = NULL;
	CObject::s_pDeleteObjectEnd = NULL;
}



POISON_END


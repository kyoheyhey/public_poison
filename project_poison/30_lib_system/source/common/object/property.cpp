﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "object/property.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///*************************************************************************************************
///	
///*************************************************************************************************

CProperty::CProperty()
{
}

CProperty::~CProperty()
{
}




///*************************************************************************************************
///	
///*************************************************************************************************

CPropertyCreator::CPropertyCreator(FUNC_CREATE _func, const c8* _className)
	: m_func(_func)
	, m_pTypeName(_className)
{
	m_typeNameCrc = CRC32(_className);
}

CPropertyCreator::~CPropertyCreator()
{
}


///-------------------------------------------------------------------------------------------------
/// プロパティ生成クラスの検索
CPropertyCreator*	CPropertyCreator::Search(hash32 _classNameCrc)
{
	CPropertyCreator* pNode = CPropertyCreator::GetNodeTop();
	while (pNode)
	{
		if (pNode->m_typeNameCrc == _classNameCrc)
		{
			return pNode;
		}
		pNode = pNode->GetNodeNext();
	}
	return NULL;
}

///-------------------------------------------------------------------------------------------------
/// プロパティ生成
CProperty*			CPropertyCreator::Create(MEM_HANDLE _memHdl, const CPropertyCreator* _pCreater)
{
	libAssert(_pCreater);
	CProperty* pProp = _pCreater->m_func(_memHdl);
	if (pProp)
	{
		// 自分の名前を教えてあげる
		pProp->SetTypeName(_pCreater->m_pTypeName);
		pProp->SetTypeNameCrc(_pCreater->m_typeNameCrc);
	}
	return pProp;
}

///-------------------------------------------------------------------------------------------------
/// プロパティ破棄
void				CPropertyCreator::Delete(CProperty* _pProp)
{
	libAssert(_pProp);
	_pProp->Finalize();
	MEM_DELETE(_pProp->GetMemHdl(), _pProp);
}


POISON_END

﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "common/text/text_utility.h"
#include <windows.h>


POISON_BGN

u32		ConvSJIStoUTF8(const u32 sjis_code)
{
	if(!(sjis_code & 0xff80)) {	return sjis_code; }

	u32 ret = 63;			// 「？」コード
	const u8 sjis_u8[2] = { SCast<u8>((sjis_code >> 8) & 0xff), SCast<u8>((sjis_code) & 0xff) };
	c8 sjis[2] = { *PCast<const c8*>( sjis_u8 ), *PCast<const c8*>( sjis_u8 + 1 ) };
	c16	utf16[2];
	u32 iLenUnicode = ::MultiByteToWideChar(CP_ACP, 0, RCast<LPCSTR>(sjis), 2, (LPWSTR)utf16, ARRAYOF(utf16));
	if( iLenUnicode <= 0) { return ret; }


	u8 utf8[5] = { 0x00, 0x00, 0x00, 0x00, 0x00 };
	int iLenUtf8 = WideCharToMultiByte(CP_UTF8, 0, utf16, iLenUnicode, NULL, 0, NULL, NULL);

	if (iLenUtf8 <= 5)
	{
		u32 size = WideCharToMultiByte(CP_UTF8, 0, utf16, iLenUnicode, RCast<LPSTR>(utf8), 5, NULL, NULL);
		if(size == 1) { ret = utf8[0]; }
		else if(size == 2) { ret = (utf8[0] << 8) + utf8[1]; }
		else if(size == 3) { ret = (utf8[0] << 16) + (utf8[1] << 8) + utf8[2]; }
		else if(size == 4) { ret = (utf8[0] << 24) + (utf8[1] << 16) + (utf8[2] << 8) + utf8[3]; }
	}

	return ret;
}

u32		ConvUTF8toSJIS(const u32 utf8_code)
{
	// アスキーコード部重複
	if(!(utf8_code & 0xff80)) { return utf8_code; }

	const u32 ret = 63u;	//< '?'

	if (utf8_code & 0xff000000)
	{
		return ret; //< '?'
	}

	const u8 utf8_u8[4] =
	{
		SCast<u8>((utf8_code >> 24) & 0xff),
		SCast<u8>((utf8_code >> 16) & 0xff),
		SCast<u8>((utf8_code >> 8) & 0xff),
		SCast<u8>(utf8_code & 0xff),
	};

	c8 utf8[4] = {};
	const u8* utf8_u8p = utf8_u8;
	c8* utf8p = utf8;
	for(u32 i = 0u; i < 4u; ++i, ++utf8_u8p){ if(*utf8_u8p) { *utf8p++ = *PCast<const c8*>(utf8_u8p); } }

	s32 wide_length = ::MultiByteToWideChar(CP_UTF8, 0, RCast<LPCSTR>(utf8), -1, NULL, 0);
	if(wide_length <= 0) { return ret; }

	u8 utf16[8] = {};
	wide_length = ::MultiByteToWideChar(CP_UTF8, 0, RCast<LPCSTR>(utf8), -1, RCast<LPWSTR>(utf16), wide_length);
	if(wide_length <= 0) { return ret; }

	s32 sjis_length = ::WideCharToMultiByte(CP_ACP, 0, RCast<LPCWSTR>(utf16), -1, NULL, 0, NULL, NULL);

	u8 sjis[3] = {};
	sjis_length = ::WideCharToMultiByte(CP_ACP, 0, RCast<LPCWSTR>(utf16), -1, RCast<LPSTR>(sjis), sjis_length, NULL, NULL);
	if(sjis_length <= 2) { return sjis_length; }

	return CTextUtility::IsSJISChar1of2(*sjis) ? SCast<u32>((*sjis << 8) | *(sjis + 1)) : SCast<u32>(*sjis);
}

POISON_END

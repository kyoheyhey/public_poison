﻿//#pragma once
#ifndef __COMPONENT_FUNC_INL__
#define __COMPONENT_FUNC_INL__


POISON_BGN


///-------------------------------------------------------------------------------------------------
/// タイプステップALL呼び出し
template < CComponentFunc::STEP_TYPE __TYPE, CComponentFunc::STEP_TYPE_IDX __TYPE_IDX >
void	CComponentFunc::CallStepAll()
{
	CComponentFunc* pCompFunc = GetTop(__TYPE_IDX);
	while (pCompFunc)
	{
		// ステップ呼び出し
		pCompFunc->CallStep<__TYPE>();

		pCompFunc = pCompFunc->GetNext(__TYPE_IDX);
	}
}

///-------------------------------------------------------------------------------------------------
/// タイプステップ呼び出し
template < CComponentFunc::STEP_TYPE __TYPE >
void	CComponentFunc::CallStep()
{
	if (GetBgnFunc())
	{
		if (!GetBgnFunc()(this, __TYPE))
		{
			return;
		}
	}

	CComponent* pComp = GetCompTop();
	while (pComp)
	{
		// ステップ呼び出し
		if (pComp->IsEnable())
		{
			if (!pComp->IsBuildEnable())
			{
				(pComp->*(CComponent::GetStepFunc<__TYPE>()))();
			}
			else if (CComponent::ALL_STEP & __TYPE)
			{
				// 構築ステップ呼び出し
				b8 ret = (pComp->*(CComponent::GetBuildFunc<__TYPE>()))();
				pComp->SetBuildEnable(ret);
			}
		}

		pComp = pComp->GetNext();
	}

	if (GetEndFunc())
	{
		GetEndFunc()(this, __TYPE);
	}
}


POISON_END

#endif	// __COMPONENT_FUNC_INL__

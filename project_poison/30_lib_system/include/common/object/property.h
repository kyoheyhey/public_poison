﻿//#pragma once
#ifndef __PROPERTY_H__
#define __PROPERTY_H__

//-------------------------------------------------------------------------------------------------
// include
#include "object.h"

//-------------------------------------------------------------------------------------------------
// prototype


POISON_BGN


//*************************************************************************************************
//@brief	プロパティクラス
//*************************************************************************************************
class CProperty : public CDontCopy
{
public:
	friend class CObject;
	friend class CPropertyCreator;

public:
	CProperty();
	~CProperty();

	//-------------------------------------------------------------------------------------------------
	//@brief	オブジェクト取得
	const CObject*	GetObject() const { return m_pObject; }
	CObject*		GetObject() { return m_pObject; }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	タイプ名取得・設定
	const c8*		GetTypeName() const { return m_pTypeName; }
	hash32			GetTypeNameCrc() const { return m_typeNameCrc; }
	void			SetTypeName(const c8* _pName) { m_pTypeName = _pName; }
	void			SetTypeNameCrc(hash32 _nameCrc) { m_typeNameCrc = _nameCrc; }

protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	オブジェクト設定
	void			SetObject(CObject* _pObj) { m_pObject = _pObj; }

	//-------------------------------------------------------------------------------------------------
	//@brief	オブジェクトリンク前ノード取得・設定
	const CProperty*	GetObjLinkPrev() const { return m_pObjLinkPrevProp; }
	CProperty*			GetObjLinkPrev() { return m_pObjLinkPrevProp; }
	void				SetObjLinkPrev(CProperty* _pProp) { m_pObjLinkPrevProp = _pProp; }
	//@brief	オブジェクトリンク後ノード取得・設定
	const CProperty*	GetObjLinkNext() const { return m_pObjLinkNextProp; }
	CProperty*			GetObjLinkNext() { return m_pObjLinkNextProp; }
	void				SetObjLinkNext(CProperty* _pProp) { m_pObjLinkNextProp = _pProp; }

	//-------------------------------------------------------------------------------------------------
	//@brief	メモリハンドル取得・設定
	MEM_HANDLE		GetMemHdl() const { return m_memHdl; }
	void			SetMemHdl(MEM_HANDLE _memHdl) { m_memHdl = _memHdl; }

	//-------------------------------------------------------------------------------------------------
	//@brief	メッセージ呼び出し取得・設定
	b8				IsEnableOnMessage() const { return m_enableOnMessage; }
	void			SetEnableOnMessage(b8 _enable) { m_enableOnMessage = _enable; }

protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	オブジェクトとリンクしたときのコールバック
	virtual void	OnLinkObject() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	メッセージ呼び出し、戻り値は呼び出しが成功したかどうか
	virtual b8		OnMessage(hash32 _crc, const void* _argv[], u32 _argc) { return CCast<const CProperty*>(this)->OnMessage(_crc, _argv, _argc); }
	//@brief	メッセージ呼び出し、戻り値は呼び出しが成功したかどうか
	virtual b8		OnMessage(hash32 _crc, const void* _argv[], u32 _argc) const { return false; }

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	virtual void	Finalize() {}


private:
	CObject*	m_pObject = NULL;			///< 接続先オブジェクト
	CProperty*	m_pObjLinkPrevProp = NULL;	///< オブジェクトリンク前ノード
	CProperty*	m_pObjLinkNextProp = NULL;	///< オブジェクトリンク後ノード

	const c8*	m_pTypeName = NULL;		///< タイプ名
	hash32		m_typeNameCrc;			///< タイプ名CRC

	MEM_HANDLE	m_memHdl = MEM_HDL_INVALID;	///< メモリハンドル
	b8			m_enableOnMessage = false;	///< OnMessageを使用するか
};



//*************************************************************************************************
//@brief	プロパティ生成クラス
//*************************************************************************************************
LINK_CLASS( CPropertyCreator )
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	生成関数型
	typedef	CProperty* (*FUNC_CREATE)(MEM_HANDLE _memHdl);

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	テンプレートで指定したクラスの生成
	template< class __PROP >
	static CProperty*	Create(MEM_HANDLE _memHdl)
	{
		CProperty* pProp = MEM_NEW(_memHdl) __PROP();
		if (pProp)
		{
			pProp->SetMemHdl(_memHdl);
		}
		return pProp;
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	クラス名取得
	template< class __PROP >
	static const c8*	GetClassName() { return POISON::GetClassName<__PROP>(); }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	プロパティ生成クラスの検索
	static CPropertyCreator*	Search(hash32 _classNameCrc);

	//-------------------------------------------------------------------------------------------------
	//@brief	プロパティ生成
	static CProperty*			Create(MEM_HANDLE _memHdl, const CPropertyCreator* _pCreater);

	//-------------------------------------------------------------------------------------------------
	//@brief	プロパティ破棄
	static void					Delete(CProperty* _pProp);

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ・デストラクタ
	CPropertyCreator(FUNC_CREATE _func, const c8* _className);
	~CPropertyCreator();

	//-------------------------------------------------------------------------------------------------
	//@brief	タイプ名取得・設定
	const c8*	GetTypeName() const { return m_pTypeName; }
	hash32		GetTypeNameCrc() const { return m_typeNameCrc; }

	//-------------------------------------------------------------------------------------------------
	//@brief	生成数取得・設定
	u32		GetCreateNum() const { return m_createNum; }
	void	SetCreateNum(u32 _num)
	{
		m_createNum = _num;
		// 最大数更新チェック
		if (m_createNum > m_maxCreateNum) { m_maxCreateNum = m_createNum; }
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	最大生成数取得
	u32		GetCreateMaxNum() const { return m_maxCreateNum; }


private:
	FUNC_CREATE	m_func = NULL;		///< クラス生成関数
	const c8*	m_pTypeName = NULL;	///< クラス名
	hash32		m_typeNameCrc;		///< クラス名CRC
	u32			m_createNum = 0;	///< 生成数
	u32			m_maxCreateNum = 0;	///< 最大生成数
};


//*************************************************************************************************
//@brief	テンプレートプロパティ継承クラス
//*************************************************************************************************
template< class __PROP, class __BASE >
class TProperty : public __BASE
{
protected:
	typedef __PROP	PROP;
	typedef __BASE	BASE;
	//@brief	自身のクラスをベースに
	typedef TProperty<PROP, BASE>	CBase;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ・デストラクタ
	TProperty()
		: BASE()
	{
		s_propertyCreater.SetCreateNum(s_propertyCreater.GetCreateNum() + 1);	// インクリメント
	}
	virtual ~TProperty()
	{
		s_propertyCreater.SetCreateNum(s_propertyCreater.GetCreateNum() - 1);	// デクリメント
	}

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	プロパティクリエイター取得
	static const CPropertyCreator&	GetPropertyCreator() { return s_propertyCreater; }

private:
	static CPropertyCreator		s_propertyCreater;	///< 静的プロパティ生成クラス
};

//@brief	staticメンバ変数のプロパティクリエイターの定義
template< class PROP, class BASE >
CPropertyCreator	TProperty<PROP, BASE>::s_propertyCreater(&(CPropertyCreator::Create<PROP>), CPropertyCreator::GetClassName<PROP>());


//*************************************************************************************************
//@brief	プロパティクリエイター登録用define
#define	REGIST_PROPERTY( __class, __base )			class __class : public TProperty< __class, __base >
#define REGIST_LINK_PROPERTY( __class, __base )		class __class : public TLinkClass< __class, TProperty< __class, __base > >

//*************************************************************************************************
//@brief	プロパティクリエイター登録用define、クラス生成時に使用、内部で自動で継承を行う
#define	PROPERTY( __class )			REGIST_PROPERTY( __class, CProperty )
#define LINK_PROPERTY( __class )	REGIST_LINK_PROPERTY( __class, CProperty )


POISON_END

#include "property.inl"

#endif	// __PROPERTY_H__

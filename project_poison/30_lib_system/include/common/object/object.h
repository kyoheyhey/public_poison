﻿//#pragma once
#ifndef __OBJECT_H__
#define __OBJECT_H__

//-------------------------------------------------------------------------------------------------
// include
#include "math/srt.h"


POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype
class CProperty;


//*************************************************************************************************
//@brief	オブジェクトクラス
//*************************************************************************************************
class CObject : public CDontCopy
{
public:
	friend class CObjectUtility;

public:
	enum FLAG : u16
	{
		FLAG_NONE					= (0),
		FLAG_ENABLE					= (1 << 0),	///< 有効かどうか
		FLAG_UPDATE					= (1 << 1),	///< 更新有効かどうか
		FLAG_DRAW					= (1 << 2),	///< 描画するかどうか
		FLAG_DELETE					= (1 << 3),	///< 削除予約
		FLAG_IGNORE_PARENT_FLAG		= (1 << 4),	///< 親のフラグ状態の影響を受けない設定。　デフォルトでENABLE・UPDATE・DRAWは親も見る(ルート以外)
		FLAG_IGNORE_PARENT_DELETE	= (1 << 5),	///< 親が削除されたときに、同時に自身が削除されない設定

		FLAG_INIT					= (FLAG_ENABLE | FLAG_UPDATE | FLAG_DRAW),
	};

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	ルートオブジェクト取得
	static CObject*	GetRoot() { return s_pRoot; }

	//-------------------------------------------------------------------------------------------------
	//@brief	親オブジェクト取得
	//@note		親がルートの場合はNULLを返す
	const CObject*	GetParent()	const { return CCast<CObject*>(this)->GetParent(); }
	CObject*		GetParent() { if (m_pParent != s_pRoot) { return m_pParent; } return NULL; }

	//-------------------------------------------------------------------------------------------------
	//@brief	子供の先頭取得
	const CObject*	GetChildTop() const { return m_pChildTop; }
	CObject*		GetChildTop() { return m_pChildTop; }

	//-------------------------------------------------------------------------------------------------
	//@brief	同じ親を持つ兄取得
	const CObject*	GetBrotherPrev()	const { return m_pBrotherPrev; }
	CObject*		GetBrotherPrev() { return m_pBrotherPrev; }

	//-------------------------------------------------------------------------------------------------
	//@brief	同じ親を持つ弟取得
	const CObject*	GetBrotherNext() const { return m_pBrotherNext; }
	CObject*		GetBrotherNext() { return m_pBrotherNext; }

	//-------------------------------------------------------------------------------------------------
	//@brief	子供数取得
	u16				GetChildNum() const { return m_childNum; }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	名前取得・設定
	const c8*		GetName() const { return m_name; }
	void			SetName(const c8* _name) { strcpy_s(m_name, _name); }
	const hash32	GetNameCrc() const { return m_hash; }
	void			SetNameCrc(const hash32 _name) { m_hash = _name; }

	//-------------------------------------------------------------------------------------------------
	//@brief	メモリハンドル取得
	MEM_HANDLE		GetMemHdl() const { return m_memHdl; }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	フラグ判定
	b8		IsFlag(FLAG _flag) const { return (GetFlag() & SCast<u16>(_flag)) != 0; }
	//@brief	フラグ取得
	u16		GetFlag() const { return m_flags; }
	//@brief	フラグ設定
	void	SetFlag(FLAG _flag, b8 _enable) { if (_enable) { m_flags |= SCast<u16>(_flag); } else { m_flags &= ~SCast<u16>(_flag); } }

	//-------------------------------------------------------------------------------------------------
	//@brief	有効かどうか
	b8		IsEnable() const { return IsFlag(FLAG_ENABLE) && !IsDelete() && (IsIgnoreParentFlag() ? (true) : m_pParent->IsEnable()); }
	//@brief	有効無効切り替え
	void	SetEnable(b8 _enable) { SetFlag(FLAG_ENABLE, _enable); }

	//-------------------------------------------------------------------------------------------------
	//@brief	更新有効かどうか
	b8		IsUpdate() const { return IsFlag(FLAG_UPDATE) && !IsDelete() && (IsIgnoreParentFlag() ? (true) : m_pParent->IsUpdate()); }
	//@brief	更新有効無効切り替え
	void	SetUpdate(b8 _enable) { SetFlag(FLAG_UPDATE, _enable); }

	//-------------------------------------------------------------------------------------------------
	//@brief	描画するかどうか
	b8		IsDraw() const { return IsFlag(FLAG_DRAW) && !IsDelete() && (IsIgnoreParentFlag() ? (true) : m_pParent->IsDraw()); }
	//@brief	描画切り替え
	void	SetDraw(b8 _enable) { SetFlag(FLAG_DRAW, _enable); }

	//-------------------------------------------------------------------------------------------------
	//@brief	削除予定かどうか
	b8		IsDelete() const { return IsFlag(FLAG_DELETE); }

	//-------------------------------------------------------------------------------------------------
	//@brief	親からフラグ情報の影響を無視するか
	b8		IsIgnoreParentFlag() const { return IsFlag(FLAG_IGNORE_PARENT_FLAG); }
	//@brief	親からフラグ情報の影響を無視するか設定
	void	SetIgnoreParentFlag(b8 _enable) { SetFlag(FLAG_IGNORE_PARENT_FLAG, _enable); }

	//-------------------------------------------------------------------------------------------------
	//@brief	親の削除時に連動して削除されるかどうか
	b8		IsIgnoreParentDelete() const { return IsFlag(FLAG_IGNORE_PARENT_DELETE); }
	//@brief	親の削除時に連動して削除されるフラグ設定
	void	SetIgnoreParentDelete(b8 _enable) { SetFlag(FLAG_IGNORE_PARENT_DELETE, _enable); }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	プロパティトップ取得
	const CProperty*	GetTopProperty() const { return m_pPropTop; }
	CProperty*			GetTopProperty() { return m_pPropTop; }
	
	//-------------------------------------------------------------------------------------------------
	//@brief	次プロパティ取得
	const CProperty*	GetNextProperty(const CProperty* _pProp) const { return CCast<CObject*>(this)->GetNextProperty(_pProp); }
	CProperty*			GetNextProperty(CProperty* _pProp);

	//-------------------------------------------------------------------------------------------------
	//@brief	プロパティ所持数取得
	u16					GetPropertyNum() const { return m_propNum; }

	//-------------------------------------------------------------------------------------------------
	//@brief	プロパティ生成
	template< class PROP >
	PROP*		CreateProperty(MEM_HANDLE _memHdl = MEM_HDL_INVALID);
	CProperty*	CreateProperty(hash32 _propNameCrc, MEM_HANDLE _memHdl = MEM_HDL_INVALID);

	//-------------------------------------------------------------------------------------------------
	//@brief	プロパティ検索
	template< class PROP >
	const PROP*			SearchProperty() const { return CCast<CObject*>(this)->SearchProperty<PROP>(); }
	template< class PROP >
	PROP*				SearchProperty();
	const CProperty*	SearchProperty(hash32 _propNameCrc) const { return CCast<CObject*>(this)->SearchProperty(_propNameCrc); }
	CProperty*			SearchProperty(hash32 _propNameCrc);
	
	//-------------------------------------------------------------------------------------------------
	//@brief	プロパティ検索＆生成
	template< class PROP >
	PROP*		SearchAndCreateProperty(MEM_HANDLE _memHdl = MEM_HDL_INVALID);
	CProperty*	SearchAndCreateProperty(hash32 _propNameCrc, MEM_HANDLE _memHdl = MEM_HDL_INVALID);

	//-------------------------------------------------------------------------------------------------
	//@brief	プロパティ破棄
	void		DeleteProperty(CProperty* _pProp);
	void		DeletePropertyAll();

	//-------------------------------------------------------------------------------------------------
	//@brief	プロパティに対してのメッセージ送信
	b8		SendMessage(hash32 _crc, const void* _argv[] = NULL, u32 _argc = 0) const;
	b8		SendMessage(hash32 _crc, const void* _argv[] = NULL, u32 _argc = 0);

	//-------------------------------------------------------------------------------------------------
	//@brief	特定のプロパティに対してのメッセージ送信
	template< class PROP >
	b8		SendMessageProperty(hash32 _crc, const void* _argv[] = NULL, u32 _argc = 0) const;
	template< class PROP >
	b8		SendMessageProperty(hash32 _crc, const void* _argv[] = NULL, u32 _argc = 0);

private:
	//-------------------------------------------------------------------------------------------------
	//@brief	プロパティリンク追加
	void	AddLinkProperty(CProperty* _pProp);

	//-------------------------------------------------------------------------------------------------
	//@brief	プロパティリンク削除
	void	DelLinkProperty(CProperty* _pProp);
	
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	拡縮取得・設定
	const CVec3&	GetScale() const { return m_srt.GetScale(); }
	CVec3&			GetScale() { return m_srt.GetScale(); }
	void			SetScale(const CVec3& _scale) { m_srt.SetScale(_scale); }
	
	//-------------------------------------------------------------------------------------------------
	//@brief	回転取得・設定
	const CEuler&	GetRotate() const { return m_srt.GetRotate(); }
	CEuler&			GetRotate() { return m_srt.GetRotate(); }
	void			SetRotate(const CEuler& _rotate) { m_srt.SetRotate(_rotate); }
	
	//-------------------------------------------------------------------------------------------------
	//@brief	移動取得・設定
	const CVec3&	GetTrans() const { return m_srt.GetTrans(); }
	CVec3&			GetTrans() { return m_srt.GetTrans(); }
	void			SetTrans(const CVec3& _trans) { m_srt.SetTrans(_trans); }

	//-------------------------------------------------------------------------------------------------
	//@brief	行列取得
	const CMtx34&	GetLocalMtx();

private:
	//-------------------------------------------------------------------------------------------------
	//@brief	ローカルで保持できないように
	CObject();
	~CObject();

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	void	Finalize();

	//-------------------------------------------------------------------------------------------------
	//@brief	子供追加
	//@param[in]	_obj		追加するオブジェクト
	void	AddChild(CObject* _obj);

	//-------------------------------------------------------------------------------------------------
	//@brief	子供削除
	//@param[in]	_obj		削除するオブジェクト
	void	DelChild(CObject* _obj);


private:
	CObject*	m_pParent = NULL;		///< 親オブジェクト
	CObject*	m_pChildTop = NULL;		///< 長男子供オブジェクト
	CObject*	m_pChildEnd = NULL;		///< 末っ子子供オブジェクト
	CObject*	m_pBrotherPrev = NULL;	///< 兄弟オブジェクト
	CObject*	m_pBrotherNext = NULL;	///< 兄弟オブジェクト

	CObject*	m_pDeleteNext = NULL;	///< 削除リンクリスト

	CProperty*	m_pPropTop = NULL;		///< 先頭プロパティ
	CProperty*	m_pPropEnd = NULL;		///< 末尾プロパティ

	c8			m_name[32] = {};		///< 名前
	hash32		m_hash;					///< ハッシュ値

	MEM_HANDLE	m_memHdl = MEM_HDL_INVALID;	///< メモリハンドル
	u16			m_flags = FLAG_INIT;		///< フラグ(FLAG)
	u16			m_childNum = 0;				///< 子オブジェクトの数
	u16			m_propNum = 0;				///< プロパティの数

	CSrt		m_srt;					///< 姿勢
	CMtx34		m_mtx;					///< 姿勢行列

	static CObject*	s_pRoot;			///< ルートオブジェクト実体
	static CObject*	s_pDeleteObjectTop;	///< 削除予約リスト先頭
	static CObject* s_pDeleteObjectEnd;	///< 削除予約リスト末尾
};




//*************************************************************************************************
//@brief	オブジェクトクラス
//*************************************************************************************************
class CObjectUtility
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	static b8		Initialize();

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	static void		Finalize();

	//-------------------------------------------------------------------------------------------------
	//@brief	オブジェクト作成
	//@return	オブジェクト
	//@param[in]	_memHdl	メモリハンドル
	//@param[in]	_name	オブジェクト名
	//@param[in]	_parent	親オブジェクト（※NULLならルートが親に）
	static CObject*	Create(MEM_HANDLE _memHdl, const c8* _name, CObject* _parent = NULL);

	//-------------------------------------------------------------------------------------------------
	//@brief	オブジェクト削除
	//@param[in]	_obj		削除するオブジェクト
	//@param[in]	_isImediate	即時削除
	static void		Delete(CObject* _obj, b8 _isImediate = false);

	//-------------------------------------------------------------------------------------------------
	//@brief	削除リストに登録されてるオブジェクト削除実行
	static void		DeleteExecute();

private:
	//-------------------------------------------------------------------------------------------------
	//@brief	最終破棄
	static void		FinDelete(CObject* _obj);
};




POISON_END

#endif	// __OBJECT_H__

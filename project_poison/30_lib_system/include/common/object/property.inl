﻿//#pragma once
#ifndef __PROPERTY_INL__
#define __PROPERTY_INL__


POISON_BGN


///-------------------------------------------------------------------------------------------------
/// プロパティ生成
template< class PROP >
PROP*	CObject::CreateProperty(MEM_HANDLE _memHdl/* = MEM_HDL_INVALID*/)
{
	if (_memHdl == MEM_HDL_INVALID) { _memHdl = m_memHdl; }
	CProperty* pProp = CPropertyCreator::Create(_memHdl, &PROP::GetPropertyCreator());
	if (pProp)
	{
		pProp->SetObject(this);
		AddLinkProperty(pProp);
		pProp->OnLinkObject();
	}
	// キャストして返す
	return SCast<PROP*>(pProp);
}


///-------------------------------------------------------------------------------------------------
/// プロパティ検索
template< class PROP >
PROP*	CObject::SearchProperty()
{
	// プロパティクリエイターのCRCから検索
	CProperty* pProp = SearchProperty(PROP::GetPropertyCreator().GetTypeNameCrc());
	// キャストして返す
	return SCast<PROP*>(pProp);
}


///-------------------------------------------------------------------------------------------------
/// プロパティ検索＆生成
template< class PROP >
PROP*	CObject::SearchAndCreateProperty(MEM_HANDLE _memHdl/* = MEM_HDL_INVALID*/)
{
	PROP* pProp = SearchProperty<PROP>();
	if (pProp == NULL)
	{
		pProp = CreateProperty<PROP>(_memHdl);
	}
	return pProp;
}


///-------------------------------------------------------------------------------------------------
/// 特定のプロパティに対してのメッセージ送信
template< class PROP >
b8		CObject::SendMessageProperty(hash32 _crc, const void* _argv[]/* = NULL*/, u32 _argc/* = 0*/) const
{
	const PROP* pProp = SearchProperty<PROP>();
	if (pProp)
	{
		// メッセージ送信
		return pProp->OnMessage(_crc, _argv, _argc);
	}
	return false;
}

template< class PROP >
b8		CObject::SendMessageProperty(hash32 _crc, const void* _argv[]/* = NULL*/, u32 _argc/* = 0*/)
{
	PROP* pProp = SearchProperty<PROP>();
	if (pProp)
	{
		// メッセージ送信
		return pProp->OnMessage(_crc, _argv, _argc);
	}
	return false;
}


POISON_END

#endif	// __PROPERTY_INL__

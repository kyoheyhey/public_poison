﻿//#pragma once
#ifndef __COMPONENT_INL__
#define __COMPONENT_INL__

#include "component_func.h"

POISON_BGN


///-------------------------------------------------------------------------------------------------
/// staticメンバ変数の定義
template< class COMP >
CComponentFunc*		TComponent<COMP>::s_pCompFunc = NULL;


///-------------------------------------------------------------------------------------------------
/// 開始コールバック取得・設定
template< class COMP >
typename TComponent<COMP>::FUNC_STEP_BEGIN	TComponent<COMP>::GetBgnFunc()
{
	return GetCompnentFunc()->GetBgnFunc();
}
template< class COMP >
void			TComponent<COMP>::SetBgnFunc(FUNC_STEP_BEGIN _func)
{
	GetCompnentFunc()->SetBgnFunc(_func);
}

///-------------------------------------------------------------------------------------------------
/// 終了コールバック取得・設定
template< class COMP >
typename TComponent<COMP>::FUNC_STEP_END	TComponent<COMP>::GetEndFunc()
{
	return GetCompnentFunc()->GetEndFunc();
}
template< class COMP >
void			TComponent<COMP>::SetEndFunc(FUNC_STEP_END _func)
{
	GetCompnentFunc()->SetEndFunc(_func);
}

///-------------------------------------------------------------------------------------------------
/// コンポーネント関数取得
template< class COMP >
CComponentFunc*		TComponent<COMP>::GetCompnentFunc(CComponent* _pComp)
{
	static CComponentFunc s_compFunc(CPropertyCreator::GetClassName<COMP>(), SCast<CComponent::STEP_TYPE>(COMP::ENABLE_STEP), _pComp->GetPriority());
	s_pCompFunc = &s_compFunc;
	return s_pCompFunc;
}


POISON_END

#endif	// __COMPONENT_INL__

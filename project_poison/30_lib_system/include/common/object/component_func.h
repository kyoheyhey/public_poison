﻿//#pragma once
#ifndef __COMPONENT_FUNC_H__
#define __COMPONENT_FUNC_H__

//-------------------------------------------------------------------------------------------------
// include
#include "component.h"


POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype


//*************************************************************************************************
//@brief	コンポーネント関数クラス
//*************************************************************************************************
class CComponentFunc
{
public:
	friend class CComponent;

public:
	//@brief	ステップタイプインデックス
	typedef		CComponent::STEP_TYPE_IDX	STEP_TYPE_IDX;
	//@brief	ステップタイプ
	typedef		CComponent::STEP_TYPE		STEP_TYPE;

	//@brief	コンポーネントの種類ごとの開始コールバック
	typedef		b8		(*FUNC_STEP_BEGIN)(CComponentFunc* _compFunc, STEP_TYPE _type);
	//@brief	コンポーネントの種類ごとの終了コールバック
	typedef		void	(*FUNC_STEP_END)(CComponentFunc* _compFunc, STEP_TYPE _type);

public:
	CComponentFunc(const c8* _className, STEP_TYPE _stepType, s32 _priority);
	CComponentFunc(const c8* _className, STEP_TYPE _stepType, const s32* _pPriority);
	~CComponentFunc();
	
	//-------------------------------------------------------------------------------------------------
	//@brief	先頭コンポーネント取得
	const CComponent*	GetCompTop() const { return m_pCompTop; }
	CComponent*			GetCompTop() { return m_pCompTop; }

	//-------------------------------------------------------------------------------------------------
	//@brief	末尾コンポーネント取得
	const CComponent*	GetCompEnd() const { return m_pCompEnd; }
	CComponent*			GetCompEnd() { return m_pCompEnd; }

	//-------------------------------------------------------------------------------------------------
	//@brief	コンポーネント数取得
	u32			GetCompNum() const { return m_compNum; }

	//-------------------------------------------------------------------------------------------------
	//@brief	コンポーネント追加
	void		AddComp(CComponent* _pComp);

	//-------------------------------------------------------------------------------------------------
	//@brief	コンポーネント削除
	void		DelComp(CComponent* _pComp);

	//-------------------------------------------------------------------------------------------------
	//@brief	タイプ名取得・設定
	const c8*	GetTypeName() const { return m_pTypeName; }
	hash32		GetTypeNameCrc() const { return m_typeNameCrc; }

	//-------------------------------------------------------------------------------------------------
	//@brief	開始コールバック取得・設定
	FUNC_STEP_BEGIN	GetBgnFunc() { return m_bgnFunc; }
	void			SetBgnFunc(FUNC_STEP_BEGIN _func) { m_bgnFunc = _func; }

	//-------------------------------------------------------------------------------------------------
	//@brief	終了コールバック取得・設定
	FUNC_STEP_END	GetEndFunc() { return m_endFunc; }
	void			SetEndFunc(FUNC_STEP_END _func) { m_endFunc = _func; }

	//-------------------------------------------------------------------------------------------------
	//@brief	タイプステップALL呼び出し
	template < STEP_TYPE __TYPE, STEP_TYPE_IDX __TYPE_IDX >
	static void	CallStepAll();

	//-------------------------------------------------------------------------------------------------
	//@brief	タイプステップ呼び出し
	template < STEP_TYPE __TYPE >
	void		CallStep();

private:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンポーネント関数追加
	static void		AddCompFunc(CComponentFunc* _pCompFunc, STEP_TYPE_IDX _stepTypeIdx, s32 _priority);

	//-------------------------------------------------------------------------------------------------
	//@brief	コンポーネント関数削除
	static void		DelCompFunc(CComponentFunc* _pCompFunc, STEP_TYPE_IDX _stepTypeIdx);
	
	//-------------------------------------------------------------------------------------------------
	//@brief	先頭コンポーネント関数取得
	static CComponentFunc*	GetTop(STEP_TYPE_IDX _stepTypeIdx) { return s_pTop[_stepTypeIdx]; }

	//-------------------------------------------------------------------------------------------------
	//@brief	末尾コンポーネント関数取得
	static u32				GetNum(STEP_TYPE_IDX _stepTypeIdx) { return s_num[_stepTypeIdx]; }

	//-------------------------------------------------------------------------------------------------
	//@brief	次コンポーネント関数取得
	const CComponentFunc*	GetNext(STEP_TYPE_IDX _stepTypeIdx) const { return m_pNext[_stepTypeIdx]; }
	CComponentFunc*			GetNext(STEP_TYPE_IDX _stepTypeIdx) { return m_pNext[_stepTypeIdx]; }

	//-------------------------------------------------------------------------------------------------
	//@brief	次コンポーネント関数設定
	void		SetNext(CComponentFunc* _pCompFunc, STEP_TYPE_IDX _stepTypeIdx) { m_pNext[_stepTypeIdx] = _pCompFunc; }

	//-------------------------------------------------------------------------------------------------
	//@brief	ステッププライオリティ取得・設定
	s32			GetPriority(STEP_TYPE_IDX _stepTypeIdx) const { return m_stepPrio[_stepTypeIdx]; }
	void		SetPriority(s32 _priority, STEP_TYPE_IDX _stepTypeIdx) { m_stepPrio[_stepTypeIdx] = _priority; }


private:
	static CComponentFunc*	s_pTop[CComponent::STEP_TYPE::STEP_TYPE_NUM];
	static u32				s_num[CComponent::STEP_TYPE::STEP_TYPE_NUM];
	CComponentFunc*			m_pNext[CComponent::STEP_TYPE::STEP_TYPE_NUM] = {};

	CComponent*				m_pCompTop = NULL;	///< 先頭ノード
	CComponent*				m_pCompEnd = NULL;	///< 末尾ノード
	u32						m_compNum = 0;		///< コンポーネント数

	FUNC_STEP_BEGIN			m_bgnFunc = NULL;	///< 開始コールバック
	FUNC_STEP_END			m_endFunc = NULL;	///< 終了コールバック

	const c8*				m_pTypeName = NULL;	///< クラス名
	hash32					m_typeNameCrc;		///< クラス名CRC
	CComponent::STEP_TYPE	m_stepType = CComponent::STEP_TYPE::NONE;
	s32						m_stepPrio[CComponent::STEP_TYPE::STEP_TYPE_NUM] = {};	///< ステッププライオリティ
};

POISON_END

#include "component_func.inl"

#endif	// __COMPONENT_FUNC_H__

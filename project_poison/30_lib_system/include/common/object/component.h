﻿//#pragma once
#ifndef __COMPONENT_H__
#define __COMPONENT_H__

//-------------------------------------------------------------------------------------------------
// include
#include "property.h"


POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype
class CComponentFunc;


//*************************************************************************************************
//@brief	
//*************************************************************************************************
class CComponent : public CProperty
{
public:
	friend class CComponentFunc;

public:
	//@brief	ベースクラスをリネーム
	typedef	CProperty	CBase;

	//@brief	有効ステップインデックス
	enum STEP_TYPE_IDX : u32
	{
		STEP_TYPE_IDX_PRE_STEP = 0,	///< PreStepの呼び出し有効
		STEP_TYPE_IDX_STEP,			///< Stepの呼び出し有効
		STEP_TYPE_IDX_POST_STEP,	///< PostStepの呼び出し有効
		STEP_TYPE_IDX_PREPARE_DRAW,	///< PrepareDrawの呼び出し有効
		STEP_TYPE_IDX_DRAW,			///< Drawの呼び出し有効	
		STEP_TYPE_IDX_NUM
	};

	//@brief	有効ステップフラグ
	enum STEP_TYPE : u32
	{
		NONE			= 0,
		PRE_STEP		= (1 << STEP_TYPE_IDX_PRE_STEP),		///< PreStepの呼び出し有効
		STEP			= (1 << STEP_TYPE_IDX_STEP),			///< Stepの呼び出し有効
		POST_STEP		= (1 << STEP_TYPE_IDX_POST_STEP),		///< PostStepの呼び出し有効
		PREPARE_DRAW	= (1 << STEP_TYPE_IDX_PREPARE_DRAW),	///< PrepareDrawの呼び出し有効
		DRAW			= (1 << STEP_TYPE_IDX_DRAW),			///< Drawの呼び出し有効	

		STEP_TYPE_NUM	= STEP_TYPE_IDX_NUM,					///< ステップタイプの個数

		ALL_STEP		= (PRE_STEP | STEP | POST_STEP),		///< ステップ全部
		ALL				= (ALL_STEP | PREPARE_DRAW | DRAW),		///< 全ステップ
	};

	//@brief	ステップフラグ(STEP_TYPEの複数ビット保持型)
	typedef	u16		STEP_FLAG;
	enum { ENABLE_STEP = NONE };

	//@brief	内部メソッド関数型
	typedef void(CComponent::* STEP_FUNC)();
	typedef b8(CComponent::* BUILD_FUNC)();

public:
	CComponent();
	~CComponent();

protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	オブジェクトとリンクしたときのコールバック
	virtual void	OnLinkObject() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	virtual void	Finalize() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	オーバーライド用PreStep関数
	virtual void	PreStep() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	オーバーライド用Step関数
	virtual void	Step() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	オーバーライド用PostStep関数
	virtual void	PostStep() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	オーバーライド用PrepareDraw関数
	virtual void	PrepareDraw() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	オーバーライド用Draw関数
	virtual void	Draw() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	ステップの関数取得
	template< STEP_TYPE __TYPE >
	static STEP_FUNC	GetStepFunc();

	//-------------------------------------------------------------------------------------------------
	//@brief	オーバーライド用Pre構築関数
	virtual b8		PreBuild() { return true; };

	//-------------------------------------------------------------------------------------------------
	//@brief	オーバーライド用構築関数
	virtual b8		Build() { return true; };

	//-------------------------------------------------------------------------------------------------
	//@brief	オーバーライド用Post構築関数
	virtual b8		PostBuild() { return true; };
	
	//-------------------------------------------------------------------------------------------------
	//@brief	構築の関数取得
	template< STEP_TYPE __TYPE >
	static BUILD_FUNC	GetBuildFunc();
	
	//-------------------------------------------------------------------------------------------------
	//@brief	コンポーネント関数取得
	virtual CComponentFunc*	GetCompnentFunc() = 0;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	前ノード取得
	const CComponent*	GetPrev() const { return m_pPrev; }
	CComponent*			GetPrev() { return m_pPrev; }

	//-------------------------------------------------------------------------------------------------
	//@brief	次ノード取得
	const CComponent*	GetNext() const { return m_pNext; }
	CComponent*			GetNext() { return m_pNext; }

	//-------------------------------------------------------------------------------------------------
	//@brief	ステッププライオリティ取得・設定
	const s32*	GetPriority() const { return m_stepPrio; }
	s32			GetPriority(STEP_TYPE_IDX _stepTypeIdx) const { return m_stepPrio[_stepTypeIdx]; }
	void		SetPriority(s32 _priority, STEP_TYPE_IDX _stepTypeIdx) { m_stepPrio[_stepTypeIdx] = _priority; }

	//-------------------------------------------------------------------------------------------------
	//@brief	有効フラグ取得
	b8			IsEnable() const { return ((GetObject() ? GetObject()->IsUpdate() : true) && IsStepEnable()); }

	//-------------------------------------------------------------------------------------------------
	//@brief	ステップ有効フラグ取得・設定
	b8			IsStepEnable() const { return m_stepEnable; }
	void		SetStepEnable(b8 _enable) { m_stepEnable = _enable; }

	//-------------------------------------------------------------------------------------------------
	//@brief	ビルドステップ有効フラグ取得・設定
	b8			IsBuildEnable() const { return m_buildEnable; }
	void		SetBuildEnable(b8 _enable) { m_buildEnable = _enable; }

protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	前ノード設定
	void		SetPrev(CComponent* _pComp) { m_pPrev = _pComp; }
	//-------------------------------------------------------------------------------------------------
	//@brief	次ノード設定
	void		SetNext(CComponent* _pComp) { m_pNext = _pComp; }

private:
	CComponent*		m_pPrev = NULL;			///< 前ノード
	CComponent*		m_pNext = NULL;			///< 後ノード
	s32				m_stepPrio[STEP_TYPE::STEP_TYPE_NUM] = {};	///< ステッププライオリティ
	b8				m_stepEnable = true;	///< ステップ有効フラグ
	b8				m_buildEnable = false;	///< ビルドステップ有効フラグ
};



//*************************************************************************************************
//@brief	テンプレートコンポーネント継承クラス
//*************************************************************************************************
template< class __COMP >
class TComponent : public CComponent
{
protected:
	typedef __COMP	COMP;
	//@brief	自身のクラスをベースに
	typedef TComponent<COMP>	CBase;
	//@brief	コンポーネントの種類ごとの開始コールバック
	typedef b8(*FUNC_STEP_BEGIN)(CComponentFunc* _compFunc, STEP_TYPE _type);
	//@brief	コンポーネントの種類ごとの終了コールバック
	typedef void(*FUNC_STEP_END)(CComponentFunc* _compFunc, STEP_TYPE _type);

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ・デストラクタ
	TComponent()
		: CComponent()
	{}
	virtual ~TComponent()
	{}

	//-------------------------------------------------------------------------------------------------
	//@brief	先頭ノード取得
	static CComponent*	GetTop() { return libAssert(s_pCompFunc); s_pCompFunc->GetTop(); }

	//-------------------------------------------------------------------------------------------------
	//@brief	末尾ノード取得
	static CComponent*	GetEnd() { return libAssert(s_pCompFunc); s_pCompFunc->GetEnd(); }

protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	開始コールバック取得・設定
	FUNC_STEP_BEGIN	GetBgnFunc();
	void			SetBgnFunc(FUNC_STEP_BEGIN _func);

	//-------------------------------------------------------------------------------------------------
	//@brief	終了コールバック取得・設定
	FUNC_STEP_END	GetEndFunc();
	void			SetEndFunc(FUNC_STEP_END _func);

private:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンポーネント関数取得
	virtual CComponentFunc*	GetCompnentFunc() override { return GetCompnentFunc(this); }

	//-------------------------------------------------------------------------------------------------
	//@brief	コンポーネント関数取得
	static CComponentFunc*	GetCompnentFunc(CComponent* _pComp);

private:
	static CComponentFunc*	s_pCompFunc;
};



//*************************************************************************************************
//@brief	プロパティクリエイター登録用define、クラス生成時に使用、内部で自動で継承を行う
#define	COMPONENT( __class )		REGIST_PROPERTY( __class, TComponent<__class> )


POISON_END

#include "component.inl"

#endif	// __COMPONENT_H__

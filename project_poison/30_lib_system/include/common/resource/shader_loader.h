﻿//#pragma once
#ifndef __SHADER_LOADER_H__
#define __SHADER_LOADER_H__

//-------------------------------------------------------------------------------------------------
// include
#include "shader/shader_utility.h"



POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype
class CResShader;
class CShaderLibrary;


//*************************************************************************************************
//@brief	シェーダローダー
//*************************************************************************************************
STATIC_SINGLETON( CShaderLoader )
{
public:
	FRIEND_STATIC_SINGLETON( CShaderLoader );

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コマンドファイルの解析
	//@return	成功か
	//@param[in]	_pRes			生成リソース
	//@param[in]	_fileBuf		コマンドファイルバッファ
	//@param[in]	_fileSize		コマンドファイルサイズ
	//@param[in]	_workMemHdl		一時アロケータ
	//@note		渡されたコマンドファイルを解析し、本クラスで所持する
	static b8	BuildResource(CShaderLibrary * _pRes, const void* _fileBuf, u32 _fileSize, MEM_HANDLE _workMemHdl);
	static b8	BuildResource(CResShader* _pRes, const void* _fileBuf, u32 _fileSize, MEM_HANDLE _workMemHdl);
};


POISON_END

#endif	// __SHADER_LOADER_H__

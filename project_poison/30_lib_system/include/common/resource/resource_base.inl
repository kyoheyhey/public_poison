﻿//#pragma once
#ifndef __RESOURCE_BASE_INL__
#define __RESOURCE_BASE_INL__


POISON_BGN

///*************************************************************************************************
///	リソースマネージャー
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// テンプレート宣言部分の省略
#define RES_MANAGER_TEMPLATE_HEAD	template <class __RES>
#define RES_MANAGER_CLASS_HEAD		CResManager< __RES >
#define RES_CLASS_HEAD				CResManager< __RES >::RES


///-------------------------------------------------------------------------------------------------
/// インスタンスポインタ定義
RES_MANAGER_TEMPLATE_HEAD
RES_MANAGER_CLASS_HEAD*	RES_MANAGER_CLASS_HEAD::m_pInstance = NULL;

///-------------------------------------------------------------------------------------------------
/// コンストラクタ・デストラクタ
RES_MANAGER_TEMPLATE_HEAD
RES_MANAGER_CLASS_HEAD::CResManager()
{
	libAssert(m_pInstance == NULL);
	m_pInstance = this;
}
RES_MANAGER_TEMPLATE_HEAD
RES_MANAGER_CLASS_HEAD::~CResManager()
{
	m_pInstance = NULL;
}

///-------------------------------------------------------------------------------------------------
/// 初期化
RES_MANAGER_TEMPLATE_HEAD
b8		RES_MANAGER_CLASS_HEAD::Initialize(IAllocator* _pAllocator, const u32 _resNum)
{
	return m_list.Initialize(_pAllocator, _resNum);
}

///-------------------------------------------------------------------------------------------------
/// 終了処理
RES_MANAGER_TEMPLATE_HEAD
void	RES_MANAGER_CLASS_HEAD::Finalize()
{
	DeleteAll();
	m_list.Finalize();
}


///-------------------------------------------------------------------------------------------------
/// 削除チェック、参照カウンタが0のリソース削除、削除した個数が帰る
RES_MANAGER_TEMPLATE_HEAD
u32		RES_MANAGER_CLASS_HEAD::CheckDelete(CHECK_DELETE_FUNC _func/* = NULL*/)
{
	u32 count = 0;
	RES_LIST::ITR* pItr = m_list.GetBgn();
	while (pItr)
	{
		RES_LIST::ITR* pCheckItr = pItr;
		pItr = m_list.GetNext(pItr);

		// 参照カウンタが0だったら
		RES& rRes = m_list.GetDataFromItr(pCheckItr);
		if (rRes.GetRefCount() <= 0)
		{
			if (_func) { _func(&rRes); }
			// リソース破棄
			rRes.Release();
			m_list.Erase(pCheckItr);
			++count;
		}
	}
	return count;
}

///-------------------------------------------------------------------------------------------------
/// リソース作成
RES_MANAGER_TEMPLATE_HEAD
typename RES_CLASS_HEAD*	RES_MANAGER_CLASS_HEAD::Create(const hash32 _nameCrc)
{
	RES_LIST::ITR* pItr = m_list.PushBack(_nameCrc);
	if (pItr)
	{
		RES* pRes = &m_list.GetDataFromItr(pItr);
		new(pRes) RES;
		return pRes;
	}
	return NULL;
}

///-------------------------------------------------------------------------------------------------
/// 検索、なければリソース作成
RES_MANAGER_TEMPLATE_HEAD
typename RES_CLASS_HEAD*	RES_MANAGER_CLASS_HEAD::SearchAndCreate(const hash32 _nameCrc)
{
	RES* pRes = m_list.SearchData(_nameCrc);
	if (pRes == NULL)
	{
		pRes = Create(_nameCrc);
	}
	return pRes;
}

///-------------------------------------------------------------------------------------------------
/// リソース削除(参照カウンタを無視して削除)
RES_MANAGER_TEMPLATE_HEAD
void	RES_MANAGER_CLASS_HEAD::Delete(const hash32 _nameCrc)
{
	RES_LIST::ITR* pItr = m_list.SearchItr(_nameCrc);
	if (pItr)
	{
		RES& rRes = m_list.GetDataFromItr(pItr);
		// リソース破棄
		rRes.Release();
		rRes.~RES();
		m_list.Erase(pItr);
	}
}

///-------------------------------------------------------------------------------------------------
/// 全削除
RES_MANAGER_TEMPLATE_HEAD
void	RES_MANAGER_CLASS_HEAD::DeleteAll()
{
	RES_LIST::ITR* pItr = m_list.GetBgn();
	while (pItr)
	{
		RES& rRes = m_list.GetDataFromItr(pItr);
		// リソース破棄
		rRes.Release();
		rRes.~RES();
		pItr = m_list.GetNext(pItr);
	}
}

//-------------------------------------------------------------------------------------------------
//@brief	リソース取得
RES_MANAGER_TEMPLATE_HEAD
typename const RES_CLASS_HEAD*	RES_MANAGER_CLASS_HEAD::Get(const u16 _idx) const
{
	return &m_list.GetDataFromIdx(_idx);
}

///-------------------------------------------------------------------------------------------------
/// リソース検索
RES_MANAGER_TEMPLATE_HEAD
typename const RES_CLASS_HEAD*	RES_MANAGER_CLASS_HEAD::Search(const hash32 _nameCrc) const
{
	return m_list.SearchData(_nameCrc);
}


POISON_END

#endif	// __RESOURCE_BASE_INL__

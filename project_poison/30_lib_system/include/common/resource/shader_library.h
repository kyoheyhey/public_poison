﻿//#pragma once
#ifndef	__SHADER_LIBRARY_H__
#define	__SHADER_LIBRARY_H__


//-------------------------------------------------------------------------------------------------
// include
#include "shader/shader_utility.h"
#include "resource/resource_base.h"


POISON_BGN


//*************************************************************************************************
//@brief	Fxリソース
//*************************************************************************************************
RES_CLASS( CResFx )
{
public:
	friend class CResShader;

public:
	CResFx();
	~CResFx();

	//-------------------------------------------------------------------------------------------------
	//@brief	解放処理
	virtual	void	Release() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	リソース構築済みか
	virtual	b8		IsEnable() const override { return m_shader.IsEnable(); }

	//-------------------------------------------------------------------------------------------------
	//@brief	名前のCRC取得・設定
	virtual hash32	GetNameCrc() const override { return m_shader.GetNameCrc(); }
	virtual void	SetNameCrc(const hash32 _name) override { m_shader.SetNameCrc(_name); }

	//-------------------------------------------------------------------------------------------------
	//@brief	シェーダの取得
	const FxShader&	GetShader() const { return m_shader; }
	FxShader&		GetShader() { return m_shader; }

private:
	PassShader*	m_pPassShader = NULL;
	TecShader*	m_pTecShader = NULL;
	u32			m_passNum = 0;
	u32			m_tecNum = 0;
	FxShader	m_shader;
};
typedef	CResManager<CResFx>	ResFxList;



//*************************************************************************************************
//@brief	リソースシェーダ
//*************************************************************************************************
RES_CLASS( CResShader )
{
public:
	friend class CShaderFxDefInner;	///< 構築クラス

public:
	//*************************************************************************************************
	//@brief	属性情報
	struct ATTR
	{
		hash32	nameCrc = hash32::Invalid;
		u16		bit = 0;
	};

	//*************************************************************************************************
	//@brief	パス情報
	struct PASS
	{
		hash32	vsNameCrc = hash32::Invalid;
		hash32	gsNameCrc = hash32::Invalid;
		hash32	psNameCrc = hash32::Invalid;
	};

	//*************************************************************************************************
	//@brief	コンボ情報
	struct COMBO
	{
		PASS*	pPass = NULL;
		u32		passNum = 0;
		u16		bit = 0;
	};

	//*************************************************************************************************
	//@brief	テクニック情報
	struct TECHNIC
	{
		ATTR*	pAttr = NULL;
		u32		attrNum = 0;

		COMBO*	pCombo = NULL;
		u32		comboNum = 0;
		hash32	nameCrc = hash32::Invalid;
	};

public:
	CResShader();
	~CResShader();

	//-------------------------------------------------------------------------------------------------
	//@brief	解放処理
	virtual	void	Release() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	リソース構築済みか
	virtual	b8		IsEnable() const override;

	//-------------------------------------------------------------------------------------------------
	//@brief	名前のCRC取得・設定
	virtual hash32	GetNameCrc() const override { return m_nameCrc; }
	virtual void	SetNameCrc(const hash32 _name) override { m_nameCrc = _name; }

	//-------------------------------------------------------------------------------------------------
	//@brief	名前の取得
	const c8*		GetName() const { return m_name; }
	void			SetName(const c8* _name) { strcpy_s(m_name, _name); }

	//-------------------------------------------------------------------------------------------------
	//@brief	生成
	b8			Create(CResFx* _pFxRes, const VTX_DECL* _pDecl, u32 _declNum) const;

private:
	TECHNIC*	m_pTec = NULL;
	u32			m_tecNum = 0;

	hash32		m_nameCrc = hash32::Invalid;
	c8			m_name[32] = {};

};

typedef	CResManager<CResShader>	ResShaderList;


//*************************************************************************************************
//@brief	シェーダライブラリ
//*************************************************************************************************
class CShaderLibrary : public IShaderLibrary
{
public:
	friend class CShaderListDefInner;	///< 構築クラス

public:
	//*************************************************************************************************
	//@brief	頂点リソース
	RES_CLASS( ResVS )
	{
	public:
		ResVS() {}
		~ResVS() {}

		//-------------------------------------------------------------------------------------------------
		//@brief	解放処理
		virtual	void	Release() override { m_shader.Release(); }

		//-------------------------------------------------------------------------------------------------
		//@brief	リソース構築済みか
		virtual	b8		IsEnable() const override { return m_shader.IsEnable(); }

		//-------------------------------------------------------------------------------------------------
		//@brief	名前のCRC取得・設定
		virtual hash32	GetNameCrc() const override { return m_name; }
		virtual void	SetNameCrc(const hash32 _name) override { m_name = _name; }

		//-------------------------------------------------------------------------------------------------
		//@brief	シェーダの取得
		const VShader&	GetShader() const { return m_shader; }
		VShader&		GetShader() { return m_shader; }

	private:
		VShader		m_shader;
		hash32		m_name;
	};
	typedef	CResManager<ResVS>	ResVSList;


	//*************************************************************************************************
	//@brief	ジオメトリリソース
	RES_CLASS( ResGS )
	{
	public:
		ResGS() {}
		~ResGS() {}

		//-------------------------------------------------------------------------------------------------
		//@brief	解放処理
		virtual	void	Release() override { m_shader.Release(); }

		//-------------------------------------------------------------------------------------------------
		//@brief	リソース構築済みか
		virtual	b8		IsEnable() const override { return m_shader.IsEnable(); }

		//-------------------------------------------------------------------------------------------------
		//@brief	名前のCRC取得・設定
		virtual hash32	GetNameCrc() const override { return m_name; }
		virtual void	SetNameCrc(const hash32 _name) override { m_name = _name; }

		//-------------------------------------------------------------------------------------------------
		//@brief	シェーダの取得
		const GShader&	GetShader() const { return m_shader; }
		GShader&		GetShader() { return m_shader; }

	private:
		GShader		m_shader;
		hash32		m_name;
	};
	typedef	CResManager<ResGS>	ResGSList;


	//*************************************************************************************************
	//@brief	ピクセルリソース
	RES_CLASS( ResPS )
	{
	public:
		ResPS() {}
		~ResPS() {}

		//-------------------------------------------------------------------------------------------------
		//@brief	解放処理
		virtual	void	Release() override { m_shader.Release(); }

		//-------------------------------------------------------------------------------------------------
		//@brief	リソース構築済みか
		virtual	b8		IsEnable() const override { return m_shader.IsEnable(); }

		//-------------------------------------------------------------------------------------------------
		//@brief	名前のCRC取得・設定
		virtual hash32	GetNameCrc() const override { return m_name; }
		virtual void	SetNameCrc(const hash32 _name) override { m_name = _name; }

		//-------------------------------------------------------------------------------------------------
		//@brief	シェーダの取得
		const PShader&	GetShader() const { return m_shader; }
		PShader&		GetShader() { return m_shader; }

	private:
		PShader		m_shader;
		hash32		m_name;
	};
	typedef	CResManager<ResPS>	ResPSList;


	//*************************************************************************************************
	//@brief	エフェクトリソース
	RES_CLASS( ResPass )
	{
	public:
		ResPass() {}
		~ResPass() {}

		//-------------------------------------------------------------------------------------------------
		//@brief	解放処理
		virtual	void	Release() override { m_shader.Release(); }

		//-------------------------------------------------------------------------------------------------
		//@brief	リソース構築済みか
		virtual	b8		IsEnable() const override { return m_shader.IsEnable(); }

		//-------------------------------------------------------------------------------------------------
		//@brief	名前のCRC取得・設定
		virtual hash32	GetNameCrc() const override { return m_name; }
		virtual void	SetNameCrc(const hash32 _name) override { m_name = _name; }

		//-------------------------------------------------------------------------------------------------
		//@brief	シェーダの取得
		const PassShader&	GetShader() const { return m_shader; }
		PassShader&			GetShader() { return m_shader; }

	private:
		PassShader	m_shader;
		hash32		m_name;
	};
	typedef	CResManager<ResPass>	ResPassList;


public:
	CShaderLibrary();
	~CShaderLibrary();

	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	b8		Initialize(MEM_HANDLE _memHdl);

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	virtual void	Finalize();
	
	//-------------------------------------------------------------------------------------------------
	//@brief	メモリハンドル取得
	virtual MEM_HANDLE	GetMemHdl() const override { return m_memHdl; }

	//-------------------------------------------------------------------------------------------------
	//@brief	VS検索
	virtual const VShader*	GetVShader(u16 _idx) const override;
	virtual const VShader*	SearchVShader(hash32 _nameCrc) const override;

	//-------------------------------------------------------------------------------------------------
	//@brief	GS検索
	virtual const GShader*	GetGShader(u16 _idx) const override;
	virtual const GShader*	SearchGShader(hash32 _nameCrc) const override;

	//-------------------------------------------------------------------------------------------------
	//@brief	PS検索
	virtual const PShader*	GetPShader(u16 _idx) const override;
	virtual const PShader*	SearchPShader(hash32 _nameCrc) const override;
	
	//-------------------------------------------------------------------------------------------------
	//@brief	Fx取得・検索＆生成
	virtual const FxShader*	GetFxShader(u16 _idx) const override;
	virtual const FxShader*	SearchAndCreateFxShader(hash32 _nameCrc, const VTX_DECL* _pDecl, u32 _declNum) override;

	//-------------------------------------------------------------------------------------------------
	//@brief	エラーシェーダ取得・設定
	virtual const PassShader*	GetError() const override { return m_pError; }
	void						SetError(const PassShader* _pError) { m_pError = _pError; }
	
	//-------------------------------------------------------------------------------------------------
	//@brief	VS追加
	VShader*	CreateVShader(hash32 _nameCrc);
	VShader*	CreateAndSearchVShader(hash32 _nameCrc);

	//-------------------------------------------------------------------------------------------------
	//@brief	GS追加
	GShader*	CreateGShader(hash32 _nameCrc);
	GShader*	CreateAndSearchGShader(hash32 _nameCrc);

	//-------------------------------------------------------------------------------------------------
	//@brief	PS追加
	PShader*	CreatePShader(hash32 _nameCrc);
	PShader*	CreateAndSearchPShader(hash32 _nameCrc);

	//-------------------------------------------------------------------------------------------------
	//@brief	リソースシェーダ追加
	CResShader*	CreateResShader(hash32 _nameCrc);


protected:
	ResVSList			m_vsList;	///< 頂点シェーダリソース
	ResGSList			m_gsList;	///< ジオメトリシェーダリソース
	ResPSList			m_psList;	///< ピクセルシェーダリソース

	ResFxList			m_fxList;	///< エフェクトシェーダリソース
	ResShaderList		m_resList;	///< リソースシェーダリスト

	const PassShader*	m_pError;	///< エラーシェーダ

	MEM_HANDLE			m_memHdl;	///< メモリハンドル
};



POISON_END

#endif	// __SHADER_LIBRARY_H__

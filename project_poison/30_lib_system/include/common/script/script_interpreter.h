﻿#pragma once
#ifndef __SCRIPT_INTERPRETER_H__
#define __SCRIPT_INTERPRETER_H__

//*************************************************************************************************
// バイナリパラメータファイル読み込み処理クラス
// 拡張スクリプトインタープリタクラス
//*************************************************************************************************


//-------------------------------------------------------------------------------------------------
// pragma
// warning C4200: 非標準の拡張機能が使用されています : 構造体または共用体中にサイズが 0 の配列があります。
#ifdef LIB_SYSTEM_WIN
#pragma warning(push)
#pragma warning(disable : 4200) 
#endif


//-------------------------------------------------------------------------------------------------
// define
//インライン展開用プリプロセッサ
#ifdef SI_INLINE_ENABLED
#define SI_INLINE	inline
#else//SI_INLINE_ENABLED
#define SI_INLINE
#endif//SI_INLINE_ENABLED

#ifdef SI_STACK_OPE_INLINE_ENABLED
#define SI_STACK_OPE_INLINE	inline
#else//SI_STACK_OPE_INLINE_ENABLED
#define SI_STACK_OPE_INLINE
#endif//SI_STACK_OPE_INLINE_ENABLED


//-------------------------------------------------------------------------------------------------
// include
#include "script_interpreter_compatible.h"
#include "script_interpreter_optimize.h"

#ifdef SI_USE_EASY_ALLOCATOR
#include SI_INCLUDE_GM_POINTER
#endif//SI_USE_EASY_ALLOCATOR


POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype
#ifdef SI_USE_EASY_ALLOCATOR
class IEasyAllocator;
typedef gmPointer<IEasyAllocator>	IEasyAllocatorPtr;
typedef gmPtrProxy<IEasyAllocator>	IEasyAllocatorPtrProxy;
#endif//SI_USE_EASY_ALLOCATOR

#ifdef SI_USE_MEMORY_STAFF
class CMemoryStaff;
#endif//SI_USE_MEMORY_STAFF

#ifdef SI_USE_TEXT_BUFF
class CTextBuff;
#endif//SI_USE_TEXT_BUFF

class CScriptInterpreter;


//*************************************************************************************************
//@brief	スクリプトファイルタイプ
//*************************************************************************************************
enum SI_FILE_TYPE
{
	SI_FILE_TYPE_BIN_OR_SJIS = 0,
	SI_FILE_TYPE_BIN = 0,
	SI_FILE_TYPE_SJIS = 0,
	SI_FILE_TYPE_UTF8 = 1,
	SI_FILE_TYPE_ASCII = 2
};

//-------------------------------------------------------------------------------------------------
//@brief	パラメータのアラインメントサイズ
static const s32	SI_RECORD_PARAM_ALIGNMENT = 4;

//-------------------------------------------------------------------------------------------------
//@brief	パラメータの型指定ビット数
static const s32	SI_RECORD_PARAM_TYPE_BITS = 2;
static const s32	SI_RECORD_PARAM_TYPE_NUM_OF_BYTE = 8 / SI_RECORD_PARAM_TYPE_BITS;
static const u8		SI_RECORD_PARAM_TYPE_BITS_MASK = (1 << SI_RECORD_PARAM_TYPE_BITS) - 1;
#define SI_CALC_RECORD_PARAM_TYPE_BYTE_NUM(num)	((num >> SI_RECORD_PARAM_TYPE_BITS) + ((num & SI_RECORD_PARAM_TYPE_BITS_MASK) != 0 ? 1 : 0))

//*************************************************************************************************
//@brief	パラメータの型識別ID
//*************************************************************************************************
enum E_SI_RECORD_PARAM_TYPE
{
	SI_RECORD_PARAM_TYPE_STRING = 0,
	SI_RECORD_PARAM_TYPE_INT,
	SI_RECORD_PARAM_TYPE_FLOAT
};

//*************************************************************************************************
//@brief	スタック種別
//*************************************************************************************************
enum SI_STACK_TYPE
{
	SI_STRING = 0,
	SI_INT = 1,
	SI_FLOAT = 2,
	SI_STRING_DIRECT = 3,	//< 内部処理用　※テキストスクリプトを解析した場合に用いられる
	SI_TYPE_UNKNOWN = -1,
	SI_ERROR = 255			//< CScriptInterpreter互換用（通常使用禁止）
};

//*************************************************************************************************
//@brief	ファイルヘッダー情報
//*************************************************************************************************
struct SI_TAG_PARAM_BIN_HEADER
{
	u32 record_count;			//< データの件数
	u32 text_data_file_offset;	//< 文字列データ領域のファイルオフセット　※ファイルの先頭から text_data までのオフセット値。
	u32 text_data_total_size;	//< 文字列データ領域全体サイズ
	u32 text_data_node_count;	//< 文字列データ数
};

//*************************************************************************************************
//@brief	文字列データ
//*************************************************************************************************
struct SI_TAG_PARAM_BIN_TEXT
{
	c8 text[0];		//< 文字列データ　※全文字列データが結合されたデータ。
};

//*************************************************************************************************
//@brief	基本データ（行データ）
//*************************************************************************************************
struct SI_TAG_PARAM_BIN_RECORD
{
	//*************************************************************************************************
	//@brief	レコードヘッダー情報
	struct HEADER {
		hash32	tag_id_crc;			//< タグID CRC
		u8		param_count = 0;	//< パラメータの数
	} header;

	//*************************************************************************************************
	//@brief	パラメータの型情報
	//@note		パラメータの型ID  ※2ビットの型ID。
	//			(0=String,1=Integer,2=Float)
	struct PARAM_TYPE {
		u8	type_id : SI_RECORD_PARAM_TYPE_BITS;
	};//param_type[param_count]

	//*************************************************************************************************
	//@brief	パラメータの値情報
	//@note		text_data_offset + text_offset がファイルの先頭からのオフセット値。
	//			※長さが 0 の文字列("")の場合、0xffffffff が格納されている。
	//			const c8* str_direct; //文字列　※テキストデータ解析時用
	union PARAM_VALUE {
		s32	i;			//< 整数
		u32	ui;			//< 整数(符号なし)
		f32	f;			//< 浮動少数
		s32	str_offset;	//< 文字列オフセット　※文字列の参照位置を示す。
	};//param_value[1]
};

//*************************************************************************************************
//@brief	デバッグ情報
//*************************************************************************************************
struct SI_TAG_PARAM_BIN_DEBUG
{
	//*************************************************************************************************
	//@brief	デバッグデータヘッダー
	struct HEADER {
		u32	total_size;			//< デバッグデータの全体サイズ
		u32	node_count;			//< デバッグデータ件数
		u32	text_offset;		//< デバッグ用文字列データ領域のオフセット（デバッグデータの先頭からのオフセット）
		u32	total_text_size;	//< デバッグ用文字列データサイズ
	} header;

	//*************************************************************************************************
	//@brief	デバッグデータ
	//@note		-NODBGオプションが指定されている場合は出力されない
	//@note		RoundingUp(text_data_file_offset + text_data_total_size, 16) + text_offset 
	//			の位置が対象文字列。
	//			※長さが 0 の文字列("")の場合、0xffffffff が格納されている。（あり得ないが）
	struct RECORD {
		hash32	tag_id_crc;		// TagIDもしくはタグのCRC値
		u32		text_offset;	// 文字列オフセット　※文字列の参照位置を示す。
	};//node[node_count];

	//(16byte alignment)

	//*************************************************************************************************
	//@brief	文字列情報
	struct TEXT {
		c8	text[0];	//< [total_text_size]; // 文字列データ　※全デバッグ用文字列データが結合されたデータ。
	};//text;

	//(16byte alignment)
};

//*************************************************************************************************
//@brief	ファイル情報領域 ※-NOINFOオプションが指定されている場合は出力されない
//*************************************************************************************************
struct SI_TAG_PARAM_BIN_FILE_INFO
{
	s8	tool_name[5];	//< 0: ツール名 "\x001t2b\x0fe"
	s8	version;		//< 5: バージョン
	s8	character_set;	//< 6: キャラクターセット   0=ShiftJIS, 1=UTF-8
	s8	endian_type;	//< 7: エンディアン種別 0=リトルエンディアン, 1=ビッグエンディアン
	s8	tag_id_is_crc;	//< 8: タグIDオプション 0=タグID, 1=タグのCRC値
	s8	has_debug_info;	//< 9: デバッグ情報出力 0=なし, 1=あり
	s8	padding[6];		//< 16バイトアラインメント用パディング
};

//-------------------------------------------------------------------------------------------------
// prototype
class SI_STACK_CORE;
class SI_STACK;
class SI_STACK_MAKER_BASE;

// 通常コールバック関数の型
typedef s32(*SI_FUNC)(SI_STACK st);

// 例外タグ用コールバック関数型
typedef s32(*SI_EXCEPTION_FUNC)(const hash32 tag_id, SI_STACK st);

//*************************************************************************************************
//@brief	タグ設定構造体
//*************************************************************************************************
struct SI_TAG_PARAM
{
	hash32		tag_id_crc;	//< タグID CRC
	SI_FUNC	func;		//< コールバック関数
};


//*************************************************************************************************
//@brief	拡張スクリプトインタープリタ３クラス
//*************************************************************************************************
class CScriptInterpreter
{
	friend class SI_STACK;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	ファイル名取得
	const c8*	GetFileName() const { return m_FileName; }

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルバッファ取得
	const void*	GetFileBuffer() const { return m_FileBuff; }

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルヘッダー情報
	const SI_TAG_PARAM_BIN_HEADER*	GetHeader() const { return this->m_Header; }

	//-------------------------------------------------------------------------------------------------
	//@brief	基本データ（行データ）
	const SI_TAG_PARAM_BIN_RECORD*	GetRecord() const { return this->m_Record; }

	//-------------------------------------------------------------------------------------------------
	//@brief	文字列データ　※スクリプトに保存されているオリジナルデータ
	const c8*	GetOriginalText() const { return this->m_Header ? RCast<const c8*>(this->m_FileBuff) + this->m_Header->text_data_file_offset : NULL; }

	//-------------------------------------------------------------------------------------------------
	//@brief	文字列データ
	const c8*	GetText() const { return this->m_Text ? this->m_Text->text : NULL; }

	//-------------------------------------------------------------------------------------------------
	//@brief	複写された文字列データ
	const c8*	GetDuplicatedText() const { return this->m_DuplicatedText; }

#ifdef SI_USE_TEXT_BUFF
	//-------------------------------------------------------------------------------------------------
	//@brief	文字列データを自動複写用バッファ
	CTextBuff*	GetTextBuffForAutoCopy()	const { return this->m_TextBuffForAutoCopy; }
	b8			IsReuseAutoCopyText()		const { return this->m_IsReuseAutoCopyText; }
#endif//SI_USE_TEXT_BUFF

	c8*	GetPlainTextBuffForAutoCopy()			const { return this->m_PlainTextBuffForAutoCopy; }
	u32	GetPlainTextBuffSizeForAutoCopy()		const { return this->m_PlainTextBuffSizeForAutoCopy; }
	u32	GetPlainTextBuffUsedSizeForAutoCopy()	const { return this->m_PlainTextBuffUsedSizeForAutoCopy; }

	//-------------------------------------------------------------------------------------------------
	//@brief	レコード件数
	u32	GetRecordCount() const { return this->m_Header ? this->m_Header->record_count : 0; }

	//-------------------------------------------------------------------------------------------------
	//@brief	文字列データ領域全体サイズ
	u32	GetTextDataSize() const { return this->m_Header ? SCast<u32>(this->m_Header->text_data_total_size) : 0; }

	//-------------------------------------------------------------------------------------------------
	//@brief	文字列データ数
	u32	GetTextNodeCount() const { return this->m_Header ? SCast<u32>(this->m_Header->text_data_node_count) : 0; }

	//-------------------------------------------------------------------------------------------------
	//@brief	現在の処理レコード
	const SI_TAG_PARAM_BIN_RECORD*	GetCurrentRecord() const { return this->m_CurrentRecord; }

	//-------------------------------------------------------------------------------------------------
	//@brief	現在の処理レコードインデックス
	s32	GetCurrentRecordIndex() const { return this->m_CurrentRecordIndex; }

	//-------------------------------------------------------------------------------------------------
	//@brief	デバッグ情報
	const SI_TAG_PARAM_BIN_DEBUG*			GetDebugInfo()					const { return this->m_DebugInfo; }
	const SI_TAG_PARAM_BIN_DEBUG::RECORD*	GetDebugIndoRecordTop()	const { return this->m_DebugInfoRecordTop; }
	const SI_TAG_PARAM_BIN_DEBUG::TEXT*		GetDebugInfoTextTop()	const { return this->m_DebugInfoTextTop; }

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイル情報
	const SI_TAG_PARAM_BIN_FILE_INFO*	GetFileInfo() const { return this->m_FileInfo; }

	//-------------------------------------------------------------------------------------------------
	//@brief	例外タグを無視
	b8		GetIgnoreExceptionTag() const { return this->m_IgnoreExceptionTag; }
	void	SetIgnoreExceptionTag(const b8 value) { this->m_IgnoreExceptionTag = value; }

	//-------------------------------------------------------------------------------------------------
	//@brief	バイナリデータ
	b8	IsBinData() const { return this->m_IsBinData; }

	//-------------------------------------------------------------------------------------------------
	//@brief	デバッグ情報有無
	b8	HasDebugInfo() const { return this->m_HasDebugInfo; }

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイル情報有無
	b8	HasFileInfo() const { return this->m_HasFileInfo; }

protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	レコードヘッダーサイズを計算（バイナリ形式）
	u32	CalcRecordHeaderSizeForBin() const;

	//-------------------------------------------------------------------------------------------------
	//@brief	レコード前半サイズを計算（バイナリ形式）
	//@note	レコードヘッダー情報＋型情報のサイズ
	u32	CalcHalfRecordSizeForBin(const SI_TAG_PARAM_BIN_RECORD* record) const;

	//-------------------------------------------------------------------------------------------------
	//@brief	レコード全体サイズを計算（バイナリ形式）
	//@note	レコードヘッダー情報＋型情報＋値情報のサイズ
	u32	CalcRecordSizeForBin(const SI_TAG_PARAM_BIN_RECORD* record) const;

	//-------------------------------------------------------------------------------------------------
	//@brief	レコード位置を計算（バイナリ形式）
	const SI_TAG_PARAM_BIN_RECORD*	CalcByteOffsetedRecordForBin(const void* base_ptr, const s32 offset) const;

	//-------------------------------------------------------------------------------------------------
	//@brief	最初のレコード位置を計算（バイナリ形式）
	const SI_TAG_PARAM_BIN_RECORD*	CalcFirstRecordForBin() const;

	//次のレコード位置を計算（バイナリ形式）
	const SI_TAG_PARAM_BIN_RECORD*	CalcNextRecordForBin(const SI_TAG_PARAM_BIN_RECORD* record) const;
	const SI_TAG_PARAM_BIN_RECORD*	CalcNextRecordForBin() const { return this->CalcNextRecordForBin(this->m_CurrentRecord); }

	//-------------------------------------------------------------------------------------------------
	//@brief	レコードの型情報の先頭位置を計算（バイナリ形式）
	//const TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* CalcParamTypeTopForBin(const TAG_PARAM_BIN_CRC_RECORD* record) const;
	const u8*	CalcParamTypeTopForBin(const SI_TAG_PARAM_BIN_RECORD* record) const;

	//-------------------------------------------------------------------------------------------------
	//@brief	レコードの値情報の先頭位置を計算（バイナリ形式）
	const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE*	CalcParamValueTopForBin(const SI_TAG_PARAM_BIN_RECORD* record) const;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ
	CScriptInterpreter() { this->Initialize(); }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	b8	Initialize();

	//-------------------------------------------------------------------------------------------------
	//@brief	全ての解析に使用するタグ情報の設定
	//@note	tag_param の順序を更新するので注意！
	//@note	tag_num、already_sorted には、必ず初期化済みの値を渡す事！（tag_num=0, already_sorted=false）
	//@note	初回のセットが完了すると、tag_num、already_sorted に値を返す為、二回目以降はその値を
	//			受け渡すと、解析・ソートを行わない。
	static b8	SetTagGlobal(SI_TAG_PARAM* tag_param_top, u16& tag_param_count, b8& tag_param_is_sorted);

	//-------------------------------------------------------------------------------------------------
	//@brief	タグ情報の設定
	//@note	tag_param の順序を更新するので注意！
	//@note	tag_num、already_sorted には、必ず初期化済みの値を渡す事！（tag_num=0, already_sorted=false）
	//@note	初回のセットが完了すると、tag_num、already_sorted に値を返す為、二回目以降はその値を
	//			受け渡すと、解析・ソートを行わない。
	b8	SetTag(SI_TAG_PARAM* tag_param_top, u16& tag_param_count, b8& tag_param_is_sorted);

	//-------------------------------------------------------------------------------------------------
	//@brief	タグ情報の事前初期化
	static b8	InitTagTable(SI_TAG_PARAM* tag_param_top, u16& tag_param_count, b8& tag_param_is_sorted);

	//-------------------------------------------------------------------------------------------------
	//@brief	拡張パラメータをセット
	void	SetExtParam(void* ext_param);

	//-------------------------------------------------------------------------------------------------
	//@brief	パラメータ解析用拡張テキストバッファをセット（テキスト形式時専用）
	void	SetParamExtTextBuff(const u32 size, c8* buff);
	void	ClearParamExtTextBuff() { this->SetParamExtTextBuff(0, NULL); }

	//-------------------------------------------------------------------------------------------------
	//@brief	パラメータ解析用拡張ワークバッファをセット（テキスト形式時専用）
	void	SetParamExtWorkBuff(const u32 size, c8* buff);
	void	ClearParamExtWorkBuff() { this->SetParamExtWorkBuff(0, NULL); }

	//-------------------------------------------------------------------------------------------------
	//@brief	標準例外タグコールバック設定
	static void	SetExceptionFuncDefault(SI_EXCEPTION_FUNC func) { s_ExceptionCallbackDefault = func; }

	//-------------------------------------------------------------------------------------------------
	//@brief	例外タグコールバック設定
	void	SetExceptionFunc(SI_EXCEPTION_FUNC func) { this->m_ExceptionCallback = func; }

	//-------------------------------------------------------------------------------------------------
	//@brief	スクリプトの設定
	b8	SetScript(const void* file_buff, const u32 file_size, const SI_FILE_TYPE script_file_type = SI_FILE_TYPE_BIN_OR_SJIS)
	{
		return SetScript(file_buff, file_size, NULL, script_file_type);
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	スクリプトの設定(デバッグ用にファイル名設定)
	b8	SetScript(const void* file_buff, const u32 file_size, const c8* _file_name, const SI_FILE_TYPE script_file_type = SI_FILE_TYPE_BIN_OR_SJIS);

	//-------------------------------------------------------------------------------------------------
	//@brief	スクリプト実行
	b8	Run();
	b8	Run(const b8 ignore_exception_tag);

	//-------------------------------------------------------------------------------------------------
	//@brief	指定したタグのレコードまでスキップ
	//@param[in]	for_next =	true  ... 次のタグ解析では指定のタグのコールバックが発生する
	//							false ... 指定のタグまでスキップする為、次のタグ解析では指定のタグの次のタグのコールバックが発生する
	b8	Skip(const hash32 tag_id_crc, const b8 for_next = true);

	//-------------------------------------------------------------------------------------------------
	//@brief	最後のレコードまでスキップ
	//@note	次のタグ解析では最後のタグのコールバックが発生する
	b8	SkipEnd();

	//-------------------------------------------------------------------------------------------------
	//@brief	全文字列データのメモリへの複写（バイナリ形式時専用）
	//@note	これを事前にやっておくと、コールバック関数には複写された領域の文字列ポインタが渡される
#ifdef SI_USE_EASY_ALLOCATOR
	const c8*	CopyTextAll(IEasyAllocatorPtrProxy allocator, u32& text_buff_size);
	const c8*	CopyTextAll(IEasyAllocatorPtrProxy allocator);
#endif//SI_USE_EASY_ALLOCATOR
#ifdef SI_USE_MEMORY_STAFF
	const c8*	CopyTextAll(CMemoryStaff* mem_man, u32& text_buff_size);
	const c8*	CopyTextAll(CMemoryStaff* mem_man);
#endif//SI_USE_MEMORY_STAFF
#ifdef SI_USE_TEXT_BUFF
	const c8*	CopyTextAll(CTextBuff* text_buff, u32& text_buff_size);
	const c8*	CopyTextAll(CTextBuff* text_buff);
	const c8*	AddTextAll(CTextBuff* text_buff, u32& text_buff_size);
	const c8*	AddTextAll(CTextBuff* text_buff);
#endif//SI_USE_TEXT_BUFF
	const c8*	CopyTextAll(c8* text_buff, const u32 text_buff_size_max, u32& text_buff_size);

	//-------------------------------------------------------------------------------------------------
	//@brief	全文字列データを解析の際に自動的にメモリへ複写（テキスト形式時専用）
#ifdef SI_USE_TEXT_BUFF
	b8	SetTextBuffForAutoCopy(CTextBuff* text_buff, const b8 is_reuse_text = false);
#endif//SI_USE_TEXT_BUFF
	b8	SetTextBuffForAutoCopy(c8* text_buff, const u32 text_buff_size_max);

protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	スクリプト実行（バイナリ形式）
	b8	RunForBin();
	b8	RunForBin(const b8 ignore_exception_tag);

	//-------------------------------------------------------------------------------------------------
	//@brief	次のレコード取得（バイナリ形式）
	const SI_TAG_PARAM_BIN_RECORD*	GetNextRecordForBin(const b8 callback = true);

	//-------------------------------------------------------------------------------------------------
	//@brief	指定したタグのレコードまでスキップ（バイナリ形式）
	//@param[in]	for_next =	true  ... 次のタグ解析では指定のタグのコールバックが発生する
	//							false ... 指定のタグまでスキップする為、次のタグ解析では指定のタグの次のタグのコールバックが発生する
	b8	SkipForBin(const hash32 tag_id_crc, const b8 for_next = true);

	//-------------------------------------------------------------------------------------------------
	//@brief	最後のレコードまでスキップ（バイナリ形式）
	//@note	次のタグ解析では最後のタグのコールバックが発生する
	b8	SkipEndForBin();

	//-------------------------------------------------------------------------------------------------
	//@brief	全文字列データのメモリへの複写（バイナリ形式時専用）
	//@note	これを事前にやっておくと、コールバック関数には複写された領域の文字列ポインタが渡される
#ifdef SI_USE_EASY_ALLOCATOR
	const c8*	CopyTextAllForBin(IEasyAllocatorPtrProxy allocator, u32& text_buff_size);
	const c8*	CopyTextAllForBin(IEasyAllocatorPtrProxy allocator);
#endif//SI_USE_EASY_ALLOCATOR
#ifdef SI_USE_MEMORY_STAFF
	const c8*	CopyTextAllForBin(CMemoryStaff* mem_man, u32& text_buff_size);
	const c8*	CopyTextAllForBin(CMemoryStaff* mem_man);
#endif//SI_USE_MEMORY_STAFF
#ifdef SI_USE_TEXT_BUFF
	const c8*	CopyTextAllForBin(CTextBuff* text_buff, u32& text_buff_size);
	const c8*	CopyTextAllForBin(CTextBuff* text_buff);
	const c8*	AddTextAllForBin(CTextBuff* text_buff, u32& text_buff_size);
	const c8*	AddTextAllForBin(CTextBuff* text_buff);
#endif//SI_USE_TEXT_BUFF
	const c8*	CopyTextAllForBin(c8* text_buff, const u32 text_buff_size_max, u32& text_buff_size);

	//-------------------------------------------------------------------------------------------------
	//@brief	文字列を取得（バイナリ形式）
	//@note	文字列の複写（CopyTextAll）が行われていると、複写先の文字列が返る
	const c8*	GetTextForBin(const u32 offset) const;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	タグIDのCRCからタグID（文字列）を取得（バイナリ形式）　※デバッグ用
	const c8*	SearchTagIDForBin(const hash32 tag_id_crc) const;

protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	スクリプト実行（テキスト形式）
	b8	RunForText();
	b8	RunForText(const b8 ignore_exception_tag);

	//-------------------------------------------------------------------------------------------------
	//@brief	レコード情報格納用バッファをセットアップ（テキスト形式）
	void	SetupRecordBuffForText(const u32 param_count_max, SI_TAG_PARAM_BIN_RECORD* record_info, u8* param_type_info, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_value_info, const u32 param_text_buff_size, c8* param_text_buff, const u32 param_work_buff_size, c8* param_work_buff);

	//-------------------------------------------------------------------------------------------------
	//@brief	次のレコード取得（バイナリ形式）
	const SI_TAG_PARAM_BIN_RECORD*	GetNextRecordForText(const b8 callback = true);

	//-------------------------------------------------------------------------------------------------
	//@brief	指定したタグのレコードまでスキップ（テキスト形式）
	//@param[in]	for_next =	true  ... 次のタグ解析では指定のタグのコールバックが発生する
	//							false ... 指定のタグまでスキップする為、次のタグ解析では指定のタグの次のタグのコールバックが発生する
	b8	SkipForText(const hash32 tag_id_crc, const b8 for_next = true);

	//-------------------------------------------------------------------------------------------------
	//@brief	最後のレコードまでスキップ（テキスト形式）
	//@note	次のタグ解析では最後のタグのコールバックが発生する
	b8	SkipEndForText();

	//-------------------------------------------------------------------------------------------------
	//@brief	全文字列データを解析の際に自動的にメモリへ複写（テキスト形式時専用）
#ifdef SI_USE_TEXT_BUFF
	b8	SetTextBuffForAutoCopyForText(CTextBuff* text_buff, const b8 is_reuse_text = false);
#endif//SI_USE_TEXT_BUFF
	b8	SetTextBuffForAutoCopyForText(c8* text_buff, const u32 text_buff_size_max);

	//-------------------------------------------------------------------------------------------------
	//@brief	文字列を取得（テキスト形式）
	const c8* GetTextForText(const u32 offset) const;

#if 0
public:
	// 外部定義されたパラメータの値をセットする　※基本的に使用禁止
	static void	SetParamType(       TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* param_types,                                                      const s32 param_index, const SI_STACK_TYPE param_type);
	static void	SetParamType(       u8*                                param_types,                                                      const s32 param_index, const SI_STACK_TYPE param_type);
	static void	SetParamValueInt(   TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* param_types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_values, const s32 param_index, const s32  param_value);
	static void	SetParamValueInt(   u8*                                param_types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_values, const s32 param_index, const s32  param_value);
	static void	SetParamValueFloat( TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* param_types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_values, const s32 param_index, const f32 param_value);
	static void	SetParamValueFloat( u8*                                param_types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_values, const s32 param_index, const f32 param_value);
	static void	SetParamValueString(TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* param_types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_values, const s32 param_index, const c8*   param_value);
	static void	SetParamValueString(u8*                                param_types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_values, const s32 param_index, const c8*   param_value);
#endif // 0

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	SI関数：ダミー　※何もしない
	static s32	_si_DUMMY(SI_STACK st);

protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	タグ情報の検索
	const SI_TAG_PARAM*	SearchTagParam(hash32 tag_id_crc);
	const SI_TAG_PARAM*	SearchTagParamInner(hash32 tag_id_crc, u32 tag_count, const SI_TAG_PARAM* tag_param_top) const;

protected:
	// グローバル設定
	// タグ設定・全体使用
	static u32				s_TagParamCountGlobal;
	static SI_TAG_PARAM*	s_TagParamTopGlobal;

	// 例外タグコールバック・初期設定
	static SI_EXCEPTION_FUNC	s_ExceptionCallbackDefault;

	// タグ設定
	u32						m_TagParamCount;
	SI_TAG_PARAM*			m_TagParamTop;
	const SI_TAG_PARAM*		m_TagParamCache;

	// 拡張パラメータ
	void*	m_ExtParam;

	// 例外タグコールバック
	SI_EXCEPTION_FUNC	m_ExceptionCallback;

	// ファイル名
	const c8*	m_FileName;

	// ファイルバッファ
	const void*	m_FileBuff;

	// ファイルサイズ
	u32	m_FileSize;

	// スクリプトファイル種別
	SI_FILE_TYPE	m_ScriptFileType;

	// ファイルヘッダー情報（バイナリ形式時専用）
	const SI_TAG_PARAM_BIN_HEADER*	m_Header;

	// 基本データ（行データ）（バイナリ形式時専用）
	const SI_TAG_PARAM_BIN_RECORD*	m_Record;

	// 文字列データ（バイナリ形式時専用）
	const SI_TAG_PARAM_BIN_TEXT*	m_Text;

	// 複写された文字列データ（バイナリ形式時専用）
	c8*	m_DuplicatedText;

#ifdef SI_USE_TEXT_BUFF
	//文字列データを自動複写用バッファ（テキスト形式時専用）
	CTextBuff*	m_TextBuffForAutoCopy;
	b8			m_IsReuseAutoCopyText;
#endif//SI_USE_TEXT_BUFF

	c8*	m_PlainTextBuffForAutoCopy;
	u32	m_PlainTextBuffSizeForAutoCopy;
	u32	m_PlainTextBuffUsedSizeForAutoCopy;

	// 現在の処理レコード
	const SI_TAG_PARAM_BIN_RECORD*	m_CurrentRecord;

	// 現在の処理レコードインデックス
	u32	m_CurrentRecordIndex;

	// デバッグ情報
	const SI_TAG_PARAM_BIN_DEBUG*			m_DebugInfo;
	const SI_TAG_PARAM_BIN_DEBUG::RECORD*	m_DebugInfoRecordTop;
	const SI_TAG_PARAM_BIN_DEBUG::TEXT*		m_DebugInfoTextTop;

	// ファイル情報
	const SI_TAG_PARAM_BIN_FILE_INFO*	m_FileInfo;

	// 例外タグを無視
	b8	m_IgnoreExceptionTag;

	// バイナリデータ
	b8	m_IsBinData;

	// デバッグ情報有無
	b8	m_HasDebugInfo;

	// ファイル情報有無
	b8	m_HasFileInfo;

	// テキスト形式データ向けレコード情報格納用バッファ
	u32										m_ParamCountMaxForText;
	SI_TAG_PARAM_BIN_RECORD*				m_RecordInfoForText;
	u8*										m_ParamTypeInfoForText;
	SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE*	m_ParamValueInfoForText;
	u32										m_ParamTextBuffSizeForText;
	c8*										m_ParamTextBuffForText;
	u32										m_ParamWorkBuffSizeForText;
	c8*										m_ParamWorkBuffForText;
	u32										m_ParamExtTextBuffSizeForText;
	c8*										m_ParamExtTextBuffForText;
	u32										m_ParamExtWorkBuffSizeForText;
	c8*										m_ParamExtWorkBuffForText;
	u32										m_CurrParsePos;
	const c8*								m_CurrParsePtr;
	u32										m_CurrParseLineNo;
	u32										m_CurrParsePosOnLineNo;
};


//-------------------------------------------------------------------------------------------------
// prototype
class SI_STACK;
class SI_STACK_MAKER_BASE;

//*************************************************************************************************
//@brief	スタック型
//*************************************************************************************************
class SI_STACK_CORE
{
	friend class SI_STACK;
	friend class SI_STACK_MAKER_BASE;
	//friend const c8* siGetStackString(const SI_STACK& st);
	friend class CScriptInterpreter;

private:
#ifdef SI_DEBUG_MODE
	//*************************************************************************************************
	//@brief	デバッグ情報（デバッガで確認する為だけの情報）
	struct DEBUF_INFO {
		b8			m_IsBinary = false;
		const c8*	m_TagID = nullptr;
		hash32		m_TagIDCRC;
		u16			m_Row = 0;
		u32			m_RecordIndex = 0;
	};
#endif//SI_DEBUG_MODE

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ
	//SI_STACK_CORE(CScriptInterpreter* si, const TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* param_type_top, const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_value_top, const u32 count, void* ext_param);
	SI_STACK_CORE(CScriptInterpreter* si, const u8* param_type_top, const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_value_top, const u32 count, void* ext_param);

private:
#if 0
	//-------------------------------------------------------------------------------------------------
	//@brief	スタックの基本情報をセット
	void	MakeStack(CScriptInterpreter* si, const TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* param_type_top, const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_value_top, const u32 count, void* ext_param);
	void	MakeStack(CScriptInterpreter* si, const u8*                                param_type_top, const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_value_top, const u32 count, void* ext_param);
#endif // 0

#ifdef SI_DEBUG_MODE
	//-------------------------------------------------------------------------------------------------
	//@brief	デバッグ情報をセット
	void	SetDebugInfo(const b8 is_binary, const c8* tag_id, const hash32 tag_id_crc, const u16 row, const u32 record_idex);
#endif//SI_DEBUG_MODE

#if 0
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	スタック基本情報をセット　※外部からの情報を扱う場合に使用（基本的に使用禁止）
	void	MakeStackEx(CScriptInterpreter* si, const TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* param_type_top, const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_value_top, const u32 count, void* ext_param);
	void	MakeStackEx(CScriptInterpreter* si, const u8*                                param_type_top, const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* param_value_top, const u32 count, void* ext_param);
#endif // 0

private:
	// フィールド
	CScriptInterpreter*	m_SI;
	//const SI_TAG_PARAM_BIN_RECORD::PARAM_TYPE*	m_ParamTypeTop;
	const u8*				m_ParamTypeTop;
	const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE*	m_ParamValueTop;
	u32		m_Count;
	void*	m_ExtParam;

#ifdef SI_DEBUG_MODE
	DEBUF_INFO	m_DebugInfo;	//< デバッグ情報（デバッガで確認する為だけの情報）
#endif//SI_DEBUG_MODE
};


//*************************************************************************************************
//@brief	スタック
//*************************************************************************************************
class SI_STACK
{
	friend const c8* siGetStackString(const SI_STACK& st);
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ
	SI_STACK(SI_STACK_CORE& core) : m_Core(core), m_CurrIndex(0) {}
	SI_STACK(const SI_STACK& stack) : m_Core(stack.m_Core), m_CurrIndex(stack.m_CurrIndex) {}

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	CScriptInterpreter本体を取得
	CScriptInterpreter*	GetSI() { return this->m_Core.m_SI; }

	//-------------------------------------------------------------------------------------------------
	//@brief	拡張パラメータを取得
	void*	GetExtParam() const { return this->m_Core.m_ExtParam; }

	//-------------------------------------------------------------------------------------------------
	//@brief	パラメータ（スタック）の数を取得
	u32		GetCount() const { return this->m_Core.m_Count; }

	//-------------------------------------------------------------------------------------------------
	//@brief	パラメータ（スタック）のカレントインデックスを取得
	s32		GetCurrIndex() const { return this->m_CurrIndex; }

	//-------------------------------------------------------------------------------------------------
	//@brief	パラメータ（スタック）のカレントインデックスからの残数をチェック
	s32		GetRemain() const { return SCast<s32>(this->m_Core.m_Count) - this->m_CurrIndex; }
	b8		IsEnoughRemain(const s32 nessary) const { return SCast<s32>(this->m_Core.m_Count) - this->m_CurrIndex >= nessary; }
	b8		HasRemain() const { return SCast<s32>(this->m_Core.m_Count) > this->m_CurrIndex; }
	b8		IsLimit() const { return SCast<s32>(this->m_Core.m_Count) <= this->m_CurrIndex; }

	//-------------------------------------------------------------------------------------------------
	//@brief	パラメータ（スタック）の型を取得
	SI_INLINE SI_STACK_TYPE	GetCurrType() const;
	SI_INLINE b8	CurrIsInt() const;			//< SI_INT型かSI_FLOAT型の時にtrue
	SI_INLINE b8	CurrIsIntStrict() const;	//< SI_INT型の時にだけ true
	SI_INLINE b8	CurrIsFloat() const;		//< SI_FLOAT型かSI_INT型の時にtrue
	SI_INLINE b8	CurrIsFloatStrict() const;	//< SI_FLOAT型の時にだけtrue
	SI_INLINE b8	CurrIsString() const;		//< SI_STRING型の時にtrue

	//-------------------------------------------------------------------------------------------------
	//@brief	パラメータ（スタック）の値を取得
	SI_INLINE const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE	GetCurrValue() const;
	SI_INLINE const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE	GetCurrValue_inc();
	SI_INLINE s32			GetCurrValueInt() const;
	SI_INLINE s32			GetCurrValueInt_inc();
	SI_INLINE f32			GetCurrValueFloat() const;
	SI_INLINE f32			GetCurrValueFloat_inc();
	SI_INLINE const c8*		GetCurrValueString() const;
	SI_INLINE const c8*		GetCurrValueString_inc();
	SI_INLINE u32			GetCurrValueStringOffset() const;
	SI_INLINE u32			GetCurrValueStringOffset_inc();
	SI_INLINE hash32		GetCurrValueCRC() const;
	SI_INLINE hash32		GetCurrValueCRC_inc();

	//-------------------------------------------------------------------------------------------------
	//@brief	パラメータ（スタック）のカレントインデックスを先頭に戻す
	void	Rewind() { this->m_CurrIndex = 0; }

	//-------------------------------------------------------------------------------------------------
	//@brief	パラメータ（スタック）のカレントインデックスを操作　※ +演算子と-演算子、[]演算子、後置++演算子、後置--演算子はコピーを返す
	//SI_STACK* operator->(void) const {return this;}
	SI_INLINE SI_STACK&	operator=(const SI_STACK& o);
	SI_STACK_OPE_INLINE SI_STACK&		operator++(void);
	SI_STACK_OPE_INLINE const SI_STACK	operator++(const s32);
	SI_STACK_OPE_INLINE SI_STACK&		operator+=(const s32 n);
	SI_STACK_OPE_INLINE const SI_STACK	operator+(const s32 n) const;
	SI_STACK_OPE_INLINE SI_STACK&		operator--(void);
	SI_STACK_OPE_INLINE const SI_STACK	operator--(const s32);
	SI_STACK_OPE_INLINE SI_STACK&		operator-=(const s32 n);
	SI_STACK_OPE_INLINE const SI_STACK	operator-(const s32 n) const;
	SI_STACK_OPE_INLINE const SI_STACK	operator[](const s32 index) const;

#if defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
	void	PrintStackOpeError(const s32 ope) const;
	void	PrintStackAccessError() const;
#endif//defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)

private:
	//-------------------------------------------------------------------------------------------------
	//@brief	パラメータ（スタック）の型を取得
	SI_INLINE SI_STACK_TYPE	GetCurrTypeDirect() const; //< ※直接使用禁止

private:
	// フィールド
	SI_STACK_CORE&	m_Core;
	s32				m_CurrIndex;
};

//*************************************************************************************************
//@brief	スタック生成用基本クラス　※直接使用しない
//*************************************************************************************************
class SI_STACK_MAKER_BASE
{
protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	スタックに値種別をセット
	//void	SetParamType(TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* types, const s32 param_index, const SI_STACK_TYPE param_type);
	void	SetParamType(u8* types, const s32 param_index, const SI_STACK_TYPE param_type);

	//-------------------------------------------------------------------------------------------------
	//@brief	スタックに値をセット
	//b8	SetParamValueBaseInt(   TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* values, const u32 param_count, const s32 param_index, const s32  param_value);
	b8	SetParamValueBaseInt(u8* types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* values, const u32 param_count, const s32 param_index, const s32  param_value);
	//b8	SetParamValueBaseFloat( TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* values, const u32 param_count, const s32 param_index, const f32 param_value);
	b8	SetParamValueBaseFloat(u8* types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* values, const u32 param_count, const s32 param_index, const f32 param_value);
	//b8 SetParamValueBaseString(TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE* types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* values, const u32 param_count, const s32 param_index, const c8*   param_value);
	b8	SetParamValueBaseString(u8* types, SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* values, const u32 param_count, const s32 param_index, const c8* param_value, c8* _text_buff = NULL, u32* _use_text_buff_size = NULL);

	//-------------------------------------------------------------------------------------------------
	//@brief	スタックの最大数を変更
	void	AdjustParamCount(SI_STACK_CORE& core, const u32 param_count) const { core.m_Count = param_count; }
};

//*************************************************************************************************
//@brief	スタック生成用クラス
//*************************************************************************************************
template<u32 count>
class SI_STACK_MAKER : public SI_STACK_MAKER_BASE
{
private:
	static const u32	PARAM_COUNT = count;
	static const u32	PARAM_TYPE_BYTES = SI_CALC_RECORD_PARAM_TYPE_BYTE_NUM(count);
	static const u32	PARAM_TEXT_BUFF_SIZE = 1024;
public:
	SI_STACK_MAKER(CScriptInterpreter* si, void* ext_param) : m_Core(si, m_ParamTypes, m_ParamValues, PARAM_COUNT, ext_param), m_Stack(m_Core) {}
	SI_STACK_MAKER(SI_STACK stack) : m_Core(stack.GetSI(), m_ParamTypes, m_ParamValues, PARAM_COUNT, stack.GetExtParam()), m_Stack(m_Core) {}

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	スタックを取得
	SI_STACK&	GetStack() { return this->m_Stack; }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	スタックに値をセット
	b8	SetParamValueInt(const s32 param_index, const s32  param_value) { return this->SetParamValueBaseInt(m_ParamTypes, m_ParamValues, PARAM_COUNT, param_index, param_value); }
	b8	SetParamValueFloat(const s32 param_index, const f32 param_value) { return this->SetParamValueBaseFloat(m_ParamTypes, m_ParamValues, PARAM_COUNT, param_index, param_value); }
	b8	SetParamValueString(const s32 param_index, const c8* param_value) { return this->SetParamValueBaseString(m_ParamTypes, m_ParamValues, PARAM_COUNT, param_index, param_value); }

	//-------------------------------------------------------------------------------------------------
	//@brief	スタックの最大数を変更　※テンプレートのパラメータで指定した数より大きくは指定不可
	b8	AdjustParamCount(const u32 param_count) {
		if (param_count >= PARAM_COUNT) return false;
		SI_STACK_MAKER_BASE::AdjustParamCount(this->m_Core, param_count);
		return true;
	}

private:
	// フィールド
	//TAG_PARAM_BIN_CRC_RECORD::PARAM_TYPE	m_ParamTypes[PARAM_COUNT];
	u8										m_ParamTypes[PARAM_TYPE_BYTES];
	SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE	m_ParamValues[PARAM_COUNT];
	SI_STACK_CORE							m_Core;
	SI_STACK								m_Stack;
};



//*************************************************************************************************
//@brief	スタック操作用外部関数
//*************************************************************************************************

//-------------------------------------------------------------------------------------------------
//@brief	パラメータ（スタック）の数を取得
SI_INLINE u32	siGetStackCount(const SI_STACK& st);

//-------------------------------------------------------------------------------------------------
//@brief	パラメータ（スタック）のカレントインデックスを取得
SI_INLINE s32	siGetStackIndex(const SI_STACK& st);

//-------------------------------------------------------------------------------------------------
//@brief	パラメータ（スタック）のカレントインデックスからの残数をチェック
SI_INLINE s32	siGetStackRemain(const SI_STACK& st);
SI_INLINE b8	siIsEnoughStackRemain(const SI_STACK& st, const s32 nessary);
SI_INLINE b8	siHasStackRemain(const SI_STACK& st);
SI_INLINE b8	siIsLimitStack(const SI_STACK& st);

//-------------------------------------------------------------------------------------------------
//@brief	パラメータ（スタック）の型を取得／判定
//SI_INLINE SI_STACK_TYPE	siGetStackTypeDirect(const SI_STACK& st);
SI_INLINE SI_STACK_TYPE	siGetStackType(const SI_STACK& st);
SI_INLINE b8	siIsInt(const SI_STACK& st);			//< SI_INT型かSI_FLOAT型の時にtrue
SI_INLINE b8	siIsIntStrict(const SI_STACK& st);		//< SI_INT型の時にだけ true
SI_INLINE b8	siIsFloat(const SI_STACK& st);			//< SI_FLOAT型かSI_INT型の時にtrue
SI_INLINE b8	siIsFloatStrict(const SI_STACK& st);	//< SI_FLOAT型の時にだけtrue
SI_INLINE b8	siIsString(const SI_STACK& st);			//< SI_STRING型の時にtrue
#ifdef SI_DEBUG_MODE
#define SI_ASSERT_STACK_IS_INT(st)				ASSERT(siIsInt(st))
#define SI_ASSERT_STACK_IS_INT_STRICT(st)		ASSERT(siIsIntStrict(st))
#define SI_ASSERT_STACK_IS_FLOAT(st)			ASSERT(siIsFloat(st))
#define SI_ASSERT_STACK_IS_FLOAT_STRICT(st)	ASSERT(siIsFloatStrict(st))
#define SI_ASSERT_STACK_IS_STRING(st)			ASSERT(siIsString(st))
#else//SI_DEBUG_MODE
#define SI_ASSERT_STACK_IS_INT(st)
#define SI_ASSERT_STACK_IS_INT_STRICT(st)
#define SI_ASSERT_STACK_IS_FLOAT(st)
#define SI_ASSERT_STACK_IS_FLOAT_STRICT(st)
#define SI_ASSERT_STACK_IS_STRING(st)
#endif//SI_DEBUG_MODE

//-------------------------------------------------------------------------------------------------
//@brief	パラメータ（スタック）の値を取得
SI_INLINE const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE	siGetStackValue(const SI_STACK& st);
SI_INLINE const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE	siGetStackValue_inc(SI_STACK& st);
SI_INLINE s32			siGetStackInt(const SI_STACK& st);
SI_INLINE s32			siGetStackInt_inc(SI_STACK& st);
SI_INLINE f32			siGetStackFloat(const SI_STACK& st);
SI_INLINE f32			siGetStackFloat_inc(SI_STACK& st);
SI_INLINE const c8*		siGetStackString(const SI_STACK& st);
SI_INLINE const c8*		siGetStackString_inc(SI_STACK& st);
SI_INLINE u32			siGetStackStringOffset(const SI_STACK& st);
SI_INLINE u32			siGetStackStringOffset_inc(SI_STACK& st);
SI_INLINE hash32		siGetStackCRC(const SI_STACK& st);
SI_INLINE hash32		siGetStackCRC_inc(SI_STACK& st);
SI_INLINE void			siGetStackVector3(SI_VECTOR3& v, const SI_STACK& st, const f32 v3 = 1.0f);
SI_INLINE void			siGetStackVector3_inc(SI_VECTOR3& v, SI_STACK& st, const f32 v3 = 1.0f);
SI_INLINE void			siGetStackVector4(SI_VECTOR4& v, const SI_STACK& st);
SI_INLINE void			siGetStackVector4_inc(SI_VECTOR4& v, SI_STACK& st);



//*************************************************************************************************
//@brief	タグ定義テーブル設定用補助マクロ
//*************************************************************************************************

//-------------------------------------------------------------------------------------------------
//@brief	タグ定義テーブル自動初期化用マクロ
#ifdef SI_ENABLE_AUTO_INIT_TAG_TABLE
#define SI_TAG_TABLE_AUTO_INIT_STATIC(name) \
			static	void __attribute__((constructor)) name##_AutoInit() \
			{ \
				CScriptInterpreter::InitTagTable(name, name##_Count, name##_IsSorted); \
			}

#define SI_TAG_TABLE_AUTO_INIT_CLASS(class_name, name) \
			void class_name::name##_AutoInit() \
			{ \
				CScriptInterpreter::InitTagTable(name, name##_Count, name##_IsSorted); \
			}

#define SI_TAG_TABLE_AUTO_INIT_CLASS_DECLARE(name) \
			static	void __attribute__((constructor)) name##_AutoInit()
#else//SI_ENABLE_AUTO_INIT_TAG_TABLE
#define SI_TAG_TABLE_AUTO_INIT_STATIC(name)
#define SI_TAG_TABLE_AUTO_INIT_CLASS(class_name, name)
#define SI_TAG_TABLE_AUTO_INIT_CLASS_DECLARE(name)
#endif//SI_ENABLE_AUTO_INIT_TAG_TABLE

//-------------------------------------------------------------------------------------------------
//@brief	クラスメンバー変数の宣言用マクロ
#define SI_TAG_TABLE_DECLARE_ON_CLASS(name) \
		static	u16 name##_Count; \
		static	b8 name##_IsSorted; \
		static SI_TAG_PARAM name[]; \
		SI_TAG_TABLE_AUTO_INIT_CLASS_DECLARE(name)

//-------------------------------------------------------------------------------------------------
//@brief	クラスメンバー変数の実体（開始）定義用マクロ
#define SI_TAG_TABLE_INSTANCE_ON_CLASS_BEGIN(class_name, name) \
		u16 class_name::name##_Count = 0; \
		b8 class_name::name##_IsSorted = false; \
		SI_TAG_PARAM class_name::name

//-------------------------------------------------------------------------------------------------
//@brief	クラスメンバー変数の実体（終了）定義用マクロ
#define SI_TAG_TABLE_INSTANCE_ON_CLASS_END(class_name, name) \
		SI_TAG_TABLE_AUTO_INIT_CLASS(class_name, name)

//-------------------------------------------------------------------------------------------------
//@brief	クラスアクセッサメソッドの宣言（のみ）用マクロ
#define SI_TAG_TABLE_ACCESSOR_DECLARE_ON_CLASS(method_name) \
		static	u16& method_name##_Count(); \
		static	b8& method_name##_IsSorted(); \
		static SI_TAG_PARAM* method_name();

//-------------------------------------------------------------------------------------------------
//@brief	クラスアクセッサメソッドの宣言＆実体定義用マクロ
#define SI_TAG_TABLE_ACCESSOR_ON_CLASS(method_name, name) \
		static	u16& method_name##_Count(){ return name##_Count; } \
		static	b8& method_name##_IsSorted(){ return name##_IsSorted; } \
		static SI_TAG_PARAM* method_name(){ return name; }

//-------------------------------------------------------------------------------------------------
//@brief	クラスアクセッサメソッドの実体定義用マクロ
#define SI_TAG_TABLE_ACCESSOR_INSTANCE_ON_CLASS(class_name, method_name, name) \
		u16& class_name::method_name##_Count(){ return name##_Count; } \
		b8& class_name::method_name##_IsSorted(){ return name##_IsSorted; } \
		SI_TAG_PARAM* class_name::method_name(){ return name; }

//-------------------------------------------------------------------------------------------------
//@brief	グローバル変数／namespace変数の参照宣言用マクロ
#define SI_TAG_TABLE_EXTERN(name) \
		extern u16 name##_Count; \
		extern b8 name##_IsSorted; \
		extern SI_TAG_PARAM name[]

//-------------------------------------------------------------------------------------------------
//@brief	グローバル変数／namespace変数の実体（開始）定義用マクロ
#define SI_TAG_TABLE_INSTANCE_BEGIN(name) \
		u16 name##_Count = 0; \
		b8 name##_IsSorted = false; \
		SI_TAG_PARAM name

//-------------------------------------------------------------------------------------------------
//@brief	グローバル変数／namespace変数の実体（終了）定義用マクロ
#define SI_TAG_TABLE_INSTANCE_END(name) \
		SI_TAG_TABLE_AUTO_INIT_STATIC(name)

//-------------------------------------------------------------------------------------------------
//@brief	static変数の実体（開始）定義用マクロ
#define SI_TAG_TABLE_STATIC_INSTANCE_BEGIN(name) \
		static	u16 name##_Count = 0; \
		static	b8 name##_IsSorted = false; \
		static SI_TAG_PARAM name

//-------------------------------------------------------------------------------------------------
//@brief	static変数の実体（終了）定義用マクロ
#define SI_TAG_TABLE_STATIC_INSTANCE_END(name) \
		SI_TAG_TABLE_AUTO_INIT_STATIC(name)

//-------------------------------------------------------------------------------------------------
//@brief	SetTag()へのタグ設定パラメータ指定用マクロ（通常変数による受け渡し用）
#define SI_SET_TAG_PARAM(name) name, name##_Count, name##_IsSorted

//-------------------------------------------------------------------------------------------------
//@brief	SetTag()へのタグ設定パラメータ指定用マクロ（クラスのメンバー変数による受け渡し用）
#define SI_SET_TAG_PARAM_CLASS(class_name, name) class_name::name, class_name::name##_Count, class_name::name##_IsSorted

//-------------------------------------------------------------------------------------------------
//@brief	SetTag()へのタグ設定パラメータ指定用マクロ（関数による受け渡し用）
#define SI_SET_TAG_PARAM_FUNC(func_name) func_name(), func_name##_Count(), func_name##_IsSorted()

//-------------------------------------------------------------------------------------------------
//@brief	SetTag()へのタグ設定パラメータ指定用マクロ（クラスのアクセッサメソッドによる受け渡し用）
#define SI_SET_TAG_PARAM_CLASS_METHOD(class_name, name) class_name::name(), class_name::name##_Count(), class_name::name##_IsSorted()

//-------------------------------------------------------------------------------------------------
//@brief	タグ定義用マクロ
#define SI_TAG(tag, func_name) { SI_CRC_FUNC(#tag), func_name }
#define SI_TAG_C(tag, class_name, method_name) { SI_CRC_FUNC(#tag), class_name::method_name }
#define SI_TAG_1(tag) { SI_CRC_FUNC(#tag), tag }
#define SI_TAG_C1(tag, class_name) { SI_CRC_FUNC(#tag), class_name::tag }
#define SI_TAG_2(tag, func_prefix) { SI_CRC_FUNC(#tag), func_prefix##tag }
#define SI_TAG_C2(tag, class_name, method_prefix) { SI_CRC_FUNC(#tag), class_name::method_prefix##tag }
#define SI_TAG_TERM() { 0, NULL }//終端


POISON_END

//-------------------------------------------------------------------------------------------------
//インライン展開
#include "script_interpreter.inl"
#define SI_DUMMY_FOR_IDE	// ※script_interpreter.inl のコードが、IDEの色替え機能に反応するようにする為のダミーマクロ（あまりうまくいかないが...） ※#include "script_interpreter.inl"より後に書く事！


//-------------------------------------------------------------------------------------------------
// pragma
#ifdef LIB_SYSTEM_WIN
#pragma warning(pop)
#endif


#endif//__SCRIPT_INTERPRETER_H__


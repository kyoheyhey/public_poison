﻿#pragma once
#ifndef __SCRIPT_INTERPRETER_OPTIMIZE_H__
#define __SCRIPT_INTERPRETER_OPTIMIZE_H__

//*************************************************************************************************
// バイナリパラメータファイル読み込み処理クラス：最適化設定
// 拡張スクリプトインタープリタクラス
//*************************************************************************************************

POISON_BGN

//-------------------------------------------------------------------------------------------------
// define

//-------------------------------------------------------------------------------------------------
// 冗長デバッグ有効化時（Debug,DebugFastターゲットなど）
#ifdef SI_VERBOSE_DEBUG_MODE

// インライン展開用プリプロセッサ
//#define SI_INLINE_ENABLED				///< インライン展開指定									※プログラムメモリ増大・デバッガ使用効率低下
//#define SI_STACK_OPE_INLINE_ENABLED	///< スタックオペレータのインライン展開指定				※プログラムメモリ増大・デバッガ使用効率低下  ※SI_INLINE_ENABLED との併用必須
#define SI_CHECK_STACK_OPE_ERROR		///< スタックオペレータ操作エラーチェック指定			※プログラムメモリ増大
#define SI_SHOW_STACK_OPE_ERROR			///< スタックオペレータ操作エラー表示指定				※プログラムメモリ増大
//#define SI_ADJUST_STACK_OPE_ERROR		///< スタックオペレータ操作エラー時の範囲超え補正指定	※プログラムメモリ増大

#else//SI_VERBOSE_DEBUG_MODE

//-------------------------------------------------------------------------------------------------
// 冗長デバッグ無効化／デバッグ有効化時（Releaseターゲットなど）
#ifdef SI_DEBUG_MODE
// インライン展開用プリプロセッサ
//#define SI_INLINE_ENABLED				///< インライン展開指定									※プログラムメモリ増大・デバッガ使用効率低下
//#define SI_STACK_OPE_INLINE_ENABLED	///< スタックオペレータのインライン展開指定				※プログラムメモリ増大・デバッガ使用効率低下  ※SI_INLINE_ENABLED との併用必須
#define SI_CHECK_STACK_OPE_ERROR		///< スタックオペレータ操作エラーチェック指定			※プログラムメモリ増大
#define SI_SHOW_STACK_OPE_ERROR			///< スタックオペレータ操作エラー表示指定				※プログラムメモリ増大
//#define SI_ADJUST_STACK_OPE_ERROR		///< スタックオペレータ操作エラー時の範囲超え補正指定	※プログラムメモリ増大

//-------------------------------------------------------------------------------------------------
// 冗長デバッグ無効化／デバッグ無効化時（FinalReleaseターゲットなど）
#else//SI_DEBUG_MODE

// インライン展開用プリプロセッサ
#define SI_INLINE_ENABLED				///< インライン展開指定									※プログラムメモリ増大・デバッガ使用効率低下
#define SI_STACK_OPE_INLINE_ENABLED		///< スタックオペレータのインライン展開指定				※プログラムメモリ増大・デバッガ使用効率低下  ※SI_INLINE_ENABLED との併用必須
//#define SI_CHECK_STACK_OPE_ERROR		///< スタックオペレータ操作エラーチェック指定			※プログラムメモリ増大
//#define SI_SHOW_STACK_OPE_ERROR		///< スタックオペレータ操作エラー表示指定				※プログラムメモリ増大
//#define SI_ADJUST_STACK_OPE_ERROR		///< スタックオペレータ操作エラー時の範囲超え補正指定	※プログラムメモリ増大

#endif//SI_DEBUG_MODE

#endif//SI_VERBOSE_DEBUG_MODE

POISON_END
	
#endif//__SCRIPT_INTERPRETER_OPTIMIZE_H__


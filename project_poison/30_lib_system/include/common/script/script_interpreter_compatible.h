﻿#pragma once
#ifndef __SCRIPT_INTERPRETER_COMPATIBLE_H__
#define __SCRIPT_INTERPRETER_COMPATIBLE_H__

//*************************************************************************************************
// バイナリパラメータファイル読み込み処理クラス：プラットフォーム別ソースコード互換性相違調整用設定
// 拡張スクリプトインタープリタクラス
//*************************************************************************************************


POISON_BGN

//-------------------------------------------------------------------------------------------------
// define

// ハッシュ値生成関数
#define SI_CRC_FUNC CRC32

#if 0
// IEasyAllocatorの使用
#define SI_USE_EASY_ALLOCATOR
#define SI_INCLUDE_GM_POINTER     "gmPointer.h"
#define SI_INCLUDE_EASY_ALLOCATOR "EasyAllocator.h"

// CMemoryStaffの使用
#define SI_USE_MEMORY_STAFF
#define SI_INCLUDE_MEMORY_STAFF "MemoryStaff.h"

// CTextBuffの使用
#define SI_USE_TEXT_BUFF
#define SI_INCLUDE_TEXT_BUFF "TextBuff.h"

// CTextLibの使用
#define SI_USE_TEXT_LIB
#define SI_INCLUDE_TEXT_LIB "TextLib.h"

// タグテーブル自動初期化の有効化
#define SI_ENABLE_AUTO_INIT_TAG_TABLE

// PRINT文の使用
#define SI_DELETE_PRINT	///< PRINT文削除
#define SI_USE_DEBUG_PRINT	///< DEBUG_PRINT系関数使用
#endif // 0

// 標準のスタックサイズ
//#define SI_DEFAULT_STACK_SIZE_LARGE	///< 大
#define SI_DEFAULT_STACK_SIZE_STADARD	///< 中
//#define SI_DEFAULT_STACK_SIZE_SMALL	///< 小

// VECTOR型
typedef f32* SI_VECTOR3;
typedef f32* SI_VECTOR4;

// ASSERT
#define SI_ASSERT_FAIL_SILENT()	libAssert(0)

#ifndef POISON_RELEASE
#define SI_DEBUG_MODE         ///< デバッグモード
#endif // !POISON_RELEASE

#ifdef POISON_DEBUG
#define SI_VERBOSE_DEBUG_MODE ///< 冗長デバッグモード
#endif // POISON_DEBUG


POISON_END
	
#endif//__SCRIPT_INTERPRETER_COMPATIBLE_H__


﻿
//*************************************************************************************************
// バイナリパラメータファイル読み込み処理クラス
// スタック操作（インライン）
//*************************************************************************************************

POISON_BGN


#if defined(SI_DUMMY_FOR_IDE) || defined(SI_INLINE_ENABLED) || defined(SI_INLINE_INSTANCE)

#if defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
#define SHOW_STACK_ACCESS_ERROR() \
	if(this->m_CurrIndex < 0 || this->m_CurrIndex >= SCast<s32>(this->m_Core.m_Count) ) \
	{ \
		this->PrintStackAccessError(); \
	}
#else//defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
#define SHOW_STACK_ACCESS_ERROR()
#endif//defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)

///-------------------------------------------------------------------------------------------------
/// パラメータ（スタック）の型を取得　※直接使用禁止
SI_INLINE SI_STACK_TYPE	SI_STACK::GetCurrTypeDirect() const
{
//	SHOW_STACK_ACCESS_ERROR();
	const s32 index_of_byte = (this->m_CurrIndex / SI_RECORD_PARAM_TYPE_NUM_OF_BYTE);
	const s32 index_in_byte = (this->m_CurrIndex % SI_RECORD_PARAM_TYPE_NUM_OF_BYTE);
	return SCast<SI_STACK_TYPE>((*(RCast<const u8*>(this->m_Core.m_ParamTypeTop) + index_of_byte) >> (index_in_byte * SI_RECORD_PARAM_TYPE_BITS)) & SI_RECORD_PARAM_TYPE_BITS_MASK);
}

///-------------------------------------------------------------------------------------------------
/// パラメータ（スタック）の型を取得
SI_INLINE SI_STACK_TYPE	SI_STACK::GetCurrType() const
{
	SI_STACK_TYPE type = this->GetCurrTypeDirect();
	return type == SI_STRING_DIRECT ? SI_STRING : type;
}
SI_INLINE b8	SI_STACK::CurrIsInt() const			//< SI_INT型かSI_FLOAT型の時にtrue
{
	SI_STACK_TYPE type = this->GetCurrTypeDirect();
	return type == SI_INT || type == SI_FLOAT;
}
SI_INLINE b8	SI_STACK::CurrIsIntStrict() const		//< SI_INT型の時にだけ true
{
	SI_STACK_TYPE type = this->GetCurrTypeDirect();
	return type == SI_INT;
}
SI_INLINE b8	SI_STACK::CurrIsFloat() const			//< SI_FLOAT型かSI_INT型の時にtrue
{
	SI_STACK_TYPE type = this->GetCurrTypeDirect();
	return type == SI_FLOAT || type == SI_INT;
}
SI_INLINE b8	SI_STACK::CurrIsFloatStrict() const	//< SI_FLOAT型の時にだけtrue
{
	SI_STACK_TYPE type = this->GetCurrTypeDirect();
	return type == SI_FLOAT;
}
SI_INLINE b8	SI_STACK::CurrIsString() const			//< SI_STRING型の時にtrue
{
	SI_STACK_TYPE type = this->GetCurrTypeDirect();
	return type == SI_STRING || type == SI_STRING_DIRECT;
}

///-------------------------------------------------------------------------------------------------
/// パラメータ（スタック）の値を取得
#define GET_CURR_VALUE() \
	SHOW_STACK_ACCESS_ERROR(); \
	const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE* value = (RCast<const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE*>(this->m_Core.m_ParamValueTop) + this->m_CurrIndex)
#define GET_CURR_TYPE() \
	const SI_STACK_TYPE type = this->GetCurrTypeDirect()
#define GET_CURR_VALUE_AND_TYPE() \
	GET_CURR_VALUE(); \
	GET_CURR_TYPE()
#define MAKE_CURR_VALUE_TMP() \
	SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE value_tmp = *value; \
	//if(type == SI_STRING) value_tmp.str_direct = this->m_Core.m_SI->GetTextForBin(value->str_offset)
#define RETURN_CURR_VALUE_TMP() \
	return value_tmp;
#define RETURN_CURR_VALUE_INT() \
	return (type == SI_INT ? value->i : type == SI_FLOAT ? SCast<s32>(value->f) : 0)
#define RETURN_CURR_VALUE_FLOAT() \
	return (type == SI_FLOAT ? value->f : type == SI_INT ? SCast<f32>(value->i) : 0.f)
#define RETURN_CURR_VALUE_STRING() \
	return (type == SI_STRING_DIRECT ? this->m_Core.m_SI->GetTextForText(value->str_offset) : type == SI_STRING ? this->m_Core.m_SI->GetTextForBin(value->str_offset) : NULL)
#define RETURN_CURR_VALUE_STRING_OFFSET() \
	return (type == SI_STRING_DIRECT || type == SI_STRING ? value->str_offset : 0)
#define RETURN_CURR_VALUE_CRC() \
	return (type == SI_STRING_DIRECT ? SI_CRC_FUNC(this->m_Core.m_SI->GetTextForText(value->str_offset)) : type == SI_STRING ? SI_CRC_FUNC(this->m_Core.m_SI->GetTextForBin(value->str_offset)) : type == SI_INT ? hash32( value->ui ): hash32( 0 ))
SI_INLINE const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE	SI_STACK::GetCurrValue() const
{
	GET_CURR_VALUE();
	MAKE_CURR_VALUE_TMP();
	RETURN_CURR_VALUE_TMP();
}
SI_INLINE const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE	SI_STACK::GetCurrValue_inc()
{
	GET_CURR_VALUE();
	MAKE_CURR_VALUE_TMP();
	++ this->m_CurrIndex;
	RETURN_CURR_VALUE_TMP();
}
SI_INLINE s32		SI_STACK::GetCurrValueInt() const
{
	GET_CURR_VALUE_AND_TYPE();
	RETURN_CURR_VALUE_INT();
}
SI_INLINE s32		SI_STACK::GetCurrValueInt_inc()
{
	GET_CURR_VALUE_AND_TYPE();
	++ this->m_CurrIndex;
	RETURN_CURR_VALUE_INT();
}
SI_INLINE f32		SI_STACK::GetCurrValueFloat() const
{
	GET_CURR_VALUE_AND_TYPE();
	RETURN_CURR_VALUE_FLOAT();
}
SI_INLINE f32		SI_STACK::GetCurrValueFloat_inc()
{
	GET_CURR_VALUE_AND_TYPE();
	++ this->m_CurrIndex;
	RETURN_CURR_VALUE_FLOAT();
}
SI_INLINE const c8*	SI_STACK::GetCurrValueString() const
{
	GET_CURR_VALUE_AND_TYPE();
	RETURN_CURR_VALUE_STRING();
}
SI_INLINE const c8*	SI_STACK::GetCurrValueString_inc()
{
	GET_CURR_VALUE_AND_TYPE();
	++ this->m_CurrIndex;
	RETURN_CURR_VALUE_STRING();
}
SI_INLINE u32		SI_STACK::GetCurrValueStringOffset() const
{
	GET_CURR_VALUE_AND_TYPE();
	RETURN_CURR_VALUE_STRING_OFFSET();
}
SI_INLINE u32		SI_STACK::GetCurrValueStringOffset_inc()
{
	GET_CURR_VALUE_AND_TYPE();
	++ this->m_CurrIndex;
	RETURN_CURR_VALUE_STRING_OFFSET();
}
SI_INLINE hash32	SI_STACK::GetCurrValueCRC() const
{
	GET_CURR_VALUE_AND_TYPE();
	RETURN_CURR_VALUE_CRC();
}
SI_INLINE hash32	SI_STACK::GetCurrValueCRC_inc()
{
	GET_CURR_VALUE_AND_TYPE();
	++ this->m_CurrIndex;
	RETURN_CURR_VALUE_CRC();
}

#undef SHOW_STACK_ACCESS_ERROR
#undef GET_CURR_VALUE
#undef GET_CURR_TYPE
#undef GET_CURR_VALUE_AND_TYPE
#undef MAKE_CURR_VALUE_TMP
#undef RETURN_CURR_VALUE_TMP
#undef RETURN_CURR_VALUE_INT
#undef RETURN_CURR_VALUE_FLOAT
#undef RETURN_CURR_VALUE_STRING
#undef RETURN_CURR_VALUE_CRC

#endif//defined(SI_DUMMY_FOR_IDE) || defined(SI_INLINE_ENABLED) || defined(SI_INLINE_INSTANCE)

///-------------------------------------------------------------------------------------------------
/// パラメータ（スタック）のカレントインデックスを操作（インライン）
#if defined(SI_DUMMY_FOR_IDE) || defined(SI_INLINE_ENABLED) || defined(SI_INLINE_INSTANCE)
SI_INLINE SI_STACK& SI_STACK::operator=(const SI_STACK& o)
{
	this->m_Core = o.m_Core;
	this->m_CurrIndex = o.m_CurrIndex;
	return *this;
}
#endif//defined(SI_DUMMY_FOR_IDE) || defined(SI_INLINE_ENABLED) || defined(SI_INLINE_INSTANCE)
#if defined(SI_DUMMY_FOR_IDE) || defined(SI_STACK_OPE_INLINE_ENABLED) || defined(SI_INLINE_INSTANCE)
SI_STACK_OPE_INLINE SI_STACK& SI_STACK::operator++(void)
{
#ifdef SI_CHECK_STACK_OPE_ERROR
	const s32 remain = SCast<s32>(this->m_Core.m_Count) - this->m_CurrIndex;
	const b8 is_allow = (remain >= 1);
#if defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
	if(!is_allow)
		this->PrintStackOpeError(1);
#endif//defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
#ifdef SI_ADJUST_STACK_OPE_ERROR
	this->m_CurrIndex += (is_allow ? 1 : 0);
#else//SI_ADJUST_STACK_OPE_ERROR
	++ this->m_CurrIndex;
#endif//SI_ADJUST_STACK_OPE_ERROR
#else//SI_CHECK_STACK_OPE_ERROR
	++ this->m_CurrIndex;
#endif//SI_CHECK_STACK_OPE_ERROR
	return *this;
}
SI_STACK_OPE_INLINE const SI_STACK SI_STACK::operator++(const s32)
{
	SI_STACK prev = *this;
#ifdef SI_CHECK_STACK_OPE_ERROR
	const s32 remain = SCast<s32>(this->m_Core.m_Count) - this->m_CurrIndex;
	const b8 is_allow = (remain >= 1);
#if defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
	if(!is_allow)
		this->PrintStackOpeError(1);
#endif//defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
#ifdef SI_ADJUST_STACK_OPE_ERROR
	this->m_CurrIndex += (is_allow ? 1 : 0);
#else//SI_ADJUST_STACK_OPE_ERROR
	++ this->m_CurrIndex;
#endif//SI_ADJUST_STACK_OPE_ERROR
#else//SI_CHECK_STACK_OPE_ERROR
	++ this->m_CurrIndex;
#endif//SI_CHECK_STACK_OPE_ERROR
	return prev;
}
SI_STACK_OPE_INLINE SI_STACK& SI_STACK::operator+=(const s32 n)
{
#ifdef SI_CHECK_STACK_OPE_ERROR
	b8 is_allow = false;
#ifdef SI_ADJUST_STACK_OPE_ERROR
	s32 n_adj = n;
#endif//SI_ADJUST_STACK_OPE_ERROR
	if(n >= 0)
	{
		const s32 remain = SCast<s32>(this->m_Core.m_Count) - this->m_CurrIndex;
		is_allow = (remain >= n);
	#ifdef SI_ADJUST_STACK_OPE_ERROR
		n_adj = (is_allow ? n : remain);
	#endif//SI_ADJUST_STACK_OPE_ERROR
	}
	else//if(n < 0)
	{
		const s32 remain = this->m_CurrIndex;
		is_allow = (remain >= -n);
	#ifdef SI_ADJUST_STACK_OPE_ERROR
		n_adj = (is_allow ? n : -remain);
	#endif//SI_ADJUST_STACK_OPE_ERROR
	}
#if defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
	if(!is_allow)
		this->PrintStackOpeError(n);
#endif//defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
#ifdef SI_ADJUST_STACK_OPE_ERROR
	this->m_CurrIndex += n_adj;
#else//SI_ADJUST_STACK_OPE_ERROR
	this->m_CurrIndex += n;
#endif//SI_ADJUST_STACK_OPE_ERROR
#else//SI_CHECK_STACK_OPE_ERROR
	this->m_CurrIndex += n;
#endif//SI_CHECK_STACK_OPE_ERROR
	return *this;
}
SI_STACK_OPE_INLINE const SI_STACK SI_STACK::operator+(const s32 n) const
{
	SI_STACK o(*this);
#ifdef SI_CHECK_STACK_OPE_ERROR
	b8 is_allow = false;
#ifdef SI_ADJUST_STACK_OPE_ERROR
	s32 n_adj = n;
#endif//SI_ADJUST_STACK_OPE_ERROR
	if(n >= 0)
	{
		const s32 remain = SCast<s32>(this->m_Core.m_Count) - this->m_CurrIndex;
		is_allow = (remain >= n);
	#ifdef SI_ADJUST_STACK_OPE_ERROR
		n_adj = (is_allow ? n : remain);
	#endif//SI_ADJUST_STACK_OPE_ERROR
	}
	else//if(n < 0)
	{
		const s32 remain = this->m_CurrIndex;
		is_allow = (remain >= -n);
	#ifdef SI_ADJUST_STACK_OPE_ERROR
		n_adj = (is_allow ? n : -remain);
	#endif//SI_ADJUST_STACK_OPE_ERROR
	}
#if defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
	if(!is_allow)
		this->PrintStackOpeError(n);
#endif//defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
#ifdef SI_ADJUST_STACK_OPE_ERROR
	o.m_CurrIndex += n_adj;
#else//SI_ADJUST_STACK_OPE_ERROR
	o.m_CurrIndex += n;
#endif//SI_ADJUST_STACK_OPE_ERROR
#else//SI_CHECK_STACK_OPE_ERROR
	o.m_CurrIndex += n;
#endif//SI_CHECK_STACK_OPE_ERROR
	return o;
}
SI_STACK_OPE_INLINE SI_STACK& SI_STACK::operator--(void)
{
#ifdef SI_CHECK_STACK_OPE_ERROR
	const s32 remain = this->m_CurrIndex;
	const b8 is_allow = (remain >= 1);
#if defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
	if(!is_allow)
		this->PrintStackOpeError(-1);
#endif//defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
#ifdef SI_ADJUST_STACK_OPE_ERROR
	this->m_CurrIndex -= (is_allow ? 1 : 0);
#else//SI_ADJUST_STACK_OPE_ERROR
	-- this->m_CurrIndex;
#endif//SI_ADJUST_STACK_OPE_ERROR
#else//SI_CHECK_STACK_OPE_ERROR
	-- this->m_CurrIndex;
#endif//SI_CHECK_STACK_OPE_ERROR
	return *this;
}
SI_STACK_OPE_INLINE const SI_STACK SI_STACK::operator--(const s32)
{
	SI_STACK prev = *this;
#ifdef SI_CHECK_STACK_OPE_ERROR
	const s32 remain = this->m_CurrIndex;
	const b8 is_allow = (remain >= 1);
#if defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
	if(!is_allow)
		this->PrintStackOpeError(-1);
#endif//defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
#ifdef SI_ADJUST_STACK_OPE_ERROR
	this->m_CurrIndex -= (is_allow ? 1 : 0);
#else//SI_ADJUST_STACK_OPE_ERROR
	-- this->m_CurrIndex;
#endif//SI_ADJUST_STACK_OPE_ERROR
#else//SI_CHECK_STACK_OPE_ERROR
	-- this->m_CurrIndex;
#endif//SI_CHECK_STACK_OPE_ERROR
	return prev;
}
SI_STACK_OPE_INLINE SI_STACK& SI_STACK::operator-=(const s32 n)
{
#ifdef SI_CHECK_STACK_OPE_ERROR
	b8 is_allow = false;
#ifdef SI_ADJUST_STACK_OPE_ERROR
	s32 n_adj = n;
#endif//SI_ADJUST_STACK_OPE_ERROR
	if(n >= 0)
	{
		const s32 remain = this->m_CurrIndex;
		is_allow = (remain >= n);
	#ifdef SI_ADJUST_STACK_OPE_ERROR
		n_adj = (is_allow ? n : remain);
	#endif//SI_ADJUST_STACK_OPE_ERROR
	}
	else//if(n < 0)
	{
		const s32 remain = SCast<s32>(this->m_Core.m_Count) - this->m_CurrIndex;
		is_allow = (remain >= -n);
	#ifdef SI_ADJUST_STACK_OPE_ERROR
		n_adj = (is_allow ? n : -remain);
	#endif//SI_ADJUST_STACK_OPE_ERROR
	}
#if defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
	if(!is_allow)
		this->PrintStackOpeError(-n);
#endif//defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
#ifdef SI_ADJUST_STACK_OPE_ERROR
	this->m_CurrIndex -= n_adj;
#else//SI_ADJUST_STACK_OPE_ERROR
	this->m_CurrIndex -= n;
#endif//SI_ADJUST_STACK_OPE_ERROR
#else//SI_CHECK_STACK_OPE_ERROR
	this->m_CurrIndex -= n;
#endif//SI_CHECK_STACK_OPE_ERROR
	return *this;
}
SI_STACK_OPE_INLINE const SI_STACK SI_STACK::operator-(const s32 n) const
{
	SI_STACK o(*this);
#ifdef SI_CHECK_STACK_OPE_ERROR
	b8 is_allow = false;
#ifdef SI_ADJUST_STACK_OPE_ERROR
	s32 n_adj = n;
#endif//SI_ADJUST_STACK_OPE_ERROR
	if(n >= 0)
	{
		const s32 remain = this->m_CurrIndex;
		is_allow = (remain >= n);
	#ifdef SI_ADJUST_STACK_OPE_ERROR
		n_adj = (is_allow ? n : remain);
	#endif//SI_ADJUST_STACK_OPE_ERROR
	}
	else//if(n < 0)
	{
		const s32 remain = SCast<s32>(this->m_Core.m_Count) - this->m_CurrIndex;
		is_allow = (remain >= -n);
	#ifdef SI_ADJUST_STACK_OPE_ERROR
		n_adj = (is_allow ? n : -remain);
	#endif//SI_ADJUST_STACK_OPE_ERROR
	}
#if defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
	if(!is_allow)
		this->PrintStackOpeError(-n);
#endif//defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
#ifdef SI_ADJUST_STACK_OPE_ERROR
	o.m_CurrIndex -= n_adj;
#else//SI_ADJUST_STACK_OPE_ERROR
	o.m_CurrIndex -= n;
#endif//SI_ADJUST_STACK_OPE_ERROR
#else//SI_CHECK_STACK_OPE_ERROR
	o.m_CurrIndex -= n;
#endif//SI_CHECK_STACK_OPE_ERROR
	return o;
}
SI_STACK_OPE_INLINE const SI_STACK SI_STACK::operator[](const s32 index) const
{
	SI_STACK o(*this);
#ifdef SI_CHECK_STACK_OPE_ERROR
	b8 is_allow = false;
#ifdef SI_ADJUST_STACK_OPE_ERROR
	s32 index_adj = index;
#endif//SI_ADJUST_STACK_OPE_ERROR
	if(index >= 0)
	{
		const s32 remain = SCast<s32>(this->m_Core.m_Count) - this->m_CurrIndex;
		is_allow = (remain >= index);
	#ifdef SI_ADJUST_STACK_OPE_ERROR
		index_adj = (is_allow ? index : remain);
	#endif//SI_ADJUST_STACK_OPE_ERROR
	}
	else//if(index < 0)
	{
		const s32 remain = this->m_CurrIndex;
		is_allow = (remain >= -index);
	#ifdef SI_ADJUST_STACK_OPE_ERROR
		index_adj = (is_allow ? index : -remain);
	#endif//SI_ADJUST_STACK_OPE_ERROR
	}
#if defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
	if(!is_allow)
		this->PrintStackOpeError(index);
#endif//defined(SI_DEBUG_MODE) && defined(SI_CHECK_STACK_OPE_ERROR) && defined(SI_SHOW_STACK_OPE_ERROR)
#ifdef SI_ADJUST_STACK_OPE_ERROR
	o.m_CurrIndex += index_adj;
#else//SI_ADJUST_STACK_OPE_ERROR
	o.m_CurrIndex += index;
#endif//SI_ADJUST_STACK_OPE_ERROR
#else//SI_CHECK_STACK_OPE_ERROR
	o.m_CurrIndex += index;
#endif//SI_CHECK_STACK_OPE_ERROR	return o;
	return o;
}
#endif//defined(SI_DUMMY_FOR_IDE) || defined(SI_STACK_OPE_INLINE_ENABLED) || defined(SI_INLINE_INSTANCE)


///-------------------------------------------------------------------------------------------------
/// スタック操作用外部関数（インライン）

#if defined(SI_DUMMY_FOR_IDE) || defined(SI_INLINE_ENABLED) || defined(SI_INLINE_INSTANCE)

///-------------------------------------------------------------------------------------------------
/// パラメータ（スタック）の数を取得
SI_INLINE u32	siGetStackCount(const SI_STACK& st) { return st.GetCount(); }

///-------------------------------------------------------------------------------------------------
/// パラメータ（スタック）のカレントインデックスを取得
SI_INLINE s32	siGetStackIndex(const SI_STACK& st) { return st.GetCurrIndex(); }

///-------------------------------------------------------------------------------------------------
/// パラメータ（スタック）のカレントインデックスからの残数をチェック
SI_INLINE s32	siGetStackRemain(const SI_STACK& st) { return st.GetRemain(); }
SI_INLINE b8	siIsEnoughStackRemain(const SI_STACK& st, const s32 nessary) { return st.IsEnoughRemain(nessary); }
SI_INLINE b8	siHasStackRemain(const SI_STACK& st) { return st.HasRemain(); }
SI_INLINE b8	siIsLimitStack(const SI_STACK& st) { return st.IsLimit(); }

///-------------------------------------------------------------------------------------------------
/// パラメータ（スタック）の型を取得
//SI_INLINE SI_STACK_TYPE	siGetStackTypeDirect(const SI_STACK& st) { return st.GetCurrTypeDirect(); }
SI_INLINE SI_STACK_TYPE	siGetStackType(const SI_STACK& st) { return st.GetCurrType(); }
SI_INLINE b8	siIsInt(const SI_STACK& st) { return st.CurrIsInt(); }					///< SI_INT型かSI_FLOAT型の時にtrue
SI_INLINE b8	siIsIntStrict(const SI_STACK& st) { return st.CurrIsIntStrict(); }		///< SI_INT型の時にだけ true
SI_INLINE b8	siIsFloat(const SI_STACK& st) { return st.CurrIsFloat(); }				///< SI_FLOAT型かSI_INT型の時にtrue
SI_INLINE b8	siIsFloatStrict(const SI_STACK& st) { return st.CurrIsFloatStrict(); }	///< SI_FLOAT型の時にだけtrue
SI_INLINE b8	siIsString(const SI_STACK& st) { return st.CurrIsString(); }				///< SI_STRING型の時にtrue

///-------------------------------------------------------------------------------------------------
/// パラメータ（スタック）の値を取得
SI_INLINE const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE	siGetStackValue(const SI_STACK& st) { return st.GetCurrValue(); }
SI_INLINE const SI_TAG_PARAM_BIN_RECORD::PARAM_VALUE	siGetStackValue_inc(SI_STACK& st) { return st.GetCurrValue_inc(); }
SI_INLINE s32			siGetStackInt(const SI_STACK& st) { return st.GetCurrValueInt(); }
SI_INLINE s32			siGetStackInt_inc(SI_STACK& st) { return st.GetCurrValueInt_inc(); }
SI_INLINE f32			siGetStackFloat(const SI_STACK& st) { return st.GetCurrValueFloat(); }
SI_INLINE f32			siGetStackFloat_inc(SI_STACK& st) { return st.GetCurrValueFloat_inc(); }
SI_INLINE const c8*		siGetStackString(const SI_STACK& st) { return st.GetCurrValueString(); }
SI_INLINE const c8*		siGetStackString_inc(SI_STACK& st) { return st.GetCurrValueString_inc(); }
SI_INLINE u32			siGetStackStringOffset(const SI_STACK& st) { return st.GetCurrValueStringOffset(); }
SI_INLINE u32			siGetStackStringOffset_inc(SI_STACK& st) { return st.GetCurrValueStringOffset_inc(); }
SI_INLINE hash32		siGetStackCRC(const SI_STACK& st) { return st.GetCurrValueCRC(); }
SI_INLINE hash32		siGetStackCRC_inc(SI_STACK& st) { return st.GetCurrValueCRC_inc(); }
SI_INLINE void			siGetStackVector3(SI_VECTOR3& v, const SI_STACK& st, const f32 v3)
{
//	if(!siIsEnoughStackRemain(st, 3))
//		return;
	v[0] = siGetStackFloat(st);
	v[1] = siGetStackFloat(st[1]);
	v[2] = siGetStackFloat(st[2]);
	v[3] = v3;
}
SI_INLINE void			siGetStackVector3_inc(SI_VECTOR3& v, SI_STACK& st, const f32 v3)
{
//	if(!siIsEnoughStackRemain(st, 3))
//		return;
	v[0] = siGetStackFloat_inc(st);
	v[1] = siGetStackFloat_inc(st);
	v[2] = siGetStackFloat_inc(st);
	v[3] = v3;
}
SI_INLINE void			siGetStackVector4(SI_VECTOR4& v, const SI_STACK& st)
{
//	if(!siIsEnoughStackRemain(st, 4))
//		return;
	v[0] = siGetStackFloat(st);
	v[1] = siGetStackFloat(st[1]);
	v[2] = siGetStackFloat(st[2]);
	v[3] = siGetStackFloat(st[3]);
}
SI_INLINE void			siGetStackVector4_inc(SI_VECTOR4& v, SI_STACK& st)
{
//	if(!siIsEnoughStackRemain(st, 4))
//		return;
	v[0] = siGetStackFloat_inc(st);
	v[1] = siGetStackFloat_inc(st);
	v[2] = siGetStackFloat_inc(st);
	v[3] = siGetStackFloat_inc(st);
}

#endif//defined(SI_DUMMY_FOR_IDE) || defined(SI_INLINE_ENABLED) || defined(SI_INLINE_INSTANCE)


POISON_END


﻿#pragma once
#ifndef __TEXT_UTILITY_H_
#define __TEXT_UTILITY_H_

//-------------------------------------------------------------------------------------------------
// include


POISON_BGN


//*************************************************************************************************
//@brief	テキストコード種別
enum class	TEXT_TYPE
{
	TEXT_TYPE_UTF8 = 0, ///< UTF-8
	TEXT_TYPE_SJIS,     ///< Shift-JIS
	TEXT_TYPE_ASCII,	///< ASCII
};

//*************************************************************************************************
//@brief	タグで呼ぶコールバック
typedef b8(TAG_CALLBACK)(const c8* _str, u32 _length, c8* _dest, u32 _dest_length, u32& _replace_length, void* _ptr);

//*************************************************************************************************
//@brief	タグの引数の限界(コマンド名を含む)
static const u32 TAG_ARG_MAX = 16;
//@brief	引数の文字数限界
static const u32 STRING_MAX = 256;

//*************************************************************************************************
//@brief	名前とコールバックの対応関係
struct TAG_FUNC
{
	TAG_CALLBACK*	func;
	hash32			crc_func_name;
};

//*************************************************************************************************
//@brief	タグ名と置換文字列の構造体
struct TAG_WORD
{
	const c8*	word;			///< 置換文字列
	hash32		tag_name_crc;	///< タグ名CRC
};

//*************************************************************************************************
//@brief	タグ名と置換文字列を配列で持ち歩く構造体
struct TAG_WORD_ARRAY
{
	const TAG_WORD*	tag_word;
	u32				tag_num;
};

//*************************************************************************************************
//@brief	タグ名と置換文字列の構造体
struct CALL_TAG_INFO
{
	TAG_CALLBACK*	func;		///< コールバック
	u32				tag_begin;	///< タグ開始文字
	u32				tag_end;	///< タグ終了文字
};

//*************************************************************************************************
//@brief	汎用文字列操作用ユーティリティクラス
//*************************************************************************************************
class CTextUtility
{
public:
	CTextUtility();
	~CTextUtility();

public:
	///	不正な文字コード
	static const u32 ILLEGAL_CHAR_CODE = 0xffffffff;

public:
	//-------------------------------------------------------------------------------------------------	
	//@brief		Shift_JISからUTF-8の変換
	//@param[in]	sjis_code SJISのコード
	//@return		UTF-8の文字コード
	static u32	SJIStoUTF8(const u32 sjis_code);

	//-------------------------------------------------------------------------------------------------	
	//@brief		UTF-8からShift_JISの変換
	//@param[in]	utf8_code UTF-8のコード
	//@return		Shift_JISの文字コード
	static u32	UTF8toSJIS(const u32 utf8_code);

	//-------------------------------------------------------------------------------------------------	
	//@brief	Shift_JIS変換判定処理（１バイトから１バイト）
	//@return		変換できればtrue
	//@param[in]	c 文字
	static inline b8	IsSJISChar1of1(const c8 c){const u32 u_c = SCast<u32>(*PCast<const u8*>(&c)) & 0xff; return ((/*u_c >= 0x00 &&*/ u_c <= 0x7f) || (u_c >= 0xa1 && u_c <= 0xdf));}

	//@brief	Shift_JIS変換判定処理（1バイトから2バイト）
	//@return		変換できればtrue
	//@param[in]	c 文字
	static inline b8	IsSJISChar1of2(const c8 c){const u32 u_c = SCast<u32>(*PCast<const u8*>(&c)) & 0xff; return ((  u_c >= 0x81 &&   u_c <= 0x9f) || (u_c >= 0xe0 && u_c <= 0xff));}

	//@brief	Shift_JIS変換判定処理（2バイトから2バイト）
	//@return		変換できればtrue
	//@param[in]	c 文字
	static inline b8	IsSJISChar2of2(const c8 c){const u32 u_c = SCast<u32>(*PCast<const u8*>(&c)) & 0xff; return ((  u_c >= 0x40 &&   u_c <= 0x7e) || (u_c >= 0x80 && u_c <= 0xfc));}
	
	
	//-------------------------------------------------------------------------------------------------	
	//@brief	Shift-JISでの全角文字判定
	//@return		変換できればtrue
	//@param[in]	c 文字
	//@note １バイト文字～２バイト文字を、符号なし３２ビット長のコードに拡張したコードで判定
	static b8	IsSJISCharZen(const u32 c){return !IsSJISCharHan(c);}	//全角文字判定

	//@brief	Shift-JISでの半角文字判定
	//@return		変換できればtrue
	//@param[in]	c 文字
	//@note １バイト文字～２バイト文字を、符号なし３２ビット長のコードに拡張したコードで判定
	static b8	IsSJISCharHan(const u32 c);

	//-------------------------------------------------------------------------------------------------	
	//@brief	Shift-JISでの文字列処理
	//@return		変換できればtrue
	//@param[in]	c 文字
	static u32	GetBytesOfSJISChar(const c8 c);
	static u32	GetBytesOfSJISChar(const c8* text_p);
	static u32	GetCharOfSJISChar(const c8* text_p, u32& bytes);
	static u32	GetCharOfSJISCharNG(const c8* text_p);
	static u32	GetCharOfSJISChar1(const c8* text_p);
	static u32	GetCharOfSJISChar2(const c8* text_p);
	
	//-------------------------------------------------------------------------------------------------	
	//@brief		文字長の取得（SJIS）
	//@return		文字長
	//@param[in]	_text		文字列
	static s32	GetLengthSJIS(const c8* _text);

	//-------------------------------------------------------------------------------------------------	
	//@brief		文字列の先頭から指定の文字数だけコピー（SJIS）
	//@return		実際にコピーした文字数
	//@param[out]	_buff		文字列
	//@param[in]	_text		コピーする文字列
	//@param[in]	_offset		先頭から数えてコピーを開始するインデックスCopyTextUTF8
	//@param[in]	_text		コピーする文字数
	//@param[in]	_len
	static s32	CopyTextSJIS(c8* _buff, const c8* _text, const u32 _offset, const u32 _len);

	//-------------------------------------------------------------------------------------------------	
	//@brief		数字情報を全角文字列に変換する（SJIS）
	//@return		実際にコピーした文字数
	//@param[out]	_buff		文字列
	//@param[in]	_bufSize	文字列サイズ
	//@param[in]	_value		数値
	//@param[in]	_digit		桁数
	static s32	ExChangeNumTextSJIS(c8* _buff, const u32 _bufSize , u32 _value , u32 _digit);

	//-------------------------------------------------------------------------------------------------	
	//@brief		文字列を指定の省略記号に置き換える（SJIS）
	//@return		置換後の文字数
	//@param[out]	_buff		結果の文字列
	//@param[in]	_text		元の文字列
	//@param[in]	_offset		先頭から数えてコピーを開始するインデックス
	//@param[in]	_len		文字数
	//@param[in]	_ellipsis	省略記号
	//@param[in]	_is_reverse	戻すかどうか
	static s32	ElideTextSJIS(c8* _buff, const c8* _text, const u32 _offset, const u32 _len, const c8* _ellipsis, b8 _is_reverse);

	//-------------------------------------------------------------------------------------------------	
	//@brief		改行コードを検索する（SJIS）
	//@return		発見した改行コードのアドレス
	//@param[in]	_text			元の文字列
	//@param[in]	crlf_code_bytes	改行コードの指定
	static const c8*	SearchCRLFCodeSJIS(const c8* _text, u32* crlf_code_bytes = NULL);

	//-------------------------------------------------------------------------------------------------	
	//@brief		テキストの最後が改行コードだった場合に削除する（SJIS）
	//@return		削除したかどうか
	//@param[in]	_buff
	//@param[in]	_text
	static b8	EraseLastCRLFCodeSJIS(c8* _buff, const c8* _text);

	//-------------------------------------------------------------------------------------------------	
	//@brief		テキストの改行コードの数を調べる（SJIS）
	//@return		改行コードの数
	//@param[in]	_text			元の文字列
	static u16	GetCRLFCodeNumSJIS(const c8* _text);
	
	//-------------------------------------------------------------------------------------------------	
	//@brief	UTF-8でのバイト判定(1バイト文字の1文字目判定)
	//@return		一文字目ならtrue
	//@param[in]	c 文字
	static inline b8	IsUTF8Char1of1(const c8 c){return ((c & 0x80) == 0x00);}	//1バイト文字の1文字目判定

	//@brief	UTF-8でのバイト判定(2バイト文字の1文字目判定)
	//@return		一文字目ならtrue
	//@param[in]	c 文字
	static inline b8	IsUTF8Char1of2(const c8 c){return ((c & 0xe0) == 0xc0);}	//2バイト文字の1文字目判定

	//@brief	UTF-8でのバイト判定(3バイト文字の1文字目判定)
	//@return		一文字目ならtrue
	//@param[in]	c 文字
	static inline b8	IsUTF8Char1of3(const c8 c){return ((c & 0xf0) == 0xe0);}	//3バイト文字の1文字目判定

	//@brief	UTF-8でのバイト判定(4バイト文字の1文字目判定)
	//@return		一文字目ならtrue
	//@param[in]	c 文字
	static inline b8	IsUTF8Char1of4(const c8 c){return ((c & 0xf8) == 0xf0);}	//4バイト文字の1文字目判定

	//@brief	UTF-8でのバイト判定(2バイト以上文字の2文字目以降判定)
	//@return		2バイト以上文字の2文字目以降ならtrue
	//@param[in]	c 文字
	static inline b8	IsUTF8Char2After(const c8 c){return ((c & 0xc0) == 0x80);}

	//@brief	UTF-8で何バイト文字かを返す関数
	//@return		戻り値がバイト数となる
	//@param[in]	c 文字
	//@note 1バイト目で何バイトか判別
	static inline u32	GetUTF8CharBytes(const c8 c){if(IsUTF8Char1of1(c)) return 1; if(IsUTF8Char1of2(c)) return 2; if(IsUTF8Char1of3(c)) return 3; if(IsUTF8Char1of4(c)) return 4; return 0;}
	
	//-------------------------------------------------------------------------------------------------	
	//@brief	UTF-8での全角文字判定
	//@return		全角文字ならtrue
	//@param[in]	c 文字
	//@note １バイト文字～２バイト文字を、符号なし３２ビット長のコードに拡張したコードで判定
	static b8	IsUTF8CharZen(const u32 c){return !IsUTF8CharHan(c);}

	//@brief	UTF-8での半角文字判定
	//@return		全角文字ならtrue
	//@param[in]	c 文字
	//@note １バイト文字～２バイト文字を、符号なし３２ビット長のコードに拡張したコードで判定
	static b8	IsUTF8CharHan(const u32 c);

	//-------------------------------------------------------------------------------------------------	
	//@brief	UTF-8で何バイトの文字か
	//@return	バイト数
	//@param[in]	c 文字
	static u32	GetBytesOfUTF8Char(const c8 c);

	//@brief	UTF-8で何バイトの文字か
	//@return	バイト数
	//@param[in]	text_p 文字列（一文字目）
	static u32	GetBytesOfUTF8Char(const c8* text_p);

	//@brief	UTF-8で何バイトの文字か
	//@return	バイト数
	//@param[in]	_code 文字コード
	static u32	GetBytesOfUTF8Char(const u32 _code);

	//-------------------------------------------------------------------------------------------------	
	//@brief	UTF-8での文字コードを返す
	//@return			文字コード
	//@param[in]		text_p 文字列（一文字目のポインタ）
	//@param[in,out]	bytes この文字のバイト数
	static u32	GetCharOfUTF8Char(const c8* text_p, u32& bytes);

	//@brief	UTF-8でＮＧの文字コードを返す
	//@return			文字コード
	//@param[in]		text_p 文字列（一文字目のポインタ）
	static u32	GetCharOfUTF8CharNG(const c8* text_p);

	//@brief	UTF-8で１バイトの文字コードを返す
	//@return			文字コード
	//@param[in]		text_p 文字列（一文字目のポインタ）
	static u32	GetCharOfUTF8Char1(const c8* text_p);

	//@brief	UTF-8で２バイトの文字コードを返す
	//@return			文字コード
	//@param[in]		text_p 文字列（一文字目のポインタ）
	static u32	GetCharOfUTF8Char2(const c8* text_p);

	//@brief	UTF-8で３バイトの文字コードを返す
	//@return			文字コード
	//@param[in]		text_p 文字列（一文字目のポインタ）
	static u32	GetCharOfUTF8Char3(const c8* text_p);

	//@brief	UTF-8で４バイトの文字コードを返す
	//@return			文字コード
	//@param[in]		text_p 文字列（一文字目のポインタ）
	static u32	GetCharOfUTF8Char4(const c8* text_p);
	
	//-------------------------------------------------------------------------------------------------	
	//@brief		バイナリを１６進文字列化
	//@return		16進文字列
	//@param[out]	dst			16進文字列
	//@param[in]	bin			バイナリデータ
	//@param[in]	bin_size	バイナリデータサイズ
	static c8*	bin2hex(c8* dst, const u8* bin, const s32 bin_size);

	//@brief		１６進文字列をバイナリ化
	//@return		バイナリデータ
	//@param[out]	dst			バイナリデータ
	//@param[in]	hex			16進文字列
	//@param[in]	hex_size	文字列サイズ
	static u8*	hex2bin(u8* dst, const c8* hex, const s32 hex_size);

	//-------------------------------------------------------------------------------------------------	
	//@brief		文字長の取得（UTF-8）
	//@return		文字長
	//@param[in]	_text		文字列
	static s32	GetLengthUTF8(const c8* _text);

	//-------------------------------------------------------------------------------------------------	
	//@brief		文字列の先頭から指定の文字数だけコピー（UTF-8）
	//@return		実際にコピーした文字数
	//@param[out]	_buff		文字列
	//@param[in]	_text		コピーする文字列
	//@param[in]	_offset		先頭から数えてコピーを開始するインデックス
	//@param[in]	_text		コピーする文字数
	//@param[in]	_len
	static s32	CopyTextUTF8(c8* _buff, const c8* _text, const u32 _offset, const u32 _len);
	
	//-------------------------------------------------------------------------------------------------	
	//@brief		改行コードを検索する（UTF-8）
	//@return		発見した改行コードのアドレス
	//@param[in]	_text			元の文字列
	//@param[in]	crlf_code_bytes	改行コードの指定
	static const c8*	SearchCRLFCodeUTF8(const c8* _text, u32* crlf_code_bytes = NULL);

	//-------------------------------------------------------------------------------------------------	
	//@brief		テキストの最後が改行コードだった場合に削除する（UTF8）
	//@return		削除したかどうか
	//@param[in]	_buff
	//@param[in]	_text
	static b8	EraseLastCRLFCodeUTF8(c8* _buff, const c8* _text);

	//-------------------------------------------------------------------------------------------------	
	//@brief		テキストの改行コードの数を調べる（UTF8）
	//@return		改行コードの数
	//@param[in]	_text			元の文字列
	static u16	GetCRLFCodeNumUTF8(const c8* _text);

	//-------------------------------------------------------------------------------------------------	
	//@brief		数字情報を全角文字列に変換する（UTF-8）
	//@return		実際にコピーした文字数
	//@param[out]	_buff		文字列
	//@param[in]	_bufSize	文字列サイズ
	//@param[in]	_value		数値
	//@param[in]	_digit		桁数
	static s32	ExChangeNumTextUTF8(c8* _buff, const u32 _bufSize, u32 _value, u32 _digit);

	//-------------------------------------------------------------------------------------------------	
	//@brief		文字列の中の指定の省略記号に置き換える（UTF-8）
	//@return		置換後の文字数
	//@param[out]	_buff		結果の文字列
	//@param[in]	_text		元の文字列
	//@param[in]	_offset		先頭から数えてコピーを開始するインデックス
	//@param[in]	_len		文字数
	//@param[in]	_ellipsis	省略記号
	//@param[in]	_is_reverse	戻すか
	static s32	ElideTextUTF8(c8* _buff, const c8* _text, const u32 _offset, const u32 _len, const c8* _ellipsis, b8 _is_reverse);

	//-------------------------------------------------------------------------------------------------	
	//@brief		'\n'と'\ｔ'を半角スペースに置き換える
	//@param[out]	_dst		置き換える文字列
	static void	ReplaceUnnecessaryChar(c8* _dst);

	//-------------------------------------------------------------------------------------------------	
	//@brief		制御文字を置き換える
	//@param[out]	_dst		置き換える文字列
	static void	ReplaceControlChar(c8* _dst);

	//-------------------------------------------------------------------------------------------------	
	//@brief		制御文字を元に戻す
	//@param[out]	_dst	元に戻す文字列
	static void	ReturnsControlChar(c8* _dst);

	//-------------------------------------------------------------------------------------------------	
	//@brief		制御文字を元に戻す
	//@param[out]	_dst	元に戻す文字列（２バイトコード時）
	static void	ReturnsControlChar(u32 &_dst);

	//-------------------------------------------------------------------------------------------------	
	//@brief		文字列を数値化する処理
	//@return		数値化した結果（該当文字列がない場合は 0 を返す）
	//@param[in]		_text		数値化したい文字列
	//@param[in]		_text_list	文字列リスト
	//@param[in]		_num_list	数値リスト
	static s32	GetStringToNum(const c8* _text, const c8** _text_list, const s32* _num_list);

	//-------------------------------------------------------------------------------------------------	
	//@brief		UTF-8 & LF改行として正当な文字列かどうかをチェック
	//@return		エラー検知するまでのバイト数（つまり_len == retなら正当）
	//@param[in]	_text		文字列
	//@param[in]	_len		調べる文字長
	static u32	TestStringIsUtf8LF(const c8* _text, u32 _len);

	//-------------------------------------------------------------------------------------------------
	//@brief		タグを文字列に置換する
	//@return		成功したかどうか
	//@param[out]	_dest			出力バッファ(サイズは確保済み)
	//@param[in]	_dest_length	出力バッファの長さ
	//@param[in]	_src			入力バッファ
	//@param[in]	_tag_word		タグ名と置換文字列の対応構造体
	//@param[in]	_tag_num		タグの数
	template<typename T>
	static b8	ReplaceTag(c8* _dest, const u32 _dest_length, const c8* _src, const TAG_WORD* _tag_word, u32 _tag_num);

	//-------------------------------------------------------------------------------------------------
	//@brief		タグを文字列に置換する
	//@return		成功したかどうか
	//@param[out]	_dest			出力バッファ(サイズは確保済み)
	//@param[in]	_dest_length	出力バッファの長さ
	//@param[in]	_src			入力バッファ
	//@param[in]	_tag_word		タグ名と置換文字列の対応構造体
	//@param[in]	_tag_num		タグの数
	//@param[in]	_text_code		文字コード
	static b8	ReplaceTag(c8* _dest, const u32 _dest_length, const c8* _src, const TAG_WORD* _tag_word, u32 _tag_num, TEXT_TYPE _text_code = TEXT_TYPE::TEXT_TYPE_UTF8);

	//-------------------------------------------------------------------------------------------------	
	//@brief		タグを解析してコールバックを呼ぶ
	//@return					成功したかどうか
	//@param[out]		_dest			出力バッファ(サイズは確保済み)
	//@param[in]		_dest_length	出力バッファの長さ
	//@param[in]		_src			入力バッファ
	//@param[in]		_tag_info		コールバックの関数ポインタ、タグの開始と終了文字
	//@param[in]		_tag_info		コールバックの関数ポインタ、タグの開始と終了文字
	//@param[in,out]	_ptr			汎用ポインタ
	template<typename T>
	static b8	CallTag(c8* _dest, u32 _dest_length, const c8* _src, const CALL_TAG_INFO& _tag_info, void* _ptr = NULL);

	//-------------------------------------------------------------------------------------------------	
	//@brief		タグを解析してコールバックを呼ぶ
	//@return					成功したかどうか
	//@param[out]		_dest			出力バッファ(サイズは確保済み)
	//@param[in]		_dest_length	出力バッファの長さ
	//@param[in]		_src			入力バッファ
	//@param[in]		_tag_info		コールバックの関数ポインタ、タグの開始と終了文字
	//@param[in]		_tag_info		コールバックの関数ポインタ、タグの開始と終了文字
	//@param[in,out]	_ptr			汎用ポインタ
	//@param[in]		_text_code		文字コード
	static b8	CallTag(c8* _dest, u32 _dest_length, const c8* _src, const CALL_TAG_INFO& _tag_info, void* _ptr = NULL, TEXT_TYPE _text_code = TEXT_TYPE::TEXT_TYPE_UTF8);

public:
	//-------------------------------------------------------------------------------------------------	
	//@brief		文字検索
	//@return		見つけた場合、1文字目のポインタを返す。見つからなければNULL
	//@param[in]	_str			文字列
	//@param[in]	_search_char	検索文字の文字コード
	//@param[in]	_length			検索する長さ
	template<typename T>
	static const c8*	SearchChar(const c8* _str, u32 _search_char, u32 _length = 0);

	//-------------------------------------------------------------------------------------------------	
	//@brief		文字検索
	//@return		見つけた場合、1文字目のポインタを返す。見つからなければNULL
	//@param[in]	_str			文字列
	//@param[in]	_search_char	検索文字の文字コード
	//@param[in]	_length			検索する長さ
	//@param[in]	_text_code		文字コード
	static const c8*	SearchChar(const c8* _str, u32 _search_char, u32 _length = 0, TEXT_TYPE _text_code = TEXT_TYPE::TEXT_TYPE_UTF8);

	//-------------------------------------------------------------------------------------------------	
	//@brief タグを検索して先頭ポインタと文字数を返す
	//@return		見つけた場合、タグの先頭を返す
	//@param[in]	_str			文字列
	//@param[in]	_tag_begin		タグ開始文字
	//@param[in]	_tag_end		タグ終了文字
	//@param[in]	_tag_length		タグの長さ(開始文字と終了文字を含んだ長さ)
	//@param[in]	_arg_length		タグの中身の長さ
	template<typename T>
	static const c8*	CheckTag(const c8* _str, u32 _tag_begin, u32 _tag_end, u32& _tag_length, u32& _arg_length);

	//-------------------------------------------------------------------------------------------------	
	//@brief タグを検索して先頭ポインタと文字数を返す
	//@return		見つけた場合、タグの先頭を返す
	//@param[in]	_str			文字列
	//@param[in]	_tag_begin		タグ開始文字
	//@param[in]	_tag_end		タグ終了文字
	//@param[in]	_tag_length		タグの長さ(開始文字と終了文字を含んだ長さ)
	//@param[in]	_arg_length		タグの中身の長さ
	//@param[in]	_text_code		文字コード
	static const c8*	CheckTag(const c8* _str, u32 _tag_begin, u32 _tag_end, u32& _tag_length, u32& _arg_length, TEXT_TYPE _text_code = TEXT_TYPE::TEXT_TYPE_UTF8);

	//-------------------------------------------------------------------------------------------------	
	//@brief 文字列を指定の文字で区切り,先頭と長さを返す
	//@return		見つけた場合、タグの中身の先頭を返す
	//@param[in]	_str			文字列
	//@param[in]	_max_length		切り出した文字列の長さ
	//@param[in]	_split			タグの区切り文字
	//@param[in]	_split_num		タグの区切り文字の数
	//@param[in]	_arg_length		タグの長さ(開始文字と終了文字を含んだ長さ)
	template<typename T>
	static const c8*	StrSplit(const c8* _str, u32 _max_length, const  u32* _split, u32 _split_num, u32& _arg_length);

	//-------------------------------------------------------------------------------------------------	
	//@brief 文字列を指定の文字で区切り,先頭と長さを返す
	//@return		見つけた場合、タグの中身の先頭を返す
	//@param[in]	_str			文字列
	//@param[in]	_max_length		切り出した文字列の長さ
	//@param[in]	_split			タグの区切り文字
	//@param[in]	_split_num		タグの区切り文字の数
	//@param[in]	_arg_length		タグの長さ(開始文字と終了文字を含んだ長さ)
	//@param[in]	_text_code		文字コード
	static const c8*	StrSplit(const c8* _str, u32 _max_length, const u32* _split, u32 _split_num, u32& _arg_length, TEXT_TYPE _text_code = TEXT_TYPE::TEXT_TYPE_UTF8);

	//-------------------------------------------------------------------------------------------------	
	//@brief 特定の文字(複数)と一致するか
	//@return		一致したか
	//@param[in]	_str			文字列
	//@param[in]	_split			タグの区切り文字
	//@param[in]	_split_num		タグの区切り文字の数
	template<typename T>
	static b8	StrCompare(const c8* _str, const u32* _split, u32 _split_num);

	//-------------------------------------------------------------------------------------------------	
	//@brief 特定の文字(複数)と一致するか
	//@return		一致したか
	//@param[in]	_str			文字列
	//@param[in]	_split			タグの区切り文字
	//@param[in]	_split_num		タグの区切り文字の数
	//@param[in]	_text_code		文字コード
	static b8	StrCompare(const c8* _str, const  u32* _split, u32 _split_num, TEXT_TYPE _text_code = TEXT_TYPE::TEXT_TYPE_UTF8);


public:
	//-------------------------------------------------------------------------------------------------	
	//@brief		文字列とCRC32でマッチングを取り、マッチングした文字列を出力 コールバック
	//@return			置換の成否
	//@param[in]		_str			入力バッファ
	//@param[in]		_length			入力バッファの長さ
	//@param[out]		_dest			出力バッファ
	//@param[in]		_dest_length	出力バッファの長さ
	//@param[out]		_replace_length	書き込んだ文字数
	//@param[in,out]	_ptr			タグ名と置換文字列を配列で持ち歩く構造体のポインタ
	template<typename T>
	static b8	SearchReplaceWord(const c8* _str, u32 _length, c8* _dest, u32 _dest_length, u32& _replace_length, void* _ptr);

public:
	//-------------------------------------------------------------------------------------------------	
	//@brief	文字コード変換
	//@param[in]		_src_type		入力文字コード
	//@param[in]		_dest_type		出力文字コード
	//@param[in]		_char			変換する文字
	//@param[in]		_error			変換できないときに置き換える文字（デフォルトで'?'）
	static u32	ConvTextCode(TEXT_TYPE _src_type, TEXT_TYPE _dest_type, u32 _char, u32 _error = 63);

	//-------------------------------------------------------------------------------------------------	
	//@brief	文字コード変換（文字列）
	//@param[in]		_src_type		入力文字コード
	//@param[in]		_dest_type		出力文字コード
	//@param[in]		_buff			出力文字列
	//@param[in]		_string			入力文字列
	//@param[in]		_error			変換できないときに置き換える文字（デフォルトで'?'）
	static const c8*	ConvTextCode(TEXT_TYPE _src_type, TEXT_TYPE _dest_type, const c8* _string, c8* _buff, u32 _error = 63);

	//-------------------------------------------------------------------------------------------------	
	//@brief	何バイトの文字か
	//@return	バイト数
	//@param[in]	文字
	static u32	GetBytesOfChar(const TEXT_TYPE _src_type, const c8 _c);

public:
	//-------------------------------------------------------------------------------------------------	
	//@brief	整数値をカンマ付きの書式で文字列型へ変換
	//@param[out]	_ret[]	変換結果を格納する文字列
	//@param[in]	_num	変換する整数値
	static void	Converts32ToStrCommaNum(c8* _ret, s32 _num);

	//-------------------------------------------------------------------------------------------------	
	//@brief	文字列の並びを逆にする
	//@param[in,out]	_str[]	並びを逆にする文字列
	static void	ReverseString(c8* _str);

	//-------------------------------------------------------------------------------------------------	
	//@brief	文字列内の半角を全角に変換
	static const size_t	UTF8_HalfToWide(c8 *_dst, const c8 *_src, const size_t _max = (size_t)(-1));
	
	//-------------------------------------------------------------------------------------------------
	//@brief	文字コード取得
	static const b8		GetStringCodeUTF8(const c8 *_str, c32 &_chr, size_t &_num);

	//-------------------------------------------------------------------------------------------------
	//@brief	文字列内の指定文字を前から検索
	static const c8*	SearchN(const c8 *_str, const c8 _val);
	static c8*			SearchN(c8 *_str, const c8 _val) { return (c8*)SearchN((const c8*)_str, _val); }

	//-------------------------------------------------------------------------------------------------
	//@brief	ルビ本文文字列を文字列から削除
	static const size_t	DeleteRubiBody(c8 *_str_dst, const c8 *_str_src);
	static const size_t	DeleteRubiBody(c8 *_str) { return DeleteRubiBody(_str, _str); }
};

//-------------------------------------------------------------------------------------------------	
//@brief		SJISの文字コードをUTF8に変換
extern u32	ConvSJIStoUTF8(const u32 sjis_code);

//-------------------------------------------------------------------------------------------------	
//@brief		UTF8の文字コードをSJISに変換
extern u32	ConvUTF8toSJIS(const u32 utf8_code);



//*************************************************************************************************
//
// 文字コードを気にせずに1文字ごとにアクセス、コードの取得を行う
//
//*************************************************************************************************	
//@brief	UTF8の文字列クラス
//*************************************************************************************************
class CStrUTF8
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ
	inline CStrUTF8(const c8* _utf8) : text(_utf8) {}

	//-------------------------------------------------------------------------------------------------
	//@brief	暗黙のconst c8*型変換
	inline operator const c8*(void) const { return text; }

	//-------------------------------------------------------------------------------------------------
	//@brief	アクセス時の動作
	inline u32	operator*(void) { u32 byte_num; return CTextUtility::GetCharOfUTF8Char(text, byte_num); }

	//-------------------------------------------------------------------------------------------------
	//@brief	++の動作
	inline const CStrUTF8&	operator++(void){ text += CTextUtility::GetBytesOfUTF8Char(text); return *this; }
	inline const CStrUTF8	operator++(s32){ CStrUTF8 temp(text); ++(*this); return temp; }

	//-------------------------------------------------------------------------------------------------
	//@brief	+=の動作
	inline CStrUTF8&	operator+=(u32 _num)
	{
		for (u32 idx = 0; idx < _num; ++idx)
		{
			++text;
		}
		return *this;
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	足し算
	inline CStrUTF8		operator+(u32 _num) const { CStrUTF8 temp(text); temp.text += _num; return temp; }

	//-------------------------------------------------------------------------------------------------
	//@brief	文字数カウント
	u32	GetChrNum() const { u32 count = 0; CStrUTF8 temp(text); while (*temp){ ++temp; ++count; }return count; }

private:
	//-------------------------------------------------------------------------------------------------
	//@brief	引き算
	inline u32	operator-(CStrUTF8 _utf8) const { return 0; }

private:
	const c8*	text;	///< 文字列
};

//*************************************************************************************************	
//@brief	SJISの文字列クラス
//*************************************************************************************************
class CStrSJIS
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ
	inline CStrSJIS(const c8* _sjis) : text(_sjis) {}

	//-------------------------------------------------------------------------------------------------
	//@brief	暗黙のconst c8*型変換
	inline operator const c8*(void) const { return text; }

	//-------------------------------------------------------------------------------------------------
	//@brief	アクセス時の動作
	inline u32	operator*(void) { u32 byte_num; return CTextUtility::GetCharOfSJISChar(text, byte_num); }

	//-------------------------------------------------------------------------------------------------
	//@brief	++の動作
	inline const CStrSJIS&	operator++(void){ text += CTextUtility::GetBytesOfSJISChar(text); return *this; }
	inline const CStrSJIS	operator++(s32){ CStrSJIS temp(text); ++(*this); return temp; }

	//-------------------------------------------------------------------------------------------------
	//@brief	+=の動作
	inline CStrSJIS&	operator+=(u32 _num)
	{
		for (u32 idx = 0; idx < _num; ++idx)
		{
			++text;
		}
		return *this;
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	足し算
	inline CStrSJIS		operator+(u32 _num) const { CStrSJIS temp(text); temp.text += _num; return temp; }

	//-------------------------------------------------------------------------------------------------
	//@brief	文字数カウント
	u32	GetChrNum() const { u32 count = 0; CStrSJIS temp(text); while (*temp){ ++temp; ++count; }return count; }

private:
	//-------------------------------------------------------------------------------------------------
	//@brief	引き算
	inline u32	operator-(CStrSJIS _sjis) const { return 0; }

private:
	const c8*	text;	///< 文字列
};

//*************************************************************************************************	
//@brief	ASCIIの文字列クラス
//*************************************************************************************************
class CStrASCII
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ
	inline CStrASCII(const c8* _ascii) : text(_ascii) {}

	//-------------------------------------------------------------------------------------------------
	//@brief	暗黙のconst c8*型変換
	inline operator const c8*(void) const { return text; }

	//-------------------------------------------------------------------------------------------------
	//@brief	アクセス時の動作
	inline u32	operator*(void) { return SCast<u32>(*text); }

	//-------------------------------------------------------------------------------------------------
	//@brief	++の動作
	inline const CStrASCII&	operator++(void){ ++text; return *this; }
	inline const CStrASCII	operator++(s32){ CStrASCII temp(text); ++text; return temp; }

	//-------------------------------------------------------------------------------------------------
	//@brief	+=の動作
	inline CStrASCII&	operator+=(u32 _num)
	{
		for (u32 idx = 0; idx < _num; ++idx)
		{
			++text;
		}
		return *this;
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	足し算
	inline CStrASCII	operator+(u32 _num){ CStrASCII temp(text); temp.text += _num; return temp; }

	//-------------------------------------------------------------------------------------------------
	//@brief	文字数カウント
	u32	GetChrNum(){ u32 count = 0; CStrASCII temp(text); while (*temp){ ++temp; ++count; }return count; }

private:
	//-------------------------------------------------------------------------------------------------
	//@brief	引き算
	inline u32	operator-(CStrASCII _sjis){ return 0; }

private:
	const c8*	text;	///< 文字列
};


POISON_END

#include "text_utility.inl"

#endif//__TEXT_UTILITY_H_

﻿

POISON_BGN


// 区切り文字
static const u32 s_tag_split[] =
{
	' ',
	'\t'
};
static const u32 s_split_num = ARRAYOF(s_tag_split);

//-------------------------------------------------------------------------------------------------	
/// タグを解析してコールバックを呼ぶ
template<typename T>
b8	CTextUtility::CallTag(c8* _dest, u32 _dest_length, const c8* _src, const CALL_TAG_INFO& _tag_info, void* _ptr)
{
	// 出力チェック
	libAssert(_dest);

	// コールバック
	TAG_CALLBACK* func = _tag_info.func;
	// タグ開始文字
	u32 tag_begin = _tag_info.tag_begin;
	// タグ終了文字
	u32 tag_end = _tag_info.tag_end;

	// 入力バッファ、指定コードで文字送り
	T src_chr = _src;
	// 出力バッファ
	c8* dst_chr = _dest;

	// 出力バッファの合計計算
	u32 buff_num = 0;

	// タグ解析
	//-------------------------------------------------------------------------------------------------
	while (*src_chr)
	{
		// タグの長さ
		u32 tag_length = 0;
		u32 arg_length = 0;

		// タグがあるかチェック
		T tag_begin_ptr = CheckTag<T>(src_chr, tag_begin, tag_end, tag_length, arg_length);
		if (!tag_begin_ptr)
		{
			break;
		}
		T tag_end_ptr = tag_begin_ptr + tag_length;

		// タグの前までのバッファ数
		u32 before_length = SCast<u32>(tag_begin_ptr.operator const c8*() - src_chr);
		// ここまでのインデックスを計算
		buff_num += before_length;
		// 出力チェック
		libAssert(!(buff_num > _dest_length));

		// タグ前までのバッファコピー
		strcpy_safe(dst_chr, _dest_length, src_chr);

		dst_chr += before_length;

		// コールバックを呼ぶ
		// 出力バッファに書き込んだサイズを返す
		u32 replace_length = 0;
		b8 ret = (func)( tag_begin_ptr+1, arg_length, dst_chr, _dest_length - buff_num, replace_length, _ptr );

		if( ret )
		{
			// ここまでのインデックスを計算
			buff_num += replace_length;
			dst_chr += replace_length;
		}
		else
		{
			strcpy_safe(dst_chr, _dest_length, tag_begin_ptr);
			buff_num += tag_length;
			dst_chr += tag_length;
		}

		// タグの終わりからループ
		src_chr = tag_end_ptr;
	}

	// 残りの文字列を格納
	buff_num += SCast<u32>(strlen(src_chr));

	// 出力チェック
	libAssert(!(buff_num > _dest_length));

	strcpy_safe(dst_chr, _dest_length, src_chr);
	//-------------------------------------------------------------------------------------------------

	return true;
}

//-------------------------------------------------------------------------------------------------	
/// 文字列とCRC32でマッチングを取り、マッチングした文字列を出力
template<typename T>
b8	CTextUtility::SearchReplaceWord(const c8* _str, u32 _length, c8* _dest, u32 _dest_length, u32& _replace_length, void* _ptr)
{
	// タグの中身の開始位置
	T chr = _str;

	// 読み終わったバッファ数を記憶
	// 引数格納--------------------------------------------------------------------------------
	u32 arg_length = 0;

	// 引数ごとに区切る
	T arg_ptr = StrSplit<T>(chr, _length, s_tag_split, s_split_num, arg_length);

	if (!arg_ptr)
	{
		return false;
	}

	// 置換
	hash32 tag_crc_name = CRC32(arg_ptr, arg_length);
	TAG_WORD_ARRAY* tag_array = PCast<TAG_WORD_ARRAY*>(_ptr);

	const TAG_WORD* tag_word = tag_array->tag_word;
	u32 word_num = tag_array->tag_num;

	for (u32 tag_word_idx = 0u; tag_word_idx < word_num; ++tag_word_idx, ++tag_word)
	{
		if (tag_crc_name == tag_word->tag_name_crc)
		{
			// 置き換える文字列
			_replace_length = SCast<u32>(strlen(tag_word->word));
			strcpy_safe(_dest, _length, tag_word->word);
			return true;
		}
	}
	//-----------------------------------------------------------------------------------------
	return false;
}

//-------------------------------------------------------------------------------------------------
/// タグを文字列に置換する
template<typename T>
b8	CTextUtility::ReplaceTag(c8* _dest, const u32 _dest_length, const c8* _src, const TAG_WORD* _tag_word, u32 _tag_num)
{
	// CallTag用のタグの開始終了文字とコールバック
	const CALL_TAG_INFO call_info =
	{
		SearchReplaceWord<T>,
		'<',
		'>'
	};

	// 置き換える文字のマッチング用構造体
	TAG_WORD_ARRAY tag_array =
	{
		_tag_word, _tag_num
	};

	return CallTag<T>(_dest, _dest_length, _src, call_info, &tag_array);
}

//-------------------------------------------------------------------------------------------------	
/// 文字検索
template<typename T>
const c8*	CTextUtility::SearchChar(const c8* _str, u32 _search_char, u32 _length)
{
	// 指定の文字コードで格納
	T chr = _str;

	u32 count = 0;

	// 一文字ずつ確認
	while (*chr)
	{
		u32 id = *chr;

		if (id == _search_char)
		{
			return chr;
		}
		chr++;

		if (_length > 0)
		{
			++count;
			if (_length <= count)
			{
				break;
			}
		}
	}
	return NULL;
}

//-------------------------------------------------------------------------------------------------	
/// タグを検索して先頭ポインタと文字数を返す
template<typename T>
const c8*	CTextUtility::CheckTag(const c8* _str, u32 _tag_begin, u32 _tag_end, u32& _tag_length, u32& _arg_length)
{
	_tag_length = 0;
	// タグの開始文字を検索
	T tag_begin_ptr = SearchChar<T>(_str, _tag_begin);
	T arg_begin_ptr = tag_begin_ptr + 1u;

	// タグがなし
	if (!tag_begin_ptr)
	{
		return NULL;
	}

	// タグの終了文字を検索
	T tag_end_ptr = SearchChar<T>(tag_begin_ptr + 1u, _tag_end);

	// タグがなし
	if (!tag_end_ptr)
	{
		return NULL;
	}

	// タグの中身の長さ
	_arg_length = SCast<u32>(tag_end_ptr.operator const c8*() - arg_begin_ptr);

	++tag_end_ptr;

	// コマンド名の長さ
	_tag_length = SCast<u32>(tag_end_ptr.operator const c8*() - tag_begin_ptr);


	return tag_begin_ptr;
}

//-------------------------------------------------------------------------------------------------	
/// 文字列を指定の文字で区切り,先頭と長さを返す
template<typename T>
const c8*	CTextUtility::StrSplit(const c8* _str, u32 _max_length, const u32* _split, u32 _split_num, u32& _arg_length)
{
	T chr = _str;

	u32 length_num = 0;

	// 空白スキップ-----------------------------------
	while (StrCompare<T>(chr, _split, _split_num))
	{
		++chr;
		length_num = SCast<u32>(chr.operator const c8*() - _str);
		if (length_num >= _max_length)
		{
			_arg_length = 0;
			return NULL;
		}
	}

	// 中身の先頭ポインタ
	T arg_begin = chr;
	// 引数スキップ-----------------------------------
	while (!StrCompare<T>(chr, _split, _split_num))
	{
		++chr;
		length_num = SCast<u32>(chr.operator const c8*() - _str);
		if (length_num >= _max_length)
		{
			break;
		}
	}
	_arg_length = SCast<u32>(chr.operator const c8*() - arg_begin);
	return arg_begin;
}

//-------------------------------------------------------------------------------------------------	
/// 特定の文字(複数)と一致するか
template<typename T>
b8	CTextUtility::StrCompare(const c8* _str, const u32* _split, u32 _split_num)
{
	if (!_str)
	{
		return false;
	}
	u32 chr = *T(_str);
	for (u32 idx = 0; idx < _split_num; ++idx, ++_split)
	{
		if (chr == *_split)
		{
			return true;
		}
	}
	return false;
}

POISON_END

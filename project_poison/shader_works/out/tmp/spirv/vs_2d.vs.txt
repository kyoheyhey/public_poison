; SPIR-V
; Version: 1.0
; Generator: Google spiregg; 0
; Bound: 39
; Schema: 0
               OpCapability Shader
               OpMemoryModel Logical GLSL450
               OpEntryPoint Vertex %main "main" %in_var_POSITION %in_var_TEXCOORD0 %in_var_COLOR0 %gl_Position %out_var_TEXCOORD0 %out_var_TEXCOORD1
               OpSource HLSL 640
               OpName %type_CB0 "type.CB0"
               OpMemberName %type_CB0 0 "u_mtxLW2D"
               OpName %CB0 "CB0"
               OpName %type_CB1 "type.CB1"
               OpMemberName %type_CB1 0 "u_uv"
               OpName %CB1 "CB1"
               OpName %in_var_POSITION "in.var.POSITION"
               OpName %in_var_TEXCOORD0 "in.var.TEXCOORD0"
               OpName %in_var_COLOR0 "in.var.COLOR0"
               OpName %out_var_TEXCOORD0 "out.var.TEXCOORD0"
               OpName %out_var_TEXCOORD1 "out.var.TEXCOORD1"
               OpName %main "main"
               OpDecorate %gl_Position BuiltIn Position
               OpDecorate %in_var_POSITION Location 0
               OpDecorate %in_var_TEXCOORD0 Location 10
               OpDecorate %in_var_COLOR0 Location 8
               OpDecorate %out_var_TEXCOORD0 Location 0
               OpDecorate %out_var_TEXCOORD1 Location 1
               OpDecorate %CB0 DescriptorSet 0
               OpDecorate %CB0 Binding 0
               OpDecorate %CB1 DescriptorSet 0
               OpDecorate %CB1 Binding 1
               OpMemberDecorate %type_CB0 0 Offset 0
               OpMemberDecorate %type_CB0 0 MatrixStride 16
               OpMemberDecorate %type_CB0 0 ColMajor
               OpDecorate %type_CB0 Block
               OpMemberDecorate %type_CB1 0 Offset 0
               OpDecorate %type_CB1 Block
        %int = OpTypeInt 32 1
      %int_0 = OpConstant %int 0
      %float = OpTypeFloat 32
    %v4float = OpTypeVector %float 4
%mat4v4float = OpTypeMatrix %v4float 4
   %type_CB0 = OpTypeStruct %mat4v4float
%_ptr_Uniform_type_CB0 = OpTypePointer Uniform %type_CB0
   %type_CB1 = OpTypeStruct %v4float
%_ptr_Uniform_type_CB1 = OpTypePointer Uniform %type_CB1
%_ptr_Input_v4float = OpTypePointer Input %v4float
    %v2float = OpTypeVector %float 2
%_ptr_Input_v2float = OpTypePointer Input %v2float
%_ptr_Output_v4float = OpTypePointer Output %v4float
%_ptr_Output_v2float = OpTypePointer Output %v2float
       %void = OpTypeVoid
         %25 = OpTypeFunction %void
%_ptr_Uniform_mat4v4float = OpTypePointer Uniform %mat4v4float
%_ptr_Uniform_v4float = OpTypePointer Uniform %v4float
        %CB0 = OpVariable %_ptr_Uniform_type_CB0 Uniform
        %CB1 = OpVariable %_ptr_Uniform_type_CB1 Uniform
%in_var_POSITION = OpVariable %_ptr_Input_v4float Input
%in_var_TEXCOORD0 = OpVariable %_ptr_Input_v2float Input
%in_var_COLOR0 = OpVariable %_ptr_Input_v4float Input
%gl_Position = OpVariable %_ptr_Output_v4float Output
%out_var_TEXCOORD0 = OpVariable %_ptr_Output_v2float Output
%out_var_TEXCOORD1 = OpVariable %_ptr_Output_v4float Output
       %main = OpFunction %void None %25
         %28 = OpLabel
         %29 = OpLoad %v4float %in_var_POSITION
         %30 = OpLoad %v2float %in_var_TEXCOORD0
         %31 = OpLoad %v4float %in_var_COLOR0
         %32 = OpAccessChain %_ptr_Uniform_mat4v4float %CB0 %int_0
         %33 = OpLoad %mat4v4float %32
         %34 = OpVectorTimesMatrix %v4float %29 %33
         %35 = OpAccessChain %_ptr_Uniform_v4float %CB1 %int_0
         %36 = OpLoad %v4float %35
         %37 = OpVectorShuffle %v2float %36 %36 0 1
         %38 = OpFAdd %v2float %30 %37
               OpStore %gl_Position %34
               OpStore %out_var_TEXCOORD0 %38
               OpStore %out_var_TEXCOORD1 %31
               OpReturn
               OpFunctionEnd

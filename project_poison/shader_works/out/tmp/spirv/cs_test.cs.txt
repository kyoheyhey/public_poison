; SPIR-V
; Version: 1.0
; Generator: Google spiregg; 0
; Bound: 70
; Schema: 0
               OpCapability Shader
               OpCapability SampledBuffer
               OpCapability ImageBuffer
          %1 = OpExtInstImport "GLSL.std.450"
               OpMemoryModel Logical GLSL450
               OpEntryPoint GLCompute %main "main" %gl_WorkGroupID %gl_GlobalInvocationID %gl_LocalInvocationID %gl_LocalInvocationIndex
               OpExecutionMode %main LocalSize 16 16 1
               OpSource HLSL 640
               OpName %type_CB0 "type.CB0"
               OpMemberName %type_CB0 0 "u_bufferSize"
               OpMemberName %type_CB0 1 "u_bufferInvSize"
               OpMemberName %type_CB0 2 "u_nearFarInvAspect"
               OpMemberName %type_CB0 3 "u_color"
               OpName %CB0 "CB0"
               OpName %type_2d_image "type.2d.image"
               OpName %in_texColor "in_texColor"
               OpName %type_buffer_image "type.buffer.image"
               OpName %out_resultBuffer "out_resultBuffer"
               OpName %main "main"
               OpDecorate %gl_WorkGroupID BuiltIn WorkgroupId
               OpDecorate %gl_GlobalInvocationID BuiltIn GlobalInvocationId
               OpDecorate %gl_LocalInvocationID BuiltIn LocalInvocationId
               OpDecorate %gl_LocalInvocationIndex BuiltIn LocalInvocationIndex
               OpDecorate %CB0 DescriptorSet 0
               OpDecorate %CB0 Binding 0
               OpDecorate %in_texColor DescriptorSet 0
               OpDecorate %in_texColor Binding 0
               OpDecorate %out_resultBuffer DescriptorSet 0
               OpDecorate %out_resultBuffer Binding 0
               OpMemberDecorate %type_CB0 0 Offset 0
               OpMemberDecorate %type_CB0 1 Offset 16
               OpMemberDecorate %type_CB0 2 Offset 32
               OpMemberDecorate %type_CB0 3 Offset 48
               OpDecorate %type_CB0 Block
        %int = OpTypeInt 32 1
      %int_1 = OpConstant %int 1
       %uint = OpTypeInt 32 0
      %int_0 = OpConstant %int 0
      %float = OpTypeFloat 32
    %float_1 = OpConstant %float 1
     %v4uint = OpTypeVector %uint 4
    %v4float = OpTypeVector %float 4
   %type_CB0 = OpTypeStruct %v4uint %v4float %v4float %v4float
%_ptr_Uniform_type_CB0 = OpTypePointer Uniform %type_CB0
%type_2d_image = OpTypeImage %float 2D 2 0 0 1 Unknown
%_ptr_UniformConstant_type_2d_image = OpTypePointer UniformConstant %type_2d_image
%type_buffer_image = OpTypeImage %float Buffer 2 0 0 2 Rgba32f
%_ptr_UniformConstant_type_buffer_image = OpTypePointer UniformConstant %type_buffer_image
     %v3uint = OpTypeVector %uint 3
%_ptr_Input_v3uint = OpTypePointer Input %v3uint
%_ptr_Input_uint = OpTypePointer Input %uint
       %void = OpTypeVoid
         %28 = OpTypeFunction %void
%_ptr_Uniform_v4uint = OpTypePointer Uniform %v4uint
%_ptr_Uniform_v4float = OpTypePointer Uniform %v4float
      %v3int = OpTypeVector %int 3
      %v2int = OpTypeVector %int 2
        %CB0 = OpVariable %_ptr_Uniform_type_CB0 Uniform
%in_texColor = OpVariable %_ptr_UniformConstant_type_2d_image UniformConstant
%out_resultBuffer = OpVariable %_ptr_UniformConstant_type_buffer_image UniformConstant
%gl_WorkGroupID = OpVariable %_ptr_Input_v3uint Input
%gl_GlobalInvocationID = OpVariable %_ptr_Input_v3uint Input
%gl_LocalInvocationID = OpVariable %_ptr_Input_v3uint Input
%gl_LocalInvocationIndex = OpVariable %_ptr_Input_uint Input
         %33 = OpUndef %float
       %main = OpFunction %void None %28
         %34 = OpLabel
         %35 = OpLoad %v3uint %gl_GlobalInvocationID
         %36 = OpCompositeExtract %uint %35 0
         %37 = OpCompositeExtract %uint %35 1
         %38 = OpConvertUToF %float %36
         %39 = OpConvertUToF %float %37
         %40 = OpCompositeConstruct %v4float %38 %39 %33 %33
         %41 = OpAccessChain %_ptr_Uniform_v4uint %CB0 %int_0
         %42 = OpLoad %v4uint %41
         %43 = OpVectorShuffle %v4uint %42 %42 0 1 0 1
         %44 = OpConvertUToF %v4float %43
         %45 = OpFMul %v4float %40 %44
         %46 = OpAccessChain %_ptr_Uniform_v4float %CB0 %int_1
         %47 = OpLoad %v4float %46
         %48 = OpVectorShuffle %v4float %47 %47 2 3 2 3
         %49 = OpFMul %v4float %45 %48
         %50 = OpConvertFToU %v4uint %49
         %51 = OpCompositeExtract %uint %50 0
         %52 = OpCompositeExtract %uint %50 1
         %53 = OpBitcast %int %51
         %54 = OpBitcast %int %52
         %55 = OpCompositeConstruct %v3int %53 %54 %int_0
         %56 = OpVectorShuffle %v2int %55 %55 0 1
         %57 = OpLoad %type_2d_image %in_texColor
         %58 = OpImageFetch %v4float %57 %56 Lod %int_0
         %59 = OpCompositeExtract %float %58 0
         %60 = OpCompositeExtract %float %58 1
         %61 = OpCompositeExtract %float %58 2
         %62 = OpCompositeConstruct %v4float %59 %60 %61 %float_1
         %63 = OpCompositeExtract %uint %42 3
         %64 = OpExtInst %uint %1 UMin %37 %63
         %65 = OpCompositeExtract %uint %42 2
         %66 = OpIMul %uint %64 %65
         %67 = OpExtInst %uint %1 UMin %36 %65
         %68 = OpIAdd %uint %66 %67
         %69 = OpLoad %type_buffer_image %out_resultBuffer
               OpImageWrite %69 %68 %62 None
               OpReturn
               OpFunctionEnd

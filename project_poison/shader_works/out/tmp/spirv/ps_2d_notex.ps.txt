; SPIR-V
; Version: 1.0
; Generator: Google spiregg; 0
; Bound: 16
; Schema: 0
               OpCapability Shader
               OpMemoryModel Logical GLSL450
               OpEntryPoint Fragment %main "main" %gl_FragCoord %in_var_TEXCOORD0 %in_var_TEXCOORD1 %out_var_SV_Target0
               OpExecutionMode %main OriginUpperLeft
               OpSource HLSL 640
               OpName %in_var_TEXCOORD0 "in.var.TEXCOORD0"
               OpName %in_var_TEXCOORD1 "in.var.TEXCOORD1"
               OpName %out_var_SV_Target0 "out.var.SV_Target0"
               OpName %main "main"
               OpDecorate %gl_FragCoord BuiltIn FragCoord
               OpDecorate %in_var_TEXCOORD0 Location 0
               OpDecorate %in_var_TEXCOORD1 Location 1
               OpDecorate %out_var_SV_Target0 Location 0
      %float = OpTypeFloat 32
    %v4float = OpTypeVector %float 4
%_ptr_Input_v4float = OpTypePointer Input %v4float
    %v2float = OpTypeVector %float 2
%_ptr_Input_v2float = OpTypePointer Input %v2float
%_ptr_Output_v4float = OpTypePointer Output %v4float
       %void = OpTypeVoid
         %13 = OpTypeFunction %void
%gl_FragCoord = OpVariable %_ptr_Input_v4float Input
%in_var_TEXCOORD0 = OpVariable %_ptr_Input_v2float Input
%in_var_TEXCOORD1 = OpVariable %_ptr_Input_v4float Input
%out_var_SV_Target0 = OpVariable %_ptr_Output_v4float Output
       %main = OpFunction %void None %13
         %14 = OpLabel
         %15 = OpLoad %v4float %in_var_TEXCOORD1
               OpStore %out_var_SV_Target0 %15
               OpReturn
               OpFunctionEnd

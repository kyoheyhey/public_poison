#version 460
layout(triangles) in;
layout(max_vertices = 3, triangle_strip) out;

layout(location = 0) in vec2 in_var_TEXCOORD0[3];
layout(location = 1) in vec4 in_var_TEXCOORD1[3];
layout(location = 0) out vec2 out_var_TEXCOORD0;
layout(location = 1) out vec4 out_var_TEXCOORD1;

void main()
{
    vec4 _23_unrolled[3];
    for (int i = 0; i < int(3); i++)
    {
        _23_unrolled[i] = gl_in[i].gl_Position;
    }
    gl_Position = _23_unrolled[0];
    out_var_TEXCOORD0 = in_var_TEXCOORD0[0];
    out_var_TEXCOORD1 = in_var_TEXCOORD1[0];
    EmitVertex();
    gl_Position = _23_unrolled[1];
    out_var_TEXCOORD0 = in_var_TEXCOORD0[1];
    out_var_TEXCOORD1 = in_var_TEXCOORD1[1];
    EmitVertex();
    gl_Position = _23_unrolled[2];
    out_var_TEXCOORD0 = in_var_TEXCOORD0[2];
    out_var_TEXCOORD1 = in_var_TEXCOORD1[2];
    EmitVertex();
    EndPrimitive();
}


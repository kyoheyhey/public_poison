; SPIR-V
; Version: 1.0
; Generator: Google spiregg; 0
; Bound: 36
; Schema: 0
               OpCapability Shader
               OpMemoryModel Logical GLSL450
               OpEntryPoint Vertex %main "main" %in_var_POSITION %in_var_COLOR0 %gl_Position %out_var_TEXCOORD0 %out_var_TEXCOORD1
               OpSource HLSL 640
               OpName %type_CB1 "type.CB1"
               OpMemberName %type_CB1 0 "u_mtxLW"
               OpMemberName %type_CB1 1 "u_mtxLV"
               OpMemberName %type_CB1 2 "u_mtxLP"
               OpMemberName %type_CB1 3 "u_mtxWV"
               OpMemberName %type_CB1 4 "u_mtxWP"
               OpMemberName %type_CB1 5 "u_mtxVP"
               OpMemberName %type_CB1 6 "u_mtxPW"
               OpMemberName %type_CB1 7 "u_mtxPV"
               OpMemberName %type_CB1 8 "u_mtxVW"
               OpMemberName %type_CB1 9 "u_mtxOldLW"
               OpMemberName %type_CB1 10 "u_mtxOldLV"
               OpMemberName %type_CB1 11 "u_mtxOldLP"
               OpMemberName %type_CB1 12 "u_mtxOldWV"
               OpMemberName %type_CB1 13 "u_mtxOldWP"
               OpMemberName %type_CB1 14 "u_mtxOldVP"
               OpName %CB1 "CB1"
               OpName %in_var_POSITION "in.var.POSITION"
               OpName %in_var_COLOR0 "in.var.COLOR0"
               OpName %out_var_TEXCOORD0 "out.var.TEXCOORD0"
               OpName %out_var_TEXCOORD1 "out.var.TEXCOORD1"
               OpName %main "main"
               OpDecorate %gl_Position BuiltIn Position
               OpDecorate %in_var_POSITION Location 0
               OpDecorate %in_var_COLOR0 Location 8
               OpDecorate %out_var_TEXCOORD0 Location 0
               OpDecorate %out_var_TEXCOORD1 Location 1
               OpDecorate %CB1 DescriptorSet 0
               OpDecorate %CB1 Binding 1
               OpMemberDecorate %type_CB1 0 Offset 0
               OpMemberDecorate %type_CB1 0 MatrixStride 16
               OpMemberDecorate %type_CB1 0 ColMajor
               OpMemberDecorate %type_CB1 1 Offset 64
               OpMemberDecorate %type_CB1 1 MatrixStride 16
               OpMemberDecorate %type_CB1 1 ColMajor
               OpMemberDecorate %type_CB1 2 Offset 128
               OpMemberDecorate %type_CB1 2 MatrixStride 16
               OpMemberDecorate %type_CB1 2 ColMajor
               OpMemberDecorate %type_CB1 3 Offset 192
               OpMemberDecorate %type_CB1 3 MatrixStride 16
               OpMemberDecorate %type_CB1 3 ColMajor
               OpMemberDecorate %type_CB1 4 Offset 256
               OpMemberDecorate %type_CB1 4 MatrixStride 16
               OpMemberDecorate %type_CB1 4 ColMajor
               OpMemberDecorate %type_CB1 5 Offset 320
               OpMemberDecorate %type_CB1 5 MatrixStride 16
               OpMemberDecorate %type_CB1 5 ColMajor
               OpMemberDecorate %type_CB1 6 Offset 384
               OpMemberDecorate %type_CB1 6 MatrixStride 16
               OpMemberDecorate %type_CB1 6 ColMajor
               OpMemberDecorate %type_CB1 7 Offset 448
               OpMemberDecorate %type_CB1 7 MatrixStride 16
               OpMemberDecorate %type_CB1 7 ColMajor
               OpMemberDecorate %type_CB1 8 Offset 512
               OpMemberDecorate %type_CB1 8 MatrixStride 16
               OpMemberDecorate %type_CB1 8 ColMajor
               OpMemberDecorate %type_CB1 9 Offset 576
               OpMemberDecorate %type_CB1 9 MatrixStride 16
               OpMemberDecorate %type_CB1 9 ColMajor
               OpMemberDecorate %type_CB1 10 Offset 640
               OpMemberDecorate %type_CB1 10 MatrixStride 16
               OpMemberDecorate %type_CB1 10 ColMajor
               OpMemberDecorate %type_CB1 11 Offset 704
               OpMemberDecorate %type_CB1 11 MatrixStride 16
               OpMemberDecorate %type_CB1 11 ColMajor
               OpMemberDecorate %type_CB1 12 Offset 768
               OpMemberDecorate %type_CB1 12 MatrixStride 16
               OpMemberDecorate %type_CB1 12 ColMajor
               OpMemberDecorate %type_CB1 13 Offset 832
               OpMemberDecorate %type_CB1 13 MatrixStride 16
               OpMemberDecorate %type_CB1 13 ColMajor
               OpMemberDecorate %type_CB1 14 Offset 896
               OpMemberDecorate %type_CB1 14 MatrixStride 16
               OpMemberDecorate %type_CB1 14 ColMajor
               OpDecorate %type_CB1 Block
      %float = OpTypeFloat 32
    %float_1 = OpConstant %float 1
        %int = OpTypeInt 32 1
      %int_2 = OpConstant %int 2
  %float_0_5 = OpConstant %float 0.5
    %v4float = OpTypeVector %float 4
%mat4v4float = OpTypeMatrix %v4float 4
   %type_CB1 = OpTypeStruct %mat4v4float %mat4v4float %mat4v4float %mat4v4float %mat4v4float %mat4v4float %mat4v4float %mat4v4float %mat4v4float %mat4v4float %mat4v4float %mat4v4float %mat4v4float %mat4v4float %mat4v4float
%_ptr_Uniform_type_CB1 = OpTypePointer Uniform %type_CB1
%_ptr_Input_v4float = OpTypePointer Input %v4float
%_ptr_Output_v4float = OpTypePointer Output %v4float
       %void = OpTypeVoid
         %20 = OpTypeFunction %void
%_ptr_Uniform_mat4v4float = OpTypePointer Uniform %mat4v4float
    %v2float = OpTypeVector %float 2
        %CB1 = OpVariable %_ptr_Uniform_type_CB1 Uniform
%in_var_POSITION = OpVariable %_ptr_Input_v4float Input
%in_var_COLOR0 = OpVariable %_ptr_Input_v4float Input
%gl_Position = OpVariable %_ptr_Output_v4float Output
%out_var_TEXCOORD0 = OpVariable %_ptr_Output_v4float Output
%out_var_TEXCOORD1 = OpVariable %_ptr_Output_v4float Output
       %main = OpFunction %void None %20
         %23 = OpLabel
         %24 = OpLoad %v4float %in_var_POSITION
         %25 = OpLoad %v4float %in_var_COLOR0
         %26 = OpCompositeInsert %v4float %float_1 %24 3
         %27 = OpAccessChain %_ptr_Uniform_mat4v4float %CB1 %int_2
         %28 = OpLoad %mat4v4float %27
         %29 = OpVectorTimesMatrix %v4float %26 %28
         %30 = OpVectorShuffle %v2float %29 %29 0 1
         %31 = OpCompositeExtract %float %29 3
         %32 = OpCompositeConstruct %v2float %31 %31
         %33 = OpFAdd %v2float %30 %32
         %34 = OpVectorTimesScalar %v2float %33 %float_0_5
         %35 = OpVectorShuffle %v4float %29 %34 4 5 2 3
               OpStore %gl_Position %29
               OpStore %out_var_TEXCOORD0 %25
               OpStore %out_var_TEXCOORD1 %35
               OpReturn
               OpFunctionEnd

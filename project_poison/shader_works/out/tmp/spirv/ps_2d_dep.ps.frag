#version 460

layout(binding = 0) uniform sampler2D SPIRV_Cross_Combinedin_texClrin_texClrSampler;
layout(binding = 1) uniform sampler2D SPIRV_Cross_Combinedin_texDepin_texDepSampler;

layout(location = 0) in vec2 in_var_TEXCOORD0;
layout(location = 1) in vec4 in_var_TEXCOORD1;
layout(location = 0) out vec4 out_var_SV_Target0;

void main()
{
    vec2 _35 = vec2(in_var_TEXCOORD0.x, 1.0 - in_var_TEXCOORD0.y);
    out_var_SV_Target0 = textureLod(SPIRV_Cross_Combinedin_texClrin_texClrSampler, _35, 0.0) * in_var_TEXCOORD1;
    gl_FragDepth = textureLod(SPIRV_Cross_Combinedin_texDepin_texDepSampler, _35, 0.0).x;
}


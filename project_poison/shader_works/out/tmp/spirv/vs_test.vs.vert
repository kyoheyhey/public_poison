#version 460

out gl_PerVertex
{
    vec4 gl_Position;
};

layout(binding = 1, std140) uniform type_CB1
{
    mat4 u_mtxLW;
    mat4 u_mtxLV;
    mat4 u_mtxLP;
    mat4 u_mtxWV;
    mat4 u_mtxWP;
    mat4 u_mtxVP;
    mat4 u_mtxPW;
    mat4 u_mtxPV;
    mat4 u_mtxVW;
    mat4 u_mtxOldLW;
    mat4 u_mtxOldLV;
    mat4 u_mtxOldLP;
    mat4 u_mtxOldWV;
    mat4 u_mtxOldWP;
    mat4 u_mtxOldVP;
} CB1;

layout(location = 0) in vec4 in_var_POSITION;
layout(location = 10) in vec2 in_var_TEXCOORD0;
layout(location = 8) in vec4 in_var_COLOR0;
layout(location = 1) in vec3 in_var_NORMAL;
layout(location = 0) out vec2 out_var_TEXCOORD0;
layout(location = 1) out vec4 out_var_TEXCOORD1;
layout(location = 2) out vec3 out_var_TEXCOORD2;

void main()
{
    gl_Position = in_var_POSITION * CB1.u_mtxLP;
    out_var_TEXCOORD0 = in_var_TEXCOORD0;
    out_var_TEXCOORD1 = in_var_COLOR0;
    out_var_TEXCOORD2 = in_var_NORMAL;
}


#version 460

layout(binding = 0) uniform sampler2D SPIRV_Cross_Combinedin_texClrin_texClrSampler;

layout(location = 0) in vec2 in_var_TEXCOORD0;
layout(location = 1) in vec4 in_var_TEXCOORD1;
layout(location = 0) out vec4 out_var_SV_Target0;

void main()
{
    out_var_SV_Target0 = textureLod(SPIRV_Cross_Combinedin_texClrin_texClrSampler, vec2(in_var_TEXCOORD0.x, 1.0 - in_var_TEXCOORD0.y), 0.0) * in_var_TEXCOORD1;
}


; SPIR-V
; Version: 1.0
; Generator: Google spiregg; 0
; Bound: 24
; Schema: 0
               OpCapability Shader
               OpMemoryModel Logical GLSL450
               OpEntryPoint Fragment %main "main" %gl_FragCoord %in_var_TEXCOORD0 %in_var_TEXCOORD1 %in_var_TEXCOORD2 %out_var_SV_Target0
               OpExecutionMode %main OriginUpperLeft
               OpSource HLSL 640
               OpName %in_var_TEXCOORD0 "in.var.TEXCOORD0"
               OpName %in_var_TEXCOORD1 "in.var.TEXCOORD1"
               OpName %in_var_TEXCOORD2 "in.var.TEXCOORD2"
               OpName %out_var_SV_Target0 "out.var.SV_Target0"
               OpName %main "main"
               OpDecorate %gl_FragCoord BuiltIn FragCoord
               OpDecorate %in_var_TEXCOORD0 Location 0
               OpDecorate %in_var_TEXCOORD1 Location 1
               OpDecorate %in_var_TEXCOORD2 Location 2
               OpDecorate %out_var_SV_Target0 Location 0
      %float = OpTypeFloat 32
    %float_1 = OpConstant %float 1
    %float_0 = OpConstant %float 0
    %v4float = OpTypeVector %float 4
%_ptr_Input_v4float = OpTypePointer Input %v4float
    %v2float = OpTypeVector %float 2
%_ptr_Input_v2float = OpTypePointer Input %v2float
    %v3float = OpTypeVector %float 3
%_ptr_Input_v3float = OpTypePointer Input %v3float
%_ptr_Output_v4float = OpTypePointer Output %v4float
       %void = OpTypeVoid
         %18 = OpTypeFunction %void
%gl_FragCoord = OpVariable %_ptr_Input_v4float Input
%in_var_TEXCOORD0 = OpVariable %_ptr_Input_v2float Input
%in_var_TEXCOORD1 = OpVariable %_ptr_Input_v4float Input
%in_var_TEXCOORD2 = OpVariable %_ptr_Input_v3float Input
%out_var_SV_Target0 = OpVariable %_ptr_Output_v4float Output
       %main = OpFunction %void None %18
         %19 = OpLabel
         %20 = OpLoad %v2float %in_var_TEXCOORD0
         %21 = OpCompositeExtract %float %20 0
         %22 = OpCompositeExtract %float %20 1
         %23 = OpCompositeConstruct %v4float %21 %22 %float_0 %float_1
               OpStore %out_var_SV_Target0 %23
               OpReturn
               OpFunctionEnd

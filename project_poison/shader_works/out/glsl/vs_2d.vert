#version 460

out gl_PerVertex
{
    vec4 gl_Position;
};

layout(binding = 0, std140) uniform type_CB0
{
    mat4 u_mtxLW2D;
} CB0;

layout(binding = 1, std140) uniform type_CB1
{
    vec4 u_uv;
} CB1;

layout(location = 0) in vec4 in_var_POSITION;
layout(location = 10) in vec2 in_var_TEXCOORD0;
layout(location = 8) in vec4 in_var_COLOR0;
layout(location = 0) out vec2 out_var_TEXCOORD0;
layout(location = 1) out vec4 out_var_TEXCOORD1;

void main()
{
    gl_Position = in_var_POSITION * CB0.u_mtxLW2D;
    out_var_TEXCOORD0 = in_var_TEXCOORD0 + CB1.u_uv.xy;
    out_var_TEXCOORD1 = in_var_COLOR0;
}


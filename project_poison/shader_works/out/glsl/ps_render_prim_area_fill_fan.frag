#version 460

layout(binding = 1, std140) uniform type_CB1
{
    mat4 u_mtxLW;
    mat4 u_mtxLV;
    mat4 u_mtxLP;
    mat4 u_mtxWV;
    mat4 u_mtxWP;
    mat4 u_mtxVP;
    mat4 u_mtxPW;
    mat4 u_mtxPV;
    mat4 u_mtxVW;
    mat4 u_mtxOldLW;
    mat4 u_mtxOldLV;
    mat4 u_mtxOldLP;
    mat4 u_mtxOldWV;
    mat4 u_mtxOldWP;
    mat4 u_mtxOldVP;
} CB1;

layout(binding = 0, std140) uniform type_CB0
{
    mat4 u_invFan;
} CB0;

layout(binding = 0) uniform sampler2D SPIRV_Cross_Combinedin_texDepin_texDepSampler;

layout(location = 0) in vec4 in_var_TEXCOORD0;
layout(location = 1) in vec4 in_var_TEXCOORD1;
layout(location = 0) out vec4 out_var_SV_Target0;

void main()
{
    vec3 _52 = in_var_TEXCOORD1.xyz / vec3(1.0 / gl_FragCoord.w);
    vec3 _55 = _52;
    _55.y = 1.0 - _52.y;
    vec4 _71 = vec4((_55.xy * vec2(2.0, -2.0)) + vec2(-1.0, 1.0), textureLod(SPIRV_Cross_Combinedin_texDepin_texDepSampler, vec2(_52.xy), 0.0).x, 1.0) * CB1.u_mtxPW;
    vec3 _75 = _71.xyz / vec3(_71.w);
    vec4 _77 = vec4(_75.x, _75.y, _75.z, _71.w);
    _77.w = 1.0;
    vec4 _80 = _77 * CB0.u_invFan;
    vec2 _84 = _80.xz;
    vec3 _93 = step(vec3(dot(_84, _84), abs(_80.y), -normalize(_84).y), vec3(1.0, 1.0, -CB0.u_invFan[3u].x));
    out_var_SV_Target0 = mix(vec4(0.0), in_var_TEXCOORD0, vec4((_93.x * _93.y) * _93.z));
}


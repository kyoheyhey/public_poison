#version 460

layout(binding = 1, std140) uniform type_CB1
{
    mat4 u_mtxLW;
    mat4 u_mtxLV;
    mat4 u_mtxLP;
    mat4 u_mtxWV;
    mat4 u_mtxWP;
    mat4 u_mtxVP;
    mat4 u_mtxPW;
    mat4 u_mtxPV;
    mat4 u_mtxVW;
    mat4 u_mtxOldLW;
    mat4 u_mtxOldLV;
    mat4 u_mtxOldLP;
    mat4 u_mtxOldWV;
    mat4 u_mtxOldWP;
    mat4 u_mtxOldVP;
} CB1;

layout(binding = 0, std140) uniform type_CB0
{
    mat4 u_invAabb;
} CB0;

layout(binding = 0) uniform sampler2D SPIRV_Cross_Combinedin_texDepin_texDepSampler;

layout(location = 0) in vec4 in_var_TEXCOORD0;
layout(location = 1) in vec4 in_var_TEXCOORD1;
layout(location = 0) out vec4 out_var_SV_Target0;

void main()
{
    vec3 _50 = in_var_TEXCOORD1.xyz / vec3(1.0 / gl_FragCoord.w);
    vec3 _53 = _50;
    _53.y = 1.0 - _50.y;
    vec4 _69 = vec4((_53.xy * vec2(2.0, -2.0)) + vec2(-1.0, 1.0), textureLod(SPIRV_Cross_Combinedin_texDepin_texDepSampler, vec2(_50.xy), 0.0).x, 1.0) * CB1.u_mtxPW;
    vec3 _73 = _69.xyz / vec3(_69.w);
    vec4 _75 = vec4(_73.x, _73.y, _73.z, _69.w);
    _75.w = 1.0;
    vec4 _80 = step(abs(_75 * CB0.u_invAabb), vec4(1.0));
    out_var_SV_Target0 = mix(vec4(0.0), in_var_TEXCOORD0, vec4((_80.x * _80.y) * _80.z));
}


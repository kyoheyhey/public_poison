#version 460

layout(binding = 1, std140) uniform type_CB1
{
    mat4 u_mtxLW;
    mat4 u_mtxLV;
    mat4 u_mtxLP;
    mat4 u_mtxWV;
    mat4 u_mtxWP;
    mat4 u_mtxVP;
    mat4 u_mtxPW;
    mat4 u_mtxPV;
    mat4 u_mtxVW;
    mat4 u_mtxOldLW;
    mat4 u_mtxOldLV;
    mat4 u_mtxOldLP;
    mat4 u_mtxOldWV;
    mat4 u_mtxOldWP;
    mat4 u_mtxOldVP;
} CB1;

layout(binding = 0, std140) uniform type_CB0
{
    vec4 u_p0;
    vec4 u_p1;
    vec4 u_radius;
} CB0;

layout(binding = 0) uniform sampler2D SPIRV_Cross_Combinedin_texDepin_texDepSampler;

layout(location = 0) in vec4 in_var_TEXCOORD0;
layout(location = 1) in vec4 in_var_TEXCOORD1;
layout(location = 0) out vec4 out_var_SV_Target0;

void main()
{
    vec3 _55 = in_var_TEXCOORD1.xyz / vec3(1.0 / gl_FragCoord.w);
    vec3 _58 = _55;
    _58.y = 1.0 - _55.y;
    vec4 _74 = vec4((_58.xy * vec2(2.0, -2.0)) + vec2(-1.0, 1.0), textureLod(SPIRV_Cross_Combinedin_texDepin_texDepSampler, vec2(_55.xy), 0.0).x, 1.0) * CB1.u_mtxPW;
    vec3 _85 = CB0.u_p1.xyz - CB0.u_p0.xyz;
    vec3 _86 = (_74.xyz / vec3(_74.w)).xyz;
    vec3 _87 = _86 - CB0.u_p0.xyz;
    vec3 _88 = _86 - CB0.u_p1.xyz;
    float _89 = dot(_87, _85);
    float _90 = dot(_85, _85);
    float _91 = dot(_87, _87);
    float _101;
    if (_89 >= _90)
    {
        _101 = dot(_88, _88);
    }
    else
    {
        _101 = (_89 <= 0.0) ? _91 : (_91 - ((_89 * _89) / _90));
    }
    out_var_SV_Target0 = mix(vec4(0.0), in_var_TEXCOORD0, vec4(step(_101, CB0.u_radius.x * CB0.u_radius.x)));
}


#version 460

layout(binding = 1, std140) uniform type_CB1
{
    mat4 u_mtxLW;
    mat4 u_mtxLV;
    mat4 u_mtxLP;
    mat4 u_mtxWV;
    mat4 u_mtxWP;
    mat4 u_mtxVP;
    mat4 u_mtxPW;
    mat4 u_mtxPV;
    mat4 u_mtxVW;
    mat4 u_mtxOldLW;
    mat4 u_mtxOldLV;
    mat4 u_mtxOldLP;
    mat4 u_mtxOldWV;
    mat4 u_mtxOldWP;
    mat4 u_mtxOldVP;
} CB1;

layout(binding = 0, std140) uniform type_CB0
{
    mat4 u_sphere;
} CB0;

layout(binding = 0) uniform sampler2D SPIRV_Cross_Combinedin_texDepin_texDepSampler;

layout(location = 0) in vec4 in_var_TEXCOORD0;
layout(location = 1) in vec4 in_var_TEXCOORD1;
layout(location = 0) out vec4 out_var_SV_Target0;

void main()
{
    vec3 _56 = in_var_TEXCOORD1.xyz / vec3(1.0 / gl_FragCoord.w);
    vec3 _59 = _56;
    _59.y = 1.0 - _56.y;
    vec4 _75 = vec4((_59.xy * vec2(2.0, -2.0)) + vec2(-1.0, 1.0), textureLod(SPIRV_Cross_Combinedin_texDepin_texDepSampler, vec2(_56.xy), 0.0).x, 1.0) * CB1.u_mtxPW;
    vec3 _88 = vec3(CB0.u_sphere[0u].w, CB0.u_sphere[1u].w, CB0.u_sphere[2u].w) - (_75.xyz / vec3(_75.w)).xyz;
    float _89 = _88.x;
    float _91 = _88.y;
    float _94 = _88.z;
    out_var_SV_Target0 = mix(vec4(0.0), in_var_TEXCOORD0, vec4(step(((_89 * _89) + (_91 * _91)) + (_94 * _94), CB0.u_sphere[0u].x * CB0.u_sphere[0u].x)));
}


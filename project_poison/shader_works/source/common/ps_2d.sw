
//-------------------------------------------------------------------------------------------------
// define
//#define DEF_NO_TEX
//#define DEF_TEX_DEPTH

//-------------------------------------------------------------------------------------------------
// include
#include "common.h"

//-------------------------------------------------------------------------------------------------
// 入力情報
struct PixelIn
{
	PS_IN_POS(float4);
	PASS_PARAM0(float2, uv);
	PASS_PARAM1(float4, clr);
};

//-------------------------------------------------------------------------------------------------
// 出力情報
struct PixelOut
{
	PS_OUT_COLOR0(float4);
#ifdef DEF_TEX_DEPTH
	PS_OUT_DEPTH(float);
#endif // DEF_TEX_DEPTH
};


//-------------------------------------------------------------------------------------------------
// texture
IN_TEX2D(in_texClr, 0);		// カラーテクスチャ
IN_TEX2D(in_texDep, 1);		// デプステクスチャ


//-------------------------------------------------------------------------------------------------
// entry
PixelOut	main(PixelIn IN)
{
	PixelOut OUT;
	//OUT.clr0 = float4(0.0, 1.0, 1.0, 1.0);
	//OUT.clr0 = IN.pos;
	//OUT.clr0 = IN.clr;
	//OUT.clr0 = float4(IN.uv, 0.0, 1.0);
#ifndef DEF_NO_TEX
	float4 texClr = TexFatchLodZero(in_texClr, IN.uv);
#else
	float4 texClr = 1.0;
#endif // !DEF_NO_TEX

	OUT.clr0 = texClr * IN.clr;
#ifdef DEF_TEX_DEPTH
	OUT.dep = TexFatchLodZero(in_texDep, IN.uv).x;
#endif // DEF_TEX_DEPTH
	return OUT;
}


//-------------------------------------------------------------------------------------------------
// define

//-------------------------------------------------------------------------------------------------
// include
#include "common.h"

//-------------------------------------------------------------------------------------------------
// 入力情報
struct VertexIn
{
	VS_IN_POS(float4);
	VS_IN_TEXCOORD0(float2);
	VS_IN_COLOR0(float4);
};

//-------------------------------------------------------------------------------------------------
// 出力情報
struct VertexOut
{
	VS_OUT_POS(float4);
	PASS_PARAM0(float2, uv);
	PASS_PARAM1(float4, clr);
};

//-------------------------------------------------------------------------------------------------
// uniform
UNIFORM_BLOCK(0)
{
	mtx44 u_mtxLW2D : packoffset(c0);
}
UNIFORM_BLOCK(1)
{
	float4 u_uv : packoffset(c0);
}


//-------------------------------------------------------------------------------------------------
// entry
VertexOut main(VertexIn IN)
{
	VertexOut OUT;
#ifdef USE_DIRECT
	OUT.pos = IN.pos;
#else
	OUT.pos = mul(u_mtxLW2D, IN.pos);
	IN.uv0 += u_uv.xy;
#endif // USE_DIRECT

	OUT.uv = IN.uv0;
	OUT.clr = IN.clr0;
	OUT.nrm = IN.nrm;
	
	return OUT;
}

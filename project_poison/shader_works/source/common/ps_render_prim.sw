
//-------------------------------------------------------------------------------------------------
// define
//#define USE_AREA_FILL_BOX
//#define USE_AREA_FILL_SPHERE
//#define USE_AREA_FILL_CAPSULE
//#define USE_AREA_FILL_PIPE
//#define USE_AREA_FILL_FAN

// ΜζhθΒΤ΅
#if defined( USE_AREA_FILL_BOX ) || defined( USE_AREA_FILL_SPHERE ) || defined( USE_AREA_FILL_CAPSULE ) || defined( USE_AREA_FILL_PIPE ) || defined( USE_AREA_FILL_FAN )
#define USE_AREA_FILL
#endif


//-------------------------------------------------------------------------------------------------
// include
#include "common.h"
#include "common_depth.h"
#include "common_uniform_block.h"


//-------------------------------------------------------------------------------------------------
// όΝξρ
struct PixelIn
{
	PS_IN_POS(float4);
	PASS_PARAM0(float4, clr);
	PASS_PARAM1(float4, wvpPos);
};

//-------------------------------------------------------------------------------------------------
// oΝξρ
struct PixelOut
{
	PS_OUT_COLOR0(float4);
};


//-------------------------------------------------------------------------------------------------
// uniform
UNIFORM_BLOCK(0)
{
#if defined( USE_AREA_FILL_BOX )
	mtx44	u_invAabb	: packoffset(c0);	// AABBsρΜtsρ
#elif defined( USE_AREA_FILL_SPHERE )
	mtx44	u_sphere	: packoffset(c0);	// 
#elif defined( USE_AREA_FILL_CAPSULE )
	float4	u_p0		: packoffset(c0);	// JvZp0
	float4	u_p1		: packoffset(c1);	// JvZp1
	float4	u_radius	: packoffset(c2);	// JvZradius
#elif defined( USE_AREA_FILL_PIPE )
	mtx44	u_invPipe	: packoffset(c0);	// ~Μtsρ
#elif defined( USE_AREA_FILL_FAN )
	mtx44	u_invFan	: packoffset(c0);	// ξΜtsρ
#else
#endif
};

//-------------------------------------------------------------------------------------------------
// texture
IN_TEX2D(in_texDep, 0);		// fvXeNX`


//-------------------------------------------------------------------------------------------------
// entry
PixelOut	main(PixelIn IN)
{
	PixelOut OUT;

	float flag = 1.0;
#ifdef USE_AREA_FILL
	{
		// XN[Xy[XUV
		float viewZ = -IN.wvpPos.w;
		IN.wvpPos.w = CONV_PS_POS_W(IN.pos);
		float3 screenUV = IN.wvpPos.xyz / IN.wvpPos.w;
		screenUV.y = 1.0f - screenUV.y;
		// [x©η[hΐWZo
		float depth = TexFatchLodZero(in_texDep, screenUV.xy).x;
		float4 pos = GetDepthToWorldPos(screenUV.xy, depth, u_mtxPW);

#if defined( USE_AREA_FILL_BOX )
		// §ϋΜ»θ
		pos = mul(u_invAabb, pos);
		pos = TEST_LEQUAL(abs(pos), 1.0);
		flag = pos.x * pos.y * pos.z;
#elif defined( USE_AREA_FILL_SPHERE )
		// Μ»θ
		float3	len = float3(u_sphere[0].w, u_sphere[1].w, u_sphere[2].w) - pos.xyz;
		flag = TEST_LEQUAL(((len.x * len.x) + (len.y * len.y) + (len.z * len.z)), u_sphere[0].x * u_sphere[0].x);
#elif defined( USE_AREA_FILL_CAPSULE )
		// JvZ»θ
		float3 ab = u_p1.xyz - u_p0.xyz;
		float3 ac = pos.xyz - u_p0.xyz;
		float3 bc = pos.xyz - u_p1.xyz;
		float e = dot(ac, ab);
		float f = dot(ab, ab);
		float len2 = dot(ac, ac) - e * e / f;
		if (e <= 0.0)
		{
			len2 = dot(ac, ac);
		}
		if (e >= f)
		{
			len2 = dot(bc, bc);
		}
		flag = TEST_LEQUAL(len2, u_radius.x * u_radius.x);
#elif defined( USE_AREA_FILL_PIPE )
		// ~»θ
		pos = mul(u_invPipe, pos);
		float2 hit = TEST_LEQUAL(float2(dot(pos.xz, pos.xz), abs(pos.y)), 1.0);
		flag = hit.x * hit.y;
#elif defined( USE_AREA_FILL_FAN )
		// ξ»θ
		pos = mul(u_invFan, pos);
		float cosValue = -u_invFan[3].x;	// px
		float3 hit = TEST_LEQUAL(float3(dot(pos.xz, pos.xz), abs(pos.y), -dot(float2(0, 1), normalize(pos.xz))), float3(1, 1, cosValue));
		flag = hit.x * hit.y * hit.z;
#else
#endif
	}
#endif // USE_AREA_FILL

	OUT.clr0 = lerp(0.0, IN.clr, flag);
	return OUT;
}


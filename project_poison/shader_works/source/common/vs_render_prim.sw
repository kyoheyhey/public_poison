
//-------------------------------------------------------------------------------------------------
// define

//-------------------------------------------------------------------------------------------------
// include
#include "common.h"
#include "common_uniform_block.h"


//-------------------------------------------------------------------------------------------------
// 入力情報
struct VertexIn
{
	VS_IN_POS(float4);
	VS_IN_COLOR0(float4);
};

//-------------------------------------------------------------------------------------------------
// 出力情報
struct VertexOut
{
	VS_OUT_POS(float4);
	PASS_PARAM0(float4, clr);
	PASS_PARAM1(float4, wvpPos);
};


//-------------------------------------------------------------------------------------------------
// entry
VertexOut main(VertexIn IN)
{
	VertexOut	OUT;

	IN.pos.w = 1.0f;
	OUT.pos = mul(u_mtxLP, IN.pos);

	OUT.clr = IN.clr0;
	OUT.wvpPos = OUT.pos;
	OUT.wvpPos.xy = CONV_SETUP_SCREEN_SPACE_UV(OUT.wvpPos);

	return OUT;
}



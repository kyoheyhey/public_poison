
//-------------------------------------------------------------------------------------------------
// define

//-------------------------------------------------------------------------------------------------
// include
#include "common.h"


#define	IN_VTX_NUM	3	// 頂点入力数
#define	OUT_VTX_NUM	3	// 頂点出力数

// 入力情報
struct GeometryIn
{
	GS_IN_POS(float4);
	PASS_PARAM0(float2, uv);
	PASS_PARAM1(float4, clr);
};

// 出力情報
struct GeometryOut
{
	GS_OUT_POS(float4);
	PASS_PARAM0(float2, uv);
	PASS_PARAM1(float4, clr);
};


GEO_OUT_MAX_COUNT(OUT_VTX_NUM)
void	main(triangle GeometryIn IN[IN_VTX_NUM], inout TriangleStream<GeometryOut> OUT)
{
	float3 edge1 = (IN[1].pos.xyz - IN[0].pos.xyz);
	float3 edge2 = (IN[2].pos.xyz - IN[0].pos.xyz);
	float3 normal = normalize(cross(edge1, edge2));

	HINT_UNROLL
	for (int i = 0; i < IN_VTX_NUM; i++)
	{
		GeometryIn geoIn = IN[i];
		GeometryOut geoOut;

		geoOut.pos = geoIn.pos;
		geoOut.uv = geoIn.uv;
		geoOut.clr = geoIn.clr;
		OUT.Append(geoOut);
	}

	OUT.RestartStrip();
}

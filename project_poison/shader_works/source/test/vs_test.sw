
//-------------------------------------------------------------------------------------------------
// define

//-------------------------------------------------------------------------------------------------
// include
#include "common.h"
#include "common_uniform_block.h"


//-------------------------------------------------------------------------------------------------
// 入力情報
struct VertexIn
{
	VS_IN_POS(float4);
	VS_IN_TEXCOORD0(float2);
	VS_IN_COLOR0(float4);
	VS_IN_NORMAL(float3);
};

//-------------------------------------------------------------------------------------------------
// 出力情報
struct VertexOut
{
	VS_OUT_POS(float4);
	PASS_PARAM0(float2, uv);
	PASS_PARAM1(float4, clr);
	PASS_PARAM2(float3, nrm);
};

//-------------------------------------------------------------------------------------------------
// uniform
UNIFORM_BLOCK(2)
{
	float4 u_uv		: packoffset(c0);
}


//-------------------------------------------------------------------------------------------------
// entry
VertexOut	main(VertexIn IN)
{
	VertexOut OUT;
	OUT.pos = mul(u_mtxLP, IN.pos);
	OUT.uv = IN.uv0;// +u_uv.xy;
	OUT.clr = IN.clr0;
	OUT.nrm = IN.nrm;
	
	return OUT;
}

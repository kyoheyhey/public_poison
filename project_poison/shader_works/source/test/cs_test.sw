
//--------------------------------------------------------------------------------------
// タイル単位でテクスチャピクセル数を処理していく

//-------------------------------------------------------------------------------------------------
// define

//-------------------------------------------------------------------------------------------------
// include
// コンピュート用の定義
#include "common_compute.h"


//--------------------------------------------------------------------------------------
// define
#define THREAD_SIZE					16
#define INV_THREAD_SIZE				(1.0f/THREAD_SIZE)
#define THREAD_TILE_SIZE			(THREAD_SIZE * THREAD_SIZE)
#define THREAD_TILE_FRUSTUM_RANGE	(0.5f/THREAD_SIZE)

#define OUTPUT_FLOAT4_BUFF

//--------------------------------------------------------------------------------------
// Constant Buffers
UNIFORM_BLOCK( 0 )
{
	uint4		u_bufferSize;		// xy=srcBufferSize, zw=dstBufferSize
	float4		u_bufferInvSize;	// xy=1.0/srcBufferSize, zw=1.0/dstBufferSize
	float4		u_nearFarInvAspect;	// x=near,  y=far, zw=Invアスペクト比
	float4		u_color;			// テストカラー
};
#define SRC_SIZE			(u_bufferSize.xy)
#define SRC_SIZE_INV		(u_bufferInvSize.xy)
#define DST_SIZE			(u_bufferSize.zw)
#define DST_SIZE_INV		(u_bufferInvSize.zw)
#define NEAR_Z				(u_nearFarInvAspect.x)
#define FAR_Z				(u_nearFarInvAspect.y)
#define NEAR_FAR_Z			(u_nearFarInvAspect.xy)
#define NEAR_FAR_INV_ASPECT	(u_nearFarInvAspect)


//--------------------------------------------------------------------------------------
// in buffer


//--------------------------------------------------------------------------------------
// in texture
R_TEX2D( float4, in_texColor,	0 );	// カラー
R_TEX2D( float4, in_texNormal,	1 );	// 法線
R_TEX2D( float4, in_texDepth,	2 );	// x=デプス


//--------------------------------------------------------------------------------------
// out buffer
#ifdef OUTPUT_FLOAT4_BUFF
RW_BUFFER( float4, out_resultBuffer, 0 );	///< 結果バッファ(rgba)
#else
RW_BUFFER( uint2, out_resultBuffer, 0 );	///< 結果バッファ(uint2=rg,ba)
#endif


//--------------------------------------------------------------------------------------
// Shared Buffers
//GROUP_SHARED uint g_sTileLightIndices[MAX_LIGHTS];





//--------------------------------------------------------------------------------------
// - RGBA 16-bit per component unpack to a float4 per texel
float4 UnpackRGBA16(uint2 _e)
{
	return float4(f16tof32(_e), f16tof32(_e >> 16));
}
//--------------------------------------------------------------------------------------
// - RGBA 16-bit per component packed into a uint2 per texel
uint2 PackRGBA16(float4 _c)
{
	return f32tof16(_c.rg) | (f32tof16(_c.ba) << 16);
}
//--------------------------------------------------------------------------------------
// ピクセル座標からIndex算出
// @param[in]	_coords	ピクセル座標インデックス
// @param[in]	_coords	書き込みバッファサイズ
uint GetFramebufferSampleAddress( uint2 _coords, uint2 _size )
{
	return min(_coords.y, _size.y) * _size.x + min(_coords.x, _size.x);
}


//--------------------------------------------------------------------------------------
/// 書き込み
/// @param[in]	_coords	ピクセル座標インデックス
/// @param[in]	_coords	書き込みバッファサイズ
/// @param[in]	_value	書き込む値
void WriteSample( uint2 _coords, uint2 _size, float4 _value )
{
#ifdef OUTPUT_FLOAT4_BUFF
	// 直接保持
	out_resultBuffer[ GetFramebufferSampleAddress(_coords, _size) ] = _value;
#else
	// 本来は rg,ba ごとに圧縮してる
	out_resultBuffer[ GetFramebufferSampleAddress(_coords, _size) ] = PackRGBA16(_value);
#endif
}



//--------------------------------------------------------------------------------------
// main
[NUM_THREADS(THREAD_SIZE, THREAD_SIZE, 1)]
void	main( CsIn IN )
{
	// グループIdx計算
	uint groupIndex = IN.GTid.y * THREAD_SIZE + IN.GTid.x;

	// uv = スレッド番号( ピクセル座標 )
	// DST_SIZE で来るのでターゲットサイズに補正
	uint4 pixelCoods = uint4( float4(IN.DTid.xy, IN.Gid.xy) * SRC_SIZE.xyxy * DST_SIZE_INV.xyxy );

	float2 uv = pixelCoods.xy * SRC_SIZE_INV;
	float3 texClr = in_texColor.Load(int3(pixelCoods.xy, 0)).xyz;
	float3 normal = in_texNormal.Load(int3(pixelCoods.xy, 0)).xyz;
	float depth = in_texDepth.Load(int3(pixelCoods.xy, 0)).x;


	// Write result
	pixelCoods.xy = IN.DTid.xy;
	//WriteSample(pixelCoods.xy, DST_SIZE, float4(uv, 0.0, 1.0));
	WriteSample(pixelCoods.xy, DST_SIZE, float4(texClr, 1.0));
	//WriteSample(pixelCoods.xy, DST_SIZE, float4(normal, 1.0));
	//WriteSample(pixelCoods.xy, DST_SIZE, u_color);
	//WriteSample(0.0.xy, DST_SIZE, float4(0.0, 1.0, 0.0, 1.0));
}


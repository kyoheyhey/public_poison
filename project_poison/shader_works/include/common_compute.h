
#ifndef	__COMMON_COMPUTE_H__
#define	__COMMON_COMPUTE_H__

#include "common.h"

//--------------------------------------------------------------------------------------
// 型定義(固定。これ以上の要素はない)
//--------------------------------------------------------------------------------------
//
//	SV_GroupID			=	グループ数はCPU側のDispatch命令で決定される
//							CPU側のDispatch命令に渡したx,y,zを最大値とする、(0 〜 最大値-1)の値
//
//	SV_DispatchThreadID	=	ディスパッチ全体の中で自分はどこか、というID
//							(SV_GID.xyz * GPU.numthreads.xyz) + SV_GTID.xyz で計算される
//							(0 〜 (Dispatch * numthreads)-1)の値
//
//	SV_GroupThreadID	=	グループのスレッド数はGPU側のnumthreads命令で決定される
//							GPU側のnumthreads命令に渡したx,y,zを最大値とする、(0 〜 最大値-1)の値
//
//	SV_GroupIndex		=	SV_GroupThreadID と numthreads のxyzを使って計算されたindex番号
//							(SV_GTID.z * numthreads.x * numthreads.y) + (SV_GTID.y * numthreads.x) + (SV_GTID.x)
//
//--------------------------------------------------------------------------------------
struct CsIn
{
	uint3	Gid		: GROUP_ID;
	uint3	DTid	: DISPATCH_THREAD_ID;
	uint3	GTid	: GROUP_THREAD_ID;
	uint	GIdx	: GROUP_INDEX;
};

#endif// __COMMON_COMPUTE_H__

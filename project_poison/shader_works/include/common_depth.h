
#ifndef	__COMMON_DEPTH_H__
#define	__COMMON_DEPTH_H__


//-------------------------------------------------------------------------------------------------
//@brief	深度値をビュー空間のZ値に変換(マイナス)
//-------------------------------------------------------------------------------------------------
//#if defined( OPENGL_PERSEPECTIVE )
//#define DEPTH_TO_VIEW_Z( _depth )	(2.0 * NEAR_Z * FAR_Z / (_depth * (FAR_Z - NEAR_Z) - (FAR_Z + NEAR_Z)))
//#elif defined( DIRECTX_PERSEPECTIVE )
//#define DEPTH_TO_VIEW_Z( _depth )	(NEAR_Z * FAR_Z / (_depth * (FAR_Z - NEAR_Z) - (FAR_Z)))
//#endif
#define DEPTH_TO_VIEW_Z( _depth, _mtxVP )	((_mtxVP[2][3] - _depth * _mtxVP[3][3]) / (_depth * _mtxVP[3][2] - _mtxVP[2][2]))

#define GetViewZ	DEPTH_TO_VIEW_Z

//-------------------------------------------------------------------------------------------------
//@brief	ビューZ(マイナス)を深度値に変換
//-------------------------------------------------------------------------------------------------
//#if defined( OPENGL_PERSEPECTIVE )
//#define VIEW_Z_TO_DEPTH( _viewZ )	(2.0 * NEAR_Z * FAR_Z / (_viewZ * (FAR_Z - NEAR_Z)) + ((FAR_Z + NEAR_Z) / (FAR_Z - NEAR_Z)))
//#elif defined( DIRECTX_PERSEPECTIVE )
//#define VIEW_Z_TO_DEPTH( _viewZ )	(NEAR_Z * FAR_Z / (_viewZ * (FAR_Z - NEAR_Z)) + (FAR_Z / (FAR_Z - NEAR_Z)))
//#endif
#define VIEW_Z_TO_DEPTH( _viewZ, _mtxVP )	((_viewZ * _mtxVP[2][2] + _mtxVP[2][3]) / (_viewZ * _mtxVP[3][2] + _mtxVP[3][3]))

#define GetDepth	VIEW_Z_TO_DEPTH

//-------------------------------------------------------------------------------------------------
//@brief	線形Z変換(非線形デプスを線形デプスに変換)
//-------------------------------------------------------------------------------------------------
#define DEPTH_TO_LINEAR_DEPTH( _viewZ, _nearZ, _farZ )	((_viewZ - _nearZ) / (_farZ - _nearZ))

#define GetDepthLinear	DEPTH_TO_LINEAR_DEPTH


//-------------------------------------------------------------------------------------------------
//@brief	ソフトパーティクル計算
//@param[in]	_depth_dst		描きこみ済みdepth(texDepth)
//@param[in]	_depth_src		確認したいdepth
//@param[in]	_fade_length	フェード距離
//-------------------------------------------------------------------------------------------------
//float GetSoftParticleRate(float _depth_dst, float _depth_src, float _fade_length)
//{
//	// zに変換
//	float2 depth = float2(_depth_dst, _depth_src);
//	depth = -GetViewZ(depth);
//
//	// Z距離(背景Z - 現在のZ) 最低0, 上限は無し
//	float z_distance = max(depth.x - depth.y, 0);
//
//	// Z距離が _fade_length 以下のとき、rateを下げていく
//	return saturate(z_distance / _fade_length);
//}


//-------------------------------------------------------------------------------------------------
//@brief	深度値をビュー座標に変換する
//@param[in]	_uv			uv座標
//@param[in]	_depth		深度
//@param[in]	_mtxPW		プロジェクション⇒ビュー行列
//-------------------------------------------------------------------------------------------------
float4	GetDepthToViewPos(float2 _uv, float _depth, mtx44 _mtxPV)
{
	// プロジェクション座標
	float4 proj_pos = float4(_uv * float2(2.0, -2.0) + float2(-1.0, 1.0), _depth, 1.0);

	// ビュー座標系
	float4 view_pos = mul(_mtxPV, proj_pos);
	view_pos.xyz /= view_pos.w;
	view_pos.w = 1.0;
	return view_pos;
}

//-------------------------------------------------------------------------------------------------
//@brief	深度値をワールド座標に変換する
//@param[in]	_uv			uv座標
//@param[in]	_depth		深度
//@param[in]	_mtxPW		プロジェクション⇒ワールド行列
//-------------------------------------------------------------------------------------------------
float4	GetDepthToWorldPos(float2 _uv, float _depth, mtx44 _mtxPW)
{
	// プロジェクション座標
	float4 proj_pos = float4(_uv * float2(2.0, -2.0) + float2(-1.0, 1.0), _depth, 1.0);

	// ワールド座標系
	float4 world_pos = mul(_mtxPW, proj_pos);
	world_pos.xyz /= world_pos.w;
	world_pos.w = 1.0;
	return world_pos;
}


#endif// __COMMON_DEPTH_H__

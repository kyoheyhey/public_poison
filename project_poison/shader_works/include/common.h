//-------------------------------------------------------------------------------------------------
// シェーダー共通ヘッダー
// USE_GLSLって定義してるけど実は使ってない〜

#ifndef	__COMMON_H__
#define	__COMMON_H__


//-------------------------------------------------------------------------------------------------
// version
#if defined( USE_HLSL ) || defined( USE_SPIRV ) || defined( USE_NX ) || defined( USE_PSSL )

#elif defined( USE_GLSL )
#version 460

#endif


//-------------------------------------------------------------------------------------------------
// 修飾子
#if defined( USE_HLSL ) || defined( USE_SPIRV ) || defined( USE_NX ) || defined( USE_PSSL )

#elif defined( USE_GLSL )
// ****layout修飾子****
// location = #		ロケーション固定
// binding = #		シェーダ側からのglUniform1i()（プログラム側からロケーションバインドの必要なくなった？）
// component = #	hlslで言うところの: packoffset(c0)
// offset = #		atomic_uintを使用時の開始位置オフセット（指定されなければ4byte自動オフセット）
// shared			デフォルト。複数のプログラムで、同じレイアウトが保障されるらしい
// packed			コンパイラによってメモリの最適化されるのを許容する
// row_major		デフォルト。行列を行優先
// column_major		行列を列優先
// std###			なぞ。#versionこれと合わせたらええんちゃうかなぁ（std140, std430だけあるらしい）

#endif


//-------------------------------------------------------------------------------------------------
// 型宣言
#if defined( USE_HLSL ) || defined( USE_SPIRV ) || defined( USE_NX )
#define is_front_face_type	bool

#elif defined( USE_PSSL )
#define is_front_face_type	uint

#elif defined( USE_GLSL )
// floatの精度の指定
precision mediump float;
// 型宣言
#define bool2				bvec2
#define bool3				bvec3
#define bool4				bvec4
// 型宣言
#define int2				ivec2
#define int3				ivec3
#define int4				ivec4
// 型宣言(uintはOpenGL 3.0 (GLSL 1.3)から)
#define uint2				uvec2
#define uint3				uvec3
#define uint4				uvec4
// 型宣言(GLSLにはない)
#define half				float
#define half2				vec2
#define half3				vec3
#define half4				vec4
// 型宣言
#define float2				vec2
#define float3				vec3
#define float4				vec4
// 型宣言(OpenGL 4.0 (GLSL 4.0)から)
//#define double				float
#define double2				dvec2
#define double3				dvec3
#define double4				dvec4
// 型宣言
#define is_front_face_type	bool

#endif


//-------------------------------------------------------------------------------------------------
// 精度指定付き変数
#if defined( USE_NX )
// NX
#define fp16int				min16int
#define fp16int2			min16int2
#define fp16int3			min16int3
#define fp16int4			min16int4
#define fp16uint			min16uint
#define fp16uint2			min16uint2
#define fp16uint3			min16uint3
#define fp16uint4			min16uint4
#define fp16float			min16float
#define fp16float2			min16float2
#define fp16float3			min16float3
#define fp16float4			min16float4
#define fp16float1x1		min16float1x1
#define fp16float1x2		min16float1x2
#define fp16float1x3		min16float1x3
#define fp16float1x4		min16float1x4
#define fp16float2x1		min16float2x1
#define fp16float2x2		min16float2x2
#define fp16float2x3		min16float2x3
#define fp16float2x4		min16float2x4
#define fp16float3x1		min16float3x1
#define fp16float3x2		min16float3x2
#define fp16float3x3		min16float3x3
#define fp16float3x4		min16float3x4
#define fp16float4x1		min16float4x1
#define fp16float4x2		min16float4x2
#define fp16float4x3		min16float4x3
#define fp16float4x4		min16float4x4
#define fp16mtx22			min16float2x2
#define fp16mtx33			min16float3x3
#define fp16mtx44			min16float4x4
#define fp16mtx32			min16float2x3
#define fp16mtx42			min16float2x4
#define fp16mtx43			min16float3x4

#else
#define fp16int				int
#define fp16int2			int2
#define fp16int3			int3
#define fp16int4			int4
#define fp16uint			uint
#define fp16uint2			uint2
#define fp16uint3			uint3
#define fp16uint4			uint4
#define fp16float			float
#define fp16float2			float2
#define fp16float3			float3
#define fp16float4			float4
#define fp16float1x1		float1x1
#define fp16float1x2		float1x2
#define fp16float1x3		float1x3
#define fp16float1x4		float1x4
#define fp16float2x1		float2x1
#define fp16float2x2		float2x2
#define fp16float2x3		float2x3
#define fp16float2x4		float2x4
#define fp16float3x1		float3x1
#define fp16float3x2		float3x2
#define fp16float3x3		float3x3
#define fp16float3x4		float3x4
#define fp16float4x1		float4x1
#define fp16float4x2		float4x2
#define fp16float4x3		float4x3
#define fp16float4x4		float4x4
#define fp16mtx22			float2x2
#define fp16mtx33			float3x3
#define fp16mtx44			float4x4
#define fp16mtx32			float2x3
#define fp16mtx42			float2x4
#define fp16mtx43			float3x4

#endif

//--------------------------------------------------------------------------------------
// 圧縮半浮動小数
#if defined( USE_PSSL )
// PS4迢ｬ閾ｪ險ｭ螳・
#define COMPRESSED_F16		compressed_16_float
#define COMPRESSED_U16		compressed_16_uint
#define COMPRESSED_S16		compressed_16_int
#define COMPRESSED_U8		compressed_8_uint
#define COMPRESSED_UN16		compressed_16_unorm
#define COMPRESSED_SN16		compressed_16_snorm

#endif

//-------------------------------------------------------------------------------------------------
// 行列型宣言
#if defined( USE_HLSL ) || defined( USE_SPIRV ) || defined( USE_NX ) || defined( USE_PSSL )
// マトリクスは行優先(と言いながらプログラム側から受け取る場合に変換をかけずに受け取る)
#pragma pack_matrix( row_major )
#define ROW_MAJOR			row_major
#define COLUMN_MAJOR		column_major
// 置き換え(GLSL互換用)
#define mtx22				float2x2
#define mtx23				float3x2
#define mtx24				float4x2
#define mtx32				float2x3
#define mtx33				float3x3
#define mtx34				float4x3
#define mtx42				float2x4
#define mtx43				float3x4
#define mtx44				float4x4

#elif defined( USE_GLSL )
// GLSL
#define ROW_MAJOR			row_major
#define COLUMN_MAJOR		column_major
#define mtx22				mat2x2
#define mtx23				mat2x3
#define mtx24				mat2x4
#define mtx32				mat3x2
#define mtx33				mat3x3
#define mtx34				mat3x4
#define mtx42				mat4x2
#define mtx43				mat4x3
#define mtx44				mat4x4

#endif


//-------------------------------------------------------------------------------------------------
// 頂点入力セマンティクス
#if defined( USE_HLSL )
// HLSL
#define VS_IN_POS( __type )				__type pos : POSITION
#define VS_IN_NORMAL( __type )			__type nrm : NORMAL
#define VS_IN_TANGENT( __type )			__type tan : TANGENT
#define VS_IN_BINORMAL( __type )		__type bin : BINORMAL
#define VS_IN_BLEND_WEIGHT0( __type )	__type bwgt0 : BLENDWEIGHT0
#define VS_IN_BLEND_WEIGHT1( __type )	__type bwgt1 : BLENDWEIGHT1
#define VS_IN_BLEND_INDICES0( __type )	__type bidx0 : BLENDINDICES0
#define VS_IN_BLEND_INDICES1( __type )	__type bidx1 : BLENDINDICES1
#define VS_IN_COLOR0( __type )			__type clr0 : COLOR0
#define VS_IN_COLOR1( __type )			__type clr1 : COLOR1
#define VS_IN_TEXCOORD0( __type )		__type uv0 : TEXCOORD0
#define VS_IN_TEXCOORD1( __type )		__type uv1 : TEXCOORD1
#define VS_IN_TEXCOORD2( __type )		__type uv2 : TEXCOORD2
#define VS_IN_TEXCOORD3( __type )		__type uv3 : TEXCOORD3
#define VS_IN_PSIZE( __type )			__type psize : PSIZE
#define VS_IN_VERTEX_ID( __type )		__type vtxID : SV_VertexID
#define VS_IN_INSTANCE_ID( __type )		__type instID : SV_InstanceID

#elif defined( USE_SPIRV ) || defined( USE_NX )
// SPIRV
#define VS_IN_POS( __type )				[[vk::location(0)]] __type pos : POSITION
#define VS_IN_NORMAL( __type )			[[vk::location(1)]] __type nrm : NORMAL
#define VS_IN_TANGENT( __type )			[[vk::location(2)]] __type tan : TANGENT
#define VS_IN_BINORMAL( __type )		[[vk::location(3)]] __type bin : BINORMAL
#define VS_IN_BLEND_WEIGHT0( __type )	[[vk::location(4)]] __type bwgt0 : BLENDWEIGHT0
#define VS_IN_BLEND_WEIGHT1( __type )	[[vk::location(5)]] __type bwgt1 : BLENDWEIGHT1
#define VS_IN_BLEND_INDICES0( __type )	[[vk::location(6)]] __type bidx0 : BLENDINDICES0
#define VS_IN_BLEND_INDICES1( __type )	[[vk::location(7)]] __type bidx1 : BLENDINDICES1
#define VS_IN_COLOR0( __type )			[[vk::location(8)]] __type clr0 : COLOR0
#define VS_IN_COLOR1( __type )			[[vk::location(9)]] __type clr1 : COLOR1
#define VS_IN_TEXCOORD0( __type )		[[vk::location(10)]] __type uv0 : TEXCOORD0
#define VS_IN_TEXCOORD1( __type )		[[vk::location(11)]] __type uv1 : TEXCOORD1
#define VS_IN_TEXCOORD2( __type )		[[vk::location(12)]] __type uv2 : TEXCOORD2
#define VS_IN_TEXCOORD3( __type )		[[vk::location(13)]] __type uv3 : TEXCOORD3
#define VS_IN_PSIZE( __type )			__type psize : PSIZE
#define VS_IN_VERTEX_ID( __type )		__type vtxID : SV_VertexID
#define VS_IN_INSTANCE_ID( __type )		__type instID : SV_InstanceID

#elif defined( USE_PSSL )
// PS4
#define VS_IN_POS( __type )				__type pos : POSITION
#define VS_IN_NORMAL( __type )			__type nrm : NORMAL
#define VS_IN_TANGENT( __type )			__type tan : TANGENT
#define VS_IN_BINORMAL( __type )		__type bin : BINORMAL
#define VS_IN_BLEND_WEIGHT0( __type )	__type bwgt0 : BLENDWEIGHT0
#define VS_IN_BLEND_WEIGHT1( __type )	__type bwgt1 : BLENDWEIGHT1
#define VS_IN_BLEND_INDICES0( __type )	__type bidx0 : BLENDINDICES0
#define VS_IN_BLEND_INDICES1( __type )	__type bidx1 : BLENDINDICES1
#define VS_IN_COLOR0( __type )			__type clr0 : COLOR0
#define VS_IN_COLOR1( __type )			__type clr1 : COLOR1
#define VS_IN_TEXCOORD0( __type )		__type uv0 : TEXCOORD0
#define VS_IN_TEXCOORD1( __type )		__type uv1 : TEXCOORD1
#define VS_IN_TEXCOORD2( __type )		__type uv2 : TEXCOORD2
#define VS_IN_TEXCOORD3( __type )		__type uv3 : TEXCOORD3
#define VS_IN_PSIZE( __type )			__type psize : PSIZE
#define VS_IN_VERTEX_ID( __type )		__type vtxID : S_VERTEX_ID
#define VS_IN_INSTANCE_ID( __type )		__type instID : S_INSTANCE_ID

#elif defined( USE_GLSL )
// GLSL
#define VS_IN_POS( __type )				layout(location = 0) in __type pos
#define VS_IN_NORMAL( __type )			layout(location = 1) in __type nrm
#define VS_IN_TANGENT( __type )			layout(location = 2) in __type tan
#define VS_IN_BINORMAL( __type )		layout(location = 3) in __type bin
#define VS_IN_BLEND_WEIGHT0( __type )	layout(location = 4) in __type bwgt0
#define VS_IN_BLEND_WEIGHT0( __type )	layout(location = 5) in __type bwgt1
#define VS_IN_BLEND_INDICES1( __type )	layout(location = 6) in __type bidx0
#define VS_IN_BLEND_INDICES1( __type )	layout(location = 7) in __type bidx0
#define VS_IN_COLOR0( __type )			layout(location = 8) in __type clr0
#define VS_IN_COLOR1( __type )			layout(location = 9) in __type clr1
#define VS_IN_TEXCOORD0( __type )		layout(location = 10) in __type uv0
#define VS_IN_TEXCOORD1( __type )		layout(location = 11) in __type uv1
#define VS_IN_TEXCOORD2( __type )		layout(location = 12) in __type uv2
#define VS_IN_TEXCOORD3( __type )		layout(location = 13) in __type uv3
#define VS_IN_PSIZE( __type )			in __type gl_PointSize		// 点描画サイズ(float)
#define	VS_IN_VERTEX_ID( __type )		in __type gl_VertexID		// インデックスID(int)
#define	VS_IN_INSTANCE_ID( __type )		in __type gl_InstanceID		// インスタンスID(int)

#endif

//--------------------------------------------------------------------------------------
// バーテックス出力セマンティクス
#if defined( USE_HLSL ) || defined( USE_SPIRV ) || defined( USE_NX )
// HLSL
#define	VS_OUT_POS( __type )			__type pos : SV_Position
#define	VS_OUT_FRONT_FACE( __type )		__type isFace : SV_IsFrontFace
//#define VS_OUT_PSIZE( __type )		__type psize : PSIZE
#define VS_OUT_CULL_LEN( __type, __n )	__type cullLen##__n : SV_CullDistance[__n]
#define VS_OUT_CLIP_LEN( __type, __n )	__type clipLen##__n : SV_ClipDistance[__n]

#elif defined( USE_PSSL )
// PS4
#define	VS_OUT_POS( __type )			__type pos : S_POSITION
#define	VS_OUT_FRONT_FACE( __type )		__type isFace : S_FRONT_FACE
//#define VS_OUT_PSIZE					__type psize : PSIZE
#define VS_OUT_CLIP_LEN( __type, __n )	__type clipLen##__n : SV_CLIP_DISTANCE[__n]
#define VS_OUT_CULL_LEN( __type, __n )	__type cullLen##__n : SV_CULL_DISTANCE[__n]

#elif defined( USE_GLSL )
// GLSL
#define	VS_OUT_POS( __type )			__type gl_Position			// 出力頂点座標(float4)
#define	VS_OUT_FRONT_FACE( __type )		__type gl_FrontFacing		// ラスタライザの表裏判定を指定(bool)
#define VS_OUT_PSIZE( __type )			__type gl_PointSize			// 点・線のサイズを指定(float)
#define VS_OUT_CLIP_LEN( __type, __n )	__type gl_ClipDistance[__n]	// クリップ距離(float)
#define VS_OUT_CULL_LEN( __type, __n )	__type gl_CullDistance[__n]	// カリング距離(float)

#endif

//--------------------------------------------------------------------------------------
// ジオメトリ入力セマンティクス
#if defined( USE_HLSL ) || defined( USE_SPIRV ) || defined( USE_NX )
#define	GS_IN_POS( __type )				__type pos : SV_Position

#elif defined( USE_PSSL )
#define	GS_IN_POS( __type )				__type pos : S_POSITION

#elif defined( USE_GLSL )
#define	GS_IN_POS( __type )				__type gl_Position

#endif
//--------------------------------------------------------------------------------------
// ジオメトリ出力セマンティクス
#if defined( USE_HLSL ) || defined( USE_SPIRV ) || defined( USE_NX )
// HLSL
#define	GS_OUT_POS( __type )				__type pos : SV_Position
#define	GS_OUT_RENDER_TARGET_ID( __type )	__type rtID : SV_RenderTargetArrayIndex
#define	GS_OUT_VIEWPORT_ID( __type )		__type vpID : SV_ViewportArrayIndex

#elif defined( USE_PSSL )
// PS4
#define	GS_OUT_POS( __type )				__type pos : S_POSITION
#define	GS_OUT_RENDER_TARGET_ID( __type )
#define	GS_OUT_VIEWPORT_ID( __type )

#elif defined( USE_GLSL )
// GLSL
#define	GS_OUT_POS( __type )				__type gl_Position
#define	GS_OUT_RENDER_TARGET_ID( __type )	__type gl_Layer
#define	GS_OUT_VIEWPORT_ID( __type )		__type gl_ViewportIndex

#endif

//--------------------------------------------------------------------------------------
// ピクセル入力セマンティクス
#if defined( USE_HLSL ) || defined( USE_SPIRV ) || defined( USE_NX )
// HLSL
#define	PS_IN_POS( __type )				__type pos : SV_Position
#define PS_IN_PRIMITIVE_ID( __type )	__type primID : SV_PrimitiveID
//#define PS_IN_PTEXCOORD( __type )

#elif defined( USE_PSSL )
// PS4
#define	PS_IN_POS( __type )				__type pos : S_POSITION
#define PS_IN_PRIMITIVE_ID( __type )	__type primID : S_PRIMITIVE_ID
//#define PS_IN_PTEXCOORD( __type )

#elif defined( USE_GLSL )
// GLSL
#define PS_IN_POS( __type )				__type gl_FragCoord		// 入力頂点座標(float4)
#define PS_IN_PRIMITIVE_ID( __type )	__type gl_PrimitiveID	// 入力プリミティブID(int)
#define PS_IN_PTEXCOORD( __type )		__type gl_PointCoord	// 点・線のUV座標(float2)

#endif

//--------------------------------------------------------------------------------------
// ピクセル出力セマンティクス
#if defined( USE_HLSL ) || defined( USE_SPIRV ) || defined( USE_NX )
// HLSL
#define	PS_OUT_COLOR0( __type )			__type clr0 : SV_Target0
#define	PS_OUT_COLOR1( __type )			__type clr1 : SV_Target1
#define	PS_OUT_COLOR2( __type )			__type clr2 : SV_Target2
#define	PS_OUT_COLOR3( __type )			__type clr3 : SV_Target3
#define	PS_OUT_COLOR4( __type )			__type clr4 : SV_Target4
#define	PS_OUT_COLOR5( __type )			__type clr5 : SV_Target5
#define	PS_OUT_COLOR6( __type )			__type clr6 : SV_Target6
#define	PS_OUT_COLOR7( __type )			__type clr7 : SV_Target7
#define	PS_OUT_DEPTH( __type )			__type dep : SV_Depth

#elif defined( USE_PSSL )
// PS4 OUT_COLOR
#define	PS_OUT_COLOR0( __type )			__type clr0 : S_TARGET_OUTPUT0
#define	PS_OUT_COLOR1( __type )			__type clr1 : S_TARGET_OUTPUT1
#define	PS_OUT_COLOR2( __type )			__type clr2 : S_TARGET_OUTPUT2
#define	PS_OUT_COLOR3( __type )			__type clr3 : S_TARGET_OUTPUT3
#define	PS_OUT_COLOR4( __type )			__type clr4 : S_TARGET_OUTPUT4
#define	PS_OUT_COLOR5( __type )			__type clr5 : S_TARGET_OUTPUT5
#define	PS_OUT_COLOR6( __type )			__type clr6 : S_TARGET_OUTPUT6
#define	PS_OUT_COLOR7( __type )			__type clr7 : S_TARGET_OUTPUT7
#define	PS_OUT_DEPTH( __type )			__type dep : S_DEPTH_OUTPUT

#elif defined( USE_GLSL )
// GLSL
//#define	PS_OUT_COLOR0( __type )			out __type gl_FragData[0]
//#define	PS_OUT_COLOR1( __type )			out __type gl_FragData[1]
//#define	PS_OUT_COLOR2( __type )			out __type gl_FragData[2]
//#define	PS_OUT_COLOR3( __type )			out __type gl_FragData[3]
//#define	PS_OUT_COLOR4( __type )			out __type gl_FragData[4]
//#define	PS_OUT_COLOR5( __type )			out __type gl_FragData[5]
//#define	PS_OUT_COLOR6( __type )			out __type gl_FragData[6]
//#define	PS_OUT_COLOR7( __type )			out __type gl_FragData[7]
#define	PS_OUT_COLOR0( __type )			layout(location = 0) out __type clr0
#define	PS_OUT_COLOR1( __type )			layout(location = 1) out __type clr1
#define	PS_OUT_COLOR2( __type )			layout(location = 2) out __type clr2
#define	PS_OUT_COLOR3( __type )			layout(location = 3) out __type clr3
#define	PS_OUT_COLOR4( __type )			layout(location = 4) out __type clr4
#define	PS_OUT_COLOR5( __type )			layout(location = 5) out __type clr5
#define	PS_OUT_COLOR6( __type )			layout(location = 6) out __type clr6
#define	PS_OUT_COLOR7( __type )			layout(location = 7) out __type clr7
#define	PS_OUT_DEPTH( __type )			out __type gl_FragDepth

#endif

//--------------------------------------------------------------------------------------
// 接続セマンティクス
#if defined( USE_HLSL ) || defined( USE_PSSL )
// HLSL
#define	PASS_PARAM0( __type, __name )		__type __name : TEXCOORD0
#define	PASS_PARAM1( __type, __name )		__type __name : TEXCOORD1
#define	PASS_PARAM2( __type, __name )		__type __name : TEXCOORD2
#define	PASS_PARAM3( __type, __name )		__type __name : TEXCOORD3
#define	PASS_PARAM4( __type, __name )		__type __name : TEXCOORD4
#define	PASS_PARAM5( __type, __name )		__type __name : TEXCOORD5
#define	PASS_PARAM6( __type, __name )		__type __name : TEXCOORD6
#define	PASS_PARAM7( __type, __name )		__type __name : TEXCOORD7
#define	PASS_PARAM8( __type, __name )		__type __name : TEXCOORD8
#define	PASS_PARAM9( __type, __name )		__type __name : TEXCOORD9
#define	PASS_PARAM10( __type, __name )		__type __name : TEXCOORD10
#define	PASS_PARAM11( __type, __name )		__type __name : TEXCOORD11
#define	PASS_PARAM12( __type, __name )		__type __name : TEXCOORD12
#define	PASS_PARAM13( __type, __name )		__type __name : TEXCOORD13
#define	PASS_PARAM14( __type, __name )		__type __name : TEXCOORD14
#define	PASS_PARAM15( __type, __name )		__type __name : TEXCOORD15

#elif defined( USE_SPIRV ) || defined( USE_NX )
// spirv
#define	PASS_PARAM0( __type, __name )		[[vk::location(0)]] __type __name : TEXCOORD0
#define	PASS_PARAM1( __type, __name )		[[vk::location(1)]] __type __name : TEXCOORD1
#define	PASS_PARAM2( __type, __name )		[[vk::location(2)]] __type __name : TEXCOORD2
#define	PASS_PARAM3( __type, __name )		[[vk::location(3)]] __type __name : TEXCOORD3
#define	PASS_PARAM4( __type, __name )		[[vk::location(4)]] __type __name : TEXCOORD4
#define	PASS_PARAM5( __type, __name )		[[vk::location(5)]] __type __name : TEXCOORD5
#define	PASS_PARAM6( __type, __name )		[[vk::location(6)]] __type __name : TEXCOORD6
#define	PASS_PARAM7( __type, __name )		[[vk::location(7)]] __type __name : TEXCOORD7
#define	PASS_PARAM8( __type, __name )		[[vk::location(8)]] __type __name : TEXCOORD8
#define	PASS_PARAM9( __type, __name )		[[vk::location(9)]] __type __name : TEXCOORD9
#define	PASS_PARAM10( __type, __name )		[[vk::location(10)]] __type __name : TEXCOORD10
#define	PASS_PARAM11( __type, __name )		[[vk::location(11)]] __type __name : TEXCOORD11
#define	PASS_PARAM12( __type, __name )		[[vk::location(12)]] __type __name : TEXCOORD12
#define	PASS_PARAM13( __type, __name )		[[vk::location(13)]] __type __name : TEXCOORD13
#define	PASS_PARAM14( __type, __name )		[[vk::location(14)]] __type __name : TEXCOORD14
#define	PASS_PARAM15( __type, __name )		[[vk::location(15)]] __type __name : TEXCOORD15

#elif defined( USE_GLSL )
// GLSL
#define PASS_PARAM0( __type, __name )		layout(location = 0) out __type __name
#define PASS_PARAM1( __type, __name )		layout(location = 1) out __type __name
#define PASS_PARAM2( __type, __name )		layout(location = 2) out __type __name
#define PASS_PARAM3( __type, __name )		layout(location = 3) out __type __name
#define PASS_PARAM4( __type, __name )		layout(location = 4) out __type __name
#define PASS_PARAM5( __type, __name )		layout(location = 5) out __type __name
#define PASS_PARAM6( __type, __name )		layout(location = 6) out __type __name
#define PASS_PARAM7( __type, __name )		layout(location = 7) out __type __name
#define PASS_PARAM8( __type, __name )		layout(location = 8) out __type __name
#define PASS_PARAM9( __type, __name )		layout(location = 9) out __type __name
#define PASS_PARAM10( __type, __name )		layout(location = 10) out __type __name
#define PASS_PARAM11( __type, __name )		layout(location = 11) out __type __name
#define PASS_PARAM12( __type, __name )		layout(location = 12) out __type __name
#define PASS_PARAM13( __type, __name )		layout(location = 13) out __type __name
#define PASS_PARAM14( __type, __name )		layout(location = 14) out __type __name
#define PASS_PARAM15( __type, __name )		layout(location = 15) out __type __name

#endif


//--------------------------------------------------------------------------------------
// ジオメトリ用アトリビュート
#if defined( USE_PSSL )
// PS4独自設定
#define	maxvertexcount	MAX_VERTEX_COUNT
#define point			Point
#define PointStream		PointBuffer
#define line			Line
#define LineStream		LineBuffer
#define triangle		Triangle
#define TriangleStream	TriangleBuffer
#define SampleLevel		SampleLOD

#endif

//--------------------------------------------------------------------------------------
// ピクセル用アトリビュート
#if defined( USE_HLSL ) || defined( USE_SPIRV ) || defined( USE_NX )
#define	FAST_DEPTH_TEST	[earlydepthstencil]

#elif defined( USE_PSSL )
#define	FAST_DEPTH_TEST	[RE_Z]

#elif defined( USE_GLSL )
#define	FAST_DEPTH_TEST	

#endif


//--------------------------------------------------------------------------------------
// コンピュートシェーダ向け
#if defined( USE_HLSL ) || defined( USE_SPIRV ) || defined( USE_NX )
// HLSL spirv
// コンピュートセマンティクス
#define GROUP_ID				SV_GroupID
#define DISPATCH_THREAD_ID		SV_DispatchThreadID
#define GROUP_THREAD_ID			SV_GroupThreadID
#define GROUP_INDEX				SV_GroupIndex
#define NUM_THREADS				numthreads
#define GROUP_SHARED			groupshared

// バリア関数(全スレッドのアクセス終了を待つ。Syncは同期も待つ)
#if 0
// 共有メモリ(groupshared)バリア
#define GroupMemoryBarrier					GroupMemoryBarrier
#define GroupMemoryBarrierWithGroupSync		GroupMemoryBarrierWithGroupSync
// メインメモリ(RWバッファ)バリア
#define DeviceMemoryBarrier					DeviceMemoryBarrier
#define DeviceMemoryBarrierWithGroupSync	DeviceMemoryBarrierWithGroupSync
// 全メモリバリア
#define AllMemoryBarrier					AllMemoryBarrier
#define AllMemoryBarrierWithGroupSync		AllMemoryBarrierWithGroupSync
// アトミック操作
#define InterlockedAdd						InterlockedAdd
#define InterlockedMin						InterlockedMin
#define InterlockedMax						InterlockedMax
#define InterlockedAnd						InterlockedAnd
#define InterlockedOr						InterlockedOr
#define InterlockedXor						InterlockedXor
#endif // 0

#elif defined( USE_PSSL )
// PS4
// コンピュートセマンティクス
#define GROUP_ID				S_GROUP_ID
#define DISPATCH_THREAD_ID		S_DISPATCH_THREAD_ID
#define GROUP_THREAD_ID			S_GROUP_THREAD_ID
#define GROUP_INDEX				S_GROUP_INDEX
//#define NUM_THREADS				NUM_THREADS
#define GROUP_SHARED			thread_group_memory

// バリア関数(全スレッドのアクセス終了を待つ。Syncは同期も待つ)
// 共有メモリ(groupshared)バリア
#define GroupMemoryBarrier					ThreadGroupMemoryBarrier
#define GroupMemoryBarrierWithGroupSync		ThreadGroupMemoryBarrierSync
// メインメモリ(RWバッファ)バリア
#define DeviceMemoryBarrier					SystemMemoryBarrier		//SharedMemoryBarrier
#define DeviceMemoryBarrierWithGroupSync	SystemMemoryBarrierSync	//SharedMemoryBarrierSync
// 全メモリバリア
#define AllMemoryBarrier					MemoryBarrier
#define AllMemoryBarrierWithGroupSync		MemoryBarrierSync
// アトミック操作
#define InterlockedAdd						AtomicAdd
#define InterlockedMin						AtomicMin
#define InterlockedMax						AtomicMax
#define InterlockedAnd						AtomicAnd
#define InterlockedOr						AtomicOr
#define InterlockedXor						AtomicXor

#elif defined( USE_GLSL )
// GLSL

#endif



//--------------------------------------------------------------------------------------
// レジスタ定義
//register(c0)	// バッファーオフセット
//register(b0)	// 定数バッファcbuffer
//register(t0)	// テクスチャtbuffer
//register(s0)	// サンプラー
//register(u0)	// アンオーダードアクセスビュー
#if defined( USE_SPIRV ) || defined (USE_NX)
#define VK_C_DESC_SET	0
#define VK_B_DESC_SET	0
#define VK_T_DESC_SET	0
#define VK_S_DESC_SET	0
#define VK_U_DESC_SET	0
#define VK_C_SPACE		space0
#define VK_B_SPACE		space1
#define VK_T_SPACE		space2
#define VK_S_SPACE		space3
#define VK_U_SPACE		space4

#elif defined( USE_GLSL )
#define GL_C_REGISTER	0
#define GL_B_REGISTER	1
#define GL_T_REGISTER	2
#define GL_S_REGISTER	3
#define GL_U_REGISTER	4

#endif


//--------------------------------------------------------------------------------------
// コンスタンスバッファ定義
#if defined (USE_HLSL)
// ユニフォーム インデックスで指定(使わなくなる予定)
#define	UNIFORM( __type, __val, __idx )		__type __val : register( c##__idx )

// ユニフォームブロック指定 しがらみ無しの純粋な指定
#define UNIFORM_BLOCK( __idx )		cbuffer	CB##__idx : register( b##__idx )
#define UNIFORM_BLOCK_DEF( __def )	cbuffer	CB##__def : register( __def )


#elif defined( USE_SPIRV ) || defined (USE_NX)
// ユニフォーム インデックスで指定(使わなくなる予定)
#define	UNIFORM( __type, __val, __idx )		[[vk::binding(__idx, VK_C_DESC_SET)]] __type __val : register( c##__idx, VK_C_SPACE )

// ユニフォームブロック指定 しがらみ無しの純粋な指定
#define UNIFORM_BLOCK( __idx )		[[vk::binding(__idx, VK_B_DESC_SET)]] cbuffer	CB##__idx : register( b##__idx, VK_B_SPACE )
#define UNIFORM_BLOCK_DEF( __def )	[[vk::binding(__def, VK_B_DESC_SET)]] cbuffer	CB##__def : register( __def, VK_B_SPACE )


#elif defined( USE_PSSL )
// ユニフォーム インデックスで指定(使わなくなる予定)
#define	UNIFORM( __type, __val, __idx )		__type __val : register( c##__idx )

// ユニフォームブロック指定 しがらみ無しの純粋な指定
#define UNIFORM_BLOCK( __idx )		ConstantBuffer	CB##__idx : register( b##__idx )
#define UNIFORM_BLOCK_DEF( __def )	ConstantBuffer	CB##__def : register( __def )


#elif defined( USE_GLSL )
// GLSL
// ユニフォーム インデックスで指定(使わなくなる予定)
#define	UNIFORM( __type, __val, __idx )		layout(binding = __idx) uniform	__type __val

// ユニフォームブロック指定 しがらみ無しの純粋な指定
#define UNIFORM_BLOCK( __idx )		layout(std140, binding = __idx) uniform	UNIFORM_SLOT##__idx
#define UNIFORM_BLOCK_DEF( __def )	layout(std140) uniform	UNIFORM_SLOT##__def


#endif


//--------------------------------------------------------------------------------------
// テクスチャユニット宣言
#if defined (USE_HLSL)
#define	IN_TEX_1D_UNIT( __val, __idx )			Texture1D			__val : register( t##__idx )
#define	IN_TEX_1D_ARRAY_UNIT( __val, __idx )	Texture1DArray		__val : register( t##__idx )
#define	IN_TEX_2D_UNIT( __val, __idx )			Texture2D			__val : register( t##__idx )
#define	IN_TEX_2D_ARRAY_UNIT( __val, __idx )	Texture2DArray		__val : register( t##__idx )
#define	IN_TEX_3D_UNIT( __val, __idx )			Texture3D			__val : register( t##__idx )
#define IN_TEX_CUBE_UNIT( __val, __idx )		TextureCube			__val : register( t##__idx )
#define IN_TEX_CUBE_ARRAY_UNIT( __val, __idx )	TextureCubeArray	__val : register( t##__idx )


#elif defined( USE_SPIRV ) || defined (USE_NX)
#define	IN_TEX_1D_UNIT( __val, __idx )			[[vk::binding(__idx, VK_T_DESC_SET)]] Texture1D			__val : register( t##__idx, VK_T_SPACE )
#define	IN_TEX_1D_ARRAY_UNIT( __val, __idx )	[[vk::binding(__idx, VK_T_DESC_SET)]] Texture1DArray	__val : register( t##__idx, VK_T_SPACE )
#define	IN_TEX_2D_UNIT( __val, __idx )			[[vk::binding(__idx, VK_T_DESC_SET)]] Texture2D			__val : register( t##__idx, VK_T_SPACE )
#define	IN_TEX_2D_ARRAY_UNIT( __val, __idx )	[[vk::binding(__idx, VK_T_DESC_SET)]] Texture2DArray	__val : register( t##__idx, VK_T_SPACE )
#define	IN_TEX_3D_UNIT( __val, __idx )			[[vk::binding(__idx, VK_T_DESC_SET)]] Texture3D			__val : register( t##__idx, VK_T_SPACE )
#define IN_TEX_CUBE_UNIT( __val, __idx )		[[vk::binding(__idx, VK_T_DESC_SET)]] TextureCube		__val : register( t##__idx, VK_T_SPACE )
#define IN_TEX_CUBE_ARRAY_UNIT( __val, __idx )	[[vk::binding(__idx, VK_T_DESC_SET)]] TextureCubeArray	__val : register( t##__idx, VK_T_SPACE )


#elif defined( USE_PSSL )
#define	IN_TEX_1D_UNIT( __val, __idx )			Texture1D			__val : register( t##__idx )
#define	IN_TEX_1D_ARRAY_UNIT( __val, __idx )	Texture1D_Array		__val : register( t##__idx )
#define	IN_TEX_2D_UNIT( __val, __idx )			Texture2D			__val : register( t##__idx )
#define	IN_TEX_2D_ARRAY_UNIT( __val, __idx )	Texture2D_Array		__val : register( t##__idx )
#define IN_TEX_CUBE_UNIT( __val, __idx )		TextureCube			__val : register( t##__idx )
#define IN_TEX_CUBE_ARRAY_UNIT( __val, __idx )	TextureCubeArray	__val : register( t##__idx )
#define	IN_TEX_3D_UNIT( __val, __idx )			TextureCube_Array	__val : register( t##__idx )


#elif defined( USE_GLSL )
#define IN_TEX_1D_UNIT( __val, __idx )			layout(binding = __idx, set = GL_T_REGISTER) uniform texture1D __val
#define IN_TEX_1D_ARRAY_UNIT( __val, __idx )	layout(binding = __idx, set = GL_T_REGISTER) uniform texture1DArray __val
#define IN_TEX_2D_UNIT( __val, __idx )			layout(binding = __idx, set = GL_T_REGISTER) uniform texture2D __val
#define IN_TEX_2D_ARRAY_UNIT( __val, __idx )	layout(binding = __idx, set = GL_T_REGISTER) uniform texture2DArray __val
#define IN_TEX_3D_UNIT( __val, __idx )			layout(binding = __idx, set = GL_T_REGISTER) uniform texture3D __val
#define IN_TEX_CUBE_UNIT( __val, __idx )		layout(binding = __idx, set = GL_T_REGISTER) uniform textureCube __val
#define IN_TEX_CUBE_ARRAY_UNIT( __val, __idx )	layout(binding = __idx, set = GL_T_REGISTER) uniform textureCubeArray __val


#endif


//--------------------------------------------------------------------------------------
// サンプラーユニット宣言
#if defined (USE_HLSL) || defined( USE_PSSL )
#define	IN_SAMPLER_UNIT( __val, __idx )			SamplerState			__val : register( s##__idx )
#define	IN_SAMPLER_CMP_UNIT( __val, __idx )		SamplerComparisonState	__val : register( s##__idx )


#elif defined( USE_SPIRV ) || defined (USE_NX)
#define	IN_SAMPLER_UNIT( __val, __idx )			[[vk::binding(__idx, VK_S_DESC_SET)]] SamplerState				__val : register( s##__idx, VK_S_SPACE )
#define	IN_SAMPLER_CMP_UNIT( __val, __idx )		[[vk::binding(__idx, VK_S_DESC_SET)]] SamplerComparisonState	__val : register( s##__idx, VK_S_SPACE )


#elif defined( USE_GLSL )
#define IN_SAMPLER_UNIT( __val, __idx )			layout(binding = __idx, set = GL_S_REGISTER) uniform sampler __val
#define IN_SAMPLER_CMP_UNIT( __val, __idx )		layout(binding = __idx, set = GL_S_REGISTER) uniform samplerShadow __val


#endif


//--------------------------------------------------------------------------------------
// テクスチャサンプラー宣言
#if defined (USE_HLSL) || defined( USE_SPIRV ) || defined (USE_NX) || defined( USE_PSSL )
// サンプラー取得
#define GetTexSampler( __val )	__val##Sampler
#define	IN_TEX1D( __val, __idx )				IN_TEX_1D_UNIT(__val, __idx);			IN_SAMPLER_UNIT(GetTexSampler( __val ), __idx)
#define	IN_TEX1D_CMP( __val, __idx )			IN_TEX_1D_UNIT(__val, __idx);			IN_SAMPLER_CMP_UNIT(GetTexSampler( __val ), __idx)
#define	IN_TEX1D_ARRAY( __val, __idx )			IN_TEX_1D_ARRAY_UNIT(__val, __idx);		IN_SAMPLER_UNIT(GetTexSampler( __val ), __idx)
#define	IN_TEX1D_ARRAY_CMP( __val, __idx )		IN_TEX_1D_ARRAY_UNIT(__val, __idx);		IN_SAMPLER_CMP_UNIT(GetTexSampler( __val ), __idx)
#define	IN_TEX2D( __val, __idx )				IN_TEX_2D_UNIT(__val, __idx);			IN_SAMPLER_UNIT(GetTexSampler( __val ), __idx)
#define	IN_TEX2D_CMP( __val, __idx )			IN_TEX_2D_UNIT(__val, __idx);			IN_SAMPLER_CMP_UNIT(GetTexSampler( __val ), __idx)
#define	IN_TEX2D_ARRAY( __val, __idx )			IN_TEX_2D_ARRAY_UNIT(__val, __idx);		IN_SAMPLER_UNIT(GetTexSampler( __val ), __idx)
#define	IN_TEX2D_ARRAY_CMP( __val, __idx )		IN_TEX_2D_ARRAY_UNIT(__val, __idx);		IN_SAMPLER_CMP_UNIT(GetTexSampler( __val ), __idx)
#define IN_TEX3D( __val, __idx )				IN_TEX_3D_UNIT(__val, __idx);			IN_SAMPLER_UNIT(GetTexSampler( __val ), __idx)
#define	IN_TEX3D_CMP( __val, __idx )			IN_TEX_3D_UNIT(__val, __idx);			IN_SAMPLER_CMP_UNIT(GetTexSampler( __val ), __idx)
#define IN_TEXCUBE( __val, __idx )				IN_TEX_CUBE_UNIT(__val, __idx);			IN_SAMPLER_UNIT(GetTexSampler( __val ), __idx)
#define	IN_TEXCUBE_ARRAY( __val, __idx )		IN_TEX_CUBE_ARRAY_UNIT(__val, __idx);	IN_SAMPLER_UNIT(GetTexSampler( __val ), __idx)


#elif defined( USE_GLSL )
#define IN_TEX1D( __val, __idx )			layout(binding = __idx, set = GL_T_REGISTER) uniform sampler1D __val
#define IN_TEX1D_CMP( __val, __idx )		layout(binding = __idx, set = GL_T_REGISTER) uniform sampler1DShadow __val
#define IN_TEX1D_ARRAY( __val, __idx )		layout(binding = __idx, set = GL_T_REGISTER) uniform sampler1DArray __val
#define IN_TEX1D_ARRAY_CMP( __val, __idx )	layout(binding = __idx, set = GL_T_REGISTER) uniform sampler1DArrayShadow __val
#define IN_TEX2D( __val, __idx )			layout(binding = __idx, set = GL_T_REGISTER) uniform sampler2D __val
#define IN_TEX2D_CMP( __val, __idx )		layout(binding = __idx, set = GL_T_REGISTER) uniform sampler2DShadow __val
#define IN_TEX2D_ARRAY( __val, __idx )		layout(binding = __idx, set = GL_T_REGISTER) uniform sampler2DArray __val
#define IN_TEX2D_ARRAY_CMP( __val, __idx )	layout(binding = __idx, set = GL_T_REGISTER) uniform sampler2DArrayShadow __val
#define IN_TEX3D( __val, __idx )			layout(binding = __idx, set = GL_T_REGISTER) uniform sampler3D __val
#define IN_TEX3D_CMP( __val, __idx )		layout(binding = __idx, set = GL_T_REGISTER) uniform sampler3DShadow __val
#define IN_TEXCUBE( __val, __idx )			layout(binding = __idx, set = GL_T_REGISTER) uniform samplerCube __val
#define IN_TEXCUBE_ARRAY( __val, __idx )	layout(binding = __idx, set = GL_T_REGISTER) uniform samplerCubeArray __val


#endif


//--------------------------------------------------------------------------------------
// テクスチャサンプリング
#if defined (USE_HLSL) || defined (USE_NX) || defined( USE_PSSL )
// テクスチャサンプル
#define TexSample( __tex, __sampler, __uv )								__tex.Sample( __sampler, __uv )
#define TexSampleOffset( __tex, __sampler, __uv, __offset )				__tex.Sample( __sampler, __uv, __offset )
// テクスチャサンプルLevel
#define TexSampleLevel( __tex, __sampler, __uv, __lv )					__tex.SampleLevel( __sampler, __uv, __lv )
#define TexSampleLevelOffset( __tex, __sampler, __uv, __lv, __offset )	__tex.SampleLevel( __sampler, __uv, __lv, __offset )
// テクスチャサンプルギャザ
#define	TexSampleGather( __tex, __sampler, __uv )						__tex.Gather( __sampler, __uv )
// テクスチャサンプルコンペア
#define TexSampleCmp( __tex, __sampler, __uv, __z )						__tex.SampleCmp( __sampler, __uv, __z )

// テクスチャ取得
#define TexFatch( __tex, __uv )								TexSample( __tex, GetTexSampler( __tex ), __uv )
// LODテクスチャ取得
#define TexFatchLod( __tex, __uv, __lv )					TexSampleLevel( __tex, GetTexSampler( __tex ), __uv, __lv )
#define TexFatchLodOffset( __tex, __uv, __lv, __offset )	TexSampleLevel( __tex, GetTexSampler( __tex ), __uv, __lv, __offset )
// LOD0テクスチャ取得
#define TexFatchLodZero( __tex, __uv )						TexFatchLod( __tex, __uv, 0 )

// テクスチャサンプルギャザ
#define	TexFatchGather( __tex, __uv )						TexSampleGather( __tex, GetTexSampler( __tex ), __uv.xy )

// テクスチャサンプリングコンペア
#define	TexFatchCmp( __tex, __uv )							TexSampleCmp( __tex, GetTexSampler( __tex ), __uv.xy, __uv.z )
#define	TexFatchArrayCmp( __tex, __uv )						TexSampleCmp( __tex, GetTexSampler( __tex ), __uv.xyz, __uv.w )


#elif defined( USE_SPIRV )
// テクスチャサンプル
float4	TexSample(in Texture1D __tex, in SamplerState __sampler, float __uv)
{
	return __tex.Sample(__sampler, __uv);
}
float4	TexSample(in Texture1DArray __tex, in SamplerState __sampler, float2 __uv)
{
	return __tex.Sample(__sampler, __uv);
}
float4	TexSample(in Texture2D __tex, in SamplerState __sampler, float2 __uv)
{
	return __tex.Sample(__sampler, float2(__uv.x, 1.0 - __uv.y));
}
float4	TexSample(in Texture2DArray __tex, in SamplerState __sampler, float3 __uv)
{
	return __tex.Sample(__sampler, float3(__uv.x, 1.0 - __uv.y, __uv.z));
}
float4	TexSample(in Texture3D __tex, in SamplerState __sampler, float3 __uv)
{
	return __tex.Sample(__sampler, float3(__uv.x, 1.0 - __uv.y, __uv.z));
}
float4	TexSample(in TextureCube __tex, in SamplerState __sampler, float3 __uv)
{
	return __tex.Sample(__sampler, __uv);
}
float4	TexSample(in TextureCubeArray __tex, in SamplerState __sampler, float4 __uv)
{
	return __tex.Sample(__sampler, __uv);
}

float4	TexSampleOffset(in Texture1D __tex, in SamplerState __sampler, float __uv, int __offset)
{
	return __tex.Sample(__sampler, __uv, __offset);
}
float4	TexSampleOffset(in Texture1DArray __tex, in SamplerState __sampler, float2 __uv, int __offset)
{
	return __tex.Sample(__sampler, __uv, __offset);
}
float4	TexSampleOffset(in Texture2D __tex, in SamplerState __sampler, float2 __uv, int2 __offset)
{
	return __tex.Sample(__sampler, float2(__uv.x, 1.0 - __uv.y), __offset);
}
float4	TexSampleOffset(in Texture2DArray __tex, in SamplerState __sampler, float3 __uv, int2 __offset)
{
	return __tex.Sample(__sampler, float3(__uv.x, 1.0 - __uv.y, __uv.z), __offset);
}
float4	TexSampleOffset(in Texture3D __tex, in SamplerState __sampler, float3 __uv, int3 __offset)
{
	return __tex.Sample(__sampler, float3(__uv.x, 1.0 - __uv.y, __uv.z), __offset);
}
float4	TexSampleOffset(in TextureCube __tex, in SamplerState __sampler, float3 __uv, int __offset)
{
	return __tex.Sample(__sampler, __uv);
}
float4	TexSampleOffset(in TextureCubeArray __tex, in SamplerState __sampler, float4 __uv, int __offset)
{
	return __tex.Sample(__sampler, __uv);
}

// テクスチャサンプルLevel
float4	TexSampleLevel(in Texture1D __tex, in SamplerState __sampler, float __uv, int __lv)
{
	return __tex.SampleLevel(__sampler, __uv, __lv);
}
float4	TexSampleLevel(in Texture1DArray __tex, in SamplerState __sampler, float2 __uv, int __lv)
{
	return __tex.SampleLevel(__sampler, __uv, __lv);
}
float4	TexSampleLevel(in Texture2D __tex, in SamplerState __sampler, float2 __uv, int __lv)
{
	return __tex.SampleLevel(__sampler, float2(__uv.x, 1.0 - __uv.y), __lv);
}
float4	TexSampleLevel(in Texture2DArray __tex, in SamplerState __sampler, float3 __uv, int __lv)
{
	return __tex.SampleLevel(__sampler, float3(__uv.x, 1.0 - __uv.y, __uv.z), __lv);
}
float4	TexSampleLevel(in Texture3D __tex, in SamplerState __sampler, float3 __uv, int __lv)
{
	return __tex.SampleLevel(__sampler, float3(__uv.x, 1.0 - __uv.y, __uv.z), __lv);
}
float4	TexSampleLevel(in TextureCube __tex, in SamplerState __sampler, float3 __uv, int __lv)
{
	return __tex.SampleLevel(__sampler, __uv, __lv);
}
float4	TexSampleLevel(in TextureCubeArray __tex, in SamplerState __sampler, float4 __uv, int __lv)
{
	return __tex.SampleLevel(__sampler, __uv, __lv);
}

float4	TexSampleLevelOffset(in Texture1D __tex, in SamplerState __sampler, float __uv, int __lv, int __offset)
{
	return __tex.SampleLevel(__sampler, __uv, __lv, __offset);
}
float4	TexSampleLevelOffset(in Texture1DArray __tex, in SamplerState __sampler, float2 __uv, int __lv, int __offset)
{
	return __tex.SampleLevel(__sampler, __uv, __lv, __offset);
}
float4	TexSampleLevelOffset(in Texture2D __tex, in SamplerState __sampler, float2 __uv, int __lv, int2 __offset)
{
	return __tex.SampleLevel(__sampler, float2(__uv.x, 1.0 - __uv.y), __lv, __offset);
}
float4	TexSampleLevelOffset(in Texture2DArray __tex, in SamplerState __sampler, float3 __uv, int __lv, int2 __offset)
{
	return __tex.SampleLevel(__sampler, float3(__uv.x, 1.0 - __uv.y, __uv.z), __lv, __offset);
}
float4	TexSampleLevelOffset(in Texture3D __tex, in SamplerState __sampler, float3 __uv, int __lv, int3 __offset)
{
	return __tex.SampleLevel(__sampler, float3(__uv.x, 1.0 - __uv.y, __uv.z), __lv, __offset);
}
float4	TexSampleLevelOffset(in TextureCube __tex, in SamplerState __sampler, float3 __uv, int __lv, int __offset)
{
	return __tex.SampleLevel(__sampler, __uv, __lv);
}
float4	TexSampleLevelOffset(in TextureCubeArray __tex, in SamplerState __sampler, float4 __uv, int __lv, int __offset)
{
	return __tex.SampleLevel(__sampler, __uv, __lv);
}

// テクスチャサンプルギャザ
float4	TexSampleGather(in Texture2D __tex, in SamplerState __sampler, float2 __uv)
{
	return __tex.Gather(__sampler, float2(__uv.x, 1.0 - __uv.y));
}
float4	TexSampleGather(in Texture2DArray __tex, in SamplerState __sampler, float3 __uv)
{
	return __tex.Gather(__sampler, float3(__uv.x, 1.0 - __uv.y, __uv.z));
}
float4	TexSampleGather(in TextureCube __tex, in SamplerState __sampler, float3 __uv)
{
	return __tex.Gather(__sampler, __uv);
}
float4	TexSampleGather(in TextureCubeArray __tex, in SamplerState __sampler, float4 __uv)
{
	return __tex.Gather(__sampler, __uv);
}

// テクスチャサンプルコンペア
float4	TexSampleCmp(in Texture1D __tex, in SamplerComparisonState __sampler, float __uv, float __z)
{
	return __tex.SampleCmp(__sampler, __uv, __z);
}
float4	TexSampleCmp(in Texture1DArray __tex, in SamplerComparisonState __sampler, float2 __uv, float __z)
{
	return __tex.SampleCmp(__sampler, __uv, __z);
}
float4	TexSampleCmp(in Texture2D __tex, in SamplerComparisonState __sampler, float2 __uv, float __z)
{
	return __tex.SampleCmp(__sampler, float2(__uv.x, 1.0 - __uv.y), __z);
}
float4	TexSampleCmp(in Texture2DArray __tex, in SamplerComparisonState __sampler, float3 __uv, float __z)
{
	return __tex.SampleCmp(__sampler, float3(__uv.x, 1.0 - __uv.y, __uv.z), __z);
}
float4	TexSampleCmp(in TextureCube __tex, in SamplerComparisonState __sampler, float3 __uv, float __z)
{
	return __tex.SampleCmp(__sampler, __uv, __z);
}
float4	TexSampleCmp(in TextureCubeArray __tex, in SamplerComparisonState __sampler, float4 __uv, float __z)
{
	return __tex.SampleCmp(__sampler, __uv, __z);
}

// テクスチャ取得
#define TexFatch( __tex, __uv )								TexSample( __tex, GetTexSampler( __tex ), __uv )
// LODテクスチャ取得
#define TexFatchLod( __tex, __uv, __lv )					TexSampleLevel( __tex, GetTexSampler( __tex ), __uv, __lv )
#define TexFatchLodOffset( __tex, __uv, __lv, __offset )	TexSampleLevel( __tex, GetTexSampler( __tex ), __uv, __lv, __offset )
// LOD0テクスチャ取得
#define TexFatchLodZero( __tex, __uv )						TexFatchLod( __tex, __uv, 0 )

// テクスチャサンプルギャザ
#define	TexFatchGather( __tex, __uv )						TexSampleGather( __tex, GetTexSampler( __tex ), __uv.xy )

// テクスチャサンプリングコンペア
#define	TexFatchCmp( __tex, __uv )							TexSampleCmp( __tex, GetTexSampler( __tex ), __uv.xy, __uv.z )
#define	TexFatchArrayCmp( __tex, __uv )						TexSampleCmp( __tex, GetTexSampler( __tex ), __uv.xyz, __uv.w )


#elif defined( USE_GLSL )
// テクスチャサンプル
#define TexSample( __tex, __sampler, __uv )								texture( sampler2D(__tex, __sampler), __uv )
#define TexSample( __tex, __sampler, __uv, __offset )					texture( sampler2D(__tex, __sampler), __uv, __offset )
// テクスチャサンプルLevel
#define TexSampleLevel( __tex, __sampler, __uv, __lv )					textureLod( sampler2D(__tex, __sampler), __uv, __lv )
#define TexSampleLevelOffset( __tex, __sampler, __uv, __lv, __offset )	textureLodOffset( sampler2D(__tex, __sampler), __uv, __lv, __offset )
// テクスチャサンプルギャザ
#define	TexSampleGather( __tex, __sampler, __uv )						textureGather( sampler2D(__tex, __sampler), __uv )
// テクスチャサンプルコンペア
#define TexSampleCmp( __tex, __sampler, __uv, __z )						shadow2DProj( sampler2DShadow(__tex, __sampler), __uv, __z )

// テクスチャ取得
#define TexFatch( __tex, __uv )								texture( __tex, __uv )
// LODテクスチャ取得
#define TexFatchLod( __tex, __uv, __lv )					textureLod( __tex, __uv, __lv )
#define TexFatchLodOffset( __tex, __uv, __lv, __offset )	textureLodOffset( __tex, __uv, __lv, __offset )
// LOD0テクスチャ取得
#define TexFatchLodZero( __tex, __uv )						TexFatchLod( __tex, __uv, 0 )

// テクスチャサンプルギャザ
#define	TexFatchGather( __tex, __uv )						textureGather( __tex, __uv )

// テクスチャサンプリングコンペア
#define	TexFatchCmp( __tex, __uv )							textureProj( __tex, __uv )
#define	TexFatchArrayCmp( __tex, __uv )						textureProj( __tex, __uv )


#endif


//-------------------------------------------------------------------------------------------------
// 読み書きレジスタ
#if defined (USE_HLSL)
#define R_BUFFER( __type, __name, __idx )			StructuredBuffer<__type>	__name : register( t##__idx )
#define	R_TEX1D( __type, __name, __idx )			Texture1D<__type>			__name : register( t##__idx )
#define	R_TEX2D( __type, __name, __idx )			Texture2D<__type>			__name : register( t##__idx )
#define	R_TEX3D( __type, __name, __idx )			Texture3D<__type>			__name : register( t##__idx )
#define BYTE_BUFFER( __name, __idx )				ByteAddressBuffer			__name : register( t##__idx )
#define RW_BUFFER( __type, __name, __idx )			RWBuffer<__type>			__name : register( u##__idx )
#define RW_STRUCT_BUFFER( __type, __name, __idx )	RWStructuredBuffer<__type>	__name : register( u##__idx )
#define RW_BYTE_BUFFER( __name, __idx )				RWByteAddressBuffer			__name : register( u##__idx )
#define RW_TEX1D( __type, __name, __idx )			RWTexture1D<__type>			__name : register( u##__idx )
#define RW_TEX1D_ARRAY( __type, __name, __idx )		RWTexture1DArray<__type>	__name : register( u##__idx )
#define RW_TEX2D( __type, __name, __idx )			RWTexture2D<__type>			__name : register( u##__idx )
#define RW_TEX2D_ARRAY( __type, __name, __idx )		RWTexture2DArray<__type>	__name : register( u##__idx )
#define RW_TEX3D( __type, __name, __idx )			RWTexture3D<__type>			__name : register( u##__idx )


#elif defined( USE_SPIRV ) || defined (USE_NX)
#define R_BUFFER( __type, __name, __idx )			[[vk::binding(__idx, VK_T_DESC_SET)]] StructuredBuffer<__type>		__name : register( t##__idx, VK_T_SPACE )
#define	R_TEX1D( __type, __name, __idx )			[[vk::binding(__idx, VK_T_DESC_SET)]] Texture1D<__type>				__name : register( t##__idx, VK_T_SPACE )
#define	R_TEX2D( __type, __name, __idx )			[[vk::binding(__idx, VK_T_DESC_SET)]] Texture2D<__type>				__name : register( t##__idx, VK_T_SPACE )
#define	R_TEX3D( __type, __name, __idx )			[[vk::binding(__idx, VK_T_DESC_SET)]] Texture3D<__type>				__name : register( t##__idx, VK_T_SPACE )
#define BYTE_BUFFER( __name, __idx )				[[vk::binding(__idx, VK_T_DESC_SET)]] ByteAddressBuffer				__name : register( t##__idx, VK_T_SPACE )
#define RW_BUFFER( __type, __name, __idx )			[[vk::binding(__idx, VK_U_DESC_SET)]] RWBuffer<__type>				__name : register( u##__idx, VK_U_SPACE )
#define RW_STRUCT_BUFFER( __type, __name, __idx )	[[vk::binding(__idx, VK_U_DESC_SET)]] RWStructuredBuffer<__type>	__name : register( u##__idx, VK_U_SPACE )
#define RW_BYTE_BUFFER( __name, __idx )				[[vk::binding(__idx, VK_U_DESC_SET)]] RWByteAddressBuffer			__name : register( u##__idx, VK_U_SPACE )
#define RW_TEX1D( __type, __name, __idx )			[[vk::binding(__idx, VK_U_DESC_SET)]] RWTexture1D<__type>			__name : register( u##__idx, VK_U_SPACE )
#define RW_TEX1D_ARRAY( __type, __name, __idx )		[[vk::binding(__idx, VK_U_DESC_SET)]] RWTexture1DArray<__type>		__name : register( u##__idx, VK_U_SPACE )
#define RW_TEX2D( __type, __name, __idx )			[[vk::binding(__idx, VK_U_DESC_SET)]] RWTexture2D<__type>			__name : register( u##__idx, VK_U_SPACE )
#define RW_TEX2D_ARRAY( __type, __name, __idx )		[[vk::binding(__idx, VK_U_DESC_SET)]] RWTexture2DArray<__type>		__name : register( u##__idx, VK_U_SPACE )
#define RW_TEX3D( __type, __name, __idx )			[[vk::binding(__idx, VK_U_DESC_SET)]] RWTexture3D<__type>			__name : register( u##__idx, VK_U_SPACE )


#elif defined( USE_PSSL )
#define R_BUFFER( __type, __name, __idx )			RegularBuffer<__type>		__name : register( t##__idx )
#define	R_TEX1D( __type, __name, __idx )			Texture1D<__type>			__name : register( t##__idx )
#define	R_TEX2D( __type, __name, __idx )			Texture2D<__type>			__name : register( t##__idx )
#define	R_TEX3D( __type, __name, __idx )			Texture3D<__type>			__name : register( t##__idx )
#define BYTE_BUFFER( __name, __idx )				ByteBuffer					__name : register( t##__idx )
#define RW_BUFFER( __type, __name, __idx )			RW_RegularBuffer<__type>	__name : register( u##__idx )
#define RW_STRUCT_BUFFER( __type, __name, __idx )	RW_RegularBuffer<__type>	__name : register( u##__idx )
#define RW_BYTE_BUFFER( __name, __idx )				RW_ByteBuffer				__name : register( u##__idx )
#define RW_TEX1D( __type, __name, __idx )			RW_Texture1D<__type>		__name : register( u##__idx )
#define RW_TEX1D_ARRAY( __type, __name, __idx )		RW_Texture1D_Array<__type>	__name : register( u##__idx )
#define RW_TEX2D( __type, __name, __idx )			RW_Texture2D<__type>		__name : register( u##__idx )
#define RW_TEX2D_ARRAY( __type, __name, __idx )		RW_Texture2D_Array<__type>	__name : register( u##__idx )
#define RW_TEX3D( __type, __name, __idx )			RW_Texture3D<__type>		__name : register( u##__idx )


#elif defined( USE_GLSL )
#define R_BUFFER( __type, __name, __idx )			layout(binding = __idx, set = GL_T_REGISTER, __type) uniform readonly image1D __name
#define R_TEX1D( __type, __name, __idx )			layout(binding = __idx, set = GL_T_REGISTER, __type) uniform readonly image1D __name
#define R_TEX2D( __type, __name, __idx )			layout(binding = __idx, set = GL_T_REGISTER, __type) uniform readonly image2D __name
#define R_TEX3D( __type, __name, __idx )			layout(binding = __idx, set = GL_T_REGISTER, __type) uniform readonly image3D __name
#define BYTE_BUFFER( __name, __idx )				layout(binding = __idx, set = GL_T_REGISTER) uniform readonly atomic_uint __name
#define RW_BUFFER( __type, __name, __idx )			layout(binding = __idx, set = GL_U_REGISTER, __type) uniform writeonly image1D __name
#define RW_STRUCT_BUFFER( __type, __name, __idx )	layout(binding = __idx, set = GL_U_REGISTER, __type) uniform writeonly image1D __name
#define RW_BYTE_BUFFER( __name, __idx )				layout(binding = __idx, set = GL_U_REGISTER) uniform writeonly atomic_uint __name
#define RW_TEX1D( __type, __name, __idx )			layout(binding = __idx, set = GL_U_REGISTER) uniform writeonly image1D __name
#define RW_TEX1D_ARRAY( __type, __name, __idx )		layout(binding = __idx, set = GL_U_REGISTER) uniform writeonly image1DArray __name
#define RW_TEX2D( __type, __name, __idx )			layout(binding = __idx, set = GL_U_REGISTER) uniform writeonly image2D __name
#define RW_TEX2D_ARRAY( __type, __name, __idx )		layout(binding = __idx, set = GL_U_REGISTER) uniform writeonly image2DArray __name
#define RW_TEX3D( __type, __name, __idx )			layout(binding = __idx, set = GL_U_REGISTER) uniform writeonly image3D __name


#endif


//-------------------------------------------------------------------------------------------------
//MEMO:	下記のタグを for(loop/unroll), if,switch(flatten/branch) につけることで最適化を行う、防ぐ、を指定できる
//		特に loop に関してはforを展開しないことで使用レジスタ数が下がるらしい(処理数増えるが)。詳細はdocを確認すること。
#if defined( USE_HLSL ) || defined( USE_SPIRV ) || defined( USE_NX ) || defined( USE_PSSL )
#define	HINT_UNROLL		[unroll]
#define	HINT_LOOP		[loop]
#define	HINT_FLATTEN	[flatten]
#define	HINT_BRANCH		[branch]

#elif defined( USE_GLSL )

#endif


//-------------------------------------------------------------------------------------------------
// ジオメトリ頂点カウント
#if defined( USE_HLSL ) || defined( USE_SPIRV ) || defined( USE_NX ) || defined( USE_PSSL )
#define	GEO_OUT_MAX_COUNT( __num )	[maxvertexcount(__num)]

#elif defined( USE_GLSL )

#endif


//-------------------------------------------------------------------------------------------------
// 条件でのパラメータ分岐
#if defined( USE_PSSL )
#else	// PS4以外
float	CndMask(bool _cnd, float _src0, float _src1)
{
	return _src0 * _cnd + _src1 * (1 - _cnd);
}
#endif 


//-------------------------------------------------------------------------------------------------
// 圧縮・解凍
void	CompressValue(out float _compressed, float _high, float _low)
{
	// bitをそのままにキャスト
	_compressed = asfloat((f32tof16(_high) << 16) | f32tof16(_low));
}
void	DecompressValue(float _compressed, out float _high, out float _low)
{
	// bitをそのままにキャスト
	uint compressedLocal = asuint(_compressed);
	_high = f16tof32(compressedLocal >> 16);
	_low = f16tof32(compressedLocal);
}


//-------------------------------------------------------------------------------------------------
// 輝度値用定数
#define GRAY_SCALE					float3( 0.29891f, 0.58661f, 0.11448f )
#define CALC_GRAY_SCALE(__col)		dot( __col.rgb, GRAY_SCALE.rgb )
// RGB -> YCbCr
#define RGB_TO_Y					float3(  0.29900f,  0.58700f,  0.11400f )
#define RGB_TO_Cb					float3( -0.16874f, -0.33126f,  0.50000f )
#define RGB_TO_Cr					float3(  0.50000f, -0.41869f, -0.08100f )
#define CALC_RGB_TO_YCbCr(__col)	float3( dot( __col.rgb, RGB_TO_Y.rgb ), dot( __col.rgb, RGB_TO_Cb.rgb ), dot( __col.rgb, RGB_TO_Cr.rgb ) )
// YCbCr -> RGB
#define YCC_TO_R					float3( 1.0f,  0.0f,      1.40200f )
#define YCC_TO_G					float3( 1.0f, -0.34414f, -0.71414f )
#define YCC_TO_B					float3( 1.0f,  1.77200f,  0.0f )
#define CALC_YCbCr_TO_RGB(__col)	float3( dot( __col.rgb, YCC_TO_R.rgb ), dot( __col.rgb, YCC_TO_G.rgb ), dot( __col.rgb, YCC_TO_B.rgb ) )
// RGB -> YCoCg
#define RGB_TO_Yog					float3(  0.2500f,  0.5000f,  0.2500f )
#define RGB_TO_Co					float3(  0.5000f,  0.0000f, -0.5000f )
#define RGB_TO_Cg					float3( -0.2500f,  0.5000f, -0.2500f )
#define CALC_RGB_TO_YCoCg(__col)	float3( dot( __col.rgb, RGB_TO_Yog.rgb ), dot( __col.rgb, RGB_TO_Co.rgb ), dot( __col.rgb, RGB_TO_Cg.rgb ) )
// YCoCg -> RGB
#define YCoCg_TO_R					float3( 1.0f,  1.0f, -1.0f )
#define YCoCg_TO_G					float3( 1.0f,  0.0f,  1.0f )
#define YCoCg_TO_B					float3( 1.0f, -1.0f, -1.0f )
#define CALC_YCoCg_TO_RGB(__col)	float3( dot( __col.rgb, YCoCg_TO_R.rgb ), dot( __col.rgb, YCoCg_TO_G.rgb ), dot( __col.rgb, YCoCg_TO_B.rgb ) )


//--------------------------------------------------------------------------------------
// 変換マクロ
#define CONV_SETUP_SCREEN_SPACE_UV(__pos_xyzw)	((__pos_xyzw.xy + __pos_xyzw.w) * 0.5f)
// ピクセルシェーダーでSV_Positionを参照する際の変換
#if defined( USE_NX ) || defined( USE_GLSL )
#define	CONV_PS_POS_W(__pos)	rcp(__pos.w)		// NXではgl_FragCoord.wに変換されるため逆数にする必要がある
#else	//	USE_NX
#define	CONV_PS_POS_W(__pos)	(__pos.w)
#endif	//	USE_NX


//-------------------------------------------------------------------------------------------------
// 数学
#define PI				3.1415926535897932384626433832795
#define INV_PI			0.31830988618379067153776752674503
#define SQRT_2			1.4142135623730950488016887242097
#define SQRT_3			1.7320508075688772935274463415059

#define HALF			0.5
#define ONE_THIRD		0.33333333
#define QUARTER			0.25

#define IDENTITY_22		mtx22(1.0f, 0.0f, 0.0f, 1.0f)
#define IDENTITY_32		mtx32(1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f)
#define IDENTITY_33		mtx33(1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f)
#define IDENTITY_43		mtx43(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f)
#define IDENTITY_44		mtx44(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f)


//-------------------------------------------------------------------------------------------------
// 比較関数（0=false, 1=true）
// x >= edge
#define	TEST_GEQUAL( _x, _edge)		(step(_edge, _x))

// x <= edge
#define	TEST_LEQUAL( _x, _edge)		(step(_x, _edge))

// x > edge
#define	TEST_GREATER( _x, _edge)	(1.0 - step(_x, _edge))

// x < edge
#define	TEST_LESS( _x, _edge)		(1.0 - step(_edge, _x))

// x != edge
#define	TEST_NOTEQUAL( _x, _edge)	(abs(sign(_x - _edge)))

// x == edge
#define	TEST_EQUAL( _x, _edge)		(1.0 - abs(sign(_x - _edge)))


//-------------------------------------------------------------------------------------------------
// 座標系デファイン
//#define OPENGL_PROJECTION		// -1~+1
#define DIRECTX_PROJECTION	// 0~1


#endif	// __COMMON_H__




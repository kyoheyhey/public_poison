//#pragma once
#ifndef __NOISE_H__
#define __NOISE_H__


//-------------------------------------------------------------------------------------------------
// ノイズ
float	Noise(float2 _texCoord, int _seed)
{
	return frac(sin(dot(_texCoord, float2(12.9898f, 78.233f)) + _seed) * 43758.5453f);
}
float	Noise(float3 _pos, int _seed)
{
	return frac(sin(dot(_pos, float3(12.9898f, 78.233f, 56.787f)) + _seed) * 43758.5453f);
}

//-------------------------------------------------------------------------------------------------
// 補間ノイズ
float	LerpNoise(float2 _texCoord, int _seed)
{
	float2 ip = floor(_texCoord);
	float2 fp = frac(_texCoord);
	float4 a = float4(
		Noise(ip + float2(0, 0), _seed),
		Noise(ip + float2(1, 0), _seed),
		Noise(ip + float2(0, 1), _seed),
		Noise(ip + float2(1, 1), _seed)
		);
	float2 u = fp * fp * (3.0 - 2.0 * fp);
	return lerp(a.x, a.y, u.x) +
		(a.z - a.x) * u.y * (1.0 - u.x) +
		(a.w - a.y) * u.x * u.y;
}
float	LerpNoise(float3 _pos, int _seed)
{
	float3 ip = floor(_pos);
	float3 fp = frac(_pos);
	float4 a = float4(
		Noise(ip + float3(0, 0, 0), _seed),
		Noise(ip + float3(1, 0, 0), _seed),
		Noise(ip + float3(0, 1, 0), _seed),
		Noise(ip + float3(1, 1, 0), _seed)
		);
	float4 b = float4(
		Noise(ip + float3(0, 0, 1), _seed),
		Noise(ip + float3(1, 0, 1), _seed),
		Noise(ip + float3(0, 1, 1), _seed),
		Noise(ip + float3(1, 1, 1), _seed)
		);
	a = lerp(a, b, fp.z);
	a.xy = lerp(a.xy, a.zw, fp.y);
	return lerp(a.x, a.y, fp.x);
}

//-------------------------------------------------------------------------------------------------
// パーリンノイズ
float	PerlinNoise(float2 _texCoord, int _seed)
{
	return (LerpNoise(_texCoord, _seed) * 32.0 +
		LerpNoise(_texCoord * 2.0, _seed) * 16.0 +
		LerpNoise(_texCoord * 4.0, _seed) * 8.0 +
		LerpNoise(_texCoord * 8.0, _seed) * 4.0 +
		LerpNoise(_texCoord * 16.0, _seed) * 2.0 +
		LerpNoise(_texCoord * 32.0, _seed)) /
		63.0;
}
float	PerlinNoise(float3 _pos, int _seed)
{
	return (LerpNoise(_pos, _seed) * 32.0 +
		LerpNoise(_pos * 2.0, _seed) * 16.0 +
		LerpNoise(_pos * 4.0, _seed) * 8.0 +
		LerpNoise(_pos * 8.0, _seed) * 4.0 +
		LerpNoise(_pos * 16.0, _seed) * 2.0 +
		LerpNoise(_pos * 32.0, _seed)) /
		63.0;
}


#endif	// __NOISE_H__

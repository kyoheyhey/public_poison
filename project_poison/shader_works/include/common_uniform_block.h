//#pragma once
#ifndef	__COMMON_UNIFORM_BLOCK_H__
#define	__COMMON_UNIFORM_BLOCK_H__

//-------------------------------------------------------------------------------------------------
// ユニフォームブロック
// vs,ps	0	グローバル、Unifromが使用
// vs,ps	1	行列情報
// vs,ps	2	カメラ・画面情報(common_camera_screen.h)
// vs,ps	3	ライト・環境情報(common_light_environment.h)
// vs,ps	4	モデルマテリアル・シェーダーパラメタ(model_material.h)
// vs		5	頂点ブレンド(model_vertex_blend.h)
// vs		6	頂点ブレンド過去(model_vertex_blend.h)
// ps		8	ポイントライト(point_light_data.h)
// ps		9	キューブマップ(cubemap.h)
//-------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------
// 基本のユニフォームブロック設定


//-------------------------------------------------------------------------------------------------
// 行列ユニフォームブロック
UNIFORM_BLOCK(1)
{
	mtx44 u_mtxLW;		///< ローカル⇒ワールド
	mtx44 u_mtxLV;		///< ローカル⇒ビュー
	mtx44 u_mtxLP;		///< ローカル⇒プロジェ
	mtx44 u_mtxWV;		///< ワールド⇒ビュー
	mtx44 u_mtxWP;		///< ワールド⇒プロジェ
	mtx44 u_mtxVP;		///< ビュー⇒プロジェ

	mtx44 u_mtxPW;		///< プロジェ⇒ワールド
	mtx44 u_mtxPV;		///< プロジェ⇒ビュー
	mtx44 u_mtxVW;		///< ビュー⇒ワールド

	mtx44 u_mtxOldLW;	///< 前回ローカル⇒ワールド
	mtx44 u_mtxOldLV;	///< 前回ローカル⇒ビュー
	mtx44 u_mtxOldLP;	///< 前回ローカル⇒ビュー
	mtx44 u_mtxOldWV;	///< 前回ワールド⇒ビュー
	mtx44 u_mtxOldWP;	///< 前回ワールド⇒プロジェ
	mtx44 u_mtxOldVP;	///< 前回ビュー⇒プロジェ
};



#endif// __COMMON_UNIFORM_BLOCK_H__

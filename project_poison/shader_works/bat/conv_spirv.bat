@ECHO OFF

rem カレントディレクトリ設定
%~d0
cd %~dp0..\

rem シェーダー名入力
SET SHADER_PATH=%~dpn1
SET SHADER_NAME=%~n1
SET SHADER_EXT=%~x1

SET CORE_BAT=bat/conv_spirv_core.bat

ECHO SPIR-Vシェーダーコンパイル  %SHADER_NAME%

rem vs出力
IF %SHADER_EXT% == .vs (
	call %CORE_BAT% "%SHADER_PATH%.vs" "out\spirv\%SHADER_NAME%.vsx" "out\tmp\spirv\%SHADER_NAME%.vs.txt" vs_6_4 "CONV_VS"
)

rem es出力
IF %SHADER_EXT% == .es (
	call %CORE_BAT% "%SHADER_PATH%.es" "out\spirv\%SHADER_NAME%.esx" "out\tmp\spirv\%SHADER_NAME%.es.txt" vs_6_4 "CONV_ES"
)

rem gs出力
IF %SHADER_EXT% == .gs (
	call %CORE_BAT% "%SHADER_PATH%.gs" "out\spirv\%SHADER_NAME%.gsx" "out\tmp\spirv\%SHADER_NAME%.gs.txt" gs_6_4 "CONV_GS"
)

rem ps出力
IF %SHADER_EXT% == .ps (
	call %CORE_BAT% "%SHADER_PATH%.ps" "out\spirv\%SHADER_NAME%.psx" "out\tmp\spirv\%SHADER_NAME%.ps.txt" ps_6_4 "CONV_PS"
)

rem cs出力
IF %SHADER_EXT% == .cs (
	call %CORE_BAT% "%SHADER_PATH%.cs" "out\spirv\%SHADER_NAME%.csx" "out\tmp\spirv\%SHADER_NAME%.cs.txt" cs_6_4 "CONV_CS"
)

pause

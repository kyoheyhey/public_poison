@ECHO OFF

ECHO ===============================================================================
ECHO  Shader Convert %1 %2
ECHO ===============================================================================

rem カレントディレクトリ設定
%~d0
cd %~dp0..\

rem ファイル名入力
SET FILE_PATH=%~dpn1
SET FILE_NAME=%~n1
SET FILE_EXT=%~x1

SET CORE_CFG_BAT=bat/conv_cfg_core.bat
SET CORE_FXCFG_BAT=bat/conv_fxcfg_core.bat

rem includeを無効に
SET TEMP_INCLUDE=%INCLUDE%
SET INCLUDE=""

IF %FILE_EXT% == .cfg (
	CALL %CORE_CFG_BAT% "%FILE_PATH%.cfg" "out\common\list\%FILE_NAME%.cfgbin" "out\tmp\common\%FILE_NAME%.cfg.txt"
)
IF %FILE_EXT% == .fxcfg (
	CALL %CORE_FXCFG_BAT% "%FILE_PATH%.fxcfg" "out\common\fxcfg\%FILE_NAME%.fxbin" "out\tmp\common\%FILE_NAME%.fxcfg.txt"
)

rem includeを戻す
SET INCLUDE = %TEMP_INCLUDE%

EXIT 0


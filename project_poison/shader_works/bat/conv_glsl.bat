@ECHO OFF

rem カレントディレクトリ設定
%~d0
cd %~dp0..\

rem シェーダー名入力
SET SHADER_PATH=%~dpn1
SET SHADER_NAME=%~n1
SET SHADER_EXT=%~x1

SET CORE_BAT=bat/conv_glsl_core.bat

ECHO GLSLシェーダーコンパイル  %SHADER_NAME%

rem vs出力
IF %SHADER_EXT% == .vs (
	call %CORE_BAT% "%SHADER_PATH%.vs" "out\glsl\%SHADER_NAME%.vert" "out\tmp\glsl\%SHADER_NAME%.vs.txt" vs_6_4 "CONV_VS"
)

rem es出力
IF %SHADER_EXT% == .es (
	call %CORE_BAT% "%SHADER_PATH%.es" "out\glsl\%SHADER_NAME%.tesc" "out\tmp\glsl\%SHADER_NAME%.es.txt" vs_6_4 "CONV_ES"
)

rem gs出力
IF %SHADER_EXT% == .gs (
	call %CORE_BAT% "%SHADER_PATH%.gs" "out\glsl\%SHADER_NAME%.geom" "out\tmp\glsl\%SHADER_NAME%.gs.txt" gs_6_4 "CONV_GS"
)

rem ps出力
IF %SHADER_EXT% == .ps (
	call %CORE_BAT% "%SHADER_PATH%.ps" "out\glsl\%SHADER_NAME%.frag" "out\tmp\glsl\%SHADER_NAME%.ps.txt" ps_6_4 "CONV_PS"
)

rem cs出力
IF %SHADER_EXT% == .cs (
	call %CORE_BAT% "%SHADER_PATH%.cs" "out\glsl\%SHADER_NAME%.comp" "out\tmp\glsl\%SHADER_NAME%.cs.txt" cs_6_4 "CONV_CS"
)

pause

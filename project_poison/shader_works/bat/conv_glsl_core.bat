
SET SPIRV_EXE_PATH=%~dp0..\bin\dxc.exe
SET GLSL_EXE_PATH=%~dp0..\bin\spirv-cross.exe
SET INCLUDE_PATH=%~dp0..\include
SET SOURCE_PATH=%~dp0..\source
SET SRC_SHADER_PATH=%~1
SET SRC_SHADER_DIRECTOR_PATH=%~dp1
SET OUT_SHADER_PATH=%~2
SET OUT_TMP_SHADER_PATH=%~3
SET OUT_TMP_PATH=%~dp3
SET OUT_FILE_NAME=%~n3
SET PROFILE=%~4
SET EXTEND=%~5


rem GLレイアウト
SET GL_LAYOUT_OPT=-fvk-use-gl-layout
rem DXレイアウト
SET DX_LAYOUT_OPT=-fvk-use-dx-layout
rem SpirVリフレクト
SET SPIRV_REFLECT_OPT=-fspv-reflect
rem 配列リソースフラット化
SET FLATTEN_RES_ARY_OPT=-fspv-flatten-resource-arrays
rem DXUVy
SET DX_UV_Y_OPT=-fvk-invert-y
rem DXポジションw
SET DX_POSITION_W_OPT=-fvk-use-dx-position-w

IF EXIST "%SRC_SHADER_PATH%" (
	rem フォルダ作成
	IF NOT EXIST %~dp2 (
		MKDIR %~dp2
	)
	IF NOT EXIST %~dp3 (
		MKDIR %~dp3
	)
	rem hlsl⇒spirvコンパイル
	IF "%EXTEND%" == "CONV_VS" (
		%SPIRV_EXE_PATH% ^
		%GL_LAYOUT_OPT% ^
		%FLATTEN_RES_ARY_OPT% ^
		-D USE_SPIRV -I %INCLUDE_PATH% -I %SOURCE_PATH% -I %SRC_SHADER_DIRECTOR_PATH% ^
		-spirv -T %PROFILE% -E main %SRC_SHADER_PATH% ^
		-Fo "%OUT_TMP_PATH%%OUT_FILE_NAME%.spv" -Fc "%OUT_TMP_SHADER_PATH%"
	)
	IF "%EXTEND%" == "CONV_ES" (
		%SPIRV_EXE_PATH% ^
		%GL_LAYOUT_OPT% ^
		%FLATTEN_RES_ARY_OPT% ^
		-D USE_SPIRV -I %INCLUDE_PATH% -I %SOURCE_PATH% -I %SRC_SHADER_DIRECTOR_PATH% ^
		-spirv -T %PROFILE% -E main %SRC_SHADER_PATH% ^
		-Fo "%OUT_TMP_PATH%%OUT_FILE_NAME%.spv" -Fc "%OUT_TMP_SHADER_PATH%"
	)
	IF "%EXTEND%" == "CONV_GS" (
		%SPIRV_EXE_PATH% ^
		%GL_LAYOUT_OPT% ^
		%FLATTEN_RES_ARY_OPT% ^
		-D USE_SPIRV -I %INCLUDE_PATH% -I %SOURCE_PATH% -I %SRC_SHADER_DIRECTOR_PATH% ^
		-spirv -T %PROFILE% -E main %SRC_SHADER_PATH% ^
		-Fo "%OUT_TMP_PATH%%OUT_FILE_NAME%.spv" -Fc "%OUT_TMP_SHADER_PATH%"
	)
	IF "%EXTEND%" == "CONV_PS" (
		%SPIRV_EXE_PATH% ^
		%GL_LAYOUT_OPT% ^
		%FLATTEN_RES_ARY_OPT% %DX_POSITION_W_OPT% ^
		-D USE_SPIRV -I %INCLUDE_PATH% -I %SOURCE_PATH% -I %SRC_SHADER_DIRECTOR_PATH% ^
		-spirv -T %PROFILE% -E main %SRC_SHADER_PATH% ^
		-Fo "%OUT_TMP_PATH%%OUT_FILE_NAME%.spv" -Fc "%OUT_TMP_SHADER_PATH%"
	)
	IF "%EXTEND%" == "CONV_CS" (
		%SPIRV_EXE_PATH% ^
		%GL_LAYOUT_OPT% ^
		%FLATTEN_RES_ARY_OPT% ^
		-D USE_SPIRV -I %INCLUDE_PATH% -I %SOURCE_PATH% -I %SRC_SHADER_DIRECTOR_PATH% ^
		-spirv -T %PROFILE% -E main %SRC_SHADER_PATH% ^
		-Fo "%OUT_TMP_PATH%%OUT_FILE_NAME%.spv" -Fc "%OUT_TMP_SHADER_PATH%"
	)
	
	rem spirv⇒glslコンパイル
	%GLSL_EXE_PATH% ^
	--version 460 --no-es ^
	--combined-samplers-inherit-bindings --separate-shader-objects ^
	"%OUT_TMP_PATH%%OUT_FILE_NAME%.spv" > "%OUT_SHADER_PATH%"
	
	rem DEL "%OUT_TMP_SHADER_PATH%"
	rem DEL "%OUT_TMP_PATH%\%OUT_FILE_NAME%.spv"
)
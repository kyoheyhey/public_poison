@ECHO OFF

rem カレントディレクトリ設定
%~d0
cd %~dp0..\

rem シェーダー名入力
SET SHADER_PATH=%~dpn1
SET SHADER_NAME=%~n1
SET SHADER_EXT=%~x1

SET CORE_BAT=bat/conv_hlsl_core.bat

ECHO HLSLシェーダーコンパイル  %SHADER_NAME%

rem vs出力
IF %SHADER_EXT% == .vs (
	call %CORE_BAT% "%SHADER_PATH%.vs" "out\hlsl\%SHADER_NAME%.vsb" "out\tmp\hlsl\%SHADER_NAME%.vs.txt" vs_5_0 "CONV_VS"
)

rem es出力
IF %SHADER_EXT% == .es (
	call %CORE_BAT% "%SHADER_PATH%.es" "out\hlsl\%SHADER_NAME%.esb" "out\tmp\hlsl\%SHADER_NAME%.es.txt" vs_5_0 "CONV_ES"
)

rem gs出力
IF %SHADER_EXT% == .gs (
	call %CORE_BAT% "%SHADER_PATH%.gs" "out\hlsl\%SHADER_NAME%.gsb" "out\tmp\hlsl\%SHADER_NAME%.gs.txt" gs_5_0 "CONV_GS"
)

rem ps出力
IF %SHADER_EXT% == .ps (
	call %CORE_BAT% "%SHADER_PATH%.ps" "out\hlsl\%SHADER_NAME%.psb" "out\tmp\hlsl\%SHADER_NAME%.ps.txt" ps_5_0 "CONV_PS"
)

rem cs出力
IF %SHADER_EXT% == .cs (
	call %CORE_BAT% "%SHADER_PATH%.cs" "out\hlsl\%SHADER_NAME%.csb" "out\tmp\hlsl\%SHADER_NAME%.cs.txt" cs_5_0 "CONV_CS"
)

pause

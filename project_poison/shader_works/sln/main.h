﻿//#pragma once

#ifndef	__MAIN_H__
#define	__MAIN_H__


// インテリセンスごまかし

typedef	unsigned int	uint;

//-------------------------------------------------------------------------------------------------
struct float2
{
	float	x, y;
	float	r, g;
};

struct float3
{
	float	x, y, z;
	float	r, g, b;
};

struct float4
{
	float	x, y, z, w;
	float	r, g, b, a;
};

//-------------------------------------------------------------------------------------------------
struct int2
{
	float	x, y;
	float	r, g;
};

struct int3
{
	float	x, y, z;
	float	r, g, b;
};

struct int4
{
	float	x, y, z, w;
	float	r, g, b, a;
};


//-------------------------------------------------------------------------------------------------
struct uint2
{
	float	x, y;
	float	r, g;
};

struct uint3
{
	float	x, y, z;
	float	r, g, b;
};

struct uint4
{
	float	x, y, z, w;
	float	r, g, b, a;
};

//-------------------------------------------------------------------------------------------------
struct bool2
{
	float	x, y;
	float	r, g;
};

struct bool3
{
	float	x, y, z;
	float	r, g, b;
};

struct bool4
{
	float	x, y, z, w;
	float	r, g, b, a;
};


//-------------------------------------------------------------------------------------------------
struct float2x2
{
	float2 m[2];
};

struct float3x2
{
	float2 m[3];
};

struct float4x2
{
	float2 m[4];
};

struct float2x3
{
	float3 m[2];
};

struct float3x3
{
	float3 m[3];
};

struct float4x3
{
	float3 m[4];
};

struct float4x3
{
	float3 m[4];
};

struct float2x4
{
	float4 m[2];
};

struct float3x4
{
	float4 m[3];
};

struct float4x4
{
	float4 m[4];
};



typedef	float2x2			mtx22;
typedef	float3x2			mtx32;
typedef	float4x2			mtx42;
typedef	float2x3			mtx23;
typedef	float3x3			mtx33;
typedef	float4x3			mtx43;
typedef	float2x4			mtx24;
typedef	float3x4			mtx34;
typedef	float4x4			mtx44;
typedef	float				min16float;
typedef	float2				min16float2;
typedef	float3				min16float3;
typedef	float4				min16float4;
typedef	float2x2			min16float2x2;
typedef	float3x2			min16float2x3;
typedef	float4x2			min16float2x4;
typedef	float2x3			min16float3x2;
typedef	float3x3			min16float3x3;
typedef	float4x3			min16float3x4;
typedef	float2x4			min16float4x2;
typedef	float3x4			min16float4x3;
typedef	float4x4			min16float4x4;



//-------------------------------------------------------------------------------------------------
/// 絶対値
float abs( float f );



#endif	// __MAIN_H__




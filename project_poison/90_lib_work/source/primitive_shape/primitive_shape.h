﻿//#pragma once
#ifndef	__PRIMITIVE_SHAPE_H__
#define	__PRIMITIVE_SHAPE_H__

#include "math/vector.h"

WORK_NAMESPACE_BGN

//*************************************************************************************************
//@brief	形状ユーティリティクラス
//*************************************************************************************************
class CShapeUtility
{
public:
	struct SHAPE_INFO
	{
		const f32*	pVtxPos;	///< 座標
		const f32*	pVtxClr;	///< 色
		const f32*	pVtxNrm;	///< 座標
		const f32*	pVtxUv;		///< UV
		const u32*	pIdx;		///< インデックス
		u32			vtxNum;		///< 頂点数
		u32			idxNum;		///< インデックス数
		SHAPE_INFO()
			: pVtxPos(NULL)
			, pVtxClr(NULL)
			, pVtxNrm(NULL)
			, pVtxUv(NULL)
			, pIdx(NULL)
			, vtxNum(0)
			, idxNum(0)
		{}
		SHAPE_INFO(const f32* _pPos, const f32* _pClr, const f32* _pNrm, const f32* _pUv, const u32* _pIdx, u32 _vtxNum, u32 _idxNum)
			: pVtxPos(_pPos)
			, pVtxClr(_pClr)
			, pVtxNrm(_pNrm)
			, pVtxUv(_pUv)
			, pIdx(_pIdx)
			, vtxNum(_vtxNum)
			, idxNum(_idxNum)
		{}
	};
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	平面取得
	static const SHAPE_INFO&	GetPlane()
	{
		static SHAPE_INFO s_lcosahedral(
			GetPlaneVtxPos(),
			GetPlaneVtxClr(),
			GetPlaneVtxNrm(),
			GetPlaneVtxUv(),
			GetPlaneIdx(),
			GetPlaneVtxNum(),
			GetPlaneIdxNum()
		);
		return s_lcosahedral;
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	箱取得
	static const SHAPE_INFO&	GetCube()
	{
		static SHAPE_INFO s_cube(
			GetCubeVtxPos(),
			GetCubeVtxClr(),
			GetCubeVtxNrm(),
			GetCubeVtxUv(),
			GetCubeIdx(),
			GetCubeVtxNum(),
			GetCubeIdxNum()
			);
		return s_cube;
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	球体取得
	static const SHAPE_INFO& GetSphere()
	{
		static SHAPE_INFO s_sphere(
			GetSphereVtxPos(),
			GetSphereVtxClr(),
			GetSphereVtxNrm(),
			GetSphereVtxUv(),
			GetSphereIdx(),
			GetSphereVtxNum(),
			GetSphereIdxNum()
		);
		return s_sphere;
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	正二十面体取得
	static const SHAPE_INFO& GetLcosahedral()
	{
		static SHAPE_INFO s_lcosahedral(
			GetLcosahedralVtxPos(),
			GetLcosahedralVtxClr(),
			GetLcosahedralVtxNrm(),
			GetLcosahedralVtxUv(),
			GetLcosahedralIdx(),
			GetLcosahedralVtxNum(),
			GetLcosahedralIdxNum()
		);
		return s_lcosahedral;
	}
	
private:
	//-------------------------------------------------------------------------------------------------
	//@brief	平面の座標(4要素)
	static const f32*	GetPlaneVtxPos();
	//-------------------------------------------------------------------------------------------------
	//@brief	平面の法線(3要素)
	static const f32*	GetPlaneVtxNrm();
	//-------------------------------------------------------------------------------------------------
	//@brief	平面の色(4要素)
	static const f32*	GetPlaneVtxClr();
	//-------------------------------------------------------------------------------------------------
	//@brief	平面のUV(2要素)
	static const f32*	GetPlaneVtxUv();
	//-------------------------------------------------------------------------------------------------
	//@brief	平面のIDX
	static const u32*	GetPlaneIdx();
	//-------------------------------------------------------------------------------------------------
	//@brief	平面の頂点数
	static const u32	GetPlaneVtxNum();
	//-------------------------------------------------------------------------------------------------
	//@brief	平面のインデックス数
	static const u32	GetPlaneIdxNum();

private:
	//-------------------------------------------------------------------------------------------------
	//@brief	箱の座標(4要素)
	static const f32*	GetCubeVtxPos();
	//-------------------------------------------------------------------------------------------------
	//@brief	箱の法線(3要素)
	static const f32*	GetCubeVtxNrm();
	//-------------------------------------------------------------------------------------------------
	//@brief	箱の色(4要素)
	static const f32*	GetCubeVtxClr();
	//-------------------------------------------------------------------------------------------------
	//@brief	箱のUV(2要素)
	static const f32*	GetCubeVtxUv();
	//-------------------------------------------------------------------------------------------------
	//@brief	箱のIDX
	static const u32*	GetCubeIdx();
	//-------------------------------------------------------------------------------------------------
	//@brief	箱の頂点数
	static const u32	GetCubeVtxNum();
	//-------------------------------------------------------------------------------------------------
	//@brief	箱のインデックス数
	static const u32	GetCubeIdxNum();

private:
	//-------------------------------------------------------------------------------------------------
	//@brief	球の座標(4要素)
	static const f32* GetSphereVtxPos();
	//-------------------------------------------------------------------------------------------------
	//@brief	球の法線(3要素)
	static const f32* GetSphereVtxNrm();
	//-------------------------------------------------------------------------------------------------
	//@brief	球の色(4要素)
	static const f32* GetSphereVtxClr();
	//-------------------------------------------------------------------------------------------------
	//@brief	球のUV(2要素)
	static const f32* GetSphereVtxUv();
	//-------------------------------------------------------------------------------------------------
	//@brief	球のIDX
	static const u32* GetSphereIdx();
	//-------------------------------------------------------------------------------------------------
	//@brief	球の頂点数
	static const u32	GetSphereVtxNum();
	//-------------------------------------------------------------------------------------------------
	//@brief	球のインデックス数
	static const u32	GetSphereIdxNum();

private:
	//-------------------------------------------------------------------------------------------------
	//@brief	正二十面体の座標(4要素)
	static const f32*	GetLcosahedralVtxPos();
	//-------------------------------------------------------------------------------------------------
	//@brief	正二十面体の法線(3要素)
	static const f32*	GetLcosahedralVtxNrm();
	//-------------------------------------------------------------------------------------------------
	//@brief	正二十面体の色(4要素)
	static const f32*	GetLcosahedralVtxClr();
	//-------------------------------------------------------------------------------------------------
	//@brief	正二十面体のUV(2要素)
	static const f32*	GetLcosahedralVtxUv();
	//-------------------------------------------------------------------------------------------------
	//@brief	正二十面体のIDX
	static const u32*	GetLcosahedralIdx();
	//-------------------------------------------------------------------------------------------------
	//@brief	正二十面体の頂点数
	static const u32	GetLcosahedralVtxNum() { return 60u; }
	//-------------------------------------------------------------------------------------------------
	//@brief	正二十面体のインデックス数
	static const u32	GetLcosahedralIdxNum() { return 60u; }
};


WORK_NAMESPACE_END

#endif	// __PRIMITIVE_SHAPE_H__

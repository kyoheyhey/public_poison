﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "primitive_shape.h"

#include "math/math.h"
#include "math/spc.h"


WORK_NAMESPACE_BGN

///*************************************************************************************************
///	平面
///*************************************************************************************************

#define PRIMITEVE_PLANE_DIV_NUM		(8)	// 分割数
#define PRIMITEVE_PLANE_EDGE_NUM	(PRIMITEVE_PLANE_DIV_NUM  + 1)	// エッジ数
static const u32	s_planeVtxNum = PRIMITEVE_PLANE_EDGE_NUM * PRIMITEVE_PLANE_EDGE_NUM;
static const u32	s_planeIdxNum = PRIMITEVE_PLANE_DIV_NUM * PRIMITEVE_PLANE_DIV_NUM * 6;

///-------------------------------------------------------------------------------------------------
/// 平面の座標(4要素)
const f32*	CShapeUtility::GetPlaneVtxPos()
{
	static f32 s_vtxPos[s_planeVtxNum * 4] = {};
	static b8 s_isInit = false;
	if (!s_isInit)
	{
		u32 idx = 0;
		for (u32 i = 0; i < PRIMITEVE_PLANE_EDGE_NUM; ++i)
		{
			for (u32 j = 0; j < PRIMITEVE_PLANE_EDGE_NUM; ++j)
			{
				s_vtxPos[idx + 0] = ((SCast<f32>(i) / PRIMITEVE_PLANE_DIV_NUM) - 0.5f) * 2.0f;
				s_vtxPos[idx + 1] = 0.0f;
				s_vtxPos[idx + 2] = ((SCast<f32>(j) / PRIMITEVE_PLANE_DIV_NUM) - 0.5f) * 2.0f;
				s_vtxPos[idx + 3] = 1.0f;
				idx += 4;
			}
		}
		s_isInit = true;
	}
	return s_vtxPos;
}

///-------------------------------------------------------------------------------------------------
/// 平面の法線(3要素)
const f32*	CShapeUtility::GetPlaneVtxNrm()
{
	static f32 s_vtxNrm[s_planeVtxNum * 3] = {};
	static b8 s_isInit = false;
	if (!s_isInit)
	{
		u32 idx = 0;
		for (u32 i = 0; i < PRIMITEVE_PLANE_EDGE_NUM; ++i)
		{
			for (u32 j = 0; j < PRIMITEVE_PLANE_EDGE_NUM; ++j)
			{
				s_vtxNrm[idx + 0] = 0.0f;
				s_vtxNrm[idx + 1] = 1.0f;
				s_vtxNrm[idx + 2] = 0.0f;
				idx += 3;
			}
		}
		s_isInit = true;
	}
	return s_vtxNrm;
}

///-------------------------------------------------------------------------------------------------
/// 平面の色(4要素)
const f32*	CShapeUtility::GetPlaneVtxClr()
{
	static f32 s_vtxClr[s_planeVtxNum * 4] = {};
	static b8 s_isInit = false;
	if (!s_isInit)
	{
		u32 idx = 0;
		for (u32 i = 0; i < PRIMITEVE_PLANE_EDGE_NUM; ++i)
		{
			for (u32 j = 0; j < PRIMITEVE_PLANE_EDGE_NUM; ++j)
			{
				s_vtxClr[idx + 0] = 1.0f;
				s_vtxClr[idx + 1] = 1.0f;
				s_vtxClr[idx + 2] = 1.0f;
				s_vtxClr[idx + 3] = 1.0f;
				idx += 4;
			}
		}
		s_isInit = true;
	}
	return s_vtxClr;
}

///-------------------------------------------------------------------------------------------------
/// 平面のUV(2要素)
const f32*	CShapeUtility::GetPlaneVtxUv()
{
	static f32 s_vtxUv[s_planeVtxNum * 2] = {};
	static b8 s_isInit = false;
	if (!s_isInit)
	{
		u32 idx = 0;
		for (u32 i = 0; i < PRIMITEVE_PLANE_EDGE_NUM; ++i)
		{
			for (u32 j = 0; j < PRIMITEVE_PLANE_EDGE_NUM; ++j)
			{
				s_vtxUv[idx + 0] = (SCast<f32>(j) / PRIMITEVE_PLANE_DIV_NUM);
				s_vtxUv[idx + 1] = 1.0f - (SCast<f32>(i) / PRIMITEVE_PLANE_DIV_NUM);
				idx += 2;
			}
		}
		s_isInit = true;
	}
	return s_vtxUv;
}

///-------------------------------------------------------------------------------------------------
/// 平面のIDX
const u32*	CShapeUtility::GetPlaneIdx()
{
	static u32 s_index[s_planeIdxNum];
	static b8 s_isInit = false;
	if (!s_isInit)
	{
		u32 idx = 0;
		for (u32 i = 0; i < PRIMITEVE_PLANE_DIV_NUM; ++i)
		{
			for (u32 j = 0; j < PRIMITEVE_PLANE_DIV_NUM; ++j)
			{
				auto calcIdx = [](u32 _x, u32 _y, u32 _division)->u32
				{
					return _x + _y * _division;
				};
				s_index[idx + 0] = calcIdx(j + 0, i + 0, PRIMITEVE_PLANE_EDGE_NUM);
				s_index[idx + 1] = calcIdx(j + 1, i + 0, PRIMITEVE_PLANE_EDGE_NUM);
				s_index[idx + 2] = calcIdx(j + 1, i + 1, PRIMITEVE_PLANE_EDGE_NUM);

				s_index[idx + 3] = calcIdx(j + 1, i + 1, PRIMITEVE_PLANE_EDGE_NUM);
				s_index[idx + 4] = calcIdx(j + 0, i + 1, PRIMITEVE_PLANE_EDGE_NUM);
				s_index[idx + 5] = calcIdx(j + 0, i + 0, PRIMITEVE_PLANE_EDGE_NUM);
				idx += 6;
			}
		}
		s_isInit = true;
	}
	return s_index;
}

///-------------------------------------------------------------------------------------------------
/// 平面の頂点数
const u32	CShapeUtility::GetPlaneVtxNum()
{
	return s_planeVtxNum;
}

///-------------------------------------------------------------------------------------------------
/// 平面のインデックス数
const u32	CShapeUtility::GetPlaneIdxNum()
{
	return s_planeIdxNum;
}


///*************************************************************************************************
///	箱
///*************************************************************************************************
/// 箱の座標
static const f32 s_vtxPos[] =
{
	+1.0f, +1.0f, +1.0f, +1.0f,
	-1.0f, +1.0f, +1.0f, +1.0f,
	-1.0f, -1.0f, +1.0f, +1.0f,
	+1.0f, -1.0f, +1.0f, +1.0f,

	-1.0f, -1.0f, -1.0f, +1.0f,
	-1.0f, +1.0f, -1.0f, +1.0f,
	+1.0f, +1.0f, -1.0f, +1.0f,
	+1.0f, -1.0f, -1.0f, +1.0f,

	+1.0f, +1.0f, -1.0f, +1.0f,
	-1.0f, +1.0f, -1.0f, +1.0f,
	-1.0f, +1.0f, +1.0f, +1.0f,
	+1.0f, +1.0f, +1.0f, +1.0f,

	-1.0f, -1.0f, +1.0f, +1.0f,
	-1.0f, -1.0f, -1.0f, +1.0f,
	+1.0f, -1.0f, -1.0f, +1.0f,
	+1.0f, -1.0f, +1.0f, +1.0f,

	+1.0f, +1.0f, -1.0f, +1.0f,
	+1.0f, +1.0f, +1.0f, +1.0f,
	+1.0f, -1.0f, +1.0f, +1.0f,
	+1.0f, -1.0f, -1.0f, +1.0f,

	-1.0f, -1.0f, +1.0f, +1.0f,
	-1.0f, +1.0f, +1.0f, +1.0f,
	-1.0f, +1.0f, -1.0f, +1.0f,
	-1.0f, -1.0f, -1.0f, +1.0f,
};
const f32*	CShapeUtility::GetCubeVtxPos()
{
	return s_vtxPos;
}

///-------------------------------------------------------------------------------------------------
/// 箱の法線
const f32*	CShapeUtility::GetCubeVtxNrm()
{
	static f32 s_vtxNrm[] =
	{
		+0.0f, +0.0f, +1.0f,
		+0.0f, +0.0f, +1.0f,
		+0.0f, +0.0f, +1.0f,
		+0.0f, +0.0f, +1.0f,

		+0.0f, +0.0f, -1.0f,
		+0.0f, +0.0f, -1.0f,
		+0.0f, +0.0f, -1.0f,
		+0.0f, +0.0f, -1.0f,

		+0.0f, +1.0f, +0.0f,
		+0.0f, +1.0f, +0.0f,
		+0.0f, +1.0f, +0.0f,
		+0.0f, +1.0f, +0.0f,

		+0.0f, -1.0f, +0.0f,
		+0.0f, -1.0f, +0.0f,
		+0.0f, -1.0f, +0.0f,
		+0.0f, -1.0f, +0.0f,

		+1.0f, +0.0f, +0.0f,
		+1.0f, +0.0f, +0.0f,
		+1.0f, +0.0f, +0.0f,
		+1.0f, +0.0f, +0.0f,

		-1.0f, +0.0f, +0.0f,
		-1.0f, +0.0f, +0.0f,
		-1.0f, +0.0f, +0.0f,
		-1.0f, +0.0f, +0.0f,
	};
	return s_vtxNrm;
}

///-------------------------------------------------------------------------------------------------
/// 箱の色
const f32*	CShapeUtility::GetCubeVtxClr()
{
	static f32 s_vtxClr[] =
	{
		+1.0f, +1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f, +1.0f,

		+1.0f, +1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f, +1.0f,

		+1.0f, +1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f, +1.0f,

		+1.0f, +1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f, +1.0f,

		+1.0f, +1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f, +1.0f,

		+1.0f, +1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f, +1.0f,
	};
	return s_vtxClr;
}

///-------------------------------------------------------------------------------------------------
/// 箱のUV
const f32*	CShapeUtility::GetCubeVtxUv()
{
	static f32 s_vtxUv[] =
	{
		1.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 1.0f,
		1.0f, 1.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		1.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 1.0f,
		1.0f, 1.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		1.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 1.0f,
		1.0f, 1.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
	};
	return s_vtxUv;
}

///-------------------------------------------------------------------------------------------------
// 箱のUV
static const u32 s_idx[] =
{
	0 + 0,	1 + 0,	2 + 0,	2 + 0,	3 + 0,	0 + 0,
	0 + 4,	1 + 4,	2 + 4,	2 + 4,	3 + 4,	0 + 4,
	0 + 8,	1 + 8,	2 + 8,	2 + 8,	3 + 8,	0 + 8,
	0 + 12, 1 + 12, 2 + 12, 2 + 12, 3 + 12, 0 + 12,
	0 + 16, 1 + 16, 2 + 16, 2 + 16, 3 + 16, 0 + 16,
	0 + 20, 1 + 20, 2 + 20, 2 + 20, 3 + 20, 0 + 20,
};
const u32*	CShapeUtility::GetCubeIdx()
{
	return s_idx;
}

///-------------------------------------------------------------------------------------------------
// 箱の頂点数
const u32	CShapeUtility::GetCubeVtxNum()
{
	return ARRAYOF(s_vtxPos) / 4;
}
///-------------------------------------------------------------------------------------------------
// 箱のインデックス数
const u32	CShapeUtility::GetCubeIdxNum()
{
	return ARRAYOF(s_idx);
}


///*************************************************************************************************
///	球体
///*************************************************************************************************

#define PRIMITEVE_SPHERE_AZIMUTH_NUM	16	// 経度分割数
#define PRIMITEVE_SPHERE_ALTITUDE_NUM	8	// 緯度分割数
const static u32 s_sphereVtxNum = PRIMITEVE_SPHERE_AZIMUTH_NUM * (PRIMITEVE_SPHERE_ALTITUDE_NUM - 1) + 2;
static const CSpc*	GetSphereSpc()
{
	static CSpc s_spcSphere[s_sphereVtxNum];
	StaticAssert(PRIMITEVE_SPHERE_AZIMUTH_NUM > 1 && PRIMITEVE_SPHERE_ALTITUDE_NUM > 1);
	static b8 s_isInit = false;
	if (!s_isInit)
	{
		u32 idx = 0;
		s_spcSphere[idx].SetAzimuth(0.0f);
		s_spcSphere[idx].SetAltitude(toRadian(-90.0f));
		s_spcSphere[idx].SetRadius(1.0f);
		++idx;
		for (u32 i = 0; i < PRIMITEVE_SPHERE_ALTITUDE_NUM - 1; ++i)
		{
			f32 altitude = toRadian(-90.0f) + (SCast<f32>(i + 1) / PRIMITEVE_SPHERE_ALTITUDE_NUM) * PI;
			for (u32 j = 0; j < PRIMITEVE_SPHERE_AZIMUTH_NUM; ++j)
			{
				f32 azimuth = (SCast<f32>(j) / PRIMITEVE_SPHERE_AZIMUTH_NUM) * PI * 2.0f;
				s_spcSphere[idx].SetAzimuth(azimuth);
				s_spcSphere[idx].SetAltitude(altitude);
				s_spcSphere[idx].SetRadius(1.0f);
				++idx;
			}
		}
		s_spcSphere[idx].SetAzimuth(0.0f);
		s_spcSphere[idx].SetAltitude(toRadian(+90.0f));
		s_spcSphere[idx].SetRadius(1.0f);
		++idx;
		Assert(idx == s_sphereVtxNum);
		s_isInit = true;
	}
	return s_spcSphere;
}

///-------------------------------------------------------------------------------------------------
// 球の座標(4要素)
const f32*	CShapeUtility::GetSphereVtxPos()
{
	static f32 s_vtxPos[s_sphereVtxNum * 4];
	static b8 s_isInit = false;
	if (!s_isInit)
	{
		for (u32 i = 0; i < s_sphereVtxNum; ++i)
		{
			CVec4 pos;
			CSpc::SpcToVecZ(pos.GetXYZ(), GetSphereSpc()[i]);
			pos[3] = 1.0f;
			for (u32 j = 0; j < 4; ++j)
			{
				s_vtxPos[4 * i + j] = pos[j];
			}
		}
		s_isInit = true;
	}
	return s_vtxPos;
}
///-------------------------------------------------------------------------------------------------
// 球の法線(3要素)
const f32*	CShapeUtility::GetSphereVtxNrm()
{
	static f32 s_vtxNrm[s_sphereVtxNum * 3];
	static b8 s_isInit = false;
	if (!s_isInit)
	{
		for (u32 i = 0; i < s_sphereVtxNum; ++i)
		{
			CVec3 pos;
			CSpc::SpcToVecZ(pos, GetSphereSpc()[i]);
			pos[3] = 1.0f;
			for (u32 j = 0; j < 3; ++j)
			{
				s_vtxNrm[3 * i + j] = pos[j];
			}
		}
		s_isInit = true;
	}
	return s_vtxNrm;
}
///-------------------------------------------------------------------------------------------------
// 球の色(4要素)
const f32*	CShapeUtility::GetSphereVtxClr()
{
	static f32 s_clr[s_sphereVtxNum * 4];
	static b8 s_isInit = false;
	if (!s_isInit)
	{
		for (u32 i = 0; i < s_sphereVtxNum * 4; i++)
		{
			s_clr[i] = 1.0f;
		}
		s_isInit = true;
	}
	return s_clr;
}
///-------------------------------------------------------------------------------------------------
// 球のUV(2要素)
const f32*	CShapeUtility::GetSphereVtxUv()
{
	auto azimuthUv = [](const CSpc& _spc)->f32
	{
		return (_spc.GetAzimuth() / PI) * 0.5f + 0.5f;
	};
	auto altitudeUv = [](const CSpc& _spc)->f32
	{
		return (2.0f * _spc.GetAltitude() / PI) * 0.5f + 0.5f;
	};
	static f32 s_vtxUv[s_sphereVtxNum * 2];
	static b8 s_isInit = false;
	if (!s_isInit)
	{
		for (u32 i = 0; i < s_sphereVtxNum; ++i)
		{
			s_vtxUv[2 * i + 0] = azimuthUv(GetSphereSpc()[i]);
			s_vtxUv[2 * i + 1] = altitudeUv(GetSphereSpc()[i]);
		}
		s_isInit = true;
	}
	return s_vtxUv;
}
///-------------------------------------------------------------------------------------------------
// 球のIDX
static const u32 s_sphereIdxNum = (PRIMITEVE_SPHERE_AZIMUTH_NUM * 3 * 2) + ((PRIMITEVE_SPHERE_ALTITUDE_NUM - 2) * PRIMITEVE_SPHERE_AZIMUTH_NUM * 6);
const u32*	CShapeUtility::GetSphereIdx()
{
	static u32 s_vtxIdx[s_sphereIdxNum];
	static b8 s_isInit = false;
	if (!s_isInit)
	{
		u32 idx = 0;
		for (u32 i = 0; i < PRIMITEVE_SPHERE_AZIMUTH_NUM; ++i)
		{
			s_vtxIdx[idx + 0] = 0;
			s_vtxIdx[idx + 1] = 1 + ((i + 0) % PRIMITEVE_SPHERE_AZIMUTH_NUM);
			s_vtxIdx[idx + 2] = 1 + ((i + 1) % PRIMITEVE_SPHERE_AZIMUTH_NUM);
			idx += 3;
		}

		u32 vtxIdx = 1;
		for (u32 i = 0; i < PRIMITEVE_SPHERE_ALTITUDE_NUM - 2; ++i)
		{
			for (u32 j = 0; j < PRIMITEVE_SPHERE_AZIMUTH_NUM; ++j)
			{
				auto calcIdx = [](u32 _x, u32 _y, u32 _startIdx, u32 _division)->u32
				{
					u32 index = _startIdx + (_x % _division) + _y * _division;
					Assert(index < s_sphereVtxNum);
					return index;
				};
				s_vtxIdx[idx + 0] = calcIdx(j + 0, 0, vtxIdx, PRIMITEVE_SPHERE_AZIMUTH_NUM);
				s_vtxIdx[idx + 1] = calcIdx(j + 0, 1, vtxIdx, PRIMITEVE_SPHERE_AZIMUTH_NUM);
				s_vtxIdx[idx + 2] = calcIdx(j + 1, 1, vtxIdx, PRIMITEVE_SPHERE_AZIMUTH_NUM);
				s_vtxIdx[idx + 3] = calcIdx(j + 1, 1, vtxIdx, PRIMITEVE_SPHERE_AZIMUTH_NUM);
				s_vtxIdx[idx + 4] = calcIdx(j + 1, 0, vtxIdx, PRIMITEVE_SPHERE_AZIMUTH_NUM);
				s_vtxIdx[idx + 5] = calcIdx(j + 0, 0, vtxIdx, PRIMITEVE_SPHERE_AZIMUTH_NUM);
				idx += 6;
			}
			vtxIdx += PRIMITEVE_SPHERE_AZIMUTH_NUM;
		}

		for (u32 i = 0; i < PRIMITEVE_SPHERE_AZIMUTH_NUM; ++i)
		{
			u32 lastIdx = s_sphereVtxNum - PRIMITEVE_SPHERE_AZIMUTH_NUM - 1;
			s_vtxIdx[idx + 0] = s_sphereVtxNum - 1;
			s_vtxIdx[idx + 1] = lastIdx + ((i + 1) % PRIMITEVE_SPHERE_AZIMUTH_NUM);
			s_vtxIdx[idx + 2] = lastIdx + ((i + 0) % PRIMITEVE_SPHERE_AZIMUTH_NUM);
			idx += 3;
		}
		Assert(idx == s_sphereIdxNum);
		s_isInit = true;
	}
	return s_vtxIdx;
}
///-------------------------------------------------------------------------------------------------
// 球の頂点数
const u32	CShapeUtility::GetSphereVtxNum()
{
	return s_sphereVtxNum;
}
///-------------------------------------------------------------------------------------------------
// 球のインデックス数
const u32	CShapeUtility::GetSphereIdxNum()
{
	return s_sphereIdxNum;
}


///*************************************************************************************************
///	正二十面体
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
// 正二十面体の座標(4要素)
static f32 s_lcosahedralEdgeLen = 4.0f / sqrtf(10.0f + 2.0f * PM_SQRT5);
static f32 s_lcosahedralAltitudeRot = 2.0f * asinf(s_lcosahedralEdgeLen / 2.0f);
static f32 s_lcosahedralAzimuthRot = Math::ToRadians(72.0f);
static CSpc s_lcosahedralSpc00(1.0f, toRadian(0.0f), toRadian(-90.0f));
static CSpc s_lcosahedralSpc01(1.0f, toRadian(0.0f) + s_lcosahedralAzimuthRot * 0.0f, toRadian(-90.0f) + s_lcosahedralAltitudeRot);
static CSpc s_lcosahedralSpc02(1.0f, toRadian(0.0f) + s_lcosahedralAzimuthRot * 1.0f, toRadian(-90.0f) + s_lcosahedralAltitudeRot);
static CSpc s_lcosahedralSpc03(1.0f, toRadian(0.0f) + s_lcosahedralAzimuthRot * 2.0f, toRadian(-90.0f) + s_lcosahedralAltitudeRot);
static CSpc s_lcosahedralSpc04(1.0f, toRadian(0.0f) + s_lcosahedralAzimuthRot * 3.0f, toRadian(-90.0f) + s_lcosahedralAltitudeRot);
static CSpc s_lcosahedralSpc05(1.0f, toRadian(0.0f) + s_lcosahedralAzimuthRot * 4.0f, toRadian(-90.0f) + s_lcosahedralAltitudeRot);
static CSpc s_lcosahedralSpc06(1.0f, toRadian(180.0f), toRadian(90.0f));
static CSpc s_lcosahedralSpc07(1.0f, toRadian(180.0f) + s_lcosahedralAzimuthRot * 0.0f, toRadian(90.0f) - s_lcosahedralAltitudeRot);
static CSpc s_lcosahedralSpc08(1.0f, toRadian(180.0f) + s_lcosahedralAzimuthRot * 1.0f, toRadian(90.0f) - s_lcosahedralAltitudeRot);
static CSpc s_lcosahedralSpc09(1.0f, toRadian(180.0f) + s_lcosahedralAzimuthRot * 2.0f, toRadian(90.0f) - s_lcosahedralAltitudeRot);
static CSpc s_lcosahedralSpc10(1.0f, toRadian(180.0f) + s_lcosahedralAzimuthRot * 3.0f, toRadian(90.0f) - s_lcosahedralAltitudeRot);
static CSpc s_lcosahedralSpc11(1.0f, toRadian(180.0f) + s_lcosahedralAzimuthRot * 4.0f, toRadian(90.0f) - s_lcosahedralAltitudeRot);
const f32*	CShapeUtility::GetLcosahedralVtxPos()
{
	static CVec3 s_pos00, s_pos01, s_pos02, s_pos03, s_pos04, s_pos05, s_pos06, s_pos07, s_pos08, s_pos09, s_pos10, s_pos11;
	CSpc::SpcToVecZ(s_pos00, s_lcosahedralSpc00);
	CSpc::SpcToVecZ(s_pos01, s_lcosahedralSpc01);
	CSpc::SpcToVecZ(s_pos02, s_lcosahedralSpc02);
	CSpc::SpcToVecZ(s_pos03, s_lcosahedralSpc03);
	CSpc::SpcToVecZ(s_pos04, s_lcosahedralSpc04);
	CSpc::SpcToVecZ(s_pos05, s_lcosahedralSpc05);
	CSpc::SpcToVecZ(s_pos06, s_lcosahedralSpc06);
	CSpc::SpcToVecZ(s_pos07, s_lcosahedralSpc07);
	CSpc::SpcToVecZ(s_pos08, s_lcosahedralSpc08);
	CSpc::SpcToVecZ(s_pos09, s_lcosahedralSpc09);
	CSpc::SpcToVecZ(s_pos10, s_lcosahedralSpc10);
	CSpc::SpcToVecZ(s_pos11, s_lcosahedralSpc11);
	static f32 s_vtxPos[] =
	{
		s_pos00.x, s_pos00.y, s_pos00.z, 1.0f, s_pos01.x, s_pos01.y, s_pos01.z, 1.0f, s_pos02.x, s_pos02.y, s_pos02.z, 1.0f,
		s_pos00.x, s_pos00.y, s_pos00.z, 1.0f, s_pos02.x, s_pos02.y, s_pos02.z, 1.0f, s_pos03.x, s_pos03.y, s_pos03.z, 1.0f,
		s_pos00.x, s_pos00.y, s_pos00.z, 1.0f, s_pos03.x, s_pos03.y, s_pos03.z, 1.0f, s_pos04.x, s_pos04.y, s_pos04.z, 1.0f,
		s_pos00.x, s_pos00.y, s_pos00.z, 1.0f, s_pos04.x, s_pos04.y, s_pos04.z, 1.0f, s_pos05.x, s_pos05.y, s_pos05.z, 1.0f,
		s_pos00.x, s_pos00.y, s_pos00.z, 1.0f, s_pos05.x, s_pos05.y, s_pos05.z, 1.0f, s_pos01.x, s_pos01.y, s_pos01.z, 1.0f,

		s_pos06.x, s_pos06.y, s_pos06.z, 1.0f, s_pos08.x, s_pos08.y, s_pos08.z, 1.0f, s_pos07.x, s_pos07.y, s_pos07.z, 1.0f,
		s_pos06.x, s_pos06.y, s_pos06.z, 1.0f, s_pos09.x, s_pos09.y, s_pos09.z, 1.0f, s_pos08.x, s_pos08.y, s_pos08.z, 1.0f,
		s_pos06.x, s_pos06.y, s_pos06.z, 1.0f, s_pos10.x, s_pos10.y, s_pos10.z, 1.0f, s_pos09.x, s_pos09.y, s_pos09.z, 1.0f,
		s_pos06.x, s_pos06.y, s_pos06.z, 1.0f, s_pos11.x, s_pos11.y, s_pos11.z, 1.0f, s_pos10.x, s_pos10.y, s_pos10.z, 1.0f,
		s_pos06.x, s_pos06.y, s_pos06.z, 1.0f, s_pos07.x, s_pos07.y, s_pos07.z, 1.0f, s_pos11.x, s_pos11.y, s_pos11.z, 1.0f,

		s_pos01.x, s_pos01.y, s_pos01.z, 1.0f, s_pos09.x, s_pos09.y, s_pos09.z, 1.0f, s_pos10.x, s_pos10.y, s_pos10.z, 1.0f,
		s_pos02.x, s_pos02.y, s_pos02.z, 1.0f, s_pos10.x, s_pos10.y, s_pos10.z, 1.0f, s_pos11.x, s_pos11.y, s_pos11.z, 1.0f,
		s_pos03.x, s_pos03.y, s_pos03.z, 1.0f, s_pos11.x, s_pos11.y, s_pos11.z, 1.0f, s_pos07.x, s_pos07.y, s_pos07.z, 1.0f,
		s_pos04.x, s_pos04.y, s_pos04.z, 1.0f, s_pos07.x, s_pos07.y, s_pos07.z, 1.0f, s_pos08.x, s_pos08.y, s_pos08.z, 1.0f,
		s_pos05.x, s_pos05.y, s_pos05.z, 1.0f, s_pos08.x, s_pos08.y, s_pos08.z, 1.0f, s_pos09.x, s_pos09.y, s_pos09.z, 1.0f,

		s_pos07.x, s_pos07.y, s_pos07.z, 1.0f, s_pos04.x, s_pos04.y, s_pos04.z, 1.0f, s_pos03.x, s_pos03.y, s_pos03.z, 1.0f,
		s_pos08.x, s_pos08.y, s_pos08.z, 1.0f, s_pos05.x, s_pos05.y, s_pos05.z, 1.0f, s_pos04.x, s_pos04.y, s_pos04.z, 1.0f,
		s_pos09.x, s_pos09.y, s_pos09.z, 1.0f, s_pos01.x, s_pos01.y, s_pos01.z, 1.0f, s_pos05.x, s_pos05.y, s_pos05.z, 1.0f,
		s_pos10.x, s_pos10.y, s_pos10.z, 1.0f, s_pos02.x, s_pos02.y, s_pos02.z, 1.0f, s_pos01.x, s_pos01.y, s_pos01.z, 1.0f,
		s_pos11.x, s_pos11.y, s_pos11.z, 1.0f, s_pos03.x, s_pos03.y, s_pos03.z, 1.0f, s_pos02.x, s_pos02.y, s_pos02.z, 1.0f,
	};
	return s_vtxPos;
}
///-------------------------------------------------------------------------------------------------
// 正二十面体の法線(3要素)
const f32*	CShapeUtility::GetLcosahedralVtxNrm()
{
	u32 cnt = 0;
	static CVec3 s_nrm[] =
	{
		(CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++])) / 3.0f,
		(CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++])) / 3.0f,
		(CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++])) / 3.0f,
		(CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++])) / 3.0f,
		(CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++])) / 3.0f,
		(CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++])) / 3.0f,
		(CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++])) / 3.0f,
		(CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++])) / 3.0f,
		(CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++])) / 3.0f,
		(CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++])) / 3.0f,
		(CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++])) / 3.0f,
		(CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++])) / 3.0f,
		(CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++])) / 3.0f,
		(CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++])) / 3.0f,
		(CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++])) / 3.0f,
		(CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++])) / 3.0f,
		(CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++])) / 3.0f,
		(CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++])) / 3.0f,
		(CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++])) / 3.0f,
		(CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++]) + CVec3(&GetLcosahedralVtxPos()[4 * cnt++])) / 3.0f,
	};
	cnt = 0;
	static f32 s_vtxNrm[] =
	{
		s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt++].z,
		s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt++].z,
		s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt++].z,
		s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt++].z,
		s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt++].z,
		s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt++].z,
		s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt++].z,
		s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt++].z,
		s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt++].z,
		s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt++].z,
		s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt++].z,
		s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt++].z,
		s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt++].z,
		s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt++].z,
		s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt++].z,
		s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt++].z,
		s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt++].z,
		s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt++].z,
		s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt++].z,
		s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt].z, s_nrm[cnt].x, s_nrm[cnt].y, s_nrm[cnt++].z,
	};
	return s_vtxNrm;
}
///-------------------------------------------------------------------------------------------------
// 正二十面体の色(4要素)
const f32*	CShapeUtility::GetLcosahedralVtxClr()
{
	static f32 s_vtxClr[] =
	{
		1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	};
	return s_vtxClr;
}
///-------------------------------------------------------------------------------------------------
// 正二十面体のUV(2要素)
const f32*	CShapeUtility::GetLcosahedralVtxUv()
{
	auto azimuthUv = [](const CSpc& _spc)->f32
	{
		return (_spc.GetAzimuth() / PI) * 0.5f + 0.5f;
	};
	auto altitudeUv = [](const CSpc& _spc)->f32
	{
		return (2.0f * _spc.GetAltitude() / PI) * 0.5f + 0.5f;
	};
	static f32 s_vtxUv[] =
	{
		azimuthUv(s_lcosahedralSpc00), altitudeUv(s_lcosahedralSpc00), azimuthUv(s_lcosahedralSpc01), altitudeUv(s_lcosahedralSpc01), azimuthUv(s_lcosahedralSpc02), altitudeUv(s_lcosahedralSpc02),
		azimuthUv(s_lcosahedralSpc00), altitudeUv(s_lcosahedralSpc00), azimuthUv(s_lcosahedralSpc02), altitudeUv(s_lcosahedralSpc02), azimuthUv(s_lcosahedralSpc03), altitudeUv(s_lcosahedralSpc03),
		azimuthUv(s_lcosahedralSpc00), altitudeUv(s_lcosahedralSpc00), azimuthUv(s_lcosahedralSpc03), altitudeUv(s_lcosahedralSpc03), azimuthUv(s_lcosahedralSpc04), altitudeUv(s_lcosahedralSpc04),
		azimuthUv(s_lcosahedralSpc00), altitudeUv(s_lcosahedralSpc00), azimuthUv(s_lcosahedralSpc04), altitudeUv(s_lcosahedralSpc04), azimuthUv(s_lcosahedralSpc05), altitudeUv(s_lcosahedralSpc05),
		azimuthUv(s_lcosahedralSpc00), altitudeUv(s_lcosahedralSpc00), azimuthUv(s_lcosahedralSpc05), altitudeUv(s_lcosahedralSpc05), azimuthUv(s_lcosahedralSpc01), altitudeUv(s_lcosahedralSpc01),

		azimuthUv(s_lcosahedralSpc06), altitudeUv(s_lcosahedralSpc06), azimuthUv(s_lcosahedralSpc08), altitudeUv(s_lcosahedralSpc08), azimuthUv(s_lcosahedralSpc07), altitudeUv(s_lcosahedralSpc07),
		azimuthUv(s_lcosahedralSpc06), altitudeUv(s_lcosahedralSpc06), azimuthUv(s_lcosahedralSpc09), altitudeUv(s_lcosahedralSpc09), azimuthUv(s_lcosahedralSpc08), altitudeUv(s_lcosahedralSpc08),
		azimuthUv(s_lcosahedralSpc06), altitudeUv(s_lcosahedralSpc06), azimuthUv(s_lcosahedralSpc10), altitudeUv(s_lcosahedralSpc10), azimuthUv(s_lcosahedralSpc09), altitudeUv(s_lcosahedralSpc09),
		azimuthUv(s_lcosahedralSpc06), altitudeUv(s_lcosahedralSpc06), azimuthUv(s_lcosahedralSpc11), altitudeUv(s_lcosahedralSpc11), azimuthUv(s_lcosahedralSpc10), altitudeUv(s_lcosahedralSpc10),
		azimuthUv(s_lcosahedralSpc06), altitudeUv(s_lcosahedralSpc06), azimuthUv(s_lcosahedralSpc07), altitudeUv(s_lcosahedralSpc07), azimuthUv(s_lcosahedralSpc11), altitudeUv(s_lcosahedralSpc11),

		azimuthUv(s_lcosahedralSpc01), altitudeUv(s_lcosahedralSpc01), azimuthUv(s_lcosahedralSpc09), altitudeUv(s_lcosahedralSpc09), azimuthUv(s_lcosahedralSpc10), altitudeUv(s_lcosahedralSpc10),
		azimuthUv(s_lcosahedralSpc02), altitudeUv(s_lcosahedralSpc02), azimuthUv(s_lcosahedralSpc10), altitudeUv(s_lcosahedralSpc10), azimuthUv(s_lcosahedralSpc11), altitudeUv(s_lcosahedralSpc11),
		azimuthUv(s_lcosahedralSpc03), altitudeUv(s_lcosahedralSpc03), azimuthUv(s_lcosahedralSpc11), altitudeUv(s_lcosahedralSpc11), azimuthUv(s_lcosahedralSpc07), altitudeUv(s_lcosahedralSpc07),
		azimuthUv(s_lcosahedralSpc04), altitudeUv(s_lcosahedralSpc04), azimuthUv(s_lcosahedralSpc07), altitudeUv(s_lcosahedralSpc07), azimuthUv(s_lcosahedralSpc08), altitudeUv(s_lcosahedralSpc08),
		azimuthUv(s_lcosahedralSpc05), altitudeUv(s_lcosahedralSpc05), azimuthUv(s_lcosahedralSpc08), altitudeUv(s_lcosahedralSpc08), azimuthUv(s_lcosahedralSpc09), altitudeUv(s_lcosahedralSpc09),

		azimuthUv(s_lcosahedralSpc07), altitudeUv(s_lcosahedralSpc07), azimuthUv(s_lcosahedralSpc04), altitudeUv(s_lcosahedralSpc04), azimuthUv(s_lcosahedralSpc03), altitudeUv(s_lcosahedralSpc03),
		azimuthUv(s_lcosahedralSpc08), altitudeUv(s_lcosahedralSpc08), azimuthUv(s_lcosahedralSpc05), altitudeUv(s_lcosahedralSpc05), azimuthUv(s_lcosahedralSpc04), altitudeUv(s_lcosahedralSpc04),
		azimuthUv(s_lcosahedralSpc09), altitudeUv(s_lcosahedralSpc09), azimuthUv(s_lcosahedralSpc01), altitudeUv(s_lcosahedralSpc01), azimuthUv(s_lcosahedralSpc05), altitudeUv(s_lcosahedralSpc05),
		azimuthUv(s_lcosahedralSpc10), altitudeUv(s_lcosahedralSpc10), azimuthUv(s_lcosahedralSpc02), altitudeUv(s_lcosahedralSpc02), azimuthUv(s_lcosahedralSpc01), altitudeUv(s_lcosahedralSpc01),
		azimuthUv(s_lcosahedralSpc11), altitudeUv(s_lcosahedralSpc11), azimuthUv(s_lcosahedralSpc03), altitudeUv(s_lcosahedralSpc03), azimuthUv(s_lcosahedralSpc02), altitudeUv(s_lcosahedralSpc02),
	};
	return s_vtxUv;
}
///-------------------------------------------------------------------------------------------------
// 正二十面体のIDX
const u32*	CShapeUtility::GetLcosahedralIdx()
{
	u32 cnt = 0;
	static u32 s_vtxIdx[] =
	{
		cnt++, cnt++, cnt++,
		cnt++, cnt++, cnt++,
		cnt++, cnt++, cnt++,
		cnt++, cnt++, cnt++,
		cnt++, cnt++, cnt++,

		cnt++, cnt++, cnt++,
		cnt++, cnt++, cnt++,
		cnt++, cnt++, cnt++,
		cnt++, cnt++, cnt++,
		cnt++, cnt++, cnt++,

		cnt++, cnt++, cnt++,
		cnt++, cnt++, cnt++,
		cnt++, cnt++, cnt++,
		cnt++, cnt++, cnt++,
		cnt++, cnt++, cnt++,

		cnt++, cnt++, cnt++,
		cnt++, cnt++, cnt++,
		cnt++, cnt++, cnt++,
		cnt++, cnt++, cnt++,
		cnt++, cnt++, cnt++,
	};
	return s_vtxIdx;
}


WORK_NAMESPACE_END

﻿//#pragma once
#ifndef __SYSTEM_GLOBAL_PARAM_H__
#define __SYSTEM_GLOBAL_PARAM_H__

//-------------------------------------------------------------------------------------------------
// include
#include "main.h"

//-------------------------------------------------------------------------------------------------
// prototype


WORK_NAMESPACE_BGN


//*************************************************************************************************
//@brief	キャンパス宣言構造体
//*************************************************************************************************
STATIC_SINGLETON(CSystemGlobalParam)
{
public:
	FRIEND_STATIC_SINGLETON(CSystemGlobalParam);

public:
	u32				m_syncInterval;
	CVec4			m_windowClearColor;
	CVec4			m_displayClearColor;
	SCREEN_ALIGN	m_align;
	SCREEN_STRETCH	m_stretch;
	u16				m_displaySize[2];
	u16				m_displayAspect[2];
	b8				m_fixAspect;

public:
	CSystemGlobalParam()
		: m_syncInterval(0)
		, m_windowClearColor(0.0f, 0.0f, 0.0f, 0.0f)
		, m_displayClearColor(0.45f, 0.55f, 0.60f, 1.00f)
		, m_align(SCREEN_ALIGN_CENTER_MIDDLE)
		, m_stretch(SCREEN_STRETCH_FIT)
		, m_displaySize{ DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT }
		, m_displayAspect{ 16, 9 }
		, m_fixAspect(true)
	{}
	~CSystemGlobalParam() {}


};



WORK_NAMESPACE_END

#endif	// __SYSTEM_GLOBAL_PARAM_H__

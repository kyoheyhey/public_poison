﻿//#pragma once
#ifndef __DEBUG_WIN_H__
#define __DEBUG_WIN_H__


WORK_NAMESPACE_BGN
//-------------------------------------------------------------------------------------------------
// prototype
class CDebugWinManager;
class CDebugWin;



//*************************************************************************************************
//@brief	デバッグウィンドウクリエイター
//*************************************************************************************************
class CDebugWinCreater
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	デバッグウィンドウ生成
	static CDebugWin*	Create(MEM_HANDLE _memHdl, hash32 _classNameCrc);

	//-------------------------------------------------------------------------------------------------
	//@brief	デバッグウィンドウ破棄
	static void			Release(MEM_HANDLE _memHdl, CDebugWin** _ppDebugWin);

protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ・デストラクタ
	CDebugWinCreater(const c8* _className);
	virtual ~CDebugWinCreater();

	//-------------------------------------------------------------------------------------------------
	//@brief	デバッグウィンドウ生成
	virtual CDebugWin*	Create(MEM_HANDLE _memHdl) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	取得
	inline const CDebugWinCreater*	GetNodePrev() const { return m_pPrev; }
	inline CDebugWinCreater*		GetNodePrev() { return m_pPrev; }
	inline const CDebugWinCreater*	GetNodeNext() const { return m_pNext; }
	inline CDebugWinCreater*		GetNodeNext() { return m_pNext; }

	//-------------------------------------------------------------------------------------------------
	//@brief	静的取得
	inline static CDebugWinCreater*	GetNodeTop() { return s_pTop; }
	inline static CDebugWinCreater*	GetNodeEnd() { return s_pEnd; }
	inline static u32				GetNodeNum() { return s_num; }

private:
	//@brief	コピーコンストラクタ(使用不可)
	CDebugWinCreater(const CDebugWinCreater& _node);
	//@brief	コピーオペレーター(使用不可)
	const CDebugWinCreater& operator=(const CDebugWinCreater& _node);

	//-------------------------------------------------------------------------------------------------
	//@brief	設定
	inline void	SetNodePrev(CDebugWinCreater* _pNode) { m_pPrev = _pNode; }
	inline void	SetNodeNext(CDebugWinCreater* _pNode) { m_pNext = _pNode; }

	//-------------------------------------------------------------------------------------------------
	//@brief	追加
	inline void	AddNodePrev(CDebugWinCreater* _pNode) { m_pPrev = _pNode; _pNode->m_pNext = SCast<CDebugWinCreater*>(this); }
	inline void	AddNodeNext(CDebugWinCreater* _pNode) { m_pNext = _pNode; _pNode->m_pPrev = SCast<CDebugWinCreater*>(this); }

	//-------------------------------------------------------------------------------------------------
	//@brief	先頭に追加
	void		AddNodeTop();
	//-------------------------------------------------------------------------------------------------
	//@brief	末尾に追加
	void		AddNodeEnd();
	//-------------------------------------------------------------------------------------------------
	//@brief	ノード削除
	void		DeleteNode();

protected:
	CDebugWinCreater*		m_pPrev = NULL;	//< 前
	CDebugWinCreater*		m_pNext = NULL;	//< 後

	const c8*				m_name = NULL;
	hash32					m_nameCrc = hash32::Invalid;

private:
	static CDebugWinCreater*	s_pTop;		//< 先頭ノード
	static CDebugWinCreater*	s_pEnd;		//< 末尾ノード
	static u32					s_num;		//< ノード数
};


//*************************************************************************************************
//@brief	テンプレートデバッグウィンドウクリエイター
//*************************************************************************************************
template <typename __DBG_WIN>
class TDebugWinCreater : public CDebugWinCreater
{
private:
	typedef __DBG_WIN		DBG_WIN;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ・デストラクタ
	TDebugWinCreater(const c8* _className)
		: CDebugWinCreater(_className)
	{}
	virtual ~TDebugWinCreater() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	デバッグウィンドウ生成
	virtual CDebugWin*	Create(MEM_HANDLE _memHdl)
	{
		DBG_WIN* pDebWin =  MEM_NEW(_memHdl) DBG_WIN;
		libAssert(pDebWin);
		return pDebWin;
	}
};

//*************************************************************************************************
//@brief	デバッグウィンドウを登録
#define REGIST_DEBUG_WIN( __debugWinClass )		static TDebugWinCreater< __debugWinClass >	s_regist##__debugWinClass( #__debugWinClass )



//*************************************************************************************************
//@brief	デバッグウィンドウ
//*************************************************************************************************
LINK_CLASS_BASE( CDebugWin, CDontCopy )
{
public:
	friend class CDebugWinManager;

public:
	CDebugWin();
	virtual ~CDebugWin();

	//-------------------------------------------------------------------------------------------------
	///@brief	有効フラグ取得
	b8			IsEnable() const { return m_enable; }
	///@brief	有効フラグ設定
	void		SetEnable(b8 _enable) { m_enable = _enable; }

	//-------------------------------------------------------------------------------------------------
	///@brief	ウィンドウ名取得
	const c8*	GetName() const { return m_name; }
	///@brief	ウィンドウ名設定
	void		SetName(const c8* _name);

	//-------------------------------------------------------------------------------------------------
	///@brief	ウィンドウCRC名取得
	hash32		GetNameCrc() const { return m_mameCrc; }

	//-------------------------------------------------------------------------------------------------
	///@brief	フラグ設定
	void		SetFlags(const u32 _flags) { m_flags = _flags; }

public:
	//-------------------------------------------------------------------------------------------------
	///@brief	更新
	virtual void	Step() {}

	//-------------------------------------------------------------------------------------------------
	///@brief	デバッグウインドウマネージャに登録コールバック
	virtual void	OnRegist() {}

	//-------------------------------------------------------------------------------------------------
	///@brief	デバッグウインドウマネージャに解除コールバック
	virtual void	OnUnregist() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	アクディブコールバック
	virtual void	OnActivate() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	ディアクティブコールバック
	virtual void	OnDeactivate() {}

private:
	//-------------------------------------------------------------------------------------------------
	///@brief	更新開始
	void	BeginStep();

	//-------------------------------------------------------------------------------------------------
	///@brief	更新終了
	void	EndStep();

private:
	c8		m_name[64] = {};		///< ウィンドウ名
	hash32	m_mameCrc = hash32::Invalid;
	u32		m_flags = 0;			///< ウィンドウフラグ
	b8		m_enable = false;		///< 有効フラグ
	b8		m_prevEnable = false;	///< 前回有効フラグ
};


WORK_NAMESPACE_END



#endif	// __DEBUG_WIN_H__

﻿//#pragma once
#ifndef __DEBUG_WIN_MANAGER_H__
#define __DEBUG_WIN_MANAGER_H__


//-------------------------------------------------------------------------------------------------
// include


WORK_NAMESPACE_BGN
//-------------------------------------------------------------------------------------------------
// prototype
class CDebugWin;


//*************************************************************************************************
//@brief	デバッグウィンドウマネージャー
//*************************************************************************************************
class CDebugWinManager : public CDontCopy
{
public:
	//*************************************************************************************************
	//@brief	メニューバー
	//*************************************************************************************************
	struct MENU_BER;
	typedef CLinkList<MENU_BER*>	MENU_BER_LIST;
	struct MENU_BER
	{
		CDebugWin*		pDebugWin = NULL;
		c8				name[32] = {};
		hash32			nameCrc = hash32::Invalid;

		MENU_BER*		pParent = NULL;
		MENU_BER_LIST	child;

		//-------------------------------------------------------------------------------------------------
		//@brief	初期化
		b8		Initialize(MEM_HANDLE _memHdl, const c8* _name, CDebugWin* _pWin, MENU_BER* _pParent);

		//-------------------------------------------------------------------------------------------------
		//@brief	破棄
		void	Finalize(MEM_HANDLE _memHdl);

		//-------------------------------------------------------------------------------------------------
		//@brief	更新
		void	Step();

		//-------------------------------------------------------------------------------------------------
		//@brief	検索
		MENU_BER*	Search(hash32 _nameCrc);
	};

	//*************************************************************************************************
	//@brief	メニュー構築
	//*************************************************************************************************
	struct INIT_MENU
	{
		const c8* menuName = NULL;
		const c8* winName = NULL;
		const c8* parentName = NULL;
	};

public:
	CDebugWinManager();
	~CDebugWinManager();

	//-------------------------------------------------------------------------------------------------
	//@brief	有効化取得
	b8			IsEnable() const { return m_enable; }
	//@brief	有効化設定
	void		SetEnable(b8 _enable) { m_enable = _enable; }

	//-------------------------------------------------------------------------------------------------
	//@brief	メモリ取得
	MEM_HANDLE	GetMemHdl() const { return m_memHdl; }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	b8		Initialize(MEM_HANDLE _memHdl);

	//-------------------------------------------------------------------------------------------------
	//@brief	登録
	CDebugWin*	Regist(hash32 _debugWinName);

	//-------------------------------------------------------------------------------------------------
	//@brief	メニューセットアップ
	b8			SetupMenu(const INIT_MENU* _pInit, u32 _num);

	//-------------------------------------------------------------------------------------------------
	//@brief	メニュー
	MENU_BER*	AddMenu(const INIT_MENU& _init);

	//-------------------------------------------------------------------------------------------------
	//@brief	更新
	void	Finalize();

	//-------------------------------------------------------------------------------------------------
	//@brief	更新
	void	Step();

private:
	//-------------------------------------------------------------------------------------------------
	//@brief	ドックスペース
	void	DockSpace();

	//-------------------------------------------------------------------------------------------------
	//@brief	メニューバー
	void	MenuBer();

	//-------------------------------------------------------------------------------------------------
	//@brief	検索
	CDebugWin*	Search(hash32 _nameCrc);


private:
	MEM_HANDLE		m_memHdl = MEM_HDL_INVALID;	///< デバッグメモリ
	b8				m_enable = false;			///< デバッグ有効化

	MENU_BER		m_pTopMenuBer;				///< メニューバー
};



//*************************************************************************************************
//@brief	デバッグウィンドウユーティリティ
//*************************************************************************************************
class CDebugWinUtility
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	マネージャー取得
	static CDebugWinManager*	GetManager();

	//-------------------------------------------------------------------------------------------------
	//@brief	セットアップ
	static void		Setup(CDebugWinManager* _pManager);

	//-------------------------------------------------------------------------------------------------
	///@brief 更新
	static void		Step();

};



WORK_NAMESPACE_END

#endif	// __DEBUG_WIN_MANAGER_H__

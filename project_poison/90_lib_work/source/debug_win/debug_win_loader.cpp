﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "debug_win_loader.h"
#include "debug_win_manager.h"
#include "debug_win.h"

#include "file/file_utility.h"
#include "script/script_interpreter.h"

#include "main.h"

///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant
#define DEBUG_WIN_STACK_NUM	16


WORK_NAMESPACE_BGN


///-------------------------------------------------------------------------------------------------
/// 読み込み時にのみ使用する情報
struct RUN_INFO_ST
{
	CDebugWinManager*			pDebugWinManager = NULL;
	CDebugWin*					pDebugWin = NULL;
	CDebugWinManager::MENU_BER* pParentDebugMenu[DEBUG_WIN_STACK_NUM] = {};
	s32							menuStackNum = 0;
};

///-------------------------------------------------------------------------------------------------
/// 解析用クラス
class CDebugWinDefInner
{
public:
	///-------------------------------------------------------------------------------------------------
	/// デバッグウィンドウ生成
	static s32 Tag_DEBUG_WIN_INFO_BGN(SI_STACK _st)
	{
		RUN_INFO_ST* pRunInfo = PCast<RUN_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }

		MEM_HANDLE memHdl = pRunInfo->pDebugWinManager->GetMemHdl();

		if (siGetStackRemain(_st) <= 0) { Assert(0);  return 0; }
		// クラス生成
		const c8* pClassName = siGetStackString_inc(_st);
		CDebugWin* pDebugWin = CDebugWinCreater::Create(memHdl, CRC32(pClassName));
		Assert(pDebugWin);

		if (siGetStackRemain(_st) <= 0) { Assert(0);  return 0; }
		// ウィンドウ名生成
		const c8* pWindowName = siGetStackString_inc(_st);
		pDebugWin->SetName(pWindowName);

		// いったん設定
		pRunInfo->pDebugWin = pDebugWin;

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	/// デバッグウィンドウパラメータ設定
	static s32 Tag_DEBUG_WIN_INFO(SI_STACK _st)
	{
		RUN_INFO_ST* pRunInfo = PCast<RUN_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }

		if (siGetStackRemain(_st) <= 0) { return 0; }
		const u32 flags = siGetStackInt_inc(_st);

		if (pRunInfo->pDebugWin)
		{
			pRunInfo->pDebugWin->SetFlags(flags);
		}

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	/// デバッグウィンドウ終わり
	static s32 Tag_DEBUG_WIN_INFO_END(SI_STACK _st)
	{
		RUN_INFO_ST* pRunInfo = PCast<RUN_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }

		pRunInfo->pDebugWin = NULL;

		return 1;
	}


	///-------------------------------------------------------------------------------------------------
	/// デバッグウィンドウメニューバー生成
	static s32 Tag_DEBUG_WIN_MENU_BER_LIST_BEG(SI_STACK _st)
	{
		RUN_INFO_ST* pRunInfo = PCast<RUN_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }

		CDebugWinManager::INIT_MENU initMenu;

		if (siGetStackRemain(_st) > 0)
		{
			const c8* pMenuBerName = siGetStackString_inc(_st);
			initMenu.menuName = pMenuBerName;
		}

		// 親情報あれば設定
		CDebugWinManager::MENU_BER* pParent = pRunInfo->pParentDebugMenu[pRunInfo->menuStackNum];
		if (pParent)
		{
			initMenu.parentName = pParent->name;
		}

		CDebugWinManager::MENU_BER* pMenuBer = pRunInfo->pDebugWinManager->AddMenu(initMenu);
		Assert(pMenuBer);

		// 親情報スタック
		pRunInfo->menuStackNum++;
		pRunInfo->pParentDebugMenu[pRunInfo->menuStackNum] = pMenuBer;
		Assert(pRunInfo->menuStackNum < DEBUG_WIN_STACK_NUM);

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	/// デバッグウィンドウメニュー設定
	static s32 Tag_DEBUG_WIN_MENU_BER(SI_STACK _st)
	{
		RUN_INFO_ST* pRunInfo = PCast<RUN_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }

		CDebugWinManager::INIT_MENU initMenu;

		if (siGetStackRemain(_st) > 0)
		{
			const c8* pMenuBerName = siGetStackString_inc(_st);
			initMenu.menuName = pMenuBerName;
			initMenu.winName = pMenuBerName;
		}

		// 親情報あれば設定
		CDebugWinManager::MENU_BER* pParent = pRunInfo->pParentDebugMenu[pRunInfo->menuStackNum];
		if (pParent)
		{
			initMenu.parentName = pParent->name;
		}

		CDebugWinManager::MENU_BER* pMenuBer = pRunInfo->pDebugWinManager->AddMenu(initMenu);
		Assert(pMenuBer);

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	/// デバッグウィンドウメニュー設定
	static s32 Tag_DEBUG_WIN_MENU_BER_NAME(SI_STACK _st)
	{
		RUN_INFO_ST* pRunInfo = PCast<RUN_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }

		CDebugWinManager::INIT_MENU initMenu;

		if (siGetStackRemain(_st) > 0)
		{
			const c8* pMenuBerName = siGetStackString_inc(_st);
			initMenu.menuName = pMenuBerName;
			initMenu.winName = pMenuBerName;
		}

		if (siGetStackRemain(_st) > 0)
		{
			const c8* pDebugWinName = siGetStackString_inc(_st);
			initMenu.winName = pDebugWinName;
		}

		// 親情報あれば設定
		CDebugWinManager::MENU_BER* pParent = pRunInfo->pParentDebugMenu[pRunInfo->menuStackNum];
		if (pParent)
		{
			initMenu.parentName = pParent->name;
		}

		CDebugWinManager::MENU_BER* pMenuBer = pRunInfo->pDebugWinManager->AddMenu(initMenu);
		Assert(pMenuBer);

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	/// デバッグウィンドウメニューバー終わり
	static s32 Tag_DEBUG_WIN_MENU_BER_LIST_END(SI_STACK _st)
	{
		RUN_INFO_ST* pRunInfo = PCast<RUN_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }

		// 親情報スタック破棄
		pRunInfo->pParentDebugMenu[pRunInfo->menuStackNum] = NULL;
		pRunInfo->menuStackNum--;
		Assert(pRunInfo->menuStackNum >= 0);

		return 1;
	}
};



///-------------------------------------------------------------------------------------------------
/// 設定ファイル解析関数リスト
SI_TAG_TABLE_INSTANCE_BEGIN(s_CDefBuildTagList)[] =
{
	SI_TAG_C2(DEBUG_WIN_INFO_BGN, CDebugWinDefInner, Tag_),
	SI_TAG_C2(DEBUG_WIN_INFO, CDebugWinDefInner, Tag_),
	SI_TAG_C2(DEBUG_WIN_INFO_END, CDebugWinDefInner, Tag_),

	SI_TAG_C2(DEBUG_WIN_MENU_BER_LIST_BEG, CDebugWinDefInner, Tag_),
	SI_TAG_C2(DEBUG_WIN_MENU_BER, CDebugWinDefInner, Tag_),
	SI_TAG_C2(DEBUG_WIN_MENU_BER_NAME, CDebugWinDefInner, Tag_),
	SI_TAG_C2(DEBUG_WIN_MENU_BER_LIST_END, CDebugWinDefInner, Tag_),

	SI_TAG_TERM()
};
SI_TAG_TABLE_INSTANCE_END(s_CDefBuildTagList);


///-------------------------------------------------------------------------------------------------
/// ファイルロード
b8	CDebugWinLoader::Build(CDebugWinManager* _pDebugWinManager, const c8* _path)
{
	FILE_HANDLE fHdl = CFileUtility::Open(_path);
	if (fHdl == FILE_HANDLE_INVALID) { return false; }

	s32 fSize = 0;
	CFileUtility::GetFileSize(fHdl, &fSize);
	c8* fBuf = PCast<c8*>(MEM_MALLOC(MEM_ID_WORK, fSize));

	CFileUtility::Read(fHdl, fBuf, fSize);

	b8 succeeded = false;
	{
		// スクリプトインタプリタを作成
		CScriptInterpreter si;

		RUN_INFO_ST runInfo;
		runInfo.pDebugWinManager = _pDebugWinManager;

		si.SetTag(SI_SET_TAG_PARAM(s_CDefBuildTagList));
		si.SetExtParam(&runInfo);
		si.SetScript(fBuf, fSize);

		// 解析
		succeeded = si.Run();
	}

	MEM_SAFE_FREE(MEM_ID_WORK, fBuf);
	CFileUtility::Close(fHdl);

	return succeeded;
}

WORK_NAMESPACE_END

﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "debug_win/debug_win.h"
#include "imgui/imgui.h"


///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant



WORK_NAMESPACE_BGN

//*************************************************************************************************
/// デバッグウィンドウクリエイター
//*************************************************************************************************
// 静的クリエイトリンクノード宣言
CDebugWinCreater*	CDebugWinCreater::s_pTop = NULL;
CDebugWinCreater*	CDebugWinCreater::s_pEnd = NULL;
u32					CDebugWinCreater::s_num = 0;

///-------------------------------------------------------------------------------------------------
/// デバッグウィンドウ生成
CDebugWin*	CDebugWinCreater::Create(MEM_HANDLE _memHdl, hash32 _classNameCrc)
{
	CDebugWinCreater* pNode = CDebugWinCreater::GetNodeTop();
	while (pNode)
	{
		if (pNode->m_nameCrc == _classNameCrc)
		{
			return pNode->Create(_memHdl);
		}
		pNode = pNode->GetNodeNext();
	}
	return NULL;
}

///-------------------------------------------------------------------------------------------------
/// デバッグウィンドウ破棄
void		CDebugWinCreater::Release(MEM_HANDLE _memHdl, CDebugWin** _ppDebugWin)
{
	MEM_SAFE_DELETE(_memHdl, (*_ppDebugWin));
}

///-------------------------------------------------------------------------------------------------
/// 先頭に追加
void	CDebugWinCreater::AddNodeTop()
{
	CDebugWinCreater* pThis = SCast<CDebugWinCreater*>(this);
	if (s_pTop)
	{
		s_pTop->AddNodePrev(pThis);
	}
	else
	{
		s_pEnd = pThis;
	}
	s_pTop = pThis;
	++s_num;
}
///-------------------------------------------------------------------------------------------------
/// 末尾に追加
void	CDebugWinCreater::AddNodeEnd()
{
	CDebugWinCreater* pThis = SCast<CDebugWinCreater*>(this);
	if (s_pEnd)
	{
		s_pEnd->AddNodeNext(pThis);
	}
	else
	{
		s_pTop = pThis;
	}
	s_pEnd = pThis;
	++s_num;
}
///-------------------------------------------------------------------------------------------------
/// ノード削除
void	CDebugWinCreater::DeleteNode()
{
	CDebugWinCreater* pThis = SCast<CDebugWinCreater*>(this);
	if (s_pTop == NULL || s_pEnd == NULL) { return; }
	if (pThis == s_pTop) { s_pTop = m_pNext; }
	if (pThis == s_pEnd) { s_pEnd = m_pPrev; }
	{
		if (m_pPrev)
		{
			m_pPrev->SetNodeNext(m_pNext);
			m_pPrev = NULL;
		}
		if (m_pNext)
		{
			m_pNext->SetNodePrev(m_pPrev);
			m_pNext = NULL;
		}
	}
	--s_num;
}

///-------------------------------------------------------------------------------------------------
/// コンストラクタ
CDebugWinCreater::CDebugWinCreater(const c8* _className)
	: m_name(_className)
	, m_nameCrc(CRC32(_className))
{
	//AddNodeTop();
	AddNodeEnd();
}

///-------------------------------------------------------------------------------------------------
/// デストラクタ
CDebugWinCreater::~CDebugWinCreater()
{
	DeleteNode();
}




//*************************************************************************************************
/// デバッグウィンドウ
//*************************************************************************************************

///-------------------------------------------------------------------------------------------------
///	コンストラクタ・デストラクタ
CDebugWin::CDebugWin()
{
}

CDebugWin::~CDebugWin()
{
}

///-------------------------------------------------------------------------------------------------
/// ウィンドウ名設定
void	CDebugWin::SetName(const c8* _name)
{
	strcpy_s(m_name, _name);
	m_mameCrc = CRC32(_name);
}

///-------------------------------------------------------------------------------------------------
/// 更新開始
void	CDebugWin::BeginStep()
{
	// アクティブコールバック
	if (m_enable == true && m_enable != m_prevEnable)
	{
		OnActivate();
	}

	ImGui::Begin(m_name, &m_enable, m_flags);
}

///-------------------------------------------------------------------------------------------------
/// 更新終了
void	CDebugWin::EndStep()
{
	ImGui::End();

	// ディアクティブコールバック
	if (m_enable == false && m_enable != m_prevEnable)
	{
		OnDeactivate();
	}

	m_prevEnable = m_enable;
}


// 登録
REGIST_DEBUG_WIN(CDebugWin);

WORK_NAMESPACE_END

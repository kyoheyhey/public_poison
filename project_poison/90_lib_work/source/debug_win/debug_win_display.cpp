﻿
#ifndef POISON_RELEASE

///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "debug_win/debug_win.h"
#include "imgui/imgui.h"

// タイム
#include "timer/timer.h"

// グローバルパラム
#include "global_param/system_global_param.h"

///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant



WORK_NAMESPACE_BGN

//*************************************************************************************************
//@brief	ディスプレイ情報デバッグウィンドウ
//*************************************************************************************************
class CDebugWinDisplayInfo : public CDebugWin
{
public:
	CDebugWinDisplayInfo();
	~CDebugWinDisplayInfo();

public:
	//-------------------------------------------------------------------------------------------------
	///@brief	更新
	virtual void	Step() override;

	//-------------------------------------------------------------------------------------------------
	///@brief	デバッグウインドウマネージャに登録コールバック
	virtual void	OnRegist() override;

	//-------------------------------------------------------------------------------------------------
	///@brief	デバッグウインドウマネージャに解除コールバック
	virtual void	OnUnregist() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	アクディブコールバック
	virtual void	OnActivate() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	ディアクティブコールバック
	virtual void	OnDeactivate() override;

};


///-------------------------------------------------------------------------------------------------
///	コンストラクタ・デストラクタ
CDebugWinDisplayInfo::CDebugWinDisplayInfo()
{
}

CDebugWinDisplayInfo::~CDebugWinDisplayInfo()
{
}

///-------------------------------------------------------------------------------------------------
/// 更新
void	CDebugWinDisplayInfo::Step()
{
	CGfxDevice* pDevice = SDCast<CGfxDevice*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);

	ImGui::Text("Window Size	Width[%d] Height[%d]", pDevice->GetWidth(), pDevice->GetHeight());
	ImGui::Text("Display Size	Width[%d] Height[%d]",
		CSystemGlobalParam::GetInstance().m_displaySize[0], CSystemGlobalParam::GetInstance().m_displaySize[1]);

	static const c8* alignsText[] =
	{
		"SCREEN_ALIGN_LEFT_TOP",
		"SCREEN_ALIGN_LEFT_MIDDLE",
		"SCREEN_ALIGN_LEFT_BOTTOM",
		"SCREEN_ALIGN_CENTER_TOP",
		"SCREEN_ALIGN_CENTER_MIDDLE",
		"SCREEN_ALIGN_CENTER_BOTTOM",
		"SCREEN_ALIGN_RIGHT_TOP",
		"SCREEN_ALIGN_RIGHT_MIDDLE",
		"SCREEN_ALIGN_RIGHT_BOTTOM",
	};
	static SCREEN_ALIGN aligns[] =
	{
		SCREEN_ALIGN_LEFT_TOP,
		SCREEN_ALIGN_LEFT_MIDDLE,
		SCREEN_ALIGN_LEFT_BOTTOM,
		SCREEN_ALIGN_CENTER_TOP,
		SCREEN_ALIGN_CENTER_MIDDLE,
		SCREEN_ALIGN_CENTER_BOTTOM,
		SCREEN_ALIGN_RIGHT_TOP,
		SCREEN_ALIGN_RIGHT_MIDDLE,
		SCREEN_ALIGN_RIGHT_BOTTOM,
	};
	StaticAssert(ARRAYOF(alignsText) == ARRAYOF(aligns));
	s32 alignIdx = 0;
	for (s32 i = 0; i < ARRAYOF(aligns); i++)
	{
		if (CSystemGlobalParam::GetInstance().m_align == aligns[i])
		{
			alignIdx = i;
			break;
		}
	}
	ImGui::Combo("ALIGN", &alignIdx, alignsText, ARRAYOF(alignsText));
	CSystemGlobalParam::GetInstance().m_align = aligns[alignIdx];

	static const c8* stretchText[] =
	{
		"SCREEN_STRETCH_FIT",
		"SCREEN_STRETCH_HORIZONTAL",
		"SCREEN_STRETCH_VERTICAL",
		"SCREEN_STRETCH_OVERSCAN",
		"SCREEN_STRETCH_DIRECT",
	};
	StaticAssert(SCREEN_STRETCH_TYPE_NUM == ARRAYOF(stretchText));
	s32 stretchIdx = SCast<s32>(CSystemGlobalParam::GetInstance().m_stretch);
	ImGui::Combo("STRETCH", &stretchIdx, stretchText, ARRAYOF(stretchText));
	CSystemGlobalParam::GetInstance().m_stretch = SCast<SCREEN_STRETCH>(stretchIdx);

	ImGui::Checkbox("Display Fixed Aspect", &CSystemGlobalParam::GetInstance().m_fixAspect);
	if (CSystemGlobalParam::GetInstance().m_fixAspect)
	{
		s32 tmpSize[] = { SCast<u16>(CSystemGlobalParam::GetInstance().m_displayAspect[0]), SCast<u16>(CSystemGlobalParam::GetInstance().m_displayAspect[1]) };
		ImGui::DragInt2("Display Aspect", tmpSize, 1.0f, 1, 32);
		CSystemGlobalParam::GetInstance().m_displayAspect[0] = SCast<u16>(tmpSize[0]);
		CSystemGlobalParam::GetInstance().m_displayAspect[1] = SCast<u16>(tmpSize[1]);
		f32 aspect = SCast<f32>(CSystemGlobalParam::GetInstance().m_displayAspect[0]) / SCast<f32>(CSystemGlobalParam::GetInstance().m_displayAspect[1]);

		s32 tmpHeightSize = SCast<s32>(CSystemGlobalParam::GetInstance().m_displaySize[1]);
		ImGui::DragInt("Display HeightSize", &tmpHeightSize, 1.0f, 0, 4096);
		CSystemGlobalParam::GetInstance().m_displaySize[1] = SCast<u16>(tmpHeightSize);
		CSystemGlobalParam::GetInstance().m_displaySize[0] = SCast<u16>(tmpHeightSize * aspect);
	}
	else
	{
		s32 tmpSize[] = { SCast<s32>(CSystemGlobalParam::GetInstance().m_displaySize[0]), SCast<s32>(CSystemGlobalParam::GetInstance().m_displaySize[1]) };
		ImGui::DragInt2("Display Size", tmpSize, 1.0f, 0, 4096);
		CSystemGlobalParam::GetInstance().m_displaySize[0] = SCast<u16>(tmpSize[0]);
		CSystemGlobalParam::GetInstance().m_displaySize[1] = SCast<u16>(tmpSize[1]);
	}

	f32 winClr[3] =
	{
		CSystemGlobalParam::GetInstance().m_windowClearColor.x,
		CSystemGlobalParam::GetInstance().m_windowClearColor.y,
		CSystemGlobalParam::GetInstance().m_windowClearColor.z,
	};
	f32 disClr[3] =
	{
		CSystemGlobalParam::GetInstance().m_displayClearColor.x,
		CSystemGlobalParam::GetInstance().m_displayClearColor.y,
		CSystemGlobalParam::GetInstance().m_displayClearColor.z,
	};
	ImGui::ColorEdit3("Window ClearColor", winClr);
	ImGui::ColorEdit3("Display ClearColor", disClr);
	CSystemGlobalParam::GetInstance().m_windowClearColor.Set(winClr);
	CSystemGlobalParam::GetInstance().m_displayClearColor.Set(disClr);

	ImGui::Dummy(ImVec2(5.0f, 5.0f));
	ImGui::Separator();
	static const c8* syncIntervalText[] =
	{
		"SYNC_NONE",
		"SYNC_60FPS",
		"SYNC_30FPS",
		"SYNC_20FPS",
		"SYNC_15FPS",
	};
	s32 syncIntervalIdx = SCast<s32>(CSystemGlobalParam::GetInstance().m_syncInterval);
	ImGui::Combo("Sync Interval", &syncIntervalIdx, syncIntervalText, ARRAYOF(syncIntervalText));
	CSystemGlobalParam::GetInstance().m_syncInterval = SCast<u32>(syncIntervalIdx);

	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	ImGui::Text("CTimeUtility stepTime %.3f ms/frame (%.1f FPS)", 1000.0f * CTimeUtility::GetStepTime(), 1.0f / CTimeUtility::GetStepTime());

	f64 gpuTimer = 0.0;
	u64 gpuPrimitiveCount = 0;
	for (u32 renderIdx = 0; renderIdx < CGfxUtility::GetRendererNum(); ++renderIdx)
	{
		gpuTimer += CGfxUtility::GetRenderTimeMilliSec(renderIdx);
		gpuPrimitiveCount += CGfxUtility::GetRenderPrimitiveCount(renderIdx);
	}

	ImGui::Dummy(ImVec2(5.0f, 5.0f));
	ImGui::Separator();
	ImGui::Text("GPU Timer %.4f ms/frame", gpuTimer);
	ImGui::Text("GPU PolygonCount %d /frame", gpuPrimitiveCount);
	//ImGui::Text("GPU Occlusion %s", (s_gpuIsOcclusion ? "true" : "false"));
	//ImGui::Text("GPU PixelCount %d /frame", s_gpuPixelCount);

}

///-------------------------------------------------------------------------------------------------
/// デバッグウインドウマネージャに登録コールバック
void	CDebugWinDisplayInfo::OnRegist()
{
}

///-------------------------------------------------------------------------------------------------
/// デバッグウインドウマネージャに解除コールバック
void	CDebugWinDisplayInfo::OnUnregist()
{
}

///-------------------------------------------------------------------------------------------------
/// アクディブコールバック
void	CDebugWinDisplayInfo::OnActivate()
{
}

///-------------------------------------------------------------------------------------------------
/// ディアクティブコールバック
void	CDebugWinDisplayInfo::OnDeactivate()
{
}


// 登録
REGIST_DEBUG_WIN(CDebugWinDisplayInfo);


WORK_NAMESPACE_END

#endif // !POISON_RELEASE

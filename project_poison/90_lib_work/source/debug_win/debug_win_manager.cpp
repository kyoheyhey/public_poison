﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "debug_win/debug_win_manager.h"
#include "debug_win/debug_win.h"

#include "imgui/imgui.h"

///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant



WORK_NAMESPACE_BGN


///-------------------------------------------------------------------------------------------------
///	初期化
b8		CDebugWinManager::MENU_BER::Initialize(MEM_HANDLE _memHdl, const c8* _name, CDebugWin* _pWin, MENU_BER* _pParent)
{
	if (_name)
	{
		strcpy_s(name, _name);
		nameCrc = CRC32(_name);
	}
	pDebugWin = _pWin;
	pParent = _pParent;
	child.Initialize(_memHdl);

	return true;
}

///-------------------------------------------------------------------------------------------------
///	破棄
void	CDebugWinManager::MENU_BER::Finalize(MEM_HANDLE _memHdl)
{
	MENU_BER_LIST::CItr& itr = child.Begin();
	while (itr != child.End())
	{
		MENU_BER* pMenu = (*itr);
		pMenu->Finalize(_memHdl);
		MEM_DELETE(_memHdl, pMenu);
		++itr;
	}
	pDebugWin = NULL;
	pParent = NULL;
	child.Finalize();
}

///-------------------------------------------------------------------------------------------------
///	メニュー
void	CDebugWinManager::MENU_BER::Step()
{
	b8 isOpen = false;

	// 子供を持ってる場合は階層化
	if (child.GetNum() > 0)
	{
		if (ImGui::BeginMenu(name))
		{
			MENU_BER_LIST::CItr& itr = child.Begin();
			while (itr != child.End())
			{
				// 子メニュー
				(*itr)->Step();
				++itr;
			}
			ImGui::EndMenu();
			isOpen |= true;
		}
	}
	// 単体の場合はそのまま
	else
	{
		if (ImGui::MenuItem(name))
		{
			isOpen |= true;
		}
	}

	// 設定されてるウィンドウ有効/無効設定
	if (pDebugWin && isOpen)
	{
		b8 enable = pDebugWin->IsEnable();
		enable ^= true;
		pDebugWin->SetEnable(enable);
	}
}

///-------------------------------------------------------------------------------------------------
///	検索
CDebugWinManager::MENU_BER*	CDebugWinManager::MENU_BER::Search(hash32 _nameCrc)
{
	MENU_BER* pRet = NULL;

	// 自身をチェック
	if (nameCrc == _nameCrc)
	{
		return this;
	}

	MENU_BER_LIST::CItr& itr = child.Begin();
	while (itr != child.End())
	{
		MENU_BER* pMenu = (*itr);
		// 子供をチェック
		if (pMenu->nameCrc == _nameCrc)
		{
			pRet = pMenu;
			break;
		}
		// 更に孫たちを検索
		pRet = pMenu->Search(_nameCrc);
		if (pRet)
		{
			break;
		}

		++itr;
	}
	return pRet;
}

///-------------------------------------------------------------------------------------------------
///	コンストラクタ・デストラクタ
CDebugWinManager::CDebugWinManager()
{
}

CDebugWinManager::~CDebugWinManager()
{
}

///-------------------------------------------------------------------------------------------------
///	初期化
b8		CDebugWinManager::Initialize(MEM_HANDLE _memHdl)
{
	m_memHdl = _memHdl;

	m_pTopMenuBer.Initialize(_memHdl, "top", NULL, NULL);

	return true;
}

///-------------------------------------------------------------------------------------------------
///	登録
CDebugWin*	CDebugWinManager::Regist(hash32 _debugWinName)
{
	// 生成
	CDebugWin* pDebugWin = CDebugWinCreater::Create(m_memHdl, _debugWinName);
	Assert(pDebugWin);

	// 登録コールバック
	pDebugWin->OnRegist();
	return pDebugWin;
}

///-------------------------------------------------------------------------------------------------
///	メニューセットアップ
b8		CDebugWinManager::SetupMenu(const INIT_MENU* _pInit, u32 _num)
{
	for (u32 i = 0; i < _num; ++i, ++_pInit)
	{
		if (!AddMenu(*_pInit))
		{
			return false;
		}
	}
	return true;
}

///-------------------------------------------------------------------------------------------------
///	メニュー
CDebugWinManager::MENU_BER*		CDebugWinManager::AddMenu(const INIT_MENU& _init)
{
	// 親メニューバー検索
	MENU_BER* pParent = &m_pTopMenuBer;
	if (_init.parentName)
	{
		pParent = m_pTopMenuBer.Search(CRC32(_init.parentName));
	}

	if (pParent)
	{
		// メニューバー生成
		MENU_BER* pChild = MEM_NEW(m_memHdl) MENU_BER;
		CDebugWin* pWin = NULL;
		if (_init.winName)
		{
			pWin = Search(CRC32(_init.winName));
		}
		pChild->Initialize(m_memHdl, _init.menuName, pWin, pParent);

		// 親に登録
		pParent->child.PushBack(pChild);
		return pChild;
	}
	return NULL;
}

///-------------------------------------------------------------------------------------------------
///	終了
void	CDebugWinManager::Finalize()
{
	CDebugWin* pDebugWin = CDebugWin::GetNodeTop();
	while (pDebugWin)
	{
		CDebugWin* pNext = pDebugWin->GetNodeNext();

		// 解除コールバック
		pDebugWin->OnUnregist();

		// 破棄
		CDebugWinCreater::Release(m_memHdl, &pDebugWin);

		// 次ノードへ
		pDebugWin = pNext;
	}

	// メニューバー削除
	m_pTopMenuBer.Finalize(m_memHdl);
}

///-------------------------------------------------------------------------------------------------
///	更新
void	CDebugWinManager::Step()
{
	if (!IsEnable()) { return; }

	DockSpace();

	CDebugWin* pDebugWin = CDebugWin::GetNodeTop();
	while (pDebugWin)
	{
		if (pDebugWin->IsEnable())
		{
			pDebugWin->BeginStep();
			{
				pDebugWin->Step();
			}
			pDebugWin->EndStep();
		}

		// 次ノードへ
		pDebugWin = pDebugWin->GetNodeNext();
	}
}


///-------------------------------------------------------------------------------------------------
///	ドックスペース
void	CDebugWinManager::DockSpace()
{
	static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;
	static ImGuiWindowFlags window_flags =
		ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoTitleBar |
		ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove |
		ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

	// ビューポートが出来次第削除予定
	dockspace_flags |= ImGuiDockNodeFlags_PassthruCentralNode;
	window_flags |= ImGuiWindowFlags_NoBackground;

	ImGuiViewport* viewport = ImGui::GetMainViewport();
	ImGui::SetNextWindowPos(viewport->GetWorkPos());
	ImGui::SetNextWindowSize(viewport->GetWorkSize());
	ImGui::SetNextWindowViewport(viewport->ID);

	ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
	ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
	if (ImGui::Begin("DebugWindow", &m_enable, window_flags))
	{
		if (ImGui::GetIO().ConfigFlags & ImGuiConfigFlags_DockingEnable)
		{
			ImGuiID dockspace_id = ImGui::GetID("DebugWindow");
			ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);
		}

		// メニューバー
		MenuBer();
	}
	ImGui::End();
	ImGui::PopStyleVar(3);
}


///-------------------------------------------------------------------------------------------------
///	メニューバー
void	CDebugWinManager::MenuBer()
{
	if (ImGui::BeginMenuBar())
	{
		// 各メニュー更新
		MENU_BER_LIST::CItr& itr = m_pTopMenuBer.child.Begin();
		while (itr != m_pTopMenuBer.child.End())
		{
			// メニュー
			(*itr)->Step();
			++itr;
		}

		ImGui::EndMenuBar();
	}
}


//-------------------------------------------------------------------------------------------------
//@brief	検索
CDebugWin* CDebugWinManager::Search(hash32 _nameCrc)
{
	CDebugWin* pDebugWin = CDebugWin::GetNodeTop();
	while (pDebugWin)
	{
		if (pDebugWin->GetNameCrc() == _nameCrc)
		{
			return pDebugWin;
		}

		// 次ノードへ
		pDebugWin = pDebugWin->GetNodeNext();
	}
	return NULL;
}



static CDebugWinManager*	s_pManager = NULL;
///-------------------------------------------------------------------------------------------------
///	マネージャー取得
CDebugWinManager* CDebugWinUtility::GetManager()
{
	return s_pManager;
}

///-------------------------------------------------------------------------------------------------
///	セットアップ
void	CDebugWinUtility::Setup(CDebugWinManager* _pManager)
{
	s_pManager = _pManager;
}

///-------------------------------------------------------------------------------------------------
///	更新
void	CDebugWinUtility::Step()
{
	Assert(s_pManager);
	s_pManager->Step();
}

WORK_NAMESPACE_END

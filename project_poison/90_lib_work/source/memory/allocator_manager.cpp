﻿#include "allocator_manager.h"

#include "memory/allocator_utility.h"
#include "memory/heap_allocator_win.h"
#include "memory/boundary_tag_allocator.h"
#include "memory/double_buffer_allocator.h"

#include "main.h"

WORK_NAMESPACE_BGN

static c8	s_constantBuffer[1024];			//< 固定バッファ
static c8	s_systemBuffer[1024];			//< システムバッファ
static c8	s_graphicsBuffer[1024 * 1024];	//< グラフィックスバッファ
static c8	s_drawObjBuffer[1024 * 1024];	//< 描画オブジェクトバッファ
static c8	s_renderBuffer[1024 * 1024];	//< レンダラーバッファ
static c8	s_oneFremeBuffer[1024];			//< １フレームバッファ
static c8	s_objectBuffer[1024 * 1024];	//< オブジェクトバッファ
static c8	s_resourceBuffer[1024 * 1024];	//< リソースバッファ
static c8	s_workBuffer[1024 * 1024 * 16];	//< ワークバッファ
static c8	s_debugBuffer[1024];			//< デバッグバッファ
static c8	s_test0Buffer[512];				//< テスト0バッファ
static c8	s_test1Buffer[256];				//< テスト1バッファ
static c8*	s_bufferList[MEM_ID_NUM] =		//< バッファリスト
{
	NULL,
	s_constantBuffer,
	s_systemBuffer,
	s_graphicsBuffer,
	s_drawObjBuffer,
	s_renderBuffer,
	s_oneFremeBuffer,
	s_objectBuffer,
	s_resourceBuffer,
	s_workBuffer,
	s_debugBuffer,
	s_test0Buffer,
	s_test1Buffer,
};
static u32	s_bufferSizeNum[MEM_ID_NUM] =
{
	0,
	sizeof(s_constantBuffer),
	sizeof(s_systemBuffer),
	sizeof(s_graphicsBuffer),
	sizeof(s_drawObjBuffer),
	sizeof(s_renderBuffer),
	sizeof(s_oneFremeBuffer),
	sizeof(s_objectBuffer),
	sizeof(s_resourceBuffer),
	sizeof(s_workBuffer),
	sizeof(s_debugBuffer),
	sizeof(s_test0Buffer),
	sizeof(s_test1Buffer),
};
static CBoundaryTagAllocator	s_allocatorConstant;		//< 固定アロケータ
static CBoundaryTagAllocator	s_allocatorSystem;			//< システムアロケータ
static CBoundaryTagAllocator	s_allocatorGraphics;		//< グラフィックスアロケータ
static CDoubleBufferAllocator	s_allocatorDrawObj;			//< 描画オブジェクトアロケータ
static CDoubleBufferAllocator	s_allocatorRenderThread;	//< レンダラーアロケータ
static CDoubleBufferAllocator	s_allocatorOneFrame;		//< １フレームスアロケータ
static CBoundaryTagAllocator	s_allocatorObject;			//< オブジェクトアロケータ
static CBoundaryTagAllocator	s_allocatorResource;		//< リソースアロケータ
static CBoundaryTagAllocator	s_allocatorWork;			//< ワークアロケータ
static CHeapAllocatorWin		s_allocatorDebug;			//< デバッグアロケータ
static CBoundaryTagAllocator	s_allocatorTest0;			//< テストアロケータ0
static CDoubleBufferAllocator	s_allocatorTest1;			//< テストアロケータ1
static IAllocator*	s_allocatorList[MEM_ID_NUM] =			//< アロケータリスト
{
	NULL,
	&s_allocatorConstant,
	&s_allocatorSystem,
	& s_allocatorGraphics,
	&s_allocatorDrawObj,
	&s_allocatorRenderThread,
	&s_allocatorOneFrame,
	&s_allocatorObject,
	&s_allocatorResource,
	&s_allocatorWork,
	&s_allocatorDebug,
	&s_allocatorTest0,
	&s_allocatorTest1,
};


///---------------------------------------------------------------------
///	初期化
void	CAllocatorManager::Initialize()
{
	StaticAssert(ARRAYOF(s_bufferList) == ARRAYOF(s_bufferSizeNum) && ARRAYOF(s_bufferList) == ARRAYOF(s_allocatorList));

	for (u32 i = 1; i < MEM_ID_NUM; ++i)
	{
		s_allocatorList[i]->Initialize(s_bufferList[i], s_bufferSizeNum[i]);
	}
	CAllocatorUtility::Initialize(s_allocatorList, MEM_ID_NUM);
}

///---------------------------------------------------------------------
///	終了
void	CAllocatorManager::Finalize()
{
	for (u32 i = 1; i < MEM_ID_NUM; i++)
	{
		s_allocatorList[i]->Finalize();
	}
}

///---------------------------------------------------------------------
///	更新
void	CAllocatorManager::Update()
{
	for (u32 i = 1; i < MEM_ID_NUM; i++)
	{
		s_allocatorList[i]->Update();
	}
}

WORK_NAMESPACE_END

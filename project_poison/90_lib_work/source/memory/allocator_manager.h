﻿#pragma once
#ifndef __ALLOCATOR_MANAGER_H__
#define __ALLOCATOR_MANAGER_H__


WORK_NAMESPACE_BGN

///*********************************************************************
///	アロケータ管理クラス
///*********************************************************************
STATIC_SINGLETON(CAllocatorManager)
{
public:
	FRIEND_STATIC_SINGLETON(CAllocatorManager);

public:
	///---------------------------------------------------------------------
	///	初期化
	static void	Initialize();

	///---------------------------------------------------------------------
	///	終了
	static void	Finalize();

	///---------------------------------------------------------------------
	///	更新
	static void	Update();
};

WORK_NAMESPACE_END


#endif	// __ALLOCATOR_MANAGER_H__

﻿//#pragma once
#ifndef __DEV_DEF_H__
#define __DEV_DEF_H__

//-------------------------------------------------------------------------------------------------
//@brief	レンダーターゲット
enum RENDER_TARGET_TYPE
{
	RT_POST = 0,	//< ポスト
	RT_MAIN,		//< メイン
	RT_NOISE,		//< ノイズ
	RT_SHADOW,		//< 影

	RT_TYPE_NUM
};

//-------------------------------------------------------------------------------------------------
//@brief	ポストバッファ
enum RT_POST_CLR
{
	RT_POST_CLR_0 = 0,

	RT_POST_CLR_NUM
};

//-------------------------------------------------------------------------------------------------
//@brief	メインバッファ
enum RT_MAIN_CLR
{
	RT_MAIN_CLR_0 = 0,

	RT_MAIN_CLR_NUM
};
enum RT_MAIN_DEP
{
	RT_MAIN_DEP_0 = 0,

	RT_MAIN_DEP_NUM
};

//-------------------------------------------------------------------------------------------------
//@brief	ノイズバッファ
enum RT_NOISE_CLR
{
	RT_NOISE_CLR_0 = 0,

	RT_NOISE_CLR_NUM
};

//-------------------------------------------------------------------------------------------------
//@brief	シャドウバッファ
enum RT_SHADOW_CLR
{
	RT_SHADOW_CLR_NUM
};


#endif	// __DEV_DEF_H__

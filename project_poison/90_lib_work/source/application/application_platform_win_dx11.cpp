﻿
#ifdef LIB_GFX_DX11

///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "application.h"

#include "main.h"
#include "dev_def.h"

#include "file/file_utility.h"
#include "file/file_operate_win.h"

#include "input/keyboard_win.h"
#include "input/mouse_win.h"

// DX11デバイス
#include "dx11/device/win/device_dx11.h"
#include "dx11/renderer/renderer_dx11.h"

// レンダーターゲット
#include "renderer/render_target.h"

// シェーダ
#include "resource/shader_library.h"
#include "resource/shader_loader.h"

// キャッシュビルダー
#include "cache_builder/vertex_cache_builder.h"
#include "cache_builder/index_cache_builder.h"
#include "cache_builder/uniform_cache_builder.h"
#include "cache_builder/rasterizer_cache_builder.h"
#include "cache_builder/blend_state_cache_builder.h"
#include "cache_builder/depth_state_cache_builder.h"
#include "cache_builder/sampler_cache_builder.h"

// query
#include "query/occlusion_query.h"
#include "query/primitive_query.h"
#include "query/timer_query.h"


///-------------------------------------------------------------------------------------------------
/// prototype


WORK_NAMESPACE_BGN
///-------------------------------------------------------------------------------------------------
/// constant
CVertexCacheBuilder		s_vertexCacheBuilder[RENDERER_NUM];
CIndexCacheBuilder		s_indexCacheBuilder[RENDERER_NUM];
CUniformCacheBuilder	s_uniformCacheBuilder[RENDERER_NUM];
CRasterizerCacheBuilder	s_rasterizerCacheBuilder[RENDERER_NUM];
CBlendStateCacheBuilder	s_blendStateCacheBuilder[RENDERER_NUM];
CDepthStateCacheBuilder	s_depthStateCacheBuilder[RENDERER_NUM];
CSamplerCacheBuilder	s_samplerCacheBuilder[RENDERER_NUM];

#ifdef POISON_DEBUG
CPrimitiveQuery			s_pPrimitiveQuery[RENDERER_NUM];
CTimerQuery				s_pTimerQuery[RENDERER_NUM];
#endif // POISON_DEBUG

///---------------------------------------------------------------------
///	プラットホーム初期化
///---------------------------------------------------------------------
b8	CApplication::PlatformInit()
{
	//---------------------------------------------------------------------
	// ファイル初期化
	{
		static CFileOperateWin s_fileOperateWin;
		static c8 s_currentDirectoryPath[256];
		//GetCurrentDirectory(sizeof(s_currentDirectoryPath), s_currentDirectoryPath);
		CFileUtility::FILE_OPERATE operate;
		operate.pFileOperate = &s_fileOperateWin;
		//operate.pBaseRootPath = s_currentDirectoryPath;
		if (!CFileUtility::Initialize(&operate, 0))
		{
			PRINT("************[Error] Failed : CFileUtility::Initialize()\n");
			return false;
		}
	}

	//---------------------------------------------------------------------
	// デバイス生成
	static CDeviceDX11 s_deviceDX11;
	{
		CDeviceUtility::Setup(&s_deviceDX11);
		// デバイス初期化
		if (!s_deviceDX11.InitBegin(MEM_ID_SYSTEM))
		{
			PRINT("************[Error] Failed : CDeviceUtility::InitBegin()\n");
			return false;
		}

		//---------------------------------------------------------------------
		// Windows初期化
		{
			CDeviceWin::INIT_DESC desc;
			desc.title = WINDOW_NAME;
			desc.style = WS_OVERLAPPEDWINDOW;
			desc.icon = ::LoadIcon(NULL, IDC_ICON);
			desc.cursor = ::LoadCursor(NULL, IDC_ARROW);
			desc.rect.width = DEFAULT_WINDOW_WIDTH;
			desc.rect.height = DEFAULT_WINDOW_HEIGHT;
			//desc.rect.posX = DEFAULT_WINDOW_POSX;
			//desc.rect.posY = DEFAULT_WINDOW_POSY;
			s_deviceDX11.InitWin(desc);
		}

		//---------------------------------------------------------------------
		// DirectX11初期化
		{
			CANVAS_DESC canvasDesc;
			canvasDesc.clrFormat = GFX_FORMAT_R8G8B8A8_UNORM;
			canvasDesc.depFormat = GFX_FORMAT_UNKNOWN;
			canvasDesc.aa = GFX_AA_1X;
			canvasDesc.swapNum = 1;
			canvasDesc.refreshRate = DEFAULT_FPS;
			if (!s_deviceDX11.CreateCanvas(canvasDesc))
			{
				PRINT("[CreateCanvas] 何がアカンかったんや...？\n");
			}
		}

		if (!s_deviceDX11.InitEnd())
		{
			PRINT("************[Error] Failed : CDeviceUtility::InitEnd()\n");
			return false;
		}
	}

	//---------------------------------------------------------------------
	// シェーダライブラリ初期化
	{
		static CShaderLibrary s_shaderLibrary;
		s_shaderLibrary.Initialize(MEM_ID_RESOURCE);
		CShaderUtility::Setup(&s_shaderLibrary);

		FILE_HANDLE fHdl = CFileUtility::Open("common/shader/list/shader_list.cfgbin");
		if (fHdl != FILE_HANDLE_INVALID)
		{
			s32 fSize = 0;
			CFileUtility::GetFileSize(fHdl, &fSize);
			c8* fBuf = MEM_CALLOC_TYPES(MEM_ID_WORK, c8, fSize);
			Assert(fBuf);
			CFileUtility::Read(fHdl, fBuf, fSize);

			if (!CShaderLoader::BuildResource(&s_shaderLibrary, fBuf, fSize, MEM_ID_WORK))
			{
				return false;
			}

			MEM_SAFE_FREE(MEM_ID_WORK, fBuf);
		}
		CFileUtility::Close(fHdl);
	}

	//---------------------------------------------------------------------
	// グラフィクス初期化
	{
		// レンダラー初期化
		static CRendererDX11 s_rendererDX11[RENDERER_NUM];
		MEM_HANDLE remderMemHdl[] =
		{
			MEM_ID_RENDER_THREAD,
		};
		const c8* renderName[] =
		{
			"MainRender",
		};
		StaticAssert(ARRAYOF(s_rendererDX11) == ARRAYOF(remderMemHdl) == ARRAYOF(renderName) == RENDERER_NUM);
		for (u32 idx = 0; idx < RENDERER_NUM; idx++)
		{
			s_rendererDX11[idx].Initialize(MEM_ALLOCATOR(remderMemHdl[idx]), renderName[idx]);

			// キャッシュビルダー初期化
			s_vertexCacheBuilder[idx].Initialize(MEM_ID_GRAPHICS, 32);
			s_indexCacheBuilder[idx].Initialize(MEM_ID_GRAPHICS, 32);
			s_uniformCacheBuilder[idx].Initialize(MEM_ID_GRAPHICS, 32, GFX_BUFFER_ATTR_CONSTANT);
			s_rasterizerCacheBuilder[idx].Initialize(MEM_ID_GRAPHICS, 32);
			s_blendStateCacheBuilder[idx].Initialize(MEM_ID_GRAPHICS, 32);
			s_depthStateCacheBuilder[idx].Initialize(MEM_ID_GRAPHICS, 32);
			s_samplerCacheBuilder[idx].Initialize(MEM_ID_GRAPHICS, 32);
			s_rendererDX11[idx].SetCacheBuilder(&s_vertexCacheBuilder[idx]);
			s_rendererDX11[idx].SetCacheBuilder(&s_indexCacheBuilder[idx]);
			s_rendererDX11[idx].SetCacheBuilder(&s_uniformCacheBuilder[idx]);
			s_rendererDX11[idx].SetCacheBuilder(&s_rasterizerCacheBuilder[idx]);
			s_rendererDX11[idx].SetCacheBuilder(&s_blendStateCacheBuilder[idx]);
			s_rendererDX11[idx].SetCacheBuilder(&s_depthStateCacheBuilder[idx]);
			s_rendererDX11[idx].SetCacheBuilder(&s_samplerCacheBuilder[idx]);

#ifdef POISON_DEBUG
			s_pPrimitiveQuery[idx].Initialize();
			s_pTimerQuery[idx].Initialize();
			s_rendererDX11[idx].SetPrimitiveQuery(&s_pPrimitiveQuery[idx]);
			s_rendererDX11[idx].SetTimerQuery(&s_pTimerQuery[idx]);
#endif // POISON_DEBUG
		}
		CGfxUtility::SetUpRenderer(s_rendererDX11, ARRAYOF(s_rendererDX11));
		
		// レンダーターゲット初期化
		static CRenderTarget s_renderTarget[RT_TYPE_NUM];
		{
			RENDER_TARGET_DESC desc;
			desc.name = "RT_Post";
			desc.memHdl = MEM_ID_RESOURCE;
			desc.width = DEFAULT_WINDOW_WIDTH;
			desc.height = DEFAULT_WINDOW_HEIGHT;
			COLOR_DESC clrDesc;
			clrDesc.format = GFX_FORMAT_R8G8B8A8_UNORM;
			clrDesc.aa = GFX_AA_1X;
			clrDesc.usage = GFX_USAGE_NONE;
			desc.pColor = &clrDesc;
			desc.clrNum = RT_POST_CLR_NUM;
			if (!s_renderTarget[RT_POST].Create(desc))
			{
				PRINT("[CRenderTarget] 何がアカンかったんや...？ [%s]\n", desc.name);
			}
		}
		{
			RENDER_TARGET_DESC desc;
			desc.name = "RT_Main";
			desc.memHdl = MEM_ID_RESOURCE;
			desc.width = DEFAULT_WINDOW_WIDTH;
			desc.height = DEFAULT_WINDOW_HEIGHT;
			COLOR_DESC clrDesc;
			clrDesc.format = GFX_FORMAT_R8G8B8A8_UNORM;
			clrDesc.aa = GFX_AA_1X;
			clrDesc.usage = GFX_USAGE_NONE;
			desc.pColor = &clrDesc;
			desc.clrNum = RT_MAIN_CLR_NUM;
			DEPTH_DESC depDesc;
			depDesc.format = GFX_FORMAT_D32_FLOAT;
			depDesc.aa = GFX_AA_1X;
			depDesc.num = 1;
			depDesc.usage = GFX_USAGE_NONE;
			desc.pDepth = &depDesc;
			desc.depNum = RT_MAIN_DEP_NUM;
			if (!s_renderTarget[RT_MAIN].Create(desc))
			{
				PRINT("[CRenderTarget] 何がアカンかったんや...？ [%s]\n", desc.name);
			}
		}
		{
			RENDER_TARGET_DESC desc;
			desc.name = "RT_Noise";
			desc.memHdl = MEM_ID_RESOURCE;
			desc.width = 256;	//rect.width;
			desc.height = 256;	//rect.height;
			COLOR_DESC clrDesc;
			clrDesc.format = GFX_FORMAT_R8G8B8A8_UNORM;
			clrDesc.aa = GFX_AA_1X;
			clrDesc.usage = GFX_USAGE_NONE;
			desc.pColor = &clrDesc;
			desc.clrNum = RT_NOISE_CLR_NUM;
			if (!s_renderTarget[RT_NOISE].Create(desc))
			{
				PRINT("[CRenderTarget] 何がアカンかったんや...？ [%s]\n", desc.name);
			}
		}
		{
			RENDER_TARGET_DESC desc;
			desc.name = "RT_Shadow";
			desc.memHdl = MEM_ID_RESOURCE;
			desc.width = 256;	//rect.width;
			desc.height = 256;	//rect.height;
			DEPTH_DESC depDesc;
			depDesc.format = GFX_FORMAT_D32_FLOAT;
			depDesc.aa = GFX_AA_1X;
			depDesc.num = 1;
			depDesc.usage = GFX_USAGE_NONE;
			desc.pDepth = &depDesc;
			desc.depNum = 1;
			if (!s_renderTarget[RT_SHADOW].Create(desc))
			{
				PRINT("[CRenderTarget] 何がアカンかったんや...？ [%s]\n", desc.name);
			}
		}
		StaticAssert(ARRAYOF(s_renderTarget) == RT_TYPE_NUM);
		CGfxUtility::SetUpRenderTarget(s_renderTarget, RT_TYPE_NUM);

		if (!CGfxUtility::Initialize(MEM_ALLOCATOR(MEM_ID_DRAW_OBJ)))
		{
			PRINT("************[Error] Failed : CGfxUtility::Initialize()\n");
			return false;
		}
	}
	
	//---------------------------------------------------------------------
	// 入力初期化
	{
		static CKeyboardWin s_keyboardWin;
		static CMouseWin s_mouseWin;
		CInputUtility::Setup(&s_keyboardWin);
		CInputUtility::Setup(&s_mouseWin);
	}

	return true;
}


///---------------------------------------------------------------------
///	プラットホーム更新
///---------------------------------------------------------------------
void	CApplication::PlatformStep()
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	// ウィンドウがリサイズor移動されていたら
	if (pDevice->IsResize())
	{
		u32 width, height;
		pDevice->GetClientSize(width, height);
		pDevice->ResizeTarget(width, height);

		//CRenderTarget* pRT = &CGfxUtility::GetRenderTarget(RT_POST);
		//pRT->ResizeTarget(width, height);
	}
}


///---------------------------------------------------------------------
///	プラットホーム終了
///---------------------------------------------------------------------
void	CApplication::PlatformFin()
{
	// キャッシュビルダー終了
	for (u32 idx = 0; idx < RENDERER_NUM; idx++)
	{
#ifdef POISON_DEBUG
		s_pPrimitiveQuery[idx].Finalize();
		s_pTimerQuery[idx].Finalize();
#endif // POISON_DEBUG

		s_vertexCacheBuilder[idx].Finalize();
		s_indexCacheBuilder[idx].Finalize();
		s_uniformCacheBuilder[idx].Finalize();
		s_rasterizerCacheBuilder[idx].Finalize();
		s_blendStateCacheBuilder[idx].Finalize();
		s_depthStateCacheBuilder[idx].Finalize();
		s_samplerCacheBuilder[idx].Finalize();
	}

	// 入力終了
	CInputUtility::Finalize();

	// グラフィクス終了
	CGfxUtility::Finalize();

	// シェーダライブラリ終了
	CShaderUtility::Finalize();

	// デバイス終了
	CDeviceUtility::Finalize();

	// ファイル終了
	CFileUtility::Finalize();
}

WORK_NAMESPACE_END

#endif // LIB_GFX_DX11

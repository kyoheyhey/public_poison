﻿
///-------------------------------------------------------------------------------------------------
/// define
#ifdef LIB_GFX_OPENGL
//#define USE_TEXT_SOURCE_SHADER
#endif // LIB_GFX_OPENGL


///-------------------------------------------------------------------------------------------------
/// include
#include "application.h"

#include "main.h"
#include "dev_def.h"
#include "memory/allocator_manager.h"
#include "test/test.h"
#include "primitive_shape/primitive_shape.h"

// オブジェクト
#include "object/object.h"
#include "object/component.h"

// タイム
#include "timer/timer.h"

// レンダーターゲット
#include "renderer/render_target.h"

// グローバルパラム
#include "global_param/system_global_param.h"

// テスト描画
//#include "test/test_draw.h"
#include "test/test_draw_2.h"

// バッファオブジェクト
#include "object/color_buffer.h"
#include "object/depth_buffer.h"
#include "object/texture_buffer.h"
#include "object/uniform_buffer.h"
#include "object/vertex_buffer.h"

// デバッグ
#include "lib_debug_def.h"
#include "debug_win/debug_win_manager.h"
#include "debug_win/debug_win_loader.h"
#include "render_prim/render_prim.h"
#include "render_prim/render_prim_elm.h"


WORK_NAMESPACE_BGN


static CDebugWinManager	s_debugWinManager;





///---------------------------------------------------------------------
///	コンストラクタ・デストラクタ
///---------------------------------------------------------------------
//CApplication::CApplication()
//{
//}
//CApplication::~CApplication()
//{
//}


///---------------------------------------------------------------------
///	アプリケーションの初期化
///---------------------------------------------------------------------
b8	CApplication::Initialize()
{
	// メモリ管理初期化
	CAllocatorManager::Initialize();

	// タイマー初期化
	CTimeUtility::Initialize();

	// プラットフォーム別初期化
	if (PlatformInit() == false)
	{
		PRINT("************[Error] Failed : CApplication::Initialize()\n");
		return false;
	}

	// オブジェクト初期化
	CObjectUtility::Initialize();

	{
		// Imgui初期化
		CImguiUtility::Initialize();

		// デバッグウィンドウ初期化
		s_debugWinManager.Initialize(MEM_ID_DEBUG);
		CDebugWinLoader::Build(&s_debugWinManager, "common/debug/window/debug_window_list.cfg");

		CDebugWinUtility::Setup(&s_debugWinManager);

		// レンダープリミティブ初期化
		static CRenderPrimElm s_primElm[4096 * 2];
		if (!CRenderPrim::Initialize(s_primElm, ARRAYOF(s_primElm), SCRC32("render_prim")))
		{
			return false;
		}
	}

	// テスト関数
	TestFunc();

	// テスト描画初期化
	//CTestDraw::Initialize();
	CTestDraw2::Initialize();

	return true;
}


///---------------------------------------------------------------------
///	アプリケーション実行
///---------------------------------------------------------------------
void	CApplication::Run()
{
	b8 isRun = true;
	while (isRun)
	{
		// デバイスの実行状況確認
		isRun &= CDeviceUtility::IsRunning();

		// メモリ管理更新
		//CAllocatorManager::Update();

		// プラットホーム別更新
		PlatformStep();
		// タイマー更新
		CTimeUtility::Step();
		// 入力更新
		CInputUtility::Step();
		// Imgui更新
		CImguiUtility::Step();


		// テスト描画更新
		//CTestDraw::Step();
		CTestDraw2::Step();

		CRenderPrimElm elm;
		elm.SetAsBox(CVec3(2.0f, 1.0f, 1.0f), CVec3(-2.0f, -1.0f, -1.0f));
		elm.SetColor(1.0f, 0.0f, 0.0f);
		CRenderPrim::Add(elm);

		// プレステップ呼び出し
		CComponentFunc::CallStepAll<CComponent::PRE_STEP, CComponent::STEP_TYPE_IDX_PRE_STEP>();

		// ステップ呼び出し
		CComponentFunc::CallStepAll<CComponent::STEP, CComponent::STEP_TYPE_IDX_STEP>();

		// ポストステップ呼び出し
		CComponentFunc::CallStepAll<CComponent::POST_STEP, CComponent::STEP_TYPE_IDX_POST_STEP>();

		{
			// デバッグウィンドウの有効/無効設定
			const CKeyboard* pKey = CInputUtility::GetKeyboard();
			if (pKey->GetButton(CKeyboard::eF2).GetTrigger())
			{
				b8 enable = s_debugWinManager.IsEnable();
				enable ^= true;
				s_debugWinManager.SetEnable(enable);
			}
			// デバッグウィンドウ更新
			CDebugWinUtility::Step();
		}


		//---------------------------------------------------------------------
		// 描画コマンド待機
		CGfxUtility::WaitDrawCommand();

		// レンダーターゲットリサイズ
		{
			RENDER_TARGET_TYPE resizeRTs[] =
			{
				RT_POST,
			};
			for (u32 idx = 0; idx < ARRAYOF(resizeRTs); idx++)
			{
				CRenderTarget* pRT = &CGfxUtility::GetRenderTarget(idx);
				pRT->ResizeTarget(CSystemGlobalParam::GetInstance().m_displaySize[0], CSystemGlobalParam::GetInstance().m_displaySize[1]);
			}
		}

		// スワップバッファ
		{
			CGfxDevice* pDevice = SDCast<CGfxDevice*>(CDeviceUtility::GetDevice());
			libAssert(pDevice);
			// はみ出し部分のカラー指定
			pDevice->SetClearColor(CSystemGlobalParam::GetInstance().m_windowClearColor);
			CRenderTarget* pPostRT = &CGfxUtility::GetRenderTarget(RT_POST);
			CGfxUtility::SwapBuffer(
				pPostRT->GetColorTexture(0),
				CSystemGlobalParam::GetInstance().m_align,
				CSystemGlobalParam::GetInstance().m_stretch
			);
		}

		// Imgui描画
		CImguiUtility::Draw();

		// 垂直同期
		CGfxUtility::WaitVsync(CSystemGlobalParam::GetInstance().m_syncInterval);


		// テスト描画準備
		//CTestDraw::PrepareDraw();
		CTestDraw2::PrepareDraw();

		// オブジェクト実行
		CObjectUtility::DeleteExecute();

		// プリペアドローステップ呼び出し
		CComponentFunc::CallStepAll<CComponent::PREPARE_DRAW, CComponent::STEP_TYPE_IDX_PREPARE_DRAW>();

		// レンダープリミティブの領域塗りつぶし処理するために必要な処理
		{
			//CRenderPrim::SetAreaFillCamera();
			//CRenderTarget* pMainRT = &CGfxUtility::GetRenderTarget(RT_MAIN);
			//CRenderPrim::SetDepthTex(pMainRT->GetDepthTexture(RT_MAIN_DEP_0));
		}

		//---------------------------------------------------------------------
		// 描画開始
		CGfxUtility::DrawBegin();

		// 画面クリア設定
		{
			// 描画関数
			RENDER_FUNC renderFunc = [](CRenderer* _pRenderer, const void* _p0, const void* _p1)->b8
			{
				PUSH_RENDERER_MARKER(_pRenderer, "Clear");
				_pRenderer->PushRenderTarget();
				{
					// 画面クリア
					CRenderTarget* pRT = &CGfxUtility::GetRenderTarget(RT_MAIN);
					_pRenderer->SetRenderTarget(pRT, CGfxUtility::RtClrBit(RT_MAIN_CLR_0), RT_MAIN_DEP_0);
					_pRenderer->ClearSurface(CSystemGlobalParam::GetInstance().m_displayClearColor);
				}
				_pRenderer->PopRenderTarget();
				POP_RENDERER_MARKER(_pRenderer);
				return true;
			};
			CGfxUtility::AddDraw(renderFunc, NULL, NULL);
		}

		// テスト描画
		//CTestDraw::Draw();
		CTestDraw2::Draw();

		// ドローステップ呼び出し
		CComponentFunc::CallStepAll<CComponent::DRAW, CComponent::STEP_TYPE_IDX_DRAW>();


		{
			// 描画関数
			RENDER_FUNC renderFunc = [](CRenderer* _pRenderer, const void* _p0, const void* _p1)->b8
			{
				PUSH_RENDERER_MARKER(_pRenderer, "RenderPrim");
				_pRenderer->PushRenderTarget();
				// レンダーターゲット設定
				CRenderTarget* pRT = &CGfxUtility::GetRenderTarget(RT_MAIN);
				_pRenderer->SetRenderTarget(pRT, CGfxUtility::RtClrBit(RT_MAIN_CLR_0), RT_MAIN_DEP_0);
				return true;
			};
			CGfxUtility::AddDraw(renderFunc, NULL, NULL);
		}
		// デバッグ描画
		CRenderPrim::Draw();
		CRenderPrim::DrawEnd();
		{
			// 描画関数
			RENDER_FUNC renderFunc = [](CRenderer* _pRenderer, const void* _p0, const void* _p1)->b8
			{
				_pRenderer->PopRenderTarget();
				POP_RENDERER_MARKER(_pRenderer);
				return true;
			};
			CGfxUtility::AddDraw(renderFunc, NULL, NULL);
		}

		// ポストレンダーターゲットコピー
		{
			// 描画関数
			RENDER_FUNC renderFunc = [](CRenderer* _pRenderer, const void* _p0, const void* _p1)->b8
			{
				PUSH_RENDERER_MARKER(_pRenderer, "PostCopyDraw");
				// シェーダ設定
				const PassShader* pCopy = CGfxUtility::GetCopyShader();
				_pRenderer->SetShader(pCopy);
				// 頂点設定
				const CVertexBuffer& vtxBuf = CGfxUtility::GetScreenVtx();
				_pRenderer->SetVertex(&vtxBuf);
				// テクスチャ設定
				CRenderTarget* pMainRT = &CGfxUtility::GetRenderTarget(RT_MAIN);
				_pRenderer->SetTexture(GFX_SHADER_PIXEL, TEXTURE_SLOT_00, pMainRT->GetColorTexture(RT_MAIN_CLR_0));
				// 描画
				_pRenderer->DrawPrim(GFX_PRIMITIVE_QUADS, vtxBuf.GetVtxNum());
				POP_RENDERER_MARKER(_pRenderer);
				return true;
			};
			CGfxUtility::AddDraw(renderFunc, NULL, NULL);
		}

		// 描画フラッシュ
		CGfxUtility::DrawFlush();

		//---------------------------------------------------------------------
		// 描画終了
		CGfxUtility::DrawEnd();
	}

	// メインループ抜ける前に描画コマンド待機
	CGfxUtility::WaitDrawCommand();
}

///---------------------------------------------------------------------
///	アプリケーションの終了処理
///---------------------------------------------------------------------
void	CApplication::Finalize()
{
	// テスト描画終了
	//CTestDraw::Finalize();
	CTestDraw2::Finalize();

	// レンダープリミティブ後処理
	CRenderPrim::Finalize();
	// デバッグウィンドウ終了
	s_debugWinManager.Finalize();
	// Imgui終了
	CImguiUtility::Finalize();

	// オブジェクト終了
	CObjectUtility::Finalize();

	// プラットフォーム別終了
	PlatformFin();

	// タイマー終了
	CTimeUtility::Finalize();
	// メモリ管理終了
	CAllocatorManager::Finalize();
}


WORK_NAMESPACE_END

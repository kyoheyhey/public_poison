﻿
#include "main.h"
#include "work_def.h"
#include "application/application.h"

using namespace WORK_NAMESPACE;
//WORK_NAMESPACE_BGN

s32	main(s32 argc, c8* argv[])
{
	if (CApplication::GetInstance().Initialize())
	{
		CApplication::GetInstance().Run();
		CApplication::GetInstance().Finalize();
	}

	return 0;
}

//WORK_NAMESPACE_END

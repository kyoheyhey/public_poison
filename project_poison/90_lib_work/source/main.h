﻿#pragma once
#ifndef __MAIN_H__
#define __MAIN_H__


WORK_NAMESPACE_BGN

enum MEM_HDL_TYPE : u32
{
	MEM_ID_INVALID = 0,			// 無効
	MEM_ID_CONSTANT,			// 固定リソース
	MEM_ID_SYSTEM,				// システム
	MEM_ID_GRAPHICS,			// グラフィクス
	MEM_ID_DRAW_OBJ,			// 描画オブジェクト
	MEM_ID_RENDER_THREAD,		// レンダラースレッド
	MEM_ID_ONE_FRAME,			// １フレーム
	MEM_ID_OBJECT,				// オブジェクト
	MEM_ID_RESOURCE,			// リソース
	MEM_ID_WORK,				// ワーク
	MEM_ID_DEBUG,				// デバッグ

	MEM_ID_TEST0,			// テスト０
	MEM_ID_TEST1,			// テスト１

	MEM_ID_NUM,			// アロケータ数
};

#define	WINDOW_NAME				"work"
#define DEFAULT_WINDOW_WIDTH	1280
#define DEFAULT_WINDOW_HEIGHT	720
#define DEFAULT_WINDOW_POSX		30
#define DEFAULT_WINDOW_POSY		20

#define DEFAULT_FPS				60

#define RENDERER_NUM			1

WORK_NAMESPACE_END

#endif	// __MAIN_H__

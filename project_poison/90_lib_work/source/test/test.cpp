﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "test.h"
#include "main.h"

#include "thread/thread_utility.h"
#include "thread/thread.h"

#include "file/file_utility.h"

#include "timer/timer.h"


#include "memory/allocator_manager.h"

// 乱数
#include <random>

#include "test_cfg_loader.h"

#include "resource/resource_base.h"

#include "object/object.h"
#include "object/property.h"
#include "object/component.h"


///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant



WORK_NAMESPACE_BGN

LINK_CLASS(CTestList)
{
public:
	CTestList();
	~CTestList();

public:
	u32 m_count = 0;
};

CTestList::CTestList()
{
	m_count = CTestList::GetNodeNum();
}
CTestList::~CTestList()
{
}


STATIC_SINGLETON(CTest)
{
	FRIEND_STATIC_SINGLETON(CTest);

protected:
	CTest() {}

public:
	u32 test = 10;
};

DYNAMIC_SINGLETON(CKaji)
{
	FRIEND_DYNAMIC_SINGLETON(CKaji);

protected:
	CKaji()
		: test(20)
	{}

public:
	u32 test = 2;
};


RES_CLASS(CResTest)
{
public:
	CResTest() {}
	~CResTest() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	解放処理
	virtual	void	Release() override
	{
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	リソース構築済みか
	virtual b8		IsEnable() const override
	{
		return true;
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	名前のCRC取得・設定
	virtual hash32	GetNameCrc() const override { return m_nameCrc; }
	virtual void	SetNameCrc(const hash32 _name) override { m_nameCrc = _name; }

private:
	hash32	m_nameCrc;
};

static CResManager<CResTest>	s_resTestManager;



PROPERTY(CTestProp)
{
public:
	CTestProp() {}
	~CTestProp() {}

private:
	hash32	m_nameCrc;
};


LINK_PROPERTY(CTestProp2)
{
public:
	CTestProp2() {}
	~CTestProp2() {}

private:
	hash32	m_nameCrc;
};


COMPONENT(CTestComp)
{
public:
	enum { ENABLE_STEP = STEP};

public:
	CTestComp() {}
	~CTestComp() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	オーバーライド用Step関数
	virtual void	Step() override
	{
		u32 test = 0;
		test += 1;
	}
};


WORK_NAMESPACE_END

// クラス登録
REGIST_NAMED_CLASS_NAME(WORK_NAMESPACE, CTestProp);
REGIST_NAMED_CLASS_NAME(WORK_NAMESPACE, CTestProp2);
REGIST_NAMED_CLASS_NAME(WORK_NAMESPACE, CTestComp);

WORK_NAMESPACE_BGN

//-------------------------------------------------------------------------------------------------
/// テスト関数
void	TestFunc()
{
	CTestProp* pTestProp = SCast<CTestProp*>(CPropertyCreator::Create(MEM_ID_DEBUG, &CTestProp::GetPropertyCreator()));
	CTestProp2* pTestProp2 = SCast<CTestProp2*>(CPropertyCreator::Create(MEM_ID_DEBUG, &CTestProp2::GetPropertyCreator()));

	CObject* pTestObj = CObjectUtility::Create(MEM_ID_DEBUG, "testObj");
	if (pTestObj)
	{
		pTestObj->CreateProperty<CTestProp>();
		pTestObj->CreateProperty<CTestProp2>();
		pTestObj->CreateProperty<CTestComp>();

		CTestProp* pTestObjProp = pTestObj->SearchProperty<CTestProp>();

		pTestObj->DeleteProperty(pTestObjProp);

		// ステップ呼び出し
		CComponentFunc::CallStepAll<CComponent::STEP, CComponent::STEP_TYPE_IDX_STEP>();


		CObjectUtility::Delete(pTestObj);
	}

	CPropertyCreator::Delete(pTestProp);
	CPropertyCreator::Delete(pTestProp2);


	// タイマー初期化
	CTimer timer;
	timer.Initialize();


	// スレッドテスト
	{
		CThreadUtility::Initialize();

		auto resultPrint = [](const hash32* _pCrc, u32 _num)
		{
			PRINT("//*****************\n");
			for (u32 i = 0; i < _num; i++, _pCrc++)
			{
				PRINT("%d, ", (*_pCrc));
			}
			PRINT("\n");
		};

		hash32 crc0[256] = {};
		hash32 crc1[256] = {};
		hash32 crc2[256] = {};
		auto TestThread0RmdFunc = [](void* _arg)->THREAD_RESULT
		{
			hash32* pCrc = PCast<hash32*>(_arg);
			for (u32 i = 0; i < ARRAYOF(crc0); i++, pCrc++)
			{
				c8 str[32];
				sprintf_safe(str, "thread0_%d", i);
				(*pCrc) = CRC32(str);
			}

			THREAD_HANDLE threadHdl = CThreadUtility::GetCurrentThreadHdl();
			u64 threadId = CThreadUtility::GetThreadID(threadHdl);
			if (CThreadUtility::IsMainThread(threadId))
			{
				threadId = 0;
			}
			return CThreadUtility::Exit(THREAD_FUNC_RESULT(0));
		};
		auto TestThread1RmdFunc = [](void* _arg)->THREAD_RESULT
		{
			hash32* pCrc = PCast<hash32*>(_arg);
			for (u32 i = 0; i < ARRAYOF(crc1); i++, pCrc++)
			{
				c8 str[32];
				sprintf_safe(str, "thread1_%d", i);
				(*pCrc) = CRC32(str);
			}

			THREAD_HANDLE threadHdl = CThreadUtility::GetCurrentThreadHdl();
			u64 threadId = CThreadUtility::GetThreadID(threadHdl);
			if (CThreadUtility::IsMainThread(threadId))
			{
				threadId = 0;
			}
			return CThreadUtility::Exit(THREAD_FUNC_RESULT(1));
		};

		f64 aveTime = 0.0;
		const u32 loopNum = 1;
		for (u32 i = 0; i < loopNum; i++)
		{
			timer.SetMeasure();
			CThread thread0, thread1;

			if (!thread0.Create(TestThread0RmdFunc, crc0, sizeof(crc0), 0, "test0"))
			{
				TestThread0RmdFunc(crc0);
			}
			if (!thread1.Create(TestThread1RmdFunc, crc1, sizeof(crc1), 0, "test1"))
			{
				TestThread1RmdFunc(crc1);
			}

			if(0)
			{
				hash32* pCrc = crc2;
				for (u32 i = 0; i < ARRAYOF(crc1); i++, pCrc++)
				{
					c8 str[32];
					sprintf_safe(str, "main_%d", i);
					(*pCrc) = CRC32(str);
				}
			}
			thread0.Wait();
			thread1.Wait();

			thread0.Delete();
			thread1.Delete();

			f64 time = timer.GetTime();
			aveTime += time;
		}
		aveTime /= loopNum;
		PRINT("threadTime [%f]ms\n", SCast<f32>(aveTime * 1000.0));
		resultPrint(crc0, ARRAYOF(crc0));
		resultPrint(crc1, ARRAYOF(crc1));
		resultPrint(crc2, ARRAYOF(crc2));


		struct ARG_PARAM
		{
			hash32*			pCrc = nullptr;
			THREAD_EVENT*	pEvent0 = nullptr;
			THREAD_EVENT*	pEvent1 = nullptr;
		};
		auto TestThread00RmdFunc = [](void* _arg)->THREAD_RESULT
		{
			ARG_PARAM* pArg = PCast<ARG_PARAM*>(_arg);

			CThreadUtility::WaitEvent(*pArg->pEvent0);

			hash32* pCrc = pArg->pCrc;
			for (u32 i = 0; i < ARRAYOF(crc0); i++, pCrc++)
			{
				c8 str[32];
				sprintf_safe(str, "thread00_%d", i);
				(*pCrc) = CRC32(str);
			}

			CThreadUtility::SignalEvent(*pArg->pEvent1);

			return CThreadUtility::Exit(THREAD_FUNC_RESULT(0));
		};
		auto TestThread11RmdFunc = [](void* _arg)->THREAD_RESULT
		{
			ARG_PARAM* pArg = PCast<ARG_PARAM*>(_arg);

			CThreadUtility::WaitEvent(*pArg->pEvent0);

			hash32* pCrc = pArg->pCrc;
			for (u32 i = 0; i < ARRAYOF(crc1); i++, pCrc++)
			{
				c8 str[32];
				sprintf_safe(str, "thread11_%d", i);
				(*pCrc) = CRC32(str);
			}

			CThreadUtility::SignalEvent(*pArg->pEvent1);

			return CThreadUtility::Exit(THREAD_FUNC_RESULT(0));
		};
		{
			CThread thread0, thread1;
			THREAD_EVENT event0, event1;
			THREAD_EVENT event01, event11;

			// イベント生成
			CThreadUtility::CreateEvent(event0, "testEvent0");
			CThreadUtility::CreateEvent(event1, "testEvent1");
			CThreadUtility::CreateEvent(event01, "testEvent01");
			CThreadUtility::CreateEvent(event11, "testEvent11");

			ARG_PARAM arg0;
			arg0.pCrc = crc0;
			arg0.pEvent0 = &event0;
			arg0.pEvent1 = &event1;

			ARG_PARAM arg1;
			arg1.pCrc = crc1;
			arg1.pEvent0 = &event01;
			arg1.pEvent1 = &event11;

			aveTime = 0.0;
			for (u32 i = 0; i < loopNum; i++)
			{
				timer.SetMeasure();

				if (!thread0.Create(TestThread00RmdFunc, &arg0, sizeof(arg0), 0, "test2"))
				{
					thread0.Delete();
					TestThread00RmdFunc(&arg0);
				}
				if (!thread1.Create(TestThread11RmdFunc, &arg1, sizeof(arg1), 0, "test3"))
				{
					thread1.Delete();
					TestThread11RmdFunc(&arg1);
				}

				{
					hash32* pCrc = crc2;
					for (u32 i = 0; i < ARRAYOF(crc1); i++, pCrc++)
					{
						c8 str[32];
						sprintf_safe(str, "main_%d", i);
						(*pCrc) = CRC32(str);
					}
				}

				CThreadUtility::SignalEvent(event0);
				CThreadUtility::SignalEvent(event01);
				CThreadUtility::WaitEvent(event1, 1);
				CThreadUtility::WaitEvent(event11);

				f64 time = timer.GetTime();
				aveTime += time;
			}
			aveTime /= loopNum;
			PRINT("threadTime [%f]ms\n", SCast<f32>(aveTime * 1000.0));

			thread0.Delete();
			thread1.Delete();

			CThreadUtility::DeleteEvent(event0);
			CThreadUtility::DeleteEvent(event1);
			CThreadUtility::DeleteEvent(event01);
			CThreadUtility::DeleteEvent(event11);
		}

		resultPrint(crc0, ARRAYOF(crc0));
		resultPrint(crc1, ARRAYOF(crc1));
		resultPrint(crc2, ARRAYOF(crc2));
	}



	{
		c8 buf[256];
		FILE_HANDLE fHdl = CFileUtility::Open("common/test.txt");
		if (fHdl != FILE_HANDLE_INVALID)
		{
			s32 fSize = 0;
			CFileUtility::GetFileSize(fHdl, &fSize);
			CFileUtility::Read(fHdl, buf, fSize);
			buf[fSize] = '\0';
		}
		PRINT("%s\n", buf);
		PRINT("\n");
		CFileUtility::Close(fHdl);
	}

	{
		u32 idx = 0;
		CLinkList<u32>	LinkList;
		LinkList.Initialize(MEM_ID_DEBUG);
		LinkList.PushBack(idx++);
		LinkList.PushBack(idx++);
		LinkList.PushBack(idx++);
		LinkList.PushBack(idx++);

		CLinkList<u32>::CItr itr = LinkList.Begin();
		while (itr != LinkList.End())
		{
			PRINT("[%d]\n", (*itr));
			++itr;
		}
		LinkList.Finalize();
	}

	{
	PRINT("%d\n", CTest::GetInstance().test);
	CTestList test0;
	CTestList test1;
	CTestList test2;
	CTestList test3;
	CTestList test4;
	CTestList* pLinkNode = CTestList::GetNodeTop();
	while (pLinkNode)
	{
		PRINT("LinkNode[%d]\n", pLinkNode->m_count);
		pLinkNode = pLinkNode->GetNodeNext();
	}
}

	{
		s32 idx = 0;
		CIdxList<s32>	idxList;
		idxList.Initialize(MEM_ID_DEBUG, 8);
		idxList.PushBack(idx++);
		idxList.PushBack(idx++);
		idxList.PushBack(idx++);
		idxList.PushBack(idx++);
		CIdxList<s32>::ITR* itr = idxList.GetBgn();
		while (itr)
		{
			CIdxList<s32>::ITR* next = idxList.GetNext(itr);
			s32 data = idxList.GetDataFromItr(itr);
			PRINT("%d\n", data);
			itr = next;
		}
		PRINT("\n");
		idx = 0;
		idxList.PopBack();
		idxList.PushFront(idx--);
		idxList.PopBack();
		idxList.PushFront(idx--);
		idxList.PopBack();
		idxList.PushFront(idx--);
		idxList.PopBack();
		idxList.PushFront(idx--);
		itr = idxList.GetBgn();
		while (itr)
		{
			CIdxList<s32>::ITR* next = idxList.GetNext(itr);
			s32 data = idxList.GetDataFromItr(itr);
			PRINT("%d\n", data);
			itr = next;
		}
		PRINT("\n");
		idx = 0;
		idxList.PopFront();
		idxList.PushBack(idx++);
		idxList.PopFront();
		idxList.PushBack(idx++);
		idxList.PopFront();
		idxList.PushBack(idx++);
		idxList.PopFront();
		idxList.PushBack(idx++);
		itr = idxList.GetBgn();
		while (itr)
		{
			CIdxList<s32>::ITR* next = idxList.GetNext(itr);
			s32 data = idxList.GetDataFromItr(itr);
			PRINT("%d\n", data);
			itr = next;
		}
		PRINT("\n");

		idx = 0;
		u16 count = 0;
		itr = idxList.GetBgn();
		while (itr)
		{
			if (count == 1)
			{
				idxList.Erase(itr);
				break;
			}
			itr = idxList.GetNext(itr);
			count++;
		}
		count = 0;
		itr = idxList.GetBgn();
		while (itr)
		{
			if (count == 1)
			{
				idxList.InsertFront(--idx, itr);
				break;
			}
			itr = idxList.GetNext(itr);
			count++;
		}
		count = 0;
		itr = idxList.GetBgn();
		while (itr)
		{
			if (count == 1)
			{
				idxList.Erase(itr);
				break;
			}
			itr = idxList.GetNext(itr);
			count++;
		}
		count = 0;
		itr = idxList.GetBgn();
		while (itr)
		{
			if (count == 1)
			{
				idxList.InsertFront(--idx, itr);
				break;
			}
			itr = idxList.GetNext(itr);
			count++;
		}
		itr = idxList.GetBgn();
		while (itr)
		{
			CIdxList<s32>::ITR* next = idxList.GetNext(itr);
			s32 data = idxList.GetDataFromItr(itr);
			PRINT("%d\n", data);
			itr = next;
		}
		PRINT("\n");
		idxList.Finalize();
	}

	{
		struct TEST_PARAM
		{
		hash32	m_nameCrc;
		u32		m_param;

		TEST_PARAM()
			: m_nameCrc(hash32::Invalid)
			, m_param(0)
		{}
		hash32	GetNameCrc() const { return m_nameCrc; }
		void	SetNameCrc(hash32 _nameCrc) { m_nameCrc = _nameCrc; }
		};
		CIdxSortList<TEST_PARAM, hash32, CSortDataIF<TEST_PARAM, hash32, false>> sortList;
		sortList.Initialize(MEM_ID_DEBUG, 8);

		{
			TEST_PARAM param;
			param.m_nameCrc = SCRC32("test0");
			param.m_param = 0;
			sortList.PushBack(param);
		}
		{
			TEST_PARAM param;
			param.m_nameCrc = SCRC32("test1");
			param.m_param = 1;
			sortList.PushBack(param);
		}
		{
			TEST_PARAM param;
			param.m_nameCrc = SCRC32("test2");
			param.m_param = 2;
			sortList.PushBack(param);
		}
		{
			TEST_PARAM param;
			param.m_nameCrc = SCRC32("test3");
			param.m_param = 3;
			sortList.PushBack(param);
		}
		{
			TEST_PARAM param;
			param.m_nameCrc = SCRC32("test2");
			param.m_param = 2;
			sortList.PushBack(param);
		}
		{
			TEST_PARAM param;
			param.m_nameCrc = SCRC32("test3");
			param.m_param = 3;
			sortList.PushBack(param);
		}
		{
			TEST_PARAM param;
			param.m_nameCrc = SCRC32("test2");
			param.m_param = 2;
			sortList.PushBack(param);
		}
		u16 sortIdx = sortList.SearchSortIdx(SCRC32("test2"));
		do
		{
			TEST_PARAM& rParam = sortList.GetDataFromSortIdx(sortIdx);
			PRINT("sort %d\n", rParam.m_param);
			sortIdx++;
		} while (sortList.GetKeyFromSortIdx(sortIdx) == SCRC32("test2"));
		PRINT("\n");
		sortList.Finalize();
	}

	//s64 sTest = LONG_MIN;
	//sTest = static_cast<s64>(FLT_MAX);
	//sTest = static_cast<s64>(-FLT_MAX);
	//sTest = static_cast<s64>(floor(-FLT_MAX));
	//sTest = static_cast<s64>(floor(-FLT_MAX));
	//sTest = -9223372036854775807ll - 1;
	//sTest = -9223372036854775807ll;
	//sTest = (s64)floor(-FLT_MAX);
	//sTest = 0;

	//u64 uTest = LONG_MAX;
	//uTest = static_cast<u64>(FLT_MAX);
	//uTest = static_cast<u64>(-FLT_MAX);
	//uTest = static_cast<u64>(floor(FLT_MAX));
	//uTest = 9223372036854775807ull;
	//uTest = (u64)floor(FLT_MAX);
	//uTest = 0;

	//s64 s64Test = LONG_MIN;
	//s64Test = static_cast<s64>(DBL_MAX);
	//s64Test = static_cast<s64>(-DBL_MAX);
	//s64Test = static_cast<s64>(floor(-DBL_MAX));
	//s64Test = -9223372036854775807ll - 1;
	//s64Test = -9223372036854775807ll;
	//s64Test = 0;

	//u64 u62Test = LONG_MAX;
	//u62Test = static_cast<u64>(DBL_MAX);
	//u62Test = static_cast<u64>(-DBL_MAX);
	//u62Test = static_cast<u64>(floor(DBL_MAX));
	//u62Test = 9223372036854775807ull;
	//u62Test = 0;



	{
		
	}
	{
		typedef void(*CMD_DRAW_FUNC)(u32 _argc, void* _argv[]);
		struct CMD_DRAW_PACKET
		{
			CMD_DRAW_FUNC	func = NULL;
			void**			argv = NULL;
			u32				argc = 0;
		};
		auto	testFunc = [](u32 _argc, void* _argv[])
		{
			f32 testF = *PCast<f32*>(_argv[0]);
			u32 testU = *PCast<u32*>(_argv[1]);
			b8 testB = *PCast<b8*>(_argv[2]);
			c8* testC = *PCast<c8**>(_argv[3]);
			libAssert(_argc >= 4);
		};

		f32 testF = 100.0f;
		u32 testU = 2;
		b8 testB = true;
		c8* testC = "test";

		u32 argc = 4;
		u32 argvSize = sizeof(void*) * argc;
		u32 bufSize = sizeof(testF) + sizeof(testU) + sizeof(testB) + sizeof(testC);
		u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(MEM_ID_TEST1, 16, argvSize + bufSize));
		libAssert(pTop);
		u8* pBufTop = PCast<u8*>(pTop + argvSize);
		f32* pBufF32 = PCast<f32*>(pBufTop);
		pBufF32[0] = testF;
		u32* pBufU32 = PCast<u32*>(&pBufF32[1]);
		pBufU32[0] = testU;
		b8* pBufB8 = PCast<b8*>(&pBufU32[1]);
		pBufB8[0] = testB;
		c8** pBufC8 = PCast<c8**>(&pBufB8[1]);
		pBufC8[0] = testC;

		CMD_DRAW_PACKET packet;
		packet.argc = argc;
		packet.argv = PCast<void**>(pTop);
		packet.argv[0] = &pBufF32[0];
		packet.argv[1] = &pBufU32[0];
		packet.argv[2] = &pBufB8[0];
		packet.argv[3] = &pBufC8[0];
		packet.func = testFunc;

		packet.func(packet.argc, packet.argv);

		MEM_FREE(MEM_ID_TEST1, pTop);
	}

#if 0
	timer.SetMeasure();
	{
		void* p[10] = {};
		u32 mallocNum = 0;
		u32 loopCount = 0;
		while (mallocNum < ARRAYOF(p))
		{
			// 生成
			for (u32 i = 0; i < ARRAYOF(p); i++)
			{
				if (p[i] == NULL)
				{
					u32 mallocSize = i + 1;
					void* ptr = MEM_MALLOC(MEM_ID_TEST0, mallocSize);
					if (ptr)
					{
						p[i] = ptr;
						++mallocNum;
					}
					break;
				}
			}
			// 破棄
			if (mallocNum > 0 && loopCount % 2 == 0)
			{
				for (u32 i = 0; i < ARRAYOF(p); i++)
				{
					if (p[i] != NULL)
					{
						MEM_FREE(MEM_ID_TEST0, p[i]);
						p[i] = NULL;
						--mallocNum;
						break;
					}
				}
			}
			++loopCount;
		}
		// 全破棄
		for (u32 i = 0; i < ARRAYOF(p); i++)
		{
			if (p[i] != NULL)
			{
				MEM_FREE(MEM_ID_TEST0, p[i]);
				p[i] = NULL;
			}
		}
		f64 time = timer.GetTime();
		PRINT("BoundaryTag Allocator  time %fs\n", time);
	}


	timer.SetMeasure();
	{
		void* p[10] = {};
		u32 mallocNum = 0;
		u32 loopCount = 0;
		while (mallocNum < ARRAYOF(p))
		{
			// 生成
			for (u32 i = 0; i < ARRAYOF(p); i++)
			{
				if (p[i] == NULL)
				{
					u32 mallocSize = i + 1;
					void* ptr = MEM_MALLOC(MEM_ID_DEBUG, mallocSize);
					if (ptr)
					{
						p[i] = ptr;
						++mallocNum;
					}
					break;
				}
			}
			// 破棄
			if (mallocNum > 0 && loopCount % 2 == 0)
			{
				for (u32 i = 0; i < ARRAYOF(p); i++)
				{
					if (p[i] != NULL)
					{
						MEM_FREE(MEM_ID_DEBUG, p[i]);
						p[i] = NULL;
						--mallocNum;
						break;
					}
				}
			}
			++loopCount;
		}
		// 全破棄
		for (u32 i = 0; i < ARRAYOF(p); i++)
		{
			if (p[i] != NULL)
			{
				MEM_FREE(MEM_ID_DEBUG, p[i]);
				p[i] = NULL;
			}
		}
		f64 time = timer.GetTime();
		PRINT("winNew Allocator  time %fs\n", time);
	}
#endif // 0


	if(0)
	{
		void* p[10] = {};
		u32 mallocNum = 0;
		std::random_device rndDev;		// 非決定的な乱数生成器を生成
		std::mt19937 mt(rndDev());		// メルセンヌ・ツイスタの32ビット版、引数は初期シード値
		std::uniform_int_distribution<>		randU32(1, 100);	// [1, 10] 範囲の一様乱数
		//std::uniform_real_distribution<u32>	randU32(1, 10);			// [1, 10] 範囲の一様乱数
		std::uniform_real_distribution<f32>	randF32(0.0f, 1.0f);	// [0.0, 1.0] 範囲の一様乱数
		while (mallocNum < ARRAYOF(p))
		{
			// 生成
			for (u32 i = 0; i < ARRAYOF(p); i++)
			{
				if (p[i] == NULL)
				{
					u32 mallocSize = randU32(mt);
					void* ptr = MEM_MALLOC(MEM_ID_TEST0, mallocSize);
					if (ptr)
					{
						p[i] = ptr;
						++mallocNum;
					}
					break;
				}
			}
			// 破棄
			if (mallocNum > 0 && randF32(mt) > 0.75f)
			{
				u32 freeIdx = 0;
				u32 uRnd = SCast<u32>(mallocNum * randF32(mt));
				for (u32 i = 0; i < ARRAYOF(p); i++)
				{
					if (p[i] != NULL)
					{
						if (uRnd <= 0)
						{
							freeIdx = i;
							break;
						}
						--uRnd;
					}
				}
				MEM_FREE(MEM_ID_TEST0, p[freeIdx]);
				p[freeIdx] = NULL;
				--mallocNum;
			}
		}
		// 全破棄
		for (u32 i = 0; i < ARRAYOF(p); i++)
		{
			if (p[i] != NULL)
			{
				MEM_FREE(MEM_ID_TEST0, p[i]);
				p[i] = NULL;
			}
		}
	}

	{
		std::random_device rndDev;		// 非決定的な乱数生成器を生成
		std::mt19937 mt(rndDev());		// メルセンヌ・ツイスタの32ビット版、引数は初期シード値
		std::uniform_int_distribution<>		randNum(1, 10);	// [1, 10] 範囲の一様乱数
		std::uniform_int_distribution<>		randSize(1, 10);	// [1, 10] 範囲の一様乱数
		//std::uniform_real_distribution<u32>	randU32(1, 10);			// [1, 10] 範囲の一様乱数
		std::uniform_real_distribution<f32>	randF32(0.0f, 1.0f);	// [0.0, 1.0] 範囲の一様乱数

		s32 loopNum = 10;
		while (loopNum > 0)
		{
			u32 mallocNum = randNum(mt);
			u32 mallocSize = randSize(mt);
			for (u32 i = 0; i < mallocNum; i++)
			{
				void* ptr = MEM_MALLOC(MEM_ID_TEST1, mallocSize);
				if (ptr == NULL)
				{
					break;
				}
			}
			--loopNum;

			// メモリ管理更新
			CAllocatorManager::Update();
		}
	}

	{
		c8 buf[1024];
		s32 fSize = 0;
		FILE_HANDLE fHdl = CFileUtility::Open("common/test_cfg.cfg");
		if (fHdl != FILE_HANDLE_INVALID)
		{
			CFileUtility::GetFileSize(fHdl, &fSize);
			CFileUtility::Read(fHdl, buf, fSize);
			buf[fSize] = '\0';
		}
		PRINT("%s\n", buf);
		PRINT("\n");
		CFileUtility::Close(fHdl);

		CTestCfgLoader::BuildResource(buf, fSize);
	}


	{
		s_resTestManager.Initialize(MEM_ALLOCATOR(MEM_ID_DEBUG), 10);

		CResTest* pRes = s_resTestManager.SearchAndCreate(CRC32("test"));
		if (pRes)
		{
			pRes->AddRefCount();
			s_resTestManager.CheckDelete();

			pRes->DelRefCount();
			s_resTestManager.CheckDelete();
		}

		s_resTestManager.Finalize();
	}

	// タイマー終了
	timer.Finalize();
}


WORK_NAMESPACE_END

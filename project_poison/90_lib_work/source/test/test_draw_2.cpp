﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "test_draw_2.h"

#include "main.h"
#include "dev_def.h"

#include "primitive_shape/primitive_shape.h"

// タイム
#include "timer/timer.h"
// ファイル
#include "file/file_utility.h"

// レンダーターゲット
#include "renderer/render_target.h"

// シェーダ
#include "shader/shader.h"
#include "shader/shader_utility.h"
#include "resource/shader_library.h"

// バッファオブジェクト
#include "object/color_buffer.h"
#include "object/depth_buffer.h"
#include "object/texture_buffer.h"
#include "object/uniform_buffer.h"
#include "object/vertex_buffer.h"
#include "object/index_buffer.h"
#include "object/sampler_state.h"
#include "object/rasterrizer_state.h"
#include "object/blend_state.h"
#include "object/depth_state.h"

// query
#include "query/occlusion_query.h"
#include "query/primitive_query.h"
#include "query/timer_query.h"

// object
#include "object/object.h"
#include "object/property.h"
#include <functional>

// camera
#include "camera.h"
#include "camera_ctrl_maya.h"

// model
#include "model_resource.h"
#include "model_component.h"

// math
#include "math/geometry.h"

// debug
#include "imgui/imgui.h"
#include "debug_win/debug_win.h"


WORK_NAMESPACE_BGN

///-------------------------------------------------------------------------------------------------
/// constant

// シェーダ
static const FxShader*	s_p2dFxShader = NULL;
static const FxShader*	s_pNoiseFxShader = NULL;

// 頂点
static CVertexBuffer	s_planeVtxBuffer;
static CIndexBuffer		s_planeIdxBuffer;

// 位置
static CMtx44 s_mtxLW = CMtx44::Identity;
static CMtx44 s_mtxLWDrawble = CMtx44::Identity;
static CMtx44 s_mtxWVDrawble = CMtx44::Identity;
static CMtx44 s_mtxVPDrawble = CMtx44::Identity;

static CVec2 s_noiseUv = CVec2::Zero;
static CVec2 s_noiseUvDrawble = CVec2::Zero;

// カメラ
static CCamera			s_camera;
static CCameraCtrlMaya	s_cameraCtrl;


//-------------------------------------------------------------------------------------------------
///@brief	初期化
b8		CTestDraw2::Initialize()
{
	{
		CPlane plane;
		Geometry::SetPlane(plane, CVec3::Zero, CVec3::Up);

		FLT_EPSILON;
		FLT_MIN;
	}

	// 2Dシェーダ読み込み
	{
		// 頂点DECL
		VTX_DECL vtxDecl[] =
		{
			{ VTX_ATTR_POS, 0, GFX_VALUE_F32, 4, },
			{ VTX_ATTR_CLR0, sizeof(f32) * 4, GFX_VALUE_F32, 4, },
			{ VTX_ATTR_UV0, sizeof(f32) * 8, GFX_VALUE_F32, 2, },
		};
		s_p2dFxShader = CShaderUtility::SearchAndCreateFxShader(SCRC32("2d"), vtxDecl, ARRAYOF(vtxDecl));
		Assert(s_p2dFxShader);
	}

	// ノイズシェーダ読み込み
	{
		// 頂点DECL
		VTX_DECL vtxDecl[] =
		{
			{ VTX_ATTR_POS, 0, GFX_VALUE_F32, 4, },
			{ VTX_ATTR_CLR0, sizeof(f32) * 4, GFX_VALUE_F32, 4, },
			{ VTX_ATTR_UV0, sizeof(f32) * 8, GFX_VALUE_F32, 2, },
		};
		s_pNoiseFxShader = CShaderUtility::SearchAndCreateFxShader(SCRC32("noise"), vtxDecl, ARRAYOF(vtxDecl));
		Assert(s_pNoiseFxShader);
	}


	// 頂点バッファ生成
	{
		static const u32 s_divisionNum = 4;
		static const u32 s_edgeNum = s_divisionNum + 1;
		struct VERTEX
		{
			f32 pos[4];
			f32 clr[4];
			f32 uv[2];
		};
		VERTEX vertex[s_edgeNum * s_edgeNum];
		u32 idx = 0;
		for (u32 i = 0; i < s_edgeNum; ++i)
		{
			for (u32 j = 0; j < s_edgeNum; ++j)
			{
				vertex[idx].pos[0] = ((SCast<f32>(i) / s_divisionNum) - 0.5f) * 2.0f;
				vertex[idx].pos[1] = 0.0f;
				vertex[idx].pos[2] = ((SCast<f32>(j) / s_divisionNum) - 0.5f) * 2.0f;
				vertex[idx].pos[3] = 1.0f;
				vertex[idx].uv[0] = (SCast<f32>(j) / s_divisionNum);
				vertex[idx].uv[1] = 1.0f - (SCast<f32>(i) / s_divisionNum);
				vertex[idx].clr[0] = vertex[idx].clr[1] = vertex[idx].clr[2] = vertex[idx].clr[3] = 1.0f;
				++idx;
			}
		}
		VTX_DESC vtxDesc(vertex, sizeof(VERTEX), ARRAYOF(vertex), GFX_USAGE_STATIC);
		if (!s_planeVtxBuffer.Create(vtxDesc))
		{
			PRINT("[VBUFFER] 何がアカンかったんや...？\n");
		}

		u32 index[s_divisionNum * s_divisionNum * 6];
		idx = 0;
		for (u32 i = 0; i < s_divisionNum; ++i)
		{
			for (u32 j = 0; j < s_divisionNum; ++j)
			{
				auto calcIdx = [](u32 _x, u32 _y, u32 _division)->u32
				{
					return _x + _y * _division;
				};
				index[idx + 0] = calcIdx(j + 0, i + 0, s_edgeNum);
				index[idx + 1] = calcIdx(j + 1, i + 0, s_edgeNum);
				index[idx + 2] = calcIdx(j + 1, i + 1, s_edgeNum);

				index[idx + 3] = calcIdx(j + 1, i + 1, s_edgeNum);
				index[idx + 4] = calcIdx(j + 0, i + 1, s_edgeNum);
				index[idx + 5] = calcIdx(j + 0, i + 0, s_edgeNum);
				idx += 6;
			}
		}
		IDX_DESC idxDesc(index, sizeof(u32), ARRAYOF(index), GFX_USAGE_STATIC);
		if (!s_planeIdxBuffer.Create(idxDesc))
		{
			PRINT("[IBUFFER] 何がアカンかったんや...？\n");
		}
	}


	{
		s_mtxLW = CMtx44::Identity;
		s_mtxLW.SetTrans(CVec3(0.0f, 1.0f, 0.0f));
		s_camera.SetPos(CVec3(0.0f, 5.0f, 4.0f));
		s_camera.SetRef(CVec3(0.0f, 0.0f, 0.0f));
		s_cameraCtrl.Setup(&s_camera);
	}

	// リソースマネージャー初期化
	CModelResourceUtility::Initialize(MEM_ALLOCATOR(MEM_ID_RESOURCE));

	// マテリアルリソース生成
	{
		CResMaterialManager* pMatResMng = CModelResourceUtility::GetMaterialManager();
		{
			const c8* matName = "testMat0";
			CResMaterial* pResMat = pMatResMng->Create(CRC32(matName));
			if (pResMat)
			{
				CResMaterial::MATERIAL_DESC desc;
				desc.shaderName = SCRC32("test");

				RASTERIZER_DESC rasterrizerDesc;
				rasterrizerDesc.cullFace = GFX_CULL_FACE_BACK;
				desc.pRasterizer = &rasterrizerDesc;

				DEPTH_STATE_DESC depthStateDesc;
				depthStateDesc.isDepthTest = true;
				depthStateDesc.isDepthWrite = true;
				depthStateDesc.depthTest = GFX_TEST_LEQUAL;
				desc.pDepthState = &depthStateDesc;

				pResMat->Create(desc, matName);
			}
		}
	}


	// メッシュリソース生成
	{
		CResMeshManager* pMeshResMng = CModelResourceUtility::GetMeshManager();
		VTX_DECL vtxDecl[] =
		{
			{ VTX_ATTR_POS, 0, GFX_VALUE_F32, 4, VTX_ATTR_POS },
			{ VTX_ATTR_CLR0, 0, GFX_VALUE_F32, 4, VTX_ATTR_CLR0, },
			{ VTX_ATTR_UV0, 0, GFX_VALUE_F32, 2, VTX_ATTR_UV0, },
			{ VTX_ATTR_NRM, 0, GFX_VALUE_F32, 3, VTX_ATTR_NRM, },
		};

		{
			const c8* meshName = "plane";
			CResMesh* pResMesh = pMeshResMng->Create(CRC32(meshName));
			if (pResMesh)
			{
				CResMesh::MESH_DECS desc;
				desc.pVtxDecl = vtxDecl;
				desc.vtxDeclNum = ARRAYOF(vtxDecl);

				const CShapeUtility::SHAPE_INFO& rShapeInfo = CShapeUtility::GetPlane();
				VTX_DESC vtxDesc[] =
				{
					{ rShapeInfo.pVtxPos, sizeof(f32) * 4, rShapeInfo.vtxNum, GFX_USAGE_STATIC },
					{ rShapeInfo.pVtxClr, sizeof(f32) * 4, rShapeInfo.vtxNum, GFX_USAGE_STATIC },
					{ rShapeInfo.pVtxUv, sizeof(f32) * 2, rShapeInfo.vtxNum, GFX_USAGE_STATIC },
					{ rShapeInfo.pVtxNrm, sizeof(f32) * 3, rShapeInfo.vtxNum, GFX_USAGE_STATIC },
				};
				desc.pVtxDesc = vtxDesc;
				desc.vtxDescNum = ARRAYOF(vtxDesc);

				IDX_DESC idxDesc(rShapeInfo.pIdx, sizeof(u32), rShapeInfo.idxNum, GFX_USAGE_STATIC);
				desc.pIdxDesc = &idxDesc;

				desc.primitive = GFX_PRIMITIVE_TRIANGLES;

				pResMesh->Create(desc, meshName);
			}
		}

		{
			const c8* meshName = "cube";
			CResMesh* pResMesh = pMeshResMng->Create(CRC32(meshName));
			if (pResMesh)
			{
				CResMesh::MESH_DECS desc;
				desc.pVtxDecl = vtxDecl;
				desc.vtxDeclNum = ARRAYOF(vtxDecl);

				const CShapeUtility::SHAPE_INFO& rShapeInfo = CShapeUtility::GetCube();
				VTX_DESC vtxDesc[] =
				{
					{ rShapeInfo.pVtxPos, sizeof(f32) * 4, rShapeInfo.vtxNum, GFX_USAGE_STATIC },
					{ rShapeInfo.pVtxClr, sizeof(f32) * 4, rShapeInfo.vtxNum, GFX_USAGE_STATIC },
					{ rShapeInfo.pVtxUv, sizeof(f32) * 2, rShapeInfo.vtxNum, GFX_USAGE_STATIC },
					{ rShapeInfo.pVtxNrm, sizeof(f32) * 3, rShapeInfo.vtxNum, GFX_USAGE_STATIC },
				};
				desc.pVtxDesc = vtxDesc;
				desc.vtxDescNum = ARRAYOF(vtxDesc);

				IDX_DESC idxDesc(rShapeInfo.pIdx, sizeof(u32), rShapeInfo.idxNum, GFX_USAGE_STATIC);
				desc.pIdxDesc = &idxDesc;

				desc.primitive = GFX_PRIMITIVE_TRIANGLES;

				pResMesh->Create(desc, meshName);
			}
		}

		{
			const c8* meshName = "sphere";
			CResMesh* pResMesh = pMeshResMng->Create(CRC32(meshName));
			if (pResMesh)
			{
				CResMesh::MESH_DECS desc;
				desc.pVtxDecl = vtxDecl;
				desc.vtxDeclNum = ARRAYOF(vtxDecl);

				const CShapeUtility::SHAPE_INFO& rShapeInfo = CShapeUtility::GetSphere();
				VTX_DESC vtxDesc[] =
				{
					{ rShapeInfo.pVtxPos, sizeof(f32) * 4, rShapeInfo.vtxNum, GFX_USAGE_STATIC },
					{ rShapeInfo.pVtxClr, sizeof(f32) * 4, rShapeInfo.vtxNum, GFX_USAGE_STATIC },
					{ rShapeInfo.pVtxUv, sizeof(f32) * 2, rShapeInfo.vtxNum, GFX_USAGE_STATIC },
					{ rShapeInfo.pVtxNrm, sizeof(f32) * 3, rShapeInfo.vtxNum, GFX_USAGE_STATIC },
				};
				desc.pVtxDesc = vtxDesc;
				desc.vtxDescNum = ARRAYOF(vtxDesc);

				IDX_DESC idxDesc(rShapeInfo.pIdx, sizeof(u32), rShapeInfo.idxNum, GFX_USAGE_STATIC);
				desc.pIdxDesc = &idxDesc;

				desc.primitive = GFX_PRIMITIVE_TRIANGLES;

				pResMesh->Create(desc, meshName);
			}
		}

		{
			const c8* meshName = "lcosahedral";
			CResMesh* pResMesh = pMeshResMng->Create(CRC32(meshName));
			if (pResMesh)
			{
				CResMesh::MESH_DECS desc;
				desc.pVtxDecl = vtxDecl;
				desc.vtxDeclNum = ARRAYOF(vtxDecl);

				const CShapeUtility::SHAPE_INFO& rShapeInfo = CShapeUtility::GetLcosahedral();
				VTX_DESC vtxDesc[] =
				{
					{ rShapeInfo.pVtxPos, sizeof(f32) * 4, rShapeInfo.vtxNum, GFX_USAGE_STATIC },
					{ rShapeInfo.pVtxClr, sizeof(f32) * 4, rShapeInfo.vtxNum, GFX_USAGE_STATIC },
					{ rShapeInfo.pVtxUv, sizeof(f32) * 2, rShapeInfo.vtxNum, GFX_USAGE_STATIC },
					{ rShapeInfo.pVtxNrm, sizeof(f32) * 3, rShapeInfo.vtxNum, GFX_USAGE_STATIC },
				};
				desc.pVtxDesc = vtxDesc;
				desc.vtxDescNum = ARRAYOF(vtxDesc);

				IDX_DESC idxDesc(rShapeInfo.pIdx, sizeof(u32), rShapeInfo.idxNum, GFX_USAGE_STATIC);
				desc.pIdxDesc = &idxDesc;

				desc.primitive = GFX_PRIMITIVE_TRIANGLES;

				pResMesh->Create(desc, meshName);
			}
		}

		// モデル構築
		{
			CObject* pObj = CObjectUtility::Create(MEM_ID_OBJECT, "test_cube");
			if (pObj)
			{
				pObj->SetTrans(CVec3(2.0f, 0.0f, 0.0f));

				CModelComponent* pModelComp = pObj->CreateProperty<CModelComponent>();
				if (pModelComp)
				{
					CModelComponent::MAT_DESC matDescs[] =
					{
						{ SCRC32("testMat0"), NULL, 0 },
					};
					CModelComponent::MESH_DESC meshDescs[] =
					{
						{ SCRC32("cube"), SCRC32("testMat0") },
					};
					pModelComp->Create(pObj->GetMemHdl(), matDescs, ARRAYOF(matDescs), meshDescs, ARRAYOF(meshDescs));
				}
			}
			pObj = CObjectUtility::Create(MEM_ID_OBJECT, "test_shpere");
			if (pObj)
			{
				pObj->SetTrans(CVec3(-2.0f, 0.5f, 0.0f));

				CModelComponent* pModelComp = pObj->CreateProperty<CModelComponent>();
				if (pModelComp)
				{
					CModelComponent::MAT_DESC matDescs[] =
					{
						{ SCRC32("testMat0"), NULL, 0 },
					};
					CModelComponent::MESH_DESC meshDescs[] =
					{
						{ SCRC32("sphere"), SCRC32("testMat0") },
						{ SCRC32("plane"), SCRC32("testMat0") },
					};
					pModelComp->Create(pObj->GetMemHdl(), matDescs, ARRAYOF(matDescs), meshDescs, ARRAYOF(meshDescs));
				}
			}
		}
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
///@brief	更新
void	CTestDraw2::Step()
{
	// ステップタイプ
	const f32 stepTime = CTimeUtility::GetStepTime();

	// 入力
	b8 isUpdate = false;
	const CKeyboard* pKey = CInputUtility::GetKeyboard();
	const CMouse* pMouse = CInputUtility::GetMouse();

	CCameraCtrlMaya::UPDATA_PARAM param;
	param.trans = CVec3::Zero;

	static f32 s_transSpeed = 10.0f;
	if (pKey->GetButton(CKeyboard::eW).GetPush())
	{
		param.trans += CVec3::Back * s_transSpeed * stepTime;
		isUpdate |= true;
	}
	if (pKey->GetButton(CKeyboard::eA).GetPush())
	{
		param.trans += CVec3::Right * s_transSpeed * stepTime;
		isUpdate |= true;
	}
	if (pKey->GetButton(CKeyboard::eS).GetPush())
	{
		param.trans += CVec3::Front * s_transSpeed * stepTime;
		isUpdate |= true;
	}
	if (pKey->GetButton(CKeyboard::eD).GetPush())
	{
		param.trans += CVec3::Left * s_transSpeed * stepTime;
		isUpdate |= true;
	}
	if (pKey->GetButton(CKeyboard::eQ).GetPush())
	{
		param.trans += CVec3::Down * s_transSpeed * stepTime;
		isUpdate |= true;
	}
	if (pKey->GetButton(CKeyboard::eE).GetPush())
	{
		param.trans += CVec3::Up * s_transSpeed * stepTime;
		isUpdate |= true;
	}

	CVec2 pointerVec = CVec2::Zero;
	CCameraCtrlMaya::MOVE_TYPE type = CCameraCtrlMaya::FPS_ROT;
	if (pMouse->GetButton(CMouse::eRight).GetPush())
	{
		pMouse->GetPointer().GetMoveDir(pointerVec);
		param.type = CCameraCtrlMaya::FPS_ROT;
		isUpdate |= true;
	}
	else if (pMouse->GetButton(CMouse::eLeft).GetPush())
	{
		pMouse->GetPointer().GetMoveDir(pointerVec);
		param.type = CCameraCtrlMaya::TPS_ROT;
		isUpdate |= true;
	}
	else if (pMouse->GetButton(CMouse::eCenter).GetPush())
	{
		pMouse->GetPointer().GetMoveDir(pointerVec);
		param.type = CCameraCtrlMaya::TRANS;
		isUpdate |= true;
	}
	else if (pMouse->GetButton(CMouse::eRight).GetPush() && pKey->GetButton(CKeyboard::eAlt).GetPush())
	{
		pMouse->GetPointer().GetMoveDir(pointerVec);
		param.type = CCameraCtrlMaya::ZOOM;
		isUpdate |= true;
	}

	static f32 s_zoomSpeed = 1.0f;
	f32 moveWheel = pMouse->GetWheel().GetMove();
	if (moveWheel != 0.0f)
	{
		moveWheel = s_zoomSpeed * moveWheel;
		isUpdate |= true;
	}

	param.screenVec = CVec3(pointerVec, moveWheel);

	// カメラ更新
	if (isUpdate)
	{
		s_cameraCtrl.Update(param);
	}
	// アスペクト比更新
	const CRenderTarget& postRT = CGfxUtility::GetRenderTarget(RT_POST);
	s_camera.SetAspect(SCast<f32>(postRT.GetWidth()) / SCast<f32>(postRT.GetHeight()));
}


//-------------------------------------------------------------------------------------------------
///@brief	描画準備
void	CTestDraw2::PrepareDraw()
{
	s_mtxLWDrawble = s_mtxLW;
	s_mtxWVDrawble = s_camera.CalcViewMtx();
	s_mtxVPDrawble = s_camera.CalcProjectionMtx();
	s_noiseUvDrawble = s_noiseUv;

	// リソースマネージャー破棄更新
	CModelResourceUtility::CheckDelete();
}


//-------------------------------------------------------------------------------------------------
///@brief	描画
void	CTestDraw2::Draw()
{
	// ノイズ描画
	{
		// 描画関数
		RENDER_FUNC renderFunc = [](CRenderer* _pRenderer, const void* _p0, const void* _p1)->b8
		{
			PUSH_RENDERER_MARKER(_pRenderer, "Noise");
			_pRenderer->PushRenderTarget();
			{
				// レンダーターゲット設定
				CRenderTarget* pRT = &CGfxUtility::GetRenderTarget(RT_NOISE);
				_pRenderer->SetRenderTarget(pRT, CGfxUtility::RtClrBit(RT_NOISE_CLR_0), INVALID_RT_IDX);
				// シェーダ設定
				_pRenderer->SetShader(s_pNoiseFxShader);
				// 属性設定
				_pRenderer->SetAttribute(SCRC32("direct"));
				// 頂点設定
				const CVertexBuffer& vtxBuf = CGfxUtility::GetScreenVtx();
				_pRenderer->SetVertex(&vtxBuf);
				// 描画
				_pRenderer->DrawPrim(GFX_PRIMITIVE_QUADS, vtxBuf.GetVtxNum());
			}
			_pRenderer->PopRenderTarget();
			POP_RENDERER_MARKER(_pRenderer);
			return true;
		};
		CGfxUtility::AddDraw(renderFunc, NULL, NULL);
	}


	{
		// 描画関数
		RENDER_FUNC renderFunc = [](CRenderer* _pRenderer, const void* _p0, const void* _p1)->b8
		{
			PUSH_RENDERER_MARKER(_pRenderer, "GroundDraw");
			_pRenderer->PushDepthState();
			_pRenderer->PushRasterizer();
			_pRenderer->PushRenderTarget();
			{
				// レンダーターゲット設定
				CRenderTarget* pRT = &CGfxUtility::GetRenderTarget(RT_MAIN);
				_pRenderer->SetRenderTarget(pRT, CGfxUtility::RtClrBit(RT_MAIN_CLR_0), RT_MAIN_DEP_0);
				// シェーダ設定
				_pRenderer->SetShader(s_p2dFxShader);
				// 頂点設定
				_pRenderer->SetVertex(&s_planeVtxBuffer);
				// インデックスバッファ設定
				_pRenderer->SetIndex(&s_planeIdxBuffer);
				// ユニフォームバッファ設定
				CMtx44 mtxLW = CMtx44::Identity;
				Mtx::Scale(mtxLW, CVec3::One * 10.0f);
				CMtx44 mtxLP = s_mtxVPDrawble * s_mtxWVDrawble * mtxLW;
				const CUniformBuffer* pUniBuffer0 = _pRenderer->CreateAndCacheUniform(sizeof(CMtx44), 1, &mtxLP);
				libAssert(pUniBuffer0);
				_pRenderer->SetUniform(GFX_SHADER_VERTEX, UNIFORM_SLOT_00, pUniBuffer0);
				CVec2 uv(0.0f, 0.0f);
				const CUniformBuffer* pUniBuffer1 = _pRenderer->CreateAndCacheUniform(sizeof(uv), 1, &uv);
				libAssert(pUniBuffer1);
				_pRenderer->SetUniform(GFX_SHADER_VERTEX, UNIFORM_SLOT_01, pUniBuffer1);
				// テクスチャ設定
				CRenderTarget* pNoiseRT = &CGfxUtility::GetRenderTarget(RT_NOISE);
				_pRenderer->SetTexture(GFX_SHADER_PIXEL, TEXTURE_SLOT_00, pNoiseRT->GetColorTexture(0));
				// サンプラー設定
				{
					SAMPLER_DESC desc;
					desc.wrapX = GFX_SAMPLER_WRAP_MIRROR;
					desc.wrapY = GFX_SAMPLER_WRAP_MIRROR;
					desc.wrapZ = GFX_SAMPLER_WRAP_MIRROR;
					desc.minFilter = GFX_SAMPLER_FILTER_LINEAR;
					desc.magFilter = GFX_SAMPLER_FILTER_LINEAR;
					desc.mipFilter = GFX_SAMPLER_FILTER_LINEAR;
					desc.anisotropy = 0;
					desc.LODMin = 0;
					desc.LODMax = 0;
					desc.LODBias = 0;
					memcpy(desc.borderColor, CVec4::Zero.v, sizeof(desc.borderColor));
					const CSampler* pSampler = _pRenderer->CreateAndCacheSampler(desc);
					Assert(pSampler);
					_pRenderer->SetSampler(GFX_SHADER_PIXEL, SAMPLER_SLOT_00, pSampler);
				}
				// ラスタライザー設定
				{
					RASTERIZER_DESC desc;
					desc.cullFace = GFX_CULL_FACE_BACK;
					const CRasterizer* pRasterizer = _pRenderer->CreateAndCacheRasterizer(desc);
					Assert(pRasterizer);
					_pRenderer->SetRasterizer(pRasterizer);
				}
				// デプスステンシルステート設定
				{
					DEPTH_STATE_DESC desc;
					desc.isDepthTest = true;
					desc.isDepthWrite = true;
					desc.depthTest = GFX_TEST_LEQUAL;
					const CDepthState* pDepthState = _pRenderer->CreateAndCacheDepthState(desc);
					libAssert(pDepthState);
					_pRenderer->SetDepthState(pDepthState);
				}
				// 描画
				_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_TRIANGLES, s_planeIdxBuffer.GetIdxNum());
				//_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_LINE_STRIP, s_planeIdxBuffer.GetIdxNum());
				//_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_POINTS, s_planeIdxBuffer.GetIdxNum());
			}
			_pRenderer->PopRenderTarget();
			_pRenderer->PopRasterizer();
			_pRenderer->PopDepthState();
			POP_RENDERER_MARKER(_pRenderer);


			// ビュー・プロジェクション行列設定
			_pRenderer->LoadMatrix(MATRIX_VIEW_PROJ, s_mtxVPDrawble);
			_pRenderer->LoadMatrix(MATRIX_WORLD_VIEW, s_mtxWVDrawble);

			return true;
		};
		CGfxUtility::AddDraw(renderFunc, NULL, NULL);
	}

}

//-------------------------------------------------------------------------------------------------
///@brief	終了
void	CTestDraw2::Finalize()
{
	// リソースマネージャー破棄
	CModelResourceUtility::Finalize();

	// 頂点バッファ破棄
	s_planeVtxBuffer.Release();
	s_planeIdxBuffer.Release();
}



//*************************************************************************************************
//@brief	テスト描画デバッグウィンドウ
//*************************************************************************************************
class CDebugWinTest2DrawInfo : public CDebugWin
{
public:
	CDebugWinTest2DrawInfo() {}
	~CDebugWinTest2DrawInfo() {}

public:
	//-------------------------------------------------------------------------------------------------
	///@brief	更新
	virtual void	Step() override
	{
		ImGui::SliderFloat2("Noise Uv", s_noiseUv.v, 0.0f, 1.0f);

		ImGui::Dummy(ImVec2(5.0f, 5.0f));
		ImGui::Separator();

		if (ImGui::BeginMenu("test0"))
		{
			if (ImGui::BeginMenu("test1"))
			{
				ImGui::MenuItem("test2");
				ImGui::EndMenu();
			}
			ImGui::MenuItem("test3");
			ImGui::EndMenu();
		}
		ImGui::MenuItem("test4");

		ImGui::Dummy(ImVec2(5.0f, 5.0f));
		ImGui::Separator();

#if 1
		static b8 show_demo_window = false;
		static b8 show_another_window = false;
		ImGui::Checkbox("Demo Window", &show_demo_window);
		ImGui::Checkbox("Another Window", &show_another_window);

		// 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
		if (show_demo_window)
		{
			ImGui::ShowDemoWindow(&show_demo_window);
		}
		// 2. Show another simple window.
		if (show_another_window)
		{
			ImGui::Begin("Another Window", &show_another_window);	// Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
			ImGui::Text("Hello from another window!");
			if (ImGui::Button("Close Me"))
			{
				show_another_window = false;
			}
			ImGui::End();
		}
#endif // 0
	}

	//-------------------------------------------------------------------------------------------------
	///@brief	デバッグウインドウマネージャに登録コールバック
	virtual void	OnRegist() override {}

	//-------------------------------------------------------------------------------------------------
	///@brief	デバッグウインドウマネージャに解除コールバック
	virtual void	OnUnregist() override {}

	//-------------------------------------------------------------------------------------------------
	//@brief	アクディブコールバック
	virtual void	OnActivate() override {}

	//-------------------------------------------------------------------------------------------------
	//@brief	ディアクティブコールバック
	virtual void	OnDeactivate() override {}

};

// 登録
REGIST_DEBUG_WIN(CDebugWinTest2DrawInfo);


//*************************************************************************************************
//@brief	テストオブジェクトデバッグウィンドウ
//*************************************************************************************************
class CDebugWinObjectInfo : public CDebugWin
{
public:
	CDebugWinObjectInfo() {}
	~CDebugWinObjectInfo() {}

public:
	//-------------------------------------------------------------------------------------------------
	///@brief	更新
	virtual void	Step() override
	{
		// プロパティ情報
		auto PropInfoFunc = [&](CObject* _obj)
		{
			if (ImGui::TreeNode("Property Info"))
			{
				CProperty* pProp = _obj->GetTopProperty();
				while (pProp)
				{
					CProperty* pNext = _obj->GetNextProperty(pProp);
					b8 s_checkFlag = true;
					if (ImGui::Checkbox(pProp->GetTypeName(), &s_checkFlag))
					{
						_obj->DeleteProperty(pProp);
					}
					pProp = pNext;
				}

				ImGui::TreePop();
			}
		};

		// プロパティ生成
		auto PropCreateFunc = [&](CObject* _obj)
		{
			if (ImGui::BeginChild("Property Create"))
			{
				u32 propNum = CPropertyCreator::GetNodeNum();
				const c8** ppPropName = PCast<const c8**>(alloca(sizeof(c8*) * propNum));
				u32 idx = 0;
				CPropertyCreator* pPropCreater = CPropertyCreator::GetNodeTop();
				while (pPropCreater)
				{
					ppPropName[idx] = pPropCreater->GetTypeName();
					pPropCreater = pPropCreater->GetNodeNext();
					idx++;
				}

				static s32 s_itemCurrent = -1;
				if (ImGui::Combo("Property List", &s_itemCurrent, ppPropName, propNum))
				{
					_obj->CreateProperty(CRC32(ppPropName[s_itemCurrent]));
				}
			}
			ImGui::EndChild();
		};

		static u32 countObj = 0;
		// 再帰削除関数
		std::function<void(CObject*)> Recursive = [&](CObject* _obj)
		{
#define	USE_IMGUI_HEADER_TREE
//#define	USE_IMGUI_TREE

#ifdef USE_IMGUI_HEADER_TREE
			ImGui::PushID(_obj->GetName());
			if (ImGui::CollapsingHeader(_obj->GetName()))
#endif // USE_IMGUI_HEADER_TREE
#ifdef USE_IMGUI_TREE
			if (ImGui::TreeNode(_obj->GetName()))
#endif // USE_IMGUI_TREE
			{
#ifdef USE_IMGUI_HEADER_TREE
				ImGui::Indent();
#endif // USE_IMGUI_HEADER_TREE

				// プロパティ情報
				if (_obj != CObject::GetRoot())
				{
					PropInfoFunc(_obj);
				}

				// プロパティ生成
				//if (ImGui::Button("Property Create"))
				if (_obj != CObject::GetRoot() && ImGui::CollapsingHeader("Property Create"))
				{
					PropCreateFunc(_obj);
				}

				ImGui::Dummy(ImVec2(5.0f, 5.0f));

				// 子供生成
				if (ImGui::Button("Create Child"))
				{
					c8 name[32] = {};
					sprintf_safe(name, "object_%d", countObj);
					CObjectUtility::Create(MEM_ID_OBJECT, name, _obj);
					countObj++;
				}
				// 破棄
				if (_obj != CObject::GetRoot() && ImGui::Button("Delete"))
				{
					CObjectUtility::Delete(_obj);
				}

				ImGui::Dummy(ImVec2(5.0f, 5.0f));

				// 子供の再帰
				CObject* pChild = _obj->GetChildTop();
				while (pChild)
				{
					CObject* pNext = pChild->GetBrotherNext();
					// 再帰
					Recursive(pChild);
					pChild = pNext;
				}

#ifdef USE_IMGUI_TREE
				ImGui::TreePop();
#endif // USE_IMGUI_TREE
#ifdef USE_IMGUI_HEADER_TREE
				ImGui::Unindent();
#endif // USE_IMGUI_HEADER_TREE
			}
#ifdef USE_IMGUI_HEADER_TREE
			ImGui::PopID();
#endif // USE_IMGUI_HEADER_TREE
		};
		Recursive(CObject::GetRoot());
	}

};

// 登録
REGIST_DEBUG_WIN(CDebugWinObjectInfo);


WORK_NAMESPACE_END

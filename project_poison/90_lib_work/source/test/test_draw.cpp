﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "test_draw.h"

#include "main.h"
#include "dev_def.h"

// タイム
#include "timer/timer.h"
// ファイル
#include "file/file_utility.h"

// レンダーターゲット
#include "renderer/render_target.h"

// シェーダ
#include "shader/shader.h"
#include "shader/shader_utility.h"
#include "resource/shader_library.h"

// バッファオブジェクト
#include "object/color_buffer.h"
#include "object/depth_buffer.h"
#include "object/texture_buffer.h"
#include "object/uniform_buffer.h"
#include "object/vertex_buffer.h"
#include "object/index_buffer.h"
#include "object/sampler_state.h"
#include "object/rasterrizer_state.h"
#include "object/blend_state.h"
#include "object/depth_state.h"

// query
#include "query/occlusion_query.h"
#include "query/primitive_query.h"
#include "query/timer_query.h"


#include "imgui/imgui.h"
#include "debug_win/debug_win.h"


WORK_NAMESPACE_BGN

///-------------------------------------------------------------------------------------------------
/// constant

// シェーダ
static const FxShader* s_p2dFxShader = NULL;
static const FxShader* s_pNoiseFxShader = NULL;
static const FxShader* s_pTestFxShader = NULL;

// 頂点
static CVertexBuffer	s_vtxBuffer;
// ユニフォームバッファ
static CUniformBuffer	s_noiseUniBuffer;
static CUniformBuffer	s_mouseUniBuffer;
// リピートサンプラー
static CSampler			s_sampler;

// オクルージョンクエリ
static COcclusionQuery	s_occlusionQuery;
static COcclusionQuery	s_occlusionPixelQuery;

// Our state
b8 show_demo_window = false;
b8 show_another_window = false;

static b8 s_gpuIsOcclusion = 0;
static u64 s_gpuPixelCount = 0;

// 位置
static CVec2 s_pos = CVec2::Zero;
static f32 s_rot = 0.0f;
static CMtx44 s_mtxLW = CMtx44::Identity;
static CMtx44 s_mtxLWDrawble = CMtx44::Identity;
static CVec2 s_noiseUv = CVec2::Zero;
static CVec2 s_noiseUvDrawble = CVec2::Zero;

//-------------------------------------------------------------------------------------------------
/// GetResultOcclusion
b8		GetResultOcclusion()
{
	return s_occlusionQuery.GetResultOcclusion();
}

//-------------------------------------------------------------------------------------------------
/// GetResultPixelCount
u64		GetResultPixelCount()
{
	return s_occlusionPixelQuery.GetResultPixelCount();
}


//-------------------------------------------------------------------------------------------------
///@brief	初期化
b8		CTestDraw::Initialize()
{
	// クエリー初期化
	{
		s_occlusionQuery.Initialize(GFX_PREDICATE_OCCL_CHEKER);
		s_occlusionPixelQuery.Initialize(GFX_PREDICATE_PIXEL_COUNTER);
	}
	// 2Dシェーダ読み込み
	{
		// 頂点DECL
		VTX_DECL vtxDecl[] =
		{
			{ VTX_ATTR_POS, 0, GFX_VALUE_F32, 4, },
			{ VTX_ATTR_CLR0, sizeof(f32) * 4, GFX_VALUE_F32, 4, },
			{ VTX_ATTR_UV0, sizeof(f32) * 8, GFX_VALUE_F32, 2, },
		};
		s_p2dFxShader = CShaderUtility::SearchAndCreateFxShader(SCRC32("2d"), vtxDecl, ARRAYOF(vtxDecl));
		Assert(s_p2dFxShader);
		}

	// ノイズシェーダ読み込み
#if 1
	{
		// 頂点DECL
		VTX_DECL vtxDecl[] =
		{
			{ VTX_ATTR_POS, 0, GFX_VALUE_F32, 4, },
			{ VTX_ATTR_CLR0, sizeof(f32) * 4, GFX_VALUE_F32, 4, },
			{ VTX_ATTR_UV0, sizeof(f32) * 8, GFX_VALUE_F32, 2, },
		};
		s_pNoiseFxShader = CShaderUtility::SearchAndCreateFxShader(SCRC32("noise"), vtxDecl, ARRAYOF(vtxDecl));
		Assert(s_pNoiseFxShader);
	}
#endif // 0

	// testシェーダspirvシェーダ読み込み
#if 1
	{
		// 頂点DECL
		VTX_DECL vtxDecl[] =
		{
				{ VTX_ATTR_POS, 0, GFX_VALUE_F32, 4 },
				{ VTX_ATTR_CLR0, sizeof(f32) * 4, GFX_VALUE_F32, 4, },
				{ VTX_ATTR_UV0, sizeof(f32) * 8, GFX_VALUE_F32, 2, },
				{ VTX_ATTR_NRM, sizeof(f32) * 10, GFX_VALUE_F32, 3, },
		};
		s_pTestFxShader = CShaderUtility::SearchAndCreateFxShader(SCRC32("test"), vtxDecl, ARRAYOF(vtxDecl));
		Assert(s_pTestFxShader);
	}
#endif // 0


	// 頂点バッファ生成
	{
		struct VERTEX
		{
			f32 pos[4];
			f32 clr[4];
			f32 uv[2];
		};
		static VERTEX vertex[] =
		{
#ifdef LIB_GFX_OPENGL
			{ { +1.0f, +1.0f, 1.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 1.0f, 0.0f } },
			{ { -1.0f, +1.0f, 1.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 0.0f, 0.0f } },
			{ { -1.0f, -1.0f, 1.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 0.0f, 1.0f } },
			{ { +1.0f, -1.0f, 1.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 1.0f, 1.0f } },
#else
			{ { +1.0f, +1.0f, 1.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 1.0f, 0.0f } },
			{ { -1.0f, +1.0f, 1.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 0.0f, 0.0f } },
			{ { +1.0f, -1.0f, 1.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 1.0f, 1.0f } },
			{ { -1.0f, -1.0f, 1.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 0.0f, 1.0f } },
#endif // LIB_GFX_OPENGL
		};
		VTX_DESC vtxDesc(vertex, sizeof(VERTEX), ARRAYOF(vertex), GFX_USAGE_DYNAMIC);
		if (!s_vtxBuffer.Create(vtxDesc))
		{
			PRINT("[VBUFFER] 何がアカンかったんや...？\n");
		}
	}

	{
		UNIFORM_DESC desc;
		desc.data = &CMtx44::Identity;
		desc.elementSize = sizeof(CMtx44::Identity);
		desc.elementNum = 1;
		desc.usage = GFX_USAGE_DYNAMIC;
		desc.attr = GFX_BUFFER_ATTR_CONSTANT;
		if (!s_mouseUniBuffer.Create(desc))
		{
			PRINT("[UNIFORM] 何がアカンかったんや...？\n");
		}
	}
	{
		static CVec2 s_vec = CVec2::Zero;
		UNIFORM_DESC desc;
		desc.data = &s_vec;
		desc.elementSize = sizeof(s_vec);
		desc.elementNum = 1;
		desc.usage = GFX_USAGE_DYNAMIC;
		desc.attr = GFX_BUFFER_ATTR_CONSTANT;
		if (!s_noiseUniBuffer.Create(desc))
		{
			PRINT("[UNIFORM] 何がアカンかったんや...？\n");
		}
	}

	// サンプラー生成
	{
		SAMPLER_DESC desc;
		desc.wrapX = GFX_SAMPLER_WRAP_MIRROR;
		desc.wrapY = GFX_SAMPLER_WRAP_MIRROR;
		desc.wrapZ = GFX_SAMPLER_WRAP_MIRROR;
		desc.minFilter = GFX_SAMPLER_FILTER_LINEAR;
		desc.magFilter = GFX_SAMPLER_FILTER_LINEAR;
		desc.mipFilter = GFX_SAMPLER_FILTER_LINEAR;
		desc.anisotropy = 0;
		desc.LODMin = 0;
		desc.LODMax = 0;
		desc.LODBias = 0;
		memcpy(desc.borderColor, CVec4::Zero.v, sizeof(desc.borderColor));
		if (!s_sampler.Create(desc))
		{
			PRINT("[SAMPLER] 何がアカンかったんや...？\n");
		}
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
///@brief	更新
void	CTestDraw::Step()
{
	// ステップタイプ
	const f32 stepTime = CTimeUtility::GetStepTime();

	// 入力
	static CVec2 s_move(0.01f * stepTime);
	b8 isUpdate = false;
	const CKeyboard* pKey = CInputUtility::GetKeyboard();
	if (pKey->GetButton(CKeyboard::eLeft).GetPush())
	{
		s_pos += CVec2::Left * s_move;
		isUpdate |= true;
	}
	if (pKey->GetButton(CKeyboard::eRight).GetPush())
	{
		s_pos += CVec2::Right * s_move;
		isUpdate |= true;
	}
	if (pKey->GetButton(CKeyboard::eUp).GetPush())
	{
		s_pos += CVec2::Up * s_move;
		isUpdate |= true;
	}
	if (pKey->GetButton(CKeyboard::eDown).GetPush())
	{
		s_pos += CVec2::Down * s_move;
		isUpdate |= true;
	}

	const CMouse* pMouse = CInputUtility::GetMouse();
	if (pMouse->GetButton(CMouse::eRight).GetPush())
	{
		CVec2 pos = pMouse->GetPointer().GetPos();
		CGfxUtility::ConvertWindowToScreenPos(pos, pos);
		s_pos = pos * 2.0f - 1.0f;
#if defined( OPENGL_VIEWPORT )
#elif defined( DIRECTX_VIEWPORT )
		s_pos[1] *= -1.0f;
#endif
		isUpdate |= true;
	}

	static f32 s_rotSpeed = toRadian(360.0f);
	static f32 s_noiseSpeed = 1.0f;
	f32 moveWheel = pMouse->GetWheel().GetMove();
	if (moveWheel != 0.0f)
	{
		s_rot += s_rotSpeed * moveWheel * stepTime;
		isUpdate |= true;
	}

	// ポリゴン座標更新
	if (isUpdate)
	{
		static f32 vertexSize = 0.1f;
#if 0
		struct VERTEX
		{
			f32 pos[4];
			f32 clr[4];
			f32 uv[2];
		};
		static VERTEX baseVertex[] =
		{
#ifdef LIB_GFX_OPENGL
			{ { +vertexSize, +vertexSize, 0.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 1.0f, 1.0f } },
			{ { -vertexSize, +vertexSize, 0.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 0.0f, 1.0f } },
			{ { -vertexSize, -vertexSize, 0.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 0.0f, 0.0f } },
			{ { +vertexSize, -vertexSize, 0.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 1.0f, 0.0f } },
#else
			{ { +vertexSize, +vertexSize, 0.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 1.0f, 0.0f } },
			{ { -vertexSize, +vertexSize, 0.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 0.0f, 0.0f } },
			{ { +vertexSize, -vertexSize, 0.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 1.0f, 1.0f } },
			{ { -vertexSize, -vertexSize, 0.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 0.0f, 1.0f } },
#endif // LIB_GFX_OPENGL
		};
		static VERTEX vertex[ARRAYOF(baseVertex)];
		memcpy(vertex, baseVertex, sizeof(baseVertex));
		for (u32 idx = 0; idx < 4; idx++)
		{
			vertex[idx].pos[0] = baseVertex[idx].pos[0] + s_pos[0];
			vertex[idx].pos[1] = baseVertex[idx].pos[1] + s_pos[1];
		}
		s_vtxBuffer.Update(sizeof(vertex), vertex);

#else
		CMtx44 mtxScl;
		Mtx::Scale(mtxScl, CVec3(vertexSize));
		CMtx44 mtxRot;
		Mtx::RotateZ(mtxRot, s_rot);
		s_mtxLW = CMtx44::Identity;
		Mtx::Mul(s_mtxLW, mtxRot, mtxScl);
		Mtx::Trans(s_mtxLW, CVec3(s_pos, 0.0f));
		s_mouseUniBuffer.Update(sizeof(s_mtxLW), &s_mtxLW);

#endif // 0
	}
	// ノイズUV更新
	s_noiseUniBuffer.Update(sizeof(s_noiseUv), &s_noiseUv);

	// クエリー取得
	s_gpuIsOcclusion = GetResultOcclusion();
	s_gpuPixelCount = GetResultPixelCount();
}


//-------------------------------------------------------------------------------------------------
///@brief	描画準備
void	CTestDraw::PrepareDraw()
{
	s_mtxLWDrawble = s_mtxLW;
	s_noiseUvDrawble = s_noiseUv;
}


//-------------------------------------------------------------------------------------------------
///@brief	描画
void	CTestDraw::Draw()
{


	// ノイズ描画
	{
		// 描画関数
		RENDER_FUNC renderFunc = [](CRenderer* _pRenderer, const void* _p0, const void* _p1)->b8
		{
			PUSH_RENDERER_MARKER(_pRenderer, "Noise");
			_pRenderer->PushRenderTarget();
			{
				// レンダーターゲット設定
				CRenderTarget* pRT = &CGfxUtility::GetRenderTarget(RT_NOISE);
				_pRenderer->SetRenderTarget(pRT, CGfxUtility::RtClrBit(RT_NOISE_CLR_0), INVALID_RT_IDX);
				// シェーダ設定
				_pRenderer->SetShader(s_pNoiseFxShader);
				// 頂点設定
				_pRenderer->SetVertex(&s_vtxBuffer);
				// 描画
				_pRenderer->DrawPrim(GFX_PRIMITIVE_QUADS, s_vtxBuffer.GetVtxNum());
			}
			_pRenderer->PopRenderTarget();
			POP_RENDERER_MARKER(_pRenderer);
			return true;
		};
		CGfxUtility::AddDraw(renderFunc, NULL, NULL);
	}


	{
		// 描画関数
		RENDER_FUNC renderFunc = [](CRenderer* _pRenderer, const void* _p0, const void* _p1)->b8
		{
			PUSH_RENDERER_MARKER(_pRenderer, "OcclutionDraw");
			_pRenderer->PushDepthState();
			_pRenderer->PushRenderTarget();
			{
				// レンダーターゲット設定
				CRenderTarget* pRT = &CGfxUtility::GetRenderTarget(RT_MAIN);
				_pRenderer->SetRenderTarget(pRT, CGfxUtility::RtClrBit(RT_MAIN_CLR_0), RT_MAIN_DEP_0);
				// シェーダ設定
				_pRenderer->SetShader(s_p2dFxShader);
				// 頂点設定
				const CVertexBuffer& vtxBuf = CGfxUtility::GetScreenVtx();
				_pRenderer->SetVertex(&vtxBuf);
				// ユニフォームバッファ設定
				CMtx44 mtxLW = CMtx44::Identity;
				mtxLW.SetTrans(CVec3(1.0f, 0.0f, 0.0f));
				const CUniformBuffer* pUniBuffer0 = _pRenderer->CreateAndCacheUniform(sizeof(mtxLW), 1, &mtxLW);
				libAssert(pUniBuffer0);
				_pRenderer->SetUniform(GFX_SHADER_VERTEX, UNIFORM_SLOT_00, pUniBuffer0);
				const CUniformBuffer* pUniBuffer1 = _pRenderer->CreateAndCacheUniform(sizeof(s_noiseUvDrawble), 1, &s_noiseUvDrawble);
				libAssert(pUniBuffer1);
				_pRenderer->SetUniform(GFX_SHADER_VERTEX, UNIFORM_SLOT_01, pUniBuffer1);
				// テクスチャ設定
				CRenderTarget* pNoiseRT = &CGfxUtility::GetRenderTarget(RT_NOISE);
				_pRenderer->SetTexture(GFX_SHADER_PIXEL, TEXTURE_SLOT_00, pNoiseRT->GetColorTexture(0));
				// サンプラー設定
				{
					SAMPLER_DESC desc;
					desc.wrapX = GFX_SAMPLER_WRAP_MIRROR;
					desc.wrapY = GFX_SAMPLER_WRAP_MIRROR;
					desc.wrapZ = GFX_SAMPLER_WRAP_MIRROR;
					desc.minFilter = GFX_SAMPLER_FILTER_LINEAR;
					desc.magFilter = GFX_SAMPLER_FILTER_LINEAR;
					desc.mipFilter = GFX_SAMPLER_FILTER_LINEAR;
					desc.anisotropy = 0;
					desc.LODMin = 0;
					desc.LODMax = 0;
					desc.LODBias = 0;
					desc.testFunc = GFX_TEST_ALWAYS;
					memcpy(desc.borderColor, CVec4::Zero.v, sizeof(desc.borderColor));
					const CSampler* pSampler = _pRenderer->CreateAndCacheSampler(desc);
					libAssert(pSampler);
					_pRenderer->SetSampler(GFX_SHADER_PIXEL, SAMPLER_SLOT_00, pSampler);
				}
				// デプスステンシルステート設定
				{
					DEPTH_STATE_DESC desc;
					desc.isDepthTest = true;
					desc.isDepthWrite = true;
					desc.depthTest = GFX_TEST_ALWAYS;
					const CDepthState* pDepthState = _pRenderer->CreateAndCacheDepthState(desc);
					libAssert(pDepthState);
					_pRenderer->SetDepthState(pDepthState);
				}
				// 描画
				_pRenderer->DrawPrim(GFX_PRIMITIVE_QUADS, vtxBuf.GetVtxNum());
			}
			_pRenderer->PopRenderTarget();
			_pRenderer->PopDepthState();
			POP_RENDERER_MARKER(_pRenderer);
			return true;
		};
		CGfxUtility::AddDraw(renderFunc, NULL, NULL);
	}

	// オクルージョン・ピクセルカウンター開始
	{
		// 描画関数
		RENDER_FUNC renderFunc = [](CRenderer* _pRenderer, const void* _p0, const void* _p1)->b8
		{
			_pRenderer->BeginQuery(&s_occlusionQuery);
			return true;
		};
		CGfxUtility::AddDraw(renderFunc, NULL, NULL);
	}


	{
		// 描画関数
		RENDER_FUNC renderFunc = [](CRenderer* _pRenderer, const void* _p0, const void* _p1)->b8
		{
			PUSH_RENDERER_MARKER(_pRenderer, "OcclutionTestDraw");
			_pRenderer->PushDepthState();
			_pRenderer->PushRenderTarget();
			{
				// レンダーターゲット設定
				CRenderTarget* pRT = &CGfxUtility::GetRenderTarget(RT_MAIN);
				_pRenderer->SetRenderTarget(pRT, CGfxUtility::RtClrBit(RT_MAIN_CLR_0), RT_MAIN_DEP_0);
				// シェーダ設定
				_pRenderer->SetShader(s_pTestFxShader);
				// 頂点設定
				const CVertexBuffer& vtxBuf = CGfxUtility::GetScreenVtx();
				_pRenderer->SetVertex(&s_vtxBuffer);
				// ユニフォームバッファ設定
				const CUniformBuffer* pUniBuffer0 = _pRenderer->CreateAndCacheUniform(sizeof(s_mtxLWDrawble), 1, &s_mtxLWDrawble);
				libAssert(pUniBuffer0);
				_pRenderer->SetUniform(GFX_SHADER_VERTEX, UNIFORM_SLOT_00, pUniBuffer0);
				const CUniformBuffer* pUniBuffer1 = _pRenderer->CreateAndCacheUniform(sizeof(s_noiseUvDrawble), 1, &s_noiseUvDrawble);
				libAssert(pUniBuffer1);
				_pRenderer->SetUniform(GFX_SHADER_VERTEX, UNIFORM_SLOT_01, pUniBuffer1);
				// デプスステンシルステート設定
				{
					DEPTH_STATE_DESC desc;
					desc.isDepthTest = true;
					desc.isDepthWrite = true;
					desc.depthTest = GFX_TEST_LEQUAL;
					const CDepthState* pDepthState = _pRenderer->CreateAndCacheDepthState(desc);
					libAssert(pDepthState);
					_pRenderer->SetDepthState(pDepthState);
				}
				// 描画
				_pRenderer->DrawPrim(GFX_PRIMITIVE_QUADS, s_vtxBuffer.GetVtxNum());
			}
			_pRenderer->PopRenderTarget();
			_pRenderer->PopDepthState();
			POP_RENDERER_MARKER(_pRenderer);
			return true;
		};
		CGfxUtility::AddDraw(renderFunc, NULL, NULL);
	}

	// オクルージョン・ピクセルカウンター終了
	{
		// 描画関数
		RENDER_FUNC renderFunc = [](CRenderer* _pRenderer, const void* _p0, const void* _p1)->b8
		{
			_pRenderer->EndQuery(&s_occlusionQuery);
			return true;
		};
		CGfxUtility::AddDraw(renderFunc, NULL, NULL);
	}


	// オクルージョン・ピクセルカウンター開始
	{
		// 描画関数
		RENDER_FUNC renderFunc = [](CRenderer* _pRenderer, const void* _p0, const void* _p1)->b8
		{
			_pRenderer->BeginQuery(&s_occlusionPixelQuery);
			return true;
		};
		CGfxUtility::AddDraw(renderFunc, NULL, NULL);
	}


	{
		// 描画関数
		RENDER_FUNC renderFunc = [](CRenderer* _pRenderer, const void* _p0, const void* _p1)->b8
		{
			PUSH_RENDERER_MARKER(_pRenderer, "TextureDraw");
			_pRenderer->PushDepthState();
			_pRenderer->PushRenderTarget();
			{
				// レンダーターゲット設定
				CRenderTarget* pRT = &CGfxUtility::GetRenderTarget(RT_MAIN);
				_pRenderer->SetRenderTarget(pRT, CGfxUtility::RtClrBit(RT_MAIN_CLR_0), RT_MAIN_DEP_0);
				// シェーダ設定
				_pRenderer->SetShader(s_p2dFxShader);
				//_pRenderer->SetShader(s_pTestFxShader);
				// 頂点設定
				const CVertexBuffer& vtxBuf = CGfxUtility::GetScreenVtx();
				_pRenderer->SetVertex(&vtxBuf);
				// ユニフォームバッファ設定
				const CUniformBuffer* pUniBuffer0 = _pRenderer->CreateAndCacheUniform(sizeof(s_mtxLWDrawble), 1, &s_mtxLWDrawble);
				libAssert(pUniBuffer0);
				_pRenderer->SetUniform(GFX_SHADER_VERTEX, UNIFORM_SLOT_00, pUniBuffer0);
				const CUniformBuffer* pUniBuffer1 = _pRenderer->CreateAndCacheUniform(sizeof(s_noiseUvDrawble), 1, &s_noiseUvDrawble);
				libAssert(pUniBuffer1);
				_pRenderer->SetUniform(GFX_SHADER_VERTEX, UNIFORM_SLOT_01, pUniBuffer1);
				// テクスチャ設定
				CRenderTarget* pNoiseRT = &CGfxUtility::GetRenderTarget(RT_NOISE);
				_pRenderer->SetTexture(GFX_SHADER_PIXEL, TEXTURE_SLOT_00, pNoiseRT->GetColorTexture(0));
				// サンプラー設定
				{
					SAMPLER_DESC desc;
					desc.wrapX = GFX_SAMPLER_WRAP_MIRROR;
					desc.wrapY = GFX_SAMPLER_WRAP_MIRROR;
					desc.wrapZ = GFX_SAMPLER_WRAP_MIRROR;
					desc.minFilter = GFX_SAMPLER_FILTER_LINEAR;
					desc.magFilter = GFX_SAMPLER_FILTER_LINEAR;
					desc.mipFilter = GFX_SAMPLER_FILTER_LINEAR;
					desc.anisotropy = 0;
					desc.LODMin = 0;
					desc.LODMax = 0;
					desc.LODBias = 0;
					memcpy(desc.borderColor, CVec4::Zero.v, sizeof(desc.borderColor));
					const CSampler* pSampler = _pRenderer->CreateAndCacheSampler(desc);
					libAssert(pSampler);
					_pRenderer->SetSampler(GFX_SHADER_PIXEL, SAMPLER_SLOT_00, pSampler);
				}
				// 描画プリディケーション
				_pRenderer->SetDrawingPredictionQuery(&s_occlusionQuery);
				// 描画
				_pRenderer->DrawPrim(GFX_PRIMITIVE_QUADS, s_vtxBuffer.GetVtxNum());
			}
			_pRenderer->PopRenderTarget();
			_pRenderer->PopDepthState();
			POP_RENDERER_MARKER(_pRenderer);
			return true;
		};
		CGfxUtility::AddDraw(renderFunc, NULL, NULL);
	}


	// オクルージョン・ピクセルカウンター終了
	{
		// 描画関数
		RENDER_FUNC renderFunc = [](CRenderer* _pRenderer, const void* _p0, const void* _p1)->b8
		{
			_pRenderer->EndQuery(&s_occlusionPixelQuery);
			return true;
		};
		CGfxUtility::AddDraw(renderFunc, NULL, NULL);
	}


	{
		// 描画関数
		RENDER_FUNC renderFunc = [](CRenderer* _pRenderer, const void* _p0, const void* _p1)->b8
		{
			PUSH_RENDERER_MARKER(_pRenderer, "PostCopyDraw");
			// シェーダ設定
			const PassShader* pCopy = CGfxUtility::GetCopyShader();
			_pRenderer->SetShader(pCopy);
			// 頂点設定
			const CVertexBuffer& vtxBuf = CGfxUtility::GetScreenVtx();
			_pRenderer->SetVertex(&s_vtxBuffer);
			// ユニフォームバッファ設定
			const CUniformBuffer* pUniBuffer0 = _pRenderer->CreateAndCacheUniform(sizeof(CMtx44), 1, &CMtx44::Identity);
			libAssert(pUniBuffer0);
			_pRenderer->SetUniform(GFX_SHADER_VERTEX, UNIFORM_SLOT_00, pUniBuffer0);
			CVec2 uvOffset(0.0f, 0.0f);
			const CUniformBuffer* pUniBuffer1 = _pRenderer->CreateAndCacheUniform(sizeof(uvOffset), 1, &uvOffset);
			libAssert(pUniBuffer1);
			_pRenderer->SetUniform(GFX_SHADER_VERTEX, UNIFORM_SLOT_01, pUniBuffer1);
			// テクスチャ設定
			CRenderTarget* pMainRT = &CGfxUtility::GetRenderTarget(RT_MAIN);
			_pRenderer->SetTexture(GFX_SHADER_PIXEL, TEXTURE_SLOT_00, pMainRT->GetColorTexture(RT_MAIN_CLR_0));
			//_pRenderer->SetTexture(GFX_SHADER_PIXEL, TEXTURE_SLOT_00, pMainRT->GetDepthTexture(RT_MAIN_DEP_0));
			// サンプラー設定
			_pRenderer->SetSampler(GFX_SHADER_PIXEL, SAMPLER_SLOT_00, &s_sampler);
			// 描画
			_pRenderer->DrawPrim(GFX_PRIMITIVE_QUADS, s_vtxBuffer.GetVtxNum());
			POP_RENDERER_MARKER(_pRenderer);
			return true;
		};
		CGfxUtility::AddDraw(renderFunc, NULL, NULL);
	}
}

//-------------------------------------------------------------------------------------------------
///@brief	終了
void	CTestDraw::Finalize()
{
	// サンプラー破棄
	s_sampler.Release();
	// ユニフォームバッファ
	s_noiseUniBuffer.Release();
	s_mouseUniBuffer.Release();
	// 頂点バッファ破棄
	s_vtxBuffer.Release();

	// クエリー破棄
	s_occlusionQuery.Finalize();
	s_occlusionPixelQuery.Finalize();
}



//*************************************************************************************************
//@brief	テスト描画デバッグウィンドウ
//*************************************************************************************************
class CDebugWinTestDrawInfo : public CDebugWin
{
public:
	CDebugWinTestDrawInfo() {}
	~CDebugWinTestDrawInfo() {}

public:
	//-------------------------------------------------------------------------------------------------
	///@brief	更新
	virtual void	Step() override
	{
		ImGui::Dummy(ImVec2(5.0f, 5.0f));
		ImGui::Separator();
		ImGui::Text("GPU Occlusion %s", (s_gpuIsOcclusion ? "true" : "false"));
		ImGui::Text("GPU PixelCount %d /frame", s_gpuPixelCount);

		ImGui::Dummy(ImVec2(5.0f, 5.0f));
		ImGui::Separator();
		ImGui::SliderFloat2("Noise Uv", s_noiseUv.v, 0.0f, 1.0f);


		ImGui::Dummy(ImVec2(5.0f, 5.0f));
		ImGui::Checkbox("Demo Window", &show_demo_window);
		ImGui::Checkbox("Another Window", &show_another_window);

		if (ImGui::BeginMenuBar())
		{
			if (ImGui::BeginMenu("test0"))
			{
				if (ImGui::BeginMenu("test1"))
				{
					ImGui::MenuItem("test2");
					ImGui::EndMenu();
				}
				ImGui::MenuItem("test3");
				ImGui::EndMenu();
			}
			ImGui::MenuItem("test4");
			ImGui::EndMenuBar();
		}


		// imgui使用例
		{
			// RenderTarget Window
			//ImGui::Begin("Render Target");
			//{
			//	CRenderTarget* pMainRT = &CGfxUtility::GetRenderTarget(RT_MAIN);
			//	//ImTextureID imTex = RCast<ImTextureID>(pMainRT->GetColorTexture(RT_MAIN_CLR_0)->GetObject().objectID);
			//	ImTextureID imTex = RCast<ImTextureID>(pMainRT->GetDepthTexture(RT_MAIN_DEP_0)->GetObject().objectID);
			//	ImVec2 size(
			//		SCast<f32>(pMainRT->GetColorTexture(RT_MAIN_CLR_0)->GetWidth() / 4),
			//		SCast<f32>(pMainRT->GetColorTexture(RT_MAIN_CLR_0)->GetHeight() / 4)
			//		);
			//	ImGui::Image(imTex, size);
			//}
			//ImGui::End();
		}
#if 1
		// 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
		if (show_demo_window)
		{
			ImGui::ShowDemoWindow(&show_demo_window);
		}
		// 2. Show another simple window.
		if (show_another_window)
		{
			ImGui::Begin("Another Window", &show_another_window);	// Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
			ImGui::Text("Hello from another window!");
			if (ImGui::Button("Close Me"))
			{
				show_another_window = false;
			}
			ImGui::End();
		}
#endif // 0
	}

	//-------------------------------------------------------------------------------------------------
	///@brief	デバッグウインドウマネージャに登録コールバック
	virtual void	OnRegist() override {}

	//-------------------------------------------------------------------------------------------------
	///@brief	デバッグウインドウマネージャに解除コールバック
	virtual void	OnUnregist() override {}

	//-------------------------------------------------------------------------------------------------
	//@brief	アクディブコールバック
	virtual void	OnActivate() override {}

	//-------------------------------------------------------------------------------------------------
	//@brief	ディアクティブコールバック
	virtual void	OnDeactivate() override {}

};

// 登録
REGIST_DEBUG_WIN(CDebugWinTestDrawInfo);

WORK_NAMESPACE_END

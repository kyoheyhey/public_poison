﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "model_component.h"

// レンダラー
//#include "renderer/renderer.h"

#include "dev_def.h"


WORK_NAMESPACE_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///*************************************************************************************************
/// マテリアルゲームインスタンス
///*************************************************************************************************

CGIMaterial::CGIMaterial()
{
}

CGIMaterial::~CGIMaterial()
{
}

///-------------------------------------------------------------------------------------------------
/// リソース構築
b8		CGIMaterial::Create(const CResMaterial* _pResMat, const CResTexture** _ppResTex, u8 _texNum)
{
	m_pResMat = _pResMat;
	m_pResMat->AddRefCount();
	for (u16 i = 0; i < _texNum; ++i)
	{
		m_ppResTex[i] = _ppResTex[i];
		m_ppResTex[i]->AddRefCount();
	}
	m_texNum = _texNum;

	return true;
}

///-------------------------------------------------------------------------------------------------
/// 解放処理
void	CGIMaterial::Release()
{
	if (m_pResMat)
	{
		m_pResMat->DelRefCount();
	}
	m_pResMat = NULL;

	for (u16 i = 0; i < ARRAYOF(m_ppResTex); ++i)
	{
		if (m_ppResTex[i])
		{
			m_ppResTex[i]->DelRefCount();
		}
		m_ppResTex[i] = NULL;
	}
	m_texNum = 0;
}


///*************************************************************************************************
/// メッシュゲームインスタンス
///*************************************************************************************************

CGIMesh::CGIMesh()
{
}

CGIMesh::~CGIMesh()
{
}

///-------------------------------------------------------------------------------------------------
/// リソース構築
b8		CGIMesh::Create(const CResMesh* _pResMesh, u16 _matIdx, hash32 _shaderNameCrc)
{
	m_pResMesh = _pResMesh;
	m_pResMesh->AddRefCount();
	m_matIdx = _matIdx;
	m_pShader = CShaderUtility::SearchAndCreateFxShader(_shaderNameCrc, m_pResMesh->GetVtxDecl(), m_pResMesh->GetVtxDeclNum());
	return true;
}

///-------------------------------------------------------------------------------------------------
/// 解放処理
void	CGIMesh::Release()
{
	if (m_pResMesh)
	{
		m_pResMesh->DelRefCount();
	}
	m_pResMesh = NULL;
	m_matIdx = 0;
	m_pShader = NULL;
}


///*************************************************************************************************
/// モデルゲームインスタンス
///*************************************************************************************************

CGIModel::CGIModel()
{
}

CGIModel::~CGIModel()
{
}

///-------------------------------------------------------------------------------------------------
/// リソース構築
b8		CGIModel::Create(const c8* _name, MEM_HANDLE _memHdl, const MAT_DESC* _pMatDesc, u16 _matNum, const MESH_DESC* _pMeshDesc, u16 _meshNum)
{
	u32 matSize = sizeof(CGIMaterial) * _matNum;
	u32 meshSize = sizeof(CGIMesh) * _meshNum;
	u32 totalSize = matSize + meshSize;
	u32 align = 32;
	u8* pBuf = PCast<u8*>(MEM_MALLOC_ALIGN(_memHdl, align, totalSize));
	Assert(pBuf);

	m_pMaterial = PCast<CGIMaterial*>(pBuf);
	for (u16 matIdx = 0; matIdx < _matNum; ++matIdx)
	{
		const CResMaterial* pResMat = _pMatDesc[matIdx].pResMat;
		if (pResMat && pResMat->IsEnable())
		{
			const CResTexture** ppResTex = _pMatDesc[matIdx].ppResTex;
			u8 texNum = _pMatDesc[matIdx].texNum;
			new(&m_pMaterial[matIdx]) CGIMaterial;
			m_pMaterial[matIdx].Create(pResMat, ppResTex, texNum);
		}
	}
	m_matNum = _matNum;
	pBuf += matSize;

	m_pMesh = PCast<CGIMesh*>(pBuf);
	for (u16 meshIdx = 0; meshIdx < _meshNum; ++meshIdx)
	{
		const CResMesh* pResMesh = _pMeshDesc[meshIdx].pResMesh;
		if (pResMesh && pResMesh->IsEnable())
		{
			u16 matIdx = _pMeshDesc[meshIdx].matIdx;
			const CGIMaterial* pGIMat = &m_pMaterial[matIdx];
			if (pGIMat && pGIMat->m_pResMat)
			{
				new(&m_pMesh[meshIdx]) CGIMesh;
				m_pMesh[meshIdx].Create(pResMesh, matIdx, pGIMat->m_pResMat->GetShaderNameCrc());
			}
		}
	}
	m_meshNum = _meshNum;
	pBuf += meshSize;

	// サイズチェック
	Assert(pBuf == PCast<const u8*>(m_pMaterial) + totalSize);

	// 名前保持
	strcpy_s(m_name, _name);
	if (m_nameCrc == hash32::Invalid)
	{
		m_nameCrc = CRC32(_name);
	}
	m_memHdl = _memHdl;

	return true;
}

///-------------------------------------------------------------------------------------------------
/// 解放処理
void	CGIModel::Release()
{
	for (u16 matIdx = 0; matIdx < m_matNum; ++matIdx)
	{
		m_pMaterial[matIdx].Release();
		m_pMaterial[matIdx].~CGIMaterial();
	}
	m_matNum = 0;

	for (u16 meshIdx = 0; meshIdx < m_meshNum; ++meshIdx)
	{
		m_pMesh[meshIdx].Release();
		m_pMesh[meshIdx].~CGIMesh();
	}
	m_meshNum = 0;

	MEM_SAFE_FREE(m_memHdl, m_pMaterial);
}



///*************************************************************************************************
/// モデルコンポーネント
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// オブジェクトとリンクしたときのコールバック
void	CModelComponent::OnLinkObject()
{
	// 基底クラスの方も必ず呼ぶ
	CBase::OnLinkObject();

	SetBgnFunc(CallbackStepBgn);
	SetEndFunc(CallbackStepEnd);
}

///-------------------------------------------------------------------------------------------------
/// 終了
void	CModelComponent::Finalize()
{
	m_model.Release();

	// 基底クラスの方も必ず呼ぶ
	CBase::Finalize();
}

///-------------------------------------------------------------------------------------------------
/// オーバーライド用Step関数
void	CModelComponent::Step()
{

}

///-------------------------------------------------------------------------------------------------
/// オーバーライド用PrepareDraw関数
void	CModelComponent::PrepareDraw()
{
	CObject* pObj = GetObject();

	// 描画用行列更新
	m_model.m_drawModelMtx.Copy(pObj->GetLocalMtx());
}

///-------------------------------------------------------------------------------------------------
/// オーバーライド用PrepareDraw関数
void	CModelComponent::Draw()
{
	CObject* pObj = GetObject();
	if (!pObj->IsDraw()) { return; }

	// 描画関数
	RENDER_FUNC renderFunc = [](CRenderer* _pRenderer, const void* _p0, const void* _p1)->b8
	{
		const CGIModel* pModel = PCast<const CGIModel*>(_p0);
		const CGIMesh* pMesh = pModel->m_pMesh;

		PUSH_RENDERER_MARKER(_pRenderer, pModel->m_name);

		// ローカル・ワールド行列設定
		_pRenderer->PushMatrix();
		_pRenderer->LoadMatrix(MATRIX_LOCAL_WORLD, pModel->m_drawModelMtx);

		for (u16 meshIdx = 0; meshIdx < pModel->m_meshNum; ++meshIdx, ++pMesh)
		{
			const CGIMaterial* pMaterial = &pModel->m_pMaterial[pMesh->m_matIdx];
			const CResMesh* pResMesh = pMesh->m_pResMesh;
			const CResMaterial* pResMat = pMaterial->m_pResMat;
			const CResTexture* const* ppResTex = pMaterial->m_ppResTex;

			// テクスチャ設定
			for (u8 texIdx = 0; texIdx < pMaterial->m_texNum; ++texIdx, ++ppResTex)
			{
				SAMPLER_SLOT samplerSlot = SCast<SAMPLER_SLOT>(texIdx);
				TEXTURE_SLOT texSlot = SCast<TEXTURE_SLOT>(texIdx);
				const CSampler* pSampler = pResMat->GetSamplerBuffer(texIdx);
				if (pSampler)
				{
					_pRenderer->SetSampler(GFX_SHADER_VERTEX, samplerSlot, pSampler);
					_pRenderer->SetSampler(GFX_SHADER_GEOMETRY, samplerSlot, pSampler);
					_pRenderer->SetSampler(GFX_SHADER_PIXEL, samplerSlot, pSampler);
				}
				const CTextureBuffer* pTexBuf = (*ppResTex)->GetTexBuffer();
				if (pTexBuf)
				{
					_pRenderer->SetTexture(GFX_SHADER_VERTEX, texSlot, pTexBuf);
					_pRenderer->SetTexture(GFX_SHADER_GEOMETRY, texSlot, pTexBuf);
					_pRenderer->SetTexture(GFX_SHADER_PIXEL, texSlot, pTexBuf);
				}
			}

			// ラスタライザー設定
			const CRasterizer* pRasterizer = pResMat->GetRasterizer();
			if (pRasterizer)
			{
				_pRenderer->SetRasterizer(pRasterizer);
			}

			// ブレンドステートバッファ設定
			const CBlendState* pBlendState = pResMat->GetBlendState();
			if (pBlendState)
			{
				_pRenderer->SetBlendState(pBlendState);
			}

			// デプスステンシルステート設定
			const CDepthState* pDepthState = pResMat->GetDepthState();
			if (pDepthState)
			{
				_pRenderer->SetDepthState(pDepthState);
			}

			// シェーダ設定
			const FxShader* pShader = pMesh->m_pShader;
			_pRenderer->SetShader(pShader);

			// 描画
			CResMesh::RenderFuncMesh(_pRenderer, pResMesh);
		}
		_pRenderer->PopMatrix();

		POP_RENDERER_MARKER(_pRenderer);

		return true;
	};
	CGfxUtility::AddDraw(renderFunc, &m_model, NULL);
}

///-------------------------------------------------------------------------------------------------
/// オーバーライド用Build関数
b8		CModelComponent::Build()
{
	return true;
}


///-------------------------------------------------------------------------------------------------
/// 開始関数
b8		CModelComponent::CallbackStepBgn(CComponentFunc* _compFunc, STEP_TYPE _type)
{
	if (_type == DRAW)
	{
		// 描画関数
		RENDER_FUNC renderFunc = [](CRenderer* _pRenderer, const void* _p0, const void* _p1)->b8
		{
			PUSH_RENDERER_MARKER(_pRenderer, "ModelDraw");
			_pRenderer->PushRenderTarget();
			// レンダーターゲット設定
			CRenderTarget* pRT = &CGfxUtility::GetRenderTarget(RT_MAIN);
			_pRenderer->SetRenderTarget(pRT, CGfxUtility::RtClrBit(RT_MAIN_CLR_0), RT_MAIN_DEP_0);
			return true;
		};
		CGfxUtility::AddDraw(renderFunc, NULL, NULL);
	}
	return true;
}

///-------------------------------------------------------------------------------------------------
/// 終了関数
void	CModelComponent::CallbackStepEnd(CComponentFunc* _compFunc, STEP_TYPE _type)
{
	if (_type == DRAW)
	{
		// 描画関数
		RENDER_FUNC renderFunc = [](CRenderer* _pRenderer, const void* _p0, const void* _p1)->b8
		{
			_pRenderer->PopRenderTarget();
			POP_RENDERER_MARKER(_pRenderer);
			return true;
		};
		CGfxUtility::AddDraw(renderFunc, NULL, NULL);
	}
	return;
}


///-------------------------------------------------------------------------------------------------
/// リソース構築
b8		CModelComponent::Create(MEM_HANDLE _memHdl, const MAT_DESC* _pMatDesc, u16 _matNum, const MESH_DESC* _pMeshDesc, u16 _meshNum)
{
	u32 texSize = 0;
	for (u16 matIdx = 0; matIdx < _matNum; ++matIdx)
	{
		texSize += sizeof(CResTexture*) * _pMatDesc[matIdx].texNum;
	}
	u32 matSize = sizeof(CGIModel::MAT_DESC*) * _matNum;
	u32 meshSize = sizeof(CGIModel::MESH_DESC) * _meshNum;
	u32 totalSize = texSize + matSize + meshSize;
	u8* pBuf = PCast<u8*>(alloca(totalSize));
	Assert(pBuf);

	const CResTexture** ppResTex = PCast<const CResTexture**>(pBuf);
	pBuf += texSize;
	CGIModel::MAT_DESC* pMatDesc = PCast<CGIModel::MAT_DESC*>(pBuf);
	pBuf += matSize;
	CGIModel::MESH_DESC* pMeshDesc = PCast<CGIModel::MESH_DESC*>(pBuf);
	pBuf += meshSize;

	u16 texCount = 0;
	for (u16 matIdx = 0; matIdx < _matNum; ++matIdx)
	{
		CResMaterialManager* pMatResMng = CModelResourceUtility::GetMaterialManager();
		pMatDesc[matIdx].pResMat = pMatResMng->Search(_pMatDesc[matIdx].matNameCrc);
		if (pMatDesc[matIdx].pResMat && pMatDesc[matIdx].pResMat->IsEnable())
		{
			pMatDesc[matIdx].ppResTex = &ppResTex[texCount];
			pMatDesc[matIdx].texNum = _pMatDesc[matIdx].texNum;
			for (u16 texIdx = 0; texIdx < _pMatDesc[matIdx].texNum; ++texIdx)
			{
				CResTextureManager* pTexResMng = CModelResourceUtility::GetTextureManager();
				ppResTex[texCount + texIdx] = pTexResMng->Search(_pMatDesc[matIdx].pTexNameCrc[texIdx]);
			}
			texCount += _pMatDesc[matIdx].texNum;
		}
	}

	for (u16 meshIdx = 0; meshIdx < _meshNum; ++meshIdx)
	{
		CResMeshManager* pMeshResMng = CModelResourceUtility::GetMeshManager();
		pMeshDesc[meshIdx].pResMesh = pMeshResMng->Search(_pMeshDesc[meshIdx].meshNameCrc);
		if (pMeshDesc[meshIdx].pResMesh && pMeshDesc[meshIdx].pResMesh->IsEnable())
		{
			u16 matIdx = 0;
			for (; matIdx < _meshNum; ++matIdx)
			{
				if (_pMeshDesc[meshIdx].matNameCrc == _pMatDesc[matIdx].matNameCrc) {
					break;
				}
			}
			pMeshDesc[meshIdx].matIdx = matIdx;
		}
	}

	CObject* pObj = GetObject();

	// モデル作成
	return m_model.Create(pObj->GetName(), _memHdl, pMatDesc, _matNum, pMeshDesc, _meshNum);
}


WORK_NAMESPACE_END

﻿#ifndef	__TEST_CFG_LOADER_H__
#define	__TEST_CFG_LOADER_H__


//*************************************************************************************************
//@brief	テストCFGローダー
//*************************************************************************************************
STATIC_SINGLETON( CTestCfgLoader )
{
public:
	FRIEND_STATIC_SINGLETON(CTestCfgLoader);
	friend class CTestCfgDefInner;


public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コマンドファイルの解析
	//@return	成功か
	//@param[in]	_fileBuf		コマンドファイルバッファ
	//@param[in]	_fileSize		コマンドファイルサイズ
	//@note		渡されたコマンドファイルを解析し、本クラスで所持する
	static b8	BuildResource(const void* _fileBuf, u32 _fileSize);
};


#endif	// __TEST_CFG_LOADER_H__

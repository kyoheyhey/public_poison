﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "camera.h"


WORK_NAMESPACE_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


CCamera::CCamera()
	: m_camPos(0.0f, 0.0f, 1.0f)
	, m_refPos(0.0f, 0.0f, 0.0f)
	, m_upVec(0.0f, 1.0f, 0.0f)
	, m_near(0.1f)
	, m_far(1000.0f)
	, m_aspect(16.0f/9.0f)
	, m_fov(toRadian(45.0f))
	, m_width(1200.0f)
	, m_height(720.0f)
	, m_isOrtho(false)
	, m_mtxWV(CMtx44::Identity)
	, m_mtxVP(CMtx44::Identity)
{
}
CCamera::~CCamera()
{
}


///-------------------------------------------------------------------------------------------------
/// ビュー行列取得
const CMtx44&	CCamera::CalcViewMtx()
{
	Mtx::LookAtView(m_mtxWV, m_camPos, m_refPos, m_upVec);
	return m_mtxWV;
}

///-------------------------------------------------------------------------------------------------
/// プロジェクション行列取得
const CMtx44& CCamera::CalcProjectionMtx()
{
	if (!IsOrtho())
	{
		Mtx::Perspective(m_mtxVP, m_fov, m_aspect, m_near, m_far);
	}
	else
	{
		Mtx::Ortho(m_mtxVP, m_width, m_height, m_near, m_far);
	}
	return m_mtxVP;
}


WORK_NAMESPACE_END

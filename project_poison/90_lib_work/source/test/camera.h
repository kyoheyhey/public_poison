﻿//#pragma once
#ifndef __CAMERA_H__
#define __CAMERA_H__


//-------------------------------------------------------------------------------------------------
// include


WORK_NAMESPACE_BGN
//-------------------------------------------------------------------------------------------------
// prototype


//*************************************************************************************************
//@brief	カメラ
//*************************************************************************************************
class CCamera
{
public:
	CCamera();
	~CCamera();

	//-------------------------------------------------------------------------------------------------
	//@brief	カメラ座標取得・設定
	const CVec3&	GetPos() const { return m_camPos; }
	void			SetPos(const CVec3& _pos) { m_camPos = _pos; }
	
	//-------------------------------------------------------------------------------------------------
	//@brief	カメラ参照点位置取得・設定
	const CVec3&	GetRef() const { return m_refPos; }
	void			SetRef(const CVec3& _vec) { m_refPos = _vec; }
	
	//-------------------------------------------------------------------------------------------------
	//@brief	カメラ↑ベクトル取得・設定
	const CVec3&	GetUp() const { return m_upVec; }
	void			SetUp(const CVec3& _vec) { m_upVec = _vec; }


	//-------------------------------------------------------------------------------------------------
	//@brief	ニア取得・設定
	const f32		GetNear() const { return m_near; }
	void			SetNear(const f32 _near) { m_near = _near; }

	//-------------------------------------------------------------------------------------------------
	//@brief	ファー取得・設定
	const f32		GetFar() const { return m_far; }
	void			SetFar(const f32 _far) { m_far = _far; }

	//-------------------------------------------------------------------------------------------------
	//@brief	アスペクト比取得・設定
	const f32		GetAspect() const { return m_aspect; }
	void			SetAspect(const f32 _aspect) { m_aspect = _aspect; }

	//-------------------------------------------------------------------------------------------------
	//@brief	画角取得・設定
	const f32		GetFov() const { return m_fov; }
	void			SetFov(const f32 _fov) { m_fov = _fov; }

	//-------------------------------------------------------------------------------------------------
	//@brief	正射影かどうか取得・設定
	const b8		IsOrtho() const { return m_isOrtho; }
	void			SetOrtho(const b8 _enable) { m_isOrtho = _enable; }


	//-------------------------------------------------------------------------------------------------
	//@brief	ビュー行列取得
	const CMtx44&	CalcViewMtx();

	//-------------------------------------------------------------------------------------------------
	//@brief	プロジェクション行列取得
	const CMtx44&	CalcProjectionMtx();


private:
	CVec3	m_camPos;
	CVec3	m_refPos;
	CVec3	m_upVec;

	f32		m_near;
	f32		m_far;
	f32		m_aspect;
	f32		m_fov;
	f32		m_width;
	f32		m_height;
	b8		m_isOrtho;

	CMtx44	m_mtxWV;
	CMtx44	m_mtxVP;
};


WORK_NAMESPACE_END

#endif	// __CAMERA_H__

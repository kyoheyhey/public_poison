﻿//#pragma once
#ifndef __MODEL_COMPONENT_H__
#define __MODEL_COMPONENT_H__


//-------------------------------------------------------------------------------------------------
// include
#include "resource/resource_base.h"
#include "object/component.h"

#include "model_resource.h"



WORK_NAMESPACE_BGN
//-------------------------------------------------------------------------------------------------
// prototype
class CGIModel;
class CModelComponent;


//*************************************************************************************************
//@brief	マテリアルゲームインスタンス
//*************************************************************************************************
class CGIMaterial
{
public:
	friend class CGIModel;
	friend class CModelComponent;

public:
	CGIMaterial();
	~CGIMaterial();

	//-------------------------------------------------------------------------------------------------
	//@brief	リソース構築
	b8		Create(const CResMaterial* _pResMat, const CResTexture** _ppResTex, u8 _texNum);

	//-------------------------------------------------------------------------------------------------
	//@brief	解放処理
	void	Release();

private:
	const CResMaterial*	m_pResMat = NULL;	///< マテリアルリソース
	const CResTexture*	m_ppResTex[CResMaterial::TEX_MAX_NUM] = {};	///< テクスチャリソース配列
	u8					m_texNum = 0;		///< テクスチャ数
};


//*************************************************************************************************
//@brief	メッシュゲームインスタンス
//*************************************************************************************************
class CGIMesh
{
public:
	friend class CGIModel;
	friend class CModelComponent;

public:
	CGIMesh();
	~CGIMesh();

	//-------------------------------------------------------------------------------------------------
	//@brief	リソース構築
	b8		Create(const CResMesh* _pResMesh, u16 _matIdx, hash32 _shaderNameCrc);

	//-------------------------------------------------------------------------------------------------
	//@brief	解放処理
	void	Release();

private:
	const FxShader* m_pShader = NULL;	///< シェーダリソース
	const CResMesh*	m_pResMesh = NULL;	///< メッシュリソース
	u16				m_matIdx = 0;		///< マテリアルインデックス
};


//*************************************************************************************************
//@brief	モデルゲームインスタンス
//*************************************************************************************************
class CGIModel
{
public:
	friend class CModelComponent;

public:
	//@brief	マテリアル構築
	struct MAT_DESC
	{
		const CResMaterial*	pResMat = NULL;
		const CResTexture** ppResTex = NULL;
		u8					texNum = 0;
	};
	//@brief	メッシュ構築
	struct MESH_DESC
	{
		const CResMesh* pResMesh = NULL;
		u16				matIdx = 0;
	};

public:
	CGIModel();
	~CGIModel();

	//-------------------------------------------------------------------------------------------------
	//@brief	リソース構築
	b8		Create(const c8* _name, MEM_HANDLE _memHdl, const MAT_DESC* _pMatDesc, u16 _matNum, const MESH_DESC* _pMeshDesc, u16 _meshNum);

	//-------------------------------------------------------------------------------------------------
	//@brief	解放処理
	void	Release();

private:
	CGIMaterial*	m_pMaterial = NULL;			///< マテリアルインスタンス
	CGIMesh*		m_pMesh = NULL;				///< メッシュインスタンス
	u16				m_matNum = 0;				///< マテリアル数
	u16				m_meshNum = 0;				///< メッシュ数
	c8				m_name[32];
	hash32			m_nameCrc;
	CMtx44			m_drawModelMtx;				///< 描画用モデル行列
	MEM_HANDLE		m_memHdl = MEM_HDL_INVALID;	///< メモリハンドル
};




//*************************************************************************************************
//@brief	モデルコンポーネント
//*************************************************************************************************
COMPONENT( CModelComponent )
{
public:
	enum { ENABLE_STEP = STEP | PREPARE_DRAW | DRAW };

public:
	//@brief	マテリアル構築
	struct MAT_DESC
	{
		hash32	matNameCrc;
		hash32* pTexNameCrc = NULL;
		u8		texNum = 0;
	};
	//@brief	メッシュ構築
	struct MESH_DESC
	{
		hash32 meshNameCrc;
		hash32 matNameCrc;
	};

public:
	CModelComponent() {}
	~CModelComponent() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	オブジェクトとリンクしたときのコールバック
	virtual void	OnLinkObject() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	virtual void	Finalize() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	オーバーライド用Step関数
	virtual void	Step() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	オーバーライド用PrepareDraw関数
	virtual void	PrepareDraw() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	オーバーライド用PrepareDraw関数
	virtual void	Draw() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	オーバーライド用Build関数
	virtual b8		Build() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	開始関数
	static b8		CallbackStepBgn(CComponentFunc* _compFunc, STEP_TYPE _type);

	//-------------------------------------------------------------------------------------------------
	//@brief	終了関数
	static void		CallbackStepEnd(CComponentFunc* _compFunc, STEP_TYPE _type);

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	リソース構築
	b8		Create(MEM_HANDLE _memHdl, const MAT_DESC* _pMatDesc, u16 _matNum, const MESH_DESC* _pMeshDesc, u16 _meshNum);


	//-------------------------------------------------------------------------------------------------
	//@brief	モデル取得
	const CGIModel&	GetModel() const { return m_model; }
	CGIModel&		GetModel() { return m_model; }

private:
	CGIModel	m_model;		///< モデル

};

WORK_NAMESPACE_END

// クラス登録
REGIST_NAMED_CLASS_NAME(WORK_NAMESPACE, CModelComponent);


#endif	// __MODEL_COMPONENT_H__

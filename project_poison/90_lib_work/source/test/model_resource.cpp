﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "model_resource.h"



WORK_NAMESPACE_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant
static CResTextureManager	s_resTextureManager;
static CResMaterialManager	s_resMaterialManager;
static CResMeshManager		s_resMeshManager;


///*************************************************************************************************
/// テクスチャリソース
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// 解放処理
void	CResTexture::Release()
{
	m_texture.Release();
}

///-------------------------------------------------------------------------------------------------
/// リソース構築済みか
b8		CResTexture::IsEnable() const
{
	return m_texture.IsEnable();
}

///-------------------------------------------------------------------------------------------------
/// 生成処理
b8		CResTexture::Create(const TEX_DESC& _desc, GFX_TEXTURE_TYPE _texType, const c8* _name)
{
	switch (_texType)
	{
	case Poison::GFX_TEXTURE_1D:
	case Poison::GFX_TEXTURE_2D:
	case Poison::GFX_TEXTURE_3D:
	case Poison::GFX_TEXTURE_2DARRAY:
		if (!m_texture.Create2D(_desc))
		{
			Assert(0);
			return false;
		}
		break;
	case Poison::GFX_TEXTURE_CUBEMAP:
	case Poison::GFX_TEXTURE_CUBEMAPARRAY:
		if (!m_texture.CreateCube(_desc))
		{
			Assert(0);
			return false;
		}
		break;
	default:
		break;
	}

	// 名前保持
	strcpy_s(m_name, _name);
	if (m_nameCrc == hash32::Invalid)
	{
		m_nameCrc = CRC32(_name);
	}
	return true;
}

///-------------------------------------------------------------------------------------------------
/// テクスチャバッファ取得
const CTextureBuffer* CResTexture::GetTexBuffer() const
{
	return m_texture.IsEnable() ? &m_texture : NULL;
}


///*************************************************************************************************
/// マテリアルリソース
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// 解放処理
void	CResMaterial::Release()
{
	m_rasterizer.Release();
	m_blendState.Release();
	m_depthState.Release();
	for (u8 i = 0; i < TEX_MAX_NUM; ++i)
	{
		m_sampler[i].Release();
	}
	m_samplerNum = 0;
}

///-------------------------------------------------------------------------------------------------
/// リソース構築済みか
b8		CResMaterial::IsEnable() const
{
	return (m_shaderName != hash32::Invalid);	// シェーダ名が設定されてるかどうか
}

///-------------------------------------------------------------------------------------------------
/// 生成処理
b8		CResMaterial::Create(const MATERIAL_DESC& _desc, const c8* _name)
{
	if (_desc.pRasterizer)
	{
		if (!m_rasterizer.Create(*_desc.pRasterizer))
		{
			Assert(0);
			return false;
		}
	}

	if (_desc.pBlendState)
	{
		if (!m_blendState.Create(*_desc.pBlendState))
		{
			Assert(0);
			return false;
		}
	}

	if (_desc.pDepthState)
	{
		if (!m_depthState.Create(*_desc.pDepthState))
		{
			Assert(0);
			return false;
		}
	}

	for (u8 i = 0; i < _desc.samplerNum; ++i)
	{
		if (!m_sampler[i].Create(_desc.pSampler[i]))
		{
			Assert(0);
			return false;
		}
	}

	m_shaderName = _desc.shaderName;

	// 名前保持
	strcpy_s(m_name, _name);
	if (m_nameCrc == hash32::Invalid)
	{
		m_nameCrc = CRC32(_name);
	}
	return true;
}

///-------------------------------------------------------------------------------------------------
/// ラスタライザバッファ取得
const CRasterizer* CResMaterial::GetRasterizer() const
{
	return m_rasterizer.IsEnable() ? &m_rasterizer : NULL;
}

///-------------------------------------------------------------------------------------------------
/// ブレンドステートバッファ取得
const CBlendState* CResMaterial::GetBlendState() const
{
	return m_blendState.IsEnable() ? &m_blendState : NULL;
}

///-------------------------------------------------------------------------------------------------
/// デプスステンシルステートバッファ取得
const CDepthState* CResMaterial::GetDepthState() const
{
	return m_depthState.IsEnable() ? &m_depthState : NULL;
}

///-------------------------------------------------------------------------------------------------
/// サンプラーバッファ取得
const CSampler*	CResMaterial::GetSamplerBuffer(u8 _idx) const
{
	Assert(TEX_MAX_NUM > _idx);
	return m_sampler[_idx].IsEnable() ? &m_sampler[_idx] : NULL;
}


///*************************************************************************************************
/// メッシュリソース
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// 解放処理
void	CResMesh::Release()
{
	for (u32 i = 0; i < VERTEX_ATTRIBUTE_TYPE_NUM; ++i)
	{
		if (m_vtxBuffer[i].IsEnable())
		{
			m_vtxBuffer[i].Release();
		}
	}
	if (m_idxBuffer.IsEnable())
	{
		m_idxBuffer.Release();
	}
}

///-------------------------------------------------------------------------------------------------
/// リソース構築済みか
b8		CResMesh::IsEnable() const
{
	return (m_vtxBuffer[VTX_ATTR_INTERLEAVED].IsEnable());	// スロット０は必ず作るので
}

///-------------------------------------------------------------------------------------------------
/// 生成処理
b8		CResMesh::Create(const MESH_DECS& _desc, const c8* _name)
{
	for (u32 declIdx = 0; declIdx < _desc.vtxDeclNum; ++declIdx)
	{
		u32 attr = _desc.pVtxDecl[declIdx].slot;
		if (!m_vtxBuffer[attr].Create(_desc.pVtxDesc[declIdx]))
		{
			Assert(0);
			return false;
		}
		if (_desc.vtxDescNum <= 1)
		{
			break;
		}
	}

	// インデックス生成
	if (_desc.pIdxDesc)
	{
		if (!m_idxBuffer.Create(*_desc.pIdxDesc))
		{
			Assert(0);
			return false;
		}
	}

	// 頂点属性コピー
	memcpy(m_vtxDecl, _desc.pVtxDecl, sizeof(VTX_DECL) * _desc.vtxDeclNum);
	// 頂点属性数
	m_vtxDeclNum = _desc.vtxDeclNum;

	// プリミティブタイプ設定
	m_primitive = _desc.primitive;

	// 名前保持
	strcpy_s(m_name, _name);
	if (m_nameCrc == hash32::Invalid)
	{
		m_nameCrc = CRC32(_name);
	}

	return true;
}

///-------------------------------------------------------------------------------------------------
/// 頂点バッファ取得
const CVertexBuffer* CResMesh::GetVtxBuffer(VERTEX_ATTRIBUTE _attr) const
{
	Assert(VERTEX_ATTRIBUTE_TYPE_NUM > _attr);
	return m_vtxBuffer[_attr].IsEnable() ? &m_vtxBuffer[_attr] : NULL;
}

///-------------------------------------------------------------------------------------------------
/// インデックスバッファ取得
const CIndexBuffer* CResMesh::GetIdxBuffer() const
{
	return m_idxBuffer.IsEnable() ? &m_idxBuffer : NULL;
}


///-------------------------------------------------------------------------------------------------
/// メッシュ描画
void		CResMesh::RenderFuncMesh(CRenderer* _pRenderer, const CResMesh* _pResMesh)
{
	// 頂点バッファ設定
	const VTX_DECL* pDecl = _pResMesh->m_vtxDecl;
	for (u32 i = 0; i < _pResMesh->m_vtxDeclNum; ++i, ++pDecl)
	{
		VERTEX_ATTRIBUTE attr = SCast<VERTEX_ATTRIBUTE>(pDecl->slot);
		_pRenderer->SetVertex(_pResMesh->GetVtxBuffer(attr), attr);
	}
	const CIndexBuffer* pIdxBuf = _pResMesh->GetIdxBuffer();
	if (pIdxBuf)
	{
		// インデックスバッファ描画
		_pRenderer->SetIndex(pIdxBuf);
		_pRenderer->DrawIndexPrim(_pResMesh->m_primitive, pIdxBuf->GetIdxNum());
	}
	else
	{
		// 描画
		_pRenderer->DrawPrim(_pResMesh->m_primitive, _pResMesh->GetVtxBuffer()->GetVtxNum());
	}
}


///*************************************************************************************************
/// テクスチャリソース
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// 初期化
b8			CModelResourceUtility::Initialize(IAllocator* _pAllocator)
{
	if (!s_resTextureManager.Initialize(_pAllocator, 32)) { return false; }
	if (!s_resMaterialManager.Initialize(_pAllocator, 32)) { return false; }
	if (!s_resMeshManager.Initialize(_pAllocator, 32)) { return false; }
	return true;
}

///-------------------------------------------------------------------------------------------------
/// 破棄チェック
void		CModelResourceUtility::CheckDelete()
{
	s_resTextureManager.CheckDelete();
	s_resMaterialManager.CheckDelete();
	s_resMeshManager.CheckDelete();
}

///-------------------------------------------------------------------------------------------------
/// 終了
void		CModelResourceUtility::Finalize()
{
	s_resTextureManager.Finalize();
	s_resMaterialManager.Finalize();
	s_resMeshManager.Finalize();
}

///-------------------------------------------------------------------------------------------------
/// テクスチャリソースマネージャー取得
CResTextureManager*		CModelResourceUtility::GetTextureManager()
{
	return &s_resTextureManager;
}

///-------------------------------------------------------------------------------------------------
/// マテリアルリソースマネージャー取得
CResMaterialManager*	CModelResourceUtility::GetMaterialManager()
{
	return &s_resMaterialManager;
}

///-------------------------------------------------------------------------------------------------
/// メッシュリソースマネージャー取得
CResMeshManager*		CModelResourceUtility::GetMeshManager()
{
	return &s_resMeshManager;
}


WORK_NAMESPACE_END

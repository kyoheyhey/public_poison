﻿
#include "test_cfg_loader.h"
#include "script/script_interpreter.h"


//-------------------------------------------------------------------------------------------------
//読み込み時にのみ使用する情報
struct RUN_INFO_ST
{
	b8	isSkip;
	hash32	prevName;

	RUN_INFO_ST()
		:isSkip(false)
		, prevName(0)
	{}
};


///-------------------------------------------------------------------------------------------------
/// 解析用クラス
class CTestCfgDefInner
{
public:
	///-------------------------------------------------------------------------------------------------
	// テスト１定義開始タグ
	static s32 Tag_TEST1_INFO_LIST_BEG(SI_STACK _st)
	{
		RUN_INFO_ST* pRunInfo = PCast<RUN_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }

		if (siGetStackRemain(_st) <= 0) { return 1; }
		const c8* pName = siGetStackString_inc(_st);
		hash32 rtName = CRC32(pName);

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// テスト１定義情報タグ
	static s32 Tag_TEST1_INFO(SI_STACK _st)
	{
		RUN_INFO_ST* pRunInfo = PCast<RUN_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		
		if (siGetStackRemain(_st) <= 0) { return 1; }
		s32 intParam = siGetStackInt_inc(_st);

		if (siGetStackRemain(_st) <= 0) { return 1; }
		f32 floatParam = siGetStackFloat_inc(_st);

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// テスト１定義終了タグ
	static s32 Tag_TEST1_INFO_LIST_END(SI_STACK _st)
	{
		RUN_INFO_ST* pRunInfo = PCast<RUN_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// テスト２定義開始タグ
	static s32 Tag_TEST2_INFO_LIST_BEG(SI_STACK _st)
	{
		RUN_INFO_ST* pRunInfo = PCast<RUN_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }

		if (siGetStackRemain(_st) <= 0) { return 1; }
		hash32 crcParam = siGetStackCRC_inc(_st);

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// テスト２定義情報タグ
	static s32 Tag_TEST2_INFO(SI_STACK _st)
	{
		RUN_INFO_ST* pRunInfo = PCast<RUN_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }

		if (siGetStackRemain(_st) <= 0) { return 1; }
		s32 intParam0 = siGetStackInt_inc(_st);

		if (siGetStackRemain(_st) <= 0) { return 1; }
		s32 intParam1 = siGetStackInt_inc(_st);

		if (siGetStackRemain(_st) <= 0) { return 1; }
		hash32 crcParam = siGetStackCRC_inc(_st);

		return 1;
	}

	///-------------------------------------------------------------------------------------------------
	// テスト２定義終了タグ
	static s32 Tag_TEST2_INFO_LIST_END(SI_STACK _st)
	{
		RUN_INFO_ST* pRunInfo = PCast<RUN_INFO_ST*>(_st.GetExtParam());
		if (!pRunInfo) { return 0; }
		return 1;
	}
};


//-------------------------------------------------------------------------------------------------
// 設定ファイル解析関数リスト
SI_TAG_TABLE_INSTANCE_BEGIN(s_CDefBuildTagList)[] =
{
	SI_TAG_C2(TEST1_INFO_LIST_BEG, CTestCfgDefInner, Tag_),
	SI_TAG_C2(TEST1_INFO, CTestCfgDefInner, Tag_),
	SI_TAG_C2(TEST1_INFO_LIST_END, CTestCfgDefInner, Tag_),

	SI_TAG_C2(TEST2_INFO_LIST_BEG, CTestCfgDefInner, Tag_),
	SI_TAG_C2(TEST2_INFO, CTestCfgDefInner, Tag_),
	SI_TAG_C2(TEST2_INFO_LIST_END, CTestCfgDefInner, Tag_),

	SI_TAG_TERM()
};
SI_TAG_TABLE_INSTANCE_END(s_CDefBuildTagList);


//-------------------------------------------------------------------------------------------------
///@brief						コマンドファイルからメモリ定義ファイル生成
///@return						成功か
///@param[in]	_retMemInfo		作成したメモリ定義
///@param[in]	_fileBuf		コマンドファイルバッファ
///@param[in]	_fileSize		コマンドファイルサイズ
///@note						
b8	CTestCfgLoader::BuildResource(const void* _fileBuf, u32 _fileSize)
{
	// 無効
	if (NULL == _fileBuf || _fileSize <= 0)
	{
		return false;
	}

	// スクリプトインタプリタを作成
	CScriptInterpreter si;

	RUN_INFO_ST runInfo;

	si.SetTag(SI_SET_TAG_PARAM(s_CDefBuildTagList));
	si.SetExtParam(&runInfo);
	si.SetScript(_fileBuf, _fileSize);

	// 解析
	b8 succeeded = si.Run();

	return succeeded;
}


﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "camera_ctrl_maya.h"


WORK_NAMESPACE_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant
static const f32 s_limitUpperRot = +PI / 2 - toRadian(1.0f);
static const f32 s_limitUnderRot = -PI / 2 + toRadian(1.0f);




CCameraCtrlMaya::CCameraCtrlMaya()
{
    m_pos = CVec3(0.0f, 0.0f, 1.0f);
    m_spc.SetAzimuth(toRadian(180.0f));
    m_spc.SetAltitude(toRadian(0.0f));
    m_spc.SetRadius(1.0f);
    m_roll = 0.0f;
}
CCameraCtrlMaya::~CCameraCtrlMaya()
{
}

///-------------------------------------------------------------------------------------------------
/// 更新
void	CCameraCtrlMaya::Update(const UPDATA_PARAM& _param)
{
    CVec3 pos = m_pCamera->GetPos();
    CVec3 ref = m_pCamera->GetRef();
    CVec3 refVec = ref - pos;
    CVec3 up = m_pCamera->GetUp();

    f32 length = Vec::Length(refVec);
    f32 aspect = m_pCamera->GetAspect();
    CAngle heightFov = m_pCamera->GetFov() * 0.5f;
    f32 height = tanf(heightFov) * length;
    f32 width = height * aspect;
    CAngle widthFov = atanf(width / length);
    heightFov *= 2.0f;
    widthFov *= 2.0f;
    height *= 2.0f;
    width *= 2.0f;

    m_pos = pos;
    CSpc::VecZToSpc(m_spc, refVec);

	switch (_param.type)
	{
    case FPS_ROT:
    {
        m_spc.SetAzimuth(m_spc.GetAzimuth() + widthFov * +_param.screenVec[0]);
#if defined( OPENGL_VIEWPORT )
        f32 altitude = m_spc.GetAltitude() + heightFov * +_param.screenVec[1];
#elif defined( DIRECTX_VIEWPORT )
        f32 altitude = m_spc.GetAltitude() + heightFov * -_param.screenVec[1];
#endif
        m_spc.SetAltitude(clamp(s_limitUnderRot, s_limitUpperRot, altitude));
        CVec3 spcVec;
        CSpc::SpcToVecZ(spcVec, m_spc);
        ref = pos + spcVec;
        m_spc.SetRadius(clamp(m_pCamera->GetNear(), m_pCamera->GetFar(), m_spc.GetRadius() + _param.screenVec[2]));
        CSpc::SpcToVecZ(refVec, m_spc);
        pos = ref - refVec;
        break;
    }
    case TPS_ROT:
    {
        m_spc.SetAzimuth(m_spc.GetAzimuth() + widthFov * -_param.screenVec[0]);
#if defined( OPENGL_VIEWPORT )
        f32 altitude = m_spc.GetAltitude() + heightFov * -_param.screenVec[1];
#elif defined( DIRECTX_VIEWPORT )
        f32 altitude = m_spc.GetAltitude() + heightFov * +_param.screenVec[1];
#endif
        m_spc.SetAltitude(clamp(s_limitUnderRot, s_limitUpperRot, altitude));
        m_spc.SetRadius(clamp(m_pCamera->GetNear(), m_pCamera->GetFar(), m_spc.GetRadius() + _param.screenVec[2]));
        CSpc::SpcToVecZ(refVec, m_spc);
        pos = ref - refVec;
        break;
    }
    case TRANS:
    {
        CVec3 viewX, viewY, viewZ;
        Vec::Normalize(viewZ, -refVec);
        Vec::Normalize(viewY, up);
        Vec::Cross(viewX, viewY, viewZ);

        CVec3 move = CVec3::Zero;
        move += viewX * -_param.screenVec[0] * width;
#if defined( OPENGL_VIEWPORT )
        move += viewY * -_param.screenVec[1] * height;
#elif defined( DIRECTX_VIEWPORT )
        move += viewY * +_param.screenVec[1] * height;
#endif
        pos += move;
        m_spc.SetRadius(clamp(m_pCamera->GetNear(), m_pCamera->GetFar(), m_spc.GetRadius() + _param.screenVec[2]));
        CSpc::SpcToVecZ(refVec, m_spc);
        ref = pos + refVec;
        break;
    }
    case ZOOM:
    {
        static f32 s_zoomSpeed = 10.0f;
        f32 moveLen = length;
        moveLen += s_zoomSpeed * _param.screenVec[0];
        moveLen = clamp(m_pCamera->GetNear(), m_pCamera->GetFar(), moveLen);
        m_spc.SetRadius(moveLen);
        CSpc::SpcToVecZ(refVec, m_spc);
        pos = ref - refVec;
        break;
    }
    case ROLL:
    {
        CAngle roll = 0.0f;
        roll -= toRadian(180.0f) * _param.screenVec[0];
#if defined( OPENGL_VIEWPORT )
        roll -= toRadian(180.0f) * _param.screenVec[0];
#elif defined( DIRECTX_VIEWPORT )
        roll += toRadian(180.0f) * _param.screenVec[0];
#endif
        m_roll += roll;
        break;
    }
    default:
        break;
	}

    // ↑ベクトル算出
    {
        CSpc spcY = m_spc;
        spcY.SetAltitude(spcY.GetAltitude() - toRadian(90.0f));
        CSpc::SpcToVecZ(up, spcY);

        if (Vec::Dot(CVec3::Up, up) <= 0.0f)
        {
            b8 test;
            test = true;
        }

        Vec::Normalize(up, up);
        // ロール回転
        CVec3 nrmRef;
        Vec::Normalize(nrmRef, -refVec);
        CQuat rollQuat;
        Rot::Rotate(rollQuat, nrmRef, m_roll);
        Rot::Transform(up, rollQuat, up);
    }

    // 平行移動
    {
        CVec3 viewX, viewY, viewZ;
        Vec::Normalize(viewZ, -refVec);
        Vec::Normalize(viewY, up);
        Vec::Cross(viewX, viewY, viewZ);

        CVec3 move = CVec3::Zero;
        move += viewX * _param.trans[0];
        move += viewY * _param.trans[1];
        move += viewZ * _param.trans[2];
        pos += move;
        ref += move;
    }
    m_pCamera->SetPos(pos);
    m_pCamera->SetRef(ref);
    m_pCamera->SetUp(up);

}


WORK_NAMESPACE_END

﻿//#pragma once
#ifndef __CAMERA_CTRL_MAYA_H__
#define __CAMERA_CTRL_MAYA_H__


//-------------------------------------------------------------------------------------------------
// include
#include "camera.h"
#include "math/spc.h"


WORK_NAMESPACE_BGN
//-------------------------------------------------------------------------------------------------
// prototype


//*************************************************************************************************
//@brief	MAYAライクカメラコントローラ
//*************************************************************************************************
class CCameraCtrlMaya
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	動きタイプ
	enum MOVE_TYPE : u8
	{
		FPS_ROT = 0,
		TPS_ROT,
		TRANS,
		ZOOM,
		ROLL,
	};

	//-------------------------------------------------------------------------------------------------
	//@brief	更新パラメータ
	struct UPDATA_PARAM
	{
		CVec3		screenVec = CVec3::Zero;
		CVec3		trans = CVec3::Zero;
		MOVE_TYPE	type = FPS_ROT;
	};

public:
	CCameraCtrlMaya();
	~CCameraCtrlMaya();

	//-------------------------------------------------------------------------------------------------
	//@brief	セットアップ
	void	Setup(CCamera* _pCamera) { m_pCamera = _pCamera; }

	//-------------------------------------------------------------------------------------------------
	//@brief	更新
	void	Update(const UPDATA_PARAM& _param);


private:
	CCamera*		m_pCamera = NULL;	///< カメラ

	CVec3			m_pos;
	CSpc			m_spc;
	CAngle			m_roll = 0.0f;
};


WORK_NAMESPACE_END

#endif	// __CAMERA_CTRL_MAYA_H__

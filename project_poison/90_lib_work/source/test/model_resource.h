﻿//#pragma once
#ifndef __MODEL_RESOURCE_H__
#define __MODEL_RESOURCE_H__


//-------------------------------------------------------------------------------------------------
// include
// バッファオブジェクト
#include "object/vertex_buffer.h"
#include "object/index_buffer.h"
#include "object/rasterrizer_state.h"
#include "object/blend_state.h"
#include "object/depth_state.h"
#include "object/texture_buffer.h"
#include "object/sampler_state.h"

// シェーダ
#include "shader/shader.h"
#include "resource/shader_library.h"



WORK_NAMESPACE_BGN
//-------------------------------------------------------------------------------------------------
// prototype


//*************************************************************************************************
//@brief	テクスチャリソース
//*************************************************************************************************
RES_CLASS( CResTexture )
{
public:
	CResTexture() {}
	~CResTexture() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	解放処理
	virtual	void	Release() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	リソース構築済みか
	virtual b8		IsEnable() const override;

	//-------------------------------------------------------------------------------------------------
	//@brief	名前のCRC取得・設定
	virtual hash32	GetNameCrc() const override { return m_nameCrc; }
	virtual void	SetNameCrc(const hash32 _name) override { m_nameCrc = _name; }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	生成処理
	b8		Create(const TEX_DESC& _desc, GFX_TEXTURE_TYPE _texType, const c8* _name);

	//-------------------------------------------------------------------------------------------------
	//@brief	テクスチャバッファ取得
	const CTextureBuffer*	GetTexBuffer() const;

private:
	c8				m_name[32];
	hash32			m_nameCrc;
	CTextureBuffer	m_texture;	///< テクスチャ
};

typedef CResManager<CResTexture>	CResTextureManager;


//*************************************************************************************************
//@brief	マテリアルリソース
//*************************************************************************************************
RES_CLASS( CResMaterial )
{
public:
	//@brief	マテリアル最大テスクチャ数
	enum { TEX_MAX_NUM = 10 };

	//@brief	マテリアルDESC
	struct MATERIAL_DESC
	{
		hash32					shaderName;			///< シェーダ名
		const RASTERIZER_DESC*	pRasterizer = NULL;	///< ラスタライザDESC
		const BLEND_STATE_DESC*	pBlendState = NULL;	///< ブレンドステートDESC
		const DEPTH_STATE_DESC*	pDepthState = NULL;	///< デプスステンシルステートDESC
		const SAMPLER_DESC*		pSampler = NULL;	///< サンプラーDESC
		u8						samplerNum = 0;		///< サンプラーDESC数(TEX_MAX_NUM以下にすること)
	};


public:
	CResMaterial() {}
	~CResMaterial() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	解放処理
	virtual	void	Release() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	リソース構築済みか
	virtual b8		IsEnable() const override;

	//-------------------------------------------------------------------------------------------------
	//@brief	名前のCRC取得・設定
	virtual hash32	GetNameCrc() const override { return m_nameCrc; }
	virtual void	SetNameCrc(const hash32 _name) override { m_nameCrc = _name; }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	生成処理
	b8		Create(const MATERIAL_DESC& _desc, const c8* _name);

	//-------------------------------------------------------------------------------------------------
	//@brief	シェーダ名CRC取得
	const hash32		GetShaderNameCrc() const { return m_shaderName; }

	//-------------------------------------------------------------------------------------------------
	//@brief	ラスタライザバッファ取得
	const CRasterizer*	GetRasterizer() const;

	//-------------------------------------------------------------------------------------------------
	//@brief	ブレンドステートバッファ取得
	const CBlendState*	GetBlendState() const;

	//-------------------------------------------------------------------------------------------------
	//@brief	デプスステンシルステートバッファ取得
	const CDepthState*	GetDepthState() const;

	//-------------------------------------------------------------------------------------------------
	//@brief	サンプラーバッファ取得
	const CSampler*		GetSamplerBuffer(u8 _idx) const;

	//-------------------------------------------------------------------------------------------------
	//@brief	サンプラー数取得
	const u8			GetSamplerNum() const { return m_samplerNum; }

private:
	c8				m_name[32];
	hash32			m_nameCrc;
	hash32			m_shaderName;			///< シェーダ名
	CRasterizer		m_rasterizer;			///< ラスタライザ
	CBlendState		m_blendState;			///< ブレンドステート
	CDepthState		m_depthState;			///< デプスステンシルステート
	CSampler		m_sampler[TEX_MAX_NUM];	///< サンプラーリスト
	u8				m_samplerNum = 0;		///< サンプラー数
};

typedef CResManager<CResMaterial>	CResMaterialManager;


//*************************************************************************************************
//@brief	メッシュリソース
//*************************************************************************************************
RES_CLASS( CResMesh )
{
public:
	//@brief	メッシュDECS
	struct MESH_DECS
	{
		const VTX_DECL*	pVtxDecl = NULL;		///< 頂点属性DECL
		u32				vtxDeclNum = 0;			///< 頂点属性DECL数
		const VTX_DESC*	pVtxDesc = NULL;		///< 頂点バッファDESC
		u32				vtxDescNum = 0;			///< 頂点バッファDESC数
		const IDX_DESC* pIdxDesc = NULL;		///< インデックスバッファDESC
		GFX_PRIMITIVE	primitive = GFX_PRIMITIVE_TRIANGLES;
	};

public:
	CResMesh() {}
	~CResMesh() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	解放処理
	virtual	void	Release() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	リソース構築済みか
	virtual b8		IsEnable() const override;

	//-------------------------------------------------------------------------------------------------
	//@brief	名前のCRC取得・設定
	virtual hash32	GetNameCrc() const override { return m_nameCrc; }
	virtual void	SetNameCrc(const hash32 _name) override { m_nameCrc = _name; }


public:
	//-------------------------------------------------------------------------------------------------
	//@brief	生成処理
	b8		Create(const MESH_DECS& _desc, const c8* _name);
	
	//-------------------------------------------------------------------------------------------------
	//@brief	頂点DECL取得
	const VTX_DECL*			GetVtxDecl() const {return m_vtxDecl;}
	const u32				GetVtxDeclNum() const {return m_vtxDeclNum;}

	//-------------------------------------------------------------------------------------------------
	//@brief	頂点プリミティブタイプ取得
	const GFX_PRIMITIVE		GetPremiteveType() const { return m_primitive; }

	//-------------------------------------------------------------------------------------------------
	//@brief	頂点バッファ取得
	const CVertexBuffer*	GetVtxBuffer(VERTEX_ATTRIBUTE _attr = VTX_ATTR_INTERLEAVED) const;

	//-------------------------------------------------------------------------------------------------
	//@brief	インデックスバッファ取得
	const CIndexBuffer*		GetIdxBuffer() const;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	メッシュ描画
	static void		RenderFuncMesh(CRenderer* _pRenderer, const CResMesh* _pResMesh);

private:
	c8				m_name[32];
	hash32			m_nameCrc;
	VTX_DECL		m_vtxDecl[VERTEX_ATTRIBUTE_TYPE_NUM];		///< 頂点属性DECL
	u32				m_vtxDeclNum = 0;							///< 頂点属性DECL数
	CVertexBuffer	m_vtxBuffer[VERTEX_ATTRIBUTE_TYPE_NUM];		///< 頂点バッファ
	CIndexBuffer	m_idxBuffer;								///< インデックスバッファ
	GFX_PRIMITIVE	m_primitive = GFX_PRIMITIVE_TRIANGLES;	///< プリミティブタイプ
};

typedef CResManager<CResMesh>	CResMeshManager;


//*************************************************************************************************
//@brief	モデルリソースユーティリティ
//*************************************************************************************************
class CModelResourceUtility
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	static b8		Initialize(IAllocator* _pAllocator);

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄チェック
	static void		CheckDelete();

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	static void		Finalize();

	//-------------------------------------------------------------------------------------------------
	//@brief	テクスチャリソースマネージャー取得
	static CResTextureManager*	GetTextureManager();
	
	//-------------------------------------------------------------------------------------------------
	//@brief	マテリアルリソースマネージャー取得
	static CResMaterialManager*	GetMaterialManager();

	//-------------------------------------------------------------------------------------------------
	//@brief	メッシュリソースマネージャー取得
	static CResMeshManager*		GetMeshManager();

};


WORK_NAMESPACE_END

#endif	// __MODEL_RESOURCE_H__

@ECHO OFF

rem 環境変数の遅延展開を有効に
rem setlocal ENABLEDELAYEDEXPANSION

rem カレントディレクトリ設定
rem %~d0
rem CD %~dp0..\

SET CURRENT_PATH=%~dp0

MKDIR %CURRENT_PATH%data\win\shader
MKDIR %CURRENT_PATH%data\common\shader

SET TRG_PATH=%CURRENT_PATH%data\win\shader\glsl
SET LINK_PATH=%CURRENT_PATH%..\shader_works\out\glsl
MKLINK /D "%TRG_PATH%" "%LINK_PATH%"


SET TRG_PATH=%CURRENT_PATH%data\win\shader\hlsl
SET LINK_PATH=%CURRENT_PATH%..\shader_works\out\hlsl
MKLINK /D "%TRG_PATH%" "%LINK_PATH%"


SET TRG_PATH=%CURRENT_PATH%data\win\shader\spirv
SET LINK_PATH=%CURRENT_PATH%..\shader_works\out\spirv
MKLINK /D "%TRG_PATH%" "%LINK_PATH%"


SET TRG_PATH=%CURRENT_PATH%data\common\shader\list
SET LINK_PATH=%CURRENT_PATH%..\shader_works\out\common\list
MKLINK /D "%TRG_PATH%" "%LINK_PATH%"


SET TRG_PATH=%CURRENT_PATH%data\common\shader\fxcfg
SET LINK_PATH=%CURRENT_PATH%..\shader_works\out\common\fxcfg
MKLINK /D "%TRG_PATH%" "%LINK_PATH%"

pause

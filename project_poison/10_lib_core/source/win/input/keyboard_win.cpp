﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "input/keyboard_win.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant



///-------------------------------------------------------------------------------------------------
/// 更新
void	CKeyboardWin::Step()
{
	static const u8	s_winKey[] = 
	{
		VK_MENU,
		VK_CONTROL,
		VK_SPACE,
		VK_RETURN,
		VK_BACK,
		VK_LEFT, VK_RIGHT, VK_UP, VK_DOWN,
		VK_F1, VK_F2, VK_F3, VK_F4, VK_F5, VK_F6, VK_F7, VK_F8, VK_F9, VK_F10, VK_F11, VK_F12,
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
	};
	StaticAssert(ARRAYOF(s_winKey) == KEYBOARD_KEY_NUM);
	for (u32 idx = 0; idx < KEYBOARD_KEY_NUM; idx++)
	{
		m_button[idx].Step(::GetAsyncKeyState(s_winKey[idx]) != 0);
	}
}

POISON_END

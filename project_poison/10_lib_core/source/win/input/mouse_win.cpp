﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "input/mouse_win.h"

#include "device/device_utility.h"
#include "device/device_win.h"
#include "math/matrix.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant



///-------------------------------------------------------------------------------------------------
/// 更新
void	CMouseWin::Step()
{
	CDeviceWin* pDevice = SDCast<CDeviceWin*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	// ウィンドウがリサイズor移動されていたら
	if (pDevice->IsResize() || pDevice->IsReposition() || m_rect == CRect())
	{
		pDevice->GetClientRect(m_rect);
	}
	// マウス座標取得
	::POINT point;
	::GetCursorPos(&point);
	CVec2 pos(
		SCast<f32>((point.x - m_rect.posX) / SCast<f32>(m_rect.width)),
		SCast<f32>((point.y - m_rect.posY) / SCast<f32>(m_rect.height))
		);
#if defined( OPENGL_VIEWPORT )
	pos.y = 1.0f - pos.y;
#elif defined( DIRECTX_VIEWPORT )
#endif
	m_pointer.Step(pos);

	// マウスホイール回転量更新
	SetWheel(SCast<f32>(pDevice->GetMouseWheelVol()));

	// クリック取得
	static const u8	s_winKey[] =
	{
		VK_LBUTTON,
		VK_RBUTTON,
		VK_MBUTTON,
	};
	StaticAssert(ARRAYOF(s_winKey) == MOUSE_KEY_NUM);
	for (u32 idx = 0; idx < MOUSE_KEY_NUM; idx++)
	{
		m_button[idx].Step(::GetAsyncKeyState(s_winKey[idx]) != 0);
	}
}


POISON_END

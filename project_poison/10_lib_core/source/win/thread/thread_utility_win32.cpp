﻿
#ifdef LIB_THREAD_WIN

///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "thread/thread_utility.h"


///-------------------------------------------------------------------------------------------------
/// prototype



POISON_BGN

///-------------------------------------------------------------------------------------------------
/// constant
static THREAD_HANDLE	s_mainThreadHdl = THREAD_HANDLE_INIT;
static u64				s_mainThreadId = 0;

const DWORD MS_VC_EXCEPTION = 0x406D1388;
typedef struct tagTHREADNAME_INFO
{
	DWORD	dwType; // Must be 0x1000.  
	LPCSTR	szName; // Pointer to name (in user addr space).  
	DWORD	dwThreadID; // Thread ID (-1=caller thread).  
	DWORD	dwFlags; // Reserved for future use, must be zero.  
} THREADNAME_INFO;

/// スレッド名設定
void	SetThreadName(u64 _threadID, const c8* _threadName)
{
	THREADNAME_INFO info;
	info.dwType = 0x1000;
	info.szName = _threadName;
	info.dwThreadID = (DWORD)_threadID;
	info.dwFlags = 0;
	__try {
		::RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
	}
	__except (EXCEPTION_EXECUTE_HANDLER) {
	}
}

///-------------------------------------------------------------------------------------------------
/// 初期化
void			CThreadUtility::Initialize()
{
	s_mainThreadHdl = GetCurrentThreadHdl();
	s_mainThreadId = GetThreadID(s_mainThreadHdl);
}

///-------------------------------------------------------------------------------------------------
/// アフィニティ設定
void			CThreadUtility::SetAffinityMask(THREAD_HANDLE _hdl, u32 _mask)
{
	DWORD_PTR mask = _mask;
	::SetThreadAffinityMask(_hdl, mask);
}

///-------------------------------------------------------------------------------------------------
/// 現在のスレッドハンドル取得
THREAD_HANDLE	CThreadUtility::GetCurrentThreadHdl()
{
	return ::GetCurrentThread();
}

///-------------------------------------------------------------------------------------------------
/// 指定のスレッドID取得
u64				CThreadUtility::GetThreadID(const THREAD_HANDLE& _hdl)
{
	return ::GetThreadId(_hdl);
}

///-------------------------------------------------------------------------------------------------
/// メインスレッドかどうか
b8				CThreadUtility::IsMainThread()
{
	return (GetThreadID(GetCurrentThreadHdl()) == s_mainThreadId);
}
b8				CThreadUtility::IsMainThread(u64 _threadId)
{
	return (_threadId == s_mainThreadId);
}

///-------------------------------------------------------------------------------------------------
/// スレッド生成
b8				CThreadUtility::Create(THREAD_HANDLE& _hdl, THREAD_FUNC* _func, void* _arg, const u32 _size, const s32 _prio, const c8* _pName)
{
	u32	tmp_id = 0;
	// スレッド開始処理
	_hdl = (HANDLE)(::_beginthreadex(NULL, _size, _func, _arg, CREATE_SUSPENDED, &tmp_id));
	if (_hdl == INVALID_HANDLE_VALUE || _hdl == HANDLE(0)) { return false; }
	// スレッド名設定
	SetThreadName(GetThreadID(_hdl), _pName);
	// 属性設定
	::SetThreadPriority(_hdl, max(min((_prio + THREAD_PRIO_MIDDLE), THREAD_PRIO_HIGHEST), THREAD_PRIO_LOWEST));
	// スレッド実行
	::ResumeThread(_hdl);
	return true;
}

///-------------------------------------------------------------------------------------------------
/// スレッド終了
THREAD_RESULT	CThreadUtility::Exit(THREAD_RESULT _result)
{
	::_endthreadex(_result);
	return _result;
}

///-------------------------------------------------------------------------------------------------
/// スレッドが稼働しているかどうか
b8				CThreadUtility::IsRunning(const THREAD_HANDLE& _hdl)
{
	DWORD l_dwExitCode;
	if (::GetExitCodeThread(_hdl, &l_dwExitCode))
	{
		return (l_dwExitCode == STILL_ACTIVE);
	}
	return false;
}

///-------------------------------------------------------------------------------------------------
/// 終了を待つ
THREAD_RESULT	CThreadUtility::Wait(const THREAD_HANDLE& _hdl)
{
	DWORD ret = ::WaitForSingleObject(_hdl, INFINITE);
	if (ret == WAIT_ABANDONED)
	{
		AssertBaseMsg(0, "ミューテックスが解放されていません。");
	}
	if (ret == WAIT_TIMEOUT)
	{
		AssertBaseMsg(0, "タイムアウトしました。");
	}
	if (ret == WAIT_FAILED)
	{
		LPVOID lpMsgBuf;
		DWORD errorCord = ::GetLastError();
		::FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
			NULL,
			errorCord,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR)&lpMsgBuf,
			0,
			NULL
		);
		AssertBaseMsg(0, lpMsgBuf);
		::LocalFree(lpMsgBuf);
	}
	DWORD dwExitCode;
	if (::GetExitCodeThread(_hdl, &dwExitCode))
	{
		return THREAD_FUNC_RESULT(dwExitCode);
	}
	// 多分来ないはず
	return THREAD_INVALID_RESULT;
}

///-------------------------------------------------------------------------------------------------
/// スレッド削除
void		CThreadUtility::Delete(THREAD_HANDLE& _hdl, THREAD_RESULT* _pResult/* = NULL*/)
{
	THREAD_RESULT result = Wait(_hdl);
	::CloseHandle(_hdl);
	if (_pResult)
	{
		(*_pResult) = result;
	}
}


///-------------------------------------------------------------------------------------------------
/// ミューテックス生成
b8			CThreadUtility::CreateMutex(THREAD_MUTEX& _mutex)
{
	::memset(&_mutex, 0, sizeof(_mutex));
	::InitializeCriticalSection(&_mutex);
	return true;
}

///-------------------------------------------------------------------------------------------------
/// ミューテックスがロック中かどうか
b8			CThreadUtility::IsMutexLock(THREAD_MUTEX& _mutex)
{
	return (::TryEnterCriticalSection(&_mutex) != 0);
}

///-------------------------------------------------------------------------------------------------
/// ミューテックスロック
SAL_DEF(_Acquires_exclusive_lock_(&_mutex))
void		CThreadUtility::MutexLock(THREAD_MUTEX& _mutex)
{
	::EnterCriticalSection(&_mutex);
}

///-------------------------------------------------------------------------------------------------
/// ミューテックスアンロック
SAL_DEF(_Releases_exclusive_lock_(&_mutex))
void		CThreadUtility::MutexUnLock(THREAD_MUTEX& _mutex)
{
	::LeaveCriticalSection(&_mutex);
}

///-------------------------------------------------------------------------------------------------
/// ミューテックス破棄
void		CThreadUtility::DeleteMutex(THREAD_MUTEX& _mutex)
{
	::DeleteCriticalSection(&_mutex);
}


///-------------------------------------------------------------------------------------------------
/// イベント生成
b8			CThreadUtility::CreateEvent(THREAD_EVENT& _event, const c8* _pName)
{
	_event = ::CreateEvent(NULL, false, false, _pName);
	return (_event != NULL);
}

///-------------------------------------------------------------------------------------------------
/// イベント待機
b8			CThreadUtility::WaitEvent(THREAD_EVENT& _event, s64 _timeOut/* = -1*/)
{
	b8 isEnd = true;
	libAssert(_event);
	if (_timeOut < 0)
	{
		::WaitForSingleObject(_event, INFINITE);
	}
	else
	{
		DWORD dwTimeOut = SCast<DWORD>(_timeOut);
		if (::WaitForSingleObject(_event, dwTimeOut) == WAIT_TIMEOUT)
		{
			isEnd = false;
		}
	}
	return isEnd;
}

///-------------------------------------------------------------------------------------------------
/// イベント発行
void		CThreadUtility::SignalEvent(THREAD_EVENT& _event)
{
	libAssert(_event);
	::SetEvent(_event);
}

///-------------------------------------------------------------------------------------------------
/// イベント破棄
void		CThreadUtility::DeleteEvent(THREAD_EVENT& _event)
{
	libAssert(_event);
	::CloseHandle(_event);
	_event = NULL;
}


POISON_END

#endif // LIB_THREAD_WIN

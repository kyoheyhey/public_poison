﻿
///-------------------------------------------------------------------------------------------------
/// define
#define	APPLICATION_NAME	"POISON"


///-------------------------------------------------------------------------------------------------
/// include
#include "device/device_win.h"

#include <stdio.h>
#include <tchar.h>
#include <limits.h>

#include "win/input/keyboard_win.h"
#include "win/input/mouse_win.h"
#include "input/pad.h"
//#include "win/input/pad_win.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// constant


///-------------------------------------------------------------------------------------------------
/// VerifyVersionInfo APIを使いOSVERINFOEXの指定したDWORDの値を取得
///-------------------------------------------------------------------------------------------------
void	GetVerDword(OSVERSIONINFOEX* ver, DWORD* dwp, DWORD mask, LONGLONG cmask)
{
	if ((BYTE*)dwp == (BYTE*)&(ver->dwMinorVersion))
	{             //            マイナーバージョンの検索である
		cmask = 0;
		cmask = ::VerSetConditionMask(cmask, VER_MAJORVERSION, VER_EQUAL);
		cmask = ::VerSetConditionMask(cmask, VER_MINORVERSION, VER_EQUAL);

		for (int n = 0; n < 65536; n++)
		{
			*dwp = n;
			BOOL b = ::VerifyVersionInfo(ver, mask, cmask);
			if (b){ return; }
		}
		*dwp = 0xffffffff;
		return;
	}
	else
	{
		UINT sum = 0;
		int bit_max = 15;
		UINT bit_chk = 1 << bit_max;
		while (0 <= bit_max)
		{
			*dwp = sum | bit_chk;
			BOOL b = ::VerifyVersionInfo(ver, mask, cmask);
			if (b){ sum |= bit_chk; }
			bit_chk >>= 1;
			--bit_max;
		}
		*dwp = sum;
	}
}
void	GetVerDword(OSVERSIONINFOEX* ver, WORD* wp, DWORD mask, LONGLONG cmask)
{
	WORD sum = 0;
	s32 bit_max = 15;
	WORD bit_chk = 1 << bit_max;
	while (0 <= bit_max)
	{
		*wp = sum + bit_chk;
		BOOL b = ::VerifyVersionInfo(ver, mask, cmask);
		if (b)
		{
			sum += bit_chk;
		}
		bit_chk >>= 1;
		--bit_max;
	}
	*wp = sum;
}

///-------------------------------------------------------------------------------------------------
///	コンストラクタ・デストラクタ
CDeviceWin::CDeviceWin()
	: m_hInstance(NULL)
	, m_hWnd(NULL)
	, m_quit(false)
	, m_isResize(false)
	, m_isReposition(false)
	, m_isMinimized(false)
	, m_isMaximized(false)
	, m_mouseWheelVol(0)
{
}
CDeviceWin::~CDeviceWin()
{
}


///-------------------------------------------------------------------------------------------------
///	初期化開始
b8	CDeviceWin::InitBegin(MEM_HANDLE _memHdl)
{
	// 基底クラスたちの初期化
	if (CDevice::InitBegin(_memHdl) == false){ return false; }

	// ハンドル取得
	m_hInstance = ::GetModuleHandle(NULL);

	// OSバージョン取得
	{
		::OSVERSIONINFOEX ver;
		ZeroMemory(&ver, sizeof(ver));
		ver.dwOSVersionInfoSize = sizeof(ver);

		LONGLONG cmask = 0;
		DWORD mask;

		cmask = ::VerSetConditionMask(cmask, VER_MAJORVERSION, VER_GREATER_EQUAL);
		mask = VER_MAJORVERSION;
		GetVerDword(&ver, &ver.dwMajorVersion, mask, cmask);

		cmask = ::VerSetConditionMask(cmask, VER_MINORVERSION, VER_GREATER_EQUAL);
		mask |= VER_MINORVERSION;
		GetVerDword(&ver, &ver.dwMinorVersion, mask, cmask);

		cmask = ::VerSetConditionMask(cmask, VER_SERVICEPACKMAJOR, VER_GREATER_EQUAL);
		mask |= VER_SERVICEPACKMAJOR;
		GetVerDword(&ver, &ver.wServicePackMajor, mask, cmask);

		cmask = ::VerSetConditionMask(cmask, VER_BUILDNUMBER, VER_GREATER_EQUAL);
		mask |= VER_BUILDNUMBER;
		GetVerDword(&ver, &ver.dwBuildNumber, mask, cmask);

		PRINT("----------------------------------------------------------------------------\n");
		PRINT("OS Major Version:%d\n", ver.dwMajorVersion);
		PRINT("OS Minor Version:%d\n", ver.dwMinorVersion);
		PRINT("OS ServicePack Major Version:%d\n", ver.wServicePackMajor);
		PRINT("OS Build Number:%d\n", ver.dwBuildNumber);
		PRINT("----------------------------------------------------------------------------\n");
		PRINT("\n");
	}
	
	return (m_hInstance != NULL);
}

///-------------------------------------------------------------------------------------------------
///	初期化終了
b8	CDeviceWin::InitEnd()
{
	return ShowWin();
}

///-------------------------------------------------------------------------------------------------
///	実行確認
b8	CDeviceWin::IsRunning()
{
	// リセット
	{
		m_isResize = false;
		m_isReposition = false;
	}

	while (::PeekMessage(&m_hMsg, NULL, 0, 0, PM_REMOVE))
	{
		// 終了
		if (m_hMsg.message == WM_QUIT)
		{
			return false;
		}
		::TranslateMessage(&m_hMsg);
		::DispatchMessage(&m_hMsg);
	}

	// 最小化したのでメッセージ待ちする
	if (IsMinimize())
	{
		while (::GetMessage(&m_hMsg, NULL, 0, 0))
		{
			::TranslateMessage(&m_hMsg);
			::DispatchMessage(&m_hMsg);

			if (IsMinimize() == false)
			{
				break;
			}

			// 終了
			if (m_quit || m_hMsg.message == WM_QUIT)
			{
				return false;
			}
		}
	}

	return true;
}

///-------------------------------------------------------------------------------------------------
///	アプリケーションの終了処理
void	CDeviceWin::Finalize()
{
	// ウィンドウの破棄
	::DestroyWindow(m_hWnd);
	::UnregisterClass(APPLICATION_NAME, m_hInstance);
}


///---------------------------------------------------------------------------------------------------------------
///	ウィンドウの作成
b8	CDeviceWin::InitWin(const INIT_DESC& _desc)
{
	// ウィンドウクラス構造体
	::WNDCLASSEX WndClassEx;
	WndClassEx.cbSize = sizeof(::WNDCLASSEX);				// 構造体サイズ
	WndClassEx.style = CS_HREDRAW | CS_VREDRAW;				// スタイル
	WndClassEx.lpfnWndProc = InitWindowProc;				// グローバルプロシージャ
	WndClassEx.cbClsExtra = 0;								// 拡張情報１
	WndClassEx.cbWndExtra = 0;								// 拡張情報２
	WndClassEx.hInstance = m_hInstance;						// インスタンスハンドル
	WndClassEx.hIcon = _desc.icon;							// アイコン
	WndClassEx.hIconSm = NULL;								// 子アイコン
	WndClassEx.hCursor = _desc.cursor;						// マウスカーソル
	WndClassEx.hbrBackground = (::HBRUSH)(COLOR_WINDOW + 1);// ウィンドウ背景
	WndClassEx.lpszMenuName = NULL;							// ウィンドウメニュー名
	WndClassEx.lpszClassName = APPLICATION_NAME;			// ウィンドウクラス名
	// ウィンドウクラスの登録
	if (!::RegisterClassEx(&WndClassEx))
	{
		::MessageBox(NULL, "ウィンドウクラスの構造体の初期エラー", "", MB_OK);
		return false;
	}

	//ウィンドウの作成
	m_hWnd = ::CreateWindowEx(
		0,								// 拡張ウィンドウスタイル
		APPLICATION_NAME,				// ウィンドウクラス名
		_desc.title,					// ウィンドウ名
		_desc.style,					// ウィンドウスタイル
		(_desc.rect.posX < 0) ? CW_USEDEFAULT : _desc.rect.posX,	// スクリーンＸ座標位置
		(_desc.rect.posY < 0) ? CW_USEDEFAULT : _desc.rect.posY,	// スクリーンＹ座標位置
		_desc.rect.width,				// ウィンドウ幅
		_desc.rect.height,				// ウィンドウ高さ
		NULL,							// 親ウィンドウのハンドル
		NULL,							// メニューのハンドル、又は子ウィンドウのハンドル
		m_hInstance,					// インスタンスハンドル
		this							// 生成するウィンドウに渡す任意パラメータ（自分自身）
		);
	if (m_hWnd == NULL)
	{
		::MessageBox(NULL, "ウィンドウクラスの構造体の初期エラー", "", MB_OK);
		return false;
	}

	return true;
}

///-------------------------------------------------------------------------------------------------
///	最小ウィンドウになっているか
//b8	CDeviceWin::IsMinimize() const
//{
//	// ウィンドウの状態取得
//	::WINDOWPLACEMENT winPlacement;
//	::GetWindowPlacement(m_hWnd, &winPlacement);
//	return (winPlacement.showCmd == SW_MINIMIZE);
//}

///-------------------------------------------------------------------------------------------------
///	ウィンドウサイズを取得
void	CDeviceWin::GetWindowSize(u32& _width, u32& _height) const
{
	::RECT winSize;
	::GetWindowRect(m_hWnd, &winSize);		// ウィンドウ全体のサイズ
	_width = winSize.right - winSize.left;
	_height = winSize.bottom - winSize.top;
}

///-------------------------------------------------------------------------------------------------
///	ウィンドウRECTを取得
void	CDeviceWin::GetWindowRect(CRect& _rect) const
{
	::RECT winSize;
	::GetWindowRect(m_hWnd, &winSize);		// ウィンドウ全体のサイズ
	_rect.posX = winSize.left;
	_rect.posY = winSize.top;
	_rect.width = winSize.right - winSize.left;
	_rect.height = winSize.bottom - winSize.top;
}

///-------------------------------------------------------------------------------------------------
///	クライアントサイズを取得
void	CDeviceWin::GetClientSize(u32& _width, u32& _height) const
{
	::RECT clientSize;
	::GetClientRect(m_hWnd, &clientSize);	// クライアント領域のサイズ
	_width = clientSize.right - clientSize.left;
	_height = clientSize.bottom - clientSize.top;
}

///-------------------------------------------------------------------------------------------------
///	クライアントRECTを取得
void	CDeviceWin::GetClientRect(CRect& _rect) const
{
	::RECT clientSize;
	::GetClientRect(m_hWnd, &clientSize);	// クライアント領域のサイズ

	::POINT clientBgn;
	clientBgn.x = clientSize.left;
	clientBgn.y = clientSize.top;
	::ClientToScreen(m_hWnd, &clientBgn);
	::POINT clientEnd;
	clientEnd.x = clientSize.right;
	clientEnd.y = clientSize.bottom;
	::ClientToScreen(m_hWnd, &clientEnd);

	_rect.posX = SCast<s32>(clientBgn.x);
	_rect.posY = SCast<s32>(clientBgn.y);
	_rect.width = SCast<u32>(clientEnd.x - clientBgn.x);
	_rect.height = SCast<u32>(clientEnd.y - clientBgn.y);
}

///-------------------------------------------------------------------------------------------------
///	ウィンドウサイズを変更
void	CDeviceWin::UpdateWindowSize(u32 _width, u32 _height)
{
	::SetWindowPos(m_hWnd, NULL, 0, 0, _width, _height, (SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOSIZE));
}

///---------------------------------------------------------------------------------------------------------------
///	ウィンドウRECT変更
void	CDeviceWin::UpdateWindowRect(const CRect& _rect)
{
	::SetWindowPos(m_hWnd, NULL, _rect.posX, _rect.posY, _rect.width, _rect.height, (SWP_NOZORDER | SWP_NOOWNERZORDER));
}

///-------------------------------------------------------------------------------------------------
///	クライアントサイズを変更
void	CDeviceWin::UpdateClientSize(u32 _width, u32 _height)
{
	::RECT winSize, clientSize;
	::GetWindowRect(m_hWnd, &winSize);		// ウィンドウ全体のサイズ
	::GetClientRect(m_hWnd, &clientSize);	// クライアント領域のサイズ
	u32 width = (winSize.right - winSize.left) - (clientSize.right - clientSize.left) + _width;
	u32 height = (winSize.bottom - winSize.top) - (clientSize.bottom - clientSize.top) + _height;
	UpdateWindowSize(width, height);
}

///---------------------------------------------------------------------------------------------------------------
///	ウィンドウ描画
b8	CDeviceWin::ShowWin(s32 _showType)
{
	s32 check = 0;
	check = ::ShowWindow(m_hWnd, _showType);
	check = ::UpdateWindow(m_hWnd);
	return (check != 0);
}


//-------------------------------------------------------------仮想関数-------------------------------------------------------------S
///---------------------------------------------------------------------------------------------------------------
//	メインウィンドウプロシージャ関数
LRESULT CALLBACK CDeviceWin::WinProc(HWND _hWnd, u32 _msg, WPARAM _wParam, LPARAM _lParam)
{
	switch (_msg)
	{
	case WM_CREATE:
		if (_hWnd != NULL)
		{
			m_hWnd = _hWnd;
		}
		break;

	case WM_KEYDOWN:
		//GetKeyDown(_wParam, LOWORD(_lParam), HIWORD(_lParam));
		//	アプリ終了
		if (_wParam == VK_ESCAPE) {
			if (::MessageBox(_hWnd, "終了しますか？", "", MB_YESNO) == IDYES)
			{
				::DestroyWindow(_hWnd);	//ウィンドウを閉じる
			}
		}
		break;

	case WM_KEYUP:
		//GetKeyUp(_wParam, LOWORD(_lParam), HIWORD(_lParam));
		break;

	case WM_MOUSEWHEEL:
		m_mouseWheelVol += GET_WHEEL_DELTA_WPARAM(_wParam) / WHEEL_DELTA;
		if (m_mouseWheelVol > INT_MAX){ m_mouseWheelVol = INT_MAX; }
		if (m_mouseWheelVol < INT_MIN){ m_mouseWheelVol = INT_MIN; }
		break;

#if 1
	case WM_SIZE:
		m_isResize = true;
		m_isMinimized = (_wParam == SIZE_MINIMIZED);
		m_isMaximized = (_wParam == SIZE_MAXIMIZED);
		break;
	case WM_MOVE:
		m_isReposition = true;
		break;
#else
	case WM_SIZING:
		m_isResize = true;
		break;
	case WM_MOVING:
		m_isReposition = true;
		break;
#endif // 0

	case WM_CLOSE:
		//::DestroyWindow(_hWnd);	//ウィンドウを閉じる
		::PostMessage(_hWnd, WM_DESTROY, 0, 0);
		break;

	case WM_DESTROY:
		::PostQuitMessage(0);
		m_quit = true;
		break;

	default:
		return ::DefWindowProc(_hWnd, _msg, _wParam, _lParam);
	}

	return 0;
}
//-------------------------------------------------------------仮想関数-------------------------------------------------------------E

///---------------------------------------------------------------------------------------------------------------
///	ＩＮＩＴウィンドウプロシージャ関数
LRESULT CALLBACK	CDeviceWin::InitWindowProc(HWND _hWnd, u32 _msg, WPARAM _wParam, LPARAM _lParam)
{
	if (_msg == WM_CREATE)
	{
		CDeviceWin* pThis = (CDeviceWin*)((LPCREATESTRUCT)_lParam)->lpCreateParams;
		::SetWindowLongPtr(_hWnd, GWLP_USERDATA, (LONG_PTR)pThis);
		::SetWindowLongPtr(_hWnd, GWLP_WNDPROC, (LONG_PTR)GlobalWindowProc);

		return GlobalWindowProc(_hWnd, _msg, _wParam, _lParam);
	}

	return ::DefWindowProc(_hWnd, _msg, _wParam, _lParam);
}

///---------------------------------------------------------------------------------------------------------------
///	グローバルウィンドウプロシージャ関数
LRESULT CALLBACK	CDeviceWin::GlobalWindowProc(HWND _hWnd, u32 _msg, WPARAM _wParam, LPARAM _lParam)
{
	return ((CDeviceWin*)::GetWindowLongPtr(_hWnd, GWLP_USERDATA))->WinProc(_hWnd, _msg, _wParam, _lParam);
}

POISON_END

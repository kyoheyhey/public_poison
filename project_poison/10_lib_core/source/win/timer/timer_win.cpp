﻿#include "timer/timer.h"


POISON_BGN

///*************************************************************************************************
/// winタイマークラス
///*************************************************************************************************
CTimer::CTimer()
{
	memset(&m_param.m_freq, 0, sizeof(LARGE_INTEGER));
	memset(&m_param.m_start, 0, sizeof(LARGE_INTEGER));
	memset(&m_param.m_end, 0, sizeof(LARGE_INTEGER));
}
CTimer::~CTimer()
{
}

///-------------------------------------------------------------------------------------------------
/// 初期化
b8		CTimer::Initialize()
{
	return (QueryPerformanceFrequency(&m_param.m_freq) != 0);
}

///-------------------------------------------------------------------------------------------------
/// 終了
void		CTimer::Finalize()
{
}

///-------------------------------------------------------------------------------------------------
/// 計測開始
b8		CTimer::SetMeasure()
{
	return (QueryPerformanceCounter(&m_param.m_start) != 0);
}

///-------------------------------------------------------------------------------------------------
/// 時間取得
f64		CTimer::GetTime()
{
	if (QueryPerformanceCounter(&m_param.m_end) && m_param.m_freq.QuadPart)
	{
		return SCast<f64>(m_param.m_end.QuadPart - m_param.m_start.QuadPart) /
			SCast<f64>(m_param.m_freq.QuadPart);
	}
	else
	{
		return 0.0;
	}
}

///-------------------------------------------------------------------------------------------------
/// 経過時間取得
f64		CTimer::GetElapsedTime()
{
	if (QueryPerformanceCounter(&m_param.m_end))
	{
		return SCast<f64>(m_param.m_end.QuadPart) /
			SCast<f64>(m_param.m_freq.QuadPart);
	}
	else
	{
		return 0.0;
	}
}


///*************************************************************************************************
/// winタイムユーティリティクラス
///*************************************************************************************************

//-------------------------------------------------------------------------------------------------
/// 現在時刻取得
CTimeUtility::DateAndTime&		CTimeUtility::GetCurrentTime(DateAndTime& _time)
{
	SYSTEMTIME systemTime;
	GetSystemTime(&systemTime);

	_time.year = SCast<u32>(systemTime.wYear);
	_time.month = SCast<u32>(systemTime.wMonth);
	_time.week = SCast<WEEK>(systemTime.wDayOfWeek);
	_time.day = SCast<u32>(systemTime.wDay);
	_time.hour = SCast<u32>(systemTime.wHour);
	_time.minute = SCast<u32>(systemTime.wMinute);
	_time.second = SCast<u32>(systemTime.wSecond);
	_time.milliSecond = SCast<f32>(systemTime.wMilliseconds) * 0.001f;
	return _time;
}

POISON_END

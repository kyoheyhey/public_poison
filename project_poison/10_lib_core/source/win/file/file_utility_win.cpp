﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "file/file_utility.h"
#include "platform_def.h"

#include <io.h>
#include <stdlib.h>
#include <shobjidl.h>
#include <shellapi.h>


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant

/// ルートパス
static const c8*	s_pRootPath = NULL;
static size_t		s_uRootPathLen = 0;

/// 操作関数
static IFileOperate*	s_pFileOperate = NULL;

#ifndef POISON_RELEASE
// デバッグ時
static const c8*		BASE_PATH[2] = { "data/", "../../data/" };
static const size_t		BASE_PATH_LEN[2] = { strlen(BASE_PATH[0]), strlen(BASE_PATH[1]) };
#else // !POISON_RELEASE
// リリース時
static const c8*		BASE_PATH[1] = { "data/" };
static const size_t		BASE_PATH_LEN[1] = { strlen(BASE_PATH[0]) };
#endif // else !POISON_RELEASE

/// ルートパス保存領域
static c8		s_rootPath[256];


///-------------------------------------------------------------------------------------------------
/// ファイルシステム初期化
b8	CFileUtility::Initialize(FILE_OPERATE* _pFileOperateList, u32 _fileOperateNum)
{
#ifndef UNUSE_LOCK
	GetLock().Initialize();
#endif // !UNUSE_LOCK
	libAssert(_pFileOperateList);

	// 使用するファイル操作インスタンス
	s_pFileOperate = _pFileOperateList->pFileOperate;
	libAssert(s_pFileOperate);

	ChangeRootPath(_pFileOperateList->pBaseRootPath);

	return true;
}


///-------------------------------------------------------------------------------------------------
/// ファイルシステム後処理
void	CFileUtility::Finalize()
{
	s_pFileOperate = NULL;
#ifndef UNUSE_LOCK
	GetLock().Finalize();
#endif // !UNUSE_LOCK
}

#ifndef UNUSE_LOCK
///-------------------------------------------------------------------------------------------------
/// ロック取得
CAtomicLockReadWrite&	CFileUtility::GetLock()
{
	static CAtomicLockReadWrite s_lock;
	return s_lock;
}
#endif // !UNUSE_LOCK

///-------------------------------------------------------------------------------------------------
/// ChangeRootPath
b8	CFileUtility::ChangeRootPath(const c8* _pRootPath, s32 _index)
{
	// ルートのパス指定
	if (!_pRootPath)
	{
		// 相対パス
		s_pRootPath = BASE_PATH[0];
		s_uRootPathLen = BASE_PATH_LEN[0];

#ifndef POISON_RELEASE
		// ベースのパスがない
		if (::_access(BASE_PATH[0], 0) != 0)
		{
			// デバッグ実行時のパスならあった
			if (::_access(BASE_PATH[1], 0) == 0)
			{
				// 相対パス
				s_pRootPath = BASE_PATH[1];
				s_uRootPathLen = BASE_PATH_LEN[1];
			}
		}
#endif// !POISON_RELEASE
	}
	else
	{
		// 保持領域にコピーしてルートパスのアクセスポインタを設定
		strcpy_s(s_rootPath, _pRootPath);
		s_pRootPath = s_rootPath;
		s_uRootPathLen = strlen(s_pRootPath);
	}

	// ルートパスがない
	if (::_access(s_pRootPath, 0) != 0)
	{
		return false;
	}
	return true;
}

///-------------------------------------------------------------------------------------------------
/// GetRootPath
const c8*	CFileUtility::GetRootPath(s32 _index)
{
	return s_pRootPath;
}

///-------------------------------------------------------------------------------------------------
/// ファイルオープン
FILE_HANDLE	CFileUtility::Open(const c8* _path, const CFileUtility::MODE _mode, const b8 _binary, const bool _async, const FILE_ATTR _attr)
{
	libAssert(_path);
	libAssert(_mode < MODE_MAX_NUM);

	c8 filePath[FO_FILEPATH_MAX_LEN] = { 0 };
	GetFilePath(_path, filePath);
	_path = filePath;

#if 0 // fopen
	// ファイルオープンモード
	static const c8* s_modeStr[] =
	{
		"r", "w", "a", "r+", "w+", "a+"
	};
	StaticAssert(ARRAYOF(s_modeStr) == MODE_MAX_NUM);
	c8 mode[5] = { NULL };
	strcpy_safe(mode, sizeof(mode), s_modeStr[_mode]);
	size_t len = strlen(mode);
	mode[len] = _binary ? 'b' : 't';
	mode[len + 1] = 0;

	// ファイルオープン
	if (::fopen_s(&s_pFile, _path, mode) != 0)
	{
		PRINT_ERROR("*****[ERROR] CFileUtility::Open() ファイルなし[%s]\n", _path);
		return FILE_HANDLE_INVALID;
	}
	return RCast<FILE_HANDLE>(s_pFile);
#else

	u32 mode = 0;
	switch (_mode)
	{
	case POISON::CFileUtility::MODE_READ:
		mode = FO_OPEN_MODE_READ;
		break;
	case POISON::CFileUtility::MODE_WRITE:
		mode = FO_OPEN_MODE_WRITE | FO_OPEN_MODE_CREATE;
		break;
	case POISON::CFileUtility::MODE_ADD_WRITE:
		mode = FO_OPEN_MODE_APPEND;
		break;
	case POISON::CFileUtility::MODE_READ_WRITE:
		mode = FO_OPEN_MODE_READ | FO_OPEN_MODE_WRITE;
		break;
	case POISON::CFileUtility::MODE_READ_WRITE2:
		mode = FO_OPEN_MODE_READWRITE | FO_OPEN_MODE_CREATE;
		break;
	case POISON::CFileUtility::MODE_ADD_READ_WRITE:
		mode = FO_OPEN_MODE_READ | FO_OPEN_MODE_APPEND;
		break;
	default:
		break;
	}

#ifndef UNUSE_LOCK
	CFileUtility::GetLock().WLock();
#endif // !UNUSE_LOCK
	FoFileHn handle = s_pFileOperate->Open(_path, mode, _async, _attr);
#ifndef UNUSE_LOCK
	CFileUtility::GetLock().WUnlock();
#endif // !UNUSE_LOCK

	// エラーでなければ
	if (s_pFileOperate->IsValidFileHandle(handle))
	{
		return RCast<FILE_HANDLE>(handle);
	}
	return FILE_HANDLE_INVALID;
#endif
}

///-------------------------------------------------------------------------------------------------
/// ファイル読み込み
CFileUtility::STATUS	CFileUtility::Read(FILE_HANDLE& _handle, void* _buf, const u32 _size, FILE_ASYNC_DATA* _asyncData)
{
	libAssert(_handle != FILE_HANDLE_INVALID);
	libAssert(_buf && _size > 0);

	u32 read_size = 0, status = 0;

#ifndef UNUSE_LOCK
	CFileUtility::GetLock().RLock();
#endif // !UNUSE_LOCK
	b8 result = s_pFileOperate->Read(RCast<FoFileHn>(_handle), _buf, _size, &read_size, &status, _asyncData);
#ifndef UNUSE_LOCK
	CFileUtility::GetLock().RUnlock();
#endif // !UNUSE_LOCK

	if (!result)
	{
		return STATUS_FAILED;
	}

	return status == FO_FILE_STATUS_COMPLETE ? STATUS_READED : STATUS_READING;
}

///-------------------------------------------------------------------------------------------------
/// ファイル書き込み
CFileUtility::STATUS	CFileUtility::Write(FILE_HANDLE& _handle, const void* _buf, const u32 _size)
{
	libAssert(_handle != FILE_HANDLE_INVALID);
	libAssert(_buf && _size > 0);

	u32 write_size = 0, status = 0;
	if (!s_pFileOperate->Write(RCast<FoFileHn>(_handle), _buf, _size, &write_size, &status, NULL))
	{
		return FU_STATUS_FAILED;
	}

	return FU_STATUS_OK;
}


///-------------------------------------------------------------------------------------------------
/// ファイルクローズ
b8	CFileUtility::Close(FILE_HANDLE& _handle, FILE_ASYNC_DATA* _asyncData, bool _async)
{
	libAssert(_handle != FILE_HANDLE_INVALID);

#ifndef UNUSE_LOCK
	CFileUtility::GetLock().WLock();
#endif // !UNUSE_LOCK
	s_pFileOperate->Close(RCast<FoFileHn>(_handle));
#ifndef UNUSE_LOCK
	CFileUtility::GetLock().WUnlock();
#endif // !UNUSE_LOCK

	_handle = FILE_HANDLE_INVALID;

	return true;
}
///-------------------------------------------------------------------------------------------------
/// ディレクトリを作成する
b8	CFileUtility::MakeDirectory(const c8* _path, s32 _index)
{
	c8 filePath[FO_FILEPATH_MAX_LEN] = { 0 };
	GetFilePath(_path, filePath);
	_path = filePath;

	return s_pFileOperate->MakeDirectory(_path);
}

///-------------------------------------------------------------------------------------------------
/// ファイル存在チェック
b8	CFileUtility::IsExistFile(const c8* _path, s32 _index)
{
	c8 filePath[FO_FILEPATH_MAX_LEN] = { 0 };
	GetFilePath(_path, filePath);
	_path = filePath;

	return s_pFileOperate->IsFileExist(_path);
}

///-------------------------------------------------------------------------------------------------
/// ディレクトリが存在するか
b8	CFileUtility::IsExistDirectory(const c8* _path, s32 _index)
{
	c8 filePath[FO_FILEPATH_MAX_LEN] = { 0 };
	GetFilePath(_path, filePath);
	_path = filePath;

	return s_pFileOperate->IsDirectoryExist(_path);
}

///-------------------------------------------------------------------------------------------------
/// ファイルを削除する
b8	CFileUtility::DeleteFile(const c8* _path, s32 _index)
{
	c8 filePath[FO_FILEPATH_MAX_LEN] = { 0 };
	GetFilePath(_path, filePath);
	_path = filePath;

	return s_pFileOperate->DeleteFile(_path);
}

///-------------------------------------------------------------------------------------------------
/// ディレクトリを削除する
b8	CFileUtility::DeleteDirectory(const c8* _path, s32 _index)
{
	c8 filePath[FO_FILEPATH_MAX_LEN] = { 0 };
	GetFilePath(_path, filePath);
	_path = filePath;

	return s_pFileOperate->DeleteDirectory(_path);
}

///-------------------------------------------------------------------------------------------------
/// 指定フォルダ直下にあるフォルダを探す
u32	CFileUtility::GetDirListInDir(const c8* _path, const FileDirInfoConfig* _config, c8 _out[][FO_FILENAME_MAX_LEN], u32 _outMaxNum, s32 _index)
{
	c8 filePath[FO_FILEPATH_MAX_LEN] = { 0 };
	GetFilePath(_path, filePath);
	_path = filePath;

	return s_pFileOperate->GetDirs(_path, _config, _out, _outMaxNum);
}

///-------------------------------------------------------------------------------------------------
/// ファイルサイズ取得
CFileUtility::STATUS	CFileUtility::GetFileSize(FILE_HANDLE& _handle, s32* _size)
{
	libAssert(_handle != FILE_HANDLE_INVALID);

	// 4ギガ制限
	s32 size = 0;

#ifndef UNUSE_LOCK
	CFileUtility::GetLock().RLock();
#endif // !UNUSE_LOCK
	u32 status = s_pFileOperate->GetFileSize(RCast<FoFileHn>(_handle), &size);
#ifndef UNUSE_LOCK
	CFileUtility::GetLock().RUnlock();
#endif // !UNUSE_LOCK

	if (status == FO_FILE_STATUS_COMPLETE)
	{
		*_size = size;
		return FU_STATUS_OK;
	}
	else if (status == FO_FILE_STATUS_PROCESSING)
	{
		return FU_STATUS_PENDING;
	}

	return FU_STATUS_FAILED;
}


///-------------------------------------------------------------------------------------------------
/// ファイルバッファのアライメント
u32	CFileUtility::GetFileBufferAlign()
{
	return 16;
}

///-------------------------------------------------------------------------------------------------
/// フォルダパスからそれと同階層のファイルをワイルドカードを指定してゲットする
u32	CFileUtility::GetFileListInDir(const c8* _pPath, const c8* _pWild, c8 _ppOut[][128], u32 _outMaxNum, s32 _index)
{
	c8 pDir[256];
	//ベースパスを追加する
	strcpy_s(pDir, s_pRootPath);
	strcat_s(pDir, _pPath);

	return s_pFileOperate->GetFiles(pDir, _pWild, _ppOut, _outMaxNum);
}

///-------------------------------------------------------------------------------------------------
/// フォルダパスからそれと同階層の拡張子を指定したファイルをゲットする
u32	CFileUtility::GetFileListInDirExt(const c8* _pPath, const c8* _pExt, c8 _ppOut[][128], u32 _outMaxNum, s32 _index)
{
	//ワイルドカードを設定する
	c8 pDir[256] = "*.";
	if (_pExt != NULL)
	{
		strcat_s(pDir, _pExt);
	}
	else
	{
		strcat_s(pDir, "*");
	}

	return GetFileListInDir(_pPath, pDir, _ppOut, _outMaxNum);
}


///-------------------------------------------------------------------------------------------------
/// 絶対パスかどうか
static b8	IsAbsolutePath(const c8* _path)
{
	return _path != NULL && strlen(_path) > 2 && _path[1] == ':';
}


///-------------------------------------------------------------------------------------------------
/// 相対ファイルパス取得
void	CFileUtility::GetFilePath(const c8* _path, c8* _filePath, s32 _index, b8 _withSystemBasePath)
{
	//const size_t baseLen = s_uRootPathLen;
	const c8* pBasePath = s_pRootPath;

#ifndef POISON_RELEASE
	// 環境パスチェック
	c8 path_env[FO_FILEPATH_MAX_LEN] = { 0 };
	{
		if (::ExpandEnvironmentStrings(_path, path_env, FO_FILEPATH_MAX_LEN) == 0)
		{
			// 失敗した場合は単純にコピー
			strcpy_s(path_env, _path);
		}
		path_env[FO_FILEPATH_MAX_LEN - 1] = '\0';

		char* p = path_env;
		for (int i = 0; i < FO_FILEPATH_MAX_LEN && *p != '\0'; ++i, ++p)
		{
			if (*p == '%') { *p = '_'; }
		}

		_path = path_env;
	}
#endif // POISON_RELEASE

	c8 path_src[FO_FILEPATH_MAX_LEN] = { 0 };
	if (_path[1] == ':')
	{
		// 絶対パスなのでそのままコピー
		strcpy_s(path_src, _path);
	}
	else
	{
		// システムベースパス付与
		if (_withSystemBasePath && IsAbsolutePath(pBasePath) == false && s_pFileOperate->GetSystemBasePath())
		{
			const c8* p = s_pFileOperate->GetSystemBasePath();

			// 相対パスなのでベースのパスを追加してコピー
			strcpy_s(path_src, p);
			strcat_s(path_src, "/");
			strcat_s(path_src, pBasePath);
		}
		else
		{
			// 相対パスなのでベースのパスを追加してコピー
			strcpy_s(path_src, pBasePath);
		}

		strcat_s(path_src, _path);

	}

	// 文字列チェックしつつコピー、変換、、とりあえず簡単に
	{
		c8* p = _filePath;
		const c8* p2 = path_src;
		while ((*p2))
		{
			if ((*p2) == '#'){
				memcpy(p, GetPlatformName(), GetPlatformNameLen());
				p += GetPlatformNameLen();
			}
			else{
				(*p) = (*p2);
				++p;
			}
			++p2;
		}
		*p = '\0';
	}
}

///-------------------------------------------------------------------------------------------------
/// エラーコード取得
u32	CFileUtility::GetLastErrorCode(s32 _index)
{
	return s_pFileOperate->GetLastErrorCode();
}

///-------------------------------------------------------------------------------------------------
/// 更新　必要なもののみ
void	CFileUtility::Update()
{
#ifndef UNUSE_LOCK
	CFileUtility::GetLock().WLock();
#endif // !UNUSE_LOCK
	s_pFileOperate->Update();
#ifndef UNUSE_LOCK
	CFileUtility::GetLock().WUnlock();
#endif // !UNUSE_LOCK
}


POISON_END

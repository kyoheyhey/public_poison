﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "win/file/file_operate_win.h"
#include "file/file_def.h"
#include <io.h>


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant




///-------------------------------------------------------------------------------------------------
/// ファイルハンドルが有効化どうか
b8	CFileOperateWin::IsValidFileHandle(FoFileHn _hn)
{
	return _hn != INVALID_HANDLE_VALUE;
}

///-------------------------------------------------------------------------------------------------
/// ファイルが存在するか？
b8	CFileOperateWin::IsFileExist(const c8* _path)
{
	return ::_access(_path, 0) == 0;
}

///-------------------------------------------------------------------------------------------------
/// ディレクトリが存在するか？
b8	CFileOperateWin::IsDirectoryExist(const c8* _path)
{
	return ::_access(_path, 0) == 0;
}

///-------------------------------------------------------------------------------------------------
/// 読み込み専用か？
b8	CFileOperateWin::IsReadOnly() const
{
	return false;
}

///-------------------------------------------------------------------------------------------------
/// ファイルのオープン
FoFileHn	CFileOperateWin::Open(const c8* _path, u32 _mode, b8 _async, u32 _attr)
{
	// トライオープンはまだは対応していない
	m_error = 0;
	if (_attr & FILE_ATTR_TRY_OPEN)
	{
		m_error = MakeErrorCode(FILE_ERROR_TRY_OPEN_FAILED);
		return NULL;
	}

	DWORD mode = 0;
	DWORD shareMode = FILE_SHARE_READ;
	DWORD createDisp = OPEN_EXISTING;
	DWORD fileAttr = FILE_ATTRIBUTE_NORMAL;
	if ((_mode & FO_OPEN_MODE_READ) != 0)
	{
		mode |= GENERIC_READ;
		if (_async) {
			fileAttr = FILE_FLAG_OVERLAPPED;
		}
	}
	if ((_mode & FO_OPEN_MODE_WRITE) != 0)
	{
		mode |= GENERIC_WRITE;
	}
	if ((_mode & FO_OPEN_MODE_CREATE) != 0)
	{
		createDisp = CREATE_ALWAYS;
	}
	if ((_mode & FO_OPEN_MODE_CREATEIFNOTEXIST) != 0)
	{
		createDisp = OPEN_ALWAYS;
	}

	// ファイルオープン
	FoFileHn hn = RCast<FoFileHn>(::CreateFile(_path, mode, shareMode, NULL, createDisp, fileAttr, NULL));

	return hn;
}

///-------------------------------------------------------------------------------------------------
/// ファイルのクローズ
void	CFileOperateWin::Close(FoFileHn _hn, FILE_ASYNC_DATA* _asdata)
{
	if (_hn == INVALID_HANDLE_VALUE)
	{
		return;
	}

	const HANDLE handle = RCast<const HANDLE>(_hn);
	::CloseHandle(handle);
}

///-------------------------------------------------------------------------------------------------
/// ディレクトリを作成する
b8	CFileOperateWin::MakeDirectory(const c8* _path)
{
	BOOL result = ::CreateDirectory(_path, NULL);
	return result == TRUE;
}

///-------------------------------------------------------------------------------------------------
/// ファイルを削除する
b8	CFileOperateWin::DeleteFile(const c8* _path)
{
	return ::SetFileAttributes(_path, FILE_ATTRIBUTE_NORMAL) == TRUE && ::DeleteFileA(_path) == TRUE;
}

//-------------------------------------------------------------------------------------------------
/// ディレクトリを削除する
b8	CFileOperateWin::DeleteDirectory(const c8* _path)
{
	return ::RemoveDirectory(_path) == TRUE;
}

///-------------------------------------------------------------------------------------------------
/// ファイルへのデータ書き込み
b8	CFileOperateWin::Write(FoFileHn _hn, const void* _buf, const u32 _size, u32* _sizeWritten, u32* _status, FILE_ASYNC_DATA* _asdata)
{
	const HANDLE handle = RCast<const HANDLE>(_hn);
	DWORD writeSize = 0;
	if (!::WriteFile(handle, _buf, _size, &writeSize, NULL))
	{
		*_status = FO_FILE_STATUS_FAILED;
		return false;
	}

	if (_sizeWritten)
	{
		*_sizeWritten = writeSize;
	}

	*_status = FO_FILE_STATUS_COMPLETE;
	return true;
}

///-------------------------------------------------------------------------------------------------
/// ファイルからのデータ読み込み
b8	CFileOperateWin::Read(FoFileHn _hn, void* _buf, const u32 _size, u32* _sizeRead, u32* _status, FILE_ASYNC_DATA* _asdata)
{
	const HANDLE handle = RCast<const HANDLE>(_hn);
	OVERLAPPED* ol = _asdata ? RCast<OVERLAPPED*>(_asdata) : NULL;

	// ファイル読み込み
	DWORD readSize = 0;

	// 非同期データがある
	if (ol != NULL && (ol->Pointer != NULL || ol->InternalHigh != 0 || ol->Internal != 0))
	{
		// 非同期のファイルの状態チェック
		if (::GetOverlappedResult(handle, ol, &readSize, FALSE))
		{
			*_status = FO_FILE_STATUS_COMPLETE;
			return true;
		}

		*_status = FO_FILE_STATUS_PROCESSING;
		return true;
	}

	// 読み込み
	if (::ReadFile(handle, _buf, _size, &readSize, ol))
	{
		*_status = FO_FILE_STATUS_COMPLETE;
		return true;
	}

	// エラーチェック.
	// 読み込み途中であれば処理中の情報を返す
	DWORD error = ::GetLastError();
	if (error == ERROR_IO_PENDING)
	{
		*_status = FO_FILE_STATUS_PROCESSING;
		return true;
	}

	*_status = FO_FILE_STATUS_FAILED;
	return false;
}

///-------------------------------------------------------------------------------------------------
/// ファイルサイズの取得
u32	CFileOperateWin::GetFileSize(FoFileHn _hn, s32* _size)
{
	if (_hn == INVALID_HANDLE_VALUE || _size == NULL)
	{
		return FO_FILE_STATUS_FAILED;
	}

	const HANDLE handle = RCast<const HANDLE>(_hn);
	*_size = SCast<s32>(::GetFileSize(handle, NULL));

	return FO_FILE_STATUS_COMPLETE;
}

///-------------------------------------------------------------------------------------------------
/// 指定フォルダ直下にあるファイルを条件指定して探す
u32	CFileOperateWin::GetFiles(const c8* _path, const c8* _cond, c8 _out[][FO_FILENAME_MAX_LEN], u32 _outMaxNum)
{
	//ファイル名の長さ
	u32 fileLen = FO_FILENAME_MAX_LEN;
	// カウンタ変数
	u32 fileCount = 0;

	// 宣言
	WIN32_FIND_DATA fdFile;
	HANDLE hFind;

	//探すパス構築
	c8 pDir[256];
	strcpy_s(pDir, _path);
	if (_cond)
	{
		strcat_s(pDir, _cond);
	}
	else
	{
		strcat_s(pDir, "*.*");
	}

	//最初の一つを見つける
	hFind = ::FindFirstFile(pDir, &fdFile);

	c8* fileName = _out ? _out[0] : NULL;
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			if (fdFile.cFileName[0] != '.')
			{
				if ((fdFile.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY)
				{
					//配列を渡さない場合はファイル数だけカウントする
					if (_out != NULL)
					{
						//ファイル数が足りない場合終了する
						if (fileCount >= _outMaxNum)
						{
							break;
						}
						size_t length = strlen(fdFile.cFileName);
						//ファイル名の長さが大きい場合は追加せずに次のファイルを読む
						if (length >= fileLen)
						{
							libAssert(length < fileLen);
							continue;
						}
						strcpy_s(fileName, length, fdFile.cFileName);
						fileName += fileLen;
					}
					fileCount++;
				}
			}
		} while (::FindNextFile(hFind, &fdFile));
		::FindClose(hFind);
	}
	return fileCount;
}

///-------------------------------------------------------------------------------------------------
/// 指定フォルダ直下にあるフォルダを探す
u32	CFileOperateWin::GetDirs(const c8* _path, const FileDirInfoConfig* _config, c8 _out[][FO_FILENAME_MAX_LEN], u32 _outMaxNum)
{
	WIN32_FIND_DATA data;
	HANDLE hn;
	c8 tmp[FO_FILEPATH_MAX_LEN] = { 0 };

	strcpy_s(tmp, _path);
	c8 last = tmp[strlen(tmp)];
	if (last != '\\' && last != '/')
	{
		strcat_s(tmp, "\\");
	}
	strcat_s(tmp, "*");
	u32 count = 0;

	hn = ::FindFirstFile(tmp, &data);
	while (hn != INVALID_HANDLE_VALUE)
	{
		if ((data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0)
		{
			if (_out)
			{
				strcpy_safe(_out[count], FO_FILENAME_MAX_LEN, data.cFileName);
			}

			++count;

			if (_outMaxNum > 0 && count >= _outMaxNum) { break; }
		}

		if (!::FindNextFile(hn, &data))
		{
			break;
		}
	}

	return count;
}


POISON_END

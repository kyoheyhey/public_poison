﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "thread/thread.h"


POISON_BGN

///-------------------------------------------------------------------------------------------------
/// constant


///-------------------------------------------------------------------------------------------------
/// 実行中かどうか
b8			CThread::IsRunning() const
{
	return CThreadUtility::IsRunning(m_handle);
}

///-------------------------------------------------------------------------------------------------
/// 生成
b8			CThread::Create(THREAD_FUNC* _func, void* _arg, const u32 _size, const s32 _prio, const c8* _pName)
{
	if (CThreadUtility::Create(m_handle, _func, _arg, _size, _prio, _pName))
	{
		strcpy_s(m_name, _pName);
		m_threadId = CThreadUtility::GetThreadID(m_handle);
		return true;
	}
	CThreadUtility::Delete(m_handle);
	return false;
}

///-------------------------------------------------------------------------------------------------
/// 削除
void		CThread::Delete()
{
	CThreadUtility::Delete(m_handle, &m_result);
	memset(this, 0, sizeof(CThread));
}

///-------------------------------------------------------------------------------------------------
/// 待機
THREAD_RESULT	CThread::Wait()
{
	THREAD_RESULT result = CThreadUtility::Wait(m_handle);
	if (result != THREAD_INVALID_RESULT)
	{
		m_result = result;
	}
	return m_result;
}


POISON_END

﻿
///-------------------------------------------------------------------------------------------------
/// define
#define	PAD_MAX_NUM	8	// パッド最大数


///-------------------------------------------------------------------------------------------------
/// include
#include "input/input_utility.h"
#include "input/keyboard.h"
#include "input/mouse.h"
#include "input/pad.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant
static CKeyboard*	s_pKeyboard = NULL;
static CMouse*		s_pMouse = NULL;
static CPad*		s_pPad[PAD_MAX_NUM] = { NULL };
static u32			s_padNum = 0;


///-------------------------------------------------------------------------------------------------
/// 取得
const CKeyboard*	CInputUtility::GetKeyboard()
{
	return s_pKeyboard;
}
const CMouse*		CInputUtility::GetMouse()
{
	return s_pMouse;
}
const CPad*			CInputUtility::GetPad(u32 _idx)
{
	libAssert(_idx < s_padNum);
	return s_pPad[_idx];
}

///-------------------------------------------------------------------------------------------------
/// セットアップ
void	CInputUtility::Setup(CKeyboard* _pKeyboard)
{
	s_pKeyboard = _pKeyboard;
}
void	CInputUtility::Setup(CMouse* _pMouse)
{
	s_pMouse = _pMouse;
}
void	CInputUtility::Setup(CPad* _pPad[], const u32 _num)
{
	libAssert(_num <= PAD_MAX_NUM);
	for (u32 idx = 0; idx < _num; idx++)
	{
		s_pPad[idx] = _pPad[idx];
	}
	s_padNum = _num;
}

///-------------------------------------------------------------------------------------------------
/// 終了
void	CInputUtility::Finalize()
{
	s_pKeyboard = NULL;
	s_pMouse = NULL;
	memset(s_pPad, 0, sizeof(s_pPad));
	s_padNum = 0;
}

///-------------------------------------------------------------------------------------------------
/// 更新
void	CInputUtility::Step()
{
	if (s_pKeyboard)
	{
		s_pKeyboard->Step();
	}
	if (s_pMouse)
	{
		s_pMouse->Step();
	}
	for (u32 idx = 0; idx < s_padNum; idx++)
	{
		s_pPad[idx]->Step();
	}
}


POISON_END

﻿
#define _USE_MATH_DEFINES
#include <math.h>
#include "math/math.h"


POISON_BGN

//========================================
// 線形補間
// 引数：	vAからvBまでvT(0～1)だけ線形補間
// 戻り値：	float
f32	Math::Lerp(f32 _vA, f32 _vB, f32 _t)
{
	return (_vB - _vA) * _t + _vA;
}

//========================================
// エルミート補間
// 引数	
// 第１：始点
// 第２：終点
// 第３：始点ベクトル
// 第４：終点ベクトル
// 第５：補間係数
// 戻り値：	float
f32	Math::Hermite(f32 _pA, f32 _pB, f32 _vA, f32 _vB, f32 _t)
{
	return
		((_t - 1.0f) * (_t - 1.0f) * ((2.0f * _t) - 1.0f) * _pA) +
		((_t * _t) * (3.0f - (2.0f * _t)) * _pB) +
		((1.0f - _t) * (1.0f - _t) * _t * _vA) +
		((_t - 1.0f) * (_t * _t) * _vB);
}

//========================================
// 余弦定理
// 引数：	a,b,cは辺の長さ、A,B,Cはcos値のポインタ引数
// 戻り値：	a,b,cで３角形が成り立つかどうか
b8	Math::LawOfCos(f32 _a, f32 _b, f32 _c, f32& _A, f32& _B, f32& _C)
{
	if (_a > (_b + _c)) return false;
	if (_b > (_c + _a)) return false;
	if (_c > (_a + _b)) return false;

	_A = (powf(_b, 2) + powf(_c, 2) - powf(_a, 2)) / (2 * _b * _c);
	_B = (powf(_c, 2) + powf(_a, 2) - powf(_b, 2)) / (2 * _c * _a);
	_C = (powf(_a, 2) + powf(_b, 2) - powf(_c, 2)) / (2 * _a * _b);
	return true;
}

//========================================
// 度単位->ラジアン単位変換
// 引数：	度単位で計測された角度
// 戻り値：	ラジアン単位
f32	Math::ToRadians(f32 _degree)
{
#define TO_RADIANS	0.01745329251f
	return TO_RADIANS * _degree;
}

//========================================
// ラジアン単位変換->度単位
// 引数：	ラジアン単位
// 戻り値：	度単位で計測された角度
f32	Math::ToDegrees(f32 _radian)
{
#define TO_DEGREES	57.2957795131f
	return TO_DEGREES * _radian;
}


POISON_END

﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "collection/idx_list.h"


POISON_BGN


CBaseIdxList::CBaseIdxList()
{
}

CBaseIdxList::~CBaseIdxList()
{
}

///-------------------------------------------------------------------------------------------------
///@brief	初期化
b8	CBaseIdxList::Initialize(IAllocator* _pAllocator, u16 _num)
{
	libAssert(_num > 0);
	m_pAllocator = _pAllocator;
	m_maxNum = _num;
	m_useNum = 0;
	m_pDataBuf = MEM_MALLOC(m_pAllocator, GetDataSize() * m_maxNum);
	libAssert(m_pDataBuf);
	m_pItrBuf = MEM_MALLOC_TYPES(m_pAllocator, ITR, m_maxNum);
	libAssert(m_pItrBuf);
	memset(m_pItrBuf, IDX_INVALID, sizeof(ITR) * m_maxNum);
	m_pFreeIdxBuf = MEM_MALLOC_TYPES(m_pAllocator, u16, m_maxNum);
	libAssert(m_pFreeIdxBuf);
	for (u16 idx = 0; idx < m_maxNum; idx++)
	{
		m_pFreeIdxBuf[idx] = idx;
	}
	m_pBgnItr = NULL;
	m_pEndItr = NULL;
	return (m_pDataBuf && m_pItrBuf);
}
b8	CBaseIdxList::Initialize(MEM_HANDLE _memHdl, u16 _num)
{
	return Initialize(MEM_ALLOCATOR(_memHdl), _num);
}

///-------------------------------------------------------------------------------------------------
///@brief	終了
void	CBaseIdxList::Finalize()
{
	m_maxNum = 0;
	m_useNum = 0;
	MEM_SAFE_FREE(m_pAllocator, m_pDataBuf);
	MEM_SAFE_FREE(m_pAllocator, m_pItrBuf);
	MEM_SAFE_FREE(m_pAllocator, m_pFreeIdxBuf);
	m_pAllocator = NULL;
	m_pBgnItr = NULL;
	m_pEndItr = NULL;
}

///-------------------------------------------------------------------------------------------------
///@brief	クリア
void	CBaseIdxList::Clear()
{
	m_useNum = 0;
	memset(m_pDataBuf, 0, GetDataSize() * m_maxNum);
	memset(m_pItrBuf, IDX_INVALID, sizeof(ITR) * m_maxNum);
	for (u16 idx = 0; idx < m_maxNum; idx++)
	{
		m_pFreeIdxBuf[idx] = idx;
	}
	m_pBgnItr = NULL;
	m_pEndItr = NULL;
}

///-------------------------------------------------------------------------------------------------
///@brief	イテレータスワップ
void	CBaseIdxList::Swap(ITR* _pItrA, ITR* _pItrB)
{
	if (CheckItr(_pItrA) == false || CheckItr(_pItrB) == false) { return; }

	ITR* prevA = GetPrev(_pItrA);
	ITR* nextA = GetNext(_pItrA);
	ITR* prevB = GetPrev(_pItrB);
	ITR* nextB = GetNext(_pItrB);
	if (prevA)
	{
		ConnectItr(prevA, _pItrB);
	}
	else
	{
		_pItrB->prevIdx = IDX_INVALID;
		m_pBgnItr = _pItrB;
	}
	if (nextA)
	{
		ConnectItr(_pItrB, nextA);
	}
	else
	{
		_pItrB->nextIdx = IDX_INVALID;
		m_pEndItr = _pItrB;
	}
	if (prevB)
	{
		ConnectItr(prevB, _pItrA);
	}
	else
	{
		_pItrA->prevIdx = IDX_INVALID;
		m_pBgnItr = _pItrA;
	}
	if (nextB)
	{
		ConnectItr(_pItrA, nextB);
	}
	else
	{
		_pItrA->nextIdx = IDX_INVALID;
		m_pEndItr = _pItrA;
	}
}

///-------------------------------------------------------------------------------------------------
///@brief	追加
CBaseIdxList::ITR*	CBaseIdxList::PushBack()
{
	// フリーインデックス取得
	u16 newIdx = PushFreeIdx();
	if (newIdx == IDX_INVALID){ return NULL; }

	// イテレータ生成
	ITR* pItr = GetItr(newIdx);
	libAssert(pItr);
	SetupItr(pItr, newIdx);

	// １より上
	if (m_useNum > 1)
	{
		// 接続情報設定
		ConnectItr(m_pEndItr, pItr);
		// 末尾交代
		m_pEndItr = pItr;
	}
	// １の時
	else
	{
		m_pBgnItr = pItr;
		m_pEndItr = pItr;
	}
	return pItr;
}

CBaseIdxList::ITR*	CBaseIdxList::PushFront()
{
	// フリーインデックス取得
	u16 newIdx = PushFreeIdx();
	if (newIdx == IDX_INVALID){ return NULL; }

	// イテレータ生成
	ITR* pItr = GetItr(newIdx);
	SetupItr(pItr, newIdx);

	// １より上
	if (m_useNum > 1)
	{
		// 接続情報設定
		ConnectItr(pItr, m_pBgnItr);
		// 先頭交代
		m_pBgnItr = pItr;
	}
	// １の時
	else
	{
		m_pBgnItr = pItr;
		m_pEndItr = pItr;
	}
	return pItr;
}


///-------------------------------------------------------------------------------------------------
///@brief	削除
b8	CBaseIdxList::PopBack()
{
	if (m_useNum <= 0) { return false; }

	// フリーインデックス追加
	ITR* pDelItr = m_pEndItr;
	PopFreeIdx(GetIdx(pDelItr));

	// 末尾の手前イテレータ設定
	ITR* pItr = GetPrev(pDelItr);
	if (pItr)
	{
		// 末尾交代
		m_pEndItr = pItr;
		m_pEndItr->nextIdx = IDX_INVALID;
	}
	else
	{
		// なくなったのでリセット
		m_pBgnItr = NULL;
		m_pEndItr = NULL;
	}
#ifndef POISON_RELEASE
	pDelItr->Clear();
#endif // !POISON_RELEASE
	return true;
}

b8	CBaseIdxList::PopFront()
{
	if (m_useNum <= 0) { return false; }

	// フリーインデックス追加
	ITR* pDelItr = m_pBgnItr;
	PopFreeIdx(GetIdx(pDelItr));

	// 先頭の後方イテレータ設定
	ITR* pItr = GetNext(pDelItr);
	if (pItr)
	{
		// 末尾交代
		m_pBgnItr = pItr;
		m_pBgnItr->prevIdx = IDX_INVALID;
	}
	else
	{
		// なくなったのでリセット
		m_pBgnItr = NULL;
		m_pEndItr = NULL;
	}
#ifndef POISON_RELEASE
	pDelItr->Clear();
#endif // !POISON_RELEASE
	return true;
}


///-------------------------------------------------------------------------------------------------
///@brief	途中追加
CBaseIdxList::ITR*	CBaseIdxList::InsertBack(ITR* _pItr)
{
	if (CheckItr(_pItr) == false) { return false; }

	// フリーインデックス取得
	u16 newIdx = PushFreeIdx();
	if (newIdx == IDX_INVALID){ return NULL; }

	// イテレータ生成
	ITR* pItr = GetItr(newIdx);
	SetupItr(pItr, newIdx);

	// ２より上
	if (m_useNum > 2)
	{
		// 接続情報設定
		if (_pItr == m_pEndItr)
		{
			ConnectItr(_pItr, pItr);
			// 末尾交代
			m_pEndItr = pItr;
		}
		else
		{
			ConnectItr(_pItr, pItr);
			ConnectItr(pItr, GetNext(_pItr));
		}
	}
	// ２以下のとき
	else
	{
		ConnectItr(_pItr, pItr);
		// 末尾交代
		m_pEndItr = pItr;
	}
	return pItr;
}

CBaseIdxList::ITR*	CBaseIdxList::InsertFront(ITR* _pItr)
{
	if (CheckItr(_pItr) == false) { return false; }

	// フリーインデックス取得
	u16 newIdx = PushFreeIdx();
	if (newIdx == IDX_INVALID){ return NULL; }

	// イテレータ生成
	ITR* pItr = GetItr(newIdx);
	SetupItr(pItr, newIdx);

	// ２より上
	if (m_useNum > 2)
	{
		// 接続情報設定
		if (_pItr == m_pBgnItr)
		{
			ConnectItr(pItr, _pItr);
			// 先頭交代
			m_pBgnItr = pItr;
		}
		else
		{
			ConnectItr(GetPrev(_pItr), pItr);
			ConnectItr(pItr, _pItr);
		}
	}
	// ２以下のとき
	else
	{
		ConnectItr(pItr, _pItr);
		// 先頭交代
		m_pBgnItr = pItr;
	}
	return pItr;
}


///-------------------------------------------------------------------------------------------------
///@brief	途中削除
b8	CBaseIdxList::Erase(ITR* _pItr)
{
	if (m_useNum <= 0) { return false; }
	if (CheckItr(_pItr) == false){ return false; }

	// フリーインデックス追加
	PopFreeIdx(GetIdx(_pItr));

	// １以上
	if (m_useNum >= 1)
	{
		// 接続情報設定
		if (GetPrev(_pItr) == NULL)
		{
			m_pBgnItr = GetNext(_pItr);
			m_pBgnItr->prevIdx = IDX_INVALID;
		}
		else if (GetNext(_pItr) == NULL)
		{
			m_pEndItr = GetPrev(_pItr);
			m_pEndItr->nextIdx = IDX_INVALID;
		}
		else
		{
			ConnectItr(GetPrev(_pItr), GetNext(_pItr));
		}
	}
	// ０のとき
	else
	{
		// なくなったのでリセット
		m_pBgnItr = NULL;
		m_pEndItr = NULL;
	}
#ifndef POISON_RELEASE
	_pItr->Clear();
#endif // !POISON_RELEASE
	return true;
}


///-------------------------------------------------------------------------------------------------
///@brief	データコピー
void	CBaseIdxList::CopyData(const CBaseIdxList& _idxList)
{
	memcpy(m_pDataBuf, _idxList.m_pDataBuf, GetDataSize() * m_maxNum);
}

///-------------------------------------------------------------------------------------------------
///@brief	イテレータコピー
void	CBaseIdxList::CopyItr(const CBaseIdxList& _idxList)
{
	memcpy(m_pItrBuf, _idxList.m_pBgnItr, sizeof(ITR) * m_maxNum);
}

///-------------------------------------------------------------------------------------------------
///@brief	フリーインデックスコピー
void	CBaseIdxList::CopyFreeIdx(const CBaseIdxList& _idxList)
{
	memcpy(m_pFreeIdxBuf, _idxList.m_pFreeIdxBuf, sizeof(u16) * m_maxNum);
}


POISON_END

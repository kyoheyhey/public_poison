﻿#include "memory/allocator_utility.h"

#include "memory/allocator.h"

POISON_BGN

///*************************************************************************************************
///	アロケータユーティリティクラス
///*************************************************************************************************
IAllocator**	CAllocatorUtility::s_allocatorList = NULL;
u32				CAllocatorUtility::s_allocatorNum = 0;

///-------------------------------------------------------------------------------------------------
///	セットアップ
void	CAllocatorUtility::Initialize(IAllocator* _ppAllocator[], u32 _num)
{
	s_allocatorList = _ppAllocator;
	s_allocatorNum = _num;
}

///-------------------------------------------------------------------------------------------------
///	生成
void*	CAllocatorUtility::Malloc(MEM_HANDLE _hdl, const MemoryMallocParam& _param)
{
	libAssert(_hdl != MEM_HDL_INVALID && _hdl < s_allocatorNum);
	IAllocator* pAllocator = *(s_allocatorList + _hdl);
	return pAllocator->Malloc(_param);
}

///-------------------------------------------------------------------------------------------------
///	破棄
u32		CAllocatorUtility::Free(MEM_HANDLE _hdl, void* _ptr)
{
	libAssert(_hdl != MEM_HDL_INVALID && _hdl < s_allocatorNum);
	IAllocator* pAllocator = *(s_allocatorList + _hdl);
	return pAllocator->Free(_ptr);
}

///-------------------------------------------------------------------------------------------------
///	スタック開始
void*	CAllocatorUtility::StackBegin(MEM_HANDLE _hdl, const MemoryStackBeginParam& _param)
{
	libAssert(_hdl != MEM_HDL_INVALID && _hdl < s_allocatorNum);
	IAllocator* pAllocator = *(s_allocatorList + _hdl);
	return pAllocator->StackBegin(_param);
}

///-------------------------------------------------------------------------------------------------
///	スタック終了
u32		CAllocatorUtility::StackEnd(MEM_HANDLE _hdl)
{
	libAssert(_hdl != MEM_HDL_INVALID && _hdl < s_allocatorNum);
	IAllocator* pAllocator = *(s_allocatorList + _hdl);
	return pAllocator->StackEnd();
}

///-------------------------------------------------------------------------------------------------
///	アロケータ取得
IAllocator*		CAllocatorUtility::GetAllocator(MEM_HANDLE _hdl)
{
	return *(s_allocatorList + _hdl);
}

POISON_END

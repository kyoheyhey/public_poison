
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
// include
#include "memory/boundary_tag_allocator.h"


///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


POISON_BGN

///-------------------------------------------------------------------------------------------------
/// コンストラクタ・デストラクタ
CBoundaryTagAllocatorBase::CBoundaryTagAllocatorBase()
{
}
CBoundaryTagAllocatorBase::~CBoundaryTagAllocatorBase()
{
}


///-------------------------------------------------------------------------------------------------
///	初期化
b8		CBoundaryTagAllocatorBase::Initialize(void* _pBuf, u32 _size)
{
	if (_size <= HEADER_SIZE + FLAG_SIZE + OFFSET_SIZE + FOOTER_SIZE) { return false; }

	m_pBuf = _pBuf;
	m_size = _size;

	m_uEndHeaderIdx = 0;

	m_uUsedSize = 0;
	m_uNowCount = 0;

	u32& header = GetHeader(0);
	header = GetSize();	// ヘッダに最大サイズ設定

	b8& flag = GetUseFlag(0);
	flag = false;		// フリー設定

	u32& footer = GetFooter(0);
	footer = 0;			// ヘッダインデックス設定
	return true;
}


///-------------------------------------------------------------------------------------------------
///	終了処理
void	CBoundaryTagAllocatorBase::Finalize()
{
	Assert(m_uUsedSize <= 0 && m_uNowCount <= 0);
	new(this) CBoundaryTagAllocatorBase;
}


///-------------------------------------------------------------------------------------------------
///	メモリ確保
void*   CBoundaryTagAllocatorBase::Malloc(const MemoryMallocParam& _param)
{
#if 1
	// 終端ヘッダーからチェック
	if (m_uEndHeaderIdx < GetSize() && GetFreeBufferSize(m_uEndHeaderIdx, _param.align) >= _param.size)
	{
		u32 bufferIdx = DivisionBlock(m_uEndHeaderIdx, _param.size, _param.align);
		// サイズ取得
		u32 bufSize = (GetHeader(m_uEndHeaderIdx) - bufferIdx) - FOOTER_SIZE;

		// 終端ヘッダーインデックス設定
		while (m_uEndHeaderIdx < GetSize())
		{
			if (!GetUseFlag(m_uEndHeaderIdx))
			{
				break;
			}
			m_uEndHeaderIdx = GetNextHeaderIdx(m_uEndHeaderIdx);
		}

		// ポインタ出荷！
		m_uUsedSize += bufSize;
		++m_uNowCount;
		return GetBuffer(bufferIdx);
	}
#endif // 0

	// 先頭からチェック
	u32 searchIdx = 0;
	while (searchIdx < GetSize())
	{
		if (!GetUseFlag(searchIdx) && GetFreeBufferSize(searchIdx, _param.align) >= _param.size)
		{
			u32 bufferIdx = DivisionBlock(searchIdx, _param.size, _param.align);
			// サイズ取得
			u32 bufSize = (GetHeader(searchIdx) - bufferIdx) - FOOTER_SIZE;

			// ポインタ出荷！
			m_uUsedSize += bufSize;
			++m_uNowCount;
			return GetBuffer(bufferIdx);
		}

		searchIdx = GetNextHeaderIdx(searchIdx);
	}

	return NULL;
}


///-------------------------------------------------------------------------------------------------
/// メモリ破棄
u32		CBoundaryTagAllocatorBase::Free(void* _ptr)
{
	// このアロケータのメモリかどうかチェック
	libAssert(!((u64)_ptr <= (u64)GetBuffer(0) || (u64)GetBuffer(GetSize()) <= (u64)_ptr));

	u32 bufferIdx = GetBufferIdx(_ptr);
	libAssert(bufferIdx >= GetOffset(bufferIdx));
	u32 headerIdx = bufferIdx - GetOffset(bufferIdx);

	// サイズ取得
	u32 bufSize = (GetHeader(headerIdx) - bufferIdx) - FOOTER_SIZE;

	// ブロックマージ
	u32 freeHeaderIdx = MergeBlock(headerIdx);

	// 終端ヘッダーインデックス更新
	if (GetHeader(freeHeaderIdx) >= m_uEndHeaderIdx || m_uEndHeaderIdx >= GetSize())
	{
		m_uEndHeaderIdx = freeHeaderIdx;
	}

	// 使用情報更新
	m_uUsedSize -= bufSize;
	--m_uNowCount;
	return bufSize;
}


///-------------------------------------------------------------------------------------------------
///	スタック開始
void*	CBoundaryTagAllocatorBase::StackBegin(const MemoryStackBeginParam& _param)
{
	return NULL;
}

///-------------------------------------------------------------------------------------------------
///	親メモリーを指定してスタック開始
b8		CBoundaryTagAllocatorBase::StackParentBegin(void* _parent, const MemoryStackBeginParam& _param)
{
	return false;
}

///-------------------------------------------------------------------------------------------------
///	スタック終了
u32		CBoundaryTagAllocatorBase::StackEnd()
{
	return 0;
}


///-------------------------------------------------------------------------------------------------
///	メモリの全体容量取得
u32		CBoundaryTagAllocatorBase::GetTotalSize() const
{
	return GetSize();
}
///	メモリの使用中容量取得
u32		CBoundaryTagAllocatorBase::GetUsedSize() const
{
	return m_uUsedSize;
}
///	メモリの取得数取得
u32		CBoundaryTagAllocatorBase::GetUsedCount() const
{
	return m_uNowCount;
}
///	メモリの全体の空き容量取得
u32		CBoundaryTagAllocatorBase::GetTotalRemainSize() const
{
	return GetSize() - m_uUsedSize;
}
///	メモリの一番大きい連続領域の空き容量取得
u32		CBoundaryTagAllocatorBase::GetMaxRemainSize(const u32 _align) const
{
	u32 maxFreeSize = 0;
	u32 idx = 0;
	while (idx < GetSize())
	{
		if (!GetUseFlag(idx))
		{
			maxFreeSize = max(maxFreeSize, GetFreeBufferSize(idx, _align));
		}
		idx = GetNextHeaderIdx(idx);
	}
	return maxFreeSize;
}


///-------------------------------------------------------------------------------------------------
///	現在のスタックサイズ取得
u32		CBoundaryTagAllocatorBase::GetStackTotalSize() const
{
	return 0;
}
/// 現在のスタックの使用サイズ
u32		CBoundaryTagAllocatorBase::GetStackUsedSize() const
{
	return 0;
}
/// 現在のスタックの取得数取得
u32		CBoundaryTagAllocatorBase::GetStackUsedCount() const
{
	return 0;
}


///-------------------------------------------------------------------------------------------------
/// ブロック分割
u32		CBoundaryTagAllocatorBase::DivisionBlock(u32 _headerIdx, size_t _size, u32 _align)
{
	// バッファインデックス
	u32 bufferIdx = GetAlignIdx(_headerIdx, _align);
	libAssert(bufferIdx >= _headerIdx + HEADER_SIZE + FLAG_SIZE + OFFSET_SIZE);

	// オフセット設定
	u8& offset = GetOffset(bufferIdx);
	offset = SCast<u8>(bufferIdx - _headerIdx);

	// ブロック全体サイズ
	u32 blockSize = offset + SCast<u32>(_size) + FOOTER_SIZE;
	u32 nextBlockSize = (GetHeader(_headerIdx) - _headerIdx) - blockSize;

	// 後方ブロックが最小サイズで確保できるサイズだった場合分割
	if (nextBlockSize > HEADER_SIZE + FLAG_SIZE + OFFSET_SIZE + FOOTER_SIZE)
	{
		// ヘッダー設定
		u32 header = GetHeader(_headerIdx);
		GetHeader(_headerIdx) = _headerIdx + blockSize;

		// フッダー設定
		GetFooter(_headerIdx) = _headerIdx;

		// 後方ヘッダー設定
		u32 nextHeaderIdx = GetNextHeaderIdx(_headerIdx);
		GetHeader(nextHeaderIdx) = header;

		// 後方フラグ設定
		GetUseFlag(nextHeaderIdx) = false;

		// 後方フッダー取得
		GetFooter(nextHeaderIdx) = nextHeaderIdx;
	}

	// フラグ設定
	GetUseFlag(_headerIdx) = true;

	return bufferIdx;
}

///-------------------------------------------------------------------------------------------------
///	ブロックマージ
u32		CBoundaryTagAllocatorBase::MergeBlock(u32 _headerIdx)
{
	// フラグ設定
	GetUseFlag(_headerIdx) = false;

	// 前方・後方フラグ取得
	b8 prevFlag = true;
	u32 prevIdx = 0;
	if (_headerIdx > 0)
	{
		prevIdx = GetPrevHeaderIdx(_headerIdx);
		if (prevIdx >= 0)
		{
			prevFlag = GetUseFlag(prevIdx);
		}
	}
	b8 nextFlag = true;
	u32 nextIdx = 0;
	if (_headerIdx < GetSize())
	{
		nextIdx = GetNextHeaderIdx(_headerIdx);
		if (nextIdx < GetSize())
		{
			nextFlag = GetUseFlag(nextIdx);
		}
	}

	u32 freeIdx = _headerIdx;

	// 前も後ろもフリー
	if (!prevFlag && !nextFlag)
	{
		u32 nextHeader = GetHeader(nextIdx);
		u32 prevFooter = GetFooter(prevIdx);
		GetHeader(prevIdx) = nextHeader;
		GetFooter(nextIdx) = prevFooter;

		freeIdx = prevIdx;
	}
	// 前がフリー
	else if (!prevFlag)
	{
		u32 nowHeader = GetHeader(_headerIdx);
		u32 prevFooter = GetFooter(prevIdx);
		GetHeader(prevIdx) = nowHeader;
		GetFooter(_headerIdx) = prevFooter;

		freeIdx = prevIdx;
	}
	// 後ろがフリー
	else if (!nextFlag)
	{
		u32 nextHeader = GetHeader(nextIdx);
		u32 nowFooter = GetFooter(_headerIdx);
		GetHeader(_headerIdx) = nextHeader;
		GetFooter(nextIdx) = nowFooter;
	}

	return freeIdx;
}

POISON_END

﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "memory/double_buffer_allocator.h"


///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant



POISON_BGN

///-------------------------------------------------------------------------------------------------
/// コンストラクタ・デストラクタ
CDoubleBufferAllocatorBase::CDoubleBufferAllocatorBase()
{
}
CDoubleBufferAllocatorBase::~CDoubleBufferAllocatorBase()
{
}


///-------------------------------------------------------------------------------------------------
/// 初期化
b8		CDoubleBufferAllocatorBase::Initialize(void* _pBuf, const u32 _size)
{
	m_pBuf = _pBuf;
	m_size = _size;

	m_uBgnIndex = 0;
	m_uEndIndex = _size;
	m_isBgn = false;

	m_uUsedSize = 0;
	m_uNowCount = 0;
	m_uPrevUsedSize = 0;
	m_uPrevCount = 0;

	return true;
}


///-------------------------------------------------------------------------------------------------
///	メモリ更新、バッファ切り替え呼び出し
void	CDoubleBufferAllocatorBase::Update()
{
	m_isBgn ^= true;
	if (m_isBgn)
	{
		m_uBgnIndex = 0;
	}
	else
	{
		m_uEndIndex = m_size;
	}
	m_uPrevUsedSize = m_uUsedSize;
	m_uPrevCount = m_uNowCount;
	m_uUsedSize = 0;
	m_uNowCount = 0;
}


///-------------------------------------------------------------------------------------------------
///	終了処理
void	CDoubleBufferAllocatorBase::Finalize()
{
	//Assert(m_uUsedSize <= 0 && m_uNowCount <= 0);
	new(this) CDoubleBufferAllocatorBase;
}


///-------------------------------------------------------------------------------------------------
///	メモリ確保
void*	CDoubleBufferAllocatorBase::Malloc(const MemoryMallocParam& _param)
{
	if (GetFreeBufferSize(_param.align) < _param.size) { return NULL; }

	return m_isBgn ? BeginMalloc(_param.size, _param.align) : EndMalloc(_param.size, _param.align);
}


///-------------------------------------------------------------------------------------------------
/// メモリの全体容量取得
u32		CDoubleBufferAllocatorBase::GetTotalSize() const
{
	return GetSize();
}
/// メモリの使用中容量取得
u32		CDoubleBufferAllocatorBase::GetUsedSize() const
{
	return m_uPrevUsedSize + m_uUsedSize;
}
/// メモリの取得数取得
u32		CDoubleBufferAllocatorBase::GetUsedCount() const
{
	return m_uPrevCount + m_uNowCount;
}
/// メモリの全体の空き容量取得
u32		CDoubleBufferAllocatorBase::GetTotalRemainSize() const
{
	return m_uEndIndex - m_uBgnIndex;
}
/// メモリの一番大きい連続領域の空き容量取得
u32		CDoubleBufferAllocatorBase::GetMaxRemainSize(const u32 _align) const
{
	return GetFreeBufferSize(_align);
}


///-------------------------------------------------------------------------------------------------
/// 前方メモリ確保
void*	CDoubleBufferAllocatorBase::BeginMalloc(size_t _size, u32 _align)
{
	u32 bufferIdx = GetAlignIdx(m_uBgnIndex, _align);

	// メモリ確保位置移動
	m_uBgnIndex = bufferIdx + SCast<u32>(_size);
	return GetBuffer(bufferIdx);
}


///-------------------------------------------------------------------------------------------------
/// 後方メモリ確保
void*	CDoubleBufferAllocatorBase::EndMalloc(size_t _size, u32 _align)
{
	u32 bufferIdx = GetAlignIdx(m_uEndIndex - SCast<u32>(_size), _align) - _align;

	// メモリ確保位置移動
	m_uEndIndex = bufferIdx;
	return GetBuffer(bufferIdx);
}


POISON_END

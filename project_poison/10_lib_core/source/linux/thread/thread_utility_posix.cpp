﻿
#ifdef LIB_THREAD_POSIX

///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "thread/thread_utility.h"


///-------------------------------------------------------------------------------------------------
#pragma comment(lib, "pthreadVC2.lib")


///-------------------------------------------------------------------------------------------------
/// prototype


POISON_BGN

///-------------------------------------------------------------------------------------------------
/// constant
static THREAD_HANDLE	s_mainThreadHdl = THREAD_HANDLE_INIT;
static u64				s_mainThreadId = 0;

const DWORD MS_VC_EXCEPTION = 0x406D1388;
typedef struct tagTHREADNAME_INFO
{
	DWORD	dwType; // Must be 0x1000.  
	LPCSTR	szName; // Pointer to name (in user addr space).  
	DWORD	dwThreadID; // Thread ID (-1=caller thread).  
	DWORD	dwFlags; // Reserved for future use, must be zero.  
} THREADNAME_INFO;

/// スレッド名設定
void	SetThreadName(u64 _threadID, const c8* _threadName)
{
	THREADNAME_INFO info;
	info.dwType = 0x1000;
	info.szName = _threadName;
	info.dwThreadID = (DWORD)_threadID;
	info.dwFlags = 0;
	__try {
		::RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
	}
	__except (EXCEPTION_EXECUTE_HANDLER) {
	}
}

///-------------------------------------------------------------------------------------------------
/// 初期化
void			CThreadUtility::Initialize()
{
	s_mainThreadHdl = GetCurrentThreadHdl();
	s_mainThreadId = GetThreadID(s_mainThreadHdl);
}

///-------------------------------------------------------------------------------------------------
/// アフィニティ設定
void			CThreadUtility::SetAffinityMask(THREAD_HANDLE _hdl, u32 _mask)
{
	// アフィニティ設定できやん
	//::pthread_setaffinity_np(_hdl, mask);
}

///-------------------------------------------------------------------------------------------------
/// 現在のスレッドハンドル取得
THREAD_HANDLE	CThreadUtility::GetCurrentThreadHdl()
{
	return ::pthread_self();
}

///-------------------------------------------------------------------------------------------------
/// 指定のスレッドID取得
u64				CThreadUtility::GetThreadID(const THREAD_HANDLE& _hdl)
{
	return ::pthread_getunique_np(_hdl);
}

///-------------------------------------------------------------------------------------------------
/// メインスレッドかどうか
b8				CThreadUtility::IsMainThread()
{
	return (GetThreadID(GetCurrentThreadHdl()) == s_mainThreadId);
}
b8				CThreadUtility::IsMainThread(u64 _threadId)
{
	return (_threadId == s_mainThreadId);
}

///-------------------------------------------------------------------------------------------------
/// スレッド生成
b8				CThreadUtility::Create(THREAD_HANDLE& _hdl, THREAD_FUNC* _func, void* _arg, const u32 _size, const s32 _prio, const c8* _pName)
{
	// 属性設定
	::pthread_attr_t attr;
	::pthread_attr_init(&attr);
	::pthread_attr_setscope(&attr, PTHREAD_SCOPE_PROCESS);
	::pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	//::pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	::pthread_attr_setstackaddr(&attr, NULL);
	::pthread_attr_setstacksize(&attr, 1024u * 1024u);
	::sched_param sched;
	sched.sched_priority = max(min((_prio + THREAD_PRIO_MIDDLE), THREAD_PRIO_HIGHEST), THREAD_PRIO_LOWEST);
	::pthread_attr_setschedparam(&attr, &sched);
	::pthread_attr_setinheritsched(&attr, PTHREAD_INHERIT_SCHED);
	::pthread_attr_setschedpolicy(&attr, SCHED_OTHER);
	// スレッド生成
	s32 ret = ::pthread_create(&_hdl, &attr, _func, _arg);
	// スレッド名設定
	SetThreadName(GetThreadID(_hdl), _pName);
	return (ret == 0);
}

///-------------------------------------------------------------------------------------------------
/// スレッド終了
THREAD_RESULT	CThreadUtility::Exit(THREAD_RESULT _result)
{
	::pthread_exit(_result);
	return _result;
}

///-------------------------------------------------------------------------------------------------
/// スレッドが稼働しているかどうか
b8				CThreadUtility::IsRunning(const THREAD_HANDLE& _hdl)
{
	s32 ret = ::pthread_kill(_hdl, 0);
	return (ret != ESRCH);
}

///-------------------------------------------------------------------------------------------------
/// 終了を待つ
THREAD_RESULT	CThreadUtility::Wait(const THREAD_HANDLE& _hdl)
{
	if (IsRunning(_hdl))
	{
		THREAD_RESULT result;
		// スレッド終了
		s32 ret = ::pthread_join(_hdl, &result);
		if (ret == 0)
		{
			return result;
		}
		if (ret == EDEADLK)
		{
			AssertBaseMsg(0, "デッドロックが発生しています。");
		}
		//if (ret == ESRCH)
		//{
		//	AssertBaseMsg(0, "指定のスレッド ID に対応するスレッドが見つかりません。");
		//}
		//if (ret == EINVAL)
		//{
		//	AssertBaseMsg(0, "指定のスレッド ID に対応するスレッドは、切り離されています。");
		//}
	}
	return THREAD_INVALID_RESULT;
}

///-------------------------------------------------------------------------------------------------
/// スレッド削除
void		CThreadUtility::Delete(THREAD_HANDLE& _hdl, THREAD_RESULT* _pResult/* = NULL*/)
{
	THREAD_RESULT result = Wait(_hdl);
	if (_pResult && result != THREAD_INVALID_RESULT)
	{
		(*_pResult) = result;
	}
}


///-------------------------------------------------------------------------------------------------
/// ミューテックス生成
b8			CThreadUtility::CreateMutex(THREAD_MUTEX& _mutex)
{
	::memset(&_mutex, 0, sizeof(_mutex));
	if (::pthread_mutex_init(&_mutex, NULL) != 0)
	{
		return false;
	}
	return true;
}

///-------------------------------------------------------------------------------------------------
/// ミューテックスがロック中かどうか
b8			CThreadUtility::IsMutexLock(THREAD_MUTEX& _mutex)
{
	return (::pthread_mutex_trylock(&_mutex) != 0);
}

///-------------------------------------------------------------------------------------------------
/// ミューテックスロック
SAL_DEF(_Acquires_exclusive_lock_(&_mutex))
void		CThreadUtility::MutexLock(THREAD_MUTEX& _mutex)
{
	::pthread_mutex_lock(&_mutex);
}

///-------------------------------------------------------------------------------------------------
/// ミューテックスアンロック
SAL_DEF(_Releases_exclusive_lock_(&_mutex))
void		CThreadUtility::MutexUnLock(THREAD_MUTEX& _mutex)
{
	::pthread_mutex_unlock(&_mutex);
}

///-------------------------------------------------------------------------------------------------
/// ミューテックス破棄
void		CThreadUtility::DeleteMutex(THREAD_MUTEX& _mutex)
{
	::pthread_mutex_destroy(&_mutex);
}


///-------------------------------------------------------------------------------------------------
/// イベント生成
b8			CThreadUtility::CreateEvent(THREAD_EVENT& _event, const c8* _pName)
{
	if (::pthread_mutex_init(&_event.mutex, NULL) != 0)
	{
		return false;
	}
	if (::pthread_cond_init(&_event.cond, NULL) != 0)
	{
		return false;
	}
	_event.signalled = false;
	strcpy_s(_event.name, _pName);
	return true;
}

///-------------------------------------------------------------------------------------------------
/// イベント待機
b8			CThreadUtility::WaitEvent(THREAD_EVENT& _event, s64 _timeOut/* = -1*/)
{
	b8 isEnd = true;
	::pthread_mutex_lock(&_event.mutex);
	while (!_event.signalled)
	{
		if (_timeOut < 0)
		{
			::pthread_cond_wait(&_event.cond, &_event.mutex);
		}
		else
		{
			// ミリ秒合わせ
			timespec timeout;
			timeout.tv_sec = _timeOut / 1000;
			timeout.tv_nsec = (_timeOut % 1000) * 1000;
			if (::pthread_cond_timedwait(&_event.cond, &_event.mutex, &timeout) == ETIMEDOUT)
			{
				isEnd = false;
				break;
			}
		}
	}
	_event.signalled = false;
	::pthread_mutex_unlock(&_event.mutex);
	return isEnd;
}

///-------------------------------------------------------------------------------------------------
/// イベント発行
void		CThreadUtility::SignalEvent(THREAD_EVENT& _event)
{
	::pthread_mutex_lock(&_event.mutex);
	_event.signalled = true;
	::pthread_mutex_unlock(&_event.mutex);
	::pthread_cond_signal(&_event.cond);
}

///-------------------------------------------------------------------------------------------------
/// イベント破棄
void		CThreadUtility::DeleteEvent(THREAD_EVENT& _event)
{
	::pthread_mutex_destroy(&_event.mutex);
	::pthread_cond_destroy(&_event.cond);
}


POISON_END

#endif // LIB_THREAD_POSIX

﻿//#pragma once
#ifndef __LIB_CORE_DEF_H__
#define __LIB_CORE_DEF_H__


//-------------------------------------------------------------------------------------------------
// include
#include "math/math.h"
#include "math/vector.h"
#include "math/rotate.h"
#include "math/matrix.h"

#include "memory/memory.h"

#include "collection/idx_list.h"
#include "collection/idx_sort_list.h"
#include "collection/link_list.h"
#include "collection/link_class.h"
#include "collection/link_node.h"

#include "smart_ptr/smart_ptr.h"
#include "singleton/static_singleton.h"
#include "singleton/dynamic_singleton.h"

#include "device/device.h"
#include "device/device_utility.h"

#include "input/keyboard.h"
#include "input/mouse.h"
#include "input/pad.h"
#include "input/input_utility.h"


#endif	// __LIB_CORE_DEF_H__

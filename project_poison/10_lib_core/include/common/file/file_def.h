﻿//#pragma once
#ifndef __FILE_DEF_H__
#define __FILE_DEF_H__


POISON_BGN

//-------------------------------------------------------------------------------------------------
//@brief	ファイルエラー
enum FILE_ERROR
{
	FILE_ERROR_NOT_FOUND = 0x1,          ///< ファイルがない
	FILE_ERROR_NOT_ENOUGH_MEMORY = 0x2,  ///< メモリが足りない
	FILE_ERROR_TRY_OPEN_FAILED = 0x4,    ///< トライオープン失敗
	FILE_ERROR_OPEN_MAX = 0x8,           ///< オープン数上限
	FILE_ERROR_LOAD_MAX = 0x10,           ///< ロード数上限
	FILE_ERROR_NOT_READY = 0x20,          ///< ロード準備ができていない
	FILE_ERROR_INVALID_HANDLE = 0x40,     ///< ハンドルが無効
	FILE_ERROR_LOAD_FAILED = 0x80,        ///< ロード失敗
	FILE_ERROR_ACCESS_DENIED = 0x100,      ///< アクセスが拒否された
};

//-------------------------------------------------------------------------------------------------
//@brief	ファイル属性
enum FILE_ATTR : u32
{
	FILE_ATTR_NONE = 0,
	FILE_ATTR_TRY_OPEN = 0x1,
	FILE_ATTR_CONTAIN_LIST = 0x2,
};


POISON_END

#endif	// __FILE_DEF_H__

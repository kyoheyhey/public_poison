﻿//#pragma once
#ifndef __FILE_OPERATE_H__
#define __FILE_OPERATE_H__


POISON_BGN

// デバッグのdefine
#ifndef POISON_RELEASE
#define DEBUG_FILE_OPERATE
#endif // !POISON_RELEASE

// Winで定義が被るので無効化
#ifdef DeleteFile
#undef DeleteFile
#endif // DeleteFile

// ファイル名の長さの最大
#define FO_FILENAME_MAX_LEN 128

// ファイルパスの長さの最大
#define FO_FILEPATH_MAX_LEN 256

// ファイルハンドル構造体
// 必要な情報がある場合中身を定義する
struct FileOperateHandle;

// ファイルハンドル
typedef FileOperateHandle* FoFileHn;

// ファイル操作データポインタ
typedef void* FILE_FIND_DATA;

// 非同期読込用データ
struct FILE_ASYNC_DATA
{
	u8 d[64];
};

// ディレクトリ検索設定
struct FileDirInfoConfig
{
};

//-------------------------------------------------------------------------------------------------
// ファイルオープンモードフラグ
static const u32 FO_OPEN_MODE_READ = 0x00000001;
static const u32 FO_OPEN_MODE_WRITE = 0x00000002;
static const u32 FO_OPEN_MODE_CREATE = 0x00000008;
static const u32 FO_OPEN_MODE_CREATEIFNOTEXIST = 0x00000010;
static const u32 FO_OPEN_MODE_READWRITE = FO_OPEN_MODE_READ | FO_OPEN_MODE_WRITE;
static const u32 FO_OPEN_MODE_APPEND = FO_OPEN_MODE_WRITE | FO_OPEN_MODE_CREATEIFNOTEXIST;

//-------------------------------------------------------------------------------------------------
// 処理ステータス
enum FO_FILE_STATUS : u32
{
	FO_FILE_STATUS_FAILED = 0x80000000,		//< 失敗
	FO_FILE_STATUS_PROCESSING = 0,			//< 処理中
	FO_FILE_STATUS_COMPLETE = 1,			//< 処理完了
};

//-------------------------------------------------------------------------------------------------
//@brief	ファイル操作クラスインターフェース
class IFileOperate
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルハンドルが有効化どうか
	virtual b8	IsValidFileHandle(FoFileHn _hn) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルが存在するか？
	virtual b8	IsFileExist(const c8* _path) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	ディレクトリが存在するか？
	virtual b8	IsDirectoryExist(const c8* _path) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	読み込み専用か？
	virtual b8	IsReadOnly() const = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルのオープン
	virtual FoFileHn	Open(const c8* _path, u32 _mode, b8 _async, u32 _attr = 0) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルのクローズ
	virtual void	Close(FoFileHn _hn, FILE_ASYNC_DATA* _asdata = 0) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	ディレクトリを作成する
	virtual b8	MakeDirectory(const c8* _path) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルを削除する
	virtual b8	DeleteFile(const c8* _path) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	ディレクトリを削除する
	virtual b8	DeleteDirectory(const c8* _path) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルへのデータ書き込み
	virtual b8	Write(FoFileHn _hn, const void* _buf, const u32 _size, u32* _sizeWritten, u32* _status, FILE_ASYNC_DATA* _asdata = 0) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルからのデータ読み込み
	virtual b8	Read(FoFileHn _hn, void* _buf, const u32 _size, u32* _sizeRead, u32* _status, FILE_ASYNC_DATA* _asdata = 0) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルサイズの取得
	virtual u32	GetFileSize(FoFileHn _hn, s32* _size) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	システムベースパス取得
	virtual const c8*	GetSystemBasePath() const { return NULL; }

	//-------------------------------------------------------------------------------------------------
	//@brief	指定フォルダ直下にあるファイルを条件指定して探す
	virtual u32	GetFiles(const c8* _path, const c8* _cond, c8 _out[][FO_FILENAME_MAX_LEN], u32 _outMaxNum) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	指定フォルダ直下にあるフォルダを探す
	//@param	_path		探すパス.
	//@param	_config		検索設定.
	//@param	_out[][]	出力先配列.
	//@param	_outMaxNum	最大の配列サイズ.
	//@return	見つかったフォルダの数.
	virtual u32	GetDirs(const c8* _path, const FileDirInfoConfig* _config, c8 _out[][FO_FILENAME_MAX_LEN], u32 _outMaxNum) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	エラーコード取得
	virtual u32	GetLastErrorCode() = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	更新　必要なもののみ
	virtual void	Update() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	エラーコード作成
	//@param[in]	_e	エラー種別
	//@return			エラーコード
	inline static u32	MakeErrorCode(u32 _e) { return FO_FILE_STATUS_FAILED | _e; }

	//-------------------------------------------------------------------------------------------------
	//@brief	エラー判定
	//@param[in]	_status	エラーコード
	inline static b8	IsError(u32 _status) { return (_status & FO_FILE_STATUS_FAILED) != 0; }
};

POISON_END

#endif	// __FILE_OPERATE_H__


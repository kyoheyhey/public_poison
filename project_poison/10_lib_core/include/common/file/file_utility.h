﻿//#pragma once
#ifndef __FILE_UTILITY_H__
#define __FILE_UTILITY_H__

#include "file_operate.h"
#include "file_def.h"

#define UNUSE_LOCK


POISON_BGN


// ファイルハンドル
typedef size_t FILE_HANDLE;
// 無効のファイルハンドル
static const FILE_HANDLE	FILE_HANDLE_INVALID = 0;

// ファイル操作インターフェース
class IFileOperate;
class CAtomicLockReadWrite;

//-------------------------------------------------------------------------------------------------
//@brief	ファイルユーティリティ
class CFileUtility
{
public:
	struct FILE_OPERATE
	{
		IFileOperate*	pFileOperate = NULL;
		const c8*		pBaseRootPath = NULL;
	};

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルシステム初期化、起動時のみ
	//@return	初期化成功、失敗
	//@note
	static b8	Initialize(FILE_OPERATE* _pFileOperateList, u32 _fileOperateNum);

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルシステム破棄、終了時のみ
	static void	Finalize();

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	ルートパス変更
	//@param[in]	_pRootPath	新しいルートパス
	//@param[in]	_index		適用するファイルオペレータのインデックス（マイナス値でデフォルトを優先）
	static b8	ChangeRootPath(const c8* _pRootPath, s32 _index = -1);

	//-------------------------------------------------------------------------------------------------
	//@brief	ルートパス取得
	//@return	パス文字列
	//@param[in]	_index	適用するファイルオペレータのインデックス（マイナス値でデフォルトを優先）
	static const c8*	GetRootPath(s32 _index = -1);

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルオープンモード
	enum MODE
	{
		MODE_READ = 0,		//< 読み取り専用(r)
		MODE_WRITE,			//< 新規作成、書き込み専用(w)
		MODE_ADD_WRITE,		//< 新規or追記、書き込み専用(a)
		MODE_READ_WRITE,	//< 読み取り、書き込み、ファイルが既にある場合のみ(r+)
		MODE_READ_WRITE2,	//< 新規作成、読み取り、書き込み(w+)
		MODE_ADD_READ_WRITE,//< 新規or追記、読み取り、書き込み(a+)

		MODE_MAX_NUM		//< 個数
	};

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルのリード状態
	enum STATUS
	{
		// 共通
		FU_STATUS_OK = 0,	//< 成功
		FU_STATUS_PENDING,	//< 処理中
		FU_STATUS_FAILED,	//< 失敗

		// 読込
		STATUS_READED = FU_STATUS_OK,		//< 読み込み完了
		STATUS_READING = FU_STATUS_PENDING,	//< 読み込み中
		STATUS_FAILED = FU_STATUS_FAILED,
	};

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルオープン
	//@return	ファイルハンドル
	//@param[in]	_path	ファイルパス
	//@param[in]	_mode	ファイルオープンモード
	//@param[in]	_binary	バイナリかどうか(Windows向け)
	//@param[in]	_async
	//@param[in]	_attr	アトリビュート（拡張用）
	static FILE_HANDLE	Open(const c8* _path, const MODE _mode = MODE_READ, const b8 _binary = true, const b8 _async = false, const FILE_ATTR _attr = FILE_ATTR_NONE);

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイル読み込み
	//@return	リード状態
	//@param[in]	_handle	ファイルハンドル
	//@param[out]	_buf	ファイルの読み込み先、_size以上のバッファが必須
	//@param[in]	_size	読み込みサイズ
	//@param[in]	_asyncData
	static STATUS	Read(FILE_HANDLE& _handle, void* _buf, const u32 _size, FILE_ASYNC_DATA* _asyncData = NULL);

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイル書き込み
	//@return	成功、失敗
	//@param[in]	_handle	ファイルハンドル
	//@param[out]	_buf	ファイルの読み込み先、_size以上のバッファが必須
	//@param[in]	_size	読み込みサイズ
	static STATUS	Write(FILE_HANDLE& _handle, const void* _buf, const u32 _size);

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルクローズ
	//@return	ファイルクローズが成功したか、falseの場合クローズ中
	//@param[in]	_handle		ファイルハンドル
	//@param[in]	_asyncData	非同期用のデータ、クローズ時のキャンセルに使用する場合がある
	//@param[in]	_async		非同期でクローズするか、クローズも非同期にする場合はtrueにして_asyncDataも必要
	static b8	Close(FILE_HANDLE& _handle, FILE_ASYNC_DATA* _asyncData = NULL, bool _async = false);

	//-------------------------------------------------------------------------------------------------
	//@brief	ディレクトリを作成する
	//@return	成功したかどうか
	//@param[in]	_path	ディレクトリパス
	//@param[in]	_index	適用するファイルオペレータのインデックス（マイナス値でデフォルトを優先）
	static b8	MakeDirectory(const c8* _path, s32 _index = -1);

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイル存在チェック
	//@return	ファイル存在の有無
	//@param[in]	_path	ファイルパス
	//@param[in]	_index	適用するファイルオペレータのインデックス（マイナス値でデフォルトを優先）
	static b8	IsExistFile(const c8* _path, s32 _index = -1);

	//-------------------------------------------------------------------------------------------------
	//@brief	ディレクトリ存在チェック
	//@return	ディレクトリ存在の有無
	//@param[in]	_path	ディレクトリパス
	//@param[in]	_index	適用するファイルオペレータのインデックス（マイナス値でデフォルトを優先）
	static b8	IsExistDirectory(const c8* _path, s32 _index = -1);

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルを削除する
	//@return	成功したかどうか
	//@param[in]	_path	ファイルパス
	//@param[in]	_index	適用するファイルオペレータのインデックス（マイナス値でデフォルトを優先）
	static b8	DeleteFile(const c8* _path, s32 _index = -1);

	//-------------------------------------------------------------------------------------------------
	//@brief	ディレクトリを削除する
	//@return	成功したかどうか
	//@param[in]	_path	ディレクトリパス
	//@param[in]	_index	適用するファイルオペレータのインデックス（マイナス値でデフォルトを優先）
	static b8	DeleteDirectory(const c8* _path, s32 _index = -1);

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルサイズ取得
	//@return	処理状態
	//@param[in]	_handle	ファイルハンドル
	//@param[out]	_size	ファイルサイズ(マイナスだと無効)
	static CFileUtility::STATUS	GetFileSize(FILE_HANDLE& _handle, s32* _size);

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルバッファのアライメント取得
	//@return	ファイルバッファのアライメント数
	static u32	GetFileBufferAlign();

	//-------------------------------------------------------------------------------------------------
	//@brief	フォルダパスからそれと同階層のファイルをワイルドカードを指定してゲットする
	//@param[in]	_pPath	フォルダパスの文字列
	//@param[in]	_pWild	"*.*"などワイルドカードを設定できる
	//@param[out]	_ppOut	ファイルパスの文字列の2次元配列
	//@param[in]	_outMaxNum	ファイルパスの文字列の個数
	//@param[in]	_index	適用するファイルオペレータのインデックス（マイナス値でデフォルトを優先）
	//@return	同じ階層にあるファイルの数
	//@note	_ppOutがNULLの場合、ファイルの数だけ数える　名前の最長は128で固定
	static u32	GetFileListInDir(const c8* _pPath, const c8* _pWild, c8 _ppOut[][128] = NULL, u32 _outMaxNum = 0, s32 _index = -1);

	//-------------------------------------------------------------------------------------------------
	//@brief	フォルダパスからそれと同階層の拡張子を指定したファイルをゲットする
	//@param[in]	_pPath		フォルダパスの文字列
	//@param[in]	_pExt		探す拡張子
	//@param[out]	_ppOut		ファイルパスの文字列の2次元配列
	//@param[in]	_outMaxNum	ファイルパスの文字列の個数
	//@param[in]	_index		適用するファイルオペレータのインデックス（マイナス値でデフォルトを優先）
	//@return	同じ階層にあるファイルの数
	//@note	_ppOutがNULLの場合、ファイルの数だけ数える　名前の最長は128で固定
	static u32	GetFileListInDirExt(const c8* _pPath, const c8* _pExt, c8 _ppOut[][128] = NULL, u32 _outMaxNum = 0, s32 _index = -1);

	//-------------------------------------------------------------------------------------------------
	//@brief	指定フォルダ直下にあるフォルダを探す
	static u32	GetDirListInDir(const c8* _path, const FileDirInfoConfig* _config, c8 _out[][FO_FILENAME_MAX_LEN] = NULL, u32 _outMaxNum = 0, s32 _index = -1);

public: // 汎用関数
	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルパスからファイル名の部分を取得
	//@return	ファイル名(拡張子含む)
	//@param[in]	_pPath	ファイルパスの文字列
	static const c8*	GetFileName(const c8* _pPath) { return POISON::GetFileName(_pPath); }
	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルパスからファイル名の部分を取得
	//@return	ファイル名(拡張子含む)
	//@param[in]	_pPath	ファイルパスの文字列
	//@param[in]	_len	パスの長さ
	static const c8*	GetFileName(const c8* _pPath, u32 _len) { return POISON::GetFileName(_pPath, _len); }

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルパスから拡張子取得
	//@return	拡張子(.は含まない)
	//@param[in]	_pPath	ファイルパスの文字列
	static const c8*	GetFileExt(const c8* _pPath) { return POISON::GetFileExt(_pPath); }
	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルパスから拡張子取得
	//@return	拡張子(.は含まない)
	//@param[in]	_pPath	ファイルパスの文字列
	//@param[in]	_len	パスの長さ
	static const c8*	GetFileExt(const c8* _pPath, u32 _len) { return POISON::GetFileExt(_pPath, _len); }

	//-------------------------------------------------------------------------------------------------
	//@brief	相対ファイルパス取得
	//@return	相対ファイルパス
	//@param[in]	_path				ファイルパスの文字列
	//@param[out]	_filePath			相対ファイルパス
	//@param[in]	_index				適用するファイルオペレータのインデックス（マイナス値でデフォルトを優先）
	//@param[in]	_withSystemBasePath	システムのベースパスも付与する
	static void GetFilePath(const c8* _path, c8* _filePath, s32 _index = -1, b8 _withSystemBasePath = false);

	//-------------------------------------------------------------------------------------------------
	//@brief エラーコード取得
	//@param[in]	_index	適用するファイルオペレータのインデックス（マイナス値でデフォルトを優先）
	static u32 GetLastErrorCode(s32 _index = -1);

	//-------------------------------------------------------------------------------------------------
	//@brief 更新　必要なもののみ
	static void Update();

private:
#ifndef UNUSE_LOCK
	//-------------------------------------------------------------------------------------------------
	//@brief ロック取得
	//@return	CAtomicLock
	static CAtomicLockReadWrite& GetLock();
#endif // !UNUSE_LOCK

};


POISON_END

#endif	// __FILE_UTILITY_H__

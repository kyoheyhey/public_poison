﻿//#pragma once
#ifndef __THREAD_DEF_H__
#define __THREAD_DEF_H__


//-------------------------------------------------------------------------------------------------
// define
#define LIB_THREAD_POSIX
//#define LIB_THREAD_WIN


//-------------------------------------------------------------------------------------------------
// include
#if defined( LIB_THREAD_POSIX )

#define HAVE_STRUCT_TIMESPEC
#include "linux/thread/pthread.h"

#elif defined( LIB_THREAD_WIN )

#endif


#endif	// __THREAD_DEF_H__

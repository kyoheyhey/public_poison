﻿#pragma once
#ifndef	__DYNAMIC_SINGLETON_H__
#define	__DYNAMIC_SINGLETON_H__

#include "memory/memory.h"

POISON_BGN

//*************************************************************************************************
//@brief	動的シングルトン基底クラス
//
//@note		※コンストラクタ許可のためFRIEND_DYNAMIC_SINGLETONでフレンド定義すること
//*************************************************************************************************
template <class T>
class CDynamicSingleton
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	インスタンス取得
	static inline T* GetInstance()
	{
		return s_pInstance;
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	static b8	Initialize(MEM_HANDLE _memHdl)
	{
		if (!s_pInstance)
		{
			void* ptr = MEM_MALLOC(_memHdl, sizeof(T));
			s_pInstance = new(ptr) T;
		}
		return (s_pInstance != NULL);
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	static void	Finalize()
	{
		MEM_SAFE_FREE(s_memHdl, s_pInstance);
	}

protected:
	CDynamicSingleton() {}
	virtual ~CDynamicSingleton()
	{
		Finalize();
	}

private:
	CDynamicSingleton(const CDynamicSingleton &) = delete;
	CDynamicSingleton& operator=(const CDynamicSingleton &) = delete;
	CDynamicSingleton(CDynamicSingleton &&) = delete;
	CDynamicSingleton& operator=(CDynamicSingleton &&) = delete;

private:
	static T*			s_pInstance;
	static MEM_HANDLE	s_memHdl;
};

template <class T> T* CDynamicSingleton<T>::s_pInstance = NULL;
template <class T> MEM_HANDLE CDynamicSingleton<T>::s_memHdl = MEM_HDL_INVALID;

//-------------------------------------------------------------------------------------------------
// シングルトンクラス定義
#define	DYNAMIC_SINGLETON( _class )	class _class : public CDynamicSingleton<_class>
#define	DYNAMIC_SINGLETON_BASE( _class, _base )	class _class : public  _base, public CDynamicSingleton<_class>
#define FRIEND_DYNAMIC_SINGLETON( _class )	friend class CDynamicSingleton<_class>;

POISON_END

#endif	// __DYNAMIC_SINGLETON_H__

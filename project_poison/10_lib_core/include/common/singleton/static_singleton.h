﻿#pragma once
#ifndef	__STATIC_SINGLETON_H__
#define	__STATIC_SINGLETON_H__

POISON_BGN

//*************************************************************************************************
//@brief	静的シングルトン基底クラス
//
//@note		※コンストラクタ許可のためFRIEND_SINGLETONでフレンド定義すること
//*************************************************************************************************
template <class T>
class CStaticSingleton
{
public:
	static inline T& GetInstance()
	{
		static T s_instance;
		return s_instance;
	}

protected:
	CStaticSingleton() {}
	virtual ~CStaticSingleton() {}

private:
	CStaticSingleton(const CStaticSingleton &) = delete;
	CStaticSingleton& operator=(const CStaticSingleton &) = delete;
	CStaticSingleton(CStaticSingleton &&) = delete;
	CStaticSingleton& operator=(CStaticSingleton &&) = delete;
};


//*************************************************************************************************
//@brief	静的定義シングルトン基底クラス
//*************************************************************************************************
template <class T>
class CStaticDefSingleton
{
public:
	static inline T* GetInstance()
	{
		return s_pInstance;
	}

protected:
	CStaticDefSingleton()
	{
		libAssert(s_pInstance == NULL);
		s_pInstance = PCast<T>(this);
	}
	virtual ~CStaticDefSingleton()
	{
		s_pInstance = NULL;
	}

private:
	CStaticDefSingleton(const CStaticDefSingleton&) = delete;
	CStaticDefSingleton& operator=(const CStaticDefSingleton&) = delete;
	CStaticDefSingleton(CStaticDefSingleton&&) = delete;
	CStaticDefSingleton& operator=(CStaticDefSingleton&&) = delete;

private:
	static T*	s_pInstance = NULL;
};

//*************************************************************************************************
//@brief	シングルトンクラス定義
#define	STATIC_SINGLETON( _class )	class _class : public CStaticSingleton<_class>
#define	STATIC_SINGLETON_BASE( _class, _base )	class _class : public  _base, public CStaticSingleton<_class>
#define FRIEND_STATIC_SINGLETON( _class )	friend class CStaticSingleton<_class>

//*************************************************************************************************
//@brief	シングルトン定義クラス定義
#define	STATIC_DEF_SINGLETON( _class )	class _class : public CStaticDefSingleton<_class>
#define	STATIC_DEF_SINGLETON_BASE( _class, _base )	class _class : public  _base, public CStaticDefSingleton<_class>

POISON_END

#endif	// __STATIC_SINGLETON_H__

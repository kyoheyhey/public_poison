﻿//#pragma once
#ifndef __PLATFORM_DEF_H__
#define __PLATFORM_DEF_H__


POISON_BGN

//-------------------------------------------------------------------------------------------------
//@brief	プラットフォームデータ名
extern const c8*	GetPlatformName();

//-------------------------------------------------------------------------------------------------
//@brief	プラットフォームデータ名文字数
extern u32		GetPlatformNameLen();

POISON_END

#endif	// __PLATFORM_DEF_H__

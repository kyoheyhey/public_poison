﻿//#pragma once
#ifndef __PAD_H__
#define __PAD_H__


//-------------------------------------------------------------------------------------------------
// include
#include "input/input.h"


POISON_BGN

//*************************************************************************************************
//@brief	パッドクラス
//*************************************************************************************************
class CPad
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	キータイプ
	enum KEY_TYPE
	{
		eLeft = 0,
		eRight,
		eUp,
		eDown,
		eA,
		eB,
		eX,
		eY,
		eL,
		eR,
		eZL,
		eZR,
		eSL,
		eSR,
		ePlus,
		eMinus,

		eCircle = eA,
		eCross = eB,
		eTriangle = eX,
		eSquare = eY,
		eL1 = eL,
		eR1 = eR,
		eL2 = eZL,
		eR2 = eZR,
		eL3 = eSL,
		eR3 = eSR,
		eStart = ePlus,
		eSelect = eMinus,

		PAD_KEY_NUM,
	};

	//-------------------------------------------------------------------------------------------------
	//@brief	アナログタイプ
	enum ANALOG_TYPE
	{
		eLX = 0,
		eLY,
		eRX,
		eRY,
		eLZ,
		eRZ,

		PAD_ANALOG_NUM,
	};

public:
	CPad() {}
	~CPad() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	キー取得
	const CButton&	GetButton(KEY_TYPE _type) const { return m_button[_type]; }

	//-------------------------------------------------------------------------------------------------
	//@brief	アナログ取得
	const CAnalog&	GetAnalog(ANALOG_TYPE _type) const { return m_analog[_type]; }

	//-------------------------------------------------------------------------------------------------
	//@brief	更新
	virtual void	Step() = 0;

protected:
	CButton		m_button[PAD_KEY_NUM];		//< キーボタン
	CAnalog		m_analog[PAD_ANALOG_NUM];	//< アナログ
};


POISON_END

#endif	// __PAD_H__

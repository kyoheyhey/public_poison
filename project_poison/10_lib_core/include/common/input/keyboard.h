﻿//#pragma once
#ifndef __KEYBOARD_H__
#define __KEYBOARD_H__

//-------------------------------------------------------------------------------------------------
// include
#include "input/input.h"


POISON_BGN

//*************************************************************************************************
//@brief	キーボードクラス
//*************************************************************************************************
class CKeyboard
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	キータイプ
	enum KEY_TYPE
	{
		eAlt,
		eCtrl,
		eSpace,
		eEnter,
		eBack,
		eLeft,
		eRight,
		eUp,
		eDown,
		eF1,
		eF2,
		eF3,
		eF4,
		eF5,
		eF6,
		eF7,
		eF8,
		eF9,
		eF10,
		eF11,
		eF12,
		e0,
		e1,
		e2,
		e3,
		e4,
		e5,
		e6,
		e7,
		e8,
		e9,
		eA,
		eB,
		eC,
		eD,
		eE,
		eF,
		eG,
		eH,
		eI,
		eJ,
		eK,
		eL,
		eM,
		eN,
		eO,
		eP,
		eQ,
		eR,
		eS,
		eT,
		eU,
		eV,
		eW,
		eX,
		eY,
		eZ,

		KEYBOARD_KEY_NUM
	};

public:
	CKeyboard() {}
	virtual ~CKeyboard() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	キー取得
	const CButton&	GetButton(KEY_TYPE _type) const { return m_button[_type]; }

	//-------------------------------------------------------------------------------------------------
	//@brief	更新
	virtual void	Step() = 0;

protected:
	CButton		m_button[KEYBOARD_KEY_NUM];		//< キーボタン
};


POISON_END

#endif	// __KEYBOARD_H__

﻿//#pragma once
#ifndef __INPUT_UTILITYH__
#define __INPUT_UTILITYH__


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype
class CKeyboard;
class CMouse;
class CPad;

//*************************************************************************************************
//@brief	インプットユーティリティクラス
//*************************************************************************************************
class CInputUtility
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	取得
	static const CKeyboard*	GetKeyboard();
	static const CMouse*	GetMouse();
	static const CPad*		GetPad(u32 _idx);

	//-------------------------------------------------------------------------------------------------
	//@brief	セットアップ
	static void	Setup(CKeyboard* _pKeyboard);
	static void	Setup(CMouse* _pMouse);
	static void	Setup(CPad* _pPad[], u32 _num);

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	static void		Finalize();

	//-------------------------------------------------------------------------------------------------
	//@brief	更新
	static void		Step();
};



POISON_END

#endif	// __INPUT_UTILITYH__

﻿//#pragma once
#ifndef __LINK_CLASS_INL__
#define __LINK_CLASS_INL__


POISON_BGN

///-------------------------------------------------------------------------------------------------
/// テンプレート宣言部分の省略
#define LINK_CLASS_TEMPLATE_HEAD	template <class __NODE, class __BASE>
#define LINK_CLASS_CLASS_HEAD		TLinkClass< __NODE, __BASE >

//*************************************************************************************************
/// リンクノードクラス
//*************************************************************************************************
// 静的リンクノードリスト宣言
LINK_CLASS_TEMPLATE_HEAD
typename LINK_CLASS_CLASS_HEAD::NODE*		LINK_CLASS_CLASS_HEAD::s_pTop = NULL;
LINK_CLASS_TEMPLATE_HEAD
typename LINK_CLASS_CLASS_HEAD::NODE*		LINK_CLASS_CLASS_HEAD::s_pEnd = NULL;
LINK_CLASS_TEMPLATE_HEAD
u32		LINK_CLASS_CLASS_HEAD::s_num = 0;
LINK_CLASS_TEMPLATE_HEAD
typename LINK_CLASS_CLASS_HEAD::ADD_NODE_TYPE	LINK_CLASS_CLASS_HEAD::s_addType = ADD_NODE_TYPE::END;

///-------------------------------------------------------------------------------------------------
/// 先頭に追加
LINK_CLASS_TEMPLATE_HEAD
void	LINK_CLASS_CLASS_HEAD::AddNodeTop()
{
	NODE* pThis = SCast<NODE*>(this);
	if (s_pTop)
	{
		s_pTop->AddNodePrev(pThis);
	}
	else
	{
		s_pEnd = pThis;
	}
	s_pTop = pThis;
	++s_num;
}
///-------------------------------------------------------------------------------------------------
/// 末尾に追加
LINK_CLASS_TEMPLATE_HEAD
void	LINK_CLASS_CLASS_HEAD::AddNodeEnd()
{
	NODE* pThis = SCast<NODE*>(this);
	if (s_pEnd)
	{
		s_pEnd->AddNodeNext(pThis);
	}
	else
	{
		s_pTop = pThis;
	}
	s_pEnd = pThis;
	++s_num;
}
///-------------------------------------------------------------------------------------------------
/// ノード削除
LINK_CLASS_TEMPLATE_HEAD
void	LINK_CLASS_CLASS_HEAD::DeleteNode()
{
	NODE* pThis = SCast<NODE*>(this);
	if (s_pTop == NULL || s_pEnd == NULL){ return; }
	if (pThis == s_pTop){ s_pTop = m_pNext; }
	if (pThis == s_pEnd){ s_pEnd = m_pPrev; }
	{
		if (m_pPrev)
		{
			m_pPrev->SetNodeNext(m_pNext);
			m_pPrev = NULL;
		}
		if (m_pNext)
		{
			m_pNext->SetNodePrev(m_pPrev);
			m_pNext = NULL;
		}
	}
	--s_num;
}

///-------------------------------------------------------------------------------------------------
/// コンストラクタ
LINK_CLASS_TEMPLATE_HEAD
LINK_CLASS_CLASS_HEAD::TLinkClass()
	: m_pPrev(NULL), m_pNext(NULL)
{
	switch (s_addType)
	{
	case ADD_NODE_TYPE::TOP:
		AddNodeTop();
		break;
	case ADD_NODE_TYPE::END:
		AddNodeEnd();
		break;
	default:
		break;
	}
}

///-------------------------------------------------------------------------------------------------
/// デストラクタ
LINK_CLASS_TEMPLATE_HEAD
LINK_CLASS_CLASS_HEAD::~TLinkClass()
{
	DeleteNode();
}

POISON_END


#endif	// __LINK_CLASS_INL__

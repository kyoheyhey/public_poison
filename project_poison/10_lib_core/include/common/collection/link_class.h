﻿//#pragma once
#ifndef __LINK_CLASS_H__
#define __LINK_CLASS_H__


#include "class.h"

POISON_BGN

//*************************************************************************************************
//@brief	リンククラス
//*************************************************************************************************
template <class __NODE, class __BASE = CDontCopy>
class TLinkClass : public __BASE
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	追加タイプ
	enum class ADD_NODE_TYPE
	{
		TOP = 0,
		END,
	};

private:
	typedef __NODE		NODE;
	typedef __BASE		BASE;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ・デストラクタ
	TLinkClass();
	virtual ~TLinkClass();

	//-------------------------------------------------------------------------------------------------
	//@brief	取得
	inline const NODE*	GetNodePrev() const	{ return m_pPrev; }
	inline NODE*		GetNodePrev()		{ return m_pPrev; }
	inline const NODE*	GetNodeNext() const	{ return m_pNext; }
	inline NODE*		GetNodeNext()		{ return m_pNext; }

	//-------------------------------------------------------------------------------------------------
	//@brief	静的取得
	inline static NODE*		GetNodeTop() { return s_pTop; }
	inline static NODE*		GetNodeEnd() { return s_pEnd; }
	inline static u32		GetNodeNum() { return s_num; }

	//-------------------------------------------------------------------------------------------------
	//@brief	追加タイプ取得・設定
	inline static ADD_NODE_TYPE	GetAddNodeType() { return s_addType; }
	inline static void	SetAddNodeType(ADD_NODE_TYPE _type) { return s_addType = _type; }

protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	設定
	inline void	SetNodePrev(NODE* _pNode) { m_pPrev = _pNode; }
	inline void	SetNodeNext(NODE* _pNode) { m_pNext = _pNode; }

	//-------------------------------------------------------------------------------------------------
	//@brief	追加
	inline void	AddNodePrev(NODE* _pNode) { m_pPrev = _pNode; _pNode->m_pNext = SCast<NODE*>(this); }
	inline void	AddNodeNext(NODE* _pNode) { m_pNext = _pNode; _pNode->m_pPrev = SCast<NODE*>(this); }

private:
	//@brief	コピーコンストラクタ(使用不可)
	TLinkClass(const TLinkClass& _node);
	//@brief	コピーオペレーター(使用不可)
	const TLinkClass& operator=(const TLinkClass& _node);

	//-------------------------------------------------------------------------------------------------
	//@brief	先頭に追加
	void		AddNodeTop();
	//-------------------------------------------------------------------------------------------------
	//@brief	末尾に追加
	void		AddNodeEnd();
	//-------------------------------------------------------------------------------------------------
	//@brief	ノード削除
	void		DeleteNode();

private:
	NODE*		m_pPrev;	//< 前
	NODE*		m_pNext;	//< 後

private:
	static NODE*			s_pTop;		//< 先頭ノード
	static NODE*			s_pEnd;		//< 末尾ノード
	static u32				s_num;		//< ノード数
	static ADD_NODE_TYPE	s_addType;	//< 追加タイプ
};

//*************************************************************************************************
//@brief	クラスをリンクリストで繋ぎたいよ定義
#define LINK_CLASS( __class )				class __class : public TLinkClass< __class, CDontCopy >
#define LINK_CLASS_BASE( __class, __base )	class __class : public TLinkClass< __class, __base >

POISON_END

#include "link_class.inl"


#endif	// __LINK_CLASS_H__

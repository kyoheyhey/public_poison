﻿//#pragma once
#ifndef __IDX_SORT_LIST_INL__
#define __IDX_SORT_LIST_INL__


POISON_BGN

///*************************************************************************************************
///	ベースソートインデックスリスト
///*************************************************************************************************

//-------------------------------------------------------------------------------------------------
/// テンプレート宣言部分の省略
#define KEY_SORT_TEMPLATE_HEAD	template <typename __KEY>
#define KEY_SORT_CLASS_HEAD		CKeySort< __KEY >

//-------------------------------------------------------------------------------------------------
///@brief	初期化
KEY_SORT_TEMPLATE_HEAD
b8		KEY_SORT_CLASS_HEAD::Initialize(IAllocator* _pAllocator, u16 _num)
{
	m_pKeySortTable = MEM_CALLOC_TYPES(_pAllocator, KEY_TABLE, _num);
	libAssert(m_pKeySortTable);
	return (m_pKeySortTable != NULL);
}

//-------------------------------------------------------------------------------------------------
///@brief	終了
KEY_SORT_TEMPLATE_HEAD
void		KEY_SORT_CLASS_HEAD::Finalize(IAllocator* _pAllocator)
{
	MEM_SAFE_FREE(_pAllocator, m_pKeySortTable);
}

//-------------------------------------------------------------------------------------------------
///@brief	クリア
KEY_SORT_TEMPLATE_HEAD
inline void		KEY_SORT_CLASS_HEAD::Clear(u16 _maxNum)
{
	memset(m_pKeySortTable, 0, sizeof(KEY_TABLE) * _maxNum);
}

//-------------------------------------------------------------------------------------------------
//@brief	キー追加
KEY_SORT_TEMPLATE_HEAD
b8		KEY_SORT_CLASS_HEAD::PushKey(u16 _sortIdx, const KEY& _key, u16 _idx, u16 _useNum)
{
	libAssert(_sortIdx <= _useNum);
	memmove(m_pKeySortTable + _sortIdx + 1, m_pKeySortTable + _sortIdx, sizeof(KEY_TABLE) * (_useNum - _sortIdx));
	KEY_TABLE& keyTable = m_pKeySortTable[_sortIdx];
	keyTable.key = _key;
	keyTable.idx = _idx;
	return true;
}

//-------------------------------------------------------------------------------------------------
//@brief	キー追加
KEY_SORT_TEMPLATE_HEAD
b8		KEY_SORT_CLASS_HEAD::PushFront(const KEY& _key, u16 _idx, u16 _useNum)
{
	u16 sortIdx = 0;
	SearchFrontSortIdx(sortIdx, _key, _useNum);
	PushKey(sortIdx, _key, _idx, _useNum);
	return true;
}

KEY_SORT_TEMPLATE_HEAD
b8		KEY_SORT_CLASS_HEAD::PushBack(const KEY& _key, u16 _idx, u16 _useNum)
{
	u16 sortIdx = 0;
	if (SearchBackSortIdx(sortIdx, _key, _useNum))
	{
		sortIdx++;
	}
	PushKey(sortIdx, _key, _idx, _useNum);
	return true;
}

//-------------------------------------------------------------------------------------------------
//@brief	キー削除
KEY_SORT_TEMPLATE_HEAD
b8		KEY_SORT_CLASS_HEAD::Pop(const KEY& _key, u16 _idx, u16 _useNum)
{
	u16 sortIdx = 0;
	if (SearchFrontSortIdx(sortIdx, _key, _useNum))
	{
		while (GetKey(sortIdx) == _key && sortIdx < _useNum)
		{
			// 同一インデックスを線形探査で調べて削除
			if (Pop(sortIdx, _idx, _useNum))
			{
				return true;
			}
			sortIdx++;
		}
	}
	return false;
}

KEY_SORT_TEMPLATE_HEAD
b8		KEY_SORT_CLASS_HEAD::Pop(u16 _sortIdx, u16 _idx, u16 _useNum)
{
	// 同一インデックスを線形探査で調べて削除
	if (GetIdx(_sortIdx) == _idx)
	{
		memmove(m_pKeySortTable + _sortIdx, m_pKeySortTable + _sortIdx + 1, sizeof(KEY_TABLE) * (_useNum - _sortIdx - 1));
		return true;
	}
	return false;
}

///-------------------------------------------------------------------------------------------------
///@brief	キー検索で先頭ソートインデックス取得
KEY_SORT_TEMPLATE_HEAD
b8		KEY_SORT_CLASS_HEAD::SearchFrontSortIdx(u16& _retSortIdx, const KEY& _key, u16 _useNum)
{
	if (SearchSortIdx(_retSortIdx, _key, _useNum))
	{
		// 同キーの先頭インデックスまで検索
		u16 bgnIdx = 0;
		while (_retSortIdx > bgnIdx && GetKey(_retSortIdx - 1) == _key)
		{
			_retSortIdx--;
		}
		return true;
	}
	return false;
}

///-------------------------------------------------------------------------------------------------
///@brief	キー検索で末尾ソートインデックス取得
KEY_SORT_TEMPLATE_HEAD
b8		KEY_SORT_CLASS_HEAD::SearchBackSortIdx(u16& _retSortIdx, const KEY& _key, u16 _useNum)
{
	if (SearchSortIdx(_retSortIdx, _key, _useNum))
	{
		// 同キーの末尾インデックスまで検索
		u16 endIdx = _useNum - 1;
		while (_retSortIdx < endIdx && GetKey(_retSortIdx + 1) == _key)
		{
			_retSortIdx++;
		}
		return true;
	}
	return false;
}

//-------------------------------------------------------------------------------------------------
//@brief	キー検索でソートインデックス取得
KEY_SORT_TEMPLATE_HEAD
b8		KEY_SORT_CLASS_HEAD::SearchSortIdx(u16& _retSortIdx, const KEY& _key, u16 _useNum)
{
	return SearchSortIdx(_retSortIdx, _key, 0, _useNum);
}

KEY_SORT_TEMPLATE_HEAD
b8		KEY_SORT_CLASS_HEAD::SearchSortIdx(u16& _retSortIdx, const KEY& _key, u16 _bgnIdx, u16 _endIdx)
{
	_retSortIdx = _bgnIdx;
	s32 bgnIdx = _bgnIdx;
	s32 endIdx = _endIdx - 1;
	// 二分探査
	while (bgnIdx <= endIdx)
	{
		s32 helfIdx = bgnIdx + (endIdx - bgnIdx) / 2;
		if (GetKey(helfIdx) == _key)
		{
			_retSortIdx = helfIdx;
			return true;
		}
		else if (GetKey(helfIdx) > _key)
		{
			endIdx = helfIdx - 1;
		}
		else
		{
			bgnIdx = helfIdx + 1;
		}
	}
	_retSortIdx = bgnIdx;
	return false;
}

///-------------------------------------------------------------------------------------------------
///@brief	キーソートコピー
KEY_SORT_TEMPLATE_HEAD
void	KEY_SORT_CLASS_HEAD::CopyKeySort(const CKeySort& _keySort, u16 _maxNum)
{
	memcpy(m_pKeySortTable, _keySort.m_pKeySortTable, sizeof(KEY_TABLE) * _maxNum);
}


///*************************************************************************************************
///	ソートインデックスリスト
///*************************************************************************************************

//-------------------------------------------------------------------------------------------------
/// テンプレート宣言部分の省略
#define SORT_IDX_LIST_TEMPLATE_HEAD	template <typename __DATA, typename __KEY, typename __INTERFACE>
#define SORT_IDX_LIST_CLASS_HEAD	CIdxSortList< __DATA, __KEY, __INTERFACE >

//-------------------------------------------------------------------------------------------------
///@brief	初期化
SORT_IDX_LIST_TEMPLATE_HEAD
b8		SORT_IDX_LIST_CLASS_HEAD::Initialize(IAllocator* _pAllocator, u16 _num)
{
	if (BASE::Initialize(_pAllocator, _num))
	{
		return SORT::Initialize(_pAllocator, _num);
	}
	return false;
}
SORT_IDX_LIST_TEMPLATE_HEAD
b8		SORT_IDX_LIST_CLASS_HEAD::Initialize(MEM_HANDLE _memHdl, u16 _num)
{
	return Initialize(MEM_ALLOCATOR(_memHdl), _num);
}

//-------------------------------------------------------------------------------------------------
///@brief	終了
SORT_IDX_LIST_TEMPLATE_HEAD
void		SORT_IDX_LIST_CLASS_HEAD::Finalize()
{
	SORT::Finalize(m_pAllocator);
	BASE::Finalize();
}

//-------------------------------------------------------------------------------------------------
///@brief	クリア
SORT_IDX_LIST_TEMPLATE_HEAD
void		SORT_IDX_LIST_CLASS_HEAD::Clear()
{
	BASE::Clear();
	SORT::Clear(m_maxNum);
}


//-------------------------------------------------------------------------------------------------
///@brief	イテレータからデータ取得
SORT_IDX_LIST_TEMPLATE_HEAD
typename SORT_IDX_LIST_CLASS_HEAD::DATA&	SORT_IDX_LIST_CLASS_HEAD::GetDataFromItr(const ITR* _pItr)
{
	return GetTopData()[BASE::GetIdx(_pItr)];
}

//-------------------------------------------------------------------------------------------------
///@brief	インデックスからデータ取得
SORT_IDX_LIST_TEMPLATE_HEAD
typename SORT_IDX_LIST_CLASS_HEAD::DATA&	SORT_IDX_LIST_CLASS_HEAD::GetDataFromIdx(u16 _idx)
{
	libAssert(BASE::CheckIdx(_idx));
	return GetTopData()[_idx];
}

//-------------------------------------------------------------------------------------------------
///@brief	ソートインデックスからデータ取得（キーソートテーブルは前詰め）
SORT_IDX_LIST_TEMPLATE_HEAD
typename SORT_IDX_LIST_CLASS_HEAD::DATA&	SORT_IDX_LIST_CLASS_HEAD::GetDataFromSortIdx(u16 _sortIdx)
{
	libAssert(CheckKeyIdx(_sortIdx));
	return GetDataFromIdx(SORT::GetIdx(_sortIdx));
}

//-------------------------------------------------------------------------------------------------
///@brief	ソートインデックスからイテレータ取得（キーソートテーブルは前詰め）
SORT_IDX_LIST_TEMPLATE_HEAD
typename SORT_IDX_LIST_CLASS_HEAD::ITR*		SORT_IDX_LIST_CLASS_HEAD::GetItrFromSortIdx(u16 _sortIdx)
{
	libAssert(CheckKeyIdx(_sortIdx));
	return GetItr(SORT::GetIdx(_sortIdx));
}

//-------------------------------------------------------------------------------------------------
///@brief	ソートインデックスからデータ取得（キーソートテーブルは前詰め）
SORT_IDX_LIST_TEMPLATE_HEAD
const typename SORT_IDX_LIST_CLASS_HEAD::KEY&	SORT_IDX_LIST_CLASS_HEAD::GetKeyFromSortIdx(u16 _sortIdx) const
{
	libAssert(CheckKeyIdx(_sortIdx));
	return SORT::GetKey(_sortIdx);
}

//-------------------------------------------------------------------------------------------------
///@brief	キー検索でソートインデックス取得
SORT_IDX_LIST_TEMPLATE_HEAD
u16	SORT_IDX_LIST_CLASS_HEAD::SearchSortIdx(const KEY& _key)
{
	u16 hitSortIdx = 0;
	if (SORT::SearchFrontSortIdx(hitSortIdx, _key, m_useNum))
	{
		return hitSortIdx;
	}
	return IDX_INVALID;
}

//-------------------------------------------------------------------------------------------------
///@brief	キー検索でイテレータ取得
SORT_IDX_LIST_TEMPLATE_HEAD
typename SORT_IDX_LIST_CLASS_HEAD::ITR*	SORT_IDX_LIST_CLASS_HEAD::SearchItr(const KEY& _key)
{
	u16 hitSortIdx = 0;
	if (SORT::SearchSortIdx(hitSortIdx, _key, m_useNum))
	{
		return BASE::GetItr(SORT::GetIdx(hitSortIdx));
	}
	return NULL;
}

//-------------------------------------------------------------------------------------------------
///@brief	キー検索でデータ取得
SORT_IDX_LIST_TEMPLATE_HEAD
typename SORT_IDX_LIST_CLASS_HEAD::DATA*	SORT_IDX_LIST_CLASS_HEAD::SearchData(const KEY& _key)
{
	u16 hitSortIdx = 0;
	if (SORT::SearchSortIdx(hitSortIdx, _key, m_useNum))
	{
		return &GetDataFromIdx(SORT::GetIdx(hitSortIdx));
	}
	return NULL;
}


//-------------------------------------------------------------------------------------------------
///@brief	追加
SORT_IDX_LIST_TEMPLATE_HEAD
typename SORT_IDX_LIST_CLASS_HEAD::ITR* SORT_IDX_LIST_CLASS_HEAD::PushFront(const KEY& _key)
{
	ITR* pItr = NULL;
	if (INTERFACE::IsEnableSameName())
	{
		pItr = BASE::PushFront();
		if (pItr)
		{
			DATA& rData = GetDataFromItr(pItr);
			INTERFACE::Clear(rData);
			INTERFACE::SetKey(rData, _key);
			SORT::PushFront(_key, BASE::GetIdx(pItr), m_useNum - 1);
		}
	}
	else
	{
		u16 sortIdx;
		// ない場合のみ追加
		if (!SORT::SearchSortIdx(sortIdx, _key, m_useNum))
		{
			pItr = BASE::PushFront();
			if (pItr)
			{
				DATA& rData = GetDataFromItr(pItr);
				INTERFACE::Clear(rData);
				INTERFACE::SetKey(rData, _key);
				SORT::PushKey(sortIdx, _key, BASE::GetIdx(pItr), m_useNum - 1);
			}
		}
		// あれば返す
		else
		{
			pItr = BASE::GetItr(SORT::GetIdx(sortIdx));
		}
	}
	return pItr;
}
SORT_IDX_LIST_TEMPLATE_HEAD
typename SORT_IDX_LIST_CLASS_HEAD::ITR*	SORT_IDX_LIST_CLASS_HEAD::PushFront(const DATA& _data)
{
	ITR* pItr = NULL;
	if (INTERFACE::IsEnableSameName())
	{
		pItr = BASE::PushFront();
		if (pItr)
		{
			INTERFACE::Copy(GetDataFromItr(pItr), _data);
			SORT::PushFront(INTERFACE::GetKey(_data), BASE::GetIdx(pItr), m_useNum - 1);
		}
	}
	else
	{
		u16 sortIdx;
		// ない場合のみ追加
		if (!SORT::SearchSortIdx(sortIdx, INTERFACE::GetKey(_data), m_useNum))
		{
			pItr = BASE::PushFront();
			if (pItr)
			{
				INTERFACE::Copy(GetDataFromItr(pItr), _data);
				SORT::PushKey(sortIdx, INTERFACE::GetKey(_data), BASE::GetIdx(pItr), m_useNum - 1);
			}
		}
		// あれば返す
		else
		{
			pItr = BASE::GetItr(SORT::GetIdx(sortIdx));
		}
	}
	return pItr;
}

SORT_IDX_LIST_TEMPLATE_HEAD
typename SORT_IDX_LIST_CLASS_HEAD::ITR* SORT_IDX_LIST_CLASS_HEAD::PushBack(const KEY& _key)
{
	ITR* pItr = NULL;
	if (INTERFACE::IsEnableSameName())
	{
		pItr = BASE::PushBack();
		if (pItr)
		{
			DATA& rData = GetDataFromItr(pItr);
			INTERFACE::Clear(rData);
			INTERFACE::SetKey(rData, _key);
			SORT::PushBack(_key, BASE::GetIdx(pItr), m_useNum - 1);
		}
	}
	else
	{
		u16 sortIdx;
		// ない場合のみ追加
		if (!SORT::SearchSortIdx(sortIdx, _key, m_useNum))
		{
			pItr = BASE::PushBack();
			if (pItr)
			{
				DATA& rData = GetDataFromItr(pItr);
				INTERFACE::Clear(rData);
				INTERFACE::SetKey(rData, _key);
				SORT::PushKey(sortIdx, _key, BASE::GetIdx(pItr), m_useNum - 1);
			}
		}
		// あれば返す
		else
		{
			pItr = BASE::GetItr(SORT::GetIdx(sortIdx));
		}
	}
	return pItr;
}
SORT_IDX_LIST_TEMPLATE_HEAD
typename SORT_IDX_LIST_CLASS_HEAD::ITR*	SORT_IDX_LIST_CLASS_HEAD::PushBack(const DATA& _data)
{
	ITR* pItr = NULL;
	if (INTERFACE::IsEnableSameName())
	{
		pItr = BASE::PushBack();
		if (pItr)
		{
			INTERFACE::Copy(GetDataFromItr(pItr), _data);
			SORT::PushBack(INTERFACE::GetKey(_data), BASE::GetIdx(pItr), m_useNum - 1);
		}
	}
	else
	{
		u16 sortIdx;
		// ない場合のみ追加
		if (!SORT::SearchSortIdx(sortIdx, INTERFACE::GetKey(_data), m_useNum))
		{
			pItr = BASE::PushBack();
			if (pItr)
			{
				INTERFACE::Copy(GetDataFromItr(pItr), _data);
				SORT::PushKey(sortIdx, INTERFACE::GetKey(_data), BASE::GetIdx(pItr), m_useNum - 1);
			}
		}
		// あれば返す
		else
		{
			pItr = BASE::GetItr(SORT::GetIdx(sortIdx));
		}
	}
	return pItr;
}


//-------------------------------------------------------------------------------------------------
///@brief	削除
SORT_IDX_LIST_TEMPLATE_HEAD
b8	SORT_IDX_LIST_CLASS_HEAD::PopFront()
{
	ITR* pItr = GetBgn();
	if (pItr)
	{
		const DATA& data = GetDataFromItr(pItr);
		SORT::Pop(INTERFACE::GetKey(data), BASE::GetIdx(pItr), m_useNum);
		return BASE::PopFront();
	}
	return false;
}

SORT_IDX_LIST_TEMPLATE_HEAD
b8	SORT_IDX_LIST_CLASS_HEAD::PopBack()
{
	ITR* pItr = GetEnd();
	if (pItr)
	{
		const DATA& data = GetDataFromItr(pItr);
		SORT::Pop(INTERFACE::GetKey(data), BASE::GetIdx(pItr), m_useNum);
		return BASE::PopBack();
	}
	return false;
}


//-------------------------------------------------------------------------------------------------
///@brief	途中追加
SORT_IDX_LIST_TEMPLATE_HEAD
typename SORT_IDX_LIST_CLASS_HEAD::ITR* SORT_IDX_LIST_CLASS_HEAD::InsertFront(const KEY& _key, ITR* _pItr)
{
	ITR* pItr = NULL;
	if (INTERFACE::IsEnableSameName())
	{
		pItr = BASE::InsertFront(_pItr);
		if (pItr)
		{
			DATA& rData = GetDataFromItr(pItr);
			INTERFACE::Clear(rData);
			INTERFACE::SetKey(rData, _key);
			SORT::PushFront(_key, BASE::GetIdx(pItr), m_useNum - 1);
		}
	}
	else
	{
		u16 sortIdx;
		// ない場合のみ追加
		if (!SORT::SearchSortIdx(sortIdx, _key, m_useNum))
		{
			pItr = BASE::InsertFront(_pItr);
			if (pItr)
			{
				DATA& rData = GetDataFromItr(pItr);
				INTERFACE::Clear(rData);
				INTERFACE::SetKey(rData, _key);
				SORT::PushKey(sortIdx, _key, BASE::GetIdx(pItr), m_useNum - 1);
			}
		}
		// あれば返す
		else
		{
			pItr = BASE::GetItr(SORT::GetIdx(sortIdx));
		}
	}
	return pItr;
}
SORT_IDX_LIST_TEMPLATE_HEAD
typename SORT_IDX_LIST_CLASS_HEAD::ITR*	SORT_IDX_LIST_CLASS_HEAD::InsertFront(const DATA& _data, ITR* _pItr)
{
	ITR* pItr = NULL;
	if (INTERFACE::IsEnableSameName())
	{
		pItr = BASE::InsertFront(_pItr);
		if (pItr)
		{
			INTERFACE::Copy(GetDataFromItr(pItr), _data);
			SORT::PushFront(INTERFACE::GetKey(_data), BASE::GetIdx(pItr), m_useNum - 1);
		}
	}
	else
	{
		u16 sortIdx;
		// ない場合のみ追加
		if (!SORT::SearchSortIdx(sortIdx, INTERFACE::GetKey(_data), m_useNum))
		{
			pItr = BASE::InsertFront(_pItr);
			if (pItr)
			{
				INTERFACE::Copy(GetDataFromItr(pItr), _data);
				SORT::PushKey(sortIdx, INTERFACE::GetKey(_data), BASE::GetIdx(pItr), m_useNum - 1);
			}
		}
		// あれば返す
		else
		{
			pItr = BASE::GetItr(SORT::GetIdx(sortIdx));
		}
	}
	return pItr;
}

SORT_IDX_LIST_TEMPLATE_HEAD
typename SORT_IDX_LIST_CLASS_HEAD::ITR* SORT_IDX_LIST_CLASS_HEAD::InsertBack(const KEY& _key, ITR* _pItr)
{
	ITR* pItr = NULL;
	if (INTERFACE::IsEnableSameName())
	{
		pItr = BASE::InsertBack(_pItr);
		if (pItr)
		{
			DATA& rData = GetDataFromItr(pItr);
			INTERFACE::Clear(rData);
			INTERFACE::SetKey(rData, _key);
			SORT::PushBack(_key, BASE::GetIdx(pItr), m_useNum - 1);
		}
	}
	else
	{
		u16 sortIdx;
		// ない場合のみ追加
		if (!SORT::SearchSortIdx(sortIdx, _key, m_useNum))
		{
			pItr = BASE::InsertBack(_pItr);
			if (pItr)
			{
				DATA& rData = GetDataFromItr(pItr);
				INTERFACE::Clear(rData);
				INTERFACE::SetKey(rData, _key);
				SORT::PushKey(sortIdx, _key, BASE::GetIdx(pItr), m_useNum - 1);
			}
		}
		// あれば返す
		else
		{
			pItr = BASE::GetItr(SORT::GetIdx(sortIdx));
		}
	}
	return pItr;
}
SORT_IDX_LIST_TEMPLATE_HEAD
typename SORT_IDX_LIST_CLASS_HEAD::ITR*	SORT_IDX_LIST_CLASS_HEAD::InsertBack(const DATA& _data, ITR* _pItr)
{
	ITR* pItr = NULL;
	if (INTERFACE::IsEnableSameName())
	{
		pItr = BASE::InsertBack(_pItr);
		if (pItr)
		{
			INTERFACE::Copy(GetDataFromItr(pItr), _data);
			SORT::PushBack(INTERFACE::GetKey(_data), BASE::GetIdx(pItr), m_useNum - 1);
		}
	}
	else
	{
		u16 sortIdx;
		// ない場合のみ追加
		if (!SORT::SearchSortIdx(sortIdx, INTERFACE::GetKey(_data), m_useNum))
		{
			pItr = BASE::InsertBack(_pItr);
			if (pItr)
			{
				INTERFACE::Copy(GetDataFromItr(pItr), _data);
				SORT::PushKey(sortIdx, INTERFACE::GetKey(_data), BASE::GetIdx(pItr), m_useNum - 1);
			}
		}
		// あれば返す
		else
		{
			pItr = BASE::GetItr(SORT::GetIdx(sortIdx));
		}
	}
	return pItr;
}


//-------------------------------------------------------------------------------------------------
///@brief	途中削除
SORT_IDX_LIST_TEMPLATE_HEAD
b8	SORT_IDX_LIST_CLASS_HEAD::Erase(ITR* _pItr)
{
	if (BASE::CheckItr(_pItr))
	{
		const DATA& data = GetDataFromItr(_pItr);
		if (SORT::Pop(INTERFACE::GetKey(data), BASE::GetIdx(_pItr), m_useNum))
		{
			return BASE::Erase(_pItr);
		}
	}
	return false;
}


POISON_END


#endif	// __IDX_SORT_LIST_INL__

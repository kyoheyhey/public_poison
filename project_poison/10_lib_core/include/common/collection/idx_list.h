﻿//#pragma once
#ifndef __IDX_LIST_H__
#define __IDX_LIST_H__


#include "memory/memory.h"

POISON_BGN

//*************************************************************************************************
//@brief	ベースインデックスリスト
//*************************************************************************************************
class CBaseIdxList : public CDontCopy
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	無効インデックス
	static const u16	IDX_INVALID = 0xffff;

public:
	//*************************************************************************************************
	//@brief	イテレータ
	//*************************************************************************************************
	struct ITR
	{
	public:
		friend class CBaseIdxList;

	private:
		//-------------------------------------------------------------------------------------------------
		//@brief	コンストラクタ・デストラクタ（外部で使用不可）
		ITR() {}
		~ITR() {}
		//-------------------------------------------------------------------------------------------------
		//@brief	コピーコンストラクタ（外部で使用不可）
		ITR(const ITR& _o) { *this = _o; }
		//-------------------------------------------------------------------------------------------------
		//@brief	コピーオペレーター（外部で使用不可）
		const ITR& operator=(const ITR& _o)
		{
			idx = _o.idx;
			prevIdx = _o.prevIdx;
			nextIdx = _o.nextIdx;
			return *this;
		}
		//-------------------------------------------------------------------------------------------------
		//@brief	セットアップ（外部で使用不可）
		inline void	Setup(u16 _idx)
		{
			idx = _idx;
			prevIdx = IDX_INVALID;
			nextIdx = IDX_INVALID;
		}
		//-------------------------------------------------------------------------------------------------
		//@brief	クリア（外部で使用不可）
		inline void	Clear()
		{
			idx = IDX_INVALID;
			prevIdx = IDX_INVALID;
			nextIdx = IDX_INVALID;
		}

	private:
		u16	idx = IDX_INVALID;		///< インデックス
		u16	prevIdx = IDX_INVALID;	///< 前方接続インデックス
		u16	nextIdx = IDX_INVALID;	///< 後方接続インデックス
	};

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ・デストラクタ
	CBaseIdxList();
	virtual ~CBaseIdxList();

	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	virtual b8		Initialize(IAllocator* _pAllocator, u16 _num);
	virtual b8		Initialize(MEM_HANDLE _memHdl, u16 _num);

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	virtual void	Finalize();

	//-------------------------------------------------------------------------------------------------
	//@brief	クリア
	virtual void	Clear();

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	使用数取得
	inline u16			GetUseNum() const { return m_useNum; }

	//-------------------------------------------------------------------------------------------------
	//@brief	最大数取得
	inline u16			GetMaxNum() const { return m_maxNum; }

	//-------------------------------------------------------------------------------------------------
	//@brief	先頭イテレータ取得
	inline ITR*			GetBgn()		{ return m_pBgnItr; }
	inline const ITR*	GetBgn() const	{ return CCast<CBaseIdxList*>(this)->GetBgn(); }

	//-------------------------------------------------------------------------------------------------
	//@brief	末尾イテレータ取得
	inline ITR*			GetEnd()		{ return m_pEndItr; }
	inline const ITR*	GetEnd() const	{ return CCast<CBaseIdxList*>(this)->GetEnd(); }

	//-------------------------------------------------------------------------------------------------
	//@brief	インデックスからイテレータ取得
	inline ITR*			GetItr(u16 _idx)		{ return CheckIdx(_idx) ? m_pItrBuf + _idx : NULL; }
	inline const ITR*	GetItr(u16 _idx) const	{ return CCast<CBaseIdxList*>(this)->GetItr(_idx); }

	//-------------------------------------------------------------------------------------------------
	//@brief	イテレータからインデックス取得
	inline u16			GetIdx(const ITR* _pItr) const { libAssert(CheckItr(_pItr)); return _pItr->idx; }

	//-------------------------------------------------------------------------------------------------
	//@brief	前方イテレータ取得
	inline ITR*			GetPrev(const ITR* _pItr)		{ libAssert(CheckItr(_pItr)); return GetItr(_pItr->prevIdx); }
	inline const ITR*	GetPrev(const ITR* _pItr) const	{ return CCast<CBaseIdxList*>(this)->GetItr(_pItr->prevIdx); }

	//-------------------------------------------------------------------------------------------------
	//@brief	後方イテレータ取得
	inline ITR*			GetNext(const ITR* _pItr)		{ libAssert(CheckItr(_pItr)); return GetItr(_pItr->nextIdx); }
	inline const ITR*	GetNext(const ITR* _pItr) const	{ return CCast<CBaseIdxList*>(this)->GetItr(_pItr->nextIdx); }

	//-------------------------------------------------------------------------------------------------
	//@brief	インデックスからデータバッファ取得
	inline void*		GetDataBuffer(u16 _idx)			{ libAssert(CheckIdx(_idx)); return PCast<u8*>(GetDataBuffer()) + (GetDataSize() * _idx); }
	inline const void*	GetDataBuffer(u16 _idx) const	{ CCast<CBaseIdxList*>(this)->GetDataBuffer(_idx); }

	//-------------------------------------------------------------------------------------------------
	//@brief	イテレータからデータバッファ取得
	inline void*		GetDataBuffer(const ITR* _pItr)			{ return GetDataBuffer(GetIdx(_pItr)); }
	inline const void*	GetDataBuffer(const ITR* _pItr) const	{ CCast<CBaseIdxList*>(this)->GetDataBuffer(_pItr); }

	//-------------------------------------------------------------------------------------------------
	//@brief	イテレータスワップ
	void	Swap(ITR* _pItrA, ITR* _pItrB);

protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	要素サイズ取得
	virtual u32		GetDataSize() const = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	データバッファ取得
	inline void*	GetDataBuffer() { return m_pDataBuf; }

	//-------------------------------------------------------------------------------------------------
	//@brief	インデックスチェック
	inline b8		CheckIdx(u16 _idx) const { return (_idx < m_maxNum); }

	//-------------------------------------------------------------------------------------------------
	//@brief	フリーインデックス取得
	inline u16		PushFreeIdx()
	{
		if (m_useNum < m_maxNum)
		{
			u16 freeIdx = m_pFreeIdxBuf[m_useNum];
			m_pFreeIdxBuf[m_useNum] = IDX_INVALID;
			++m_useNum;
			return freeIdx;
		}
		return IDX_INVALID;
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	フリーインデックス戻し
	inline void		PopFreeIdx(u16 _idx)
	{
		libAssert(m_useNum > 0);
		--m_useNum;
		m_pFreeIdxBuf[m_useNum] = _idx;
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	イテレータチェック
	inline b8		CheckItr(const ITR* _pItr) const { return (_pItr && _pItr == GetItr(_pItr->idx)); }

	//-------------------------------------------------------------------------------------------------
	//@brief	イテレータセットアップ
	inline void		SetupItr(ITR* _pItr, u16 _idx) const
	{
		_pItr->Setup(_idx);
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	イテレータ連結
	inline void		ConnectItr(ITR* _pPrevItr, ITR* _pNextItr) const
	{
		_pPrevItr->nextIdx = GetIdx(_pNextItr);
		_pNextItr->prevIdx = GetIdx(_pPrevItr);
	}

protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	追加
	//@return	追加されたイテレータ
	ITR*	PushBack();
	ITR*	PushFront();

	//-------------------------------------------------------------------------------------------------
	//@brief	削除
	//@return	削除出来たか
	b8		PopBack();
	b8		PopFront();

	//-------------------------------------------------------------------------------------------------
	//@brief	途中追加
	//@return	データインデックス
	ITR*	InsertBack(ITR* _pItr);
	ITR*	InsertFront(ITR* _pItr);

	//-------------------------------------------------------------------------------------------------
	//@brief	途中削除
	//@return	追加されたイテレータ
	b8		Erase(ITR* _pItr);

	//-------------------------------------------------------------------------------------------------
	//@brief	データコピー
	void	CopyData(const CBaseIdxList& _idxList);

	//-------------------------------------------------------------------------------------------------
	//@brief	イテレータコピー
	void	CopyItr(const CBaseIdxList& _idxList);

	//-------------------------------------------------------------------------------------------------
	//@brief	フリーインデックスコピー
	void	CopyFreeIdx(const CBaseIdxList& _idxList);

protected:
	void*		m_pDataBuf = NULL;			///< データバッファ
	ITR*		m_pItrBuf = NULL;			///< イテレータバッファ
	u16*		m_pFreeIdxBuf = NULL;		///< フリーインデックスバッファ
	ITR*		m_pBgnItr = NULL;			///< 先頭イテレータ
	ITR*		m_pEndItr = NULL;			///< 末尾イテレータ
	IAllocator* m_pAllocator = NULL;		///< アロケータ
	u16			m_useNum = 0;				///< 使用数
	u16			m_maxNum = 0;				///< 最大数
};


//*************************************************************************************************
//@brief	データインターフェース
//*************************************************************************************************
template <typename __DATA>
class CDataIF
{
private:
	//-------------------------------------------------------------------------------------------------
	//@brief	型定義
	typedef	__DATA		DATA;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	データコピー
	static inline	void	Copy(DATA& _dst, const DATA& _src)	{ _dst = _src; }
	//-------------------------------------------------------------------------------------------------
	//@brief	未使用領域の初期化
	static inline	void	Clear(DATA& _data)					{ new(&_data) DATA(); }
};

template <typename __DATA>
class CDataPtrIF
{
private:
	//-------------------------------------------------------------------------------------------------
	//@brief	型定義
	typedef	__DATA		DATA;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	データコピー
	static inline	void	Copy(DATA& _dst, const DATA& _src)	{ _dst = _src; }
	//-------------------------------------------------------------------------------------------------
	//@brief	未使用領域の初期化
	static inline	void	Clear(DATA& _data)					{ _data = NULL; }
};


//*************************************************************************************************
//@brief	インデックスリスト
//*************************************************************************************************
template <typename __DATA, typename __INTERFACE = CDataIF<__DATA>>
class CIdxList : public CBaseIdxList
{
private:
	//-------------------------------------------------------------------------------------------------
	//@brief	型定義
	typedef __DATA			DATA;
	typedef __INTERFACE		INTERFACE;
	typedef CBaseIdxList	BASE;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ・デストラクタ
	CIdxList() {}
	~CIdxList() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	イテレータからデータ取得
	inline DATA&		GetDataFromItr(const ITR* _pItr);
	inline const DATA&	GetDataFromItr(const ITR* _pItr) const { return CCast<CIdxList*>(this)->GetDataFromItr(_pItr); }

	//-------------------------------------------------------------------------------------------------
	//@brief	インデックスからデータ取得
	inline DATA&		GetDataFromIdx(u16 _idx);
	inline const DATA&	GetDataFromIdx(u16 _idx) const { return CCast<CIdxList*>(this)->GetDataFromIdx(_idx); }

	//-------------------------------------------------------------------------------------------------
	//@brief	追加
	//@return	追加されたイテレータ
	ITR*	PushBack(const DATA& _data);
	ITR*	PushFront(const DATA& _data);

	//-------------------------------------------------------------------------------------------------
	//@brief	削除
	//@return	削除出来たか
	b8		PopBack();
	b8		PopFront();

	//-------------------------------------------------------------------------------------------------
	//@brief	途中追加
	//@return	追加されたイテレータ
	ITR*	InsertBack(const DATA& _data, ITR* _pItr);
	ITR*	InsertFront(const DATA& _data, ITR* _pItr);

	//-------------------------------------------------------------------------------------------------
	//@brief	途中削除
	//@return	削除出来たか
	b8		Erase(ITR* _pItr);

private:
	//-------------------------------------------------------------------------------------------------
	//@brief	要素サイズ取得
	virtual u32	GetDataSize() const { return sizeof(DATA); }

	//-------------------------------------------------------------------------------------------------
	//@brief	先頭データバッファ取得
	inline DATA*		GetTopData()		{ return RCast<DATA*>(GetDataBuffer()); }
	inline const DATA*	GetTopData() const	{ return CCast<CIdxList*>(this)->GetDataBuffer(); }
};


POISON_END

#include "idx_list.inl"


#endif	// __IDX_LIST_H__

﻿//#pragma once
#ifndef __LINK_LIST_INL__
#define __LINK_LIST_INL__


POISON_BGN

///-------------------------------------------------------------------------------------------------
/// テンプレート宣言部分の省略
#define LINK_LIST_TEMPLATE_HEAD	template <typename __DATA>
#define LINK_LIST_CLASS_HEAD	CLinkList< __DATA >

///*************************************************************************************************
/// ノード構造体
///*************************************************************************************************
LINK_LIST_TEMPLATE_HEAD
LINK_LIST_CLASS_HEAD::CNode::CNode()
	: m_prev(NULL)
	, m_next(NULL)
{}

LINK_LIST_TEMPLATE_HEAD
LINK_LIST_CLASS_HEAD::CNode::CNode(const DATA& _data)
	: m_prev(NULL)
	, m_next(NULL)
	, m_data(_data)
{}

LINK_LIST_TEMPLATE_HEAD
LINK_LIST_CLASS_HEAD::CNode::~CNode()
{}

///*************************************************************************************************
///@brief	ノードイテレータークラス
///*************************************************************************************************
LINK_LIST_TEMPLATE_HEAD
LINK_LIST_CLASS_HEAD::CIterator::CIterator()
	: m_pNode(NULL)
{}

LINK_LIST_TEMPLATE_HEAD
LINK_LIST_CLASS_HEAD::CIterator::CIterator(CNode* _pNode)
	: m_pNode(_pNode)
{}

LINK_LIST_TEMPLATE_HEAD
LINK_LIST_CLASS_HEAD::CIterator::~CIterator()
{}

///-------------------------------------------------------------------------------------------------
///@brief	参照
LINK_LIST_TEMPLATE_HEAD
inline typename LINK_LIST_CLASS_HEAD::DATA&	LINK_LIST_CLASS_HEAD::CIterator::operator * ()
{
	libAssert(m_pNode);
	return m_pNode->Get();
}

///-------------------------------------------------------------------------------------------------
///@brief	比較
LINK_LIST_TEMPLATE_HEAD
inline b8	LINK_LIST_CLASS_HEAD::CIterator::operator == (CIterator& _itr)
{
	return (m_pNode == _itr.m_pNode);
}
LINK_LIST_TEMPLATE_HEAD
inline b8	LINK_LIST_CLASS_HEAD::CIterator::operator != (CIterator& _itr)
{
	return (m_pNode != _itr.m_pNode);
}

///-------------------------------------------------------------------------------------------------
///@brief	前置
LINK_LIST_TEMPLATE_HEAD
inline typename LINK_LIST_CLASS_HEAD::CIterator&	LINK_LIST_CLASS_HEAD::CIterator::operator ++ ()
{
	libAssert(m_pNode);
	if (m_pNode->GetNext())
	{
		m_pNode = m_pNode->GetNext();
	}
	return *this;
}
LINK_LIST_TEMPLATE_HEAD
inline typename LINK_LIST_CLASS_HEAD::CIterator&	LINK_LIST_CLASS_HEAD::CIterator::operator += (u32 _num)
{
	libAssert(m_pNode);
	while (_num > 0)
	{
		if (m_pNode->GetNext())
		{
			m_pNode = m_pNode->GetNext();
		}
		--_num;
	}
	return *this;
}

///-------------------------------------------------------------------------------------------------
///@brief	後置
LINK_LIST_TEMPLATE_HEAD
inline typename LINK_LIST_CLASS_HEAD::CIterator&	LINK_LIST_CLASS_HEAD::CIterator::operator -- ()
{
	libAssert(m_pNode);
	if (m_pNode->GetPrev())
	{
		m_pNode = m_pNode->GetPrev();
	}
	return *this;
}
LINK_LIST_TEMPLATE_HEAD
inline typename LINK_LIST_CLASS_HEAD::CIterator&	LINK_LIST_CLASS_HEAD::CIterator::operator -= (u32 _num)
{
	libAssert(m_pNode);
	while (_num > 0)
	{
		if (m_pNode->GetPrev())
		{
			m_pNode = m_pNode->GetPrev();
		}
		--_num;
	}
	return *this;
}

///*************************************************************************************************
///@brief	リンクリストクラス
///*************************************************************************************************
LINK_LIST_TEMPLATE_HEAD
LINK_LIST_CLASS_HEAD::CLinkList()
	: m_pAllocator(NULL)
	, m_num(0)
{
	m_top.SetNext(&m_end);
	m_end.SetPrev(&m_top);
}

LINK_LIST_TEMPLATE_HEAD
LINK_LIST_CLASS_HEAD::~CLinkList()
{}

///-------------------------------------------------------------------------------------------------
///@brief	前方イテレータ取得
LINK_LIST_TEMPLATE_HEAD
inline typename LINK_LIST_CLASS_HEAD::CIterator	LINK_LIST_CLASS_HEAD::Begin() const
{
	//return CIterator(m_top.GetNext() == &m_end ? NULL : m_top.GetNext());
	return CIterator(m_top.GetNext());
}

///-------------------------------------------------------------------------------------------------
///@brief	後方イテレータ取得
LINK_LIST_TEMPLATE_HEAD
inline typename LINK_LIST_CLASS_HEAD::CIterator	LINK_LIST_CLASS_HEAD::End() const
{
	return CIterator(m_end.GetPrev()->GetNext());
}

///-------------------------------------------------------------------------------------------------
///@brief	逆方向前方イテレータ取得
///@note		逆方向に辿るときは--を使うこと
LINK_LIST_TEMPLATE_HEAD
inline typename LINK_LIST_CLASS_HEAD::CIterator	LINK_LIST_CLASS_HEAD::ReBegin() const
{
	//return CIterator(m_end.GetPrev() == &m_top ? NULL : m_end.GetPrev());
	return CIterator(m_end.GetPrev());
}

///-------------------------------------------------------------------------------------------------
///@brief	逆方向後方イテレータ取得
///@note		逆方向に辿るときは--を使うこと
LINK_LIST_TEMPLATE_HEAD
inline typename LINK_LIST_CLASS_HEAD::CIterator	LINK_LIST_CLASS_HEAD::ReEnd() const
{
	return CIterator(m_top.GetNext()->GetPrev());
}

//-------------------------------------------------------------------------------------------------
//@brief	初期化
LINK_LIST_TEMPLATE_HEAD
b8		LINK_LIST_CLASS_HEAD::Initialize(IAllocator* _pAllocator)
{
	m_num = 0;
	m_pAllocator = _pAllocator;
	return (m_pAllocator != NULL);
}
LINK_LIST_TEMPLATE_HEAD
b8		LINK_LIST_CLASS_HEAD::Initialize(MEM_HANDLE _memHdl)
{
	return Initialize(MEM_ALLOCATOR(_memHdl));
}

//-------------------------------------------------------------------------------------------------
//@brief	終了
LINK_LIST_TEMPLATE_HEAD
void	LINK_LIST_CLASS_HEAD::Finalize()
{
	while (m_num > 0)
	{
		PopBack();
	}
}

///-------------------------------------------------------------------------------------------------
///@brief	ノード追加（後方）
LINK_LIST_TEMPLATE_HEAD
b8		LINK_LIST_CLASS_HEAD::PushBack(const DATA& _data)
{
	CNode* newNode = NewNode(_data);
	if (newNode == NULL)	return false;
	if (m_num > 0)
	{
		CNode*  lastNode = m_end.GetPrev();
		lastNode->SetNext(newNode);
		newNode->SetPrev(lastNode);
		newNode->SetNext(&m_end);
		m_end.SetPrev(newNode);
	}
	else
	{
		m_top.SetNext(newNode);
		newNode->SetPrev(&m_top);
		newNode->SetNext(&m_end);
		m_end.SetPrev(newNode);
	}
	++m_num;
	return true;
}

///-------------------------------------------------------------------------------------------------
///@brief	ノード追加（前方）
LINK_LIST_TEMPLATE_HEAD
b8		LINK_LIST_CLASS_HEAD::PushFront(const DATA& _data)
{
	CNode* newNode = NewNode(_data);
	if (newNode == NULL)	return false;
	if (m_num > 0)
	{
		CNode*  firstNode = m_top.GetNext();
		m_top.SetNext(newNode);
		newNode->SetPrev(&m_top);
		newNode->SetNext(firstNode);
		firstNode->SetPrev(newNode);
	}
	else
	{
		m_top.SetNext(newNode);
		newNode->SetPrev(&m_top);
		newNode->SetNext(&m_end);
		m_end.SetPrev(newNode);
	}
	++m_num;
	return true;
}

///-------------------------------------------------------------------------------------------------
///@brief	ノード追加（途中）（後方）
LINK_LIST_TEMPLATE_HEAD
b8		LINK_LIST_CLASS_HEAD::InsertBack(CIterator& _itr, const DATA& _data)
{
	libAssert(_itr.m_pNode);
	CNode* node = _itr.m_pNode;
	CNode* back = node->GetNext();
	if (node == &m_top || node == &m_end)
	{
		return false;
	}
	CNode* newNode = NewNode(_data);
	if (newNode == NULL)	return false;
	node->SetNext(newNode);
	newNode->SetPrev(node);
	newNode->SetNext(back);
	back->SetPrev(newNode);
	++m_num;
	return true;
}

///-------------------------------------------------------------------------------------------------
///@brief	ノード追加（途中）（前方）
LINK_LIST_TEMPLATE_HEAD
b8		LINK_LIST_CLASS_HEAD::InsertFront(CIterator& _itr, const DATA& _data)
{
	libAssert(_itr.m_pNode);
	CNode* node = _itr.m_pNode;
	CNode* front = node->GetPrev();
	if (node == &m_top || node == &m_end)
	{
		return false;
	}
	CNode* newNode = NewNode(_data);
	if (newNode == NULL)    return false;
	front->SetNext(newNode);
	newNode->SetPrev(front);
	newNode->SetNext(node);
	node->SetPrev(newNode);
	++m_num;
	return true;
}

///-------------------------------------------------------------------------------------------------
///@brief	ノード削除（後方）
LINK_LIST_TEMPLATE_HEAD
void	LINK_LIST_CLASS_HEAD::PopBack()
{
	if (m_num > 0)
	{
		CNode* oldLast = m_end.GetPrev();
		CNode* newLast = oldLast->GetPrev();
		newLast->SetNext(&m_end);
		m_end.SetPrev(newLast);
		DeleteNode(oldLast);
		--m_num;
	}
}

///-------------------------------------------------------------------------------------------------
///@brief	ノード削除（前方）
LINK_LIST_TEMPLATE_HEAD
void	LINK_LIST_CLASS_HEAD::PopFront()
{
	if (m_num > 0)
	{
		CNode* oldFirst = m_top.GetNext();
		CNode* newFirst = oldFirst->GetNext();
		m_top.SetNext(newFirst);
		newFirst->SetPrev(&m_top);
		DeleteNode(oldFirst);
		--m_num;
	}
}

///-------------------------------------------------------------------------------------------------
///@brief	ノード削除（途中）
LINK_LIST_TEMPLATE_HEAD
void	LINK_LIST_CLASS_HEAD::Erase(CIterator& _itr)
{
	libAssert(_itr.m_pNode);
	if (m_num > 0)
	{
		CNode* node = _itr.m_pNode;
		CNode* front = node->GetPrev();
		CNode* back = node->GetNext();
		front->SetNext(back);
		back->SetPrev(front);
		DeleteNode(node);
		--m_num;
	}
}

///-------------------------------------------------------------------------------------------------
///@brief	ノード交換
LINK_LIST_TEMPLATE_HEAD
inline b8	LINK_LIST_CLASS_HEAD::Swap(CIterator& _itrA, CIterator& _itrB)
{
	libAssert(_itrA.m_pNode);
	libAssert(_itrB.m_pNode);
	if (m_num <= 1 || _itrA == _itrB)	return false;

	CNode* aNode = _itrA.m_pNode;
	CNode* aPrevBufNode = aNode->GetPrev();
	CNode* aNextBufNode = aNode->GetNext();
	CNode* bNode = _itrB.m_pNode;
	CNode* bPrevBufNode = bNode->GetPrev();
	CNode* bNextBufNode = bNode->GetNext();

	// Ａノード設定
	bPrevBufNode->SetNext(aNode);
	aNode->SetPrev(bPrevBufNode);
	aNode->SetNext(bNextBufNode);
	bNextBufNode->SetPrev(aNode);

	// Ｂノード設定
	aPrevBufNode->SetNext(bNode);
	bNode->SetPrev(aPrevBufNode);
	bNode->SetNext(aNextBufNode);
	aNextBufNode->SetPrev(bNode);

	// イテレータ戻し
	CIterator buf = _itrA;
	_itrA = _itrB;
	_itrB = buf;
	return true;
}

///-------------------------------------------------------------------------------------------------
///@brief	ノード生成
LINK_LIST_TEMPLATE_HEAD
typename LINK_LIST_CLASS_HEAD::CNode*	LINK_LIST_CLASS_HEAD::NewNode(const DATA& _data)
{
	return MEM_NEW(m_pAllocator) CNode(_data);
	//void* pPtr = MEM_MALLOC(m_pAllocator, sizeof(CNode));
	//return new(pPtr) CNode(_data);
}

///-------------------------------------------------------------------------------------------------
///@brief	ノード破棄
LINK_LIST_TEMPLATE_HEAD
void	LINK_LIST_CLASS_HEAD::DeleteNode(CNode* _node)
{
	MEM_SAFE_DELETE(m_pAllocator, _node);
}

POISON_END

#endif	// __LINK_LIST_INL__

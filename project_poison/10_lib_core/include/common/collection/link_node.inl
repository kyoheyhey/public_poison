﻿//#pragma once
#ifndef __LINK_NODE_INL__
#define __LINK_NODE_INL__


POISON_BGN

///-------------------------------------------------------------------------------------------------
/// テンプレート宣言部分の省略
#define LINK_NODE_TEMPLATE_HEAD	template <class __NODE, class __BASE>
#define LINK_NODE_CLASS_HEAD	CLinkNode< __NODE, __BASE >


///-------------------------------------------------------------------------------------------------
/// 末尾追加
LINK_NODE_TEMPLATE_HEAD
void	LINK_NODE_CLASS_HEAD::PushBack(NODE* _pNode)
{
	if (m_pEnd) {
		m_pEnd->SetNext(_pNode);
		_pNode->SetPrev(m_pEnd);
	}
	else {
		m_pTop = _pNode;
		_pNode->SetPrev(NULL);
	}
	m_pEnd = _pNode;
	_pNode->SetNext(NULL);
	++m_num;
}

///-------------------------------------------------------------------------------------------------
/// 先頭追加
LINK_NODE_TEMPLATE_HEAD
void	LINK_NODE_CLASS_HEAD::PushFront(NODE* _pNode)
{
	if (m_pTop) {
		m_pTop->SetPrev(_pNode);
		_pNode->SetNext(m_pTop);
	}
	else {
		_pNode->SetNext(NULL);
		m_pEnd = _pNode;
	}
	m_pTop = _pNode;
	_pNode->SetPrev(NULL);
	++m_num;
}

///-------------------------------------------------------------------------------------------------
/// 挿入
LINK_NODE_TEMPLATE_HEAD
void	LINK_NODE_CLASS_HEAD::Insert(NODE* _pPrev, NODE* _pNode)
{
	libAssert(_pPrev && _pNode);
	NODE* pNext = _pPrev->GetNext();
	libAssert(pNext != _pNode);

	_pPrev->SetNext(_pNode);
	_pNode->SetPrev(_pPrev);
	_pNode->SetNext(pNext);
	if (pNext) {
		pNext->SetPrev(_pNode);
	}
	else {
		m_pEnd = _pNode;
	}
	++m_num;
}

///-------------------------------------------------------------------------------------------------
/// 削除
LINK_NODE_TEMPLATE_HEAD
void	LINK_NODE_CLASS_HEAD::Pop(NODE* _pNode)
{
	NODE* pPrev = _pNode->GetPrev();
	NODE* pNext = _pNode->GetNext();

	// 次がいたら前とつなげる、いなかったら最後尾を更新
	if (pNext) {
		pNext->SetPrev(pPrev);
	}
	else {
		libAssert(m_pEnd == _pNode);
		m_pEnd = pPrev;
	}

	if (pPrev) {
		pPrev->SetNext(pNext);
	}
	else {
		libAssert(m_pTop == _pNode);
		m_pTop = pNext;
	}
	--m_num;
}


POISON_END

#endif	// __LINK_NODE_INL__

﻿//#pragma once
#ifndef __IDX_SORT_LIST_H__
#define __IDX_SORT_LIST_H__


#include "idx_list.h"

POISON_BGN


//*************************************************************************************************
//@brief	ソートデータインターフェース
//*************************************************************************************************
template <typename __DATA, typename __KEY, b8 IS_ENABLE_SAME_NAME = true>
class CSortDataIF
{
private:
	//-------------------------------------------------------------------------------------------------
	//@brief	型定義
	typedef	__DATA		DATA;
	typedef __KEY		KEY;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	検索用キー取得
	static inline	KEY		GetKey(const DATA& _data)			{ return _data.GetNameCrc(); }
	//-------------------------------------------------------------------------------------------------
	//@brief	検索用キーセット
	static inline	void	SetKey(DATA& _data, KEY _uName)		{ _data.SetNameCrc(_uName); }
	//-------------------------------------------------------------------------------------------------
	//@brief	データコピー
	static inline	void	Copy(DATA& _dst, const DATA& _src)	{ _dst = _src; }
	//-------------------------------------------------------------------------------------------------
	//@brief	未使用領域の初期化
	static inline	void	Clear(DATA& _data)					{ new(&_data) DATA(); }
	//-------------------------------------------------------------------------------------------------
	//@brief	同名登録の許可
	static inline b8		IsEnableSameName()					{ return IS_ENABLE_SAME_NAME; }
};

template <typename __DATA, typename __KEY, b8 IS_ENABLE_SAME_NAME = true>
class CSortDataPtrIF
{
private:
	//-------------------------------------------------------------------------------------------------
	//@brief	型定義
	typedef	__DATA		DATA;
	typedef __KEY		KEY;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	検索用キー取得
	static inline	KEY		GetKey(const DATA& _data)			{ return _data->GetNameCrc(); }
	//-------------------------------------------------------------------------------------------------
	//@brief	検索用キーセット
	static inline	void	SetKey(DATA& _data, KEY _uName)		{ _data->SetNameCrc(_uName); }
	//-------------------------------------------------------------------------------------------------
	//@brief	データコピー
	static inline	void	Copy(DATA& _dst, const DATA& _src)	{ _dst = _src; }
	//-------------------------------------------------------------------------------------------------
	//@brief	未使用領域の初期化
	static inline	void	Clear(DATA& _data)					{ _data = NULL; }
	//-------------------------------------------------------------------------------------------------
	//@brief	同名登録の許可
	static inline b8		IsEnableSameName()					{ return IS_ENABLE_SAME_NAME; }
};



//*************************************************************************************************
//@brief	キーソート
//*************************************************************************************************
template <typename __KEY>
class CKeySort
{
private:
	//-------------------------------------------------------------------------------------------------
	//@brief	型定義
	typedef __KEY		KEY;

protected:
	//*************************************************************************************************
	//@brief	キーテーブル
	struct KEY_TABLE
	{
		KEY	key;	///< キー値
		u16 idx;	///< インデックス
	};

public:
	CKeySort() {}
	~CKeySort() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	b8		Initialize(IAllocator* _pAllocator, u16 _maxNum);

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	void	Finalize(IAllocator* _pAllocator);

	//-------------------------------------------------------------------------------------------------
	//@brief	クリア
	void	Clear(u16 _maxNum);

protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	キー追加
	//@param[in]	_sortIdx	ソートインデックス値
	//@param[in]	_key		キー値
	//@param[in]	_idx		インデックス値
	//@param[in]	_useNum		使用数
	//@return		追加できたか
	b8		PushKey(u16 _sortIdx, const KEY& _key, u16 _idx, u16 _useNum);

	//-------------------------------------------------------------------------------------------------
	//@brief	キー追加
	//@param[in]	_key		キー値
	//@param[in]	_idx		インデックス値
	//@param[in]	_useNum		使用数
	//@return		追加できたか
	b8		PushFront(const KEY& _key, u16 _idx, u16 _useNum);
	b8		PushBack(const KEY& _key, u16 _idx, u16 _useNum);

	//-------------------------------------------------------------------------------------------------
	//@brief	キー削除
	//@param[in]	_key		キー値
	//@param[in]	_sortIdx	ソートインデックス
	//@param[in]	_idx		インデックス値
	//@param[in]	_useNum		使用数
	//@return		削除できたか
	b8		Pop(const KEY& _key, u16 _idx, u16 _useNum);
	b8		Pop(u16 _sortIdx, u16 _idx, u16 _useNum);

	//-------------------------------------------------------------------------------------------------
	//@brief	キー検索で同キーの先頭ソートインデックス取得
	//@param[out]	_retKeyIdx	検索ヒットした先頭ソートインデックスか、なければ最も近い次ソートインデックス
	//@param[in]	_key		キー値
	//@param[in]	_useNum		使用数
	//@return		検索ヒットしたか
	b8		SearchFrontSortIdx(u16& _retSortIdx, const KEY& _key, u16 _useNum);

	//-------------------------------------------------------------------------------------------------
	//@brief	キー検索で同キーの末尾ソートインデックス取得
	//@param[out]	_retKeyIdx	検索ヒットした末尾ソートインデックスか、なければ最も近い次ソートインデックス
	//@param[in]	_key		キー値
	//@param[in]	_useNum		使用数
	//@return		検索ヒットしたか
	b8		SearchBackSortIdx(u16& _retSortIdx, const KEY& _key, u16 _useNum);

	//-------------------------------------------------------------------------------------------------
	//@brief	キー検索でソートインデックス取得
	//@param[out]	_retKeyIdx	検索ヒットしたソートインデックスか、なければ最も近い次ソートインデックス
	//@param[in]	_key		キー値
	//@param[in]	_useNum		使用数
	//@param[in]	_bgnIdx		検索先頭
	//@param[in]	_endIdx		検索末尾
	//@return		検索ヒットしたか
	b8		SearchSortIdx(u16& _retSortIdx, const KEY& _key, u16 _useNum);
	b8		SearchSortIdx(u16& _retSortIdx, const KEY& _key, u16 _bgnIdx, u16 _endIdx);

	//-------------------------------------------------------------------------------------------------
	//@brief	キー取得
	inline const KEY&		GetKey(u16 _sortIdx) const { return m_pKeySortTable[_sortIdx].key; }

	//-------------------------------------------------------------------------------------------------
	//@brief	インデックス取得
	inline const u16		GetIdx(u16 _sortIdx) const { return m_pKeySortTable[_sortIdx].idx; }

	//-------------------------------------------------------------------------------------------------
	//@brief	キーソートコピー
	void	CopyKeySort(const CKeySort& _keySort, u16 _maxNum);


protected:
	KEY_TABLE*	m_pKeySortTable = NULL;		///< キーソートテーブル
};



//*************************************************************************************************
//@brief	インデックスソートリスト
//*************************************************************************************************
template <typename __DATA, typename __KEY, typename __INTERFACE = CSortDataIF<__DATA, __KEY>>
class CIdxSortList : public CBaseIdxList, public CKeySort<__KEY>
{
private:
	//-------------------------------------------------------------------------------------------------
	//@brief	型定義
	typedef __DATA			DATA;
	typedef __KEY			KEY;
	typedef __INTERFACE		INTERFACE;
	typedef CBaseIdxList	BASE;
	typedef CKeySort<__KEY>	SORT;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ・デストラクタ
	CIdxSortList() {}
	~CIdxSortList() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	virtual b8		Initialize(IAllocator* _pAllocator, u16 _num);
	virtual b8		Initialize(MEM_HANDLE _mem, u16 _num);

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	virtual void	Finalize();

	//-------------------------------------------------------------------------------------------------
	//@brief	クリア
	virtual void	Clear();

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	イテレータからデータ取得
	inline DATA&		GetDataFromItr(const ITR* _pItr);
	inline const DATA&	GetDataFromItr(const ITR* _pItr) const { return CCast<CIdxSortList*>(this)->GetDataFromItr(_pItr); }

	//-------------------------------------------------------------------------------------------------
	//@brief	インデックスからデータ取得
	inline DATA&		GetDataFromIdx(u16 _idx);
	inline const DATA&	GetDataFromIdx(u16 _idx) const { return CCast<CIdxSortList*>(this)->GetDataFromIdx(_idx); }

	//-------------------------------------------------------------------------------------------------
	//@brief	ソートインデックスからデータ取得（キーソートテーブルは前詰め）
	inline DATA&		GetDataFromSortIdx(u16 _sortIdx);
	inline const DATA&	GetDataFromSortIdx(u16 _sortIdx) const { return CCast<CIdxSortList*>(this)->GetDataFromSortIdx(_sortIdx); }
	
	//-------------------------------------------------------------------------------------------------
	//@brief	ソートインデックスからイテレータ取得（キーソートテーブルは前詰め）
	inline ITR*			GetItrFromSortIdx(u16 _sortIdx);
	inline const ITR*	GetItrFromSortIdx(u16 _sortIdx) const { return CCast<CIdxSortList*>(this)->GetItrFromSortIdx(_sortIdx); }

	//-------------------------------------------------------------------------------------------------
	//@brief	ソートインデックスからキー取得（キーソートテーブルは前詰め）
	inline const KEY&	GetKeyFromSortIdx(u16 _sortIdx) const;

	//-------------------------------------------------------------------------------------------------
	//@brief	キー検索でソートインデックス取得
	//@param[in]	_key		キー値
	u16			SearchSortIdx(const KEY& _key);
	const u16	SearchSortIdx(const KEY& _key) const { return CCast<CIdxSortList*>(this)->SearchSortIdx(_key); }

	//-------------------------------------------------------------------------------------------------
	//@brief	キー検索でイテレータ取得
	//@param[in]	_key		キー値
	ITR*		SearchItr(const KEY& _key);
	const ITR*	SearchItr(const KEY& _key) const { return CCast<CIdxSortList*>(this)->SearchItr(_key); }

	//-------------------------------------------------------------------------------------------------
	//@brief	キー検索でデータ取得
	//@param[in]	_key		キー値
	DATA*		SearchData(const KEY& _key);
	const DATA*	SearchData(const KEY& _key) const { return CCast<CIdxSortList*>(this)->SearchData(_key); }

	//-------------------------------------------------------------------------------------------------
	//@brief	追加
	//@return	追加されたイテレータ
	ITR*	PushFront(const KEY& _key);
	ITR*	PushFront(const DATA& _data);
	ITR*	PushBack(const KEY& _key);
	ITR*	PushBack(const DATA& _data);

	//-------------------------------------------------------------------------------------------------
	//@brief	削除
	//@return	削除出来たか
	b8		PopFront();
	b8		PopBack();

	//-------------------------------------------------------------------------------------------------
	//@brief	途中追加
	//@return	追加されたイテレータ
	ITR*	InsertFront(const KEY& _key, ITR* _pItr);
	ITR*	InsertFront(const DATA& _data, ITR* _pItr);
	ITR*	InsertBack(const KEY& _key, ITR* _pItr);
	ITR*	InsertBack(const DATA& _data, ITR* _pItr);

	//-------------------------------------------------------------------------------------------------
	//@brief	途中削除
	//@return	削除出来たか
	b8		Erase(ITR* _pItr);

protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	要素サイズ取得
	virtual u32	GetDataSize() const { return sizeof(DATA); }

	//-------------------------------------------------------------------------------------------------
	//@brief	先頭データバッファ取得
	inline DATA*		GetTopData()		{ return RCast<DATA*>(GetDataBuffer()); }
	inline const DATA*	GetTopData() const	{ return CCast<CIdxSortList*>(this)->GetDataBuffer(); }

	//-------------------------------------------------------------------------------------------------
	//@brief	キーインデックスチェック
	inline b8	CheckKeyIdx(u16 _keyIdx) const { return (_keyIdx < m_useNum); }
};

POISON_END

#include "idx_sort_list.inl"


#endif	// __IDX_SORT_LIST_H__

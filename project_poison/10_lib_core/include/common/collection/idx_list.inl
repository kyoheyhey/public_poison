﻿//#pragma once
#ifndef __IDX_LIST_INL__
#define __IDX_LIST_INL__


POISON_BGN

///*************************************************************************************************
///	インデックスリスト
///*************************************************************************************************

//-------------------------------------------------------------------------------------------------
/// テンプレート宣言部分の省略
#define IDX_LIST_TEMPLATE_HEAD	template <typename __DATA, typename __INTERFACE>
#define IDX_LIST_CLASS_HEAD		CIdxList< __DATA, __INTERFACE >

//-------------------------------------------------------------------------------------------------
///@brief	イテレータからデータ取得
IDX_LIST_TEMPLATE_HEAD
inline typename IDX_LIST_CLASS_HEAD::DATA&	IDX_LIST_CLASS_HEAD::GetDataFromItr(const ITR* _pItr)
{
	return GetTopData()[GetIdx(_pItr)];
}

//-------------------------------------------------------------------------------------------------
///@brief	インデックスからデータ取得
IDX_LIST_TEMPLATE_HEAD
inline typename IDX_LIST_CLASS_HEAD::DATA&	IDX_LIST_CLASS_HEAD::GetDataFromIdx(u16 _idx)
{
	libAssert(CheckIdx(_idx));
	return GetTopData()[_idx];
}

//-------------------------------------------------------------------------------------------------
///@brief	追加
IDX_LIST_TEMPLATE_HEAD
typename IDX_LIST_CLASS_HEAD::ITR*	IDX_LIST_CLASS_HEAD::PushBack(const DATA& _data)
{
	ITR* pItr = BASE::PushBack();
	if (pItr)
	{
		INTERFACE::Copy(GetDataFromItr(pItr), _data);
	}
	return pItr;
}

IDX_LIST_TEMPLATE_HEAD
typename IDX_LIST_CLASS_HEAD::ITR*	IDX_LIST_CLASS_HEAD::PushFront(const DATA& _data)
{
	ITR* pItr = BASE::PushFront();
	if (pItr)
	{
		INTERFACE::Copy(GetDataFromItr(pItr), _data);
	}
	return pItr;
}


//-------------------------------------------------------------------------------------------------
///@brief	削除
IDX_LIST_TEMPLATE_HEAD
b8	IDX_LIST_CLASS_HEAD::PopBack()
{
	return BASE::PopBack();
}

IDX_LIST_TEMPLATE_HEAD
b8	IDX_LIST_CLASS_HEAD::PopFront()
{
	return BASE::PopFront();
}


//-------------------------------------------------------------------------------------------------
///@brief	途中追加
IDX_LIST_TEMPLATE_HEAD
typename IDX_LIST_CLASS_HEAD::ITR*	IDX_LIST_CLASS_HEAD::InsertBack(const DATA& _data, ITR* _pItr)
{
	ITR* pItr = BASE::InsertBack(_pItr);
	if (pItr)
	{
		INTERFACE::Copy(GetDataFromItr(pItr), _data);
	}
	return pItr;
}

IDX_LIST_TEMPLATE_HEAD
typename IDX_LIST_CLASS_HEAD::ITR*	IDX_LIST_CLASS_HEAD::InsertFront(const DATA& _data, ITR* _pItr)
{
	ITR* pItr = BASE::InsertFront(_pItr);
	if (pItr)
	{
		INTERFACE::Copy(GetDataFromItr(pItr), _data);
	}
	return pItr;
}


//-------------------------------------------------------------------------------------------------
///@brief	途中削除
IDX_LIST_TEMPLATE_HEAD
b8	IDX_LIST_CLASS_HEAD::Erase(ITR* _pItr)
{
	return BASE::Erase(_pItr);
}

POISON_END


#endif	// __IDX_LIST_INL__

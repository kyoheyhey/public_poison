﻿//#pragma once
#ifndef __THREAD_H__
#define __THREAD_H__


//-------------------------------------------------------------------------------------------------
// include
#include "thread/thread_utility.h"


POISON_BGN


//*************************************************************************************************
//@brief	スレッド
//*************************************************************************************************
class CThread
{
public:
	CThread() {}
	~CThread() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	スレッドハンドル取得
	const THREAD_HANDLE&	GetHandle() const { return m_handle; }
	THREAD_HANDLE&			GetHandle() { return m_handle; }

	//-------------------------------------------------------------------------------------------------
	//@brief	スレッドID取得
	const u64				GetThreadId() const { return m_threadId; }

	//-------------------------------------------------------------------------------------------------
	//@brief	スレッド戻り値取得
	const THREAD_RESULT		GetResult() const { return m_result; }

	//-------------------------------------------------------------------------------------------------
	//@brief	名前取得
	const c8*		GetName() const { return m_name; }

	//-------------------------------------------------------------------------------------------------
	//@brief	実行中かどうか
	b8				IsRunning() const;

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄されたかどうか
	b8				IsDelete() const { return (m_threadId == 0); }

	//-------------------------------------------------------------------------------------------------
	//@brief	生成
	b8				Create(THREAD_FUNC* _func, void* _arg, const u32 _size, const s32 _prio, const c8* _pName);

	//-------------------------------------------------------------------------------------------------
	//@brief	削除
	void			Delete();

	//-------------------------------------------------------------------------------------------------
	//@brief　待機
	THREAD_RESULT	Wait();

private:
	THREAD_HANDLE	m_handle = THREAD_HANDLE_INIT;
	u64				m_threadId = 0;
	THREAD_RESULT	m_result = THREAD_INVALID_RESULT;
	c8				m_name[32] = {};
};


POISON_END

#endif	// __THREAD_H__

﻿//#pragma once
#ifndef __THREAD_UTILITY_H__
#define __THREAD_UTILITY_H__


//-------------------------------------------------------------------------------------------------
// define
#define	THREAD_PRIO_HIGHEST	(6)		//(THREAD_PRIORITY_HIGHEST)
#define	THREAD_PRIO_LOWEST	(-7)	//(THREAD_PRIORITY_LOWEST)
#define	THREAD_PRIO_MIDDLE	( 0)


// windowsではSALアノテーション使用
#ifdef LIB_SYSTEM_WIN
#define SAL_DEF(__anoos)	__anoos
#else
#define SAL_DEF(__anoos)
#endif

//-------------------------------------------------------------------------------------------------
// include
#include "thread_def.h"


POISON_BGN

#ifdef LIB_THREAD_WIN
// スレッドハンドル
typedef	HANDLE				THREAD_HANDLE;
#define THREAD_HANDLE_INIT	INVALID_HANDLE_VALUE

// スレッド戻り値
typedef u32					THREAD_RESULT;
#define THREAD_FUNC_RESULT(_ret)	((THREAD_RESULT)(_ret))
#define THREAD_INVALID_RESULT		((THREAD_RESULT)(0xffffffff))

// ミューテックス
typedef CRITICAL_SECTION	THREAD_MUTEX;

// スレッドイベント
typedef	HANDLE				THREAD_EVENT;
#endif // LIB_THREAD_WIN

#ifdef LIB_THREAD_POSIX
// スレッドハンドル
typedef	pthread_t			THREAD_HANDLE;
static const THREAD_HANDLE	s_initThreadHandle = { NULL, 0 };
#define THREAD_HANDLE_INIT	s_initThreadHandle

// スレッド戻り値
typedef void*				THREAD_RESULT;
#define THREAD_FUNC_RESULT(_ret)	((THREAD_RESULT)(_ret))
#define THREAD_INVALID_RESULT		((THREAD_RESULT)(0xffffffff))

// ミューテックス
typedef pthread_mutex_t		THREAD_MUTEX;

// スレッドイベント
struct THREAD_EVENT
{
	pthread_mutex_t	mutex;
	pthread_cond_t	cond;
	c8				name[32] = {};
	b8				signalled;
};
#endif // LIB_THREAD_POSIX

// スレッド関数型
typedef THREAD_RESULT(THREAD_FUNC)(void* _arg);


//*************************************************************************************************
//@brief	スレッドユーティリティ
//
//*************************************************************************************************
class CThreadUtility
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	static void				Initialize();

	//-------------------------------------------------------------------------------------------------
	//@brief	スレッド生成
	static void				SetAffinityMask(THREAD_HANDLE _hdl, u32 _mask);

	//-------------------------------------------------------------------------------------------------
	//@brief	現在のスレッドハンドル取得
	static THREAD_HANDLE	GetCurrentThreadHdl();

	//-------------------------------------------------------------------------------------------------
	//@brief	指定のスレッドID取得
	static u64				GetThreadID(const THREAD_HANDLE& _hdl);

	//-------------------------------------------------------------------------------------------------
	//@brief	メインスレッドかどうか
	static b8				IsMainThread();
	static b8				IsMainThread(u64 _threadId);

	//-------------------------------------------------------------------------------------------------
	//@brief	スレッド生成
	static b8				Create(THREAD_HANDLE& _hdl, THREAD_FUNC* _func, void* _arg, const u32 _size, const s32 _prio, const c8* _pName);

	//-------------------------------------------------------------------------------------------------
	//@brief	スレッド終了
	static THREAD_RESULT	Exit(THREAD_RESULT _result);

	//-------------------------------------------------------------------------------------------------
	//@brief	スレッドが稼働しているかどうか
	static b8				IsRunning(const THREAD_HANDLE& _hdl);

	//-------------------------------------------------------------------------------------------------
	//@brief	終了を待つ
	static THREAD_RESULT	Wait(const THREAD_HANDLE& _hdl);

	//-------------------------------------------------------------------------------------------------
	//@brief	スレッド削除
	static void				Delete(THREAD_HANDLE& _hdl, THREAD_RESULT* _pResult = NULL);


	//-------------------------------------------------------------------------------------------------
	//@brief	ミューテックス生成
	static b8		CreateMutex(THREAD_MUTEX& _mutex);

	//-------------------------------------------------------------------------------------------------
	//@brief	ミューテックスがロック中かどうか
	static b8		IsMutexLock(THREAD_MUTEX& _mutex);

	//-------------------------------------------------------------------------------------------------
	//@brief	ミューテックスロック
	SAL_DEF(_Acquires_exclusive_lock_(&_mutex))
	static void		MutexLock(THREAD_MUTEX& _mutex);

	//-------------------------------------------------------------------------------------------------
	//@brief	ミューテックスアンロック
	SAL_DEF(_Releases_exclusive_lock_(&_mutex))
	static void		MutexUnLock(THREAD_MUTEX& _mutex);

	//-------------------------------------------------------------------------------------------------
	//@brief	ミューテックス破棄
	static void		DeleteMutex(THREAD_MUTEX& _mutex);


	//-------------------------------------------------------------------------------------------------
	//@brief	イベント生成
	static b8		CreateEvent(THREAD_EVENT& _event, const c8* _pName);

	//-------------------------------------------------------------------------------------------------
	//@brief	イベント待機
	static b8		WaitEvent(THREAD_EVENT& _event, s64 _timeOut = -1);

	//-------------------------------------------------------------------------------------------------
	//@brief	イベント発行
	static void		SignalEvent(THREAD_EVENT& _event);

	//-------------------------------------------------------------------------------------------------
	//@brief	イベント破棄
	static void		DeleteEvent(THREAD_EVENT& _event);
};



POISON_END

#endif	// __THREAD_UTILITY_H__

﻿//#pragma once
#ifndef __SMART_PTR_INL__
#define __SMART_PTR_INL__


POISON_BGN

///*************************************************************************************************
/// スマートポインタクラス
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// デフォルトコンストラクタ
template <typename T>
CSmartPtr<T>::CSmartPtr(MEM_HANDLE _memHdl/* = MEM_HDL_INVALID*/, T* _src/* = NULL*/)
{
	m_memHdl = _memHdl;

	// そもそもデフォルトコンストラクタで呼んだらダメでしょ
	if (m_memHdl != MEM_HDL_INVALID)
	{
		m_pRefCnt = PCast<u32*>(MEM_MALLOC(m_memHdl, sizeof(u32)));
		*m_pRefCnt = 0;
	}
	else
	{
		m_pRefCnt = NULL;
	}
	m_pPtr = _src;
	AddRef();
}

///-------------------------------------------------------------------------------------------------
/// コピーコンストラクタ
template <typename T>
CSmartPtr<T>::CSmartPtr(const CSmartPtr& _src)
{
	// ポインタコピー
	m_memHdl = _src.m_memHdl;
	m_pRefCnt = _src.m_pRefCnt;
	m_pPtr = _src.m_pPtr;

	// 自分自身の参照カウンタを増加
	AddRef();
}

///-------------------------------------------------------------------------------------------------
/// デストラクタ
template <typename T>
CSmartPtr<T>::~CSmartPtr()
{
	Release();
}

///-------------------------------------------------------------------------------------------------
/// ポインタを移譲
template <typename T>
void	CSmartPtr<T>::Reset(MEM_HANDLE _memHdl/* = MEM_HDL_INVALID*/, T* _src/* = NULL*/)
{
	if (_src == m_pPtr)
	{
		return;
	}
	// 参照カウンタを減らした後に再初期化
	Release();
	CSmartPtr(_memHdl, _src);
}

///-------------------------------------------------------------------------------------------------
/// 参照カウンタ増加
template <typename T>
void	CSmartPtr<T>::AddRef()
{
	if (m_pRefCnt)	++(*m_pRefCnt);
}

///-------------------------------------------------------------------------------------------------
/// 参照カウンタを減少
template <typename T>
void	CSmartPtr<T>::Release()
{
	if (m_pRefCnt == NULL)  return;

	if (--(*m_pRefCnt) == 0)
	{
		// オブジェクト解放
		MEM_SAFE_DELETE(m_memHdl, m_pPtr);

		// 参照カウンター解放
		MEM_SAFE_FREE(m_memHdl, m_pRefCnt);
	}
}

///-------------------------------------------------------------------------------------------------
/// ポインタの貸し出し
template <typename T>
T*	CSmartPtr<T>::GetPtr()
{
	return m_pPtr;
}

template <typename T>
const T*	CSmartPtr<T>::GetPtr() const
{
	return m_pPtr;
}

///-------------------------------------------------------------------------------------------------
/// 参照カウンタ取得
template <typename T>
u32			CSmartPtr<T>::GetRefCnt() const
{
	return (m_pRefCnt != NULL ? *m_pRefCnt : 0);
}

template <typename T>
const u32*	CSmartPtr<T>::GetRefCntPtr() const
{
	return m_pRefCnt;
}

///-------------------------------------------------------------------------------------------------
/// =演算子（明示的なコピー）
template <typename T>
CSmartPtr<T>&	CSmartPtr<T>::operator = (const CSmartPtr& _src)
{
	// 自分自身への代入は不正で意味が無いので行わない。
	if (_src.m_pPtr == m_pPtr)
	{
		return (*this);
	}

	// 自分の参照カウンタを1つ減少
	Release();

	// ポインタコピー
	m_memHdl = _src.m_memHdl;
	m_pRefCnt = _src.m_pRefCnt;
	m_pPtr = _src.m_pPtr;

	// 自分自身の参照カウンタを増加
	AddRef();

	return (*this);
}

///-------------------------------------------------------------------------------------------------
/// *演算子
template <typename T>
T&		CSmartPtr<T>::operator * ()
{
	return *m_pPtr;
}

template <typename T>
const T&	CSmartPtr<T>::operator * () const
{
	return *m_pPtr;
}

///-------------------------------------------------------------------------------------------------
/// ->演算子
template <typename T>
T*		CSmartPtr<T>::operator -> ()
{
	return m_pPtr;
}

template <typename T>
const T*	CSmartPtr<T>::operator -> () const
{
	return m_pPtr;
}

///-------------------------------------------------------------------------------------------------
/// ==比較演算子
template <typename T>
b8		CSmartPtr<T>::operator == (const CSmartPtr& _src) const
{
	return (_src.m_pPtr == m_pPtr);
}

///-------------------------------------------------------------------------------------------------
/// !=比較演算子
template <typename T>
b8		CSmartPtr<T>::operator != (const CSmartPtr& _src) const
{
	return (_src.m_pPtr != m_pPtr);
}

POISON_END

#endif	// __SMART_PTR_INL__

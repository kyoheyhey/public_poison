﻿//#pragma once
#ifndef	__MATRIX_H__
#define	__MATRIX_H__


//-------------------------------------------------------------------------------------------------
// include
#include "vector.h"
#include "rotate.h"
#include "matrix_float.h"


//-------------------------------------------------------------------------------------------------
// define
#define MATRIX_COLUMN_MAJOR	// 列優先
//#define MATRIX_ROW_MAJOR	// 行優先

//#define OPENGL_PROJECTION	// -1～+1
#define DIRECTX_PROJECTION	// 0～+1

//#define OPENGL_VIEWPORT	// 左下原点
#define DIRECTX_VIEWPORT	// 左上原点


POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype


//*************************************************************************************************
//@brief	２×２行列クラス
//*************************************************************************************************
template< typename MTX >
class TMtx22 : public MTX
{
private:
	friend class	Vec;
	friend class	Mtx;

public:
	//@brief	型定義
	typedef	typename MTX::FLOAT		FLOAT;
	//@brief	ベクトル型
	typedef typename MTX::VECTOR2	TVec2;
	typedef typename MTX::VECTOR3	TVec3;
	typedef typename MTX::VECTOR4	TVec4;
	typedef typename MTX::EULER		TEuler;
	typedef typename MTX::QUAT		TQuat;

public:
	TMtx22();
	TMtx22(
		FLOAT _00, FLOAT _01,
		FLOAT _10, FLOAT _11
		);
	TMtx22(const FLOAT* _p);
	TMtx22(const TVec2& _v0, const TVec2& _v1);
	TMtx22(const TVec2* _p);
	TMtx22(const TVec2& _scale);

	//-------------------------------------------------------------------------------------------------
	//@brief	compar operators
	TMtx22&	operator=(const TMtx22&);			///< 代入
	b8		operator==(const TMtx22&) const;	///< 一致
	b8		operator!=(const TMtx22&) const;	///< 不一致

	//-------------------------------------------------------------------------------------------------
	//@brief	assignment operators
	TMtx22&	operator+=(const TMtx22&);			///< 加算
	TMtx22& operator-=(const TMtx22&);			///< 減算
	TMtx22& operator*=(const TMtx22&);			///< 乗算

	//-------------------------------------------------------------------------------------------------
	//@brief	scale operators
	TMtx22& operator*=(const TVec2&);			///< スケール乗算
	TMtx22& operator/=(const TVec2&);			///< スケール除算
	TMtx22& operator*=(FLOAT);					///< スケール乗算
	TMtx22& operator/=(FLOAT);					///< スケール除算

	//-------------------------------------------------------------------------------------------------
	//@brief	binary operators
	TMtx22	operator+(const TMtx22&) const;		///< 加算
	TMtx22	operator-(const TMtx22&) const;		///< 減算
	TMtx22	operator*(const TMtx22&) const;		///< 乗算
	TVec2	operator*(const TVec2&) const;		///< 乗算

	//-------------------------------------------------------------------------------------------------
	//@brief	GetAxis
	TVec2&	GetAxisX(TVec2& _vec) const;
	TVec2&	GetAxisY(TVec2& _vec) const;
	void	GetAxis(TVec2& _x, TVec2& _y) const;

	//-------------------------------------------------------------------------------------------------
	//@brief	SetAxis
	void	SetAxisX(const TVec2& _vec);
	void	SetAxisY(const TVec2& _vec);
	void	SetAxis(const TVec2& _x, const TVec2& _y);

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	定数値
	static const TMtx22	Identity;
};


//*************************************************************************************************
//@brief	２×３行列クラス
//*************************************************************************************************
template< typename MTX >
class TMtx23 : public MTX
{
private:
	friend class	Vec;
	friend class	Mtx;

private:
	typedef typename MTX::MTX22		MTX22;

public:
	//@brief	型定義
	typedef	typename MTX::FLOAT		FLOAT;
	//@brief	ベクトル型
	typedef typename MTX::VECTOR2	TVec2;
	typedef typename MTX::VECTOR3	TVec3;
	typedef typename MTX::VECTOR4	TVec4;
	typedef typename MTX::EULER		TEuler;
	typedef typename MTX::QUAT		TQuat;
	//@brief	マトリックス型
	typedef	TMtx22<MTX22>			TMtx22;

public:
	TMtx23();
	TMtx23(
		FLOAT _00, FLOAT _01, FLOAT _02,
		FLOAT _10, FLOAT _11, FLOAT _12
		);
	TMtx23(const FLOAT* _pF);
	TMtx23(const TVec3& _v0, const TVec3& _v1);
	TMtx23(const TVec3* _pV);
	TMtx23(const TMtx22& _mtx22);
	TMtx23(const TMtx22& _mtx22, const TVec2& _trans);
	TMtx23(const TVec2& _scale);

	//-------------------------------------------------------------------------------------------------
	//@brief	compar operators
	TMtx23&	operator=(const TMtx23&);			///< 代入
	b8		operator==(const TMtx23&) const;	///< 一致
	b8		operator!=(const TMtx23&) const;	///< 不一致

	//-------------------------------------------------------------------------------------------------
	//@brief	assignment operators
	TMtx23&	operator+=(const TMtx23&);			///< 加算
	TMtx23& operator-=(const TMtx23&);			///< 減算
	TMtx23& operator*=(const TMtx23&);			///< 乗算

	//-------------------------------------------------------------------------------------------------
	//@brief	scale operators
	TMtx23& operator*=(const TVec2&);			///< スケール乗算
	TMtx23& operator/=(const TVec2&);			///< スケール除算
	TMtx23& operator*=(FLOAT);					///< スケール乗算
	TMtx23& operator/=(FLOAT);					///< スケール除算

	//-------------------------------------------------------------------------------------------------
	//@brief	binary operators
	TMtx23	operator+(const TMtx23&) const;		///< 加算
	TMtx23	operator-(const TMtx23&) const;		///< 減算
	TMtx23	operator*(const TMtx23&) const;		///< 乗算
	TVec2	operator*(const TVec2&) const;		///< 乗算

	//-------------------------------------------------------------------------------------------------
	//@brief	GetAxis
	TVec2&	GetAxisX(TVec2& _vec) const;
	TVec2&	GetAxisY(TVec2& _vec) const;
	TVec2&	GetAxisZ(TVec2& _vec) const;
	void	GetAxis(TVec2& _x, TVec2& _y) const;
	void	GetAxis(TVec2& _x, TVec2& _y, TVec2& _z) const;

	//-------------------------------------------------------------------------------------------------
	//@brief	SetAxis
	void	SetAxisX(const TVec2& _vec);
	void	SetAxisY(const TVec2& _vec);
	void	SetAxisZ(const TVec2& _vec);
	void	SetAxis(const TVec2& _x, const TVec2& _y);
	void	SetAxis(const TVec2& _x, const TVec2& _y, const TVec2& _z);

	///-------------------------------------------------------------------------------------------------
	//@brief	GetTrans
	TVec2&	GetTrans(TVec2& _trans) const;

	//-------------------------------------------------------------------------------------------------
	//@brief	SetTrans
	void	SetTrans(const TVec2& _trans);

	//-------------------------------------------------------------------------------------------------
	//@brief	Get22
	const TMtx22&	GetMtx22() const { return CCast<TMtx23*>(this)->GetMtx22(); }
	TMtx22&			GetMtx22();
	
	//-------------------------------------------------------------------------------------------------
	//@brief	Set22
	TMtx23& SetMtx22(const TMtx22& _mtx22);

	//-------------------------------------------------------------------------------------------------
	//@brief	Copy
	TMtx23&	Copy(const TMtx22& _mtx22);

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	定数値
	static const TMtx23	Identity;
};


//*************************************************************************************************
//@brief	３×３行列クラス
//*************************************************************************************************
template< typename MTX >
class TMtx33 : public MTX
{
private:
	friend class	Vec;
	friend class	Mtx;

private:
	typedef typename MTX::MTX22		MTX22;
	typedef typename MTX::MTX23		MTX23;

public:
	//@brief	型定義
	typedef	typename MTX::FLOAT		FLOAT;
	//@brief	ベクトル型
	typedef typename MTX::VECTOR2	TVec2;
	typedef typename MTX::VECTOR3	TVec3;
	typedef typename MTX::VECTOR4	TVec4;
	typedef typename MTX::EULER		TEuler;
	typedef typename MTX::QUAT		TQuat;
	//@brief	マトリックス型
	typedef	TMtx22<MTX22>			TMtx22;
	typedef	TMtx23<MTX23>			TMtx23;

public:
	TMtx33();
	TMtx33(
		FLOAT _00, FLOAT _01, FLOAT _02,
		FLOAT _10, FLOAT _11, FLOAT _12,
		FLOAT _20, FLOAT _21, FLOAT _22
		);
	TMtx33(const FLOAT* _pF);
	TMtx33(const TVec3& _v0, const TVec3& _v1, const TVec3& _v2);
	TMtx33(const TVec3* _pV);
	TMtx33(const TMtx22& _mtx22);
	TMtx33(const TMtx22& _mtx22, const TVec2& _trans);
	TMtx33(const TMtx23& _mtx23);
	TMtx33(const TVec3& _scale);

	//-------------------------------------------------------------------------------------------------
	//@brief	compar operators
	TMtx33&	operator=(const TMtx33&);			///< 代入
	b8		operator==(const TMtx33&) const;	///< 一致
	b8		operator!=(const TMtx33&) const;	///< 不一致

	//-------------------------------------------------------------------------------------------------
	//@brief	assignment operators
	TMtx33&	operator+=(const TMtx33&);			///< 加算
	TMtx33& operator-=(const TMtx33&);			///< 減算
	TMtx33& operator*=(const TMtx33&);			///< 乗算

	//-------------------------------------------------------------------------------------------------
	//@brief	scale operators
	TMtx33& operator*=(const TVec2&);			///< スケール乗算
	TMtx33& operator/=(const TVec2&);			///< スケール除算
	TMtx33& operator*=(const TVec3&);			///< スケール乗算
	TMtx33& operator/=(const TVec3&);			///< スケール除算
	TMtx33& operator*=(FLOAT);					///< スケール乗算
	TMtx33& operator/=(FLOAT);					///< スケール除算

	//-------------------------------------------------------------------------------------------------
	//@brief	binary operators
	TMtx33	operator+(const TMtx33&) const;		///< 加算
	TMtx33	operator-(const TMtx33&) const;		///< 減算
	TMtx33	operator*(const TMtx33&) const;		///< 乗算
	TVec2	operator*(const TVec2&) const;		///< 乗算
	TVec3	operator*(const TVec3&) const;		///< 乗算

	//-------------------------------------------------------------------------------------------------
	//@brief	GetAxis
	TVec3&	GetAxisX(TVec3& _vec) const;
	TVec3&	GetAxisY(TVec3& _vec) const;
	TVec3&	GetAxisZ(TVec3& _vec) const;
	void	GetAxis(TVec3& _x, TVec3& _y, TVec3& _z) const;
	//-------------------------------------------------------------------------------------------------
	//@brief	SetAxis
	void	SetAxisX(const TVec3& _vec);
	void	SetAxisY(const TVec3& _vec);
	void	SetAxisZ(const TVec3& _vec);
	void	SetAxis(const TVec3& _x, const TVec3& _y, const TVec3& _z);

	//-------------------------------------------------------------------------------------------------
	//@brief	GetTrans
	TVec2&	GetTrans(TVec2& _vec) const;
	//@brief	SetTrans
	void	SetTrans(const TVec2& _vec);

	//-------------------------------------------------------------------------------------------------
	//@brief	Get22
	const TMtx22&	GetMtx22() const { return CCast<TMtx33*>(this)->GetMtx22(); }
	TMtx22&			GetMtx22();
	
	//-------------------------------------------------------------------------------------------------
	//@brief	Get23
	const TMtx23&	GetMtx23() const { return CCast<TMtx33*>(this)->GetMtx23(); }
	TMtx23&			GetMtx23();
	
	//-------------------------------------------------------------------------------------------------
	//@brief	Set22
	TMtx33& SetMtx22(const TMtx22& _mtx22);
	//-------------------------------------------------------------------------------------------------
	//@brief	Set23
	TMtx33&	SetMtx23(const TMtx23& _mtx23);

	//-------------------------------------------------------------------------------------------------
	//@brief	Copy
	TMtx33&	Copy(const TMtx22& _mtx22);
	TMtx33&	Copy(const TMtx23& _mtx23);

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	定数値
	static const TMtx33	Identity;
};


//*************************************************************************************************
//@brief	３×４行列クラス
//*************************************************************************************************
template< typename MTX >
class TMtx34 : public MTX
{
private:
	friend class	Vec;
	friend class	Mtx;

private:
	typedef typename MTX::MTX22		MTX22;
	typedef typename MTX::MTX23		MTX23;
	typedef typename MTX::MTX33		MTX33;

public:
	//@brief	型定義
	typedef	typename MTX::FLOAT		FLOAT;
	//@brief	ベクトル型
	typedef typename MTX::VECTOR2	TVec2;
	typedef typename MTX::VECTOR3	TVec3;
	typedef typename MTX::VECTOR4	TVec4;
	typedef typename MTX::EULER		TEuler;
	typedef typename MTX::QUAT		TQuat;
	//@brief	マトリックス型
	typedef	TMtx22<MTX22>			TMtx22;
	typedef	TMtx23<MTX23>			TMtx23;
	typedef	TMtx33<MTX33>			TMtx33;

public:
	TMtx34();
	TMtx34(
		FLOAT _00, FLOAT _01, FLOAT _02, FLOAT _03,
		FLOAT _10, FLOAT _11, FLOAT _12, FLOAT _13,
		FLOAT _20, FLOAT _21, FLOAT _22, FLOAT _23
		);
	TMtx34(const FLOAT* _pF);
	TMtx34(const TVec4& _v0, const TVec4& _v1, const TVec4& _v2);
	TMtx34(const TVec4* _pV);
	TMtx34(const TMtx33& _mtx33);
	TMtx34(const TMtx33& _mtx33, const TVec3& _trans);
	TMtx34(const TVec3& _scale);

	//-------------------------------------------------------------------------------------------------
	//@brief	compar operators
	TMtx34&	operator=(const TMtx34&);			///< 代入
	b8		operator==(const TMtx34&) const;	///< 一致
	b8		operator!=(const TMtx34&) const;	///< 不一致

	//-------------------------------------------------------------------------------------------------
	//@brief	assignment operators
	TMtx34&	operator+=(const TMtx34&);			///< 加算
	TMtx34&	operator-=(const TMtx34&);			///< 減算
	TMtx34&	operator*=(const TMtx34&);			///< 乗算

	//-------------------------------------------------------------------------------------------------
	//@brief	scale operators
	TMtx34& operator*=(const TVec3&);			///< スケール乗算
	TMtx34& operator/=(const TVec3&);			///< スケール除算
	TMtx34& operator*=(FLOAT);					///< スケール乗算
	TMtx34& operator/=(FLOAT);					///< スケール除算

	//-------------------------------------------------------------------------------------------------
	//@brief	binary operators
	TMtx34	operator+(const TMtx34&) const;		///< 加算
	TMtx34	operator-(const TMtx34&) const;		///< 減算
	TMtx34	operator*(const TMtx34&) const;		///< 乗算
	TVec3	operator*(const TVec3&) const;		///< 乗算
	TVec4	operator*(const TVec4&) const;		///< 乗算

	//-------------------------------------------------------------------------------------------------
	//@brief	GetAxis
	TVec3&	GetAxisX(TVec3& _vec) const;
	TVec3&	GetAxisY(TVec3& _vec) const;
	TVec3&	GetAxisZ(TVec3& _vec) const;
	TVec3&	GetAxisW(TVec3& _vec) const;
	void	GetAxis(TVec3& _x, TVec3& _y, TVec3& _z) const;
	void	GetAxis(TVec3& _x, TVec3& _y, TVec3& _z, TVec3& _w) const;

	//-------------------------------------------------------------------------------------------------
	//@brief	SetAxis
	void	SetAxisX(const TVec3& _vec);
	void	SetAxisY(const TVec3& _vec);
	void	SetAxisZ(const TVec3& _vec);
	void	SetAxisW(const TVec3& _vec);
	void	SetAxis(const TVec3& _x, const TVec3& _y, const TVec3& _z);
	void	SetAxis(const TVec3& _x, const TVec3& _y, const TVec3& _z, const TVec3& _w);

	//-------------------------------------------------------------------------------------------------
	//@brief	GetTrans
	TVec3&	GetTrans(TVec3& _trans) const;
	//-------------------------------------------------------------------------------------------------
	//@brief	SetTrans
	void	SetTrans(const TVec3& _trans);

	//-------------------------------------------------------------------------------------------------
	//@brief	Get33
	const TMtx33&	GetMtx33() const { return CCast<TMtx34*>(this)->GetMtx33(); }
	TMtx33&			GetMtx33();
	
	//-------------------------------------------------------------------------------------------------
	//@brief	Set33
	TMtx34& SetMtx33(const TMtx33& _mtx33);

	//-------------------------------------------------------------------------------------------------
	//@brief	Copy
	TMtx34&	Copy(const TMtx33& _mtx33);

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	定数値
	static const TMtx34	Identity;
};



//*************************************************************************************************
//@brief	４×４行列クラス
//*************************************************************************************************
template< typename MTX >
class TMtx44 : public MTX
{
private:
	friend class	Vec;
	friend class	Mtx;

private:
	typedef typename MTX::MTX22		MTX22;
	typedef typename MTX::MTX23		MTX23;
	typedef typename MTX::MTX33		MTX33;
	typedef typename MTX::MTX34		MTX34;

public:
	//@brief	型定義
	typedef	typename MTX::FLOAT		FLOAT;
	//@brief	ベクトル型
	typedef typename MTX::VECTOR2	TVec2;
	typedef typename MTX::VECTOR3	TVec3;
	typedef typename MTX::VECTOR4	TVec4;
	typedef typename MTX::EULER		TEuler;
	typedef typename MTX::QUAT		TQuat;
	//@brief	マトリックス型
	typedef	TMtx22<MTX22>			TMtx22;
	typedef	TMtx23<MTX23>			TMtx23;
	typedef	TMtx33<MTX33>			TMtx33;
	typedef	TMtx34<MTX34>			TMtx34;

public:
	TMtx44();
	TMtx44(
		FLOAT _00, FLOAT _01, FLOAT _02, FLOAT _03,
		FLOAT _10, FLOAT _11, FLOAT _12, FLOAT _13,
		FLOAT _20, FLOAT _21, FLOAT _22, FLOAT _23,
		FLOAT _30, FLOAT _31, FLOAT _32, FLOAT _33
		);
	TMtx44(const FLOAT* _pF);
	TMtx44(const TVec4& _v0, const TVec4& _v1, const TVec4& _v2, const TVec4& _v3);
	TMtx44(const TVec4* _pV);
	TMtx44(const TMtx33& _mtx33);
	TMtx44(const TMtx33& _mtx33, const TVec3& _trans);
	TMtx44(const TMtx34& _mtx34);
	TMtx44(const TVec3& _scale);
	TMtx44(const TVec4& _scale);

	//-------------------------------------------------------------------------------------------------
	//@brief	compar operators
	TMtx44&	operator=(const TMtx44&);			///< 代入
	b8		operator==(const TMtx44&) const;	///< 一致
	b8		operator!=(const TMtx44&) const;	///< 不一致

	//-------------------------------------------------------------------------------------------------
	//@brief	assignment operators
	TMtx44&	operator+=(const TMtx44&);			///< 加算
	TMtx44&	operator-=(const TMtx44&);			///< 減算
	TMtx44&	operator*=(const TMtx44&);			///< 乗算

	//-------------------------------------------------------------------------------------------------
	//@brief	scale operators
	TMtx44& operator*=(const TVec3&);			///< スケール乗算
	TMtx44& operator/=(const TVec3&);			///< スケール除算
	TMtx44& operator*=(const TVec4&);			///< スケール乗算
	TMtx44& operator/=(const TVec4&);			///< スケール除算
	TMtx44&	operator*=(FLOAT);					///< スケール乗算
	TMtx44&	operator/=(FLOAT);					///< スケール除算

	//-------------------------------------------------------------------------------------------------
	//@brief	binary operators
	TMtx44	operator+(const TMtx44&) const;		///< 加算
	TMtx44	operator-(const TMtx44&) const;		///< 減算
	TMtx44	operator*(const TMtx44&) const;		///< 乗算
	TVec3	operator*(const TVec3&) const;		///< 乗算
	TVec4	operator*(const TVec4&) const;		///< 乗算

	//-------------------------------------------------------------------------------------------------
	//@brief	GetAxis
	TVec4&	GetAxisX(TVec4& _vec) const;
	TVec4&	GetAxisY(TVec4& _vec) const;
	TVec4&	GetAxisZ(TVec4& _vec) const;
	TVec4&	GetAxisW(TVec4& _vec) const;
	void	GetAxis(TVec4& _x, TVec4& _y, TVec4& _z) const;
	void	GetAxis(TVec4& _x, TVec4& _y, TVec4& _z, TVec4& _w) const;

	//-------------------------------------------------------------------------------------------------
	//@brief	SetAxis
	void	SetAxisX(const TVec4& _vec);
	void	SetAxisY(const TVec4& _vec);
	void	SetAxisZ(const TVec4& _vec);
	void	SetAxisW(const TVec4& _vec);
	void	SetAxis(const TVec4& _x, const TVec4& _y, const TVec4& _z);
	void	SetAxis(const TVec4& _x, const TVec4& _y, const TVec4& _z, const TVec4& _w);

	//-------------------------------------------------------------------------------------------------
	//@brief	GetTrans
	TVec3&	GetTrans(TVec3& _trans) const;
	TVec4&	GetTrans(TVec4& _trans) const;
	//@brief	SetTrans
	void	SetTrans(const TVec3& _trans);
	void	SetTrans(const TVec4& _trans);

	//-------------------------------------------------------------------------------------------------
	//@brief	Get33
	const TMtx33&	GetMtx33() const { return CCast<TMtx44*>(this)->GetMtx33(); }
	TMtx33&			GetMtx33();
	//-------------------------------------------------------------------------------------------------
	//@brief	Get34
	const TMtx34&	GetMtx34() const { return CCast<TMtx44*>(this)->GetMtx34(); }
	TMtx34&			GetMtx34();

	//-------------------------------------------------------------------------------------------------
	//@brief	Set33
	TMtx44& SetMtx33(const TMtx33& _mtx33);
	//-------------------------------------------------------------------------------------------------
	//@brief	Set34
	TMtx44&	SetMtx34(const TMtx34& _mtx34);

	//-------------------------------------------------------------------------------------------------
	//@brief	Copy
	TMtx44&	Copy(const TMtx33& _mtx33);
	TMtx44&	Copy(const TMtx34& _mtx34);

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	定数値
	static const TMtx44	Identity;
};



//*************************************************************************************************
//@brief	行列計算関数
//*************************************************************************************************
class Mtx
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	２×２行列関数群
	template< typename MTX >
	static typename TMtx22<MTX>::FLOAT	Det(const TMtx22<MTX>& _mtx);
	template< typename MTX >
	static TMtx22<MTX>&	Identity(TMtx22<MTX>& _mtx);
	template< typename MTX >
	static TMtx22<MTX>&	Transpose(TMtx22<MTX>& _ret, const TMtx22<MTX>& _src);
	template< typename MTX >
	static TMtx22<MTX>&	Transpose(TMtx22<MTX>& _ret);
	template< typename MTX >
	static TMtx22<MTX>&	Normalize(TMtx22<MTX>& _ret, const TMtx22<MTX>& _src);
	template< typename MTX >
	static b8			Inverse(TMtx22<MTX>& _ret, const TMtx22<MTX>& _src);

	template< typename MTX >
	static TMtx22<MTX>&	Scale(TMtx22<MTX>& _ret, const typename TMtx22<MTX>::FLOAT _x, const typename TMtx22<MTX>::FLOAT _y);
	template< typename MTX >
	static TMtx22<MTX>&	Scale(TMtx22<MTX>& _ret, const typename TMtx22<MTX>::FLOAT _xy);
	template< typename MTX >
	static TMtx22<MTX>&	Scale(TMtx22<MTX>& _ret, const typename TMtx22<MTX>::FLOAT* _xy);
	template< typename VEC, typename MTX >
	static TMtx22<MTX>&	Scale(TMtx22<MTX>& _ret, const TVec2<VEC>& _xy);

	template< typename MTX >
	static TMtx22<MTX>&	Rotate(TMtx22<MTX>& _ret, const typename TMtx22<MTX>::FLOAT _sin, const typename TMtx22<MTX>::FLOAT _cos);
	template< typename MTX >
	static TMtx22<MTX>&	Rotate(TMtx22<MTX>& _ret, const typename TMtx22<MTX>::FLOAT _angle);

	template< typename MTX >
	static TMtx22<MTX>&	Mul(TMtx22<MTX>& _ret, const TMtx22<MTX>& _mtxA, const TMtx22<MTX>& _mtxB);


	//-------------------------------------------------------------------------------------------------
	//@brief	２×３行列関数群
	template< typename MTX >
	static typename TMtx23<MTX>::FLOAT	Det(const TMtx23<MTX>& _mtx);
	template< typename MTX >
	static TMtx23<MTX>&	Identity(TMtx23<MTX>& _mtx);
	template< typename MTX >
	static TMtx23<MTX>&	Transpose(TMtx23<MTX>& _ret, const TMtx23<MTX>& _src);
	template< typename MTX >
	static TMtx23<MTX>&	Transpose(TMtx23<MTX>& _ret);
	template< typename MTX >
	static b8			Inverse(TMtx23<MTX>& _ret, const TMtx23<MTX>& _src);

	template< typename MTX >
	static TMtx23<MTX>&	Scale(TMtx23<MTX>& _ret, const typename TMtx23<MTX>::FLOAT _x, const typename TMtx23<MTX>::FLOAT _y);
	template< typename MTX >
	static TMtx23<MTX>&	Scale(TMtx23<MTX>& _ret, const typename TMtx23<MTX>::FLOAT _xy);
	template< typename MTX >
	static TMtx23<MTX>&	Scale(TMtx23<MTX>& _ret, const typename TMtx23<MTX>::FLOAT* _xy);
	template< typename VEC, typename MTX >
	static TMtx23<MTX>&	Scale(TMtx23<MTX>& _ret, const TVec2<VEC>& _xy);

	template< typename MTX >
	static TMtx23<MTX>&	Rotate(TMtx23<MTX>& _ret, const typename TMtx23<MTX>::FLOAT _sin, const typename TMtx23<MTX>::FLOAT _cos);
	template< typename MTX >
	static TMtx23<MTX>&	Rotate(TMtx23<MTX>& _ret, const typename TMtx23<MTX>::FLOAT _angle);
	template< typename VEC, typename MTX >
	static TMtx23<MTX>&	Rotate(TMtx23<MTX>& _ret, const TVec2<VEC>& _vecA, const TVec2<VEC>& _vecB);

	template< typename MTX >
	static TMtx23<MTX>&	Trans(TMtx23<MTX>& _ret, const typename TMtx23<MTX>::FLOAT _x, const typename TMtx23<MTX>::FLOAT _y);
	template< typename MTX >
	static TMtx23<MTX>&	Trans(TMtx23<MTX>& _ret, const typename TMtx23<MTX>::FLOAT* _xy);
	template< typename VEC, typename MTX >
	static TMtx23<MTX>&	Trans(TMtx23<MTX>& _ret, const TVec2<VEC>& _xy);

	template< typename MTX >
	static TMtx23<MTX>&	Mul(TMtx23<MTX>& _ret, const TMtx23<MTX>& _mtxA, const TMtx23<MTX>& _mtxB);


	//-------------------------------------------------------------------------------------------------
	//@brief	３×３行列関数群
	template< typename MTX >
	static typename TMtx33<MTX>::FLOAT	Det(const TMtx33<MTX>& _mtx);
	template< typename MTX >
	static TMtx33<MTX>&	Identity(TMtx33<MTX>& _mtx);
	template< typename MTX >
	static TMtx33<MTX>&	Transpose(TMtx33<MTX>& _ret, const TMtx33<MTX>& _src);
	template< typename MTX >
	static TMtx33<MTX>&	Transpose(TMtx33<MTX>& _ret);
	template< typename MTX >
	static TMtx33<MTX>&	Normalize(TMtx33<MTX>& _ret, const TMtx33<MTX>& _src);
	template< typename MTX >
	static b8			Inverse(TMtx33<MTX>& _ret, const TMtx33<MTX>& _src);

	template< typename MTX >
	static TMtx33<MTX>&	Scale(TMtx33<MTX>& _ret, const typename TMtx33<MTX>::FLOAT _x, const typename TMtx33<MTX>::FLOAT _y, const typename TMtx33<MTX>::FLOAT _z);
	template< typename MTX >
	static TMtx33<MTX>&	Scale(TMtx33<MTX>& _ret, const typename TMtx33<MTX>::FLOAT _xyz);
	template< typename MTX >
	static TMtx33<MTX>&	Scale(TMtx33<MTX>& _ret, const typename TMtx33<MTX>::FLOAT* _xyz);
	template< typename VEC, typename MTX >
	static TMtx33<MTX>&	Scale(TMtx33<MTX>& _ret, const TVec3<VEC>& _xyz);

	template< typename MTX >
	static TMtx33<MTX>&	RotateX(TMtx33<MTX>& _ret, const typename TMtx33<MTX>::FLOAT _sin, const typename TMtx33<MTX>::FLOAT _cos);
	template< typename MTX >
	static TMtx33<MTX>&	RotateX(TMtx33<MTX>& _ret, const typename TMtx33<MTX>::FLOAT _angle);
	template< typename MTX >
	static TMtx33<MTX>&	RotateY(TMtx33<MTX>& _ret, const typename TMtx33<MTX>::FLOAT _sin, const typename TMtx33<MTX>::FLOAT _cos);
	template< typename MTX >
	static TMtx33<MTX>&	RotateY(TMtx33<MTX>& _ret, const typename TMtx33<MTX>::FLOAT _angle);
	template< typename MTX >
	static TMtx33<MTX>&	RotateZ(TMtx33<MTX>& _ret, const typename TMtx33<MTX>::FLOAT _sin, const typename TMtx33<MTX>::FLOAT _cos);
	template< typename MTX >
	static TMtx33<MTX>&	RotateZ(TMtx33<MTX>& _ret, const typename TMtx33<MTX>::FLOAT _angle);

	template< typename MTX >
	static TMtx33<MTX>&	RotateXYZ(TMtx33<MTX>& _ret, const typename TMtx33<MTX>::FLOAT _x, const typename TMtx33<MTX>::FLOAT _y, const typename TMtx33<MTX>::FLOAT _z);
	template< typename MTX >
	static TMtx33<MTX>&	RotateXYZ(TMtx33<MTX>& _ret, const typename TMtx33<MTX>::FLOAT* _xyz);
	template< typename VEC, typename MTX >
	static TMtx33<MTX>&	RotateXYZ(TMtx33<MTX>& _ret, const TVec3<VEC>& _xyz);

	template< typename MTX >
	static TMtx33<MTX>&	RotateXZY(TMtx33<MTX>& _ret, const typename TMtx33<MTX>::FLOAT _x, const typename TMtx33<MTX>::FLOAT _y, const typename TMtx33<MTX>::FLOAT _z);
	template< typename MTX >
	static TMtx33<MTX>&	RotateXZY(TMtx33<MTX>& _ret, const typename TMtx33<MTX>::FLOAT* _xyz);
	template< typename VEC, typename MTX >
	static TMtx33<MTX>&	RotateXZY(TMtx33<MTX>& _ret, const TVec3<VEC>& _xyz);

	template< typename MTX >
	static TMtx33<MTX>&	RotateYXZ(TMtx33<MTX>& _ret, const typename TMtx33<MTX>::FLOAT _x, const typename TMtx33<MTX>::FLOAT _y, const typename TMtx33<MTX>::FLOAT _z);
	template< typename MTX >
	static TMtx33<MTX>&	RotateYXZ(TMtx33<MTX>& _ret, const typename TMtx33<MTX>::FLOAT* _xyz);
	template< typename VEC, typename MTX >
	static TMtx33<MTX>&	RotateYXZ(TMtx33<MTX>& _ret, const TVec3<VEC>& _xyz);

	template< typename MTX >
	static TMtx33<MTX>&	RotateYZX(TMtx33<MTX>& _ret, const typename TMtx33<MTX>::FLOAT _x, const typename TMtx33<MTX>::FLOAT _y, const typename TMtx33<MTX>::FLOAT _z);
	template< typename MTX >
	static TMtx33<MTX>&	RotateYZX(TMtx33<MTX>& _ret, const typename TMtx33<MTX>::FLOAT* _xyz);
	template< typename VEC, typename MTX >
	static TMtx33<MTX>&	RotateYZX(TMtx33<MTX>& _ret, const TVec3<VEC>& _xyz);

	template< typename MTX >
	static TMtx33<MTX>&	RotateZXY(TMtx33<MTX>& _ret, const typename TMtx33<MTX>::FLOAT _x, const typename TMtx33<MTX>::FLOAT _y, const typename TMtx33<MTX>::FLOAT _z);
	template< typename MTX >
	static TMtx33<MTX>&	RotateZXY(TMtx33<MTX>& _ret, const typename TMtx33<MTX>::FLOAT* _xyz);
	template< typename VEC, typename MTX >
	static TMtx33<MTX>&	RotateZXY(TMtx33<MTX>& _ret, const TVec3<VEC>& _xyz);

	template< typename MTX >
	static TMtx33<MTX>&	RotateZYX(TMtx33<MTX>& _ret, const typename TMtx33<MTX>::FLOAT _x, const typename TMtx33<MTX>::FLOAT _y, const typename TMtx33<MTX>::FLOAT _z);
	template< typename MTX >
	static TMtx33<MTX>&	RotateZYX(TMtx33<MTX>& _ret, const typename TMtx33<MTX>::FLOAT* _xyz);
	template< typename VEC, typename MTX >
	static TMtx33<MTX>&	RotateZYX(TMtx33<MTX>& _ret, const TVec3<VEC>& _xyz);

	template< typename VEC, typename MTX >
	static TMtx33<MTX>&	Rotate(TMtx33<MTX>& _ret, const TVec3<VEC>& _vec, const typename TMtx33<MTX>::FLOAT _sin, const typename TMtx33<MTX>::FLOAT _cos);
	template< typename VEC, typename MTX >
	static TMtx33<MTX>&	Rotate(TMtx33<MTX>& _ret, const TVec3<VEC>& _vec, const typename TMtx33<MTX>::FLOAT _angle);
	template< typename VEC, typename MTX >
	static TMtx33<MTX>&	Rotate(TMtx33<MTX>& _ret, const TEuler<VEC>& _euler);
	template< typename VEC, typename MTX >
	static TMtx33<MTX>& Rotate(TMtx33<MTX>& _ret, const TQuat<VEC>& _quat);

	template< typename MTX >
	static TMtx33<MTX>&	Mul(TMtx33<MTX>& _ret, const TMtx33<MTX>& _mtxA, const TMtx33<MTX>& _mtxB);


	//-------------------------------------------------------------------------------------------------
	//@brief	３×４行列関数群
	template< typename MTX >
	static typename TMtx34<MTX>::FLOAT	Det(const TMtx34<MTX>& _mtx);
	template< typename MTX >
	static TMtx34<MTX>&	Identity(TMtx34<MTX>& _mtx);
	template< typename MTX >
	static TMtx34<MTX>&	Transpose(TMtx34<MTX>& _ret, const TMtx34<MTX>& _src);
	template< typename MTX >
	static TMtx34<MTX>&	Transpose(TMtx34<MTX>& _ret);
	template< typename MTX >
	static b8			Inverse(TMtx34<MTX>& _ret, const TMtx34<MTX>& _src);

	template< typename MTX >
	static TMtx34<MTX>&	Scale(TMtx34<MTX>& _ret, const typename TMtx34<MTX>::FLOAT _x, const typename TMtx34<MTX>::FLOAT _y, const typename TMtx34<MTX>::FLOAT _z);
	template< typename MTX >
	static TMtx34<MTX>&	Scale(TMtx34<MTX>& _ret, const typename TMtx34<MTX>::FLOAT _xyz);
	template< typename MTX >
	static TMtx34<MTX>&	Scale(TMtx34<MTX>& _ret, const typename TMtx34<MTX>::FLOAT* _xyz);
	template< typename VEC, typename MTX >
	static TMtx34<MTX>&	Scale(TMtx34<MTX>& _ret, const TVec3<VEC>& _xyz);
	template< typename VEC, typename MTX >
	static TMtx34<MTX>&	Scale(TMtx34<MTX>& _ret, const TVec4<VEC>& _xyzw);

	template< typename MTX >
	static TMtx34<MTX>&	RotateX(TMtx34<MTX>& _ret, const typename TMtx34<MTX>::FLOAT _sin, const typename TMtx34<MTX>::FLOAT _cos);
	template< typename MTX >
	static TMtx34<MTX>&	RotateX(TMtx34<MTX>& _ret, const typename TMtx34<MTX>::FLOAT _angle);
	template< typename MTX >
	static TMtx34<MTX>&	RotateY(TMtx34<MTX>& _ret, const typename TMtx34<MTX>::FLOAT _sin, const typename TMtx34<MTX>::FLOAT _cos);
	template< typename MTX >
	static TMtx34<MTX>&	RotateY(TMtx34<MTX>& _ret, const typename TMtx34<MTX>::FLOAT _angle);
	template< typename MTX >
	static TMtx34<MTX>&	RotateZ(TMtx34<MTX>& _ret, const typename TMtx34<MTX>::FLOAT _sin, const typename TMtx34<MTX>::FLOAT _cos);
	template< typename MTX >
	static TMtx34<MTX>&	RotateZ(TMtx34<MTX>& _ret, const typename TMtx34<MTX>::FLOAT _angle);

	template< typename VEC, typename MTX >
	static TMtx34<MTX>&	Rotate(TMtx34<MTX>& _ret, const TVec3<VEC>& _vec, const typename TMtx34<MTX>::FLOAT _sin, const typename TMtx34<MTX>::FLOAT _cos);
	template< typename VEC, typename MTX >
	static TMtx34<MTX>&	Rotate(TMtx34<MTX>& _ret, const TVec3<VEC>& _vec, const typename TMtx34<MTX>::FLOAT _angle);
	template< typename VEC, typename MTX >
	static TMtx34<MTX>&	Rotate(TMtx34<MTX>& _ret, const TVec4<VEC>& _vec, const typename TMtx34<MTX>::FLOAT _sin, const typename TMtx34<MTX>::FLOAT _cos);
	template< typename VEC, typename MTX >
	static TMtx34<MTX>&	Rotate(TMtx34<MTX>& _ret, const TVec4<VEC>& _vec, const typename TMtx34<MTX>::FLOAT _angle);
	template< typename VEC, typename MTX >
	static TMtx34<MTX>&	Rotate(TMtx34<MTX>& _ret, const TEuler<VEC>& _euler);
	template< typename VEC, typename MTX >
	static TMtx34<MTX>& Rotate(TMtx34<MTX>& _ret, const TQuat<VEC>& _quat);

	template< typename MTX >
	static TMtx34<MTX>&	Trans(TMtx34<MTX>& _ret, const typename TMtx34<MTX>::FLOAT _x, const typename TMtx34<MTX>::FLOAT _y, const typename TMtx34<MTX>::FLOAT _z);
	template< typename MTX >
	static TMtx34<MTX>&	Trans(TMtx34<MTX>& _ret, const typename TMtx34<MTX>::FLOAT* _xyz);
	template< typename VEC, typename MTX >
	static TMtx34<MTX>&	Trans(TMtx34<MTX>& _ret, const TVec3<VEC>& _xyz);
	template< typename VEC, typename MTX >
	static TMtx34<MTX>&	Trans(TMtx34<MTX>& _ret, const TVec4<VEC>& _xyzw);

	template< typename MTX >
	static TMtx34<MTX>&	Mul(TMtx34<MTX>& _ret, const TMtx34<MTX>& _mtxA, const TMtx34<MTX>& _mtxB);

	template< typename VEC, typename MTX >
	static TMtx34<MTX>&	LookAt(TMtx34<MTX>& _ret, const TVec3<VEC>& _eye, const TVec3<VEC>& _at, const TVec3<VEC>& _up);
	template< typename VEC, typename MTX >
	static TMtx34<MTX>&	LookAtView(TMtx34<MTX>& _ret, const TVec3<VEC>& _eye, const TVec3<VEC>& _at, const TVec3<VEC>& _up);


	//-------------------------------------------------------------------------------------------------
	//@brief	４×４行列関数群
	template< typename MTX >
	static typename TMtx44<MTX>::FLOAT	Det(const TMtx44<MTX>& _mtx);
	template< typename MTX >
	static TMtx44<MTX>&	Identity(TMtx44<MTX>& _mtx);
	template< typename MTX >
	static TMtx44<MTX>&	Transpose(TMtx44<MTX>& _ret, const TMtx44<MTX>& _src);
	template< typename MTX >
	static TMtx44<MTX>&	Transpose(TMtx44<MTX>& _ret);
	template< typename MTX >
	static TMtx44<MTX>&	Normalize(TMtx44<MTX>& _ret, const TMtx44<MTX>& _src);
	template< typename MTX >
	static b8			Inverse(TMtx44<MTX>& _ret, const TMtx44<MTX>& _src);

	template< typename MTX >
	static TMtx44<MTX>&	Scale(TMtx44<MTX>& _ret, const typename TMtx44<MTX>::FLOAT _x, const typename TMtx44<MTX>::FLOAT _y, const typename TMtx44<MTX>::FLOAT _z);
	template< typename MTX >
	static TMtx44<MTX>&	Scale(TMtx44<MTX>& _ret, const typename TMtx44<MTX>::FLOAT _xyz);
	template< typename MTX >
	static TMtx44<MTX>&	Scale(TMtx44<MTX>& _ret, const typename TMtx44<MTX>::FLOAT* _xyz);
	template< typename VEC, typename MTX >
	static TMtx44<MTX>&	Scale(TMtx44<MTX>& _ret, const TVec3<VEC>& _xyz);
	template< typename MTX >
	static TMtx44<MTX>&	Scale(TMtx44<MTX>& _ret, const typename TMtx44<MTX>::FLOAT _x, const typename TMtx44<MTX>::FLOAT _y, const typename TMtx44<MTX>::FLOAT _z, const typename TMtx44<MTX>::FLOAT _w);
	template< typename VEC, typename MTX >
	static TMtx44<MTX>&	Scale(TMtx44<MTX>& _ret, const TVec4<VEC>& _xyzw);

	template< typename MTX >
	static TMtx44<MTX>&	RotateX(TMtx44<MTX>& _ret, const typename TMtx44<MTX>::FLOAT _sin, const typename TMtx44<MTX>::FLOAT _cos);
	template< typename MTX >
	static TMtx44<MTX>&	RotateX(TMtx44<MTX>& _ret, const typename TMtx44<MTX>::FLOAT _angle);
	template< typename MTX >
	static TMtx44<MTX>&	RotateY(TMtx44<MTX>& _ret, const typename TMtx44<MTX>::FLOAT _sin, const typename TMtx44<MTX>::FLOAT _cos);
	template< typename MTX >
	static TMtx44<MTX>&	RotateY(TMtx44<MTX>& _ret, const typename TMtx44<MTX>::FLOAT _angle);
	template< typename MTX >
	static TMtx44<MTX>&	RotateZ(TMtx44<MTX>& _ret, const typename TMtx44<MTX>::FLOAT _sin, const typename TMtx44<MTX>::FLOAT _cos);
	template< typename MTX >
	static TMtx44<MTX>&	RotateZ(TMtx44<MTX>& _ret, const typename TMtx44<MTX>::FLOAT _angle);

	template< typename VEC, typename MTX >
	static TMtx44<MTX>&	Rotate(TMtx44<MTX>& _ret, const TVec3<VEC>& _vec, const typename TMtx44<MTX>::FLOAT _sin, const typename TMtx44<MTX>::FLOAT _cos);
	template< typename VEC, typename MTX >
	static TMtx44<MTX>&	Rotate(TMtx44<MTX>& _ret, const TVec3<VEC>& _vec, const typename TMtx44<MTX>::FLOAT _angle);
	template< typename VEC, typename MTX >
	static TMtx44<MTX>&	Rotate(TMtx44<MTX>& _ret, const TVec4<VEC>& _vec, const typename TMtx44<MTX>::FLOAT _sin, const typename TMtx44<MTX>::FLOAT _cos);
	template< typename VEC, typename MTX >
	static TMtx44<MTX>&	Rotate(TMtx44<MTX>& _ret, const TVec4<VEC>& _vec, const typename TMtx44<MTX>::FLOAT _angle);
	template< typename VEC, typename MTX >
	static TMtx44<MTX>&	Rotate(TMtx44<MTX>& _ret, const TEuler<VEC>& _euler);
	template< typename VEC, typename MTX >
	static TMtx44<MTX>& Rotate(TMtx44<MTX>& _ret, const TQuat<VEC>& _quat);

	template< typename MTX >
	static TMtx44<MTX>&	Trans(TMtx44<MTX>& _ret, const typename TMtx44<MTX>::FLOAT _x, const typename TMtx44<MTX>::FLOAT _y, const typename TMtx44<MTX>::FLOAT _z);
	template< typename MTX >
	static TMtx44<MTX>&	Trans(TMtx44<MTX>& _ret, const typename TMtx44<MTX>::FLOAT* _xyz);
	template< typename VEC, typename MTX >
	static TMtx44<MTX>&	Trans(TMtx44<MTX>& _ret, const TVec3<VEC>& _xyz);
	template< typename MTX >
	static TMtx44<MTX>&	Trans(TMtx44<MTX>& _ret, const typename TMtx44<MTX>::FLOAT _x, const typename TMtx44<MTX>::FLOAT _y, const typename TMtx44<MTX>::FLOAT _z, const typename TMtx44<MTX>::FLOAT _w);
	template< typename VEC, typename MTX >
	static TMtx44<MTX>&	Trans(TMtx44<MTX>& _ret, const TVec4<VEC>& _xyzw);

	template< typename MTX >
	static TMtx44<MTX>&	Mul(TMtx44<MTX>& _ret, const TMtx44<MTX>& _mtxA, const TMtx44<MTX>& _mtxB);

	template< typename VEC, typename MTX >
	static TMtx44<MTX>&	LookAt(TMtx44<MTX>& _ret, const TVec3<VEC>& _eye, const TVec3<VEC>& _at, const TVec3<VEC>& _up);
	template< typename VEC, typename MTX >
	static TMtx44<MTX>&	LookAtView(TMtx44<MTX>& _ret, const TVec3<VEC>& _eye, const TVec3<VEC>& _at, const TVec3<VEC>& _up);

	template< typename MTX >
	static TMtx44<MTX>&	Perspective(TMtx44<MTX>& _ret, const typename TMtx44<MTX>::FLOAT _left, const typename TMtx44<MTX>::FLOAT _right, const typename TMtx44<MTX>::FLOAT _bottom, const typename TMtx44<MTX>::FLOAT _top, const typename TMtx44<MTX>::FLOAT _near, const typename TMtx44<MTX>::FLOAT _far);
	template< typename MTX >
	static TMtx44<MTX>&	Perspective(TMtx44<MTX>& _ret, const typename TMtx44<MTX>::FLOAT _fov, const typename TMtx44<MTX>::FLOAT _aspect, const typename TMtx44<MTX>::FLOAT _near, const typename TMtx44<MTX>::FLOAT _far);
	template< typename MTX >
	static TMtx44<MTX>&	Ortho(TMtx44<MTX>& _ret, const typename TMtx44<MTX>::FLOAT _left, const typename TMtx44<MTX>::FLOAT _right, const typename TMtx44<MTX>::FLOAT _bottom, const typename TMtx44<MTX>::FLOAT _top, const typename TMtx44<MTX>::FLOAT _near, const typename TMtx44<MTX>::FLOAT _far);
	template< typename MTX >
	static TMtx44<MTX>&	Ortho(TMtx44<MTX>& _ret, const typename TMtx44<MTX>::FLOAT _width, const typename TMtx44<MTX>::FLOAT _height, const typename TMtx44<MTX>::FLOAT _near, const typename TMtx44<MTX>::FLOAT _far);


	//-------------------------------------------------------------------------------------------------
	//@brief	行列×ベクトル
	template< typename VEC, typename MTX >
	static TVec2<VEC>&	Transform(TVec2<VEC>& _ret, const TMtx22<MTX>& _mtx, const TVec2<VEC>& _src);
	template< typename VEC, typename MTX >
	static TVec2<VEC>&	Transform(TVec2<VEC>& _ret, const TMtx23<MTX>& _mtx, const TVec2<VEC>& _src);
	template< typename VEC, typename MTX >
	static TVec2<VEC>&	Transform(TVec2<VEC>& _ret, const TMtx33<MTX>& _mtx, const TVec2<VEC>& _src);
	template< typename VEC, typename MTX >
	static TVec3<VEC>&	Transform(TVec3<VEC>& _ret, const TMtx33<MTX>& _mtx, const TVec3<VEC>& _src);
	template< typename VEC, typename MTX >
	static TVec4<VEC>&	Transform(TVec4<VEC>& _ret, const TMtx33<MTX>& _mtx, const TVec4<VEC>& _src);
	template< typename VEC, typename MTX >
	static TVec3<VEC>&	Transform(TVec3<VEC>& _ret, const TMtx34<MTX>& _mtx, const TVec3<VEC>& _src);
	template< typename VEC, typename MTX >
	static TVec4<VEC>&	Transform(TVec4<VEC>& _ret, const TMtx34<MTX>& _mtx, const TVec4<VEC>& _src);
	template< typename VEC, typename MTX >
	static TVec3<VEC>&	Transform(TVec3<VEC>& _ret, const TMtx44<MTX>& _mtx, const TVec3<VEC>& _src);
	template< typename VEC, typename MTX >
	static TVec4<VEC>&	Transform(TVec4<VEC>& _ret, const TMtx44<MTX>& _mtx, const TVec4<VEC>& _src);


	//-------------------------------------------------------------------------------------------------
	//@brief	行列からスケール成分抽出
	template< typename VEC, typename MTX >
	static TVec2<VEC>&	GetScale(TVec2<VEC>& _ret, const TMtx22<MTX>& _mtx);
	template< typename VEC, typename MTX >
	static TVec2<VEC>&	GetScale2(TVec2<VEC>& _ret, const TMtx22<MTX>& _mtx);
	
	template< typename VEC, typename MTX >
	static TVec2<VEC>&	GetScale(TVec2<VEC>& _ret, const TMtx23<MTX>& _mtx);
	template< typename VEC, typename MTX >
	static TVec2<VEC>&	GetScale2(TVec2<VEC>& _ret, const TMtx23<MTX>& _mtx);
	
	template< typename VEC, typename MTX >
	static TVec2<VEC>&	GetScale(TVec2<VEC>& _ret, const TMtx33<MTX>& _mtx);
	template< typename VEC, typename MTX >
	static TVec2<VEC>&	GetScale2(TVec2<VEC>& _ret, const TMtx33<MTX>& _mtx);
	template< typename VEC, typename MTX >
	static TVec3<VEC>&	GetScale(TVec3<VEC>& _ret, const TMtx33<MTX>& _mtx);
	template< typename VEC, typename MTX >
	static TVec3<VEC>&	GetScale2(TVec3<VEC>& _ret, const TMtx33<MTX>& _mtx);
	
	template< typename VEC, typename MTX >
	static TVec3<VEC>&	GetScale(TVec3<VEC>& _ret, const TMtx34<MTX>& _mtx);
	template< typename VEC, typename MTX >
	static TVec3<VEC>&	GetScale2(TVec3<VEC>& _ret, const TMtx34<MTX>& _mtx);
	
	template< typename VEC, typename MTX >
	static TVec3<VEC>&	GetScale(TVec3<VEC>& _ret, const TMtx44<MTX>& _mtx);
	template< typename VEC, typename MTX >
	static TVec3<VEC>&	GetScale2(TVec3<VEC>& _ret, const TMtx44<MTX>& _mtx);
	template< typename VEC, typename MTX >
	static TVec4<VEC>&	GetScale(TVec4<VEC>& _ret, const TMtx44<MTX>& _mtx);
	template< typename VEC, typename MTX >
	static TVec4<VEC>&	GetScale2(TVec4<VEC>& _ret, const TMtx44<MTX>& _mtx);


	//-------------------------------------------------------------------------------------------------
	//@brief	行列⇒オイラー変換
	template < typename VEC, typename MTX >
	static TEuler<VEC>&	MtxToEulerXYZ(TEuler<VEC>& _ret, const TMtx33<MTX>& _mtx);
	template < typename VEC, typename MTX >
	static TEuler<VEC>& MtxToEulerXZY(TEuler<VEC>& _ret, const TMtx33<MTX>& _mtx);
	template < typename VEC, typename MTX >
	static TEuler<VEC>& MtxToEulerYXZ(TEuler<VEC>& _ret, const TMtx33<MTX>& _mtx);
	template < typename VEC, typename MTX >
	static TEuler<VEC>& MtxToEulerYZX(TEuler<VEC>& _ret, const TMtx33<MTX>& _mtx);
	template < typename VEC, typename MTX >
	static TEuler<VEC>& MtxToEulerZXY(TEuler<VEC>& _ret, const TMtx33<MTX>& _mtx);
	template < typename VEC, typename MTX >
	static TEuler<VEC>& MtxToEulerZYX(TEuler<VEC>& _ret, const TMtx33<MTX>& _mtx);

	template < typename VEC, typename MTX >
	static TEuler<VEC>&	MtxToEulerXYZ(TEuler<VEC>& _ret, const TMtx34<MTX>& _mtx);
	template < typename VEC, typename MTX >
	static TEuler<VEC>& MtxToEulerXZY(TEuler<VEC>& _ret, const TMtx34<MTX>& _mtx);
	template < typename VEC, typename MTX >
	static TEuler<VEC>& MtxToEulerYXZ(TEuler<VEC>& _ret, const TMtx34<MTX>& _mtx);
	template < typename VEC, typename MTX >
	static TEuler<VEC>& MtxToEulerYZX(TEuler<VEC>& _ret, const TMtx34<MTX>& _mtx);
	template < typename VEC, typename MTX >
	static TEuler<VEC>& MtxToEulerZXY(TEuler<VEC>& _ret, const TMtx34<MTX>& _mtx);
	template < typename VEC, typename MTX >
	static TEuler<VEC>& MtxToEulerZYX(TEuler<VEC>& _ret, const TMtx34<MTX>& _mtx);

	template < typename VEC, typename MTX >
	static TEuler<VEC>&	MtxToEulerXYZ(TEuler<VEC>& _ret, const TMtx44<MTX>& _mtx);
	template < typename VEC, typename MTX >
	static TEuler<VEC>& MtxToEulerXZY(TEuler<VEC>& _ret, const TMtx44<MTX>& _mtx);
	template < typename VEC, typename MTX >
	static TEuler<VEC>& MtxToEulerYXZ(TEuler<VEC>& _ret, const TMtx44<MTX>& _mtx);
	template < typename VEC, typename MTX >
	static TEuler<VEC>& MtxToEulerYZX(TEuler<VEC>& _ret, const TMtx44<MTX>& _mtx);
	template < typename VEC, typename MTX >
	static TEuler<VEC>& MtxToEulerZXY(TEuler<VEC>& _ret, const TMtx44<MTX>& _mtx);
	template < typename VEC, typename MTX >
	static TEuler<VEC>& MtxToEulerZYX(TEuler<VEC>& _ret, const TMtx44<MTX>& _mtx);


	//-------------------------------------------------------------------------------------------------
	//@brief	行列⇒クォータニオン変換
	template < typename VEC, typename MTX >
	static TQuat<VEC>&	MtxToQuat(TQuat<VEC>& _ret, const TMtx33<MTX>& _mtx);
	template < typename VEC, typename MTX >
	static TQuat<VEC>&	MtxToQuat(TQuat<VEC>& _ret, const TMtx34<MTX>& _mtx);
	template < typename VEC, typename MTX >
	static TQuat<VEC>&	MtxToQuat(TQuat<VEC>& _ret, const TMtx44<MTX>& _mtx);
	

	//-------------------------------------------------------------------------------------------------
	//@brief	行列成分分解
	template< typename VEC, typename MTX >
	static void			Decompose(TVec3<VEC>& _outScale, TEuler<VEC>& _outRot, const TMtx33<MTX>& _src, const typename TEuler<VEC>::ROT_TYPE _type = typename TEuler<VEC>::ROT_ZXY);
	template< typename VEC, typename MTX >
	static void			Decompose(TVec3<VEC>& _outScale, TQuat<VEC>& _outRot, const TMtx33<MTX>& _src);

	template< typename VEC, typename MTX >
	static void			Decompose(TVec3<VEC>& _outScale, TEuler<VEC>& _outRot, TVec3<VEC>& _outTrans, const TMtx34<MTX>& _src, const typename TEuler<VEC>::ROT_TYPE _type = typename TEuler<VEC>::ROT_ZXY);
	template< typename VEC, typename MTX >
	static void			Decompose(TVec3<VEC>& _outScale, TQuat<VEC>& _outRot, TVec3<VEC>& _outTrans, const TMtx34<MTX>& _src);

	template< typename VEC, typename MTX >
	static void			Decompose(TVec3<VEC>& _outScale, TEuler<VEC>& _outRot, TVec3<VEC>& _outTrans, const TMtx44<MTX>& _src, const typename TEuler<VEC>::ROT_TYPE _type = typename TEuler<VEC>::ROT_ZXY);
	template< typename VEC, typename MTX >
	static void			Decompose(TVec3<VEC>& _outScale, TQuat<VEC>& _outRot, TVec3<VEC>& _outTrans, const TMtx44<MTX>& _src);
};


//*************************************************************************************************
//@brief	行列定義
typedef TMtx23<MATRIX22>	CMtx22;
typedef TMtx23<MATRIX23>	CMtx23;
typedef TMtx33<MATRIX33>	CMtx33;
typedef TMtx34<MATRIX34>	CMtx34;
typedef TMtx44<MATRIX44>	CMtx44;

POISON_END

#include "matrix.inl"
#include "matrix_func.inl"

#endif	//__MATRIX_H__

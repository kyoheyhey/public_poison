﻿//#pragma once
#ifndef __VECTOR_FLOAT_H__
#define __VECTOR_FLOAT_H__


//-------------------------------------------------------------------------------------------------
// include


//-------------------------------------------------------------------------------------------------
// prototype


POISON_BGN


//*************************************************************************************************
//@brief	ベクトル用参照float
//*************************************************************************************************
struct VecRefFloat
{
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ
	VecRefFloat(f32& _f)
		: f(&_f) {}
	//@brief	コピーコンストラクタ
	VecRefFloat(const VecRefFloat& _f)
		: f(_f.f) {}

	//-------------------------------------------------------------------------------------------------
	//@brief	f32の代入
	VecRefFloat& operator=(f32 _f) { *f = _f; CHECK_ENABLE_FLOAT(*f); return (*this); }
	//@brief	コピー
	VecRefFloat& operator=(const VecRefFloat& _f) { *f = *_f.f; return (*this); }

	//-------------------------------------------------------------------------------------------------
	//@brief	f32への暗黙のキャスト
	operator f32& () { return *f; }
	//@brief	f32への暗黙のキャスト
	operator const f32& () const { return *f; }

public:
	f32*	f;	///< f32の参照
};


//*************************************************************************************************
//@brief	実クラス
//*************************************************************************************************
ALIGN(16) struct VECTOR2
{
	//-------------------------------------------------------------------------------------------------
	//@brief	フロート型
	typedef	f32				FLOAT;
	//@brief	参照型フロート型
	typedef VecRefFloat		REF_FLOAT;

	//-------------------------------------------------------------------------------------------------
	//@brief	operator
	inline const FLOAT&	operator[](const u32 _index) const { return v[_index]; }	///< 配列アクセスconst
	inline REF_FLOAT	operator[](const u32 _index) { return v[_index]; }			///< 配列アクセス

	//-------------------------------------------------------------------------------------------------
	//@brief	バッファ取得
	inline const FLOAT*	GetPtr() const { return v; }
	inline FLOAT*		GetPtr() { return v; }

	union{
		struct {
			f32 x, y;
		};
		f32 v[4];	// アライメント関係上4
	};
};

ALIGN(16) struct VECTOR3
{
	//-------------------------------------------------------------------------------------------------
	//@brief	フロート型
	typedef	f32				FLOAT;
	//@brief	参照型フロート型
	typedef VecRefFloat		REF_FLOAT;
	//@brief	ベクトル型
	typedef VECTOR2			VEC2;

	//-------------------------------------------------------------------------------------------------
	//@brief	operator
	inline const FLOAT&	operator[](const u32 _index) const { return v[_index]; }	///< 配列アクセスconst
	inline REF_FLOAT	operator[](const u32 _index) { return v[_index]; }			///< 配列アクセス

	//-------------------------------------------------------------------------------------------------
	//@brief	バッファ取得
	inline const FLOAT*	GetPtr() const { return v; }
	inline FLOAT*		GetPtr() { return v; }

	union{
		struct {
			f32 x, y, z;
		};
		f32 v[4];	// アライメント関係上4
	};
};

ALIGN(16) struct VECTOR4
{
	//-------------------------------------------------------------------------------------------------
	//@brief	フロート型
	typedef	f32				FLOAT;
	//@brief	参照型フロート型
	typedef VecRefFloat		REF_FLOAT;
	//@brief	ベクトル型
	typedef VECTOR2			VEC2;
	typedef VECTOR3			VEC3;

	//-------------------------------------------------------------------------------------------------
	//@brief	operator
	inline const FLOAT&	operator[](const u32 _index) const { return v[_index]; }	///< 配列アクセスconst
	inline REF_FLOAT	operator[](const u32 _index) { return v[_index]; }			///< 配列アクセス

	//-------------------------------------------------------------------------------------------------
	//@brief	バッファ取得
	inline const FLOAT*	GetPtr() const { return v; }
	inline FLOAT*		GetPtr() { return v; }

	union{
		struct {
			f32 x, y, z, w;
		};
		f32 v[4];
	};
};


POISON_END

#endif	// __VECTOR_FLOAT_H__

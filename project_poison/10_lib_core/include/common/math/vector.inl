﻿//#pragma once
#ifndef __VECTOR_INL__
#define __VECTOR_INL__


POISON_BGN

//-------------------------------------------------------------------------------------------------
/// テンプレート宣言部分の省略
#define VEC_TEMPLATE_HEAD	template< typename VEC >
#define VEC2_CLASS_HEAD		TVec2< VEC >
#define VEC3_CLASS_HEAD		TVec3< VEC >
#define VEC4_CLASS_HEAD		TVec4< VEC >


///*************************************************************************************************
/// ２ベクトルクラス
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// コンストラクタ
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD::TVec2()
{
	x = y = 0.0f;
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD::TVec2(FLOAT _xy)
{
	x = y = _xy;
	CHECK_ENABLE_VECTOR2();
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD::TVec2(FLOAT _x, FLOAT _y)
{
	x = _x; y = _y;
	CHECK_ENABLE_VECTOR2();
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD::TVec2(const FLOAT* _xy)
{
	x = _xy[0]; y = _xy[1];
	CHECK_ENABLE_VECTOR2();
}

///-------------------------------------------------------------------------------------------------
/// compar operators
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	VEC2_CLASS_HEAD::operator=(const TVec2& _vec)
{
	x = _vec[0]; y = _vec[1];
	CHECK_ENABLE_VECTOR2();
	return *this;
}
VEC_TEMPLATE_HEAD
b8		VEC2_CLASS_HEAD::operator==(const TVec2& _vec) const
{
	return (x == _vec[0] && y == _vec[1]);
}
VEC_TEMPLATE_HEAD
b8		VEC2_CLASS_HEAD::operator!=(const TVec2& _vec) const
{
	return (x != _vec[0] || y != _vec[1]);
}

///-------------------------------------------------------------------------------------------------
/// assignment operators
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	VEC2_CLASS_HEAD::operator+=(const TVec2& _vec)
{
	x += _vec[0]; y += _vec[1];
	CHECK_ENABLE_VECTOR2();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	VEC2_CLASS_HEAD::operator-=(const TVec2& _vec)
{
	x -= _vec[0]; y -= _vec[1];
	CHECK_ENABLE_VECTOR2();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	VEC2_CLASS_HEAD::operator*=(const TVec2& _vec)
{
	x *= _vec[0]; y *= _vec[1];
	CHECK_ENABLE_VECTOR2();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	VEC2_CLASS_HEAD::operator/=(const TVec2& _vec)
{
	x /= _vec[0]; y /= _vec[1];
	CHECK_ENABLE_VECTOR2();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	VEC2_CLASS_HEAD::operator+=(FLOAT _scl)
{
	x += _scl; y += _scl;
	CHECK_ENABLE_VECTOR2();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	VEC2_CLASS_HEAD::operator-=(FLOAT _scl)
{
	x -= _scl; y -= _scl;
	CHECK_ENABLE_VECTOR2();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	VEC2_CLASS_HEAD::operator*=(FLOAT _scl)
{
	x *= _scl; y *= _scl;
	CHECK_ENABLE_VECTOR2();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	VEC2_CLASS_HEAD::operator/=(FLOAT _scl)
{
	x /= _scl; y /= _scl;
	CHECK_ENABLE_VECTOR2();
	return *this;
}

///-------------------------------------------------------------------------------------------------
/// binary operators
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD		VEC2_CLASS_HEAD::operator+(const TVec2& _vec) const
{
	return TVec2(x + _vec[0], y + _vec[1]);
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD		VEC2_CLASS_HEAD::operator-(const TVec2& _vec) const
{
	return TVec2(x - _vec[0], y - _vec[1]);
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD		VEC2_CLASS_HEAD::operator*(const TVec2& _vec) const
{
	return TVec2(x * _vec[0], y * _vec[1]);
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD		VEC2_CLASS_HEAD::operator/(const TVec2& _vec) const
{
	return TVec2(x / _vec[0], y / _vec[1]);
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD		VEC2_CLASS_HEAD::operator+(const FLOAT _scl) const
{
	return TVec2(x + _scl, y + _scl);
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD		VEC2_CLASS_HEAD::operator-(const FLOAT _scl) const
{
	return TVec2(x - _scl, y - _scl);
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD		VEC2_CLASS_HEAD::operator*(const FLOAT _scl) const
{
	return TVec2(x * _scl, y * _scl);
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD		VEC2_CLASS_HEAD::operator/(const FLOAT _scl) const
{
	return TVec2(x / _scl, y / _scl);
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD		VEC2_CLASS_HEAD::operator-() const
{
	return TVec2(-x, -y);
}

///-------------------------------------------------------------------------------------------------
/// Getrer
VEC_TEMPLATE_HEAD
typename const VEC2_CLASS_HEAD::FLOAT	VEC2_CLASS_HEAD::GetX() const
{
	return x;
}
VEC_TEMPLATE_HEAD
typename const VEC2_CLASS_HEAD::FLOAT	VEC2_CLASS_HEAD::GetY() const
{
	return y;
}

///-------------------------------------------------------------------------------------------------
/// Setter
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	VEC2_CLASS_HEAD::Set(FLOAT _v)
{
	x = _v; y = _v;
	CHECK_ENABLE_VECTOR2();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	VEC2_CLASS_HEAD::Set(FLOAT _x, FLOAT _y)
{
	x = _x; y = _y;
	CHECK_ENABLE_VECTOR2();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	VEC2_CLASS_HEAD::Set(const FLOAT* _xy)
{
	x = _xy[0]; y = _xy[1];
	CHECK_ENABLE_VECTOR2();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	VEC2_CLASS_HEAD::SetX(FLOAT _x)
{
	x = _x;
	CHECK_ENABLE_VECTOR2();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	VEC2_CLASS_HEAD::SetY(FLOAT _y)
{
	y = _y;
	CHECK_ENABLE_VECTOR2();
	return *this;
}

///-------------------------------------------------------------------------------------------------
/// 定数値
VEC_TEMPLATE_HEAD
const VEC2_CLASS_HEAD	VEC2_CLASS_HEAD::Zero(	+0.0f, +0.0f);
VEC_TEMPLATE_HEAD
const VEC2_CLASS_HEAD	VEC2_CLASS_HEAD::One(	+1.0f, +1.0f);
VEC_TEMPLATE_HEAD
const VEC2_CLASS_HEAD	VEC2_CLASS_HEAD::Left(	-1.0f, +0.0f);
VEC_TEMPLATE_HEAD
const VEC2_CLASS_HEAD	VEC2_CLASS_HEAD::Right(	+1.0f, +0.0f);
VEC_TEMPLATE_HEAD
const VEC2_CLASS_HEAD	VEC2_CLASS_HEAD::Up(	+0.0f, +1.0f);
VEC_TEMPLATE_HEAD
const VEC2_CLASS_HEAD	VEC2_CLASS_HEAD::Down(	+0.0f, -1.0f);




///*************************************************************************************************
/// ３ベクトルクラス
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// コンストラクタ
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD::TVec3()
{
	x = y = z = 0.0f;
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD::TVec3(FLOAT _xyz)
{
	x = y = z = _xyz;
	CHECK_ENABLE_VECTOR3();
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD::TVec3(FLOAT _x, FLOAT _y, FLOAT _z)
{
	x = _x; y = _y; z = _z;
	CHECK_ENABLE_VECTOR3();
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD::TVec3(const FLOAT* _xyz)
{
	Set(_xyz);
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD::TVec3(const TVec2& _xy, FLOAT _z)
{
	Set(_xy, _z);
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD::TVec3(FLOAT _x, const TVec2& _yz)
{
	Set(_x, _yz);
}

///-------------------------------------------------------------------------------------------------
/// compar operators
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	VEC3_CLASS_HEAD::operator=(const TVec3& _vec)
{
	x = _vec[0]; y = _vec[1]; z = _vec[2];
	CHECK_ENABLE_VECTOR3();
	return *this;
}
VEC_TEMPLATE_HEAD
b8		VEC3_CLASS_HEAD::operator==(const TVec3& _vec) const
{
	return (x == _vec[0] && y == _vec[1] && z == _vec[2]);
}
VEC_TEMPLATE_HEAD
b8		VEC3_CLASS_HEAD::operator!=(const TVec3& _vec) const
{
	return (x != _vec[0] || y != _vec[1] || z != _vec[2]);
}

///-------------------------------------------------------------------------------------------------
/// assignment operators
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	VEC3_CLASS_HEAD::operator+=(const TVec3& _vec)
{
	x += _vec[0]; y += _vec[1]; z += _vec[2];
	CHECK_ENABLE_VECTOR3();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD& VEC3_CLASS_HEAD::operator-=(const TVec3& _vec)
{
	x -= _vec[0]; y -= _vec[1]; z -= _vec[2];
	CHECK_ENABLE_VECTOR3();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD& VEC3_CLASS_HEAD::operator*=(const TVec3& _vec)
{
	x *= _vec[0]; y *= _vec[1]; z *= _vec[2];
	CHECK_ENABLE_VECTOR3();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD& VEC3_CLASS_HEAD::operator/=(const TVec3& _vec)
{
	x /= _vec[0]; y /= _vec[1]; z /= _vec[2];
	CHECK_ENABLE_VECTOR3();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD& VEC3_CLASS_HEAD::operator+=(FLOAT _scl)
{
	x += _scl; y += _scl; z += _scl;
	CHECK_ENABLE_VECTOR3();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD& VEC3_CLASS_HEAD::operator-=(FLOAT _scl)
{
	x -= _scl; y -= _scl; z -= _scl;
	CHECK_ENABLE_VECTOR3();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD& VEC3_CLASS_HEAD::operator*=(FLOAT _scl)
{
	x *= _scl; y *= _scl; z *= _scl;
	CHECK_ENABLE_VECTOR3();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	VEC3_CLASS_HEAD::operator/=(FLOAT _scl)
{
	x /= _scl; y /= _scl; z /= _scl;
	CHECK_ENABLE_VECTOR3();
	return *this;
}

///-------------------------------------------------------------------------------------------------
/// binary operators
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD		VEC3_CLASS_HEAD::operator+(const TVec3& _vec) const
{
	return TVec3(x + _vec[0], y + _vec[1], z + _vec[2]);
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD		VEC3_CLASS_HEAD::operator-(const TVec3& _vec) const
{
	return TVec3(x - _vec[0], y - _vec[1], z - _vec[2]);
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD		VEC3_CLASS_HEAD::operator*(const TVec3& _vec) const
{
	return TVec3(x * _vec[0], y * _vec[1], z * _vec[2]);
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD		VEC3_CLASS_HEAD::operator/(const TVec3& _vec) const
{
	return TVec3(x / _vec[0], y / _vec[1], z / _vec[2]);
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD		VEC3_CLASS_HEAD::operator+(const FLOAT _scl) const
{
	return TVec3(x + _scl, y + _scl, z + _scl);
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD		VEC3_CLASS_HEAD::operator-(const FLOAT _scl) const
{
	return TVec3(x - _scl, y - _scl, z - _scl);
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD		VEC3_CLASS_HEAD::operator*(const FLOAT _scl) const
{
	return TVec3(x * _scl, y * _scl, z * _scl);
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD		VEC3_CLASS_HEAD::operator/(const FLOAT _scl) const
{
	return TVec3(x / _scl, y / _scl, z / _scl);
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD		VEC3_CLASS_HEAD::operator-() const
{
	return TVec3(-x, -y, -z);
}


///-------------------------------------------------------------------------------------------------
/// Getter
VEC_TEMPLATE_HEAD
typename const VEC3_CLASS_HEAD::FLOAT	VEC3_CLASS_HEAD::GetX() const
{
	return x;
}
VEC_TEMPLATE_HEAD
typename const VEC3_CLASS_HEAD::FLOAT	VEC3_CLASS_HEAD::GetY() const
{
	return y;
}
VEC_TEMPLATE_HEAD
typename const VEC3_CLASS_HEAD::FLOAT	VEC3_CLASS_HEAD::GetZ() const
{
	return z;
}

VEC_TEMPLATE_HEAD
typename VEC3_CLASS_HEAD::TVec2&	VEC3_CLASS_HEAD::GetXY()
{
	return *PCast<TVec2*>(this);
}

///-------------------------------------------------------------------------------------------------
/// Setter
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	VEC3_CLASS_HEAD::Set(FLOAT _v)
{
	x = _v; y = _v; z = _v;
	CHECK_ENABLE_VECTOR3();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	VEC3_CLASS_HEAD::Set(FLOAT _x, FLOAT _y, FLOAT _z)
{
	x = _x; y = _y; z = _z;
	CHECK_ENABLE_VECTOR3();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	VEC3_CLASS_HEAD::Set(const FLOAT* _xyz)
{
	return Set(_xyz[0], _xyz[1], _xyz[2]);
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	VEC3_CLASS_HEAD::Set(const TVec2& _xy, FLOAT _z)
{
	return Set(_xy[0], _xy[1], _z);
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	VEC3_CLASS_HEAD::Set(FLOAT _x, const TVec2& _yz)
{
	return Set(_x, _yz[0], _yz[1]);
}

VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	VEC3_CLASS_HEAD::SetX(const FLOAT _x)
{
	CHECK_ENABLE_FLOAT(_x);
	x = _x;
	return *this;
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	VEC3_CLASS_HEAD::SetY(const FLOAT _y)
{
	CHECK_ENABLE_FLOAT(_y);
	y = _y;
	return *this;
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	VEC3_CLASS_HEAD::SetZ(const FLOAT _z)
{
	CHECK_ENABLE_FLOAT(_z);
	z = _z;
	return *this;
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	VEC3_CLASS_HEAD::SetXY(const TVec2& _xy)
{
	SetX(_xy[0]); SetY(_xy[1]);
	return *this;
}

///-------------------------------------------------------------------------------------------------
/// 定数値
VEC_TEMPLATE_HEAD
const VEC3_CLASS_HEAD	VEC3_CLASS_HEAD::Zero(	+0.0f, +0.0f, +0.0f);
VEC_TEMPLATE_HEAD
const VEC3_CLASS_HEAD	VEC3_CLASS_HEAD::One(	+1.0f, +1.0f, +1.0f);
VEC_TEMPLATE_HEAD
const VEC3_CLASS_HEAD	VEC3_CLASS_HEAD::Left(	+1.0f, +0.0f, +0.0f);
VEC_TEMPLATE_HEAD
const VEC3_CLASS_HEAD	VEC3_CLASS_HEAD::Right(	-1.0f, +0.0f, +0.0f);
VEC_TEMPLATE_HEAD
const VEC3_CLASS_HEAD	VEC3_CLASS_HEAD::Up(	+0.0f, +1.0f, +0.0f);
VEC_TEMPLATE_HEAD
const VEC3_CLASS_HEAD	VEC3_CLASS_HEAD::Down(	+0.0f, -1.0f, +0.0f);
VEC_TEMPLATE_HEAD
const VEC3_CLASS_HEAD	VEC3_CLASS_HEAD::Front(	+0.0f, +0.0f, +1.0f);
VEC_TEMPLATE_HEAD
const VEC3_CLASS_HEAD	VEC3_CLASS_HEAD::Back(	+0.0f, +0.0f, -1.0f);




///*************************************************************************************************
/// ４ベクトルクラス
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// コンストラクタ
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD::TVec4()
{
	x = y = z = w = 0.0f;
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD::TVec4(FLOAT _xyzw)
{
	x = y = z = w = _xyzw;
	CHECK_ENABLE_VECTOR4();
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD::TVec4(FLOAT _x, FLOAT _y, FLOAT _z, FLOAT _w)
{
	x = _x; y = _y; z = _z; w = _w;
	CHECK_ENABLE_VECTOR4();
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD::TVec4(const FLOAT* _xyzw)
{
	Set(_xyzw);
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD::TVec4(const TVec3& _xyz, FLOAT _w)
{
	Set(_xyz, _w);
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD::TVec4(const TVec2& _xy, const TVec2& _zw)
{
	Set(_xy, _zw);
}

///-------------------------------------------------------------------------------------------------
/// compar operators
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	VEC4_CLASS_HEAD::operator=(const TVec4& _vec)
{
	x = _vec[0]; y = _vec[1]; z = _vec[2]; w = _vec[3];
	CHECK_ENABLE_VECTOR4();
	return *this;
}
VEC_TEMPLATE_HEAD
b8		VEC4_CLASS_HEAD::operator==(const TVec4& _vec) const
{
	return (x == _vec[0] && y == _vec[1] && z == _vec[2] && w == _vec[3]);
}
VEC_TEMPLATE_HEAD
b8		VEC4_CLASS_HEAD::operator!=(const TVec4& _vec) const
{
	return (x != _vec[0] || y != _vec[1] || z != _vec[2] || w != _vec[3]);
}

///-------------------------------------------------------------------------------------------------
/// assignment operators
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	VEC4_CLASS_HEAD::operator+=(const TVec4& _vec)
{
	x += _vec[0]; y += _vec[1]; z += _vec[2]; w += _vec[3];
	CHECK_ENABLE_VECTOR4();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD& VEC4_CLASS_HEAD::operator-=(const TVec4& _vec)
{
	x -= _vec[0]; y -= _vec[1]; z -= _vec[2]; w -= _vec[3];
	CHECK_ENABLE_VECTOR4();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD& VEC4_CLASS_HEAD::operator*=(const TVec4& _vec)
{
	x *= _vec[0]; y *= _vec[1]; z *= _vec[2]; w *= _vec[3];
	CHECK_ENABLE_VECTOR4();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD& VEC4_CLASS_HEAD::operator/=(const TVec4& _vec)
{
	x /= _vec[0]; y /= _vec[1]; z /= _vec[2]; w /= _vec[3];
	CHECK_ENABLE_VECTOR4();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD& VEC4_CLASS_HEAD::operator+=(FLOAT _scl)
{
	x += _scl; y += _scl; z += _scl; w += _scl;
	CHECK_ENABLE_VECTOR4();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD& VEC4_CLASS_HEAD::operator-=(FLOAT _scl)
{
	x -= _scl; y -= _scl; z -= _scl; w -= _scl;
	CHECK_ENABLE_VECTOR4();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD& VEC4_CLASS_HEAD::operator*=(FLOAT _scl)
{
	x *= _scl; y *= _scl; z *= _scl; w *= _scl;
	CHECK_ENABLE_VECTOR4();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	VEC4_CLASS_HEAD::operator/=(FLOAT _scl)
{
	x /= _scl; y /= _scl; z /= _scl; w /= _scl;
	CHECK_ENABLE_VECTOR4();
	return *this;
}

///-------------------------------------------------------------------------------------------------
/// binary operators
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD		VEC4_CLASS_HEAD::operator+(const TVec4& _vec) const
{
	return TVec4(x + _vec[0], y + _vec[1], z + _vec[2], w + _vec[3]);
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD		VEC4_CLASS_HEAD::operator-(const TVec4& _vec) const
{
	return TVec4(x - _vec[0], y - _vec[1], z - _vec[2], w - _vec[3]);
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD		VEC4_CLASS_HEAD::operator*(const TVec4& _vec) const
{
	return TVec4(x * _vec[0], y * _vec[1], z * _vec[2], w * _vec[3]);
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD		VEC4_CLASS_HEAD::operator/(const TVec4& _vec) const
{
	return TVec4(x / _vec[0], y / _vec[1], z / _vec[2], w / _vec[3]);
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD		VEC4_CLASS_HEAD::operator+(const FLOAT _scl) const
{
	return TVec4(x + _scl, y + _scl, z + _scl, w + _scl);
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD		VEC4_CLASS_HEAD::operator-(const FLOAT _scl) const
{
	return TVec4(x - _scl, y - _scl, z - _scl, w - _scl);
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD		VEC4_CLASS_HEAD::operator*(const FLOAT _scl) const
{
	return TVec4(x * _scl, y * _scl, z * _scl, w * _scl);
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD		VEC4_CLASS_HEAD::operator/(const FLOAT _scl) const
{
	return TVec4(x / _scl, y / _scl, z / _scl, w / _scl);
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD		VEC4_CLASS_HEAD::operator-() const
{
	return TVec4(-x, -y, -z, -w);
}


///-------------------------------------------------------------------------------------------------
/// Getter
VEC_TEMPLATE_HEAD
typename const VEC4_CLASS_HEAD::FLOAT	VEC4_CLASS_HEAD::GetX() const
{
	return x;
}
VEC_TEMPLATE_HEAD
typename const VEC4_CLASS_HEAD::FLOAT	VEC4_CLASS_HEAD::GetY() const
{
	return y;
}
VEC_TEMPLATE_HEAD
typename const VEC4_CLASS_HEAD::FLOAT	VEC4_CLASS_HEAD::GetZ() const
{
	return z;
}
VEC_TEMPLATE_HEAD
typename const VEC4_CLASS_HEAD::FLOAT	VEC4_CLASS_HEAD::GetW() const
{
	return w;
}

VEC_TEMPLATE_HEAD
typename VEC4_CLASS_HEAD::TVec2&	VEC4_CLASS_HEAD::GetXY()
{
	return *PCast<TVec2*>(this);
}
VEC_TEMPLATE_HEAD
typename VEC4_CLASS_HEAD::TVec3&	VEC4_CLASS_HEAD::GetXYZ()
{
	return *PCast<TVec3*>(this);
}

///-------------------------------------------------------------------------------------------------
/// Setter
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	VEC4_CLASS_HEAD::Set(FLOAT _v)
{
	x = _v; y = _v; z = _v; w = _v;
	CHECK_ENABLE_VECTOR4();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	VEC4_CLASS_HEAD::Set(FLOAT _x, FLOAT _y, FLOAT _z, FLOAT _w)
{
	x = _x; y = _y; z = _z; w = _w;
	CHECK_ENABLE_VECTOR4();
	return *this;
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	VEC4_CLASS_HEAD::Set(const FLOAT* _xyzw)
{
	return Set(_xyzw[0], _xyzw[1], _xyzw[2], _xyzw[3]);
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	VEC4_CLASS_HEAD::Set(const TVec3& _xyz, FLOAT _w)
{
	return Set(_xyz[0], _xyz[1], _xyz[2], _w);
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	VEC4_CLASS_HEAD::Set(const TVec2& _xy, const TVec2& _zw)
{
	return Set(_xy[0], _xy[1], _zw[0], _zw[1]);
}

VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	VEC4_CLASS_HEAD::SetX(const FLOAT _x)
{
	CHECK_ENABLE_FLOAT(_x);
	x = _x;
	return *this;
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	VEC4_CLASS_HEAD::SetY(const FLOAT _y)
{
	CHECK_ENABLE_FLOAT(_y);
	y = _y;
	return *this;
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	VEC4_CLASS_HEAD::SetZ(const FLOAT _z)
{
	CHECK_ENABLE_FLOAT(_z);
	z = _z;
	return *this;
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	VEC4_CLASS_HEAD::SetW(const FLOAT _w)
{
	CHECK_ENABLE_FLOAT(_w);
	w = _w;
	return *this;
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	VEC4_CLASS_HEAD::SetXY(const TVec2& _xy)
{
	SetX(_xy[0]); SetY(_xy[1]);
	return *this;
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	VEC4_CLASS_HEAD::SetXYZ(const TVec3& _xyz)
{
	SetX(_xyz[0]); SetY(_xyz[1]); SetZ(_xyz[2]);
	return *this;
}

///-------------------------------------------------------------------------------------------------
/// 定数値
VEC_TEMPLATE_HEAD
const VEC4_CLASS_HEAD	VEC4_CLASS_HEAD::Zero(		+0.0f, +0.0f, +0.0f, +0.0f);
VEC_TEMPLATE_HEAD
const VEC4_CLASS_HEAD	VEC4_CLASS_HEAD::One(		+1.0f, +1.0f, +1.0f, +1.0f);
VEC_TEMPLATE_HEAD
const VEC4_CLASS_HEAD	VEC4_CLASS_HEAD::Left(		+1.0f, +0.0f, +0.0f, +1.0f);
VEC_TEMPLATE_HEAD
const VEC4_CLASS_HEAD	VEC4_CLASS_HEAD::Right(		-1.0f, +0.0f, +0.0f, +1.0f);
VEC_TEMPLATE_HEAD
const VEC4_CLASS_HEAD	VEC4_CLASS_HEAD::Up(		+0.0f, +1.0f, +0.0f, +1.0f);
VEC_TEMPLATE_HEAD
const VEC4_CLASS_HEAD	VEC4_CLASS_HEAD::Down(		+0.0f, -1.0f, +0.0f, +1.0f);
VEC_TEMPLATE_HEAD
const VEC4_CLASS_HEAD	VEC4_CLASS_HEAD::Front(		+0.0f, +0.0f, +1.0f, +1.0f);
VEC_TEMPLATE_HEAD
const VEC4_CLASS_HEAD	VEC4_CLASS_HEAD::Back(		+0.0f, +0.0f, -1.0f, +1.0f);

VEC_TEMPLATE_HEAD
const VEC4_CLASS_HEAD	VEC4_CLASS_HEAD::Black(		+0.0f, +0.0f, +0.0f, +0.0f);
VEC_TEMPLATE_HEAD
const VEC4_CLASS_HEAD	VEC4_CLASS_HEAD::White(		+1.0f, +1.0f, +1.0f, +1.0f);
VEC_TEMPLATE_HEAD
const VEC4_CLASS_HEAD	VEC4_CLASS_HEAD::Gray(		+0.5f, +0.5f, +0.5f, +0.5f);
VEC_TEMPLATE_HEAD
const VEC4_CLASS_HEAD	VEC4_CLASS_HEAD::Red(		+1.0f, +0.0f, +0.0f, +1.0f);
VEC_TEMPLATE_HEAD
const VEC4_CLASS_HEAD	VEC4_CLASS_HEAD::Green(		+0.0f, +1.0f, +0.0f, +1.0f);
VEC_TEMPLATE_HEAD
const VEC4_CLASS_HEAD	VEC4_CLASS_HEAD::Blue(		+0.0f, +0.0f, +1.0f, +1.0f);
VEC_TEMPLATE_HEAD
const VEC4_CLASS_HEAD	VEC4_CLASS_HEAD::Yellow(	+1.0f, +1.0f, +0.0f, +1.0f);
VEC_TEMPLATE_HEAD
const VEC4_CLASS_HEAD	VEC4_CLASS_HEAD::Turquoise(	+0.0f, +1.0f, +1.0f, +1.0f);
VEC_TEMPLATE_HEAD
const VEC4_CLASS_HEAD	VEC4_CLASS_HEAD::Purple(	+1.0f, +0.0f, +1.0f, +1.0f);


POISON_END

#endif	// __VECTOR_INL__

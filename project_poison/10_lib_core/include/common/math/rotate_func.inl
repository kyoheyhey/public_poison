﻿//#pragma once
#ifndef __ROTATE_FUNC_INL__
#define __ROTATE_FUNC_INL__


POISON_BGN

//-------------------------------------------------------------------------------------------------
/// テンプレート宣言部分の省略
//#define VEC_TEMPLATE_HEAD		template< typename VEC >
//#define VEC2_CLASS_HEAD		TVec2< VEC >
//#define ROT_VEC3_CLASS_HEAD		TVec3< VEC >
#define ROT_VEC3_CLASS_HEAD		TVec3< typename VEC::VEC3 >
//#define VEC4_CLASS_HEAD		TVec4< VEC >
//#define EULER_CLASS_HEAD		TEuler< VEC >
//#define QUAT_CLASS_HEAD		TQuat< VEC >


///*************************************************************************************************
/// 回転関数群
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// クォータニオンの正規化
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::Identity(QUAT_CLASS_HEAD& _ret)
{
	_ret.x = 0.0f;
	_ret.y = 0.0f;
	_ret.z = 0.0f;
	_ret.w = 1.0f;
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// クォータニオンの加算Ａ＋Ｂ
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::Add(QUAT_CLASS_HEAD& _ret, const QUAT_CLASS_HEAD& _quatA, const QUAT_CLASS_HEAD& _quatB)
{
	_ret.Set(_quatA.x + _quatB.x, _quatA.y + _quatB.y, _quatA.z + _quatB.z, _quatA.w + _quatB.w);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// クォータニオンの減算Ａ－Ｂ
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::Sub(QUAT_CLASS_HEAD& _ret, const QUAT_CLASS_HEAD& _quatA, const QUAT_CLASS_HEAD& _quatB)
{
	_ret.Set(_quatA.x - _quatB.x, _quatA.y - _quatB.y, _quatA.z - _quatB.z, _quatA.w - _quatB.w);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// クォータニオンの乗算Ａ＊Ｂ
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::Mul(QUAT_CLASS_HEAD& _ret, const QUAT_CLASS_HEAD& _quatA, const QUAT_CLASS_HEAD& _quatB)
{
	_ret.w = (_quatA.w * _quatB.w) - (_quatA.x * _quatB.x) - (_quatA.y * _quatB.y) - (_quatA.z * _quatB.z);
	_ret.x = (_quatA.y * _quatB.z) - (_quatA.z * _quatB.y) + (_quatA.w * _quatB.x) + (_quatA.x * _quatB.w);
	_ret.y = (_quatA.z * _quatB.x) - (_quatA.z * _quatB.z) + (_quatA.w * _quatB.y) + (_quatA.y * _quatB.w);
	_ret.z = (_quatA.x * _quatB.y) - (_quatA.z * _quatB.x) + (_quatA.w * _quatB.z) + (_quatA.z * _quatB.w);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// クォータニオンのスカラー倍
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::Scale(QUAT_CLASS_HEAD& _ret, const QUAT_CLASS_HEAD& _quat, const typename QUAT_CLASS_HEAD::FLOAT _s)
{
	_ret.w = _quat.w * _s;
	_ret.x = _quat.x * _s;
	_ret.y = _quat.y * _s;
	_ret.z = _quat.z * _s;
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// クォータニオンの絶対値
VEC_TEMPLATE_HEAD
typename QUAT_CLASS_HEAD::FLOAT		Rot::Length(const QUAT_CLASS_HEAD& _quat)
{
	return Vec::Length(_quat.GetXYZW());
}

///-------------------------------------------------------------------------------------------------
/// クォータニオンの絶対値の２乗
VEC_TEMPLATE_HEAD
typename QUAT_CLASS_HEAD::FLOAT		Rot::Length2(const QUAT_CLASS_HEAD& _quat)
{
	return Vec::Length2(_quat.GetXYZW());
}

///-------------------------------------------------------------------------------------------------
/// クォータニオンの正規化
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::Normalize(QUAT_CLASS_HEAD& _ret, const QUAT_CLASS_HEAD& _src)
{
	Vec::Normalize(_ret.GetXYZW(), _src.GetXYZW());
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// クォータニオンの逆数
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::Inverse(QUAT_CLASS_HEAD& _ret, const QUAT_CLASS_HEAD& _src)
{
	const QUAT_CLASS_HEAD::FLOAT mag = -1.0f / Vec::Length2(_src.GetXYZW());
	_ret = _src * mag;
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 任意軸回転クォータニオン
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::Rotate(QUAT_CLASS_HEAD& _ret, const ROT_VEC3_CLASS_HEAD& _rot, const typename QUAT_CLASS_HEAD::FLOAT _angle)
{
	QUAT_CLASS_HEAD::FLOAT s = sinf(_angle);
	_ret.w = cosf(_angle);
	_ret.x = _rot.x * s;
	_ret.y = _rot.y * s;
	_ret.z = _rot.z * s;
	return _ret;
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::Rotate(QUAT_CLASS_HEAD& _ret, const VEC4_CLASS_HEAD& _rot, const typename QUAT_CLASS_HEAD::FLOAT _angle)
{
	QUAT_CLASS_HEAD::FLOAT s = sinf(_angle);
	_ret.w = cosf(_angle);
	_ret.x = _rot.x * s;
	_ret.y = _rot.y * s;
	_ret.z = _rot.z * s;
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 固定軸回転クォータニオン
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::RotateX(QUAT_CLASS_HEAD& _ret, const typename QUAT_CLASS_HEAD::FLOAT _angle)
{
	Rotate(_ret, ROT_VEC3_CLASS_HEAD::Left, _angle);
	return _ret;
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::RotateY(QUAT_CLASS_HEAD& _ret, const typename QUAT_CLASS_HEAD::FLOAT _angle)
{
	Rotate(_ret, ROT_VEC3_CLASS_HEAD::Up, _angle);
	return _ret;
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::RotateZ(QUAT_CLASS_HEAD& _ret, const typename QUAT_CLASS_HEAD::FLOAT _angle)
{
	Rotate(_ret, ROT_VEC3_CLASS_HEAD::Front, _angle);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// XYZ回転クォータニオン
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::RotateXYZ(QUAT_CLASS_HEAD& _ret, const typename QUAT_CLASS_HEAD::FLOAT _x, const typename QUAT_CLASS_HEAD::FLOAT _y, const typename QUAT_CLASS_HEAD::FLOAT _z)
{
	Rotate(_ret, ROT_VEC3_CLASS_HEAD::Front, _z);
	QUAT_CLASS_HEAD temp;
	Rotate(temp, ROT_VEC3_CLASS_HEAD::Up, _y);
	_ret *= temp;
	Rotate(temp, ROT_VEC3_CLASS_HEAD::Left, _x);
	_ret *= temp;
	return _ret;
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::RotateXYZ(QUAT_CLASS_HEAD& _ret, const typename QUAT_CLASS_HEAD::FLOAT* _xyz)
{
	return RotateXYZ(_ret, _xyz[0], _xyz[1], _xyz[2]);
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::RotateXYZ(QUAT_CLASS_HEAD& _ret, const ROT_VEC3_CLASS_HEAD& _xyz)
{
	return RotateXYZ(_ret, _xyz[0], _xyz[1], _xyz[2]);
}

///-------------------------------------------------------------------------------------------------
/// XZY回転クォータニオン
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::RotateXZY(QUAT_CLASS_HEAD& _ret, const typename QUAT_CLASS_HEAD::FLOAT _x, const typename QUAT_CLASS_HEAD::FLOAT _y, const typename QUAT_CLASS_HEAD::FLOAT _z)
{
	Rotate(_ret, ROT_VEC3_CLASS_HEAD::Up, _y);
	QUAT_CLASS_HEAD temp;
	Rotate(temp, ROT_VEC3_CLASS_HEAD::Front, _z);
	_ret *= temp;
	Rotate(temp, ROT_VEC3_CLASS_HEAD::Left, _x);
	_ret *= temp;
	return _ret;
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::RotateXZY(QUAT_CLASS_HEAD& _ret, const typename QUAT_CLASS_HEAD::FLOAT* _xyz)
{
	return RotateXZY(_ret, _xyz[0], _xyz[1], _xyz[2]);
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::RotateXZY(QUAT_CLASS_HEAD& _ret, const ROT_VEC3_CLASS_HEAD& _xyz)
{
	return RotateXZY(_ret, _xyz[0], _xyz[1], _xyz[2]);
}

///-------------------------------------------------------------------------------------------------
/// YXZ回転クォータニオン
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::RotateYXZ(QUAT_CLASS_HEAD& _ret, const typename QUAT_CLASS_HEAD::FLOAT _x, const typename QUAT_CLASS_HEAD::FLOAT _y, const typename QUAT_CLASS_HEAD::FLOAT _z)
{
	Rotate(_ret, ROT_VEC3_CLASS_HEAD::Front, _z);
	QUAT_CLASS_HEAD temp;
	Rotate(temp, ROT_VEC3_CLASS_HEAD::Left, _x);
	_ret *= temp;
	Rotate(temp, ROT_VEC3_CLASS_HEAD::Up, _y);
	_ret *= temp;
	return _ret;
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::RotateYXZ(QUAT_CLASS_HEAD& _ret, const typename QUAT_CLASS_HEAD::FLOAT* _xyz)
{
	return RotateYXZ(_ret, _xyz[0], _xyz[1], _xyz[2]);
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::RotateYXZ(QUAT_CLASS_HEAD& _ret, const ROT_VEC3_CLASS_HEAD& _xyz)
{
	return RotateYXZ(_ret, _xyz[0], _xyz[1], _xyz[2]);
}

///-------------------------------------------------------------------------------------------------
/// YZX回転クォータニオン
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::RotateYZX(QUAT_CLASS_HEAD& _ret, const typename QUAT_CLASS_HEAD::FLOAT _x, const typename QUAT_CLASS_HEAD::FLOAT _y, const typename QUAT_CLASS_HEAD::FLOAT _z)
{
	Rotate(_ret, ROT_VEC3_CLASS_HEAD::Left, _x);
	QUAT_CLASS_HEAD temp;
	Rotate(temp, ROT_VEC3_CLASS_HEAD::Front, _z);
	_ret *= temp;
	Rotate(temp, ROT_VEC3_CLASS_HEAD::Up, _y);
	_ret *= temp;
	return _ret;
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::RotateYZX(QUAT_CLASS_HEAD& _ret, const typename QUAT_CLASS_HEAD::FLOAT* _xyz)
{
	return RotateYZX(_ret, _xyz[0], _xyz[1], _xyz[2]);
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::RotateYZX(QUAT_CLASS_HEAD& _ret, const ROT_VEC3_CLASS_HEAD& _xyz)
{
	return RotateYZX(_ret, _xyz[0], _xyz[1], _xyz[2]);
}

///-------------------------------------------------------------------------------------------------
/// ZXY回転クォータニオン
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::RotateZXY(QUAT_CLASS_HEAD& _ret, const typename QUAT_CLASS_HEAD::FLOAT _x, const typename QUAT_CLASS_HEAD::FLOAT _y, const typename QUAT_CLASS_HEAD::FLOAT _z)
{
	Rotate(_ret, ROT_VEC3_CLASS_HEAD::Up, _y);
	QUAT_CLASS_HEAD temp;
	Rotate(temp, ROT_VEC3_CLASS_HEAD::Left, _x);
	_ret *= temp;
	Rotate(temp, ROT_VEC3_CLASS_HEAD::Front, _z);
	_ret *= temp;
	return _ret;
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::RotateZXY(QUAT_CLASS_HEAD& _ret, const typename QUAT_CLASS_HEAD::FLOAT* _xyz)
{
	return RotateZXY(_ret, _xyz[0], _xyz[1], _xyz[2]);
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::RotateZXY(QUAT_CLASS_HEAD& _ret, const ROT_VEC3_CLASS_HEAD& _xyz)
{
	return RotateZXY(_ret, _xyz[0], _xyz[1], _xyz[2]);
}

///-------------------------------------------------------------------------------------------------
/// ZYX回転クォータニオン
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::RotateZYX(QUAT_CLASS_HEAD& _ret, const typename QUAT_CLASS_HEAD::FLOAT _x, const typename QUAT_CLASS_HEAD::FLOAT _y, const typename QUAT_CLASS_HEAD::FLOAT _z)
{
	Rotate(_ret, ROT_VEC3_CLASS_HEAD::Left, _x);
	QUAT_CLASS_HEAD temp;
	Rotate(temp, ROT_VEC3_CLASS_HEAD::Up, _y);
	_ret *= temp;
	Rotate(temp, ROT_VEC3_CLASS_HEAD::Front, _z);
	_ret *= temp;
	return _ret;
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::RotateZYX(QUAT_CLASS_HEAD& _ret, const typename QUAT_CLASS_HEAD::FLOAT* _xyz)
{
	return RotateZYX(_ret, _xyz[0], _xyz[1], _xyz[2]);
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::RotateZYX(QUAT_CLASS_HEAD& _ret, const ROT_VEC3_CLASS_HEAD& _xyz)
{
	return RotateZYX(_ret, _xyz[0], _xyz[1], _xyz[2]);
}

///-------------------------------------------------------------------------------------------------
/// クォータニオン同士を線形補間
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::Lerp(QUAT_CLASS_HEAD& _ret, const QUAT_CLASS_HEAD& _quatA, const QUAT_CLASS_HEAD& _quatB, const typename QUAT_CLASS_HEAD::FLOAT _t)
{
	// ベクトルで計算
	Vec::Lerp(_ret.GetXYZW(), _quatA.GetXYZW(), _quatB.GetXYZW(), _t);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
///クォータニオン同士を球面線形補間
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::Slerp(QUAT_CLASS_HEAD& _ret, const QUAT_CLASS_HEAD& _quatA, const QUAT_CLASS_HEAD& _quatB, const typename QUAT_CLASS_HEAD::FLOAT _t)
{
	QUAT_CLASS_HEAD::FLOAT qr = (_quatA.w*_quatB.w) + (_quatA.x*_quatB.x) + (_quatA.y*_quatB.y) + (_quatA.z*_quatB.z);
	CVec2 s;
	s.x = 1.0f - (qr * qr);
	if (s.x == 0.0f) {
		_ret = _quatA;
		return _ret;
	}
	QUAT_CLASS_HEAD::FLOAT ph = acosf(qr);
	if (ph < 0.0f && ph > PM_PI / 2.0f)
	{
		qr = -(_quatA.w*_quatB.w) - (_quatA.x*_quatB.x) - (_quatA.y*_quatB.y) - (_quatA.z*_quatB.z);
		ph = acosf(qr);
		s.x = sinf(ph * (1.0f - _t)) / sinf(ph);
		s.y = sinf(ph * _t) / sinf(ph);
		_ret.x = (_quatA.x * s.x) - (_quatB.x * s.y);
		_ret.y = (_quatA.y * s.x) - (_quatB.y * s.y);
		_ret.z = (_quatA.z * s.x) - (_quatB.z * s.y);
		_ret.w = (_quatA.w * s.x) - (_quatB.w * s.y);
	}
	else
	{
		s.x = sinf(ph * (1.0f - _t)) / sinf(ph);
		s.y = sinf(ph * _t) / sinf(ph);
		_ret.x = (_quatA.x * s.x) - (_quatB.x * s.y);
		_ret.y = (_quatA.y * s.x) - (_quatB.y * s.y);
		_ret.z = (_quatA.z * s.x) - (_quatB.z * s.y);
		_ret.w = (_quatA.w * s.x) - (_quatB.w * s.y);
	}
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 回転行列からクォータニオンに変換
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Rot::EulerToQuat(QUAT_CLASS_HEAD& _ret, const EULER_CLASS_HEAD& _euler)
{
	typedef QUAT_CLASS_HEAD& (*ROT_FUNC)(QUAT_CLASS_HEAD&, const ROT_VEC3_CLASS_HEAD&);
	static const ROT_FUNC s_rotFunc[] =
	{
		&RotateXYZ,
		&RotateXZY,
		&RotateYXZ,
		&RotateYZX,
		&RotateZXY,
		&RotateZYX,
	};
	StaticAssert(ARRAYOF(s_rotFunc) == EULER_CLASS_HEAD::ROT_TYPE_NUM);
	libAssert(_euler.GetRotType() < EULER_CLASS_HEAD::ROT_TYPE_NUM);
	return s_rotFunc[_euler.GetRotType()](_ret, _euler.GetXYZ());
}

VEC_TEMPLATE_HEAD
EULER_CLASS_HEAD&	Rot::QuatToEulerXYZ(EULER_CLASS_HEAD& _ret, const QUAT_CLASS_HEAD& _quat)
{
	VEC::FLOAT l = Vec::Length2(_quat.getXYZW());
	if (l <= 0.0f) {
		_ret.Set(0.0f, 0.0f, 0.0f);
		return _ret;
	}

	const VEC::FLOAT s = 2.f / l;

	const VEC::FLOAT xs = _quat[0] * s;
	const VEC::FLOAT ys = _quat[1] * s;
	const VEC::FLOAT zs = _quat[2] * s;

	const VEC::FLOAT wx = _quat[3] * xs;
	const VEC::FLOAT wy = _quat[3] * ys;
	const VEC::FLOAT wz = _quat[3] * zs;

	const VEC::FLOAT xx = _quat[0] * xs;
	const VEC::FLOAT yy = _quat[1] * ys;
	const VEC::FLOAT zz = _quat[2] * zs;

	const VEC::FLOAT xy = _quat[0] * ys;
	const VEC::FLOAT yz = _quat[1] * zs;
	const VEC::FLOAT zx = _quat[2] * xs;

	_ret[0][0] = 1.f - (yy + zz);
	MATRIX_VAL(_ret, 0, 1) = xy + wz;
	MATRIX_VAL(_ret, 0, 2) = zx - wy;

	MATRIX_VAL(_ret, 1, 0) = xy - wz;
	_ret[1][1] = 1.f - (xx + zz);
	MATRIX_VAL(_ret, 1, 2) = yz + wx;

	MATRIX_VAL(_ret, 2, 0) = zx + wy;
	MATRIX_VAL(_ret, 2, 1) = yz - wx;
	_ret[2][2] = 1.f - (xx + yy);
	return _ret;
}


////-------------------------------------------------------------------------------------------------
// オイラーで座標の射影変換
VEC_TEMPLATE_HEAD
ROT_VEC3_CLASS_HEAD&	Rot::Transform(ROT_VEC3_CLASS_HEAD& _ret, const EULER_CLASS_HEAD& _euler, const ROT_VEC3_CLASS_HEAD& _src)
{
	// ToDo:

	//CMtx33 temp;
	//Mtx::Rotate(temp, _euler);
	//return Transform(_ret, temp, _src);

	return _ret;
}

VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	Rot::Transform(VEC4_CLASS_HEAD& _ret, const EULER_CLASS_HEAD& _euler, const VEC4_CLASS_HEAD& _src)
{
	Transform(_ret.GetXYZ(), _euler, _src.GetXYZ());
	return _ret;
}

////-------------------------------------------------------------------------------------------------
// クォータニオンで座標の射影変換
VEC_TEMPLATE_HEAD
ROT_VEC3_CLASS_HEAD&	Rot::Transform(ROT_VEC3_CLASS_HEAD& _ret, const QUAT_CLASS_HEAD& _quat, const ROT_VEC3_CLASS_HEAD& _src)
{
	QUAT_CLASS_HEAD temp;
	temp.w = -(_quat.x*_src.x) - (_quat.y*_src.y) - (_quat.z*_src.z);
	temp.x = +(_quat.y*_src.z) - (_quat.z*_src.y) + (_quat.w*_src.x);
	temp.y = +(_quat.z*_src.x) - (_quat.x*_src.z) + (_quat.w*_src.y);
	temp.z = +(_quat.x*_src.y) - (_quat.y*_src.x) + (_quat.w*_src.z);

	_ret.x = -(temp.y*_quat.z) + (temp.z*_quat.y) - (temp.w*_quat.x) + (temp.x*_quat.w);
	_ret.y = -(temp.z*_quat.x) + (temp.x*_quat.z) - (temp.w*_quat.y) + (temp.y*_quat.w);
	_ret.z = -(temp.x*_quat.y) + (temp.y*_quat.x) - (temp.w*_quat.z) + (temp.z*_quat.w);
	return _ret;
}

VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	Rot::Transform(VEC4_CLASS_HEAD& _ret, const QUAT_CLASS_HEAD& _quat, const VEC4_CLASS_HEAD& _src)
{
	Transform(_ret.GetXYZ(), _quat, _src.GetXYZ());
	return _ret;
}


POISON_END

#endif	// __ROTATE_FUNC_INL__

﻿//#pragma once
#ifndef	__GEOMETRY_H__
#define	__GEOMETRY_H__

//-------------------------------------------------------------------------------------------------
// include
#include "vector.h"
#include "matrix.h"


POISON_BGN

//*************************************************************************************************
//@brief	平面クラス
//*************************************************************************************************
template< typename VEC >
class TPlane
{
private:
	friend class	Geometry;

private:
	typedef	typename VEC::VEC2	VEC2;
	typedef	typename VEC::VEC3	VEC3;

public:
	//@brief	型定義
	typedef	typename VEC::FLOAT	FLOAT;
	//@brief	ベクトル型
	typedef	TVec2<VEC2>			TVec2;
	typedef	TVec3<VEC3>			TVec3;
	typedef	TVec4<VEC>			TVec4;

public:
	TPlane() {}
	TPlane(const TVec3& _nrm, FLOAT _dot)
		: plane(_nrm, _dot)
	{}
	TPlane(FLOAT _x, FLOAT _y, FLOAT _z, FLOAT _dot)
		: plane(_x, _y, _z, _dot)
	{}
	TPlane(const TVec3& _p0, const TVec3& _p1, const TVec3& _p2)
	{
		TVec3 nrm;
		Vec::Cross(nrm, _p1 - _p0, _p2 - _p0);
		Vec::Normalize(GetNrm(), nrm);
		SetDot(Vec::Dot(_p0, GetNrm()));
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	法線取得・設定
	const TVec3&	GetNrm() const {return plane.GetXYZ(); }
	TVec3&			GetNrm() {return plane.GetXYZ(); }
	void			SetNrm(const TVec3& _nrm) { plane.SetXYZ(_nrm); }
	
	//-------------------------------------------------------------------------------------------------
	//@brief	内積取得・設定
	const FLOAT&	GetDot() const {return plane.GetW(); }
	FLOAT&			GetDot() {return plane.GetW(); }
	void			SetDot(FLOAT _dot) { plane.SetW(_dot); }

public:
	TVec4	plane;

};

//*************************************************************************************************
//@brief	SPHERE
//*************************************************************************************************
template< typename VEC >
class TSphere
{
private:
	friend class	Geometry;

private:
	typedef	typename VEC::VEC2	VEC2;
	typedef	typename VEC::VEC3	VEC3;

public:
	//@brief	型定義
	typedef	typename VEC::FLOAT	FLOAT;
	//@brief	ベクトル型
	typedef	TVec2<VEC2>			TVec2;
	typedef	TVec3<VEC3>			TVec3;
	typedef	TVec4<VEC>			TVec4;

public:
	TSphere() {}
	TSphere(const TVec3& _pos, FLOAT _radius)
		: sphere(_pos, _radius)
	{}
	TSphere(FLOAT _x, FLOAT _y, FLOAT _z, FLOAT _radius)
		: sphere(_x, _y, _z, _radius)
	{}
	~TSphere() {}
	
	//-------------------------------------------------------------------------------------------------
	//@brief	位置取得・設定
	const TVec3&	GetPos() const {return sphere.GetXYZ(); }
	TVec3&			GetPos() {return sphere.GetXYZ(); }
	void			SetPos(const TVec3& _pos) { sphere.SetXYZ(_pos); }
	
	//-------------------------------------------------------------------------------------------------
	//@brief	半径取得・設定
	const FLOAT&	GetRadius() const {return sphere.GetW(); }
	FLOAT&			GetRadius() {return sphere.GetW(); }
	void			SetRadius(FLOAT _radius) { sphere.SetW(_radius); }

public:
	TVec4	sphere;
};

//*************************************************************************************************
//@brief	AABB
//*************************************************************************************************
template< typename VEC >
class TAabb
{
private:
	friend class	Geometry;

public:
	//@brief	型定義
	typedef	typename VEC::FLOAT	FLOAT;
	//@brief	ベクトル型
	typedef	TVec3<VEC>			TVec3;

public:
	TAabb() {}
	TAabb(const TVec3& _min, const TVec3& _max)
		: min(_min)
		, max(_max)
	{}

public:
	TVec3	min;
	TVec3	max;
};

//*************************************************************************************************
///@brief	OBB
//*************************************************************************************************
template< typename VEC >
class TObb
{
private:
	friend class	Geometry;

public:
	//@brief	型定義
	typedef	typename VEC::FLOAT	FLOAT;
	//@brief	ベクトル型
	typedef	TVec3<VEC>			TVec3;

public:
	TObb() 
	{
		axis[0] = TVec3::Left;
		axis[1] = TVec3::Up;
		axis[2] = TVec3::Foward;
	}
	TObb(const TVec3& _center, const TVec3& _scale)
		: center(_center)
		, scale(_scale)
	{
		axis[0] = TVec3::Left;
		axis[1] = TVec3::Up;
		axis[2] = TVec3::Foward;
	}

public:
	TVec3	center;
	TVec3	scale;
	TVec3	axis[3];
};

//*************************************************************************************************
///@brief	Frustum
//*************************************************************************************************
template< typename VEC, u32 PLANE_NUM = 8 >
class TFrustum
{
private:
	friend class	Geometry;

private:
	typedef	typename VEC::VEC2	VEC2;
	typedef	typename VEC::VEC3	VEC3;

public:
	//@brief	型定義
	typedef	typename VEC::FLOAT	FLOAT;
	//@brief	ベクトル型
	typedef	TVec2<VEC2>			TVec2;
	typedef	TVec3<VEC3>			TVec3;
	typedef	TVec4<VEC>			TVec4;
	//@brief	平面型
	typedef	TPlane<VEC>			TPlane;

public:
	TFrustum();
	TFrustum(const TPlane (&_plane)[6])
	{
		for (u32 i = 0; i < ARRAYOF(_plane); i++)
		{
			plane[i] = _plane[i];
		}
	}
	TFrustum(const TPlane (&_plane)[8])
	{
		for (u32 i = 0; i < ARRAYOF(_plane); i++)
		{
			plane[i] = _plane[i];
		}
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	平行取得・設定
	const TPlane&	GetPlane(u32 _idx) const { return plane[_idx]; }
	TPlane&			GetPlane(u32 _idx) { return plane[_idx]; }
	void			SetPlane(u32 _idx, const TPlane& _plane) { plane[_idx] = _plane; }

	//-------------------------------------------------------------------------------------------------
	//@brief	平行数取得
	static u32		GetPlaneNum() { return PLANE_NUM; }

public:
	TPlane	plane[PLANE_NUM];
};


//*************************************************************************************************
//@brief	幾何学演算ユーティリティ
//*************************************************************************************************
class Geometry
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	平面を算出
	//@return	Plane
	//@param[in]	_plane		Plane
	//@param[in]	_pos		平面上の点
	//@param[in]	_nrm		法線(正規化されていること)
	template< typename VEC3, typename VEC4 >
	static TPlane<VEC4>&		SetPlane(TPlane<VEC4>& _plane, const TVec3<VEC3>& _pos, const TVec3<VEC3>& _nrm);
	//-------------------------------------------------------------------------------------------------
	//@brief	平面を算出
	//@return	Plane
	//@param[in]	_plane		Plane
	//@param[in]	_pos		平面をなす3点(反時計回り)
	template< typename VEC3, typename VEC4 >
	static TPlane<VEC4>&		SetPlane(TPlane<VEC4>& _plane, const TVec3<VEC3>& _pos0, const TVec3<VEC3>& _pos1, const TVec3<VEC3>& _pos2);
	template< typename VEC3, typename VEC4 >
	static TPlane<VEC4>&		SetPlane(TPlane<VEC4>& _plane, const TVec3<VEC3> (&_pos)[3]);
	template< typename VEC4 >
	static TPlane<VEC4>&		SetPlane(TPlane<VEC4>& _plane, const TVec4<VEC4> (&_pos)[3]);

	//-------------------------------------------------------------------------------------------------
	//@brief	平面を１次変換
	//@return	Plane
	//@param[in]	_plane		Plane
	//@param[in]	_mtx		変換行列
	template< typename VEC, typename MTX >
	static TPlane<VEC>&		TransformPlane(TPlane<VEC>& _ret, const TPlane<VEC>& _plane, const TMtx44<MTX>& _mtx);


public:
	//-------------------------------------------------------------------------------------------------
	//@brief	三角形に対するPからなる重心座標
	//@return	重心座標
	//@param[out]	_ret		重心座標
	//@param[in]	_p0			点
	//@param[in]	_t0			三角形の頂点0
	//@param[in]	_t1			三角形の頂点1
	//@param[in]	_t2			三角形の頂点2
	template< typename VEC >
	static TVec3<VEC>	Barycentric(TVec3<VEC>& _ret, const TVec3<VEC>& _p0, const TVec3<VEC>& _t0, const TVec3<VEC>& _t1, const TVec3<VEC>& _t2);
	template< typename VEC >
	static TVec3<VEC>	Barycentric(TVec3<VEC>& _ret, const TVec3<VEC>& _p0, const TVec3<VEC>(&_t)[3]) { return Barycentric(_ret, _p0, _t[0], _t[1], _t[2]); }

	//-------------------------------------------------------------------------------------------------
	//@brief	点と線分の最近接点
	//@return	平方距離
	//@param[out]	_ret		最近接点
	//@param[in]	_p0			点
	//@param[in]	_s0			線分の頂点0
	//@param[in]	_s1			線分の頂点1
	//@param[out]	_t			距離算出に使った点の内分率
	template< typename VEC >
	static typename VEC::FLOAT	ClosestPtPointSegment(TVec3<VEC>& _ret, const TVec3<VEC>& _p0, const TVec3<VEC>& _s0, const TVec3<VEC>& _s1, typename VEC::FLOAT* _t = NULL);
	
	//-------------------------------------------------------------------------------------------------
	//@brief	点と線分の距離
	//@return	平方距離
	//@param[in]	_p0			点
	//@param[in]	_s0			線分の頂点0
	//@param[in]	_s1			線分の頂点1
	template< typename VEC >
	static typename VEC::FLOAT	SqDistPointSegment(const TVec3<VEC>& _p0, const TVec3<VEC>& _s0, const TVec3<VEC>& _s1);

	//-------------------------------------------------------------------------------------------------
	//@brief	点と三角形の最近接点
	//@return	平方距離
	//@param[out]	_ret		最近接点
	//@param[in]	_p0			点
	//@param[in]	_t0			三角形の頂点0
	//@param[in]	_t1			三角形の頂点1
	//@param[in]	_t2			三角形の頂点2
	template< typename VEC >
	static typename VEC::FLOAT	ClosestPtPointTriangle(TVec3<VEC>& _ret, const TVec3<VEC>& _p0, const TVec3<VEC>& _t0, const TVec3<VEC>& _t1, const TVec3<VEC>& _t2);
	template< typename VEC >
	static typename VEC::FLOAT	ClosestPtPointTriangle(TVec3<VEC>& _ret, const TVec3<VEC>& _p0, const TVec3<VEC>(&_t)[3]) { return ClosestPtPointTriangle(_ret, _p0, _t[0], _t[1], _t[2]); }


public:
	//-------------------------------------------------------------------------------------------------
	//@brief	線分と線分の最近接点
	//@return	平方距離
	//@param[out]	_ret0		線分0の最近接点
	//@param[out]	_ret1		線分1の最近接点
	//@param[in]	_s00		線分0の頂点0
	//@param[in]	_s01		線分0の頂点1
	//@param[in]	_s10		線分1の頂点0
	//@param[in]	_s11		線分1の頂点1
	//@param[out]	_t0			線分0の距離算出に使った点の内分率
	//@param[out]	_t1			線分1の距離算出に使った点の内分率
	template< typename VEC >
	static typename VEC::FLOAT	ClosestPtSegmentSegment(TVec3<VEC>& _ret0, TVec3<VEC>& _ret1, const TVec3<VEC>& _s00, const TVec3<VEC>& _s01, const TVec3<VEC>& _s10, const TVec3<VEC>& _s11, typename VEC::FLOAT* _t0 = NULL, typename VEC::FLOAT* _t1 = NULL);

	//-------------------------------------------------------------------------------------------------
	//@brief	線分と線分の距離
	//@return	平方距離
	//@param[in]	_s00		線分0の頂点0
	//@param[in]	_s01		線分0の頂点1
	//@param[in]	_s10		線分1の頂点0
	//@param[in]	_s11		線分1の頂点1
	template< typename VEC >
	static typename VEC::FLOAT	SqDistSegmentSegment(const TVec3<VEC>& _s00, const TVec3<VEC>& _s01, const TVec3<VEC>& _s10, const TVec3<VEC>& _s11);

	//-------------------------------------------------------------------------------------------------
	//@brief	線分と三角形の交点
	//@return	平方距離
	//@param[out]	_ret		交点
	//@param[in]	_s0			線分の頂点0
	//@param[in]	_s1			線分の頂点1
	//@param[in]	_t0			三角形の頂点0
	//@param[in]	_t1			三角形の頂点1
	//@param[in]	_t2			三角形の頂点2
	template< typename VEC >
	static typename VEC::FLOAT	IntersectSegmentTriangle(TVec3<VEC>& _ret, const TVec3<VEC>& _s0, const TVec3<VEC>& _s1, const TVec3<VEC>& _t0, const TVec3<VEC>& _t1, const TVec3<VEC>& _t2);
	template< typename VEC >
	static typename VEC::FLOAT	IntersectSegmentTriangle(TVec3<VEC>& _ret, const TVec3<VEC>& _s0, const TVec3<VEC>& _s1, const TVec3<VEC>(&_t)[3]) { return ClosestPtPointTriangle(_ret, _p0, _t[0], _t[1], _t[2]); }


public:
	//-------------------------------------------------------------------------------------------------
	///@brief	点と平面の最近接点
	///@return	距離
	///@param[out]	_ret		最近接点
	///@param[in]	_p			点
	///@param[in]	_plane		平面
	template< typename VEC3, typename VEC4 >
	static typename VEC4::FLOAT	ClosestPtPointPlane(TVec3<VEC3>& _ret, const TVec3<VEC3>& _p, const TPlane<VEC4>& _plane);

	//-------------------------------------------------------------------------------------------------
	///@brief	点と平面の距離
	///@return	距離
	///@param[in]	_p			点
	///@param[in]	_plane		平面
	template< typename VEC3, typename VEC4 >
	static typename VEC4::FLOAT	DistPointPlane(const TVec3<VEC3>& _p, const TPlane<VEC4>& _plane);

	//-------------------------------------------------------------------------------------------------
	///@brief	線分と平面の交点
	///@return	交差したか
	///@param[in]	_ret		交点
	//@param[in]	_s0			線分の頂点0
	//@param[in]	_s1			線分の頂点1
	///@param[in]	_plane		平面
	//@param[out]	_t			距離算出に使った点の内分率
	template< typename VEC3, typename VEC4 >
	static b8					IntersectSegmentPlane(TVec3<VEC3>& _ret, const TVec3<VEC3>& _p0, const TVec3<VEC3>& _p1, const TPlane<VEC4>& _plane, typename VEC4::FLOAT* _t = NULL);


public:
	//-------------------------------------------------------------------------------------------------
	///@brief	点と球体の最近接点
	///@return	距離
	///@param[out]	_ret		最近接点
	///@param[in]	_p			点
	///@param[in]	_sphere		球体
	template< typename VEC3, typename VEC4 >
	static typename VEC4::FLOAT	ClosestPtPointSphere(TVec3<VEC3>& _ret, const TVec3<VEC3>& _p, const TSphere<VEC4>& _sphere);

	//-------------------------------------------------------------------------------------------------
	///@brief	点と球体の距離
	///@return	距離
	///@param[in]	_p			点
	///@param[in]	_sphere		球体
	template< typename VEC3, typename VEC4 >
	static typename VEC4::FLOAT	DistPointSphere(const TVec3<VEC3>& _p, const TSphere<VEC4>& _sphere);

	//-------------------------------------------------------------------------------------------------
	///@brief	レイと球体の交点
	///@return	交差距離(0以下の場合交差なし)
	///@param[in]	_ret		交点
	//@param[in]	_p			レイの始点
	//@param[in]	_d			レイの方向(正規化してあるもの)
	///@param[in]	_sphere		球体
	template< typename VEC3, typename VEC4 >
	static typename VEC4::FLOAT	IntersectRaySphere(TVec3<VEC3>& _ret, const TVec3<VEC3>& _p, const TVec3<VEC3>& _d, const TSphere<VEC4>& _sphere);

	//-------------------------------------------------------------------------------------------------
	///@brief	レイと球体の交差判定
	///@return	交差したか
	//@param[in]	_p			レイの始点
	//@param[in]	_d			レイの方向(正規化してあるもの)
	///@param[in]	_sphere		球体
	template< typename VEC3, typename VEC4 >
	static b8					TestRaySphere(const TVec3<VEC3>& _p, const TVec3<VEC3>& _d, const TSphere<VEC4>& _sphere);

	//-------------------------------------------------------------------------------------------------
	///@brief	三角形と球体の交差判定
	///@return	交差したか
	//@param[in]	_t0			三角形の頂点0
	//@param[in]	_t1			三角形の頂点1
	//@param[in]	_t2			三角形の頂点2
	///@param[in]	_sphere		球体
	template< typename VEC >
	static b8					TestTriangleSphere(const TVec3<VEC>& _t0, const TVec3<VEC>& _t1, const TVec3<VEC>& _t2, const TSphere<VEC>& _sphere);
	template< typename VEC >
	static b8					TestTriangleSphere(const TVec3<VEC>(&_t)[3], const TSphere<VEC>& _sphere) { return TestTriangleSphere(_t[0], _t[1], _t[2], _sphere); }

	//-------------------------------------------------------------------------------------------------
	///@brief	平面と球体の交差判定
	///@return	交差したか
	//@param[in]	_plane		平面
	///@param[in]	_sphere		球体
	//@param[in]	_inside		内側に入り切ってるかどうか
	template< typename VEC >
	static b8					TestPlaneSphere(const TPlane<VEC>& _plane, const TSphere<VEC>& _sphere, b8* _inside = NULL);


public:
	//-------------------------------------------------------------------------------------------------
	//@brief	点とAABBの最近接点
	//@return	平方距離
	//@param[in]	_ret		最近接点
	//@param[in]	_p			点
	//@param[in]	_aabb		AABB
	//@param[in]	_maskAxis	指定軸マスク(X=0x001,Y=0x010,Z=0x100)
	template< typename VEC >
	static typename VEC::FLOAT	ClosestPtPointAABB(TVec3<VEC>& _ret, const TVec3<VEC>& _p, const TAabb<VEC>& _aabb, const u32 _maskAxis = 0);

	//-------------------------------------------------------------------------------------------------
	//@brief	点とAABBの距離の平方
	//@return	平方距離
	//@param[in]	_p			点
	//@param[in]	_aabb		AABB
	//@param[in]	_maskAxis	指定軸マスク(X=0x001,Y=0x010,Z=0x100)
	template< typename VEC >
	static typename VEC::FLOAT	SqDistPointAABB(const TVec3<VEC>& _p, const TAabb<VEC>& _aabb, const u32 _maskAxis = 0);

	//-------------------------------------------------------------------------------------------------
	///@brief	レイとAABBの交差判定
	///@return	交差点までの距離。0以下の場合交差なし。
	///@param[in]	_ret		交点
	//@param[in]	_p			レイの始点
	//@param[in]	_d			レイの方向(正規化してあるもの)
	///@param[in]	_aabb		AABB
	///@note	
	template< typename VEC >
	static typename VEC::FLOAT	IntersectRayAABB(TVec3<VEC>& _ret, const TVec3<VEC>& _p, const TVec3<VEC>& _d, const TAabb<VEC>& _aabb);

	//-------------------------------------------------------------------------------------------------
	//@brief	レイとAABBの接触判定
	//@return	接触しているか
	//@param[in]	_p			レイの始点
	//@param[in]	_d			レイの方向(正規化してあるもの)
	//@param[in]	_aabb		AABB
	//@note	
	template< typename VEC >
	static b8					TestRayAABB(const TVec3<VEC>& _p, const TVec3<VEC>& _d, const TAabb<VEC>& _aabb);

	//-------------------------------------------------------------------------------------------------
	//@brief	線分とAABBの接触判定
	//@return	接触しているか
	//@param[in]	_p0			線分0
	//@param[in]	_p1			線分1
	//@param[in]	_aabb		AABB
	//@note	
	template< typename VEC >
	static b8					TestSegmentAABB(const TVec3<VEC>& _p0, const TVec3<VEC>& _p1, const TAabb<VEC>& _aabb);

	//-------------------------------------------------------------------------------------------------
	///@brief	三角形とAABBの交差判定
	///@return	交差したか
	//@param[in]	_t0			三角形の頂点0
	//@param[in]	_t1			三角形の頂点1
	//@param[in]	_t2			三角形の頂点2
	///@param[in]	_aabb		AABB
	template< typename VEC >
	static b8					TestTriangleAABB(const TVec3<VEC>& _t0, const TVec3<VEC>& _t1, const TVec3<VEC>& _t2, const TAabb<VEC>& _aabb);
	template< typename VEC >
	static b8					TestTriangleAABB(const TVec3<VEC>(&_t)[3], const TAabb<VEC>& _aabb) { return TestTriangleAABB(_t[0], _t[1], _t[2], _aabb); }

	//-------------------------------------------------------------------------------------------------
	///@brief	平面とAABBの交差判定
	///@return	交差したか
	//@param[in]	_plane		平面
	///@param[in]	_aabb		AABB
	//@param[in]	_inside		内側に入り切ってるかどうか
	template< typename VEC3, typename VEC4 >
	static b8					TestPlaneAABB(const TPlane<VEC4>& _plane, const TAabb<VEC3>& _aabb, b8* _inside = NULL);

	//-------------------------------------------------------------------------------------------------
	///@brief	球体とAABBの交差判定
	///@return	交差したか
	//@param[in]	_sphere		球体
	///@param[in]	_aabb		AABB
	template< typename VEC3, typename VEC4 >
	static b8					TestSphereAABB(const TSphere<VEC4>& _sphere, const TAabb<VEC3>& _aabb);

};

//*************************************************************************************************
//@brief	PLANE
typedef TPlane<VECTOR4>		CPlane;

//*************************************************************************************************
//@brief	SPHERE
typedef TSphere<VECTOR4>	CSphere;

//*************************************************************************************************
//@brief	AABB
typedef TAabb<VECTOR3>		CAabb;

//*************************************************************************************************
//@brief	OBB
typedef TObb<VECTOR3>		CObb;

//*************************************************************************************************
//@brief	Frustum
typedef TFrustum<VECTOR4>	CFrustum;


POISON_END

#include "geometry.inl"


#endif	// __GEOMETRY_H__

﻿//#pragma once
#ifndef __ANGLE_H__
#define __ANGLE_H__


//-------------------------------------------------------------------------------------------------
// include

//-------------------------------------------------------------------------------------------------
// prototype


POISON_BGN


//*************************************************************************************************
//@brief	アングル型、-PI～PIの角度で管理
//*************************************************************************************************
struct CAngle
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ
	CAngle() : radian(0.0f) {}
	CAngle(f32 _radian) : radian(Limit(_radian)) {}
	CAngle(const CAngle& _angle) : radian(_angle.radian) {}

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	浮動少数にキャスト
	inline operator	f32() const { return radian; }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	加算
	inline CAngle	operator+(f32 _radian) const;
	inline CAngle	operator+(const CAngle& _angle) const;
	//-------------------------------------------------------------------------------------------------
	//@brief	加算イコール
	inline CAngle&	operator+=(f32 _radian);
	inline CAngle&	operator+=(const CAngle& _angle);

	//-------------------------------------------------------------------------------------------------
	//@brief	減算
	inline CAngle	operator-(f32 _radian) const;
	inline CAngle	operator-(const CAngle& _angle) const;
	//-------------------------------------------------------------------------------------------------
	//@brief	減算イコール
	inline CAngle&	operator-=(f32 _radian);
	inline CAngle&	operator-=(const CAngle& _angle);

	//-------------------------------------------------------------------------------------------------
	//@brief	乗算
	inline CAngle	operator*(f32 _scale) const;
	//-------------------------------------------------------------------------------------------------
	//@brief	乗算イコール
	inline CAngle&	operator*=(f32 _scale);

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	デグリーで取得
	inline f32	GetDegree() const { return radian * RAD_TO_DEG; }

	//-------------------------------------------------------------------------------------------------
	//@brief	反転(180度回転)
	inline CAngle&	Inverse();

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	指定した角度を-PI～PI内に変換します
	inline static CAngle	Limit(const f32 _radian);

	//-------------------------------------------------------------------------------------------------
	//@brief	指定した角度をmin～max内に変換します
	inline static CAngle	Limit2(const CAngle& _rot, const f32 _min, const f32 _max);

	//-------------------------------------------------------------------------------------------------
	//@brief	反転(180度回転)
	inline static CAngle	Inverse(const CAngle& _angle);

	//-------------------------------------------------------------------------------------------------
	//@brief	補間した角度を取得
	inline static CAngle	Lerp(const CAngle& _angle0, const CAngle& _angle1, const f32 _rate);

private:
	f32		radian;		//< ラジアンの値
};


POISON_END

#include "angle.inl"


#endif	// __ANGLE_H__

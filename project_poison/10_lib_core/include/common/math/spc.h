﻿//#pragma once
#ifndef __SPC_H__
#define __SPC_H__


//-------------------------------------------------------------------------------------------------
// include
#include "angle.h"
#include "vector.h"


POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype


//*************************************************************************************************
//@brief	球面座標クラス
//*************************************************************************************************
class CSpc
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ
	CSpc() {};
	CSpc(const f32 _radius, const f32 _azimuth, const f32 _altitude);


public:
	//-------------------------------------------------------------------------------------------------
	//@brief	半径取得・設定
	f32		GetRadius() const { return m_fRadius; }
	void	SetRadius(const f32 _val) { m_fRadius = _val; }

	//-------------------------------------------------------------------------------------------------
	//@brief	方位角(横回転、Y軸回転)取得・設定
	CAngle	GetAzimuth() const { return m_fAzimuth; }
	void	SetAzimuth(const CAngle _val) { m_fAzimuth = _val; }

	//-------------------------------------------------------------------------------------------------
	//@brief	仰角(縦回転、X軸回転)取得・設定
	CAngle	GetAltitude() const { return m_fAltitude; }
	void	SetAltitude(const CAngle _val) { m_fAltitude = _val; }


public:
	//-------------------------------------------------------------------------------------------------
	//@brief	球座標系からワールド座標系
	static const CVec3& SpcToVecX(CVec3& _ret, const CSpc& _spc);
	static const CVec3& SpcToVecZ(CVec3& _ret, const CSpc& _spc);

	//-------------------------------------------------------------------------------------------------
	//@brief	ワールド座標系から球座標系
	static const CSpc&	VecXToSpc(CSpc& _ret, const CVec3& _vec);
	static const CSpc&	VecZToSpc(CSpc& _ret, const CVec3& _vec);

	//-------------------------------------------------------------------------------------------------
	//@brief	線形補間
	static const CSpc&	Lerp(CSpc& _ret, const CSpc& _spc0, const CSpc& _spc1, f32 _rate);


public:
	f32		m_fRadius;		//< 半径、中心からの距離
	CAngle	m_fAzimuth;		//< 方位角(横回転、Y軸回転)
	CAngle	m_fAltitude;	//< 仰角(縦回転、X軸回転)
};


POISON_END

//#include "spc.inl"


#endif	// __SPC_H__

﻿//#pragma once
#ifndef __SPC_INL__
#define __SPC_INL__


POISON_BGN


///-------------------------------------------------------------------------------------------------
/// コンストラクタ
CSpc::CSpc(const f32 _radius, const f32 _azimuth, const f32 _altitude)
	: m_fRadius(_radius)
	, m_fAzimuth(_azimuth)
	, m_fAltitude(_altitude)
{
}

///-------------------------------------------------------------------------------------------------
/// SpcToVec
const CVec3& CSpc::SpcToVecX(CVec3& _ret, const CSpc& _spc)
{
	const f32 c0 = cosf(_spc.GetAzimuth());
	const f32 c1 = cosf(_spc.GetAltitude());
	const f32 s0 = sinf(_spc.GetAzimuth());
	const f32 s1 = sinf(_spc.GetAltitude());
	const f32 r = _spc.GetRadius();
	_ret = CVec3(r * c0 * c1, r * s1, r * s0 * c1);		// 0度の時に+X方向に向くように
	return _ret;
}
const CVec3& CSpc::SpcToVecY(CVec3& _ret, const CSpc& _spc)
{
	const CAngle altitude = _spc.GetAltitude() + toRadian(45.0f);	// 0度の時に+Y方向に向くように
	const f32 c0 = cosf(_spc.GetAzimuth());
	const f32 c1 = cosf(altitude);
	const f32 s0 = sinf(_spc.GetAzimuth());
	const f32 s1 = sinf(altitude);
	const f32 r = _spc.GetRadius();
	_ret = CVec3(r * s0 * c1, r * s1, r * c0 * c1);
	return _ret;
}
const CVec3&	CSpc::SpcToVecZ(CVec3& _ret, const CSpc& _spc)
{
	const f32 c0 = cosf(_spc.GetAzimuth());
	const f32 c1 = cosf(_spc.GetAltitude());
	const f32 s0 = sinf(_spc.GetAzimuth());
	const f32 s1 = sinf(_spc.GetAltitude());
	const f32 r = _spc.GetRadius();
	_ret = CVec3(r * s0 * c1, -r * s1, r * c0 * c1);	// 0度の時に+Z方向に向くように
	return _ret;
}


///-------------------------------------------------------------------------------------------------
/// VecToSpc

const CSpc&		CSpc::VecXToSpc(CSpc& _ret, const CVec3& _vec)
{
	// 半径取得
	f32 r = Vec::Length(_vec);
	if (r <= 0.0f) { r = 0.0001f; }
	const f32 x = _vec[0];
	const f32 y = _vec[1];
	const f32 z = _vec[2];
	_ret = CSpc(r, (x != 0.0f || z != 0.0f) ? atan2f(z, x) : 0.0f, asinf(y / r));	// 0度のときに+X方向を向くように
	return _ret;
}
const CSpc&		CSpc::VecYToSpc(CSpc& _ret, const CVec3& _vec)
{
	// 半径取得
	f32 r = Vec::Length(_vec);
	if (r <= 0.0f) { r = 0.0001f; }
	const f32 x = _vec[0];
	const f32 y = _vec[1];
	const f32 z = _vec[2];
	_ret = CSpc(r, (x != 0.0f || z != 0.0f) ? atan2f(x, z) : 0.0f, acosf(y / r));	// 0度のときに+Y方向を向くように
	return _ret;
}
const CSpc&		CSpc::VecZToSpc(CSpc& _ret, const CVec3& _vec)
{
	// 半径取得
	f32 r = Vec::Length(_vec);
	if (r <= 0.0f) { r = 0.0001f; }
	const f32 x = _vec[0];
	const f32 y = _vec[1];
	const f32 z = _vec[2];
	_ret = CSpc(r, (x != 0.0f || z != 0.0f) ? atan2f(x, z) : 0.0f, -asinf(y / r));	// 0度のときに+Z方向を向くように
	return _ret;
}


///-------------------------------------------------------------------------------------------------
/// Lerp
const CSpc&		CSpc::Lerp(CSpc& _ret, const CSpc& _spc0, const CSpc& _spc1, f32 _rate)
{
	_ret.SetAzimuth(CAngle::Lerp(_spc0.GetAzimuth(), _spc1.GetAzimuth(), _rate));
	_ret.SetAltitude(CAngle::Lerp(_spc0.GetAltitude(), _spc1.GetAltitude(), _rate));
	_ret.SetRadius((_spc1.GetRadius() - _spc0.GetRadius()) * _rate + _spc0.GetRadius());
	return _ret;
}


POISON_END

#endif	// __SPC_INL__

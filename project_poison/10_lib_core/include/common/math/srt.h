﻿//#pragma once
#ifndef __SRT_H__
#define __SRT_H__


//-------------------------------------------------------------------------------------------------
// include
#include "matrix.h"


POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype


//*************************************************************************************************
//@brief	拡縮・回転・移動クラス（オイラー版）
//*************************************************************************************************
class CSrt
{
public:
	CSrt() {}
	CSrt(const CVec3& _scl, const CEuler& _rot, const CVec3& _trs);
	~CSrt() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	拡縮取得・設定
	const CVec3&	GetScale() const { return m_scale; }
	CVec3&			GetScale() { return m_scale; }
	void			SetScale(const CVec3& _scale) { m_scale = _scale; }
	
	//-------------------------------------------------------------------------------------------------
	//@brief	回転取得・設定
	const CEuler&	GetRotate() const { return m_rotate; }
	CEuler&			GetRotate() { return m_rotate; }
	void			SetRotate(const CEuler& _rotate) { m_rotate = _rotate; }
	
	//-------------------------------------------------------------------------------------------------
	//@brief	移動取得・設定
	const CVec3&	GetTrans() const { return m_trans; }
	CVec3&			GetTrans() { return m_trans; }
	void			SetTrans(const CVec3& _trans) { m_trans = _trans; }

	//-------------------------------------------------------------------------------------------------
	//@brief	行列取得
	CMtx34&			GetCalcMtx(CMtx34& _mtx) const;
	CMtx44&			GetCalcMtx(CMtx44& _mtx) const;

private:
	CVec3	m_scale = CVec3::One;		///< 拡縮
	CEuler	m_rotate = CEuler::Zero;	///< 回転
	CVec3	m_trans = CVec3::Zero;		///< 移動
};



//*************************************************************************************************
//@brief	拡縮・回転・移動クラス（クォータニオン版）
//*************************************************************************************************
class CSqt
{
public:
	CSqt() {}
	CSqt(const CVec3& _scl, const CQuat& _rot, const CVec3& _trs);
	~CSqt() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	拡縮取得・設定
	const CVec3&	GetScale() const { return m_scale; }
	CVec3&			GetScale() { return m_scale; }
	void			SetScale(const CVec3& _scale) { m_scale = _scale; }
	
	//-------------------------------------------------------------------------------------------------
	//@brief	回転取得・設定
	const CQuat&	GetRotate() const { return m_rotate; }
	CQuat&			GetRotate() { return m_rotate; }
	void			SetRotate(const CQuat& _rotate) { m_rotate = _rotate; }
	
	//-------------------------------------------------------------------------------------------------
	//@brief	移動取得・設定
	const CVec3&	GetTrans() const { return m_trans; }
	CVec3&			GetTrans() { return m_trans; }
	void			SetTrans(const CVec3& _trans) { m_trans = _trans; }

	//-------------------------------------------------------------------------------------------------
	//@brief	行列取得
	CMtx34&			GetCalcMtx(CMtx34& _mtx) const;
	CMtx44&			GetCalcMtx(CMtx44& _mtx) const;

private:
	CVec3	m_scale = CVec3::One;		///< 拡縮
	CQuat	m_rotate = CQuat::Identity;	///< 回転
	CVec3	m_trans = CVec3::Zero;		///< 移動
};


POISON_END

//#include "srt.inl"

#endif	// __SRT_H__

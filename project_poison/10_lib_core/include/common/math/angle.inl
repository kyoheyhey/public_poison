﻿//#pragma once
#ifndef __ANGLE_INL__
#define __ANGLE_INL__


POISON_BGN


///-------------------------------------------------------------------------------------------------
/// 加算
CAngle	CAngle::operator+(f32 _radian) const
{
	return CAngle(radian + _radian);
}

CAngle	CAngle::operator+(const CAngle& _angle) const
{
	return CAngle(radian + _angle.radian);
}

///-------------------------------------------------------------------------------------------------
/// 加算イコール
CAngle&	CAngle::operator+=(f32 _radian)
{
	this->radian = Limit(this->radian + _radian);
	return (*this);
}

CAngle&	CAngle::operator+=(const CAngle& _angle)
{
	this->radian = Limit(this->radian + _angle.radian);
	return (*this);
}

///-------------------------------------------------------------------------------------------------
/// 減算
CAngle	CAngle::operator-(f32 _radian) const
{
	return CAngle(radian - _radian);
}

CAngle	CAngle::operator-(const CAngle& _angle) const
{
	return CAngle(radian - _angle.radian);
}

///-------------------------------------------------------------------------------------------------
/// 減算イコール
CAngle&	CAngle::operator-=(f32 _radian)
{
	this->radian = Limit(this->radian - _radian);
	return (*this);
}

CAngle&	CAngle::operator-=(const CAngle& _angle)
{
	radian = Limit(radian - _angle.radian);
	return (*this);
}

///-------------------------------------------------------------------------------------------------
/// 乗算
CAngle	CAngle::operator*(f32 _scale) const
{
	return CAngle(this->radian * _scale);
}

///-------------------------------------------------------------------------------------------------
/// 乗算イコール
CAngle&	CAngle::operator*=(f32 _scale)
{
	radian = Limit(radian * _scale);
	return (*this);
}

///-------------------------------------------------------------------------------------------------
/// Limit
CAngle	CAngle::Limit(const f32 _radian)
{
	CAngle ret;
	ret.radian = _radian;
	f32 pi = PI;
	if (_radian >= 0.f)
	{
		if (_radian <= PI) { return ret; }
	}
	else
	{
		if (_radian > -PI) { return ret; }
		pi = -PI;
	}
	ret.radian += pi;
	s32 i = (s32)(ret.radian / PI_2);
	ret.radian -= ((f32)i) * PI_2 + pi;
	return ret;
}

///-------------------------------------------------------------------------------------------------
/// Limit2
CAngle	CAngle::Limit2(const CAngle& _rot, const f32 _min, const f32 _max)
{
	CAngle ret = _rot;
	// 範囲内
	if (_min <= _max)
	{
		if (_rot.radian < _min) {
			ret.radian = _min;
		}
		else if (_rot.radian > _max) {
			ret.radian = _max;
		}
	}
	// 逆範囲内
	else
	{
		// min以上、max以下なら範囲外
		if (_rot.radian > _min&& _rot.radian < _max)
		{
			// 範囲
			f32 range = (PI + _max) + (PI - _min);
			// 中心角度
			CAngle center = _min + range * 0.5f;
			CAngle diff = ret - center;
			// プラス方向
			if (diff >= 0.f) {
				ret = _min;
			}
			// マイナス方向
			else {
				ret = _max;
			}
		}
	}

	return ret;
}

///-------------------------------------------------------------------------------------------------
/// 反転(180度回転)
CAngle&	CAngle::Inverse()
{
	radian = Limit(radian + PI);
	return (*this);
}

///-------------------------------------------------------------------------------------------------
/// 反転(180度回転)
CAngle	CAngle::Inverse(const CAngle& _angle)
{
	return Limit(_angle.radian + PI);
}

///-------------------------------------------------------------------------------------------------
/// 補間した角度を取得
CAngle	CAngle::Lerp(const CAngle& _angle0, const CAngle& _angle1, const f32 _rate)
{
	return ((_angle1 - _angle0) * _rate + _angle0);
}


POISON_END

#endif	// __ANGLE_INL__

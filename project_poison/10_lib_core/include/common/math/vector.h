﻿//#pragma once
#ifndef	__VECTOR_H__
#define	__VECTOR_H__


//-------------------------------------------------------------------------------------------------
// include


POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype


//*************************************************************************************************
//@brief	floatの有効チェック
#define CHECK_ENABLE_FLOAT(__fval)		libAssert( IsEnableFloat((__fval)) )
//*************************************************************************************************
//@brief	ベクトル有効チェック
#define CHECK_ENABLE_VECTOR2()			libAssert( IsEnableFloat(x) && IsEnableFloat(y) )
#define CHECK_ENABLE_VECTOR3()			libAssert( IsEnableFloat(x) && IsEnableFloat(y) && IsEnableFloat(z) )
#define CHECK_ENABLE_VECTOR4()			libAssert( IsEnableFloat(x) && IsEnableFloat(y) && IsEnableFloat(z) && IsEnableFloat(w))
//*************************************************************************************************
//@brief	正規化の有効チェック
#define CHECK_ENABLE_NORMALIZE(__fval)	libAssert( IsEnableDivision((__fval)) )

POISON_END

#include "vector_float.h"

POISON_BGN

//*************************************************************************************************
//@brief	２ベクトルクラス
//*************************************************************************************************
template < typename VEC >
class TVec2 : public VEC
{
private:
	friend class	Vec;
	friend class	Mtx;

public:
	//@brief	型定義
	typedef	typename VEC::FLOAT		FLOAT;

public:
	TVec2();
	TVec2(FLOAT _xy);
	TVec2(FLOAT _x, FLOAT _y);
	TVec2(const FLOAT* _xy);

	//-------------------------------------------------------------------------------------------------
	//@brief	compar operators
	TVec2&	operator=(const TVec2&);		///< 代入
	b8		operator==(const TVec2&) const;	///< 一致
	b8		operator!=(const TVec2&) const;	///< 不一致

	//-------------------------------------------------------------------------------------------------
	//@brief	assignment operators
	TVec2&	operator+=(const TVec2&);		///< 加算
	TVec2&	operator-=(const TVec2&);		///< 減算
	TVec2&	operator*=(const TVec2&);		///< 乗算
	TVec2&	operator/=(const TVec2&);		///< 除算

	TVec2&	operator+=(FLOAT);				///< 加算
	TVec2&	operator-=(FLOAT);				///< 減算
	TVec2&	operator*=(FLOAT);				///< 乗算
	TVec2&	operator/=(FLOAT);				///< 除算

	//-------------------------------------------------------------------------------------------------
	//@brief	binary operators
	TVec2	operator+(const TVec2&) const;	///< 加算
	TVec2	operator-(const TVec2&) const;	///< 減算
	TVec2	operator*(const TVec2&) const;	///< 乗算
	TVec2	operator/(const TVec2&) const;	///< 除算

	TVec2	operator+(FLOAT) const;			///< 加算
	TVec2	operator-(FLOAT) const;			///< 減算
	TVec2	operator*(FLOAT) const;			///< 乗算
	TVec2	operator/(FLOAT) const;			///< 除算

	TVec2	operator-()	const;				///< マイナス反転
	
	//-------------------------------------------------------------------------------------------------
	//@brief	Getrer
	const FLOAT	GetX() const;
	const FLOAT	GetY() const;

	//-------------------------------------------------------------------------------------------------
	//@brief	Setter
	TVec2&		Set(FLOAT _v);
	TVec2&		Set(FLOAT _x, FLOAT _y);
	TVec2&		Set(const FLOAT* _xy);
	TVec2&		SetX(const FLOAT _x);
	TVec2&		SetY(const FLOAT _y);

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	定数値
	static const TVec2	Zero;
	static const TVec2	One;
	static const TVec2	Left;
	static const TVec2	Right;
	static const TVec2	Up;
	static const TVec2	Down;
};


//*************************************************************************************************
//@brief	３ベクトルクラス
//*************************************************************************************************
template < typename VEC >
class TVec3 : public VEC
{
private:
	friend class	Vec;
	friend class	Mtx;

private:
	typedef	typename VEC::VEC2	VEC2;

public:
	//@brief	型定義
	typedef	typename VEC::FLOAT	FLOAT;
	//@brief	ベクトル型
	typedef	TVec2<VEC2>			TVec2;

public:
	TVec3();
	TVec3(FLOAT _xyx);
	TVec3(FLOAT _x, FLOAT _y, FLOAT _z);
	TVec3(const FLOAT* _xyz);
	TVec3(const TVec2& _xy, FLOAT _z);
	TVec3(FLOAT _x, const TVec2& _yz);

	//-------------------------------------------------------------------------------------------------
	//@brief	compar operators
	TVec3&	operator=(const TVec3&);		///< 代入
	b8		operator==(const TVec3&) const;	///< 一致
	b8		operator!=(const TVec3&) const;	///< 不一致

	//-------------------------------------------------------------------------------------------------
	//@brief	assignment operators
	TVec3&	operator+=(const TVec3&);		///< 加算
	TVec3&	operator-=(const TVec3&);		///< 減算
	TVec3&	operator*=(const TVec3&);		///< 乗算
	TVec3&	operator/=(const TVec3&);		///< 除算

	TVec3&	operator+=(FLOAT);				///< 加算
	TVec3&	operator-=(FLOAT);				///< 減算
	TVec3&	operator*=(FLOAT);				///< 乗算
	TVec3&	operator/=(FLOAT);				///< 除算

	//-------------------------------------------------------------------------------------------------
	//@brief	binary operators
	TVec3	operator+(const TVec3&) const;	///< 加算
	TVec3	operator-(const TVec3&) const;	///< 減算
	TVec3	operator*(const TVec3&) const;	///< 乗算
	TVec3	operator/(const TVec3&) const;	///< 除算

	TVec3	operator+(FLOAT) const;			///< 加算
	TVec3	operator-(FLOAT) const;			///< 減算
	TVec3	operator*(FLOAT) const;			///< 乗算
	TVec3	operator/(FLOAT) const;			///< 除算

	TVec3	operator-()	const;				///< マイナス反転
	
	//-------------------------------------------------------------------------------------------------
	//@brief	Getrer
	const FLOAT		GetX() const;
	const FLOAT		GetY() const;
	const FLOAT		GetZ() const;
	const TVec2&	GetXY() const { return CCast<TVec3*>(this)->GetXY(); }
	TVec2&			GetXY();

	//-------------------------------------------------------------------------------------------------
	//@brief	Setter
	TVec3&		Set(FLOAT _v);
	TVec3&		Set(FLOAT _x, FLOAT _y, FLOAT _z);
	TVec3&		Set(const FLOAT* _xyz);
	TVec3&		Set(const TVec2& _xy, FLOAT _z);
	TVec3&		Set(FLOAT _x, const TVec2& _yz);
	TVec3&		SetX(const FLOAT _x);
	TVec3&		SetY(const FLOAT _y);
	TVec3&		SetZ(const FLOAT _z);
	TVec3&		SetXY(const TVec2& _xy);


public:
	//-------------------------------------------------------------------------------------------------
	//@brief	定数値
	static const TVec3	Zero;
	static const TVec3	One;
	static const TVec3	Left;
	static const TVec3	Right;
	static const TVec3	Up;
	static const TVec3	Down;
	static const TVec3	Front;
	static const TVec3	Back;
};


//*************************************************************************************************
//@brief	４ベクトルクラス
//*************************************************************************************************
template < typename VEC >
class TVec4 : public VEC
{
private:
	friend class	Vec;
	friend class	Mtx;

private:
	typedef	typename VEC::VEC2	VEC2;
	typedef	typename VEC::VEC3	VEC3;

public:
	//@brief	型定義
	typedef	typename VEC::FLOAT	FLOAT;
	//@brief	ベクトル型
	typedef	TVec2<VEC2>			TVec2;
	typedef	TVec3<VEC3>			TVec3;

public:
	TVec4();
	TVec4(FLOAT _xyzw);
	TVec4(FLOAT _x, FLOAT _y, FLOAT _z, FLOAT _w);
	TVec4(const FLOAT* _xyzw);
	TVec4(const TVec3& _xyz, FLOAT _w);
	TVec4(const TVec2& _xy, const TVec2& _zw);

	//-------------------------------------------------------------------------------------------------
	//@brief	compar operators
	TVec4&	operator=(const TVec4&);		///< 代入
	b8		operator==(const TVec4&) const;	///< 一致
	b8		operator!=(const TVec4&) const;	///< 不一致

	//-------------------------------------------------------------------------------------------------
	//@brief	assignment operators
	TVec4&	operator+=(const TVec4&);		///< 加算
	TVec4&	operator-=(const TVec4&);		///< 減算
	TVec4&	operator*=(const TVec4&);		///< 乗算
	TVec4&	operator/=(const TVec4&);		///< 除算

	TVec4&	operator+=(FLOAT);				///< 加算
	TVec4&	operator-=(FLOAT);				///< 減算
	TVec4&	operator*=(FLOAT);				///< 乗算
	TVec4&	operator/=(FLOAT);				///< 除算

	//-------------------------------------------------------------------------------------------------
	//@brief	binary operators
	TVec4	operator+(const TVec4&) const;	///< 加算
	TVec4	operator-(const TVec4&) const;	///< 減算
	TVec4	operator*(const TVec4&) const;	///< 乗算
	TVec4	operator/(const TVec4&) const;	///< 除算

	TVec4	operator+(FLOAT) const;			///< 加算
	TVec4	operator-(FLOAT) const;			///< 減算
	TVec4	operator*(FLOAT) const;			///< 乗算
	TVec4	operator/(FLOAT) const;			///< 除算

	TVec4	operator-()	const;				///< マイナス反転
	
	//-------------------------------------------------------------------------------------------------
	//@brief	Getter
	const FLOAT		GetX() const;
	const FLOAT		GetY() const;
	const FLOAT		GetZ() const;
	const FLOAT		GetW() const;
	const TVec2&	GetXY() const { return CCast<TVec4*>(this)->GetXY(); }
	TVec2&			GetXY();
	const TVec3&	GetXYZ() const { return CCast<TVec4*>(this)->GetXYZ(); }
	TVec3&			GetXYZ();

	//-------------------------------------------------------------------------------------------------
	//@brief	Setter
	TVec4&		Set(FLOAT _v);
	TVec4&		Set(FLOAT _x, FLOAT _y, FLOAT _z, FLOAT _w);
	TVec4&		Set(const FLOAT* _xyzw);
	TVec4&		Set(const TVec3& _xyz, FLOAT _w);
	TVec4&		Set(const TVec2& _xy, const TVec2& _zw);
	TVec4&		SetX(const FLOAT _x);
	TVec4&		SetY(const FLOAT _y);
	TVec4&		SetZ(const FLOAT _z);
	TVec4&		SetW(const FLOAT _w);
	TVec4&		SetXY(const TVec2& _xy);
	TVec4&		SetXYZ(const TVec3& _xyz);

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	定数値
	static const TVec4	Zero;
	static const TVec4	One;
	static const TVec4	Left;
	static const TVec4	Right;
	static const TVec4	Up;
	static const TVec4	Down;
	static const TVec4	Front;
	static const TVec4	Back;

	static const TVec4	Black;
	static const TVec4	White;
	static const TVec4	Gray;
	static const TVec4	Red;
	static const TVec4	Green;
	static const TVec4	Blue;
	static const TVec4	Yellow;
	static const TVec4	Turquoise;
	static const TVec4	Purple;
};



//*************************************************************************************************
//@brief	ベクトル計算関数
//*************************************************************************************************
class Vec
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	２ベクトル関数群
	template< typename VEC >
	static void							Copy(typename TVec2<VEC>::FLOAT* _ret, const TVec2<VEC>& _vec);
	template< typename VEC >
	static void							Copy(typename TVec2<VEC>::FLOAT(&_ret)[2], const TVec2<VEC>& _vec);
	template< typename VEC >
	static typename TVec2<VEC>::FLOAT	Min(const TVec2<VEC>& _vec);
	template< typename VEC >
	static typename TVec2<VEC>::FLOAT	Max(const TVec2<VEC>& _vec);
	template< typename VEC >
	static TVec2<VEC>&					Min(TVec2<VEC>& _ret, const TVec2<VEC>& _vecA, const TVec2<VEC>& _vecB);
	template< typename VEC >
	static TVec2<VEC>&					Max(TVec2<VEC>& _ret, const TVec2<VEC>& _vecA, const TVec2<VEC>& _vecB);
	template< typename VEC >
	static typename TVec2<VEC>::FLOAT	Length(const TVec2<VEC>& _vec);
	template< typename VEC >
	static typename TVec2<VEC>::FLOAT	Length2(const TVec2<VEC>& _vec);
	template< typename VEC >
	static TVec2<VEC>&					Normalize(TVec2<VEC>& _ret, const TVec2<VEC>& _src);
	template< typename VEC >
	static TVec2<VEC>					Normalize(const TVec2<VEC>& _src);
	template< typename VEC >
	static typename TVec2<VEC>::FLOAT	Distance(const TVec2<VEC>& _vecA, const TVec2<VEC>& _vecB);
	template< typename VEC >
	static TVec2<VEC>&			Add(TVec2<VEC>& _ret, const TVec2<VEC>& _vecA, const TVec2<VEC>& _vecB);
	template< typename VEC >
	static TVec2<VEC>&			Sub(TVec2<VEC>& _ret, const TVec2<VEC>& _vecA, const TVec2<VEC>& _vecB);
	template< typename VEC >
	static TVec2<VEC>&			Scale(TVec2<VEC>& _ret, const TVec2<VEC>& _vec, const typename TVec2<VEC>::FLOAT _scalar);
	template< typename VEC >
	static typename TVec2<VEC>::FLOAT	Dot(const TVec2<VEC>& _vecA, const TVec2<VEC>& _vecB);
	template< typename VEC >
	static typename TVec2<VEC>::FLOAT	Cross(const TVec2<VEC>& _vecA, const TVec2<VEC>& _vecB);
	template< typename VEC >
	static typename TVec2<VEC>::FLOAT	Sin(const TVec2<VEC>& _vecA, const TVec2<VEC>& _vecB);
	template< typename VEC >
	static typename TVec2<VEC>::FLOAT	Cos(const TVec2<VEC>& _vecA, const TVec2<VEC>& _vecB);
	template< typename VEC >
	static TVec2<VEC>&			Lerp(TVec2<VEC>& _ret, const TVec2<VEC>& _vA, const TVec2<VEC>& _vB, const typename TVec2<VEC>::FLOAT _t);
	template< typename VEC >
	static TVec2<VEC>&			Hermite(TVec2<VEC>& _ret, const TVec2<VEC>& _posA, const TVec2<VEC>& _vecA, const TVec2<VEC>& _posB, const TVec2<VEC>& _vecB, const typename TVec2<VEC>::FLOAT _t);

	//-------------------------------------------------------------------------------------------------
	//@brief	３ベクトル関数群
	template< typename VEC >
	static void							Copy(typename TVec3<VEC>::FLOAT* _ret, const TVec3<VEC>& _vec);
	template< typename VEC >
	static void							Copy(typename TVec3<VEC>::FLOAT(&_ret)[3], const TVec3<VEC>& _vec);
	template< typename VEC >
	static typename TVec3<VEC>::FLOAT	Min(const TVec3<VEC>& _vec);
	template< typename VEC >
	static typename TVec3<VEC>::FLOAT	Max(const TVec3<VEC>& _vec);
	template< typename VEC >
	static TVec3<VEC>&					Min(TVec3<VEC>& _ret, const TVec3<VEC>& _vecA, const TVec3<VEC>& _vecB);
	template< typename VEC >
	static TVec3<VEC>&					Max(TVec3<VEC>& _ret, const TVec3<VEC>& _vecA, const TVec3<VEC>& _vecB);
	template< typename VEC >
	static typename TVec3<VEC>::FLOAT	Length(const TVec3<VEC>& _vec);
	template< typename VEC >
	static typename TVec3<VEC>::FLOAT	Length2(const TVec3<VEC>& _vec);
	template< typename VEC >
	static TVec3<VEC>&					Normalize(TVec3<VEC>& _ret, const TVec3<VEC>& _src);
	template< typename VEC >
	static TVec3<VEC>					Normalize(const TVec3<VEC>& _src);
	template< typename VEC >
	static typename TVec3<VEC>::FLOAT	Distance(const TVec3<VEC>& _vecA, const TVec3<VEC>& _vecB);
	template< typename VEC >
	static TVec3<VEC>&			Add(TVec3<VEC>& _ret, const TVec3<VEC>& _vecA, const TVec3<VEC>& _vecB);
	template< typename VEC >
	static TVec3<VEC>&			Sub(TVec3<VEC>& _ret, const TVec3<VEC>& _vecA, const TVec3<VEC>& _vecB);
	template< typename VEC >
	static TVec3<VEC>&			Scale(TVec3<VEC>& _ret, const TVec3<VEC>& _vec, const typename TVec3<VEC>::FLOAT _scalar);
	template< typename VEC >
	static typename TVec3<VEC>::FLOAT	Dot(const TVec3<VEC>& _vecA, const TVec3<VEC>& _vecB);
	template< typename VEC >
	static TVec3<VEC>&					Cross(TVec3<VEC>& _ret, const TVec3<VEC>& _vecA, const TVec3<VEC>& _vecB);
	template< typename VEC >
	static TVec3<VEC>					Cross(const TVec3<VEC>& _vecA, const TVec3<VEC>& _vecB);
	template< typename VEC >
	static typename TVec3<VEC>::FLOAT	ScalarTrio(const TVec3<VEC>& _vecA, const TVec3<VEC>& _vecB, const TVec3<VEC>& _vecC);
	template< typename VEC >
	static TVec3<VEC>&					VectorTrio(TVec3<VEC>& _ret, const TVec3<VEC>& _vecA, const TVec3<VEC>& _vecB, TVec3<VEC>& _vecC);
	template< typename VEC >
	static TVec3<VEC>&			Projection(TVec3<VEC>& _ret, const TVec3<VEC>& _srcVec, const TVec3<VEC>& _nVec);
	template< typename VEC >
	static TVec3<VEC>&			Lerp(TVec3<VEC>& _ret, const TVec3<VEC>& _vA, const TVec3<VEC>& _vB, const typename TVec3<VEC>::FLOAT _t);
	template< typename VEC >
	static TVec3<VEC>&			Hermite(TVec3<VEC>& _ret, const TVec3<VEC>& _posA, const TVec3<VEC>& _vecA, const TVec3<VEC>& _posB, const TVec3<VEC>& _vecB, const typename TVec3<VEC>::FLOAT _t);

	//-------------------------------------------------------------------------------------------------
	//@brief	４ベクトル関数群
	template< typename VEC >
	static typename void				Copy(typename TVec4<VEC>::FLOAT* _ret, const TVec4<VEC>& _vec);
	template< typename VEC >
	static typename void				Copy(typename TVec4<VEC>::FLOAT(&_ret)[4], const TVec4<VEC>& _vec);
	template< typename VEC >
	static typename TVec4<VEC>::FLOAT	Min(const TVec4<VEC>& _vec);
	template< typename VEC >
	static typename TVec4<VEC>::FLOAT	Max(const TVec4<VEC>& _vec);
	template< typename VEC >
	static TVec4<VEC>&					Min(TVec4<VEC>& _ret, const TVec4<VEC>& _vecA, const TVec4<VEC>& _vecB);
	template< typename VEC >
	static TVec4<VEC>&					Max(TVec4<VEC>& _ret, const TVec4<VEC>& _vecA, const TVec4<VEC>& _vecB);
	template< typename VEC >
	static typename TVec4<VEC>::FLOAT	Length(const TVec4<VEC>& _vec);
	template< typename VEC >
	static typename TVec4<VEC>::FLOAT	Length2(const TVec4<VEC>& _vec);
	template< typename VEC >
	static TVec4<VEC>&					Normalize(TVec4<VEC>& _ret, const TVec4<VEC>& _src);
	template< typename VEC >
	static TVec4<VEC>					Normalize(const TVec4<VEC>& _src);
	template< typename VEC >
	static typename TVec4<VEC>::FLOAT	Distance(const TVec4<VEC>& _vecA, const TVec4<VEC>& _vecB);
	template< typename VEC >
	static TVec4<VEC>&			Lerp(TVec4<VEC>& _ret, const TVec4<VEC>& _vA, const TVec4<VEC>& _vB, const typename TVec4<VEC>::FLOAT _t);
	template< typename VEC >
	static TVec4<VEC>&			Hermite(TVec4<VEC>& _ret, const TVec4<VEC>& _posA, const TVec4<VEC>& _vecA, const TVec4<VEC>& _posB, const TVec4<VEC>& _vecB, const typename TVec4<VEC>::FLOAT _t);

};


//*************************************************************************************************
//@brief	ベクター定義
typedef	TVec2<VECTOR2>	CVec2;
typedef	TVec3<VECTOR3>	CVec3;
typedef	TVec4<VECTOR4>	CVec4;


POISON_END

#include "vector.inl"
#include "vector_func.inl"

#endif	//__VECTOR_H__

﻿//#pragma once
#ifndef	__GEOMETRY_INL__
#define	__GEOMETRY_INL__


POISON_BGN

//-------------------------------------------------------------------------------------------------
/// テンプレート宣言部分の省略
//#define VEC_TEMPLATE_HEAD		template< typename VEC >
//#define MTX_TEMPLATE_HEAD		template< typename MTX >
//#define VEC_MTX_TEMPLATE_HEAD	template< typename VEC, typename MTX >
//#define VEC2_CLASS_HEAD		TVec2< VEC >
//#define VEC3_CLASS_HEAD		TVec3< VEC >
//#define VEC4_CLASS_HEAD		TVec4< VEC >
//#define EULER_CLASS_HEAD		TEuler< VEC >
//#define QUAT_CLASS_HEAD		TQuat< VEC >
//#define MTX22_CLASS_HEAD		TMtx22< MTX >
//#define MTX23_CLASS_HEAD		TMtx23< MTX >
//#define MTX33_CLASS_HEAD		TMtx33< MTX >
//#define MTX34_CLASS_HEAD		TMtx34< MTX >
//#define MTX44_CLASS_HEAD		TMtx44< MTX >


///-------------------------------------------------------------------------------------------------
/// 平面を算出
template< typename VEC3, typename VEC4 >
TPlane<VEC4>&	Geometry::SetPlane(TPlane<VEC4>& _plane, const TVec3<VEC3>& _pos, const TVec3<VEC3>& _nrm)
{
	_plane.SetNrm(_nrm);
	_plane.SetDot(Vec::Dot(_nrm, _pos));
	return _plane;
}

///-------------------------------------------------------------------------------------------------
/// 平面を算出
template< typename VEC3, typename VEC4 >
TPlane<VEC4>&	Geometry::SetPlane(TPlane<VEC4>& _plane, const TVec3<VEC3>& _pos0, const TVec3<VEC3>& _pos1, const TVec3<VEC3>& _pos2)
{
	TVec3<VEC3> cross;
	Vec::Normalize(_plane.GetNrm(), Vec::Cross(cross, _pos1 - _pos0, _pos2 - _pos0));
	_plane.SetDot(Vec::Dot(_plane.GetNrm(), _pos0));
	return _plane;
}
template< typename VEC3, typename VEC4 >
TPlane<VEC4>&	Geometry::SetPlane(TPlane<VEC4>& _plane, const TVec3<VEC3>(&_pos)[3])
{
	TVec3<VEC3> cross;
	Vec::Normalize(_plane.GetNrm(), Vec::Cross(cross, _pos[1] - _pos[0], _pos[2] - _pos[0]));
	_plane.SetDot(Vec::Dot(_plane.GetNrm(), _pos[0]));
	return _plane;
}
template< typename VEC4 >
TPlane<VEC4>&	Geometry::SetPlane(TPlane<VEC4>& _plane, const TVec4<VEC4>(&_pos)[3])
{
	TVec3<VEC4::VEC3> cross;
	Vec::Normalize(_plane.GetNrm(), Vec::Cross(cross, _pos[1].GetXYZ() - _pos[0].GetXYZ(), _pos[2].GetXYZ() - _pos[0].GetXYZ()));
	_plane.SetDot(Vec::Dot(_plane.GetNrm(), _pos[0].GetXYZ()));
	return _plane;
}

///-------------------------------------------------------------------------------------------------
/// 平面を１次変換
template< typename VEC, typename MTX >
TPlane<VEC>&	Geometry::TransformPlane(TPlane<VEC>& _ret, const TPlane<VEC>& _plane, const TMtx44<MTX>& _mtx)
{
	TMtx44<MTX> M;
	Mtx::Transpose(M, _mtx);
	Mtx::Inverse(M, M);

	typename TVec4<VEC> P(_plane.GetNrm(), _plane.GetDot());
	typename TVec4<VEC> P1;
	Mtx::Mul(P1, M, P);

	typename VEC::FLOAT len = Vec::Length(P1.GetXYZ());
	if (len < FLT_MIN) { len = 1.0f; }

	new(&_ret) TPlane<VEC>(P1.GetXYZ() / len, P1.GetW() / len);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 三角形に対するPからなる重心座標
template< typename VEC >
TVec3<VEC>		Geometry::Barycentric(TVec3<VEC>& _ret, const TVec3<VEC>& _p0, const TVec3<VEC>& _t0, const TVec3<VEC>& _t1, const TVec3<VEC>& _t2)
{
	TVec3<VEC> v0 = _t1 - _t0, v1 = _t2 - _t0, v2 = _p0 - _t0;
	typename VEC::FLOAT d00 = Vec::Dot(v0, v0);
	typename VEC::FLOAT d01 = Vec::Dot(v0, v1);
	typename VEC::FLOAT d11 = Vec::Dot(v1, v1);
	typename VEC::FLOAT d20 = Vec::Dot(v2, v0);
	typename VEC::FLOAT d21 = Vec::Dot(v2, v1);
	typename VEC::FLOAT denom = d00 * d11 - d01 * d01;
	_ret[0] = (d11 * d20 - d01 * d21) / denom;
	_ret[1] = (d00 * d21 - d01 * d20) / denom;
	_ret[2] = 1.0f - _ret[0] - _ret[1];
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 点と直線の距離
template< typename VEC >
typename VEC::FLOAT	Geometry::ClosestPtPointSegment(TVec3<VEC>& _ret, const TVec3<VEC>& _p0, const TVec3<VEC>& _s0, const TVec3<VEC>& _s1, typename VEC::FLOAT* _t/* = NULL*/)
{
	typename VEC::FLOAT ret = 0.0f;
	TVec3<VEC> ab = _s1 - _s0;
	TVec3<VEC> ac = _p0 - _s0;
	// ab上に_p0を射影、Dot(ab, ab)による除算は延長
	typename VEC::FLOAT t = Vec::Dot(ac, ab);
	if (t < = 0.0f)
	{
		// _p0は範囲[_s0, _s1]の外側、_s0側に射影され_s0までクランプ
		t = 0.0f;
		_ret = _s0;
		ret = Vec::Dot(ac, ac);
	}
	else
	{
		typename VEC::FLOAT denom = Vec::Dot(ab, ab);
		if (t >= denom)
		{
			// _p0は範囲[_s0, _s1]の外側、_s1側に射影され_s1までクランプ
			t = 1.0f;
			_ret = _s1;
			TVec3<VEC> bc = _p0 - _s1;
			ret = Vec::Dot(bc, bc);
		}
		else
		{
			// _p0は範囲[_s0, _s1]の内側、この時まで除算は延長
			typename VEC::FLOAT d = t / denom;
			_ret = _s0 + d * ab;
			ret = Vec::Dot(ac, ac) - t * d;
		}
	}

	if (_t) { *_t = t; }
	return ret;
}

///-------------------------------------------------------------------------------------------------
/// 点と直線の距離
template< typename VEC >
typename VEC::FLOAT	Geometry::SqDistPointSegment(const TVec3<VEC>& _p0, const TVec3<VEC>& _s0, const TVec3<VEC>& _s1)
{
	TVec3<VEC> ab = _s1 - _s0;
	TVec3<VEC> ac = _p0 - _s0;
	TVec3<VEC> bc = _p0 - _s1;
	typename VEC::FLOAT e = Vec::Dot(ac, ab);
	if (e <= 0.0f) { return Vec::Dot(ac, ac); }
	typename VEC::FLOAT f = Vec::Dot(ab, ab);
	if (e >= f) { return Vec::Dot(bc, bc); }
	return Vec::Dot(ac, ac) - e * e / f;
}

///-------------------------------------------------------------------------------------------------
/// 点と三角形の最近接点
template< typename VEC >
typename VEC::FLOAT	Geometry::ClosestPtPointTriangle(TVec3<VEC>& _ret, const TVec3<VEC>& _p0, const TVec3<VEC>& _t0, const TVec3<VEC>& _t1, const TVec3<VEC>& _t2)
{
	// PがAの外側の頂点領域の中にあるかチェック
	typename VEC::FLOAT ret = 0.0f;
	TVec3<VEC> ab = _t1 - _t0;
	TVec3<VEC> ac = _t2 - _t0;
	TVec3<VEC> ap = _p0 - _t0;
	typename VEC::FLOAT d0 = Vec::Dot(ab, ap);
	typename VEC::FLOAT d1 = Vec::Dot(ac, ap);
	if(d0 <= 0.0f && d1 <= 0.0f)
	{
		_ret = _t0;
		return Vec::Length2(_p0 - _ret);
	}

	// PがBの外側の頂点領域の中にあるかチェック
	TVec3<VEC> bp = _p0 - _t1;
	typename VEC::FLOAT d2 = Vec::Dot(ab, bp);
	typename VEC::FLOAT d3 = Vec::Dot(ac, bp);
	if (d2 >= 0.0f && d3 <= d2)
	{
		_ret = _t1;
		return Vec::Length2(_p0 - _ret);
	}

	// PがABの辺領域の中にあるかチェック。あればPのAB上に対する射影を返す
	typename VEC::FLOAT vc = d0 * d3 - d2 * d1;
	if (vc <= 0.0f && d0 >= 0.0f && d2 <= 0.0f)
	{
		typename VEC::FLOAT v = d0 / (d0 - d2);
		_ret = _t0 + v * ab;
		return Vec::Length2(_p0 - _ret);
	}

	// PがCの外側の頂点領域の中にあるかチェック
	TVec3<VEC> cp = _p0 - _t2;
	typename VEC::FLOAT d4 = Vec::Dot(ab, cp);
	typename VEC::FLOAT d5 = Vec::Dot(ac, cp);
	if (d5 >= 0.0f && d4 <= d5)
	{
		_ret = _t2;
		return Vec::Length2(_p0 - _ret);
	}

	// PがACの辺領域の中にあるかチェック。あればPのAC上に対する射影を返す
	typename VEC::FLOAT vb = d4 * d1 - d0 * d5;
	if (vb <= 0.0f && d1 >= 0.0f && d5 <= 0.0f)
	{
		typename VEC::FLOAT w = d1 / (d1 - d5);
		_ret = _t0 + w * ac;
		return Vec::Length2(_p0 - _ret);
	}

	// PがBCの辺領域の中にあるかチェック。あればPのBC上に対する射影を返す
	typename VEC::FLOAT va = d2 * d5 - d4 * d3;
	if (va <= 0.0f && (d3 - d2) >= 0.0f && (d4 - d5) >= 0.0f)
	{
		typename VEC::FLOAT w = (d3 - d2) / ((d3 - d2) + (d4 - d5));
		_ret = _t1 + w * (_t2 - _t1);
		return Vec::Length2(_p0 - _ret);
	}

	// Pは面領域の中。Qをその重心座標(u,v,w)を用いて算出
	typename VEC::FLOAT denom = 1.0f / (va + vb + vc);
	typename VEC::FLOAT v = vb * denom;
	typename VEC::FLOAT w = vc * denom;
	_ret = _t0 + ab + v * ac * w;
	return Vec::Length2(_p0 - _ret);
}


///-------------------------------------------------------------------------------------------------
/// 線分と線分の最近接点
template< typename VEC >
typename VEC::FLOAT	 Geometry::ClosestPtSegmentSegment(
	TVec3<VEC>& _ret0, TVec3<VEC>& _ret1,
	const TVec3<VEC>& _s00, const TVec3<VEC>& _s01,
	const TVec3<VEC>& _s10, const TVec3<VEC>& _s11,
	typename VEC::FLOAT* _t0/* = NULL*/, typename VEC::FLOAT* _t1/* = NULL*/)
{
	typename VEC::FLOAT t0 = 0.0f;
	typename VEC::FLOAT t1 = 0.0f;
	TVec3<VEC> d0 = _s01 - _s00;	// 線分s0の方向ベクトル
	TVec3<VEC> d1 = _s11 - _s10;	// 線分s1の方向ベクトル
	TVec3<VEC> r = _s00 - _s10;
	typename VEC::FLOAT a = Vec::Dot(d0, d0);	// 線分s0の距離の平方、常に非負
	typename VEC::FLOAT e = Vec::Dot(d1, d1);	// 線分s1の距離の平方、常に非負
	typename VEC::FLOAT f = Vec::Dot(d1, r);
	// 片方あるいは両方の線分が点に縮退しているかチェック
	if (a <= FLT_EPSILON && e <= FLT_EPSILON)
	{
		// 両面の線分が点に縮退
		t0 = t1 = 0.0f;
		if (_t0) { *_t0 = t0; }
		if (_t1) { *_t1 = t1; }
		_ret0 = _s00;
		_ret1 = _s10;
		TVec3<VEC> ab = _ret0 - _ret1;
		return Vec::Dot(ab, ab);
	}
	if (a <= FLT_EPSILON)
	{
		// 最初の線分が点に縮退
		t0 = 0.0f;
		t1 = clamp(0.0f, 1.0f, f / e);
	}
	else
	{
		typename VEC::FLOAT c = Vec::Dot(d0, r);
		if (e <= FLT_EPSILON)
		{
			// 2番目の線分が縮退
			t1 = 0.0f;
			t0 = clamp(0.0f, 1.0f, -c / a);
		}
		else
		{
			// ここから一般的な縮退の場合を開始
			typename VEC::FLOAT b = Vec::Dot(d0, d1);
			typename VEC::FLOAT denom = a * e - b * b;
			if (denom != 0.0f)
			{
				t0 = clamp(0.0f, 1.0f, (b * f - c * e) / denom);
			}
			else
			{
				t0 = 0.0f;
			}
			// 除算の延長の為いろいろ小技
			typename VEC::FLOAT tnom = b * t0 + f;
			if (tnom < 0.0f)
			{
				t1 = 0.0f;
				t0 = clamp(0.0f, 1.0f, -c / a);
			}
			else if (tnom > e)
			{
				t1 = 1.0f;
				t0 = clamp(0.0f, 1.0f, (b - c) / a);
			}
			else
			{
				t1 = tnom / e;
			}
		}
	}

	if (_t0) { *_t0 = t0; }
	if (_t1) { *_t1 = t1; }
	_ret0 = _s00 * d0 * t0;
	_ret1 = _s10 * d1 * t1;
	TVec3<VEC> ab = _ret0 - _ret1;
	return Vec::Dot(ab, ab);
}

///-------------------------------------------------------------------------------------------------
/// 線分と線分の距離
template< typename VEC >
typename VEC::FLOAT	Geometry::SqDistSegmentSegment(const TVec3<VEC>& _s00, const TVec3<VEC>& _s01, const TVec3<VEC>& _s10, const TVec3<VEC>& _s11)
{
	TVec3<VEC> d0 = _s01 - _s00;	// 線分s0の方向ベクトル
	TVec3<VEC> d1 = _s11 - _s10;	// 線分s1の方向ベクトル
	typename VEC::FLOAT a = Vec::Dot(d0, d0);	// 線分s0の距離の平方、常に非負
	typename VEC::FLOAT e = Vec::Dot(d1, d1);	// 線分s1の距離の平方、常に非負
	// 片方あるいは両方の線分が点に縮退しているかチェック
	if (a <= FLT_EPSILON && e <= FLT_EPSILON)
	{
		// 両面の線分が点に縮退
		TVec3<VEC> ab = _s00 - _s10;
		return Vec::Dot(ab, ab);
	}

	typename VEC::FLOAT t0 = 0.0f;
	typename VEC::FLOAT t1 = 0.0f;
	TVec3<VEC> r = _s00 - _s10;
	typename VEC::FLOAT f = Vec::Dot(d1, r);
	if (a <= FLT_EPSILON)
	{
		// 最初の線分が点に縮退
		t0 = 0.0f;
		t1 = clamp(0.0f, 1.0f, f / e);
	}
	else
	{
		typename VEC::FLOAT c = Vec::Dot(d0, r);
		if (e <= FLT_EPSILON)
		{
			// 2番目の線分が縮退
			t1 = 0.0f;
			t0 = clamp(0.0f, 1.0f, -c / a);
		}
		else
		{
			// ここから一般的な縮退の場合を開始
			typename VEC::FLOAT b = Vec::Dot(d0, d1);
			typename VEC::FLOAT denom = a * e - b * b;
			if (denom != 0.0f)
			{
				t0 = clamp(0.0f, 1.0f, (b * f - c * e) / denom);
			}
			else
			{
				t0 = 0.0f;
			}
			// 除算の延長の為いろいろ小技
			typename VEC::FLOAT tnom = b * t0 + f;
			if (tnom < 0.0f)
			{
				t1 = 0.0f;
				t0 = clamp(0.0f, 1.0f, -c / a);
			}
			else if (tnom > e)
			{
				t1 = 1.0f;
				t0 = clamp(0.0f, 1.0f, (b - c) / a);
			}
			else
			{
				t1 = tnom / e;
			}
		}
	}

	TVec3<VEC> ab = (_s00 * d0 * t0) - (_s10 * d1 * t1);
	return Vec::Dot(ab, ab);
}

///-------------------------------------------------------------------------------------------------
/// 線分と三角形の交点
template< typename VEC >
typename VEC::FLOAT		Geometry::IntersectSegmentTriangle(TVec3<VEC>& _ret, const TVec3<VEC>& _s0, const TVec3<VEC>& _s1, const TVec3<VEC>& _t0, const TVec3<VEC>& _t1, const TVec3<VEC>& _t2)
{
	TVec3<VEC> ab = _t1 - _t0;
	TVec3<VEC> ac = _t1 - _t0;
	TVec3<VEC> qp = _s0 - _s1;
	TVec3<VEC> n;
	// 三角形法線を計算。キャッシュすることも可能
	Vec::Cross(n, ab, ac);
	// 分母dを計算。d <= 0の場合線分は三角形に平行又は離れる方向なので早期チェック
	typename VEC::FLOAT d = Vec::Dot(qp, n);
	if (d <= 0.0f) { return -1.0f; }
	// pqと三角形の平面との交差の値tを計算
	TVec3<VEC> ap = _s0 - _t0;
	typename VEC::FLOAT t = Vec::Dot(ap, n);
	if (t < 0.0f) { return -1.0f; }
	if (t > d) { return -1.0f; }	// 線分に対して。レイの場合はこの判定不要
	// 重心座標の成分を算出し範囲内にあるか判定
	TVec3<VEC> e;
	Vec::Cross(e, qp, ap);
	TVec3<VEC> uvw;
	uvw[1] = Vec::Dot(ac, e);
	if (uvw[1] < 0.0f || uvw[1] > d) { return -1.0f; }
	uvw[2] = -Vec::Dot(ab, e);
	if (uvw[2] < 0.0f || uvw[1] + uvw[2] > d) { return -1.0f; }
	// 重心座標成分計算
	typename VEC::FLOAT ood = 1.0f / d;
	t *= ood;
	//uvw[1] *= ood;
	//uvw[2] *= ood;
	//uvw[0] = 1.0f - uvw[1] - uvw[2];
	// 重心座標から座標算出
	_ret = _s0 - qp * t;
	return Vec::Length2(_ret - _s0);
}

///-------------------------------------------------------------------------------------------------
/// 点と平面の最近接点
template< typename VEC3, typename VEC4 >
typename VEC4::FLOAT	Geometry::ClosestPtPointPlane(TVec3<VEC3>& _ret, const TVec3<VEC3>& _p, const TPlane<VEC4>& _plane)
{
	typename VEC4::FLOAT t = DistPointPlane(_p, _plane);
	_ret = _p - t * _plane.GetNrm();
	return t;
}

///-------------------------------------------------------------------------------------------------
/// 点と平面の距離
template< typename VEC3, typename VEC4 >
typename VEC4::FLOAT	Geometry::DistPointPlane(const TVec3<VEC3>& _p, const TPlane<VEC4>& _plane)
{
	return Vec::Dot(_p, _plane.GetNrm()) - _plane.GetDot();
}

///-------------------------------------------------------------------------------------------------
/// 直線と平面の交点
template< typename VEC3, typename VEC4 >
b8						Geometry::IntersectSegmentPlane(TVec3<VEC3>& _ret, const TVec3<VEC3>& _p0, const TVec3<VEC3>& _p1, const TPlane<VEC4>& _plane, typename VEC4::FLOAT* _t/* = NULL*/)
{
	// 方向のあるabに対して平面と交差するtを算出
	TVec3<VEC3> ab = _p1 - _p0;
	typename VEC4::FLOAT d = Vec::Dot(_plane.GetNrm(), ab);
	// tが[0~1]の間にある場合は交差点を返す
	b8 ret = false;
	if (d != 0.0f)
	{
		typename VEC4::FLOAT t = (_plane.GetDot() - Vec::Dot(_plane.GetNrm(), _p0)) / d;
		if (0.0f <= t && t <= 1.0f)
		{
			_ret = _p0 + t * ab;
			ret = true;
		}
	}

	if (_t) { *_t = t; }
	return ret;
}


///-------------------------------------------------------------------------------------------------
/// 点と球体の最近接点
template< typename VEC3, typename VEC4 >
typename VEC4::FLOAT	Geometry::ClosestPtPointSphere(TVec3<VEC3>& _ret, const TVec3<VEC3>& _p, const TSphere<VEC4>& _sphere)
{
	TVec3<VEC3> ab = _sphere.GetPos() - _p;
	typename VEC4::FLOAT ret = 0.0f;
	typename VEC4::FLOAT len = Vec::Length2(ab);
	if (len >= _sphere.GetRadius() * _sphere.GetRadius())
	{
		len = sqrtf(len);
		ret = len - _sphere.GetRadius();
		_ret = _p + ab * (ret / len);
	}
	return ret;
}

///-------------------------------------------------------------------------------------------------
/// 点と球体の距離
template< typename VEC3, typename VEC4 >
typename VEC4::FLOAT	Geometry::DistPointSphere(const TVec3<VEC3>& _p, const TSphere<VEC4>& _sphere)
{
	return max(0.0f, Vec::Length(_sphere.GetPos() - _p) - _sphere.GetRadius());
}

///-------------------------------------------------------------------------------------------------
/// レイと球体の交点
template< typename VEC3, typename VEC4 >
typename VEC4::FLOAT	Geometry::IntersectRaySphere(TVec3<VEC3>& _ret, const TVec3<VEC3>& _p, const TVec3<VEC3>& _d, const TSphere<VEC4>& _sphere)
{
	TVec3<VEC3> m = _p - _sphere.GetPos();
	typename VEC4::FLOAT b = Vec::Dot(m, _d);
	typename VEC4::FLOAT c = Vec::Dot(m, _m) - _sphere.GetRadius() * _sphere.GetRadius();
	// rの原点がsの外側にあり、(c > 0)、rがsから離れていく方向を指している場合(b > 0)に終了
	if (c > 0.0f && b > 0.0f) { return -1.0f; }
	typename VEC4::FLOAT discr = b * b - c;
	// 負の判定式はレイが球体を外れていることに一致
	if (discr < 0.0f) { return -1.0f; }
	// これでレイは球体と交差していることが確定。交差する最小の値tを算出
	typename VEC4::FLOAT t = -b - sqrtf(discr);
	// tが負である場合、例は級の内側から開始しているのでtを０にクランプ
	if (t < 0.0f) { t = 0.0f; }
	_ret = _p + t * _d;
	return t;
}

///-------------------------------------------------------------------------------------------------
/// レイと球体の交差判定
template< typename VEC3, typename VEC4 >
b8						Geometry::TestRaySphere(const TVec3<VEC3>& _p, const TVec3<VEC3>& _d, const TSphere<VEC4>& _sphere)
{
	TVec3<VEC3> m = _p - _sphere.GetPos();
	typename VEC4::FLOAT c = Vec::Dot(m, _m) - _sphere.GetRadius() * _sphere.GetRadius();
	// 少なくとも１つの実数解が存在しているので交差してる
	if (c <= 0.0f) { return true; }
	typename VEC4::FLOAT b = Vec::Dot(m, _d);
	// レイの始点が球体の外側でレイが離れる方向を指している場合は早期終了
	if (b > 0.0f) { return false; }
	typename VEC4::FLOAT discr = b * b - c;
	// 負の判定式はレイが球体を外れていることに一致
	if (discr < 0.0f) { return false; }
	return true;
}

///-------------------------------------------------------------------------------------------------
/// 三角形と球体の交差判定
template< typename VEC >
b8						Geometry::TestTriangleSphere(const TVec3<VEC>& _t0, const TVec3<VEC>& _t1, const TVec3<VEC>& _t2, const TSphere<VEC>& _sphere)
{
	TVec3<VEC> ret;
	typename VEC::FLOAT d = ClosestPtPointTriangle(ret, _sphere.GetPos(), _t0, _t1, _t2);
	return (d <= _sphere.GetRadius() * _sphere.GetRadius());
}

///-------------------------------------------------------------------------------------------------
/// 平面と球体の交差判定
template< typename VEC >
b8						Geometry::TestPlaneSphere(const TPlane<VEC>& _plane, const TSphere<VEC>& _sphere, b8* _inside/* = NULL*/)
{
	typename VEC::FLOAT dist = Vec::Dot(_sphere.GetPos(), _plane.GetNrm()); -_plane.GetDot();
	if (_inside) { *_inside = (dist < -_sphere.GetRadius()); }
	return (fabsf(dist) <= _sphere.GetRadius());
}


///-------------------------------------------------------------------------------------------------
/// 点とAABBの最近接点
template< typename VEC >
typename VEC::FLOAT		Geometry::ClosestPtPointAABB(TVec3<VEC>& _ret, const TVec3<VEC>& _p, const TAabb<VEC>& _aabb, const u32 _maskAxis/* = 0*/)
{
	f32 sqDist = 0.0f;
	for (u32 i = 0; i < 3; ++i)
	{
		// 除外指定された軸は見ない
		if (_maskAxis & (1u << i)) { continue; }

		typename VEC::FLOAT v = _p[i];
		if (v < _aabb.min[i])
		{
			sqDist += (_aabb.min[i] - v) * (_aabb.min[i] - v);
			v = _aabb.min[i];
		}
		if (v > _aabb.max[i])
		{
			sqDist += (v - _aabb.max[i]) * (v - _aabb.max[i]);
			v = _aabb.max[i];
		}
		_ret[i] = v;
	}
	return sqDist;
}

///-------------------------------------------------------------------------------------------------
/// 点とAABBの距離の平方
template< typename VEC >
typename VEC::FLOAT		Geometry::SqDistPointAABB(const TVec3<VEC>& _p, const TAabb<VEC>& _aabb, const u32 _maskAxis/* = 0*/)
{
	f32 sqDist = 0.0f;
	for (u32 i = 0; i < 3; ++i)
	{
		// 除外指定された軸は見ない
		if (_maskAxis & (1u << i)) { continue; }

		typename VEC::FLOAT v = _p[i];
		if (v < _aabb.min[i])
		{
			sqDist += (_aabb.min[i] - v) * (_aabb.min[i] - v);
		}
		if (v > _aabb.max[i])
		{
			sqDist += (v - _aabb.max[i]) * (v - _aabb.max[i]);
		}
	}
	return sqDist;
}

///-------------------------------------------------------------------------------------------------
/// レイとAABBの交差判定
template< typename VEC >
typename VEC::FLOAT		Geometry::IntersectRayAABB(TVec3<VEC>& _ret, const TVec3<VEC>& _p, const TVec3<VEC>& _d, const TAabb<VEC>& _aabb)
{
	typename VEC::FLOAT tMin = 0.0f;
	typename VEC::FLOAT tMax = FLT_MAX;
	for (u32 i = 0; i < 3; ++i)
	{
		if (fabsf(_d[i]) < FLT_EPSILON)
		{
			if (_p[i] < _aabb.min[i] || _p[i] > _aabb.max[i]) { return -1.0f; }
		}
		else
		{
			typename VEC::FLOAT ood = 1.0f / _d[i];
			typename VEC::FLOAT t1 = (_aabb.min[i] - _p[i]) * ood;
			typename VEC::FLOAT t2 = (_aabb.max[i] - _p[i]) * ood;
			// 逆だった場合スワップ
			if (t1 > t2)
			{
				typename VEC::FLOAT tmp = t1;
				t1 = t2;
				t2 = tmp;
			}
			if (t1 > tMin) { tMin = t1; };
			if (t2 < tMax) { tMax = t2; };
			if (tMin > tMax) { return -1.0f; }
		}
	}
	_ret = _p + _d * tMin;
	return tMin;
}

///-------------------------------------------------------------------------------------------------
/// レイとAABBの接触判定
template< typename VEC >
b8						Geometry::TestRayAABB(const TVec3<VEC>& _p, const TVec3<VEC>& _d, const TAabb<VEC>& _aabb)
{
	typename VEC::FLOAT tMin = 0.0f;
	typename VEC::FLOAT tMax = FLT_MAX;
	for (u32 i = 0; i < 3; ++i)
	{
		if (fabsf(_d[i]) < FLT_EPSILON)
		{
			if (_p[i] < _aabb.min[i] || _p[i] > _aabb.max[i]) { return false; }
		}
		else
		{
			typename VEC::FLOAT ood = 1.0f / _d[i];
			typename VEC::FLOAT t1 = (_aabb.min[i] - _p[i]) * ood;
			typename VEC::FLOAT t2 = (_aabb.max[i] - _p[i]) * ood;
			// 逆だった場合スワップ
			if (t1 > t2)
			{
				typename VEC::FLOAT tmp = t1;
				t1 = t2;
				t2 = tmp;
			}
			if (t1 > tMin) { tMin = t1; };
			if (t2 < tMax) { tMax = t2; };
			if (tMin > tMax) { return false; }
		}
	}
	return true;
}

///-------------------------------------------------------------------------------------------------
/// 線分とAABBの接触判定
template< typename VEC >
b8						Geometry::TestSegmentAABB(const TVec3<VEC>& _p0, const TVec3<VEC>& _p1, const TAabb<VEC>& _aabb)
{
	TVec3<VEC> c = (_aabb.min + _aabb.max) * 0.5f;	// ボックスの中心
	TVec3<VEC> e = _aabb.max - c;					// ボックスの幅の半分の範囲
	TVec3<VEC> m = (_p0 + _p1) * 0.5f;				// 線分の中心
	TVec3<VEC> d = _p1 - m;							// 線分の長さ半分ベクトル
	m = m - c;										// ボックスと線分を原点まで平行移動

	// 分離軸であるかチェック
	TVec3<VEC> adv;
	for (u32 i = 0; i < 3; ++i)
	{
		adv[i] = fabsf(d[i]);
		if (fabsf(m[i]) > e[i] + adv[i]) { return false; }
	}
	// 線分が座標軸に平行の時の演算の誤りを軽減
	adv += FLT_EPSILON;

	// 線分のベクトルの外積を座標軸に対してチェック
	if (fabsf(m[1] * d[2] - m[2] * d[1]) > e[1] * adv[2] + e[2] * adv[1]) { return false; }
	if (fabsf(m[2] * d[0] - m[0] * d[2]) > e[0] * adv[2] + e[2] * adv[0]) { return false; }
	if (fabsf(m[0] * d[1] - m[1] * d[0]) > e[0] * adv[1] + e[1] * adv[0]) { return false; }
	return true;
}

///-------------------------------------------------------------------------------------------------
/// 三角形とAABBの交差判定
template< typename VEC >
b8						Geometry::TestTriangleAABB(const TVec3<VEC>& _t0, const TVec3<VEC>& _t1, const TVec3<VEC>& _t2, const TAabb<VEC>& _aabb)
{
	TVec3<VEC> c = (_aabb.min + _aabb.max) * 0.5f;	// AABBの中心
	TVec3<VEC> e = _aabb.max - c;					// 正の範囲

	// 三角形をAABBから原点まで平行移動
	TVec3<VEC> v[3];
	v[0] = _t0 - c; v[1] = _t1 - c; v[2] = _t2 - c;
	// AABBの面法線に一致する3つの軸を判定
	for (u32 i = 0; i < 3; ++i)
	{
		if (max(v[0][i], max(v[1][i], v[2][i])) < -e[i] || min(v[0][i], min(v[1][i], v[2][i])) > e[i]) { return false; }
	}

	// 三角形に対して辺のベクトルを算出
	TVec3<VEC> f[3];
	f[0] = v[1] - v[0]; f[1] = v[2] - v[1]; f[2] = v[0] - v[2];
	// 9軸を判定
	TVec3<VEC> a[9] =
	{
		{ +0.0f,	-f[0][2],	+f[0][1] },
		{ +0.0f,	-f[1][2],	+f[1][1] },
		{ +0.0f,	-f[2][2],	+f[2][1] },
		{ +f[0][2],	+0.0f,		-f[0][0] },
		{ +f[1][2],	+0.0f,		-f[1][0] },
		{ +f[2][2],	+0.0f,		-f[2][0] },
		{ -f[0][1],	+f[0][0],	+0.0f },
		{ -f[1][1],	+f[1][0],	+0.0f },
		{ -f[2][1],	+f[2][0],	+0.0f },
	}
	for (u32 i = 0; i < ARRAYOF(a); ++i)
	{
		typename VEC::FLOAT p[3];
		typename VEC::FLOAT r = 0.0f;
		for (u32 j = 0; j < 3; ++j)
		{
			p[j] = Vec::Dot(v[j], a[i]);
			r += e[j] * fabsf(a[i][j]);
		}
		if (max(-max(p[0], max(p[1], p[2])), min(p[0], min(p[1], p[2]))) > r) { return false; }
	}

	// 平面とAABBの交差判定
	TVec3<VEC> planeNrm;
	Vec::Cross(planeNrm, f[0], f[1]);
	typename VEC::FLOAT planeDot = Vec::Dot(planeNrm, v[0]);
	typename VEC::FLOAT r = e[0] * fabsf(planeNrm[0]) + e[1] * fabsf(planeNrm[1]) + e[2] * fabsf(planeNrm[2]);
	// 平面からAABBの中心までの距離
	typename VEC::FLOAT s = Vec::Dot(planeNrm, c) - planeDot;
	return (fabsf(s) <= r);
}

///-------------------------------------------------------------------------------------------------
/// 平面とAABBの交差判定
template< typename VEC3, typename VEC4 >
b8						Geometry::TestPlaneAABB(const TPlane<VEC4>& _plane, const TAabb<VEC3>& _aabb, b8* _inside/* = NULL*/)
{
	TVec3<VEC3> c = (_aabb.max + _aabb.min) * 0.5f;	// AABBの中心
	TVec3<VEC3> e = _aabb.max - c;					// 正の範囲
	typename VEC4::FLOAT r = e[0] * fabsf(_plane.GetNrm()[0]) + e[1] * fabsf(_plane.GetNrm()[1]) + e[2] * fabsf(_plane.GetNrm()[2]);
	// 平面からAABBの中心までの距離
	typename VEC4::FLOAT s = Vec::Dot(_plane.GetNrm(), c) - _plane.GetDot();
	// 距離sが間隔[-r,+r]の範囲内に収まる場合は考査が起こる
	if (_inside) { *_inside = (s > r); }
	return (fabsf(s) <= r);
}

///-------------------------------------------------------------------------------------------------
/// 球体とAABBの交差判定
template< typename VEC3, typename VEC4 >
b8						Geometry::TestSphereAABB(const TSphere<VEC4>& _sphere, const TAabb<VEC3>& _aabb)
{
	// 球体の中心とAABBの間の距離の平方を算出
	typename VEC4::FLOAT  sqDist = SqDistPointAABB(_sphere.GetPos(), _aabb);
	// 球体とAABBが交差するのはそれらの間の距離が球体の半径より小さい場合
	return (sqDist <= _sphere.GetRadius() * _sphere.GetRadius());
}


POISON_END

#endif	// __GEOMETRY_INL__

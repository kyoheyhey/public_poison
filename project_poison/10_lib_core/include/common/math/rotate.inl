﻿//#pragma once
#ifndef __ROTATE_INL__
#define __ROTATE_INL__


POISON_BGN

//-------------------------------------------------------------------------------------------------
/// テンプレート宣言部分の省略
//#define VEC_TEMPLATE_HEAD		template< typename VEC >
//#define VEC2_CLASS_HEAD		TVec2< VEC >
//#define VEC3_CLASS_HEAD		TVec3< VEC >
//#define VEC4_CLASS_HEAD		TVec4< VEC >
#define EULER_CLASS_HEAD		TEuler< VEC >
#define QUAT_CLASS_HEAD			TQuat< VEC >

///*************************************************************************************************
///	オイラークラス
///*************************************************************************************************
VEC_TEMPLATE_HEAD
EULER_CLASS_HEAD::TEuler()
{
	x = y = z = 0.0f;
	static const ROT_TYPE s_default = ROT_ZXY;
	w = *PCast<const FLOAT*>(&s_default);
}
VEC_TEMPLATE_HEAD
EULER_CLASS_HEAD::TEuler(FLOAT _x, FLOAT _y, FLOAT _z, ROT_TYPE _type/* = ROT_ZXY*/)
{
	x = _x;
	y = _y;
	z = _z;
	w = *PCast<FLOAT*>(&_type);
}
VEC_TEMPLATE_HEAD
EULER_CLASS_HEAD::TEuler(const FLOAT* _xyz, ROT_TYPE _type/* = ROT_ZXY*/)
{
	Set(_xyz, _type);
}
VEC_TEMPLATE_HEAD
EULER_CLASS_HEAD::TEuler(const TVec3& _xyz, ROT_TYPE _type/* = ROT_ZXY*/)
{
	Set(_xyz, _type);
}

///-------------------------------------------------------------------------------------------------
/// compar operators
VEC_TEMPLATE_HEAD
EULER_CLASS_HEAD&	EULER_CLASS_HEAD::operator=(const TEuler& _rot)
{
	x = _rot.x; y = _rot.y; z = _rot.z; w = _rot.w;
	CHECK_ENABLE_VECTOR3();
	return *this;
}
VEC_TEMPLATE_HEAD
b8		EULER_CLASS_HEAD::operator==(const TEuler& _rot) const
{
	return (x == _rot.x && y == _rot.y && z == _rot.z && w == _rot.w);
}
VEC_TEMPLATE_HEAD
b8		EULER_CLASS_HEAD::operator!=(const TEuler& _rot) const
{
	return (x != _rot.x || y != _rot.y || z != _rot.z || w != _rot.w);
}

///-------------------------------------------------------------------------------------------------
/// assignment operators
VEC_TEMPLATE_HEAD
EULER_CLASS_HEAD&	EULER_CLASS_HEAD::operator+=(const TEuler& _rot)
{
	x += _rot.x;
	y += _rot.y;
	z += _rot.z;
	return *this;
}
VEC_TEMPLATE_HEAD
EULER_CLASS_HEAD&	EULER_CLASS_HEAD::operator-=(const TEuler& _rot)
{
	x -= _rot.x;
	y -= _rot.y;
	z -= _rot.z;
	return *this;
}

///-------------------------------------------------------------------------------------------------
/// binary operators
VEC_TEMPLATE_HEAD
EULER_CLASS_HEAD	EULER_CLASS_HEAD::operator+(const TEuler& _rot) const
{
	EULER_CLASS_HEAD temp;
	temp.x = x + _rot.x;
	temp.y = y + _rot.y;
	temp.z = z + _rot.z;
	return temp;
}
VEC_TEMPLATE_HEAD
EULER_CLASS_HEAD	EULER_CLASS_HEAD::operator-(const TEuler& _rot) const
{
	EULER_CLASS_HEAD temp;
	temp.x = x - _rot.x;
	temp.y = y - _rot.y;
	temp.z = z - _rot.z;
	return temp;
}

///-------------------------------------------------------------------------------------------------
/// Getter
VEC_TEMPLATE_HEAD
typename EULER_CLASS_HEAD::TVec3&		EULER_CLASS_HEAD::GetXYZ()
{
	return *PCast<TVec3*>(this);
}
VEC_TEMPLATE_HEAD
typename EULER_CLASS_HEAD::ROT_TYPE		EULER_CLASS_HEAD::GetRotType() const
{
	return *PCast<const ROT_TYPE*>(&w);
}

///-------------------------------------------------------------------------------------------------
/// Getter
VEC_TEMPLATE_HEAD
void	EULER_CLASS_HEAD::Set(FLOAT _x, FLOAT _y, FLOAT _z, ROT_TYPE _type/* = ROT_ZXY*/)
{
	x = _x;
	y = _y;
	z = _z;
	w = *PCast<FLOAT*>(&_type);
}
VEC_TEMPLATE_HEAD
void	EULER_CLASS_HEAD::Set(const FLOAT* _xyz, ROT_TYPE _type/* = ROT_ZXY*/)
{
	Set(_xyz[0], _xyz[1], _xyz[2], _type);
}
VEC_TEMPLATE_HEAD
void	EULER_CLASS_HEAD::Set(const TVec3& _xyz, ROT_TYPE _type/* = ROT_ZXY*/)
{
	Set(_xyz[0], _xyz[1], _xyz[2], _type);
}
VEC_TEMPLATE_HEAD
void	EULER_CLASS_HEAD::Set(ROT_TYPE _type)
{
	w = *PCast<FLOAT*>(&_type);
}

///-------------------------------------------------------------------------------------------------
/// 定数値
VEC_TEMPLATE_HEAD
const EULER_CLASS_HEAD	EULER_CLASS_HEAD::Zero(0.0f, 0.0f, 0.0f);



///*************************************************************************************************
/// クォータニオンクラス
///*************************************************************************************************
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD::TQuat()
{
	x = y = z = 0.0f;
	w = 1.0f;
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD::TQuat(FLOAT _x, FLOAT _y, FLOAT _z, FLOAT _w)
{
	x = _x;
	y = _y;
	z = _z;
	w = _w;
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD::TQuat(const FLOAT* _xyzw)
{
	Set(_xyzw);
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD::TQuat(const TVec4& _xyzw)
{
	Set(_xyzw);
}

///-------------------------------------------------------------------------------------------------
/// compar operators
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	QUAT_CLASS_HEAD::operator=(const TQuat& _rot)
{
	x = _rot.x; y = _rot.y; z = _rot.z; w = _rot.w;
	CHECK_ENABLE_VECTOR4();
	return *this;
}
VEC_TEMPLATE_HEAD
b8		QUAT_CLASS_HEAD::operator==(const TQuat& _rot) const
{
	return (x == _rot.x && y == _rot.y && z == _rot.z && w == _rot.w);
}
VEC_TEMPLATE_HEAD
b8		QUAT_CLASS_HEAD::operator!=(const TQuat& _rot) const
{
	return (x != _rot.x || y != _rot.y || z != _rot.z || w != _rot.w);
}

///-------------------------------------------------------------------------------------------------
/// assignment operators
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	QUAT_CLASS_HEAD::operator+=(const TQuat& _rot)
{
	x += _rot.x;
	y += _rot.y;
	z += _rot.z;
	w += _rot.w;
	return *this;
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	QUAT_CLASS_HEAD::operator-=(const TQuat& _rot)
{
	x -= _rot.x;
	y -= _rot.y;
	z -= _rot.z;
	w -= _rot.w;
	return *this;
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	QUAT_CLASS_HEAD::operator*=(const TQuat& _rot)
{
	w = w * _rot.w - x * _rot.x - y * _rot.y - z * _rot.z;
	x = w * _rot.x + x * _rot.w + y * _rot.z - z * _rot.y;
	y = w * _rot.y + y * _rot.w + z * _rot.x - x * _rot.z;
	z = w * _rot.z + z * _rot.w + x * _rot.y - y * _rot.x;
	return	*this;
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	QUAT_CLASS_HEAD::operator*=(FLOAT _var)
{
	x *= _var;
	y *= _var;
	z *= _var;
	w *= _var;
	return *this;
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	QUAT_CLASS_HEAD::operator/=(FLOAT _var)
{
	x /= _var;
	y /= _var;
	z /= _var;
	w /= _var;
	return *this;
}

///-------------------------------------------------------------------------------------------------
/// binary operators
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD	QUAT_CLASS_HEAD::operator+(const TQuat& _rot) const
{
	QUAT_CLASS_HEAD	temp;
	temp.x = x + _rot.x;
	temp.y = y + _rot.y;
	temp.z = z + _rot.z;
	temp.w = w + _rot.w;
	return	temp;
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD	QUAT_CLASS_HEAD::operator-(const TQuat& _rot) const
{
	QUAT_CLASS_HEAD	temp;
	temp.x = x - _rot.x;
	temp.y = y - _rot.y;
	temp.z = z - _rot.z;
	temp.w = w - _rot.w;
	return	temp;
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD	QUAT_CLASS_HEAD::operator*(const TQuat& _rot) const
{
	QUAT_CLASS_HEAD	temp;
	temp.w = w * _rot.w - x * _rot.x - y * _rot.y - z * _rot.z;
	temp.x = w * _rot.x + x * _rot.w + y * _rot.z - z * _rot.y;
	temp.y = w * _rot.y + y * _rot.w + z * _rot.x - x * _rot.z;
	temp.z = w * _rot.z + z * _rot.w + x * _rot.y - y * _rot.x;
	return	temp;
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD	QUAT_CLASS_HEAD::operator*(const FLOAT _var) const
{
	QUAT_CLASS_HEAD	temp;
	temp.x = x * _var;
	temp.y = y * _var;
	temp.z = z * _var;
	temp.w = w * _var;
	return	temp;
}
VEC_TEMPLATE_HEAD
QUAT_CLASS_HEAD	QUAT_CLASS_HEAD::operator/(const FLOAT _var) const
{
	QUAT_CLASS_HEAD	temp;
	temp.x = x / _var;
	temp.y = y / _var;
	temp.z = z / _var;
	temp.w = w / _var;
	return	temp;
}

///-------------------------------------------------------------------------------------------------
/// Getter
VEC_TEMPLATE_HEAD
typename QUAT_CLASS_HEAD::TVec4&	QUAT_CLASS_HEAD::GetXYZW()
{
	return *PCast<TVec4*>(this);
}

///-------------------------------------------------------------------------------------------------
/// Setter
VEC_TEMPLATE_HEAD
void	QUAT_CLASS_HEAD::Set(FLOAT _x, FLOAT _y, FLOAT _z, FLOAT _w)
{
	x = _x;
	y = _y;
	z = _z;
	w = _w;
}
VEC_TEMPLATE_HEAD
void	QUAT_CLASS_HEAD::Set(const FLOAT* _xyzw)
{
	Set(_xyzw[0], _xyzw[1], _xyzw[2], _xyzw[3]);
}
VEC_TEMPLATE_HEAD
void	QUAT_CLASS_HEAD::Set(const TVec4& _xyzw)
{
	Set(_xyzw[0], _xyzw[1], _xyzw[2], _xyzw[3]);
}

///-------------------------------------------------------------------------------------------------
/// 定数値
VEC_TEMPLATE_HEAD
const QUAT_CLASS_HEAD	QUAT_CLASS_HEAD::Identity = { +0.0f, +0.0f, +0.0f, +1.0f };




POISON_END

#endif	// __ROTATE_INL__

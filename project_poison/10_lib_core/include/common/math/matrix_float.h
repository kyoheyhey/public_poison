﻿//#pragma once
#ifndef __MATRIX_FLOAT_H__
#define __MATRIX_FLOAT_H__


//-------------------------------------------------------------------------------------------------
// include


//-------------------------------------------------------------------------------------------------
// prototype


POISON_BGN

//*************************************************************************************************
//@brief	実クラス
//*************************************************************************************************
ALIGN(16) struct MATRIX22
{
	//-------------------------------------------------------------------------------------------------
	//@brief	ベクトル型
	typedef CVec2				VECTOR2;
	typedef CVec3				VECTOR3;
	typedef CVec4				VECTOR4;
	typedef CEuler				EULER;
	typedef CQuat				QUAT;
	//@brief	フロート型
	typedef	VECTOR2::FLOAT		FLOAT;
	//@brief	参照型フロート型
	typedef VECTOR2::REF_FLOAT	REF_FLOAT;

	//-------------------------------------------------------------------------------------------------
	//@brief	operator
	inline const VECTOR2&	operator[](const u32 _index) const { return v[_index]; }	///< 配列アクセスconst
	inline VECTOR2&			operator[](const u32 _index) { return v[_index]; }			///< 配列アクセス

	//-------------------------------------------------------------------------------------------------
	//@brief	バッファ取得
	inline const VECTOR2*	GetPtr() const { return v; }
	inline VECTOR2*			GetPtr() { return v; }

	union {
		struct {
			VECTOR2 v[2];
		};
		FLOAT m[2][4];
	};
	MATRIX22() {}
};

ALIGN(16) struct MATRIX23
{
	//-------------------------------------------------------------------------------------------------
	//@brief	ベクトル型
	typedef CVec2				VECTOR2;
	typedef CVec3				VECTOR3;
	typedef CVec4				VECTOR4;
	typedef CEuler				EULER;
	typedef CQuat				QUAT;
	//@brief	フロート型
	typedef	VECTOR3::FLOAT		FLOAT;
	//@brief	参照型フロート型
	typedef VECTOR3::REF_FLOAT	REF_FLOAT;
	//@brief	マトリックス型
	typedef MATRIX22			MTX22;

	//-------------------------------------------------------------------------------------------------
	//@brief	operator
	inline const VECTOR3&	operator[](const u32 _index) const { return v[_index]; }	///< 配列アクセスconst
	inline VECTOR3&			operator[](const u32 _index) { return v[_index]; }			///< 配列アクセス

	//-------------------------------------------------------------------------------------------------
	//@brief	バッファ取得
	inline const VECTOR3*	GetPtr() const { return v; }
	inline VECTOR3*			GetPtr() { return v; }

	union {
		struct {
			VECTOR3 v[2];
		};
		FLOAT m[2][4];
	};
	MATRIX23() {}
};

ALIGN(16) struct MATRIX33
{
	//-------------------------------------------------------------------------------------------------
	//@brief	ベクトル型
	typedef CVec2				VECTOR2;
	typedef CVec3				VECTOR3;
	typedef CVec4				VECTOR4;
	typedef CEuler				EULER;
	typedef CQuat				QUAT;
	//@brief	フロート型
	typedef	VECTOR3::FLOAT		FLOAT;
	//@brief	参照型フロート型
	typedef VECTOR3::REF_FLOAT	REF_FLOAT;
	//@brief	マトリックス型
	typedef MATRIX22			MTX22;
	typedef MATRIX23			MTX23;

	//-------------------------------------------------------------------------------------------------
	//@brief	operator
	inline const VECTOR3&	operator[](const u32 _index) const { return v[_index]; }	///< 配列アクセスconst
	inline VECTOR3&			operator[](const u32 _index) { return v[_index]; }			///< 配列アクセス

	//-------------------------------------------------------------------------------------------------
	//@brief	バッファ取得
	inline const VECTOR3*	GetPtr() const { return v; }
	inline VECTOR3*			GetPtr() { return v; }

	union {
		struct {
			VECTOR3 v[3];
		};
		FLOAT m[3][4];
	};
	MATRIX33() {}
};

ALIGN(16) struct MATRIX34
{
	//-------------------------------------------------------------------------------------------------
	//@brief	ベクトル型
	typedef CVec2				VECTOR2;
	typedef CVec3				VECTOR3;
	typedef CVec4				VECTOR4;
	typedef CEuler				EULER;
	typedef CQuat				QUAT;
	//@brief	フロート型
	typedef	VECTOR4::FLOAT		FLOAT;
	//@brief	参照型フロート型
	typedef VECTOR4::REF_FLOAT	REF_FLOAT;
	//@brief	マトリックス型
	typedef MATRIX22			MTX22;
	typedef MATRIX23			MTX23;
	typedef MATRIX33			MTX33;

	//-------------------------------------------------------------------------------------------------
	//@brief	operator
	inline const VECTOR4&	operator[](const u32 _index) const { return v[_index]; }	///< 配列アクセスconst
	inline VECTOR4&			operator[](const u32 _index) { return v[_index]; }			///< 配列アクセス

	//-------------------------------------------------------------------------------------------------
	//@brief	バッファ取得
	inline const VECTOR4*	GetPtr() const { return v; }
	inline VECTOR4*			GetPtr() { return v; }

	union {
		struct {
			VECTOR4 v[3];
		};
		FLOAT m[3][4];
	};
	MATRIX34() {}
};

ALIGN(16) struct MATRIX44
{
	//-------------------------------------------------------------------------------------------------
	//@brief	ベクトル型
	typedef CVec2				VECTOR2;
	typedef CVec3				VECTOR3;
	typedef CVec4				VECTOR4;
	typedef CEuler				EULER;
	typedef CQuat				QUAT;
	//@brief	フロート型
	typedef	VECTOR4::FLOAT		FLOAT;
	//@brief	参照型フロート型
	typedef VECTOR4::REF_FLOAT	REF_FLOAT;
	//@brief	マトリックス型
	typedef MATRIX22			MTX22;
	typedef MATRIX23			MTX23;
	typedef MATRIX33			MTX33;
	typedef MATRIX34			MTX34;

	//-------------------------------------------------------------------------------------------------
	//@brief	operator
	inline const VECTOR4&	operator[](const u32 _index) const { return v[_index]; }	///< 配列アクセスconst
	inline VECTOR4&			operator[](const u32 _index) { return v[_index]; }			///< 配列アクセス

	//-------------------------------------------------------------------------------------------------
	//@brief	バッファ取得
	inline const VECTOR4*	GetPtr() const { return v; }
	inline VECTOR4*			GetPtr() { return v; }

	union {
		struct {
			VECTOR4 v[4];
		};
		FLOAT m[4][4];
	};
	MATRIX44() {}
};


POISON_END

#endif	// __MATRIX_FLOAT_H__

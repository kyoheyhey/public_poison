﻿//#pragma once
#ifndef __VECTOR_FUNC_INL__
#define __VECTOR_FUNC_INL__


POISON_BGN

//-------------------------------------------------------------------------------------------------
/// テンプレート宣言部分の省略
//#define VEC_TEMPLATE_HEAD	template< typename VEC >
//#define VEC2_CLASS_HEAD		TVec2< VEC >
//#define VEC3_CLASS_HEAD		TVec3< VEC >
//#define VEC4_CLASS_HEAD		TVec4< VEC >


///*************************************************************************************************
/// ２ベクトル関数群
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
// 配列へのコピー
VEC_TEMPLATE_HEAD
void					Vec::Copy(typename VEC2_CLASS_HEAD::FLOAT* _ret, const VEC2_CLASS_HEAD& _vec)
{
	_ret[0] = _vec[0]; _ret[1] = _vec[1];
}

VEC_TEMPLATE_HEAD
void					Vec::Copy(typename VEC2_CLASS_HEAD::FLOAT(&_ret)[2], const VEC2_CLASS_HEAD& _vec)
{
	_ret[0] = _vec[0]; _ret[1] = _vec[1];
}

///-------------------------------------------------------------------------------------------------
// ベクトルの最大最小の要素
VEC_TEMPLATE_HEAD
typename VEC2_CLASS_HEAD::FLOAT		Vec::Min(const VEC2_CLASS_HEAD& _vec)
{
	return _vec.GetX() <= _vec.GetY() ? _vec.GetX() : _vec.GetY();
}
VEC_TEMPLATE_HEAD
typename VEC2_CLASS_HEAD::FLOAT		Vec::Max(const VEC2_CLASS_HEAD& _vec)
{
	return _vec.GetX() >= _vec.GetY() ? _vec.GetX() : _vec.GetY();
}

///-------------------------------------------------------------------------------------------------
// ベクトルの最大最小
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	Vec::Min(VEC2_CLASS_HEAD& _ret, const VEC2_CLASS_HEAD& _vecA, const VEC2_CLASS_HEAD& _vecB)
{
	_ret.SetX((_vecA.GetX() <= _vecB.GetX()) ? _vecA.GetX() : _vecB.GetX());
	_ret.SetY((_vecA.GetY() <= _vecB.GetY()) ? _vecA.GetY() : _vecB.GetY());
	return _ret;
}
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	Vec::Max(VEC2_CLASS_HEAD& _ret, const VEC2_CLASS_HEAD& _vecA, const VEC2_CLASS_HEAD& _vecB)
{
	_ret.SetX((_vecA.GetX() >= _vecB.GetX()) ? _vecA.GetX() : _vecB.GetX());
	_ret.SetY((_vecA.GetY() >= _vecB.GetY()) ? _vecA.GetY() : _vecB.GetY());
	return _ret;
}

///-------------------------------------------------------------------------------------------------
// ベクトルの大きさ
VEC_TEMPLATE_HEAD
typename VEC2_CLASS_HEAD::FLOAT		Vec::Length(const VEC2_CLASS_HEAD& _vec)
{
	return sqrtf(Length2(_vec));
}

///-------------------------------------------------------------------------------------------------
// ベクトルの２乗の大きさ
VEC_TEMPLATE_HEAD
typename VEC2_CLASS_HEAD::FLOAT		Vec::Length2(const VEC2_CLASS_HEAD& _vec)
{
	return (_vec.x * _vec.x) + (_vec.y * _vec.y);
}

///-------------------------------------------------------------------------------------------------
// 単位ベクトル
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	Vec::Normalize(VEC2_CLASS_HEAD& _ret, const VEC2_CLASS_HEAD& _src)
{
	typename VEC2_CLASS_HEAD::FLOAT len = Length(_src);
	CHECK_ENABLE_NORMALIZE(len);
	return _ret = _src / len
}

VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD		Vec::Normalize(const VEC2_CLASS_HEAD& _src)
{
	typename VEC2_CLASS_HEAD::FLOAT len = Length(_src);
	CHECK_ENABLE_NORMALIZE(len);
	return _src / len;
}

///-------------------------------------------------------------------------------------------------
//２つのベクトルの距離
VEC_TEMPLATE_HEAD
typename VEC2_CLASS_HEAD::FLOAT		Vec::Distance(const VEC2_CLASS_HEAD& _vecA, const VEC2_CLASS_HEAD& _vecB)
{
	return Length(_vecB - _vecA);
}

///-------------------------------------------------------------------------------------------------
// ベクトルの加算 = Ａ＋Ｂ
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	Vec::Add(VEC2_CLASS_HEAD& _ret, const VEC2_CLASS_HEAD& _vecA, const VEC2_CLASS_HEAD& _vecB)
{
	return _ret = _vecA + _vecB;
}

///-------------------------------------------------------------------------------------------------
// ベクトルの減算 = Ａ－Ｂ
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	Vec::Sub(VEC2_CLASS_HEAD& _ret, const VEC2_CLASS_HEAD& _vecA, const VEC2_CLASS_HEAD& _vecB)
{
	return _ret = _vecA - _vecB;
}

///-------------------------------------------------------------------------------------------------
// ベクトルのスカラー倍
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	Vec::Scale(VEC2_CLASS_HEAD& _ret, const VEC2_CLASS_HEAD& _vec, const typename VEC2_CLASS_HEAD::FLOAT _scalar)
{
	return _ret = _vec * _scalar;
}

///-------------------------------------------------------------------------------------------------
// ベクトルの内積 = Ａ・Ｂ
VEC_TEMPLATE_HEAD
typename VEC2_CLASS_HEAD::FLOAT		Vec::Dot(const VEC2_CLASS_HEAD& _vecA, const VEC2_CLASS_HEAD& _vecB)
{
	return (_vecA.x * _vecB.x) + (_vecA.y * _vecB.y);
}

///-------------------------------------------------------------------------------------------------
// ベクトルの外積 = Ａ×Ｂ
VEC_TEMPLATE_HEAD
typename VEC2_CLASS_HEAD::FLOAT		Vec::Cross(const VEC2_CLASS_HEAD& _vecA, const VEC2_CLASS_HEAD& _vecB)
{
	return (_vecA.x * _vecB.y) - (_vecA.y * _vecB.x);
}

///-------------------------------------------------------------------------------------------------
//２つのベクトルのなすsin値
VEC_TEMPLATE_HEAD
typename VEC2_CLASS_HEAD::FLOAT		Vec::Sin(const VEC2_CLASS_HEAD& _vecA, const VEC2_CLASS_HEAD& _vecB)
{
	return Cross(_vecA, _vecB) /
		(Length(_vecA) * Length(_vecB));
}

///-------------------------------------------------------------------------------------------------
//２つのベクトルのなすcos値
VEC_TEMPLATE_HEAD
typename VEC2_CLASS_HEAD::FLOAT		Vec::Cos(const VEC2_CLASS_HEAD& _vecA, const VEC2_CLASS_HEAD& _vecB)
{
	return Dot(_vecA, _vecB) /
		(Length(_vecA) * Length(_vecB));
}

///-------------------------------------------------------------------------------------------------
// 線形補間
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	Lerp(VEC2_CLASS_HEAD& _ret, const VEC2_CLASS_HEAD& _vA, const VEC2_CLASS_HEAD& _vB, const typename VEC2_CLASS_HEAD::FLOAT _t)
{
	_ret.x = Math::Lerp(_vA.x, _vB.x, _t);
	_ret.y = Math::Lerp(_vA.y, _vB.y, _t);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
// エルミート補間
VEC_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	Vec::Hermite(VEC2_CLASS_HEAD& _ret, const VEC2_CLASS_HEAD& _posA, const VEC2_CLASS_HEAD& _vecA, const VEC2_CLASS_HEAD& _posB, const VEC2_CLASS_HEAD& _vecB, const typename VEC2_CLASS_HEAD::FLOAT _t)
{
	_ret.x = Math::Hermite(_posA.x, _posB.x, _vecA.x, _vecB.x, _t);
	_ret.y = Math::Hermite(_posA.y, _posB.y, _vecA.y, _vecB.y, _t);
	return _ret;
}



///*************************************************************************************************
///３ベクトル関数群
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
// 配列へのコピー
VEC_TEMPLATE_HEAD
void				Vec::Copy(typename VEC3_CLASS_HEAD::FLOAT* _ret, const VEC3_CLASS_HEAD& _vec)
{
	_ret[0] = _vec[0]; _ret[1] = _vec[1]; _ret[2] = _vec[2];
}

VEC_TEMPLATE_HEAD
void				Vec::Copy(typename VEC3_CLASS_HEAD::FLOAT(&_ret)[3], const VEC3_CLASS_HEAD& _vec)
{
	_ret[0] = _vec[0]; _ret[1] = _vec[1]; _ret[2] = _vec[2];
}

///-------------------------------------------------------------------------------------------------
// ベクトルの最大最小の要素
VEC_TEMPLATE_HEAD
typename VEC3_CLASS_HEAD::FLOAT		Vec::Min(const VEC3_CLASS_HEAD& _vec)
{
	VEC3_CLASS_HEAD::FLOAT minXY = _vec.GetX() <= _vec.GetY() ? _vec.GetX() : _vec.GetY();
	return minXY <= _vec.GetZ() ? minXY : _vec.GetZ();
}
VEC_TEMPLATE_HEAD
typename VEC3_CLASS_HEAD::FLOAT		Vec::Max(const VEC3_CLASS_HEAD& _vec)
{
	VEC3_CLASS_HEAD::FLOAT maxXY = _vec.GetX() >= _vec.GetY() ? _vec.GetX() : _vec.GetY();
	return maxXY >= _vec.GetZ() ? maxXY : _vec.GetZ();
}

///-------------------------------------------------------------------------------------------------
// ベクトルの最大最小
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	Vec::Min(VEC3_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _vecA, const VEC3_CLASS_HEAD& _vecB)
{
	_ret.SetX((_vecA.GetX() <= _vecB.GetX()) ? _vecA.GetX() : _vecB.GetX());
	_ret.SetY((_vecA.GetY() <= _vecB.GetY()) ? _vecA.GetY() : _vecB.GetY());
	_ret.SetZ((_vecA.GetZ() <= _vecB.GetZ()) ? _vecA.GetZ() : _vecB.GetZ());
	return _ret;
}
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	Vec::Max(VEC3_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _vecA, const VEC3_CLASS_HEAD& _vecB)
{
	_ret.SetX((_vecA.GetX() >= _vecB.GetX()) ? _vecA.GetX() : _vecB.GetX());
	_ret.SetY((_vecA.GetY() >= _vecB.GetY()) ? _vecA.GetY() : _vecB.GetY());
	_ret.SetZ((_vecA.GetZ() >= _vecB.GetZ()) ? _vecA.GetZ() : _vecB.GetZ());
	return _ret;
}

///-------------------------------------------------------------------------------------------------
// ベクトルの大きさ
VEC_TEMPLATE_HEAD
typename VEC3_CLASS_HEAD::FLOAT		Vec::Length(const VEC3_CLASS_HEAD& _vec)
{
	return sqrtf(Length2(_vec));
}

///-------------------------------------------------------------------------------------------------
// ベクトルの２乗大きさ
VEC_TEMPLATE_HEAD
typename VEC3_CLASS_HEAD::FLOAT		Vec::Length2(const VEC3_CLASS_HEAD& _vec)
{
	return (_vec.x * _vec.x) + (_vec.y * _vec.y) + (_vec.z * _vec.z);
}

///-------------------------------------------------------------------------------------------------
// 単位ベクトル
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&		Vec::Normalize(VEC3_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _src)
{
	typename VEC3_CLASS_HEAD::FLOAT len = Length(_src);
	CHECK_ENABLE_NORMALIZE(len);
	return _ret = _src / len;
}

VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD			Vec::Normalize(const VEC3_CLASS_HEAD& _src)
{
	typename VEC3_CLASS_HEAD::FLOAT len = Length(_src);
	CHECK_ENABLE_NORMALIZE(len);
	return _src / len;
}

///-------------------------------------------------------------------------------------------------
// ２つのベクトルの距離
VEC_TEMPLATE_HEAD
typename VEC3_CLASS_HEAD::FLOAT		Vec::Distance(const VEC3_CLASS_HEAD& _vecA, const VEC3_CLASS_HEAD& _vecB)
{
	return Length(_vecB - _vecA);
}

///-------------------------------------------------------------------------------------------------
// ベクトルの加算 = Ａ＋Ｂ
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	Vec::Add(VEC3_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _vecA, const VEC3_CLASS_HEAD& _vecB)
{
	return _ret = _vecA + _vecB;
}

///-------------------------------------------------------------------------------------------------
// ベクトルの減算 = Ａ－Ｂ
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	Vec::Sub(VEC3_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _vecA, const VEC3_CLASS_HEAD& _vecB)
{
	return _ret = _vecA - _vecB;
}

///-------------------------------------------------------------------------------------------------
// ベクトルのスカラー倍
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	Vec::Scale(VEC3_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _vec, const typename VEC3_CLASS_HEAD::FLOAT _scalar)
{
	return _ret = _vec * _scalar;
}

///-------------------------------------------------------------------------------------------------
// ベクトルの内積 = Ａ・Ｂ
VEC_TEMPLATE_HEAD
typename VEC3_CLASS_HEAD::FLOAT		Vec::Dot(const VEC3_CLASS_HEAD& _vecA, const VEC3_CLASS_HEAD& _vecB)
{
	return (_vecA.x * _vecB.x) + (_vecA.y * _vecB.y) + (_vecA.z * _vecB.z);
}

///-------------------------------------------------------------------------------------------------
// ベクトルの外積 = Ａ×Ｂ
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	Vec::Cross(VEC3_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _vecA, const VEC3_CLASS_HEAD& _vecB)
{
	_ret.x = (_vecA.y*_vecB.z) - (_vecA.z*_vecB.y);
	_ret.y = (_vecA.z*_vecB.x) - (_vecA.x*_vecB.z);
	_ret.z = (_vecA.x*_vecB.y) - (_vecA.y*_vecB.x);
	return _ret;
}

VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD		Vec::Cross(const VEC3_CLASS_HEAD& _vecA, const VEC3_CLASS_HEAD& _vecB)
{
	VEC3_CLASS_HEAD ret(
		(_vecA.y * _vecB.z) - (_vecA.z * _vecB.y),
		(_vecA.z * _vecB.x) - (_vecA.x * _vecB.z),
		(_vecA.x * _vecB.y) - (_vecA.y * _vecB.x)
	);
	return ret;
}

///-------------------------------------------------------------------------------------------------
// スカラー三重奏 = Ａ・(Ｂ×Ｃ)
VEC_TEMPLATE_HEAD
typename VEC3_CLASS_HEAD::FLOAT		Vec::ScalarTrio(const VEC3_CLASS_HEAD& _vecA, const VEC3_CLASS_HEAD& _vecB, const VEC3_CLASS_HEAD& _vecC)
{
	VEC3_CLASS_HEAD vec((_vecB.y*_vecC.z) - (_vecB.z*_vecC.y),
		(_vecB.z*_vecC.x) - (_vecB.x*_vecC.z),
		(_vecB.x*_vecC.y) - (_vecB.y*_vecC.x)
		);
	return	Dot(_vecA, vec);
}

///-------------------------------------------------------------------------------------------------
// ベクトル三重奏 = Ａ×(Ｂ×Ｃ)
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	Vec::VectorTrio(VEC3_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _vecA, const VEC3_CLASS_HEAD& _vecB, VEC3_CLASS_HEAD& _vecC)
{
	VEC3_CLASS_HEAD vec;
	Cross(vec, _vecB, _vecC);
	Cross(_ret, _vecA, vec);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
// ベクトルの射影
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	Vec::Projection(VEC3_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _srcVec, const VEC3_CLASS_HEAD& _nVec)
{
#if 0
	VEC3_CLASS_HEAD vec;
	Cross(vec, _nVec, _srcVec);
	Cross(_ret, vec, _nVec);
#endif // 0
	_ret = _srcVec - _nVec * Vec::Dot(_nVec, _srcVec);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
// 線形補間
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	Vec::Lerp(VEC3_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _vA, const VEC3_CLASS_HEAD& _vB, const typename VEC3_CLASS_HEAD::FLOAT _t)
{
	_ret.x = Math::Lerp(_vA.x, _vB.x, _t);
	_ret.y = Math::Lerp(_vA.y, _vB.y, _t);
	_ret.z = Math::Lerp(_vA.z, _vB.z, _t);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
// エルミート補間
VEC_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	Vec::Hermite(VEC3_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _posA, const VEC3_CLASS_HEAD& _vecA, const VEC3_CLASS_HEAD& _posB, const VEC3_CLASS_HEAD& _vecB, const typename VEC3_CLASS_HEAD::FLOAT _t)
{
	_ret.x = Math::Hermite(_posA.x, _posB.x, _vecA.x, _vecB.x, _t);
	_ret.y = Math::Hermite(_posA.y, _posB.y, _vecA.y, _vecB.y, _t);
	_ret.z = Math::Hermite(_posA.z, _posB.z, _vecA.z, _vecB.z, _t);
	return _ret;
}



///*************************************************************************************************
/// ４ベクトル関数群
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
// 配列へのコピー
VEC_TEMPLATE_HEAD
void				Vec::Copy(typename VEC4_CLASS_HEAD::FLOAT* _ret, const VEC4_CLASS_HEAD& _vec)
{
	_ret[0] = _vec[0]; _ret[1] = _vec[1]; _ret[2] = _vec[2]; _ret[3] = _vec[3];
}

VEC_TEMPLATE_HEAD
void				Vec::Copy(typename VEC4_CLASS_HEAD::FLOAT(&_ret)[4], const VEC4_CLASS_HEAD& _vec)
{
	_ret[0] = _vec[0]; _ret[1] = _vec[1]; _ret[2] = _vec[2]; _ret[3] = _vec[3];
}

///-------------------------------------------------------------------------------------------------
// ベクトルの最大最小の要素
VEC_TEMPLATE_HEAD
typename VEC4_CLASS_HEAD::FLOAT		Vec::Min(const VEC4_CLASS_HEAD& _vec)
{
	VEC4_CLASS_HEAD::FLOAT minXY = _vec.GetX() <= _vec.GetY() ? _vec.GetX() : _vec.GetY();
	VEC4_CLASS_HEAD::FLOAT minZW = _vec.GetZ() <= _vec.GetW() ? _vec.GetZ() : _vec.GetW();
	return minXY <= minZW ? minXY : minZW;
}
VEC_TEMPLATE_HEAD
typename VEC4_CLASS_HEAD::FLOAT		Vec::Max(const VEC4_CLASS_HEAD& _vec)
{
	VEC4_CLASS_HEAD::FLOAT maxXY = _vec.GetX() >= _vec.GetY() ? _vec.GetX() : _vec.GetY();
	VEC4_CLASS_HEAD::FLOAT maxZW = _vec.GetZ() >= _vec.GetW() ? _vec.GetZ() : _vec.GetW();
	return maxXY >= maxZW ? maxXY : maxZW;
}

///-------------------------------------------------------------------------------------------------
// ベクトルの最大最小
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	Vec::Min(VEC4_CLASS_HEAD& _ret, const VEC4_CLASS_HEAD& _vecA, const VEC4_CLASS_HEAD& _vecB)
{
	_ret.SetX((_vecA.GetX() <= _vecB.GetX()) ? _vecA.GetX() : _vecB.GetX());
	_ret.SetY((_vecA.GetY() <= _vecB.GetY()) ? _vecA.GetY() : _vecB.GetY());
	_ret.SetZ((_vecA.GetZ() <= _vecB.GetZ()) ? _vecA.GetZ() : _vecB.GetZ());
	_ret.SetW((_vecA.GetW() <= _vecB.GetW()) ? _vecA.GetW() : _vecB.GetW());
	return _ret;
}
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	Vec::Max(VEC4_CLASS_HEAD& _ret, const VEC4_CLASS_HEAD& _vecA, const VEC4_CLASS_HEAD& _vecB)
{
	_ret.SetX((_vecA.GetX() >= _vecB.GetX()) ? _vecA.GetX() : _vecB.GetX());
	_ret.SetY((_vecA.GetY() >= _vecB.GetY()) ? _vecA.GetY() : _vecB.GetY());
	_ret.SetZ((_vecA.GetZ() >= _vecB.GetZ()) ? _vecA.GetZ() : _vecB.GetZ());
	_ret.SetW((_vecA.GetW() >= _vecB.GetW()) ? _vecA.GetW() : _vecB.GetW());
	return _ret;
}

///-------------------------------------------------------------------------------------------------
// ベクトルの大きさ
VEC_TEMPLATE_HEAD
typename VEC4_CLASS_HEAD::FLOAT		Vec::Length(const VEC4_CLASS_HEAD& _vec)
{
	return sqrtf(Length2(_vec));
}

///-------------------------------------------------------------------------------------------------
// ベクトルの２乗大きさ
VEC_TEMPLATE_HEAD
typename VEC4_CLASS_HEAD::FLOAT		Vec::Length2(const VEC4_CLASS_HEAD& _vec)
{
	return (_vec.x * _vec.x) + (_vec.y * _vec.y) + (_vec.z * _vec.z) + (_vec.w * _vec.w);
}

///-------------------------------------------------------------------------------------------------
// 単位ベクトル
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	Vec::Normalize(VEC4_CLASS_HEAD& _ret, const VEC4_CLASS_HEAD& _src)
{
	typename VEC4_CLASS_HEAD::FLOAT len = Length(_src);
	CHECK_ENABLE_NORMALIZE(len);
	return _ret = _src / len;
}

VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD		Vec::Normalize(const VEC4_CLASS_HEAD& _src)
{
	typename VEC4_CLASS_HEAD::FLOAT len = Length(_src);
	CHECK_ENABLE_NORMALIZE(len);
	return _src / len;
}

///-------------------------------------------------------------------------------------------------
// ２つのベクトルの距離
VEC_TEMPLATE_HEAD
typename VEC4_CLASS_HEAD::FLOAT		Vec::Distance(const VEC4_CLASS_HEAD& _vecA, const VEC4_CLASS_HEAD& _vecB)
{
	return Length(_vecB - _vecA);
}

///-------------------------------------------------------------------------------------------------
// 線形補間
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	Vec::Lerp(VEC4_CLASS_HEAD& _ret, const VEC4_CLASS_HEAD& _vA, const VEC4_CLASS_HEAD& _vB, const typename VEC4_CLASS_HEAD::FLOAT _t)
{
	_ret.x = Math::Lerp(_vA.x, _vB.x, _t);
	_ret.y = Math::Lerp(_vA.y, _vB.y, _t);
	_ret.z = Math::Lerp(_vA.z, _vB.z, _t);
	_ret.w = Math::Lerp(_vA.w, _vB.w, _t);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
// エルミート補間
VEC_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	Vec::Hermite(VEC4_CLASS_HEAD& _ret, const VEC4_CLASS_HEAD& _posA, const VEC4_CLASS_HEAD& _vecA, const VEC4_CLASS_HEAD& _posB, const VEC4_CLASS_HEAD& _vecB, const typename VEC4_CLASS_HEAD::FLOAT _t)
{
	_ret.x = Math::Hermite(_posA.x, _posB.x, _vecA.x, _vecB.x, _t);
	_ret.y = Math::Hermite(_posA.y, _posB.y, _vecA.y, _vecB.y, _t);
	_ret.z = Math::Hermite(_posA.z, _posB.z, _vecA.z, _vecB.z, _t);
	_ret.w = Math::Hermite(_posA.w, _posB.w, _vecA.w, _vecB.w, _t);
	return _ret;
}


POISON_END

#endif	// __VECTOR_FUNC_INL__

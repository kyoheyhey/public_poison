﻿//#pragma once
#ifndef __MATRIX_FUNC_INL__
#define __MATRIX_FUNC_INL__




POISON_BGN

//-------------------------------------------------------------------------------------------------
/// テンプレート宣言部分の省略
//#define MTX_TEMPLATE_HEAD		template< typename MTX >
#define VEC_MTX_TEMPLATE_HEAD	template< typename VEC, typename MTX >
//#define VEC2_CLASS_HEAD		TVec2< VEC >
//#define VEC3_CLASS_HEAD		TVec3< VEC >
//#define VEC4_CLASS_HEAD		TVec4< VEC >
//#define EULER_CLASS_HEAD		TEuler< VEC >
//#define QUAT_CLASS_HEAD		TQuat< VEC >
//#define MTX22_CLASS_HEAD		TMtx22< MTX >
//#define MTX23_CLASS_HEAD		TMtx23< MTX >
//#define MTX33_CLASS_HEAD		TMtx33< MTX >
//#define MTX34_CLASS_HEAD		TMtx34< MTX >
//#define MTX44_CLASS_HEAD		TMtx44< MTX >


///*************************************************************************************************
/// ２×２行列関数群
///*************************************************************************************************
///-------------------------------------------------------------------------------------------------
/// 行列式
MTX_TEMPLATE_HEAD
typename MTX22_CLASS_HEAD::FLOAT	Mtx::Det(const MTX22_CLASS_HEAD& _mtx)
{
	return (_mtx[0][0]*_mtx[1][1]) - (_mtx[0][1]*_mtx[1][0]);
}

///-------------------------------------------------------------------------------------------------
/// 単位行列
MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD&		Mtx::Identity(MTX22_CLASS_HEAD& _mtx)
{
	_mtx[0][0] = _mtx[1][1] = 1.0f;
	_mtx[0][1] = _mtx[1][0] = 0.0f;
	return _mtx;
}

///-------------------------------------------------------------------------------------------------
/// 転置行列
MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD&	Mtx::Transpose(MTX22_CLASS_HEAD& _ret, const MTX22_CLASS_HEAD& _src)
{
	_ret[0][0] = _src[0][0], _ret[0][1] = _src[1][0];
	_ret[1][0] = _src[0][1], _ret[1][1] = _src[1][1];
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD&	Mtx::Transpose(MTX22_CLASS_HEAD& _ret)
{
	swap(_ret[0][1], _ret[1][0]);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 正規化行列
MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD&	Mtx::Normalize(MTX22_CLASS_HEAD& _ret, const MTX22_CLASS_HEAD& _src)
{
	MTX22_CLASS_HEAD temp;
	_src.GetAxis(temp.v[0], temp.v[1]);
	Vec::Normalize(temp.v[0], temp.v[0]);
	Vec::Normalize(temp.v[1], temp.v[1]);
	Mtx::Transpose(_ret, temp);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 逆行列
MTX_TEMPLATE_HEAD
b8		Mtx::Inverse(MTX22_CLASS_HEAD& _ret, const MTX22_CLASS_HEAD& _src)
{
	MTX22_CLASS_HEAD::FLOAT val = Mtx::Det(_src);
	if (val)
	{
		_ret[0][0] = _src[1][1], _ret[0][1] = -_src[0][1];
		_ret[1][0] = -_src[1][0], _ret[1][1] = _src[0][0];
		_ret /= val;
		return true;
	}
	return false;
}

///-------------------------------------------------------------------------------------------------
// 拡縮行列
MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD&	Mtx::Scale(MTX22_CLASS_HEAD& _ret, const typename MTX22_CLASS_HEAD::FLOAT _x, const typename MTX22_CLASS_HEAD::FLOAT _y)
{
	_ret[0][0] = _x;
	_ret[1][1] = _y;
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD&	Mtx::Scale(MTX22_CLASS_HEAD& _ret, const typename MTX22_CLASS_HEAD::FLOAT _xy)
{
	return Scale(_ret, _xy, _xy);
}
MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD&	Mtx::Scale(MTX22_CLASS_HEAD& _ret, const typename MTX22_CLASS_HEAD::FLOAT* _xy)
{
	return Scale(_ret, _xy[0], _xy[1]);
}
VEC_MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD&	Mtx::Scale(MTX22_CLASS_HEAD& _ret, const VEC2_CLASS_HEAD& _xy)
{
	return Scale(_ret, _xy[0], _xy[1]);
}

///-------------------------------------------------------------------------------------------------
/// 回転行列
MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD&	Mtx::Rotate(MTX22_CLASS_HEAD& _ret, const typename MTX22_CLASS_HEAD::FLOAT _sin, const typename MTX22_CLASS_HEAD::FLOAT _cos)
{
	_ret[0][0] = _cos, _ret[0][1] = -_sin;
	_ret[1][0] = _sin, _ret[1][1] = _cos;
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD&	Mtx::Rotate(MTX22_CLASS_HEAD& _ret, const typename MTX22_CLASS_HEAD::FLOAT _angle)
{
	return Rotate(_ret, sinf(_angle), cosf(_angle));
}

///-------------------------------------------------------------------------------------------------
/// 行列の乗算
MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD&	Mtx::Mul(MTX22_CLASS_HEAD& _ret, const MTX22_CLASS_HEAD& _mtxA, const MTX22_CLASS_HEAD& _mtxB)
{
	_ret[0][0] = (_mtxA[0][0] * _mtxB[0][0]) + (_mtxA[0][1] * _mtxB[1][0]);
	_ret[0][1] = (_mtxA[0][0] * _mtxB[0][1]) + (_mtxA[0][1] * _mtxB[1][1]);
	_ret[1][0] = (_mtxA[1][0] * _mtxB[0][0]) + (_mtxA[1][1] * _mtxB[1][0]);
	_ret[1][1] = (_mtxA[1][0] * _mtxB[0][1]) + (_mtxA[1][1] * _mtxB[1][1]);
	return _ret;
}



///*************************************************************************************************
/// ２×３行列関数群
///*************************************************************************************************
///-------------------------------------------------------------------------------------------------
/// 行列式
MTX_TEMPLATE_HEAD
typename MTX23_CLASS_HEAD::FLOAT	Mtx::Det(const MTX23_CLASS_HEAD& _mtx)
{
	MTX22_CLASS_HEAD mtx0(_mtx[1][1], _mtx[1][2], 0.0f, 1.0f);
	MTX22_CLASS_HEAD mtx1(_mtx[1][0], _mtx[1][2], 0.0f, 1.0f);
	MTX22_CLASS_HEAD mtx2(_mtx[1][0], _mtx[1][1], 0.0f, 0.0f);
	return (_mtx[0][0] * Det(mtx0)) - (_mtx[0][1] * Det(mtx1)) + (_mtx[0][2] * Det(mtx2));
}

///-------------------------------------------------------------------------------------------------
/// 単位行列
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&		Mtx::Identity(MTX23_CLASS_HEAD& _mtx)
{
	_mtx[0][0] = _mtx[1][1] = 1.0f;
	_mtx[0][1] = _mtx[0][2] = _mtx[1][0] = _mtx[1][2] = 0.0f;
	return _mtx;
}

///-------------------------------------------------------------------------------------------------
/// 転置行列
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	Mtx::Transpose(MTX23_CLASS_HEAD& _ret, const MTX23_CLASS_HEAD& _src)
{
	_ret[0][0] = _src[0][0], _ret[0][1] = _src[1][0], _ret[0][2] = 0.0f;
	_ret[1][0] = _src[0][1], _ret[1][1] = _src[1][1], _ret[1][2] = 0.0f;
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	Mtx::Transpose(MTX23_CLASS_HEAD& _ret)
{
	swap(_ret[0][1], _ret[1][0]);
	_ret[0][2] = 0.0f;
	_ret[1][2] = 0.0f;
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 逆行列
MTX_TEMPLATE_HEAD
b8		Mtx::Inverse(MTX23_CLASS_HEAD& _ret, const MTX23_CLASS_HEAD& _src)
{
	MTX23_CLASS_HEAD::FLOAT _val = Mtx::Det(_src);
	if (_val) {
		_ret[0][0] = (_src[1][1] * _src[2][2]);
		_ret[0][1] = (_src[0][2] * _src[2][1]) - (_src[0][1]);
		_ret[0][2] = (_src[0][1] * _src[1][2]) - (_src[0][2] * _src[1][1]);

		_ret[1][0] = (_src[1][2] * _src[2][0]) - (_src[1][0]);
		_ret[1][1] = (_src[0][0] * _src[2][2]);
		_ret[1][2] = (_src[0][2] * _src[1][0]) - (_src[0][0] * _src[1][2]);
		_ret /= _val;
		return true;
	}
	return false;
}

///-------------------------------------------------------------------------------------------------
// 拡縮行列
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	Mtx::Scale(MTX23_CLASS_HEAD& _ret, const typename MTX23_CLASS_HEAD::FLOAT _x, const typename MTX23_CLASS_HEAD::FLOAT _y)
{
	Scale(_ret.GetMtx22(), _x, _y);
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	Mtx::Scale(MTX23_CLASS_HEAD& _ret, const typename MTX23_CLASS_HEAD::FLOAT _xy)
{
	Scale(_ret.GetMtx22(), _xy, _xy);
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	Mtx::Scale(MTX23_CLASS_HEAD& _ret, const typename MTX23_CLASS_HEAD::FLOAT* _xy)
{
	Scale(_ret.GetMtx22(), _xy[0], _xy[1]);
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	Mtx::Scale(MTX23_CLASS_HEAD& _ret, const VEC2_CLASS_HEAD& _xy)
{
	Scale(_ret.GetMtx22(), _xy[0], _xy[1]);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 回転行列
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	Mtx::Rotate(MTX23_CLASS_HEAD& _ret, const typename MTX23_CLASS_HEAD::FLOAT _sin, const typename MTX23_CLASS_HEAD::FLOAT _cos)
{
	Rotate(_ret.GetMtx22(), _sin, _cos);
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	Mtx::Rotate(MTX23_CLASS_HEAD& _ret, const typename MTX23_CLASS_HEAD::FLOAT _angle)
{
	Rotate(_ret.GetMtx22(), _angle);
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	Mtx::Rotate(MTX23_CLASS_HEAD& _ret, const VEC2_CLASS_HEAD& _vecA, const VEC2_CLASS_HEAD& _vecB)
{
	Mtx::Rotate(_ret, Vec::Sin(_vecA, _vecB), Vec::Cos(_vecA, _vecB));
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 平行行列
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	Mtx::Trans(MTX23_CLASS_HEAD& _ret, const typename MTX23_CLASS_HEAD::FLOAT _x, const typename MTX23_CLASS_HEAD::FLOAT _y)
{
	_ret[0][2] = _x;
	_ret[1][2] = _y;
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	Mtx::Trans(MTX23_CLASS_HEAD& _ret, const typename MTX23_CLASS_HEAD::FLOAT* _xy)
{
	return Trans(_ret, _xy[0], _xy[1]);
}
VEC_MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	Mtx::Trans(MTX23_CLASS_HEAD& _ret, const VEC2_CLASS_HEAD& _xy)
{
	return Trans(_ret, _xy[0], _xy[1]);
}

///-------------------------------------------------------------------------------------------------
/// 行列の乗算
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	Mtx::Mul(MTX23_CLASS_HEAD& _ret, const MTX23_CLASS_HEAD& _mtxA, const MTX23_CLASS_HEAD& _mtxB)
{
	_ret[0][0] = (_mtxA[0][0] * _mtxB[0][0]) + (_mtxA[0][1] * _mtxB[1][0]);
	_ret[0][1] = (_mtxA[0][0] * _mtxB[0][1]) + (_mtxA[0][1] * _mtxB[1][1]);
	_ret[0][2] = (_mtxA[0][0] * _mtxB[0][2]) + (_mtxA[0][1] * _mtxB[1][2]) + (_mtxA[0][2]);

	_ret[1][0] = (_mtxA[1][0] * _mtxB[0][0]) + (_mtxA[1][1] * _mtxB[1][0]);
	_ret[1][1] = (_mtxA[1][0] * _mtxB[0][1]) + (_mtxA[1][1] * _mtxB[1][1]);
	_ret[1][2] = (_mtxA[1][0] * _mtxB[0][2]) + (_mtxA[1][1] * _mtxB[1][2]) + (_mtxA[1][2]);
	return _ret;
}



///*************************************************************************************************
/// ３×３行列関数群
///*************************************************************************************************
///-------------------------------------------------------------------------------------------------
/// 行列式
MTX_TEMPLATE_HEAD
typename MTX33_CLASS_HEAD::FLOAT		Mtx::Det(const MTX33_CLASS_HEAD& _mtx)
{
	MTX22_CLASS_HEAD mtx0(_mtx[1][1], _mtx[1][2], _mtx[2][1], _mtx[2][2]);
	MTX22_CLASS_HEAD mtx1(_mtx[1][0], _mtx[1][2], _mtx[2][0], _mtx[2][2]);
	MTX22_CLASS_HEAD mtx2(_mtx[1][0], _mtx[1][1], _mtx[2][0], _mtx[2][1]);
	return (_mtx[0][0] * Det(mtx0)) - (_mtx[0][1] * Det(mtx1)) + (_mtx[0][2] * Det(mtx2));
}

///-------------------------------------------------------------------------------------------------
/// 単位行列
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&		Mtx::Identity(MTX33_CLASS_HEAD& _mtx)
{
	_mtx[0][0] = _mtx[1][1] = _mtx[2][2] = 1.0f;
	_mtx[0][1] = _mtx[0][2] = _mtx[1][0] = _mtx[1][2] = _mtx[2][0] = _mtx[2][1] = 0.0f;
	return _mtx;
}

///-------------------------------------------------------------------------------------------------
/// 転置行列
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::Transpose(MTX33_CLASS_HEAD& _ret, const MTX33_CLASS_HEAD& _src)
{
	_ret[0][0] = _src[0][0], _ret[0][1] = _src[1][0], _ret[0][2] = _src[2][0];
	_ret[1][0] = _src[0][1], _ret[1][1] = _src[1][1], _ret[1][2] = _src[2][1];
	_ret[2][0] = _src[0][2], _ret[2][1] = _src[1][2], _ret[2][2] = _src[2][2];
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::Transpose(MTX33_CLASS_HEAD& _ret)
{
	swap(_ret[0][1], _ret[1][0]);
	swap(_ret[0][2], _ret[2][0]);
	swap(_ret[1][2], _ret[2][1]);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 正規化行列
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::Normalize(MTX33_CLASS_HEAD& _ret, const MTX33_CLASS_HEAD& _src)
{
	MTX33_CLASS_HEAD temp;
	_src.GetAxis(temp.v[0], temp.v[1], temp.v[2]);
	Vec::Normalize(temp.v[0], temp.v[0]);
	Vec::Normalize(temp.v[1], temp.v[1]);
	Vec::Normalize(temp.v[2], temp.v[2]);
	Mtx::Transpose(_ret, temp);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 逆行列
MTX_TEMPLATE_HEAD
b8		Mtx::Inverse(MTX33_CLASS_HEAD& _ret, const MTX33_CLASS_HEAD& _src)
{
	MTX33_CLASS_HEAD::FLOAT _val = Det(_src);
	if (_val) {
		_ret[0][0] = (_src[1][1] * _src[2][2]) - (_src[1][2] * _src[2][1]);
		_ret[0][1] = (_src[0][2] * _src[2][1]) - (_src[0][1] * _src[2][2]);
		_ret[0][2] = (_src[0][1] * _src[1][2]) - (_src[0][2] * _src[1][1]);

		_ret[1][0] = (_src[1][2] * _src[2][0]) - (_src[1][0] * _src[2][2]);
		_ret[1][1] = (_src[0][0] * _src[2][2]) - (_src[0][2] * _src[2][0]);
		_ret[1][2] = (_src[0][2] * _src[1][0]) - (_src[0][0] * _src[1][2]);

		_ret[2][0] = (_src[1][0] * _src[2][1]) - (_src[1][1] * _src[2][0]);
		_ret[2][1] = (_src[0][1] * _src[2][0]) - (_src[0][0] * _src[2][1]);
		_ret[2][2] = (_src[0][0] * _src[1][1]) - (_src[0][1] * _src[1][0]);
		_ret /= _val;
		return true;
	}
	return false;
}

///-------------------------------------------------------------------------------------------------
/// 拡縮行列
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::Scale(MTX33_CLASS_HEAD& _ret, const typename MTX33_CLASS_HEAD::FLOAT _x, const typename MTX33_CLASS_HEAD::FLOAT _y, const typename MTX33_CLASS_HEAD::FLOAT _z)
{
	_ret[0][0] = _x;
	_ret[1][1] = _y;
	_ret[2][2] = _z;
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::Scale(MTX33_CLASS_HEAD& _ret, const typename MTX33_CLASS_HEAD::FLOAT _xyz)
{
	return Scale(_ret, _xyz, _xyz, _xyz);
}
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::Scale(MTX33_CLASS_HEAD& _ret, const typename MTX33_CLASS_HEAD::FLOAT* _xyz)
{
	return Scale(_ret, _xyz[0], _xyz[1], _xyz[2]);
}
VEC_MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::Scale(MTX33_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _xyz)
{
	return Scale(_ret, _xyz[0], _xyz[1], _xyz[2]);
}

///-------------------------------------------------------------------------------------------------
/// Ｘ軸回転行列
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateX(MTX33_CLASS_HEAD& _ret, const typename MTX33_CLASS_HEAD::FLOAT _sin, const typename MTX33_CLASS_HEAD::FLOAT _cos)
{
	_ret[1][1] = _cos, _ret[1][2] = -_sin;
	_ret[2][1] = _sin, _ret[2][2] = _cos;
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateX(MTX33_CLASS_HEAD& _ret, const typename MTX33_CLASS_HEAD::FLOAT _angle)
{
	return RotateX(_ret, sinf(_angle), cosf(_angle));
}

///-------------------------------------------------------------------------------------------------
/// Ｙ軸回転行列
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateY(MTX33_CLASS_HEAD& _ret, const typename MTX33_CLASS_HEAD::FLOAT _sin, const typename MTX33_CLASS_HEAD::FLOAT _cos)
{
	_ret[0][0] = _cos, _ret[0][2] = _sin;
	_ret[2][0] = -_sin, _ret[2][2] = _cos;
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateY(MTX33_CLASS_HEAD& _ret, const typename MTX33_CLASS_HEAD::FLOAT _angle)
{
	return RotateY(_ret, sinf(_angle), cosf(_angle));
}

///-------------------------------------------------------------------------------------------------
/// Ｚ軸回転行列
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateZ(MTX33_CLASS_HEAD& _ret, const typename MTX33_CLASS_HEAD::FLOAT _sin, const typename MTX33_CLASS_HEAD::FLOAT _cos)
{
	_ret[0][0] = _cos, _ret[0][1] = -_sin;
	_ret[1][0] = _sin, _ret[1][1] = _cos;
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateZ(MTX33_CLASS_HEAD& _ret, const typename MTX33_CLASS_HEAD::FLOAT _angle)
{
	return RotateZ(_ret, sinf(_angle), cosf(_angle));
}

///-------------------------------------------------------------------------------------------------
/// XYZ回転
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateXYZ(MTX33_CLASS_HEAD& _ret, const typename MTX33_CLASS_HEAD::FLOAT _x, const typename MTX33_CLASS_HEAD::FLOAT _y, const typename MTX33_CLASS_HEAD::FLOAT _z)
{
	RotateZ(_ret, _z);
	MTX33_CLASS_HEAD temp;
	RotateY(temp, _y);
	_ret *= temp;
	temp = MTX33_CLASS_HEAD::Identity;
	RotateX(temp, _x);
	_ret *= temp;
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateXYZ(MTX33_CLASS_HEAD& _ret, const typename MTX33_CLASS_HEAD::FLOAT* _xyz)
{
	return RotateXYZ(_ret, _xyz[0], _xyz[1], _xyz[2]);
}
VEC_MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateXYZ(MTX33_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _xyz)
{
	return RotateXYZ(_ret, _xyz[0], _xyz[1], _xyz[2]);
}

///-------------------------------------------------------------------------------------------------
/// XZY回転
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateXZY(MTX33_CLASS_HEAD& _ret, const typename MTX33_CLASS_HEAD::FLOAT _x, const typename MTX33_CLASS_HEAD::FLOAT _y, const typename MTX33_CLASS_HEAD::FLOAT _z)
{
	RotateY(_ret, _y);
	MTX33_CLASS_HEAD temp;
	RotateZ(temp, _z);
	_ret *= temp;
	temp = MTX33_CLASS_HEAD::Identity;
	RotateX(temp, _x);
	_ret *= temp;
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateXZY(MTX33_CLASS_HEAD& _ret, const typename MTX33_CLASS_HEAD::FLOAT* _xyz)
{
	return RotateXZY(_ret, _xyz[0], _xyz[1], _xyz[2]);
}
VEC_MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateXZY(MTX33_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _xyz)
{
	return RotateXZY(_ret, _xyz[0], _xyz[1], _xyz[2]);
}

///-------------------------------------------------------------------------------------------------
/// YXZ回転
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateYXZ(MTX33_CLASS_HEAD& _ret, const typename MTX33_CLASS_HEAD::FLOAT _x, const typename MTX33_CLASS_HEAD::FLOAT _y, const typename MTX33_CLASS_HEAD::FLOAT _z)
{
	RotateZ(_ret, _z);
	MTX33_CLASS_HEAD temp;
	RotateX(temp, _x);
	_ret *= temp;
	temp = MTX33_CLASS_HEAD::Identity;
	RotateY(temp, _y);
	_ret *= temp;
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateYXZ(MTX33_CLASS_HEAD& _ret, const typename MTX33_CLASS_HEAD::FLOAT* _xyz)
{
	return RotateYXZ(_ret, _xyz[0], _xyz[1], _xyz[2]);
}
VEC_MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateYXZ(MTX33_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _xyz)
{
	return RotateYXZ(_ret, _xyz[0], _xyz[1], _xyz[2]);
}

///-------------------------------------------------------------------------------------------------
/// YZX回転
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateYZX(MTX33_CLASS_HEAD& _ret, const typename MTX33_CLASS_HEAD::FLOAT _x, const typename MTX33_CLASS_HEAD::FLOAT _y, const typename MTX33_CLASS_HEAD::FLOAT _z)
{
	RotateX(_ret, _x);
	MTX33_CLASS_HEAD temp;
	RotateZ(temp, _z);
	_ret *= temp;
	temp = MTX33_CLASS_HEAD::Identity;
	RotateY(temp, _y);
	_ret *= temp;
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateYZX(MTX33_CLASS_HEAD& _ret, const typename MTX33_CLASS_HEAD::FLOAT* _xyz)
{
	return RotateYZX(_ret, _xyz[0], _xyz[1], _xyz[2]);
}
VEC_MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateYZX(MTX33_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _xyz)
{
	return RotateYZX(_ret, _xyz[0], _xyz[1], _xyz[2]);
}

///-------------------------------------------------------------------------------------------------
/// ZXY回転
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateZXY(MTX33_CLASS_HEAD& _ret, const typename MTX33_CLASS_HEAD::FLOAT _x, const typename MTX33_CLASS_HEAD::FLOAT _y, const typename MTX33_CLASS_HEAD::FLOAT _z)
{
	RotateY(_ret, _y);
	MTX33_CLASS_HEAD temp;
	RotateX(temp, _x);
	_ret *= temp;
	temp = MTX33_CLASS_HEAD::Identity;
	RotateZ(temp, _z);
	_ret *= temp;
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateZXY(MTX33_CLASS_HEAD& _ret, const typename MTX33_CLASS_HEAD::FLOAT* _xyz)
{
	return RotateZXY(_ret, _xyz[0], _xyz[1], _xyz[2]);
}
VEC_MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateZXY(MTX33_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _xyz)
{
	return RotateZXY(_ret, _xyz[0], _xyz[1], _xyz[2]);
}

///-------------------------------------------------------------------------------------------------
/// ZYX回転
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateZYX(MTX33_CLASS_HEAD& _ret, const typename MTX33_CLASS_HEAD::FLOAT _x, const typename MTX33_CLASS_HEAD::FLOAT _y, const typename MTX33_CLASS_HEAD::FLOAT _z)
{
	RotateX(_ret, _x);
	MTX33_CLASS_HEAD temp;
	RotateY(temp, _y);
	_ret *= temp;
	temp = MTX33_CLASS_HEAD::Identity;
	RotateZ(temp, _z);
	_ret *= temp;
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateZYX(MTX33_CLASS_HEAD& _ret, const typename MTX33_CLASS_HEAD::FLOAT* _xyz)
{
	return RotateZYX(_ret, _xyz[0], _xyz[1], _xyz[2]);
}
VEC_MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::RotateZYX(MTX33_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _xyz)
{
	return RotateZYX(_ret, _xyz[0], _xyz[1], _xyz[2]);
}

///-------------------------------------------------------------------------------------------------
/// 任意軸回転行列
VEC_MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::Rotate(MTX33_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _vec, const typename MTX33_CLASS_HEAD::FLOAT _sin, const typename MTX33_CLASS_HEAD::FLOAT _cos)
{
	MTX33_CLASS_HEAD::FLOAT t = 1.0f - _cos;
	_ret[0][0] = (_vec.x*_vec.x)*t + _cos,			_ret[0][1] = (_vec.x*_vec.y)*t - (_vec.z)*_sin,	_ret[0][2] = (_vec.z*_vec.x)*t + (_vec.y)*_sin;
	_ret[1][0] = (_vec.x*_vec.y)*t + (_vec.z)*_sin,	_ret[1][1] = (_vec.y*_vec.y)*t + _cos,			_ret[1][2] = (_vec.y*_vec.z)*t - (_vec.x)*_sin;
	_ret[2][0] = (_vec.z*_vec.x)*t - (_vec.y)*_sin,	_ret[2][1] = (_vec.y*_vec.z)*t + (_vec.x)*_sin,	_ret[2][2] = (_vec.z*_vec.z)*t + _cos;
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::Rotate(MTX33_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _vec, const typename MTX33_CLASS_HEAD::FLOAT _angle)
{
	return Rotate(_ret, _vec, sinf(_angle), cosf(_angle));
}

///-------------------------------------------------------------------------------------------------
/// 回転行列
VEC_MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::Rotate(MTX33_CLASS_HEAD& _ret, const EULER_CLASS_HEAD& _euler)
{
	typedef MTX33_CLASS_HEAD& (*ROT_FUNC)(MTX33_CLASS_HEAD&, const TVec3< typename VEC::VEC3 >&);
	static const ROT_FUNC s_rotFunc[] =
	{
		&RotateXYZ,
		&RotateXZY,
		&RotateYXZ,
		&RotateYZX,
		&RotateZXY,
		&RotateZYX,
	};
	StaticAssert(ARRAYOF(s_rotFunc) == EULER_CLASS_HEAD::ROT_TYPE_NUM);
	libAssert(_euler.GetRotType() < EULER_CLASS_HEAD::ROT_TYPE_NUM);
	return s_rotFunc[_euler.GetRotType()](_ret, _euler.GetXYZ());
}

///-------------------------------------------------------------------------------------------------
/// クォータニオンから行列に変換
VEC_MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::Rotate(MTX33_CLASS_HEAD& _ret, const QUAT_CLASS_HEAD& _quat)
{
	MTX33_CLASS_HEAD temp;
	temp[0][0] = _quat.x * _quat.x;
	temp[0][1] = _quat.y * _quat.y;
	temp[0][2] = _quat.z * _quat.z;

	temp[1][0] = _quat.y * _quat.z;
	temp[1][1] = _quat.x * _quat.z;
	temp[1][2] = _quat.x * _quat.y;

	temp[2][0] = _quat.w * _quat.x;
	temp[2][1] = _quat.w * _quat.y;
	temp[2][2] = _quat.w * _quat.z;

	_ret[0][0] = 1.0f - 2.0f * (temp[0][1] + temp[0][2]);
	_ret[0][1] = 2.0f * (temp[1][2] + temp[2][2]);
	_ret[0][2] = 2.0f * (temp[1][1] - temp[2][1]);

	_ret[1][0] = 2.0f * (temp[1][2] - temp[2][2]);
	_ret[1][1] = 1.0f - 2.0f * (temp[0][0] + temp[0][2]);
	_ret[1][2] = 2.0f * (temp[1][0] + temp[2][0]);

	_ret[2][0] = 2.0f * (temp[1][1] + temp[2][1]);
	_ret[2][1] = 2.0f * (temp[1][0] - temp[2][0]);
	_ret[2][2] = 1.0f - 2.0f * (temp[0][0] + temp[0][1]);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 行列の乗算
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	Mtx::Mul(MTX33_CLASS_HEAD& _ret, const MTX33_CLASS_HEAD& _mtxA, const MTX33_CLASS_HEAD& _mtxB)
{
	_ret[0][0] = (_mtxA[0][0] * _mtxB[0][0]) + (_mtxA[0][1] * _mtxB[1][0]) + (_mtxA[0][2] * _mtxB[2][0]);
	_ret[0][1] = (_mtxA[0][0] * _mtxB[0][1]) + (_mtxA[0][1] * _mtxB[1][1]) + (_mtxA[0][2] * _mtxB[2][1]);
	_ret[0][2] = (_mtxA[0][0] * _mtxB[0][2]) + (_mtxA[0][1] * _mtxB[1][2]) + (_mtxA[0][2] * _mtxB[2][2]);

	_ret[1][0] = (_mtxA[1][0] * _mtxB[0][0]) + (_mtxA[1][1] * _mtxB[1][0]) + (_mtxA[1][2] * _mtxB[2][0]);
	_ret[1][1] = (_mtxA[1][0] * _mtxB[0][1]) + (_mtxA[1][1] * _mtxB[1][1]) + (_mtxA[1][2] * _mtxB[2][1]);
	_ret[1][2] = (_mtxA[1][0] * _mtxB[0][2]) + (_mtxA[1][1] * _mtxB[1][2]) + (_mtxA[1][2] * _mtxB[2][2]);

	_ret[2][0] = (_mtxA[2][0] * _mtxB[0][0]) + (_mtxA[2][1] * _mtxB[1][0]) + (_mtxA[2][2] * _mtxB[2][0]);
	_ret[2][1] = (_mtxA[2][0] * _mtxB[0][1]) + (_mtxA[2][1] * _mtxB[1][1]) + (_mtxA[2][2] * _mtxB[2][1]);
	_ret[2][2] = (_mtxA[2][0] * _mtxB[0][2]) + (_mtxA[2][1] * _mtxB[1][2]) + (_mtxA[2][2] * _mtxB[2][2]);
	return _ret;
}



///*************************************************************************************************
/// ３×４行列関数群
///*************************************************************************************************
///-------------------------------------------------------------------------------------------------
/// 行列式
MTX_TEMPLATE_HEAD
typename MTX34_CLASS_HEAD::FLOAT	Mtx::Det(const MTX34_CLASS_HEAD& _mtx)
{
	MTX33_CLASS_HEAD	mtx0(_mtx[1][1], _mtx[1][2], _mtx[1][3], _mtx[2][1], _mtx[2][2], _mtx[2][3], 0.0f, 0.0f, 1.0f);
	MTX33_CLASS_HEAD	mtx1(_mtx[1][0], _mtx[1][2], _mtx[1][3], _mtx[2][0], _mtx[2][2], _mtx[2][3], 0.0f, 0.0f, 1.0f);
	MTX33_CLASS_HEAD	mtx2(_mtx[1][0], _mtx[1][1], _mtx[1][3], _mtx[2][0], _mtx[2][1], _mtx[2][3], 0.0f, 0.0f, 1.0f);
	MTX33_CLASS_HEAD	mtx3(_mtx[1][0], _mtx[1][1], _mtx[1][2], _mtx[2][0], _mtx[2][1], _mtx[2][2], 0.0f, 0.0f, 0.0f);
	return (_mtx[0][0] * Mtx::Det(mtx0)) - (_mtx[0][1] * Mtx::Det(mtx1)) + (_mtx[0][2] * Mtx::Det(mtx2)) - (_mtx[0][3] * Mtx::Det(mtx3));
}

///-------------------------------------------------------------------------------------------------
/// 単位行列
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&		Mtx::Identity(MTX34_CLASS_HEAD& _mtx)
{
	_mtx[0][0] = _mtx[1][1] = _mtx[2][2] = 1.0f;
	_mtx[0][3] = _mtx[1][3] = _mtx[2][3] = 0.0f;
	_mtx[0][1] = _mtx[0][2] = _mtx[1][0] = _mtx[1][2] = _mtx[2][0] = _mtx[2][1] = 0.0f;
	return _mtx;
}

///-------------------------------------------------------------------------------------------------
/// 転置行列
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::Transpose(MTX34_CLASS_HEAD& _ret, const MTX34_CLASS_HEAD& _src)
{
	_ret[0][0] = _src[0][0], _ret[0][1] = _src[1][0], _ret[0][2] = _src[2][0], _ret[0][3] = 0.0f;
	_ret[1][0] = _src[0][1], _ret[1][1] = _src[1][1], _ret[1][2] = _src[2][1], _ret[1][3] = 0.0f;
	_ret[2][0] = _src[0][2], _ret[2][1] = _src[1][2], _ret[2][2] = _src[2][2], _ret[2][3] = 0.0f;
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::Transpose(MTX34_CLASS_HEAD& _ret)
{
	swap(_ret[0][1], _ret[1][0]);
	swap(_ret[0][2], _ret[2][0]);
	swap(_ret[1][2], _ret[2][1]);
	_ret[0][3] = 0.0f;
	_ret[1][3] = 0.0f;
	_ret[2][3] = 0.0f;
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 逆行列
MTX_TEMPLATE_HEAD
b8		Mtx::Inverse(MTX34_CLASS_HEAD& _ret, const MTX34_CLASS_HEAD& _src)
{
	MTX34_CLASS_HEAD::FLOAT _val = Mtx::Det(_src);
	if (_val) {
		_ret[0][0] = (_src[1][1] * _src[2][2])
			- (_src[1][2] * _src[2][1]);
		_ret[0][1] = (_src[0][2] * _src[2][1])
			- (_src[0][1] * _src[2][2]);
		_ret[0][2] = (_src[0][1] * _src[1][2])
			- (_src[0][2] * _src[1][1]);
		_ret[0][3] = (_src[0][1] * _src[1][3] * _src[2][2]) + (_src[0][2] * _src[1][1] * _src[2][3]) + (_src[0][3] * _src[1][2] * _src[2][1])
			- (_src[0][1] * _src[1][2] * _src[2][3]) - (_src[0][2] * _src[1][3] * _src[2][1]) - (_src[0][3] * _src[1][1] * _src[2][2]);

		_ret[1][0] = (_src[1][2] * _src[2][0])
			- (_src[1][0] * _src[2][2]);
		_ret[1][1] = (_src[0][0] * _src[2][2])
			- (_src[0][2] * _src[2][0]);
		_ret[1][2] = (_src[0][2] * _src[1][0])
			- (_src[0][0] * _src[1][2]);
		_ret[1][3] = (_src[0][0] * _src[1][2] * _src[2][3]) + (_src[0][2] * _src[1][3] * _src[2][0]) + (_src[0][3] * _src[1][0] * _src[2][2])
			- (_src[0][0] * _src[1][3] * _src[2][2]) - (_src[0][2] * _src[1][0] * _src[2][3]) - (_src[0][3] * _src[1][2] * _src[2][0]);

		_ret[2][0] = (_src[1][0] * _src[2][1])
			- (_src[1][1] * _src[2][0]);
		_ret[2][1] = (_src[0][1] * _src[2][0])
			- (_src[0][0] * _src[2][1]);
		_ret[2][2] = (_src[0][0] * _src[1][1])
			- (_src[0][1] * _src[1][0]);
		_ret[2][3] = (_src[0][0] * _src[1][3] * _src[2][1]) + (_src[0][1] * _src[1][0] * _src[2][3]) + (_src[0][3] * _src[1][1] * _src[2][0])
			- (_src[0][0] * _src[1][1] * _src[2][3]) - (_src[0][1] * _src[1][3] * _src[2][0]) - (_src[0][3] * _src[1][0] * _src[1][2]);

		_ret /= _val;
		return true;
	}
	return false;
}

///-------------------------------------------------------------------------------------------------
// 拡縮行列
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::Scale(MTX34_CLASS_HEAD& _ret, const typename MTX34_CLASS_HEAD::FLOAT _x, const typename MTX34_CLASS_HEAD::FLOAT _y, const typename MTX34_CLASS_HEAD::FLOAT _z)
{
	Scale(_ret.GetMtx33(), _x, _y, _z);
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::Scale(MTX34_CLASS_HEAD& _ret, const typename MTX34_CLASS_HEAD::FLOAT _xyz)
{
	Scale(_ret.GetMtx33(), _xyz);
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::Scale(MTX34_CLASS_HEAD& _ret, const typename MTX34_CLASS_HEAD::FLOAT* _xyz)
{
	Scale(_ret.GetMtx33(), _xyz);
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::Scale(MTX34_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _xyz)
{
	Scale(_ret.GetMtx33(), _xyz);
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::Scale(MTX34_CLASS_HEAD& _ret, const VEC4_CLASS_HEAD& _xyzw)
{
	Scale(_ret.GetMtx33(), _xyzw.GetXYZ());
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// Ｘ軸回転行列
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::RotateX(MTX34_CLASS_HEAD& _ret, const typename MTX34_CLASS_HEAD::FLOAT _sin, const typename MTX34_CLASS_HEAD::FLOAT _cos)
{
	RotateX(_ret.GetMtx33(), _sin, _cos);
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::RotateX(MTX34_CLASS_HEAD& _ret, const typename MTX34_CLASS_HEAD::FLOAT _angle)
{
	RotateX(_ret.GetMtx33(), _angle);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// Ｙ軸回転行列
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::RotateY(MTX34_CLASS_HEAD& _ret, const typename MTX34_CLASS_HEAD::FLOAT _sin, const typename MTX34_CLASS_HEAD::FLOAT _cos)
{
	RotateY(_ret.GetMtx33(), _sin, _cos);
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::RotateY(MTX34_CLASS_HEAD& _ret, const typename MTX34_CLASS_HEAD::FLOAT _angle)
{
	RotateY(_ret.GetMtx33(), _angle);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// Ｚ軸回転行列
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::RotateZ(MTX34_CLASS_HEAD& _ret, const typename MTX34_CLASS_HEAD::FLOAT _sin, const typename MTX34_CLASS_HEAD::FLOAT _cos)
{
	RotateZ(_ret.GetMtx33(), _sin, _cos);
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::RotateZ(MTX34_CLASS_HEAD& _ret, const typename MTX34_CLASS_HEAD::FLOAT _angle)
{
	RotateZ(_ret.GetMtx33(), _angle);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 任意軸回転行列
VEC_MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::Rotate(MTX34_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _vec, const typename MTX34_CLASS_HEAD::FLOAT _sin, const typename MTX34_CLASS_HEAD::FLOAT _cos)
{
	Rotate(_ret.GetMtx33(), _vec, _sin, _cos);
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::Rotate(MTX34_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _vec, const typename MTX34_CLASS_HEAD::FLOAT _angle)
{
	Rotate(_ret.GetMtx33(), _vec, _angle);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 回転行列
VEC_MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::Rotate(MTX34_CLASS_HEAD& _ret, const EULER_CLASS_HEAD& _euler)
{
	Rotate(_ret.GetMtx33(), _euler);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// クォータニオンから行列に変換
VEC_MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::Rotate(MTX34_CLASS_HEAD& _ret, const QUAT_CLASS_HEAD& _quat)
{
	Rotate(_ret.GetMtx33(), _quat);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 平行移動行列
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::Trans(MTX34_CLASS_HEAD& _ret, const typename MTX34_CLASS_HEAD::FLOAT _x, const typename MTX34_CLASS_HEAD::FLOAT _y, const typename MTX34_CLASS_HEAD::FLOAT _z)
{
	_ret[0][3] = _x;
	_ret[1][3] = _y;
	_ret[2][3] = _z;
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::Trans(MTX34_CLASS_HEAD& _ret, const typename MTX34_CLASS_HEAD::FLOAT* _xyz)
{
	return Trans(_ret, _xyz[0], _xyz[1], _xyz[2]);
}
VEC_MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::Trans(MTX34_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _xyz)
{
	return Trans(_ret, _xyz[0], _xyz[1], _xyz[2]);
}
VEC_MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::Trans(MTX34_CLASS_HEAD& _ret, const VEC4_CLASS_HEAD& _xyzw)
{
	return Trans(_ret, _xyzw[0], _xyzw[1], _xyzw[2]);
}

///-------------------------------------------------------------------------------------------------
/// 行列の乗算
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::Mul(MTX34_CLASS_HEAD& _ret, const MTX34_CLASS_HEAD& _mtxA, const MTX34_CLASS_HEAD& _mtxB)
{
	_ret[0][0] = (_mtxA[0][0] * _mtxB[0][0]) + (_mtxA[0][1] * _mtxB[1][0]) + (_mtxA[0][2] * _mtxB[2][0]);
	_ret[0][1] = (_mtxA[0][0] * _mtxB[0][1]) + (_mtxA[0][1] * _mtxB[1][1]) + (_mtxA[0][2] * _mtxB[2][1]);
	_ret[0][2] = (_mtxA[0][0] * _mtxB[0][2]) + (_mtxA[0][1] * _mtxB[1][2]) + (_mtxA[0][2] * _mtxB[2][2]);
	_ret[0][3] = (_mtxA[0][0] * _mtxB[0][3]) + (_mtxA[0][1] * _mtxB[1][3]) + (_mtxA[0][2] * _mtxB[2][3]) + (_mtxA[0][3]);

	_ret[1][0] = (_mtxA[1][0] * _mtxB[0][0]) + (_mtxA[1][1] * _mtxB[1][0]) + (_mtxA[1][2] * _mtxB[2][0]);
	_ret[1][1] = (_mtxA[1][0] * _mtxB[0][1]) + (_mtxA[1][1] * _mtxB[1][1]) + (_mtxA[1][2] * _mtxB[2][1]);
	_ret[1][2] = (_mtxA[1][0] * _mtxB[0][2]) + (_mtxA[1][1] * _mtxB[1][2]) + (_mtxA[1][2] * _mtxB[2][2]);
	_ret[1][3] = (_mtxA[1][0] * _mtxB[0][3]) + (_mtxA[1][1] * _mtxB[1][3]) + (_mtxA[1][2] * _mtxB[2][3]) + (_mtxA[1][3]);

	_ret[2][0] = (_mtxA[2][0] * _mtxB[0][0]) + (_mtxA[2][1] * _mtxB[1][0]) + (_mtxA[2][2] * _mtxB[2][0]);
	_ret[2][1] = (_mtxA[2][0] * _mtxB[0][1]) + (_mtxA[2][1] * _mtxB[1][1]) + (_mtxA[2][2] * _mtxB[2][1]);
	_ret[2][2] = (_mtxA[2][0] * _mtxB[0][2]) + (_mtxA[2][1] * _mtxB[1][2]) + (_mtxA[2][2] * _mtxB[2][2]);
	_ret[2][3] = (_mtxA[2][0] * _mtxB[0][3]) + (_mtxA[2][1] * _mtxB[1][3]) + (_mtxA[2][2] * _mtxB[2][3]) + (_mtxA[2][3]);

	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 視線回転行列
VEC_MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::LookAt(MTX34_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _eye, const VEC3_CLASS_HEAD& _at, const VEC3_CLASS_HEAD& _up)
{
	VEC3_CLASS_HEAD vX = _eye;
	VEC3_CLASS_HEAD vY = _up;
	VEC3_CLASS_HEAD vZ = _at - vX;

	Vec::Cross(vX, vY, vZ);
	Vec::Cross(vY, vZ, vX);
	Vec::Normalize(vX, vX);
	Vec::Normalize(vY, vY);
	Vec::Normalize(vZ, vZ);

	_ret.SetAxis(vX, vY, vZ);
	_ret.SetTrans(VEC3_CLASS_HEAD(Vec::Dot(_eye, vX), Vec::Dot(_eye, vY), Vec::Dot(_eye, vZ)));
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// ビュー行列
VEC_MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	Mtx::LookAtView(MTX34_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _eye, const VEC3_CLASS_HEAD& _at, const VEC3_CLASS_HEAD& _up)
{
	VEC3_CLASS_HEAD vZ;
	Vec::Normalize(vZ, _eye - _at);
	VEC3_CLASS_HEAD vX;
	Vec::Cross(vX, _up, vZ);
	Vec::Normalize(vX, vX);
	VEC3_CLASS_HEAD vY;
	Vec::Cross(vY, vZ, vX);
	//Vec::Normalize(vY, vY); なしでOK

	_ret.v[0].Set(vX, -Vec::Dot(vX, _eye));
	_ret.v[1].Set(vY, -Vec::Dot(vY, _eye));
	_ret.v[2].Set(vZ, -Vec::Dot(vZ, _eye));
	_ret.v[3].Set(0.0f, 0.0f, 0.0f, 1.0f);
	return _ret;
}



///*************************************************************************************************
/// ４×４行列関数群
///*************************************************************************************************
///-------------------------------------------------------------------------------------------------
/// 行列式
MTX_TEMPLATE_HEAD
typename MTX44_CLASS_HEAD::FLOAT	Mtx::Det(const MTX44_CLASS_HEAD& _mtx)
{
	MTX33_CLASS_HEAD	mtx0(_mtx[1][1], _mtx[1][2], _mtx[1][3], _mtx[2][1], _mtx[2][2], _mtx[2][3], _mtx[3][1], _mtx[3][2], _mtx[3][3]);
	MTX33_CLASS_HEAD	mtx1(_mtx[1][0], _mtx[1][2], _mtx[1][3], _mtx[2][0], _mtx[2][2], _mtx[2][3], _mtx[3][0], _mtx[3][2], _mtx[3][3]);
	MTX33_CLASS_HEAD	mtx2(_mtx[1][0], _mtx[1][1], _mtx[1][3], _mtx[2][0], _mtx[2][1], _mtx[2][3], _mtx[3][0], _mtx[3][1], _mtx[3][3]);
	MTX33_CLASS_HEAD	mtx3(_mtx[1][0], _mtx[1][1], _mtx[1][2], _mtx[2][0], _mtx[2][1], _mtx[2][2], _mtx[3][0], _mtx[3][1], _mtx[3][2]);
	return (_mtx[0][0]*Mtx::Det(mtx0)) - (_mtx[0][1]*Mtx::Det(mtx1)) + (_mtx[0][2]*Mtx::Det(mtx2)) - (_mtx[0][3]*Mtx::Det(mtx3));
}

///-------------------------------------------------------------------------------------------------
/// 単位行列
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&		Mtx::Identity(MTX44_CLASS_HEAD& _mtx)
{
	_mtx[0][0] = _mtx[1][1] = _mtx[2][2] = _mtx[3][3] = 1.0f;
	_mtx[0][3] = _mtx[1][3] = _mtx[2][3] = 0.0f;
	_mtx[0][1] = _mtx[0][2] = _mtx[1][0] = _mtx[1][2] = _mtx[2][0] = _mtx[2][1] = _mtx[3][0] = _mtx[3][1] = _mtx[3][2] = 0.0f;
	return _mtx;
}

///-------------------------------------------------------------------------------------------------
/// 転置行列
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Transpose(MTX44_CLASS_HEAD& _ret, const MTX44_CLASS_HEAD& _src)
{
	_ret[0][0] = _src[0][0], _ret[0][1] = _src[1][0], _ret[0][2] = _src[2][0], _ret[0][3] = _src[3][0];
	_ret[1][0] = _src[0][1], _ret[1][1] = _src[1][1], _ret[1][2] = _src[2][1], _ret[1][3] = _src[3][1];
	_ret[2][0] = _src[0][2], _ret[2][1] = _src[1][2], _ret[2][2] = _src[2][2], _ret[2][3] = _src[3][2];
	_ret[3][0] = _src[0][3], _ret[3][1] = _src[1][3], _ret[2][3] = _src[3][2], _ret[3][3] = _src[3][3];
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Transpose(MTX44_CLASS_HEAD& _ret)
{
	swap(_ret[0][1], _ret[1][0]);
	swap(_ret[0][2], _ret[2][0]);
	swap(_ret[0][3], _ret[3][0]);
	swap(_ret[1][2], _ret[2][1]);
	swap(_ret[1][3], _ret[3][1]);
	swap(_ret[2][3], _ret[3][2]);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 正規化行列
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Normalize(MTX44_CLASS_HEAD& _ret, const MTX44_CLASS_HEAD& _src)
{
	MTX44_CLASS_HEAD temp;
	_src.GetAxis(temp.v[0], temp.v[1], temp.v[2], temp.v[3]);
	Vec::Normalize(temp.v[0], temp.v[0]);
	Vec::Normalize(temp.v[1], temp.v[1]);
	Vec::Normalize(temp.v[2], temp.v[2]);
	Vec::Normalize(temp.v[3], temp.v[3]);
	Mtx::Transpose(_ret, temp);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 逆行列
MTX_TEMPLATE_HEAD
b8		Mtx::Inverse(MTX44_CLASS_HEAD& _ret, const MTX44_CLASS_HEAD& _src)
{
	MTX44_CLASS_HEAD::FLOAT _val = Mtx::Det(_src);
	if (_val) {
		_ret[0][0] = (_src[1][1]*_src[2][2]*_src[3][3]) + (_src[1][2]*_src[2][3]*_src[3][1]) + (_src[1][3]*_src[2][1]*_src[3][2])
			- (_src[1][1]*_src[2][3]*_src[3][2]) - (_src[1][2]*_src[2][1]*_src[3][3]) - (_src[1][3]*_src[2][2]*_src[3][1]);
		_ret[0][1] = (_src[0][1]*_src[2][3]*_src[3][2]) + (_src[0][2]*_src[2][1]*_src[3][3]) + (_src[0][3]*_src[2][2]*_src[3][1])
			- (_src[0][1]*_src[2][2]*_src[3][3]) - (_src[0][2]*_src[2][3]*_src[3][1]) - (_src[0][3]*_src[2][1]*_src[3][2]);
		_ret[0][2] = (_src[0][1]*_src[1][2]*_src[3][3]) + (_src[0][2]*_src[2][3]*_src[3][1]) + (_src[1][3]*_src[1][1]*_src[3][2])
			- (_src[0][1]*_src[1][3]*_src[3][2]) - (_src[0][2]*_src[1][1]*_src[3][3]) - (_src[0][3]*_src[1][2]*_src[3][1]);
		_ret[0][3] = (_src[0][1]*_src[1][3]*_src[2][2]) + (_src[0][2]*_src[1][1]*_src[2][3]) + (_src[0][3]*_src[1][2]*_src[2][1])
			- (_src[0][1]*_src[1][2]*_src[2][3]) - (_src[0][2]*_src[1][3]*_src[2][1]) - (_src[0][3]*_src[1][1]*_src[2][2]);

		_ret[1][0] = (_src[1][0]*_src[2][3]*_src[3][2]) + (_src[1][2]*_src[2][0]*_src[3][3]) + (_src[1][3]*_src[2][2]*_src[3][0])
			- (_src[1][0]*_src[2][2]*_src[3][3]) - (_src[1][2]*_src[2][3]*_src[3][0]) - (_src[1][3]*_src[2][0]*_src[3][2]);
		_ret[1][1] = (_src[0][0]*_src[2][2]*_src[3][3]) + (_src[0][2]*_src[2][3]*_src[3][0]) + (_src[0][3]*_src[2][0]*_src[3][2])
			- (_src[0][0]*_src[2][3]*_src[3][2]) - (_src[0][2]*_src[2][0]*_src[3][3]) - (_src[0][3]*_src[2][2]*_src[3][0]);
		_ret[1][2] = (_src[0][0]*_src[1][3]*_src[3][2]) + (_src[0][2]*_src[1][0]*_src[3][3]) + (_src[0][3]*_src[1][2]*_src[3][0])
			- (_src[0][0]*_src[1][2]*_src[3][3]) - (_src[0][2]*_src[1][3]*_src[3][0]) - (_src[0][3]*_src[1][0]*_src[3][2]);
		_ret[1][3] = (_src[0][0]*_src[1][2]*_src[2][3]) + (_src[0][2]*_src[1][3]*_src[2][0]) + (_src[0][3]*_src[1][0]*_src[2][2])
			- (_src[0][0]*_src[1][3]*_src[2][2]) - (_src[0][2]*_src[1][0]*_src[2][3]) - (_src[0][3]*_src[1][2]*_src[2][0]);

		_ret[2][0] = (_src[1][0]*_src[2][1]*_src[3][3]) + (_src[1][1]*_src[2][3]*_src[3][0]) + (_src[1][3]*_src[2][0]*_src[3][1])
			- (_src[1][0]*_src[2][3]*_src[3][1]) - (_src[1][1]*_src[2][0]*_src[3][3]) - (_src[1][3]*_src[2][1]*_src[3][0]);
		_ret[2][1] = (_src[0][0]*_src[2][3]*_src[3][1]) + (_src[0][1]*_src[2][0]*_src[3][3]) + (_src[0][3]*_src[2][1]*_src[3][0])
			- (_src[0][0]*_src[2][1]*_src[3][3]) - (_src[0][1]*_src[2][3]*_src[3][0]) - (_src[0][3]*_src[2][0]*_src[3][1]);
		_ret[2][2] = (_src[0][0]*_src[1][1]*_src[3][3]) + (_src[0][1]*_src[1][3]*_src[3][0]) + (_src[0][3]*_src[1][0]*_src[3][1])
			- (_src[0][0]*_src[1][3]*_src[3][1]) - (_src[0][1]*_src[1][0]*_src[3][3]) - (_src[0][3]*_src[1][1]*_src[3][0]);
		_ret[2][3] = (_src[0][0]*_src[1][3]*_src[2][1]) + (_src[0][1]*_src[1][0]*_src[2][3]) + (_src[0][3]*_src[1][1]*_src[2][0])
			- (_src[0][0]*_src[1][1]*_src[2][3]) - (_src[0][1]*_src[1][3]*_src[2][0]) - (_src[0][3]*_src[1][0]*_src[1][2]);

		_ret[3][0] = (_src[1][0]*_src[2][2]*_src[3][1]) + (_src[1][1]*_src[2][0]*_src[3][2]) + (_src[1][2]*_src[2][1]*_src[3][0])
			- (_src[1][0]*_src[2][1]*_src[3][2]) - (_src[1][1]*_src[2][2]*_src[3][0]) - (_src[1][2]*_src[2][0]*_src[3][1]);
		_ret[3][1] = (_src[0][0]*_src[2][1]*_src[3][2]) + (_src[0][1]*_src[2][2]*_src[3][0]) + (_src[0][2]*_src[2][0]*_src[3][1])
			- (_src[0][0]*_src[2][2]*_src[3][1]) - (_src[0][1]*_src[2][0]*_src[3][2]) - (_src[0][2]*_src[2][1]*_src[3][0]);
		_ret[3][2] = (_src[0][0]*_src[1][2]*_src[3][1]) + (_src[0][1]*_src[1][0]*_src[3][2]) + (_src[0][2]*_src[1][1]*_src[3][0])
			- (_src[0][0]*_src[1][1]*_src[3][2]) - (_src[0][1]*_src[1][2]*_src[3][0]) - (_src[0][2]*_src[1][0]*_src[3][1]);
		_ret[3][3] = (_src[0][0]*_src[1][1]*_src[2][2]) + (_src[0][1]*_src[1][2]*_src[2][0]) + (_src[0][2]*_src[1][0]*_src[2][1])
			- (_src[0][0]*_src[1][2]*_src[2][1]) - (_src[0][1]*_src[1][0]*_src[2][2]) - (_src[0][2]*_src[1][1]*_src[3][0]);
		_ret /= _val;
		return true;
	}
	return false;
}

///-------------------------------------------------------------------------------------------------
// 拡縮行列
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Scale(MTX44_CLASS_HEAD& _ret, const typename MTX44_CLASS_HEAD::FLOAT _x, const typename MTX44_CLASS_HEAD::FLOAT _y, const typename MTX44_CLASS_HEAD::FLOAT _z)
{
	Scale(_ret.GetMtx33(), _x, _y, _z);
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Scale(MTX44_CLASS_HEAD& _ret, const typename MTX44_CLASS_HEAD::FLOAT _xyz)
{
	Scale(_ret.GetMtx33(), _xyz);
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Scale(MTX44_CLASS_HEAD& _ret, const typename MTX44_CLASS_HEAD::FLOAT* _xyz)
{
	Scale(_ret.GetMtx33(), _xyz);
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Scale(MTX44_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _xyz)
{
	Scale(_ret.GetMtx33(), _xyz);
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Scale(MTX44_CLASS_HEAD& _ret, const typename MTX44_CLASS_HEAD::FLOAT _x, const typename MTX44_CLASS_HEAD::FLOAT _y, const typename MTX44_CLASS_HEAD::FLOAT _z, const typename MTX44_CLASS_HEAD::FLOAT _w)
{
	_ret[0][0] = _x;
	_ret[1][1] = _y;
	_ret[2][2] = _z;
	_ret[3][3] = _w;
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Scale(MTX44_CLASS_HEAD& _ret, const VEC4_CLASS_HEAD& _xyzw)
{
	return Scale(_ret, _xyzw[0], _xyzw[1], _xyzw[2], _xyzw[3]);
}

///-------------------------------------------------------------------------------------------------
/// Ｘ軸回転行列
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::RotateX(MTX44_CLASS_HEAD& _ret, const typename MTX44_CLASS_HEAD::FLOAT _sin, const typename MTX44_CLASS_HEAD::FLOAT _cos)
{
	RotateX(_ret.GetMtx33(), _sin, _cos);
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::RotateX(MTX44_CLASS_HEAD& _ret, const typename MTX44_CLASS_HEAD::FLOAT _angle)
{
	RotateX(_ret.GetMtx33(), _angle);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// Ｙ軸回転行列
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::RotateY(MTX44_CLASS_HEAD& _ret, const typename MTX44_CLASS_HEAD::FLOAT _sin, const typename MTX44_CLASS_HEAD::FLOAT _cos)
{
	RotateY(_ret.GetMtx33(), _sin, _cos);
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::RotateY(MTX44_CLASS_HEAD& _ret, const typename MTX44_CLASS_HEAD::FLOAT _angle)
{
	RotateY(_ret.GetMtx33(), _angle);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// Ｚ軸回転行列
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::RotateZ(MTX44_CLASS_HEAD& _ret, const typename MTX44_CLASS_HEAD::FLOAT _sin, const typename MTX44_CLASS_HEAD::FLOAT _cos)
{
	RotateZ(_ret.GetMtx33(), _sin, _cos);
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::RotateZ(MTX44_CLASS_HEAD& _ret, const typename MTX44_CLASS_HEAD::FLOAT _angle)
{
	RotateZ(_ret.GetMtx33(), _angle);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 任意軸回転行列
VEC_MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Rotate(MTX44_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _vec, const typename MTX44_CLASS_HEAD::FLOAT _sin, const typename MTX44_CLASS_HEAD::FLOAT _cos)
{
	Rotate(_ret.GetMtx33(), _vec, _sin, _cos);
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Rotate(MTX44_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _vec, const typename MTX44_CLASS_HEAD::FLOAT _angle)
{
	Rotate(_ret.GetMtx33(), _vec, _angle);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 回転行列
VEC_MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Rotate(MTX44_CLASS_HEAD& _ret, const EULER_CLASS_HEAD& _euler)
{
	Rotate(_ret.GetMtx33(), _euler);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// クォータニオンから行列に変換
VEC_MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Rotate(MTX44_CLASS_HEAD& _ret, const QUAT_CLASS_HEAD& _quat)
{
	Rotate(_ret.GetMtx33(), _quat);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 平行移動行列
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Trans(MTX44_CLASS_HEAD& _ret, const typename MTX44_CLASS_HEAD::FLOAT _x, const typename MTX44_CLASS_HEAD::FLOAT _y, const typename MTX44_CLASS_HEAD::FLOAT _z)
{
	Trans(_ret.GetMtx34(), _x, _y, _z);
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Trans(MTX44_CLASS_HEAD& _ret, const typename MTX44_CLASS_HEAD::FLOAT* _xyz)
{
	Trans(_ret.GetMtx34(), _xyz);
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Trans(MTX44_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _xyz)
{
	Trans(_ret.GetMtx34(), _xyz);
	return _ret;
}
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Trans(MTX44_CLASS_HEAD& _ret, const typename MTX44_CLASS_HEAD::FLOAT _x, const typename MTX44_CLASS_HEAD::FLOAT _y, const typename MTX44_CLASS_HEAD::FLOAT _z, const typename MTX44_CLASS_HEAD::FLOAT _w)
{
	_ret[0][3] = _x;
	_ret[1][3] = _y;
	_ret[2][3] = _z;
	_ret[3][3] = _w;
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Trans(MTX44_CLASS_HEAD& _ret, const VEC4_CLASS_HEAD& _xyzw)
{
	return Trans(_ret, _xyzw[0], _xyzw[1], _xyzw[2], _xyzw[3]);
}

///-------------------------------------------------------------------------------------------------
/// 行列の乗算
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Mul(MTX44_CLASS_HEAD& _ret, const MTX44_CLASS_HEAD& _mtxA, const MTX44_CLASS_HEAD& _mtxB)
{
	_ret[0][0] = (_mtxA[0][0] * _mtxB[0][0]) + (_mtxA[0][1] * _mtxB[1][0]) + (_mtxA[0][2] * _mtxB[2][0]) + (_mtxA[0][3] * _mtxB[3][0]);
	_ret[0][1] = (_mtxA[0][0] * _mtxB[0][1]) + (_mtxA[0][1] * _mtxB[1][1]) + (_mtxA[0][2] * _mtxB[2][1]) + (_mtxA[0][3] * _mtxB[3][1]);
	_ret[0][2] = (_mtxA[0][0] * _mtxB[0][2]) + (_mtxA[0][1] * _mtxB[1][2]) + (_mtxA[0][2] * _mtxB[2][2]) + (_mtxA[0][3] * _mtxB[3][2]);
	_ret[0][3] = (_mtxA[0][0] * _mtxB[0][3]) + (_mtxA[0][1] * _mtxB[1][3]) + (_mtxA[0][2] * _mtxB[2][3]) + (_mtxA[0][3] * _mtxB[3][3]);

	_ret[1][0] = (_mtxA[1][0] * _mtxB[0][0]) + (_mtxA[1][1] * _mtxB[1][0]) + (_mtxA[1][2] * _mtxB[2][0]) + (_mtxA[1][3] * _mtxB[3][0]);
	_ret[1][1] = (_mtxA[1][0] * _mtxB[0][1]) + (_mtxA[1][1] * _mtxB[1][1]) + (_mtxA[1][2] * _mtxB[2][1]) + (_mtxA[1][3] * _mtxB[3][1]);
	_ret[1][2] = (_mtxA[1][0] * _mtxB[0][2]) + (_mtxA[1][1] * _mtxB[1][2]) + (_mtxA[1][2] * _mtxB[2][2]) + (_mtxA[1][3] * _mtxB[3][2]);
	_ret[1][3] = (_mtxA[1][0] * _mtxB[0][3]) + (_mtxA[1][1] * _mtxB[1][3]) + (_mtxA[1][2] * _mtxB[2][3]) + (_mtxA[1][3] * _mtxB[3][3]);

	_ret[2][0] = (_mtxA[2][0] * _mtxB[0][0]) + (_mtxA[2][1] * _mtxB[1][0]) + (_mtxA[2][2] * _mtxB[2][0]) + (_mtxA[2][3] * _mtxB[3][0]);
	_ret[2][1] = (_mtxA[2][0] * _mtxB[0][1]) + (_mtxA[2][1] * _mtxB[1][1]) + (_mtxA[2][2] * _mtxB[2][1]) + (_mtxA[2][3] * _mtxB[3][1]);
	_ret[2][2] = (_mtxA[2][0] * _mtxB[0][2]) + (_mtxA[2][1] * _mtxB[1][2]) + (_mtxA[2][2] * _mtxB[2][2]) + (_mtxA[2][3] * _mtxB[3][2]);
	_ret[2][3] = (_mtxA[2][0] * _mtxB[0][3]) + (_mtxA[2][1] * _mtxB[1][3]) + (_mtxA[2][2] * _mtxB[2][3]) + (_mtxA[2][3] * _mtxB[3][3]);

	_ret[3][0] = (_mtxA[3][0] * _mtxB[0][0]) + (_mtxA[3][1] * _mtxB[1][0]) + (_mtxA[3][2] * _mtxB[2][0]) + (_mtxA[3][3] * _mtxB[3][0]);
	_ret[3][1] = (_mtxA[3][0] * _mtxB[0][1]) + (_mtxA[3][1] * _mtxB[1][1]) + (_mtxA[3][2] * _mtxB[2][1]) + (_mtxA[3][3] * _mtxB[3][1]);
	_ret[3][2] = (_mtxA[3][0] * _mtxB[0][2]) + (_mtxA[3][1] * _mtxB[1][2]) + (_mtxA[3][2] * _mtxB[2][2]) + (_mtxA[3][3] * _mtxB[3][2]);
	_ret[3][3] = (_mtxA[3][0] * _mtxB[0][3]) + (_mtxA[3][1] * _mtxB[1][3]) + (_mtxA[3][2] * _mtxB[2][3]) + (_mtxA[3][3] * _mtxB[3][3]);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 視線回転行列
VEC_MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::LookAt(MTX44_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _eye, const VEC3_CLASS_HEAD& _at, const VEC3_CLASS_HEAD& _up)
{
	LookAt(_ret.GetMtx34(), _eye, _at, _up);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// ビュー行列
VEC_MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::LookAtView(MTX44_CLASS_HEAD& _ret, const VEC3_CLASS_HEAD& _eye, const VEC3_CLASS_HEAD& _at, const VEC3_CLASS_HEAD& _up)
{
	LookAtView(_ret.GetMtx34(), _eye, _at, _up);
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// パースペクティブ行列
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Perspective(MTX44_CLASS_HEAD& _ret, const typename MTX44_CLASS_HEAD::FLOAT _left, const typename MTX44_CLASS_HEAD::FLOAT _right, const typename MTX44_CLASS_HEAD::FLOAT _bottom, const typename MTX44_CLASS_HEAD::FLOAT _top, const typename MTX44_CLASS_HEAD::FLOAT _near, const typename MTX44_CLASS_HEAD::FLOAT _far)
{
	const MTX44_CLASS_HEAD::FLOAT invW = 1.0f / (_right - _left);
	const MTX44_CLASS_HEAD::FLOAT invH = 1.0f / (_top - _bottom);
	const MTX44_CLASS_HEAD::FLOAT invZ = 1.0f / (_near - _far);
	memset(&_ret, 0, sizeof(_ret));

#if defined( OPENGL_PROJECTION )
	_ret[0][0] = 2.0f * _near * invW;
	_ret[1][1] = 2.0f * _near * invH;
	_ret[2][2] = (_far + _near) * invZ;
#if defined( MATRIX_COLUMN_MAJOR )			// 列優先
	_ret[0][2] = -(_right + _left) * invW;
	_ret[1][2] = -(_top + _bottom) * invH;
	_ret[3][2] = -1.0f;
	_ret[2][3] = 2.0f * _far * _near * invZ;
#elif defined( MATRIX_ROW_MAJOR )			// 行優先
	_ret[2][0] = -(_right + _left) * invW;
	_ret[2][1] = -(_top + _bottom) * invH;
	_ret[2][3] = -1.0f;
	_ret[3][2] = 2.0f * _far * _near * invZ;
#endif 

#elif defined( DIRECTX_PROJECTION )
	_ret[0][0] = 2.0f * _near * invW;
	_ret[1][1] = 2.0f * _near * invH;
	_ret[2][2] = _far * invZ;
#if defined( MATRIX_COLUMN_MAJOR )			// 列優先
	_ret[0][2] = (_left + _right) * invW;
	_ret[1][2] = (_top + _bottom) * invH;
	_ret[3][2] = -1.0f;
	_ret[2][3] = _near * _far * invZ;
#elif defined( MATRIX_ROW_MAJOR )			// 行優先
	_ret[2][0] = (_left + _right) * invW;
	_ret[2][1] = (_top + _bottom) * invH;
	_ret[2][3] = -1.0f;
	_ret[3][2] = _near * _far * invZ;
#endif 

	// Persepective
	// 2 * n / (r - l),	0,					l + r / (r - l),	0
	// 0,				2 * n / (t - b),	t + b / (t - b),	0
	// 0,				0,					f / (n - f),		n * f / (n - f)
	// 0,				0,					-1,					0

#endif
	return _ret;
}

MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Perspective(MTX44_CLASS_HEAD& _ret, const typename MTX44_CLASS_HEAD::FLOAT _fov, const typename MTX44_CLASS_HEAD::FLOAT _aspect, const typename MTX44_CLASS_HEAD::FLOAT _near, const typename MTX44_CLASS_HEAD::FLOAT _far)
{
	const MTX44_CLASS_HEAD::FLOAT h = 1.0f / tanf(_fov * 0.5f);
	const MTX44_CLASS_HEAD::FLOAT w = h / _aspect;
	const MTX44_CLASS_HEAD::FLOAT invZ = 1.0f / (_near - _far);
	memset(&_ret, 0, sizeof(_ret));

#if defined( OPENGL_PROJECTION )
	_ret[0][0] = w;
	_ret[1][1] = h;
	_ret[2][2] = (_far + _near) * invZ;
#if defined( MATRIX_COLUMN_MAJOR )			// 列優先
	_ret[3][2] = -1.0f;
	_ret[2][3] = 2.0f * _far * _near * invZ;
#elif defined( MATRIX_ROW_MAJOR )			// 行優先
	_ret[2][3] = -1.0f;
	_ret[3][2] = 2.0f * _far * _near * invZ;
#endif 

#elif defined( DIRECTX_PROJECTION )
	_ret[0][0] = w;
	_ret[1][1] = h;
	_ret[2][2] = _far * invZ;
#if defined( MATRIX_COLUMN_MAJOR )			// 列優先
	_ret[3][2] = -1.0f;
	_ret[2][3] = _near * _far * invZ;
#elif defined( MATRIX_ROW_MAJOR )			// 行優先
	_ret[2][3] = -1.0f;
	_ret[3][2] = _near * _far * invZ;
#endif 

	// Persepective
	// w,				0,					0,					0
	// 0,				h,					0,					0
	// 0,				0,					f / (n - f),		n * f / (n - f)
	// 0,				0,					-1,					0

#endif
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 正射影行列
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Ortho(MTX44_CLASS_HEAD& _ret, const typename MTX44_CLASS_HEAD::FLOAT _left, const typename MTX44_CLASS_HEAD::FLOAT _right, const typename MTX44_CLASS_HEAD::FLOAT _bottom, const typename MTX44_CLASS_HEAD::FLOAT _top, const typename MTX44_CLASS_HEAD::FLOAT _near, const typename MTX44_CLASS_HEAD::FLOAT _far)
{
	const MTX44_CLASS_HEAD::FLOAT invW = 1.0f / (_right - _left);
	const MTX44_CLASS_HEAD::FLOAT invH = 1.0f / (_top - _bottom);
	const MTX44_CLASS_HEAD::FLOAT invZ = 1.0f / (_near - _far);
	memset(&_ret, 0, sizeof(_ret));

#if defined( OPENGL_PROJECTION )
	_ret[0][0] = 2.0f * invW;
	_ret[1][1] = 2.0f * invH;
	_ret[2][2] = 2.0f * invZ;
	_ret[3][3] = 1.0f;

#if defined( MATRIX_COLUMN_MAJOR )			// 列優先
	_ret[0][3] = -(_left + _right) * invW;
	_ret[1][3] = -(_top + _bottom) * invH;
	_ret[2][3] = (_far + _near) * invZ;
#elif defined( MATRIX_ROW_MAJOR )			// 行優先
	_ret[3][0] = -(_left + _right) * invW;
	_ret[3][1] = -(_top + _bottom) * invH;
	_ret[3][2] = (_far + _near) * invZ;
#endif

#elif defined( DIRECTX_PROJECTION )
	_ret[0][0] = 2.0f * invW;
	_ret[1][1] = 2.0f * invH;
	_ret[2][2] = invZ;
	_ret[3][3] = 1.0f;
#if defined( MATRIX_COLUMN_MAJOR )			// 列優先
	_ret[0][3] = -(_left + _right) * invW;
	_ret[1][3] = -(_top + _bottom) * invH;
	_ret[2][3] = -_near * invZ;
#elif defined( MATRIX_ROW_MAJOR )			// 行優先
	_ret[3][0] = -(_left + _right) * invW;
	_ret[3][1] = -(_top + _bottom) * invH;
	_ret[3][2] = -_near * invZ;
#endif

	// Ortho
	// 2 / (r - l),		0,					0,					-(l + r) / (r - l)
	// 0,				2 / (t - b),		0,					-(t + b) / (t - b)
	// 0,				0,					1 / (n - f),		-n / (n - f)
	// 0,				0,					0,					1

#endif
	return _ret;
}

MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	Mtx::Ortho(MTX44_CLASS_HEAD& _ret, const typename MTX44_CLASS_HEAD::FLOAT _width, const typename MTX44_CLASS_HEAD::FLOAT _height, const typename MTX44_CLASS_HEAD::FLOAT _near, const typename MTX44_CLASS_HEAD::FLOAT _far)
{
	const MTX44_CLASS_HEAD::FLOAT invZ = 1.0f / (_near - _far);
	memset(&_ret, 0, sizeof(_ret));

#if defined( OPENGL_PROJECTION )
	_ret[0][0] = 2.0f / _width;
	_ret[1][1] = 2.0f / _height;
	_ret[2][2] = 2.0f * invZ;
	_ret[3][3] = 1.0f;
#if defined( MATRIX_COLUMN_MAJOR )			// 列優先
	_ret[2][3] = (_far + _near) * invZ;
#elif defined( MATRIX_ROW_MAJOR )			// 行優先
	_ret[3][2] = (_far + _near) * invZ;
#endif

#elif defined( DIRECTX_PROJECTION )
	_ret[0][0] = 2.0f / _width;
	_ret[1][1] = 2.0f / _height;
	_ret[2][2] = invZ;
	_ret[3][3] = 1.0f;
#if defined( MATRIX_COLUMN_MAJOR )			// 列優先
	_ret[2][3] = -_near * invZ;
#elif defined( MATRIX_ROW_MAJOR )			// 行優先
	_ret[3][2] = -_near * invZ;
#endif

	// Ortho
	// 2 / w,			0,					0,					0
	// 0,				2 / h,				0,					0
	// 0,				0,					1 / (n - f),		-n / (n - f)
	// 0,				0,					0,					1

#endif
	return _ret;
}



///*************************************************************************************************
/// ベクトル変換
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
// 行列×ベクトル
VEC_MTX_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	Mtx::Transform(VEC2_CLASS_HEAD& _ret, const MTX22_CLASS_HEAD& _mtx, const VEC2_CLASS_HEAD& _src)
{
	_ret.x = (_ret.x*_mtx[0][0]) + (_ret.y*_mtx[0][1]);
	_ret.y = (_ret.x*_mtx[1][0]) + (_ret.y*_mtx[1][1]);
	return _ret;
}

VEC_MTX_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	Mtx::Transform(VEC2_CLASS_HEAD& _ret, const MTX23_CLASS_HEAD& _mtx, const VEC2_CLASS_HEAD& _src)
{
	_ret.x = (_ret.x*_mtx[0][0]) + (_ret.y*_mtx[0][1]) + _mtx[0][2];
	_ret.y = (_ret.x*_mtx[1][0]) + (_ret.y*_mtx[1][1]) + _mtx[1][2];
	return _ret;
}

VEC_MTX_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	Mtx::Transform(VEC2_CLASS_HEAD& _ret, const MTX33_CLASS_HEAD& _mtx, const VEC2_CLASS_HEAD& _src)
{
	_ret.x = (_ret.x*_mtx[0][0]) + (_ret.y*_mtx[0][1]) + _mtx[0][2];
	_ret.y = (_ret.x*_mtx[1][0]) + (_ret.y*_mtx[1][1]) + _mtx[1][2];
	return _ret;
}

VEC_MTX_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	Mtx::Transform(VEC3_CLASS_HEAD& _ret, const MTX33_CLASS_HEAD& _mtx, const VEC3_CLASS_HEAD& _src)
{
	_ret.x = (_ret.x*_mtx[0][0]) + (_ret.y*_mtx[0][1]) + (_ret.z*_mtx[0][2]);
	_ret.y = (_ret.x*_mtx[1][0]) + (_ret.y*_mtx[1][1]) + (_ret.z*_mtx[1][2]);
	_ret.z = (_ret.x*_mtx[2][0]) + (_ret.y*_mtx[2][1]) + (_ret.z*_mtx[2][2]);
	return _ret;
}

VEC_MTX_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	Mtx::Transform(VEC4_CLASS_HEAD& _ret, const MTX33_CLASS_HEAD& _mtx, const VEC4_CLASS_HEAD& _src)
{
	Transform(_ret.GetXYZ(), _mtx, _src.GetXYZ());
	return _ret;
}

VEC_MTX_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	Mtx::Transform(VEC3_CLASS_HEAD& _ret, const MTX34_CLASS_HEAD& _mtx, const VEC3_CLASS_HEAD& _src)
{
	_ret.x = (_ret.x*_mtx[0][0]) + (_ret.y*_mtx[0][1]) + (_ret.z*_mtx[0][2]) + _mtx[0][3];
	_ret.y = (_ret.x*_mtx[1][0]) + (_ret.y*_mtx[1][1]) + (_ret.z*_mtx[1][2]) + _mtx[1][3];
	_ret.z = (_ret.x*_mtx[2][0]) + (_ret.y*_mtx[2][1]) + (_ret.z*_mtx[2][2]) + _mtx[2][3];
	return _ret;
}

VEC_MTX_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	Mtx::Transform(VEC4_CLASS_HEAD& _ret, const MTX34_CLASS_HEAD& _mtx, const VEC4_CLASS_HEAD& _src)
{
	Transform(_ret.GetXYZ(), _mtx, _src.GetXYZ());
	return _ret;
}

VEC_MTX_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	Mtx::Transform(VEC3_CLASS_HEAD& _ret, const MTX44_CLASS_HEAD& _mtx, const VEC3_CLASS_HEAD& _src)
{
	return Transform(_ret, _mtx.GetMtx34(), _src);
}

VEC_MTX_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	Mtx::Transform(VEC4_CLASS_HEAD& _ret, const MTX44_CLASS_HEAD& _mtx, const VEC4_CLASS_HEAD& _src)
{
	_ret.x = (_ret.x*_mtx[0][0]) + (_ret.y*_mtx[0][1]) + (_ret.z*_mtx[0][2]) + (_ret.w*_mtx[0][3]);
	_ret.y = (_ret.x*_mtx[1][0]) + (_ret.y*_mtx[1][1]) + (_ret.z*_mtx[1][2]) + (_ret.w*_mtx[1][3]);
	_ret.z = (_ret.x*_mtx[2][0]) + (_ret.y*_mtx[2][1]) + (_ret.z*_mtx[2][2]) + (_ret.w*_mtx[2][3]);
	_ret.w = (_ret.x*_mtx[3][0]) + (_ret.y*_mtx[3][1]) + (_ret.z*_mtx[3][2]) + (_ret.w*_mtx[3][3]);
	return _ret;
}


///*************************************************************************************************
/// スケール抽出
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// 行列からスケール成分抽出
VEC_MTX_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	GetScale(VEC2_CLASS_HEAD& _ret, const MTX22_CLASS_HEAD& _mtx)
{
	VEC2_CLASS_HEAD vX, vY;
	_mtx.GetAxis(vX, vY);
	_ret.x = Vec::Length(vX);
	_ret.y = Vec::Length(vY);
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	GetScale2(VEC2_CLASS_HEAD& _ret, const MTX22_CLASS_HEAD& _mtx)
{
	VEC2_CLASS_HEAD vX, vY;
	_mtx.GetAxis(vX, vY);
	_ret.x = Vec::Length2(vX);	// ２乗
	_ret.y = Vec::Length2(vY);	// ２乗
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	GetScale(VEC2_CLASS_HEAD& _ret, const MTX23_CLASS_HEAD& _mtx)
{
	GetScale(_ret, _mtx.GetMtx22());
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	GetScale2(VEC2_CLASS_HEAD& _ret, const MTX23_CLASS_HEAD& _mtx)
{
	GetScale2(_ret, _mtx.GetMtx22());
	return _ret;
}

VEC_MTX_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	GetScale(VEC2_CLASS_HEAD& _ret, const MTX33_CLASS_HEAD& _mtx)
{
	GetScale(_ret, _mtx.GetMtx22());
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
VEC2_CLASS_HEAD&	GetScale2(VEC2_CLASS_HEAD& _ret, const MTX33_CLASS_HEAD& _mtx)
{
	GetScale2(_ret, _mtx.GetMtx22());
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	GetScale(VEC3_CLASS_HEAD& _ret, const MTX33_CLASS_HEAD& _mtx)
{
	VEC3_CLASS_HEAD vX, vY, vZ;
	_mtx.GetAxis(vX, vY, vZ);
	_ret.x = Vec::Length(vX);
	_ret.y = Vec::Length(vY);
	_ret.z = Vec::Length(vZ);
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	GetScale2(VEC3_CLASS_HEAD& _ret, const MTX33_CLASS_HEAD& _mtx)
{
	VEC3_CLASS_HEAD vX, vY, vZ;
	_mtx.GetAxis(vX, vY, vZ);
	_ret.x = Vec::Length2(vX);	// ２乗
	_ret.y = Vec::Length2(vY);	// ２乗
	_ret.z = Vec::Length2(vZ);	// ２乗
	return _ret;
}

VEC_MTX_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	GetScale(VEC3_CLASS_HEAD& _ret, const MTX34_CLASS_HEAD& _mtx)
{
	GetScale(_ret, _mtx.GetMtx33());
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	GetScale2(VEC3_CLASS_HEAD& _ret, const MTX34_CLASS_HEAD& _mtx)
{
	GetScale2(_ret, _mtx.GetMtx33());
	return _ret;
}

VEC_MTX_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	GetScale(VEC3_CLASS_HEAD& _ret, const MTX44_CLASS_HEAD& _mtx)
{
	GetScale(_ret, _mtx.GetMtx33());
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
VEC3_CLASS_HEAD&	GetScale2(VEC3_CLASS_HEAD& _ret, const MTX44_CLASS_HEAD& _mtx)
{
	GetScale2(_ret, _mtx.GetMtx33());
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	GetScale(VEC4_CLASS_HEAD& _ret, const MTX44_CLASS_HEAD& _mtx)
{
	VEC4_CLASS_HEAD vX, vY, vZ, vW;
	_mtx.GetAxis(vX, vY, vZ, vW);
	_ret.x = Vec::Length(vX);
	_ret.y = Vec::Length(vY);
	_ret.z = Vec::Length(vZ);
	_ret.w = Vec::Length(vW);
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
VEC4_CLASS_HEAD&	GetScale2(VEC4_CLASS_HEAD& _ret, const MTX44_CLASS_HEAD& _mtx)
{
	VEC4_CLASS_HEAD vX, vY, vZ, vW;
	_mtx.GetAxis(vX, vY, vZ, vW);
	_ret.x = Vec::Length2(vX);	// ２乗
	_ret.y = Vec::Length2(vY);	// ２乗
	_ret.z = Vec::Length2(vZ);	// ２乗
	_ret.w = Vec::Length2(vW);	// ２乗
	return _ret;
}



///*************************************************************************************************
/// 回転変換
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// 回転行列からオイラーに変換
VEC_MTX_TEMPLATE_HEAD
EULER_CLASS_HEAD&	Mtx::MtxToEulerXYZ(EULER_CLASS_HEAD& _ret, const MTX33_CLASS_HEAD& _mtx)
{
	// ToDo:
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
EULER_CLASS_HEAD&	Mtx::MtxToEulerXZY(EULER_CLASS_HEAD& _ret, const MTX33_CLASS_HEAD& _mtx)
{
	// ToDo:
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
EULER_CLASS_HEAD&	Mtx::MtxToEulerYXZ(EULER_CLASS_HEAD& _ret, const MTX33_CLASS_HEAD& _mtx)
{
	// ToDo:
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
EULER_CLASS_HEAD&	Mtx::MtxToEulerYZX(EULER_CLASS_HEAD& _ret, const MTX33_CLASS_HEAD& _mtx)
{
	// ToDo:
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
EULER_CLASS_HEAD&	Mtx::MtxToEulerZXY(EULER_CLASS_HEAD& _ret, const MTX33_CLASS_HEAD& _mtx)
{
	// ToDo:
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
EULER_CLASS_HEAD&	Mtx::MtxToEulerZYX(EULER_CLASS_HEAD& _ret, const MTX33_CLASS_HEAD& _mtx)
{
	// ToDo:
	return _ret;
}

VEC_MTX_TEMPLATE_HEAD
EULER_CLASS_HEAD&	Mtx::MtxToEulerXYZ(EULER_CLASS_HEAD& _ret, const MTX34_CLASS_HEAD& _mtx)
{
	MtxToEulerXYZ(_ret, _mtx.GetMtx33());
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
EULER_CLASS_HEAD&	Mtx::MtxToEulerXZY(EULER_CLASS_HEAD& _ret, const MTX34_CLASS_HEAD& _mtx)
{
	MtxToEulerXZY(_ret, _mtx.GetMtx33());
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
EULER_CLASS_HEAD&	Mtx::MtxToEulerYXZ(EULER_CLASS_HEAD& _ret, const MTX34_CLASS_HEAD& _mtx)
{
	MtxToEulerYXZ(_ret, _mtx.GetMtx33());
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
EULER_CLASS_HEAD&	Mtx::MtxToEulerYZX(EULER_CLASS_HEAD& _ret, const MTX34_CLASS_HEAD& _mtx)
{
	MtxToEulerYZX(_ret, _mtx.GetMtx33());
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
EULER_CLASS_HEAD&	Mtx::MtxToEulerZXY(EULER_CLASS_HEAD& _ret, const MTX34_CLASS_HEAD& _mtx)
{
	MtxToEulerZXY(_ret, _mtx.GetMtx33());
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
EULER_CLASS_HEAD&	Mtx::MtxToEulerZYX(EULER_CLASS_HEAD& _ret, const MTX34_CLASS_HEAD& _mtx)
{
	MtxToEulerZYX(_ret, _mtx.GetMtx33());
	return _ret;
}

VEC_MTX_TEMPLATE_HEAD
EULER_CLASS_HEAD&	Mtx::MtxToEulerXYZ(EULER_CLASS_HEAD& _ret, const MTX44_CLASS_HEAD& _mtx)
{
	MtxToEulerXYZ(_ret, _mtx.GetMtx33());
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
EULER_CLASS_HEAD&	Mtx::MtxToEulerXZY(EULER_CLASS_HEAD& _ret, const MTX44_CLASS_HEAD& _mtx)
{
	MtxToEulerXZY(_ret, _mtx.GetMtx33());
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
EULER_CLASS_HEAD&	Mtx::MtxToEulerYXZ(EULER_CLASS_HEAD& _ret, const MTX44_CLASS_HEAD& _mtx)
{
	MtxToEulerYXZ(_ret, _mtx.GetMtx33());
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
EULER_CLASS_HEAD&	Mtx::MtxToEulerYZX(EULER_CLASS_HEAD& _ret, const MTX44_CLASS_HEAD& _mtx)
{
	MtxToEulerYZX(_ret, _mtx.GetMtx33());
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
EULER_CLASS_HEAD&	Mtx::MtxToEulerZXY(EULER_CLASS_HEAD& _ret, const MTX44_CLASS_HEAD& _mtx)
{
	MtxToEulerZXY(_ret, _mtx.GetMtx33());
	return _ret;
}
VEC_MTX_TEMPLATE_HEAD
EULER_CLASS_HEAD&	Mtx::MtxToEulerZYX(EULER_CLASS_HEAD& _ret, const MTX44_CLASS_HEAD& _mtx)
{
	MtxToEulerZYX(_ret, _mtx.GetMtx33());
	return _ret;
}

///-------------------------------------------------------------------------------------------------
/// 回転行列からクォータニオンに変換
VEC_MTX_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Mtx::MtxToQuat(QUAT_CLASS_HEAD& _ret, const MTX33_CLASS_HEAD& _mtx)
{
	VEC::FLOAT s;
	VEC::FLOAT tr = _mtx[0][0] + _mtx[1][1] + _mtx[2][2] + 1.0f;
	if (tr >= 1.0f) {
		s = 0.5f / sqrtf(tr);
		_ret.w = 0.25f / s;
		_ret.x = (_mtx[1][2] - _mtx[2][1]) * s;
		_ret.y = (_mtx[2][0] - _mtx[0][2]) * s;
		_ret.z = (_mtx[0][1] - _mtx[1][0]) * s;
		return _ret;
	}
	VEC::FLOAT max;
	if (_mtx[1][1] > _mtx[2][2]) {
		max = _mtx[1][1];
	}
	else {
		max = _mtx[2][2];
	}
	if (max < _mtx[0][0]) {
		s = sqrtf(_mtx[0][0] - (_mtx[1][1] + _mtx[2][2]) + 1.0f);
		VEC::FLOAT x = s * 0.5f;
		s = 0.5f / s;
		_ret.x = x;
		_ret.y = (_mtx[0][1] + _mtx[1][0]) * s;
		_ret.z = (_mtx[2][0] + _mtx[0][2]) * s;
		_ret.w = (_mtx[1][2] - _mtx[2][1]) * s;
	}
	else if (max == _mtx[1][1]) {
		s = sqrtf(_mtx[1][1] - (_mtx[2][2] + _mtx[0][0]) + 1.0f);
		VEC::FLOAT y = s * 0.5f;
		s = 0.5f / s;
		_ret.x = (_mtx[0][1] + _mtx[1][0]) * s;
		_ret.y = y;
		_ret.z = (_mtx[1][2] - _mtx[2][1]) * s;
		_ret.w = (_mtx[2][0] + _mtx[0][2]) * s;
	}
	else {
		s = sqrtf(_mtx[2][2] - (_mtx[0][0] + _mtx[1][1]) + 1.0f);
		VEC::FLOAT z = s * 0.5f;
		s = 0.5f / s;
		_ret.x = (_mtx[2][0] + _mtx[0][2]) * s;
		_ret.y = (_mtx[1][2] - _mtx[2][1]) * s;
		_ret.z = z;
		_ret.w = (_mtx[0][1] + _mtx[1][0]) * s;
	}
	return _ret;
}

VEC_MTX_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Mtx::MtxToQuat(QUAT_CLASS_HEAD& _ret, const MTX34_CLASS_HEAD& _mtx)
{
	MtxToQuat(_ret, _mtx.GetMtx33());
	return _ret;
}

VEC_MTX_TEMPLATE_HEAD
QUAT_CLASS_HEAD&	Mtx::MtxToQuat(QUAT_CLASS_HEAD& _ret, const MTX44_CLASS_HEAD& _mtx)
{
	MtxToQuat(_ret, _mtx.GetMtx33());
	return _ret;
}


///*************************************************************************************************
/// 行列成分抽出
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// 行列をスケール成分、回転成分に分割
VEC_MTX_TEMPLATE_HEAD
void	Mtx::Decompose(VEC3_CLASS_HEAD& _outScale, EULER_CLASS_HEAD& _outRot, const MTX33_CLASS_HEAD& _src, const typename EULER_CLASS_HEAD::ROT_TYPE _type)
{
	// スケール成分抽出
	GetScale(_outScale, _src);

	// 回転成分抽出
	typedef EULER_CLASS_HEAD& (*MTX_EULER_FUNC)(EULER_CLASS_HEAD&, const MTX33_CLASS_HEAD&);
	static const MTX_EULER_FUNC s_mtxEulerFunc[] =
	{
		&MtxToEulerXYZ,
		&MtxToEulerXZY,
		&MtxToEulerYXZ,
		&MtxToEulerYZX,
		&MtxToEulerZXY,
		&MtxToEulerZYX,
	};
	StaticAssert(ARRAYOF(s_mtxEulerFunc) == EULER_CLASS_HEAD::ROT_TYPE_NUM);
	libAssert(_type < EULER_CLASS_HEAD::ROT_TYPE_NUM);
	s_mtxEulerFunc[_type](_outRot, _src);
}
VEC_MTX_TEMPLATE_HEAD
void	Mtx::Decompose(VEC3_CLASS_HEAD& _outScale, QUAT_CLASS_HEAD& _outRot, const MTX33_CLASS_HEAD& _src)
{
	// スケール成分抽出
	GetScale(_outScale, _src);

	// 回転成分抽出
	Mtx::MtxToQuat(_outRot, _src);
}

VEC_MTX_TEMPLATE_HEAD
void	Mtx::Decompose(VEC3_CLASS_HEAD& _outScale, EULER_CLASS_HEAD& _outRot, VEC3_CLASS_HEAD& _outTrans, const MTX34_CLASS_HEAD& _src, const typename EULER_CLASS_HEAD::ROT_TYPE _type)
{
	// スケール成分抽出
	// 回転成分抽出
	Decompose(_outScale, _outRot, _src.GetMtx33());

	// 移動成分抽出
	_src.GetTrans(_outTrans);
}
VEC_MTX_TEMPLATE_HEAD
void	Mtx::Decompose(VEC3_CLASS_HEAD& _outScale, QUAT_CLASS_HEAD& _outRot, VEC3_CLASS_HEAD& _outTrans, const MTX34_CLASS_HEAD& _src)
{
	// スケール成分抽出
	// 回転成分抽出
	Decompose(_outScale, _outRot, _src.GetMtx33());

	// 移動成分抽出
	_src.GetTrans(_outTrans);
}

VEC_MTX_TEMPLATE_HEAD
void	Mtx::Decompose(VEC3_CLASS_HEAD& _outScale, EULER_CLASS_HEAD& _outRot, VEC3_CLASS_HEAD& _outTrans, const MTX44_CLASS_HEAD& _src, const typename EULER_CLASS_HEAD::ROT_TYPE _type)
{
	Decompose(_outScale, _outRot, _outTrans, _src.GetMtx34());
}
VEC_MTX_TEMPLATE_HEAD
void	Mtx::Decompose(VEC3_CLASS_HEAD& _outScale, QUAT_CLASS_HEAD& _outRot, VEC3_CLASS_HEAD& _outTrans, const MTX44_CLASS_HEAD& _src)
{
	Decompose(_outScale, _outRot, _outTrans, _src.GetMtx34());
}

POISON_END


#endif	// __MATRIX_FUNC_INL__

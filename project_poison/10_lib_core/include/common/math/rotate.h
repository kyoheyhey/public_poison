﻿//#pragma once
#ifndef	__ROTATE_H__
#define	__ROTATE_H__


//-------------------------------------------------------------------------------------------------
// include
#include "vector.h"


POISON_BGN


//*************************************************************************************************
//@brief	オイラークラス
//*************************************************************************************************
template < typename VEC >
class TEuler : public VEC
{
private:
	friend class	Vec;
	friend class	Mtx;
	friend class	Rot;

private:
	typedef	typename VEC::VEC2	VEC2;
	typedef	typename VEC::VEC3	VEC3;

public:
	//@brief	型定義
	typedef	typename VEC::FLOAT	FLOAT;
	//@brief	ベクトル型
	typedef	TVec2<VEC2>			TVec2;
	typedef	TVec3<VEC3>			TVec3;
	typedef	TVec4<VEC>			TVec4;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	回転タイプ
	enum ROT_TYPE
	{
		ROT_XYZ = 0,
		ROT_XZY,
		ROT_YXZ,
		ROT_YZX,
		ROT_ZXY,
		ROT_ZYX,

		ROT_TYPE_NUM
	};

public:
	TEuler();
	TEuler(FLOAT _x, FLOAT _y, FLOAT _z, ROT_TYPE _type = ROT_ZXY);
	TEuler(const FLOAT* _xyz, ROT_TYPE _type = ROT_ZXY);
	TEuler(const TVec3& _xyz, ROT_TYPE _type = ROT_ZXY);
	
	//-------------------------------------------------------------------------------------------------
	//@brief	compar operators
	TEuler&	operator=(const TEuler&);			///< 代入
	b8		operator==(const TEuler&) const;	///< 一致
	b8		operator!=(const TEuler&) const;	///< 不一致

	//-------------------------------------------------------------------------------------------------
	//@brief	assignment operators
	TEuler&	operator+=(const TEuler&);
	TEuler&	operator-=(const TEuler&);

	//-------------------------------------------------------------------------------------------------
	//@brief	binary operators
	TEuler	operator+(const TEuler&) const;
	TEuler	operator-(const TEuler&) const;

	//-------------------------------------------------------------------------------------------------
	//@brief	Setter
	void	Set(FLOAT _x, FLOAT _y, FLOAT _z, ROT_TYPE _type = ROT_ZXY);
	void	Set(const FLOAT* _xyz, ROT_TYPE _type = ROT_ZXY);
	void	Set(const TVec3& _xyz, ROT_TYPE _type = ROT_ZXY);
	void	Set(ROT_TYPE _type);

	//-------------------------------------------------------------------------------------------------
	//@brief	Getter
	TVec3&			GetXYZ();
	const TVec3&	GetXYZ() const { return CCast<TEuler*>(this)->GetXYZ(); }
	ROT_TYPE		GetRotType() const;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	定数値
	static const TEuler	Zero;
};


//*************************************************************************************************
//@brief	クォータニオンクラス
//*************************************************************************************************
template < typename VEC >
class TQuat : public VEC
{
private:
	friend class	Vec;
	friend class	Mtx;
	friend class	Rot;

private:
	typedef	typename VEC::VEC2	VEC2;
	typedef	typename VEC::VEC3	VEC3;

public:
	//@brief	型定義
	typedef	typename VEC::FLOAT	FLOAT;
	//@brief	ベクトル型
	typedef	TVec2<VEC2>			TVec2;
	typedef	TVec3<VEC3>			TVec3;
	typedef	TVec4<VEC>			TVec4;

public:
	TQuat();
	TQuat(FLOAT _x, FLOAT _y, FLOAT _z, FLOAT _w);
	TQuat(const FLOAT* _xyzw);
	TQuat(const TVec4& _xyzw);
	
	//-------------------------------------------------------------------------------------------------
	//@brief	compar operators
	TQuat&	operator=(const TQuat&);		///< 代入
	b8		operator==(const TQuat&) const;	///< 一致
	b8		operator!=(const TQuat&) const;	///< 不一致

	//-------------------------------------------------------------------------------------------------
	//@brief	assignment operators
	TQuat&	operator+=(const TQuat&);
	TQuat&	operator-=(const TQuat&);
	TQuat&	operator*=(const TQuat&);
	TQuat&	operator*=(FLOAT);
	TQuat&	operator/=(FLOAT);

	//-------------------------------------------------------------------------------------------------
	//@brief	binary operators
	TQuat	operator+(const TQuat&) const;
	TQuat	operator-(const TQuat&) const;
	TQuat	operator*(const TQuat&) const;
	TQuat	operator*(FLOAT) const;
	TQuat	operator/(FLOAT) const;

	//-------------------------------------------------------------------------------------------------
	//@brief	Setter
	void	Set(FLOAT _x, FLOAT _y, FLOAT _z, FLOAT _w);
	void	Set(const FLOAT* _xyzw);
	void	Set(const TVec4& _xyzw);

	//-------------------------------------------------------------------------------------------------
	//@brief	Getter
	TVec4&			GetXYZW();
	const TVec4&	GetXYZW() const { return CCast<TQuat*>(this)->GetXYZW(); }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	定数値
	static const TQuat	Identity;
};



//*************************************************************************************************
//@brief	回転計算関数
//*************************************************************************************************
class Rot
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	乗算・減算・乗算
	template < typename VEC >
	static TEuler<VEC>&	Add(TEuler<VEC>& _ret, const TEuler<VEC>& _quatA, const TEuler<VEC>& _quatB);
	template < typename VEC >
	static TEuler<VEC>&	Sub(TEuler<VEC>& _ret, const TEuler<VEC>& _quatA, const TEuler<VEC>& _quatB);
	template < typename VEC >
	static TEuler<VEC>&	Mul(TEuler<VEC>& _ret, const TEuler<VEC>& _quatA, const TEuler<VEC>& _quatB);
	template < typename VEC >
	static TEuler<VEC>&	Scale(TEuler<VEC>& _ret, const TEuler<VEC>& _quat, const typename TEuler<VEC>::FLOAT _s);

	//-------------------------------------------------------------------------------------------------
	//@brief	乗算・減算・乗算
	template < typename VEC >
	static TQuat<VEC>&	Add(TQuat<VEC>& _ret, const TQuat<VEC>& _quatA, const TQuat<VEC>& _quatB);
	template < typename VEC >
	static TQuat<VEC>&	Sub(TQuat<VEC>& _ret, const TQuat<VEC>& _quatA, const TQuat<VEC>& _quatB);
	template < typename VEC >
	static TQuat<VEC>&	Mul(TQuat<VEC>& _ret, const TQuat<VEC>& _quatA, const TQuat<VEC>& _quatB);
	template < typename VEC >
	static TQuat<VEC>&	Scale(TQuat<VEC>& _ret, const TQuat<VEC>& _quat, const typename TQuat<VEC>::FLOAT _s);

	//-------------------------------------------------------------------------------------------------
	//@brief	クォータニオン計算
	template < typename VEC >
	static TQuat<VEC>&	Identity(TQuat<VEC>& _ret);
	template < typename VEC >
	static typename TQuat<VEC>::FLOAT	Length(const TQuat<VEC>& _quat);
	template < typename VEC >
	static typename TQuat<VEC>::FLOAT	Length2(const TQuat<VEC>& _quat);
	template < typename VEC >
	static TQuat<VEC>&	Normalize(TQuat<VEC>& _ret, const TQuat<VEC>& _src);
	template < typename VEC >
	static TQuat<VEC>&	Inverse(TQuat<VEC>& _ret, const TQuat<VEC>& _src);

	template < typename VEC >
	static TQuat<VEC>&	Rotate(TQuat<VEC>& _ret, const TVec3<typename VEC::VEC3>& _vec, const typename TQuat<VEC>::FLOAT _angle);
	template < typename VEC >
	static TQuat<VEC>&	Rotate(TQuat<VEC>& _ret, const TVec4<VEC>& _vec, const typename TQuat<VEC>::FLOAT _angle);
	
	template < typename VEC >
	static TQuat<VEC>&	RotateX(TQuat<VEC>& _ret, const typename TQuat<VEC>::FLOAT _angle);
	template < typename VEC >
	static TQuat<VEC>&	RotateY(TQuat<VEC>& _ret, const typename TQuat<VEC>::FLOAT _angle);
	template < typename VEC >
	static TQuat<VEC>&	RotateZ(TQuat<VEC>& _ret, const typename TQuat<VEC>::FLOAT _angle);

	template < typename VEC >
	static TQuat<VEC>&	RotateXYZ(TQuat<VEC>& _ret, const typename TQuat<VEC>::FLOAT _x, const typename TQuat<VEC>::FLOAT _y, const typename TQuat<VEC>::FLOAT _z);
	template < typename VEC >
	static TQuat<VEC>&	RotateXYZ(TQuat<VEC>& _ret, const typename TQuat<VEC>::FLOAT* _xyz);
	template < typename VEC >
	static TQuat<VEC>&	RotateXYZ(TQuat<VEC>& _ret, const TVec3<typename VEC::VEC3>& _xyz);

	template < typename VEC >
	static TQuat<VEC>&	RotateXZY(TQuat<VEC>& _ret, const typename TQuat<VEC>::FLOAT _x, const typename TQuat<VEC>::FLOAT _y, const typename TQuat<VEC>::FLOAT _z);
	template < typename VEC >
	static TQuat<VEC>&	RotateXZY(TQuat<VEC>& _ret, const typename TQuat<VEC>::FLOAT* _xyz);
	template < typename VEC >
	static TQuat<VEC>&	RotateXZY(TQuat<VEC>& _ret, const TVec3<typename VEC::VEC3>& _xyz);

	template < typename VEC >
	static TQuat<VEC>&	RotateYXZ(TQuat<VEC>& _ret, const typename TQuat<VEC>::FLOAT _x, const typename TQuat<VEC>::FLOAT _y, const typename TQuat<VEC>::FLOAT _z);
	template < typename VEC >
	static TQuat<VEC>&	RotateYXZ(TQuat<VEC>& _ret, const typename TQuat<VEC>::FLOAT* _xyz);
	template < typename VEC >
	static TQuat<VEC>&	RotateYXZ(TQuat<VEC>& _ret, const TVec3<typename VEC::VEC3>& _xyz);

	template < typename VEC >
	static TQuat<VEC>&	RotateYZX(TQuat<VEC>& _ret, const typename TQuat<VEC>::FLOAT _x, const typename TQuat<VEC>::FLOAT _y, const typename TQuat<VEC>::FLOAT _z);
	template < typename VEC >
	static TQuat<VEC>&	RotateYZX(TQuat<VEC>& _ret, const typename TQuat<VEC>::FLOAT* _xyz);
	template < typename VEC >
	static TQuat<VEC>&	RotateYZX(TQuat<VEC>& _ret, const TVec3<typename VEC::VEC3>& _xyz);

	template < typename VEC >
	static TQuat<VEC>&	RotateZXY(TQuat<VEC>& _ret, const typename TQuat<VEC>::FLOAT _x, const typename TQuat<VEC>::FLOAT _y, const typename TQuat<VEC>::FLOAT _z);
	template < typename VEC >
	static TQuat<VEC>&	RotateZXY(TQuat<VEC>& _ret, const typename TQuat<VEC>::FLOAT* _xyz);
	template < typename VEC >
	static TQuat<VEC>&	RotateZXY(TQuat<VEC>& _ret, const TVec3<typename VEC::VEC3>& _xyz);

	template < typename VEC >
	static TQuat<VEC>&	RotateZYX(TQuat<VEC>& _ret, const typename TQuat<VEC>::FLOAT _x, const typename TQuat<VEC>::FLOAT _y, const typename TQuat<VEC>::FLOAT _z);
	template < typename VEC >
	static TQuat<VEC>&	RotateZYX(TQuat<VEC>& _ret, const typename TQuat<VEC>::FLOAT* _xyz);
	template < typename VEC >
	static TQuat<VEC>&	RotateZYX(TQuat<VEC>& _ret, const TVec3<typename VEC::VEC3>& _xyz);

	template < typename VEC >
	static TQuat<VEC>&	Lerp(TQuat<VEC>& _ret, const TQuat<VEC>& _quatA, const TQuat<VEC>& _quatB, const typename TQuat<VEC>::FLOAT _t);
	template < typename VEC >
	static TQuat<VEC>&	Slerp(TQuat<VEC>& _ret, const TQuat<VEC>& _quatA, const TQuat<VEC>& _quatB, const typename TQuat<VEC>::FLOAT _t);

	//-------------------------------------------------------------------------------------------------
	//@brief	変換
	template < typename VEC >
	static TQuat<VEC>&	EulerToQuat(TQuat<VEC>& _ret, const TEuler<VEC>& _euler);
	template < typename VEC >
	static TEuler<VEC>&	QuatToEulerXYZ(TEuler<VEC>& _ret, const TQuat<VEC>& _quat);
	template < typename VEC >
	static TEuler<VEC>& QuatToEulerXZY(TEuler<VEC>& _ret, const TQuat<VEC>& _quat);
	template < typename VEC >
	static TEuler<VEC>& QuatToEulerYXZ(TEuler<VEC>& _ret, const TQuat<VEC>& _quat);
	template < typename VEC >
	static TEuler<VEC>& QuatToEulerYZZ(TEuler<VEC>& _ret, const TQuat<VEC>& _quat);
	template < typename VEC >
	static TEuler<VEC>& QuatToEulerZXY(TEuler<VEC>& _ret, const TQuat<VEC>& _quat);
	template < typename VEC >
	static TEuler<VEC>& QuatToEulerZYX(TEuler<VEC>& _ret, const TQuat<VEC>& _quat);

	//-------------------------------------------------------------------------------------------------
	//@brief	ベクトル変換
	template< typename VEC >
	static TVec3<typename VEC::VEC3>&	Transform(TVec3<typename VEC::VEC3>& _ret, const TEuler<VEC>& _euler, const TVec3<typename VEC::VEC3>& _src);
	template< typename VEC >
	static TVec4<VEC>&	Transform(TVec4<VEC>& _ret, const TEuler<VEC>& _euler, const TVec4<VEC>& _src);

	template< typename VEC >
	static TVec3<typename VEC::VEC3>&	Transform(TVec3<typename VEC::VEC3>& _ret, const TQuat<VEC>& _quat, const TVec3<typename VEC::VEC3>& _src);
	template< typename VEC >
	static TVec4<VEC>&	Transform(TVec4<VEC>& _ret, const TQuat<VEC>& _quat, const TVec4<VEC>& _src);
};


//*************************************************************************************************
//@brief	回転定義
typedef	TEuler<VECTOR4>	CEuler;
typedef	TQuat<VECTOR4>	CQuat;


POISON_END

#include "rotate.inl"
#include "rotate_func.inl"

#endif	//__ROTATE_H__

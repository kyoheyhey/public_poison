﻿#pragma once
#ifndef	__MATH_H__
#define	__MATH_H__

#include <math.h>

POISON_BGN

//-------------------------------------------------------------------------------------------------
// define
#define PM_U16_MAX		0xffff
#define PM_U32_MAX		0xffffffff
#define PM_U64_MAX		0xffffffffffffffff
#define PM_FLOAT_MAX	0x7f7fffff

#define	PM_E		2.718281828f
#define	PM_LOG2E	1.442695041f
#define	PM_LOG10E	0.434294482f
#define	PM_LN2		0.693147181f
#define	PM_LN10		2.302585093f
#define	PM_PI		3.141592653f
#define	PM_PI_2		1.570796327f
#define	PM_PI_4		0.785398163f
#define	PM_1_PI		0.318309886f
#define	PM_2_PI		0.636619772f
#define	PM_2_SQRTPI	1.128379167f
#define	PM_SQRT2	1.414213562f
#define	PM_SQRT1_2	0.707106781f
#define	PM_SQRT3	1.732050807f
#define	PM_SQRT1_3	0.577350269f
#define	PM_SQRT5	2.236067977f
#define	PM_SQRT1_5	0.447213595f

#define PM_EPSILON	1.192092896e-07f	// smallest such that 1.0+FLT_EPSILON != 1.0



//*************************************************************************************************
//@brief	数学関数群
//*************************************************************************************************
class Math
{
public:
	///---------------------------------------------------------------------
	/// 線形補間
	/// 引数：	vAからvBまでt(0～1)だけ線形補間
	/// 戻り値：	f32
	static f32	Lerp(f32 _vA, f32 _vB, f32 _t);

	///---------------------------------------------------------------------
	/// エルミート補間
	/// 引数	
	///	第１：始点
	///	第２：終点
	///	第３：始点ベクトル
	///	第４：終点ベクトル
	///	第５：補間係数
	/// 戻り値：	f32
	static f32	Hermite(f32 _pA, f32 _pB, f32 _vA, f32 _vB, f32 _t);

	///---------------------------------------------------------------------
	/// 余弦定理
	/// 引数：[in]	a,b,cは辺の長さ
	/// 引数：[out]	A,B,Cはcos値のref
	/// 戻り値：	a,b,cで３角形が成り立つかどうか
	static b8	LawOfCos(f32 _a, f32 _b, f32 _c, f32& _A, f32& _B, f32& _C);

	///---------------------------------------------------------------------
	/// 度単位->ラジアン単位変換
	/// 引数：	度単位で計測された角度
	/// 戻り値：	ラジアン単位
	static f32	ToRadians(f32 _degree);

	///---------------------------------------------------------------------
	/// ラジアン単位変換->度単位
	/// 引数：	ラジアン単位
	/// 戻り値：	度単位で計測された角度
	static f32	ToDegrees(f32 _radian);
};


POISON_END

#endif	//__MATH_H__

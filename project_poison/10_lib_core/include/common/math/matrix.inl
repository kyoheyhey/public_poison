﻿//#pragma once
#ifndef __MATRIX_INL__
#define __MATRIX_INL__


POISON_BGN

//-------------------------------------------------------------------------------------------------
/// テンプレート宣言部分の省略
#define MTX_TEMPLATE_HEAD		template< typename MTX >
#define MTX22_CLASS_HEAD		TMtx22< MTX >
#define MTX23_CLASS_HEAD		TMtx23< MTX >
#define MTX33_CLASS_HEAD		TMtx33< MTX >
#define MTX34_CLASS_HEAD		TMtx34< MTX >
#define MTX44_CLASS_HEAD		TMtx44< MTX >



///*************************************************************************************************
/// ２×２行列クラス
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
// コンストラクタ
MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD::TMtx22()
{
	m[0][0] = m[1][1] = 1.0f;
	m[0][1] = m[1][0] = 0.0f;
}
MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD::TMtx22(
	FLOAT _00, FLOAT _01,
	FLOAT _10, FLOAT _11
	)
{
	v[0].Set(_00, _01);
	v[1].Set(_10, _11);
}
MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD::TMtx22(const FLOAT* _pF)
{
	m[0][0] = _pF[0], m[0][1] = _pF[1];
	m[1][0] = _pF[2], m[1][1] = _pF[3];
}

MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD::TMtx22(const TVec2& _v0, const TVec2& _v1)
{
	v[0] = _v0; v[1] = _v1;
}
MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD::TMtx22(const TVec2* _pV)
{
	v[0] = _pV[0]; v[1] = _pV[1];
}

MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD::TMtx22(const TVec2& _scale)
{
	m[0][0] = _scale[0];  m[1][1] = _scale[1];
	m[0][1] = m[1][0]= 0.0f;
}

///-------------------------------------------------------------------------------------------------
/// compar operators
MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD&	MTX22_CLASS_HEAD::operator=(const TMtx22& _mtx)
{
	m[0][0] = _mtx[0][0], m[0][1] = _mtx[0][1], m[0][2];
	m[1][0] = _mtx[1][0], m[1][1] = _mtx[1][1], m[1][2];
	return *this;
}
MTX_TEMPLATE_HEAD
b8					MTX22_CLASS_HEAD::operator==(const TMtx22& _mtx) const
{
	return (
		m[0][0] == _mtx[0][0] && m[0][1] == _mtx[0][1] &&
		m[1][0] == _mtx[1][0] && m[1][1] == _mtx[1][1]
		);
}
MTX_TEMPLATE_HEAD
b8					MTX22_CLASS_HEAD::operator!=(const TMtx22& _mtx) const
{
	return (
		m[0][0] != _mtx[0][0] || m[0][1] != _mtx[0][1] ||
		m[1][0] != _mtx[1][0] || m[1][1] != _mtx[1][1]
		);
}


///-------------------------------------------------------------------------------------------------
// assignment operators
MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD&	MTX22_CLASS_HEAD::operator+=(const TMtx22& _mtx)
{
	m[0][0] += _mtx[0][0], m[0][1] += _mtx[0][1];
	m[1][0] += _mtx[1][0], m[1][1] += _mtx[1][1];
	return *this;
}

MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD&	MTX22_CLASS_HEAD::operator*=(const TMtx22& _mtx)
{
	TMtx22 temp;
	temp = *this;
	m[0][0] = (temp[0][0] * _mtx[0][0]) + (temp[0][1] * _mtx[1][0]);
	m[0][1] = (temp[0][0] * _mtx[0][1]) + (temp[0][1] * _mtx[1][1]);
	m[1][0] = (temp[1][0] * _mtx[0][0]) + (temp[1][1] * _mtx[1][0]);
	m[1][1] = (temp[1][0] * _mtx[0][1]) + (temp[1][1] * _mtx[1][1]);
	return *this;
}

///-------------------------------------------------------------------------------------------------
// scale operators
MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD&	MTX22_CLASS_HEAD::operator*=(const TVec2& _vec)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0] *= _vec;
	v[1] *= _vec;
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0] *= _vec[0];
	v[1] *= _vec[1];
#endif
	return *this;
}

MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD&	MTX22_CLASS_HEAD::operator/=(const TVec2& _vec)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0] /= _vec;
	v[1] /= _vec;
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0] /= _vec[0];
	v[1] /= _vec[1];
#endif
	return *this;
}

MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD&	MTX22_CLASS_HEAD::operator*=(FLOAT _val)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0] *= TVec2(_val, 0.0f);
	v[1] *= TVec2(0.0f, _val);
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0] *= _val;
	v[1] *= _val;
#endif
	return *this;
}

MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD&	MTX22_CLASS_HEAD::operator/=(FLOAT _val)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0] /= TVec2(_val, 0.0f);
	v[1] /= TVec2(0.0f, _val);
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0] /= _val;
	v[1] /= _val;
#endif
	return *this;
}

///-------------------------------------------------------------------------------------------------
// binary operators
MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD	MTX22_CLASS_HEAD::operator+(const TMtx22& _mtx) const
{
	TMtx22 temp;
	temp[0][0] = (m[0][0] + _mtx[0][0]), temp[0][1] = (m[0][1] + _mtx[0][1]);
	temp[1][0] = (m[1][0] + _mtx[1][0]), temp[1][1] = (m[1][1] + _mtx[1][1]);
	return temp;
}

MTX_TEMPLATE_HEAD
MTX22_CLASS_HEAD	MTX22_CLASS_HEAD::operator*(const TMtx22& _mtx) const
{
	TMtx22 temp;
	temp[0][0] = (m[0][0] * _mtx[0][0]) + (m[0][1] * _mtx[1][0]);
	temp[0][1] = (m[0][0] * _mtx[0][1]) + (m[0][1] * _mtx[1][1]);
	temp[1][0] = (m[1][0] * _mtx[0][0]) + (m[1][1] * _mtx[1][0]);
	temp[1][1] = (m[1][0] * _mtx[0][1]) + (m[1][1] * _mtx[1][1]);
	return temp;
}

MTX_TEMPLATE_HEAD
typename MTX22_CLASS_HEAD::TVec2	MTX22_CLASS_HEAD::operator*(const TVec2& _vec) const
{
	TVec2 temp;
	temp.x = (_vec.x*m[0][0]) + (_vec.y*m[0][1]);
	temp.y = (_vec.x*m[1][0]) + (_vec.y*m[1][1]);
	return temp;
}

///-------------------------------------------------------------------------------------------------
/// GetAxis
MTX_TEMPLATE_HEAD
typename MTX22_CLASS_HEAD::TVec2&	MTX22_CLASS_HEAD::GetAxisX(TVec2& _vec) const
{
	_vec.Set(m[0][0], m[1][0]);
	return _vec;
}
MTX_TEMPLATE_HEAD
typename MTX22_CLASS_HEAD::TVec2&	MTX22_CLASS_HEAD::GetAxisY(TVec2& _vec) const
{
	_vec.Set(m[0][1], m[1][1]);
	return _vec;
}
MTX_TEMPLATE_HEAD
void	MTX22_CLASS_HEAD::GetAxis(TVec2& _x, TVec2& _y) const
{
	GetAxisX(_x);
	GetAxisY(_y);
}

///-------------------------------------------------------------------------------------------------
/// SetAxis
MTX_TEMPLATE_HEAD
void	MTX22_CLASS_HEAD::SetAxisX(const TVec2& _vec)
{
	m[0][0] = _vec.x, m[1][0] = _vec.y;
}
MTX_TEMPLATE_HEAD
void	MTX22_CLASS_HEAD::SetAxisY(const TVec2& _vec)
{
	m[0][1] = _vec.x, m[1][1] = _vec.y;
}
MTX_TEMPLATE_HEAD
void	MTX22_CLASS_HEAD::SetAxis(const TVec2& _x, const TVec2& _y)
{
	SetAxisX(_x);
	SetAxisY(_y);
}

///-------------------------------------------------------------------------------------------------
/// 定数値
MTX_TEMPLATE_HEAD
const MTX22_CLASS_HEAD	MTX22_CLASS_HEAD::Identity(
	1.0f, 0.0f,
	0.0f, 1.0f
	);



///*************************************************************************************************
/// ２×３行列クラス
///*************************************************************************************************
///-------------------------------------------------------------------------------------------------
// コンストラクタ
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD::TMtx23()
{
	m[0][0] = m[1][1] = 1.0f;
	m[0][1] = m[0][2] = m[1][0] = m[1][2] = 0.0f;
}
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD::TMtx23(
	FLOAT _00, FLOAT _01, FLOAT _02,
	FLOAT _10, FLOAT _11, FLOAT _12
	)
{
	v[0].Set(_00, _01, _02);
	v[1].Set(_10, _11, _12);
}
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD::TMtx23(const FLOAT* _pF)
{
	m[0][0] = _pF[0], m[0][1] = _pF[1], m[0][2] = _pF[2];
	m[1][0] = _pF[3], m[1][1] = _pF[4], m[1][2] = _pF[5];
}

MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD::TMtx23(const TVec3& _v0, const TVec3& _v1)
{
	v[0] = _v0; v[1] = _v1;
}
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD::TMtx23(const TVec3* _pV)
{
	v[0] = _pV[0]; v[1] = _pV[1];
}

MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD::TMtx23(const TMtx22& _mtx22)
{
	Copy(_mtx22);
}
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD::TMtx23(const TMtx22& _mtx22, const TVec2& _trans)
{
	Copy(_mtx22);
	SetTrans(_trans);
}

MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD::TMtx23(const TVec2& _scale)
{
	m[0][0] = _scale[0];  m[1][1] = _scale[1];
	m[0][1] = m[0][2] = m[1][0] = m[1][2] = 0.0f;
}

///-------------------------------------------------------------------------------------------------
/// compar operators
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	MTX23_CLASS_HEAD::operator=(const TMtx23& _mtx)
{
	m[0][0] = _mtx[0][0], m[0][1] = _mtx[0][1], m[0][2] = _mtx[0][2];
	m[1][0] = _mtx[1][0], m[1][1] = _mtx[1][1], m[1][2] = _mtx[1][2];
	return *this;
}
MTX_TEMPLATE_HEAD
b8					MTX23_CLASS_HEAD::operator==(const TMtx23& _mtx) const
{
	return (
		m[0][0] == _mtx[0][0] && m[0][1] == _mtx[0][1] && m[0][2] == _mtx[0][2] &&
		m[1][0] == _mtx[1][0] && m[1][1] == _mtx[1][1] && m[1][2] == _mtx[1][2]
		);
}
MTX_TEMPLATE_HEAD
b8					MTX23_CLASS_HEAD::operator!=(const TMtx23& _mtx) const
{
	return (
		m[0][0] != _mtx[0][0] || m[0][1] != _mtx[0][1] || m[0][2] != _mtx[0][2] ||
		m[1][0] != _mtx[1][0] || m[1][1] != _mtx[1][1] || m[1][2] != _mtx[1][2]
		);
}

///-------------------------------------------------------------------------------------------------
// assignment operators
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	MTX23_CLASS_HEAD::operator+=(const TMtx23& _mtx)
{
	m[0][0] += _mtx[0][0], m[0][1] += _mtx[0][1], m[0][2] += _mtx[0][2];
	m[1][0] += _mtx[1][0], m[1][1] += _mtx[1][1], m[1][2] += _mtx[1][2];
	return *this;
}

MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	MTX23_CLASS_HEAD::operator*=(const TMtx23& _mtx)
{
	TMtx23	temp;
	temp = *this;
	m[0][0] = (temp[0][0] * _mtx[0][0]) + (temp[0][1] * _mtx[1][0]);
	m[0][1] = (temp[0][0] * _mtx[0][1]) + (temp[0][1] * _mtx[1][1]);
	m[0][2] = (temp[0][0] * _mtx[0][2]) + (temp[0][1] * _mtx[1][2]) + (temp[0][2] * 1.0f);

	m[1][0] = (temp[1][0] * _mtx[0][0]) + (temp[1][1] * _mtx[1][0]);
	m[1][1] = (temp[1][0] * _mtx[0][1]) + (temp[1][1] * _mtx[1][1]);
	m[1][2] = (temp[1][0] * _mtx[0][2]) + (temp[1][1] * _mtx[1][2]) + (temp[1][2] * 1.0f);
	return *this;
}

///-------------------------------------------------------------------------------------------------
// scale operators
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	MTX23_CLASS_HEAD::operator*=(const TVec2& _vec)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0].GetXY() *= _vec;
	v[1].GetXY() *= _vec;
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0].GetXY() *= _vec[0];
	v[1].GetXY() *= _vec[1];
#endif
	return *this;
}

MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	MTX23_CLASS_HEAD::operator/=(const TVec2& _vec)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0].GetXY() /= _vec;
	v[1].GetXY() /= _vec;
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0].GetXY() /= _vec[0];
	v[1].GetXY() /= _vec[1];
#endif
	return *this;
}

MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	MTX23_CLASS_HEAD::operator*=(FLOAT _val)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0].GetXY() *= TVec2(_val, 0.0f);
	v[1].GetXY() *= TVec2(0.0f, _val);
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0].GetXY() *= _val;
	v[1].GetXY() *= _val;
#endif
	return *this;
}

MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	MTX23_CLASS_HEAD::operator/=(FLOAT _val)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0].GetXY() /= TVec2(_val, 0.0f);
	v[1].GetXY() /= TVec2(0.0f, _val);
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0].GetXY() /= _val;
	v[1].GetXY() /= _val;
#endif
	return *this;
}

///-------------------------------------------------------------------------------------------------
// binary operators
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD	MTX23_CLASS_HEAD::operator+(const TMtx23& _mtx) const
{
	TMtx23	temp;
	temp[0][0] = (m[0][0] + _mtx[0][0]), temp[0][1] = (m[0][1] + _mtx[0][1]), temp[0][2] = (m[0][2] + _mtx[0][2]);
	temp[1][0] = (m[1][0] + _mtx[1][0]), temp[1][1] = (m[1][1] + _mtx[1][1]), temp[1][2] = (m[1][2] + _mtx[1][2]);
	return temp;
}

MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD	MTX23_CLASS_HEAD::operator*(const TMtx23& _mtx) const
{
	TMtx23	temp;
	temp[0][0] = (m[0][0] * _mtx[0][0]) + (m[0][1] * _mtx[1][0]) + (m[0][2] * _mtx[2][0]);
	temp[0][1] = (m[0][0] * _mtx[0][1]) + (m[0][1] * _mtx[1][1]) + (m[0][2] * _mtx[2][1]);
	temp[0][2] = (m[0][0] * _mtx[0][2]) + (m[0][1] * _mtx[1][2]) + (m[0][2] * 1.0f);

	temp[1][0] = (m[1][0] * _mtx[0][0]) + (m[1][1] * _mtx[1][0]) + (m[1][2] * _mtx[2][0]);
	temp[1][1] = (m[1][0] * _mtx[0][1]) + (m[1][1] * _mtx[1][1]) + (m[1][2] * _mtx[2][1]);
	temp[1][2] = (m[1][0] * _mtx[0][2]) + (m[1][1] * _mtx[1][2]) + (m[1][2] * 1.0f);
	return temp;
}

MTX_TEMPLATE_HEAD
typename MTX23_CLASS_HEAD::TVec2	MTX23_CLASS_HEAD::operator*(const TVec2& _vec) const
{
	TVec2 temp;
	temp.x = (_vec.x*m[0][0]) + (_vec.y*m[0][1]) + m[0][2];
	temp.y = (_vec.x*m[1][0]) + (_vec.y*m[1][1]) + m[1][2];
	return temp;
}

///-------------------------------------------------------------------------------------------------
/// GetAxis
MTX_TEMPLATE_HEAD
typename MTX23_CLASS_HEAD::TVec2&	MTX23_CLASS_HEAD::GetAxisX(TVec2& _vec) const
{
	_vec.Set(m[0][0], m[1][0]);
	return _vec;
}
MTX_TEMPLATE_HEAD
typename MTX23_CLASS_HEAD::TVec2&	MTX23_CLASS_HEAD::GetAxisY(TVec2& _vec) const
{
	_vec.Set(m[0][1], m[1][1]);
	return _vec;
}
MTX_TEMPLATE_HEAD
typename MTX23_CLASS_HEAD::TVec2&	MTX23_CLASS_HEAD::GetAxisZ(TVec2& _vec) const
{
	_vec.Set(m[0][2], m[1][2]);
	return _vec;
}
MTX_TEMPLATE_HEAD
void	MTX23_CLASS_HEAD::GetAxis(TVec2& _x, TVec2& _y) const
{
	GetAxisX(_x);
	GetAxisY(_y);
}MTX_TEMPLATE_HEAD
void	MTX23_CLASS_HEAD::GetAxis(TVec2& _x, TVec2& _y, TVec2& _z) const
{
	GetAxisX(_x);
	GetAxisY(_y);
	GetAxisZ(_z);
}

///-------------------------------------------------------------------------------------------------
/// SetAxis
MTX_TEMPLATE_HEAD
void	MTX23_CLASS_HEAD::SetAxisX(const TVec2& _vec)
{
	m[0][0] = _vec.x, m[1][0] = _vec.y;
}
MTX_TEMPLATE_HEAD
void	MTX23_CLASS_HEAD::SetAxisY(const TVec2& _vec)
{
	m[0][1] = _vec.x, m[1][1] = _vec.y;
}
MTX_TEMPLATE_HEAD
void	MTX23_CLASS_HEAD::SetAxisZ(const TVec2& _vec)
{
	m[0][2] = _vec.x, m[1][2] = _vec.y;
}
MTX_TEMPLATE_HEAD
void	MTX23_CLASS_HEAD::SetAxis(const TVec2& _x, const TVec2& _y)
{
	SetAxisX(_x);
	SetAxisY(_y);
}
MTX_TEMPLATE_HEAD
void	MTX23_CLASS_HEAD::SetAxis(const TVec2& _x, const TVec2& _y, const TVec2& _z)
{
	SetAxisX(_x);
	SetAxisY(_y);
	SetAxisZ(_z);
}

///-------------------------------------------------------------------------------------------------
/// GetTrans
MTX_TEMPLATE_HEAD
typename MTX23_CLASS_HEAD::TVec2&	MTX23_CLASS_HEAD::GetTrans(TVec2& _trans) const
{
	_trans.Set(m[0][2], m[1][2]);
	return _trans;
}

///-------------------------------------------------------------------------------------------------
/// SetTrans
MTX_TEMPLATE_HEAD
void	MTX23_CLASS_HEAD::SetTrans(const TVec2& _trans)
{
	m[0][2] = _trans.x, m[1][2] = _trans.y;
}

///-------------------------------------------------------------------------------------------------
/// Get22
MTX_TEMPLATE_HEAD
typename MTX23_CLASS_HEAD::TMtx22&	MTX23_CLASS_HEAD::GetMtx22()
{
	return *PCast<TMtx22*>(this);
}

///-------------------------------------------------------------------------------------------------
// Set22
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	MTX23_CLASS_HEAD::SetMtx22(const TMtx22& _mtx22)
{
	m[0][0] = _mtx22[0][0], m[0][1] = _mtx22[0][1];
	m[1][0] = _mtx22[1][0], m[1][1] = _mtx22[1][1];
	return *this;
}

///-------------------------------------------------------------------------------------------------
// Copy
MTX_TEMPLATE_HEAD
MTX23_CLASS_HEAD&	MTX23_CLASS_HEAD::Copy(const TMtx22& _mtx22)
{
	m[0][0] = _mtx22[0][0], m[0][1] = _mtx22[0][1], m[0][2] = 0.0f;
	m[1][0] = _mtx22[1][0], m[1][1] = _mtx22[1][1], m[1][2] = 0.0f;
	return *this;
}

///-------------------------------------------------------------------------------------------------
/// 定数値
MTX_TEMPLATE_HEAD
const MTX23_CLASS_HEAD	MTX23_CLASS_HEAD::Identity(
	1.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f
	);



///*************************************************************************************************
/// ３×３行列クラス
///*************************************************************************************************
///-------------------------------------------------------------------------------------------------
// コンストラクタ
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD::TMtx33()
{
	m[0][0] = m[1][1] = m[2][2]  = 1.0f;
	m[0][1] = m[0][2] = m[1][0] = m[1][2] = m[2][0] = m[2][1] = 0.0f;
}
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD::TMtx33(
	FLOAT _00, FLOAT _01, FLOAT _02,
	FLOAT _10, FLOAT _11, FLOAT _12,
	FLOAT _20, FLOAT _21, FLOAT _22
	)
{
	m[0][0] = _00, m[0][1] = _01, m[0][2] = _02;
	m[1][0] = _10, m[1][1] = _11, m[1][2] = _12;
	m[2][0] = _20, m[2][1] = _21, m[2][2] = _22;
}
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD::TMtx33(const FLOAT* _pF)
{
	m[0][0] = _pF[0], m[0][1] = _pF[1], m[0][2] = _pF[2];
	m[1][0] = _pF[3], m[1][1] = _pF[4], m[1][2] = _pF[5];
	m[2][0] = _pF[6], m[2][1] = _pF[7], m[2][2] = _pF[8];
}

MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD::TMtx33(const TVec3& _v0, const TVec3& _v1, const TVec3& _v2)
{
	v[0] = _v0; v[1] = _v1; v[2] = _v2;
}
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD::TMtx33(const TVec3* _pV)
{
	v[0] = _pV[0]; v[1] = _pV[1]; v[2] = _pV[2];
}

MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD::TMtx33(const TMtx22& _mtx22)
{
	Copy(_mtx22);
}
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD::TMtx33(const TMtx22& _mtx22, const TVec2& _trans)
{
	Copy(_mtx22);
	SetTrans(_trans);
}

MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD::TMtx33(const TMtx23& _mtx23)
{
	Copy(_mtx23);
}

MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD::TMtx33(const TVec3& _scale)
{
	m[0][0] = _scale[0];  m[1][1] = _scale[1];  m[2][2] = _scale[2];
	m[0][1] = m[0][2] = m[1][0] = m[1][2] = m[2][0] = m[2][1] = 0.0f;
}

///-------------------------------------------------------------------------------------------------
/// compar operators
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	MTX33_CLASS_HEAD::operator=(const TMtx33& _mtx)
{
	m[0][0] = _mtx[0][0], m[0][1] = _mtx[0][1], m[0][2] = _mtx[0][2];
	m[1][0] = _mtx[1][0], m[1][1] = _mtx[1][1], m[1][2] = _mtx[1][2];
	m[2][0] = _mtx[2][0], m[2][1] = _mtx[2][1], m[2][2] = _mtx[2][2];
	return *this;
}
MTX_TEMPLATE_HEAD
b8					MTX33_CLASS_HEAD::operator==(const TMtx33& _mtx) const
{
	return (
		m[0][0] == _mtx[0][0] && m[0][1] == _mtx[0][1] && m[0][2] == _mtx[0][2] &&
		m[1][0] == _mtx[1][0] && m[1][1] == _mtx[1][1] && m[1][2] == _mtx[1][2] &&
		m[2][0] == _mtx[2][0] && m[2][1] == _mtx[2][1] && m[2][2] == _mtx[2][2]
		);
}
MTX_TEMPLATE_HEAD
b8					MTX33_CLASS_HEAD::operator!=(const TMtx33& _mtx) const
{
	return (
		m[0][0] != _mtx[0][0] || m[0][1] != _mtx[0][1] || m[0][2] != _mtx[0][2] ||
		m[1][0] != _mtx[1][0] || m[1][1] != _mtx[1][1] || m[1][2] != _mtx[1][2] ||
		m[2][0] != _mtx[2][0] || m[2][1] != _mtx[2][1] || m[2][2] != _mtx[2][2]
		);
}

///-------------------------------------------------------------------------------------------------
// assignment operators
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	MTX33_CLASS_HEAD::operator+=(const TMtx33& _mtx)
{
	m[0][0] += _mtx[0][0], m[0][1] += _mtx[0][1], m[0][2] += _mtx[0][2];
	m[1][0] += _mtx[1][0], m[1][1] += _mtx[1][1], m[1][2] += _mtx[1][2];
	m[2][0] += _mtx[2][0], m[2][1] += _mtx[2][1], m[2][2] += _mtx[2][2];
	return *this;
}

MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	MTX33_CLASS_HEAD::operator*=(const TMtx33& _mtx)
{
	TMtx33 temp;
	temp = *this;
	m[0][0] = (temp[0][0] * _mtx[0][0]) + (temp[0][1] * _mtx[1][0]) + (temp[0][2] * _mtx[2][0]);
	m[0][1] = (temp[0][0] * _mtx[0][1]) + (temp[0][1] * _mtx[1][1]) + (temp[0][2] * _mtx[2][1]);
	m[0][2] = (temp[0][0] * _mtx[0][2]) + (temp[0][1] * _mtx[1][2]) + (temp[0][2] * _mtx[2][2]);

	m[1][0] = (temp[1][0] * _mtx[0][0]) + (temp[1][1] * _mtx[1][0]) + (temp[1][2] * _mtx[2][0]);
	m[1][1] = (temp[1][0] * _mtx[0][1]) + (temp[1][1] * _mtx[1][1]) + (temp[1][2] * _mtx[2][1]);
	m[1][2] = (temp[1][0] * _mtx[0][2]) + (temp[1][1] * _mtx[1][2]) + (temp[1][2] * _mtx[2][2]);

	m[2][0] = (temp[2][0] * _mtx[0][0]) + (temp[2][1] * _mtx[1][0]) + (temp[2][2] * _mtx[2][0]);
	m[2][1] = (temp[2][0] * _mtx[0][1]) + (temp[2][1] * _mtx[1][1]) + (temp[2][2] * _mtx[2][1]);
	m[2][2] = (temp[2][0] * _mtx[0][2]) + (temp[2][1] * _mtx[1][2]) + (temp[2][2] * _mtx[2][2]);
	return *this;
}

///-------------------------------------------------------------------------------------------------
// scale operators
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	MTX33_CLASS_HEAD::operator*=(const TVec2& _vec)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0].GetXY() *= _vec;
	v[1].GetXY() *= _vec;
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0].GetXY() *= _vec[0];
	v[1].GetXY() *= _vec[1];
#endif
	return *this;
}

MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	MTX33_CLASS_HEAD::operator/=(const TVec2& _vec)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0].GetXY() /= _vec;
	v[1].GetXY() /= _vec;
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0].GetXY() /= _vec[0];
	v[1].GetXY() /= _vec[1];
#endif
	return *this;
}

MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	MTX33_CLASS_HEAD::operator*=(const TVec3& _vec)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0] *= _vec;
	v[1] *= _vec;
	v[2] *= _vec;
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0] *= _vec[0];
	v[1] *= _vec[1];
	v[2] *= _vec[2];
#endif
	return *this;
}

MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	MTX33_CLASS_HEAD::operator/=(const TVec3& _vec)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0] /= _vec;
	v[1] /= _vec;
	v[2] /= _vec;
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0] /= _vec[0];
	v[1] /= _vec[1];
	v[2] /= _vec[2];
#endif
	return *this;
}

MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	MTX33_CLASS_HEAD::operator*=(FLOAT _val)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0] *= TVec3(_val, 0.0f, 0.0f);
	v[1] *= TVec3(0.0f, _val, 0.0f);
	v[2] *= TVec3(0.0f, 0.0f, _val);
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0] *= _val;
	v[1] *= _val;
	v[2] *= _val;
#endif
	return *this;
}

MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	MTX33_CLASS_HEAD::operator/=(FLOAT _val)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0] /= TVec3(_val, 0.0f, 0.0f);
	v[1] /= TVec3(0.0f, _val, 0.0f);
	v[2] /= TVec3(0.0f, 0.0f, _val);
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0] /= _val;
	v[1] /= _val;
	v[2] /= _val;
#endif
	return *this;
}

///-------------------------------------------------------------------------------------------------
// binary operators
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD	MTX33_CLASS_HEAD::operator+(const TMtx33& _mtx) const
{
	TMtx33 temp;
	temp[0][0] = (m[0][0] + _mtx[0][0]), temp[0][1] = (m[0][1] + _mtx[0][1]), temp[0][2] = (m[0][2] + _mtx[0][2]);
	temp[1][0] = (m[1][0] + _mtx[1][0]), temp[1][1] = (m[1][1] + _mtx[1][1]), temp[1][2] = (m[1][2] + _mtx[1][2]);
	temp[2][0] = (m[2][0] + _mtx[2][0]), temp[2][1] = (m[2][1] + _mtx[2][1]), temp[2][2] = (m[2][2] + _mtx[2][2]);
	return temp;
}

MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD	MTX33_CLASS_HEAD::operator*(const TMtx33& _mtx) const
{
	TMtx33 temp;
	temp[0][0] = (m[0][0] * _mtx[0][0]) + (m[0][1] * _mtx[1][0]) + (m[0][2] * _mtx[2][0]);
	temp[0][1] = (m[0][0] * _mtx[0][1]) + (m[0][1] * _mtx[1][1]) + (m[0][2] * _mtx[2][1]);
	temp[0][2] = (m[0][0] * _mtx[0][2]) + (m[0][1] * _mtx[1][2]) + (m[0][2] * _mtx[2][2]);

	temp[1][0] = (m[1][0] * _mtx[0][0]) + (m[1][1] * _mtx[1][0]) + (m[1][2] * _mtx[2][0]);
	temp[1][1] = (m[1][0] * _mtx[0][1]) + (m[1][1] * _mtx[1][1]) + (m[1][2] * _mtx[2][1]);
	temp[1][2] = (m[1][0] * _mtx[0][2]) + (m[1][1] * _mtx[1][2]) + (m[1][2] * _mtx[2][2]);

	temp[2][0] = (m[2][0] * _mtx[0][0]) + (m[2][1] * _mtx[1][0]) + (m[2][2] * _mtx[2][0]);
	temp[2][1] = (m[2][0] * _mtx[0][1]) + (m[2][1] * _mtx[1][1]) + (m[2][2] * _mtx[2][1]);
	temp[2][2] = (m[2][0] * _mtx[0][2]) + (m[2][1] * _mtx[1][2]) + (m[2][2] * _mtx[2][2]);
	return temp;
}

MTX_TEMPLATE_HEAD
typename MTX33_CLASS_HEAD::TVec2	MTX33_CLASS_HEAD::operator*(const TVec2& _vec) const
{
	TVec2 temp;
	temp.x = (_vec.x*m[0][0]) + (_vec.y*m[0][1]) + m[0][2];
	temp.y = (_vec.x*m[1][0]) + (_vec.y*m[1][1]) + m[1][2];
	return temp;
}

MTX_TEMPLATE_HEAD
typename MTX33_CLASS_HEAD::TVec3	MTX33_CLASS_HEAD::operator*(const TVec3& _vec) const
{
	TVec3 temp;
	temp.x = (_vec.x*m[0][0]) + (_vec.y*m[0][1]) + (_vec.z*m[0][2]);
	temp.y = (_vec.x*m[1][0]) + (_vec.y*m[1][1]) + (_vec.z*m[1][2]);
	temp.z = (_vec.x*m[2][0]) + (_vec.y*m[2][1]) + (_vec.z*m[2][2]);
	return temp;
}

///-------------------------------------------------------------------------------------------------
/// GetAxis
MTX_TEMPLATE_HEAD
typename MTX33_CLASS_HEAD::TVec3&	MTX33_CLASS_HEAD::GetAxisX(TVec3& _vec) const
{
	_vec.Set(m[0][0], m[1][0], m[2][0]);
	return _vec;
}
MTX_TEMPLATE_HEAD
typename MTX33_CLASS_HEAD::TVec3&	MTX33_CLASS_HEAD::GetAxisY(TVec3& _vec) const
{
	_vec.Set(m[0][1], m[1][1], m[2][1]);
	return _vec;
}
MTX_TEMPLATE_HEAD
typename MTX33_CLASS_HEAD::TVec3&	MTX33_CLASS_HEAD::GetAxisZ(TVec3& _vec) const
{
	_vec.Set(m[0][2], m[1][2], m[2][2]);
	return _vec;
}
MTX_TEMPLATE_HEAD
void	MTX33_CLASS_HEAD::GetAxis(TVec3& _x, TVec3& _y, TVec3& _z) const
{
	GetAxisX(_x);
	GetAxisY(_y);
	GetAxisZ(_z);
}

///-------------------------------------------------------------------------------------------------
/// SetAxis
MTX_TEMPLATE_HEAD
void	MTX33_CLASS_HEAD::SetAxisX(const TVec3& _vec)
{
	m[0][0] = _vec.x, m[1][0] = _vec.y, m[2][0] = _vec.z;
}
MTX_TEMPLATE_HEAD
void	MTX33_CLASS_HEAD::SetAxisY(const TVec3& _vec)
{
	m[0][1] = _vec.x, m[1][1] = _vec.y, m[2][1] = _vec.z;
}
MTX_TEMPLATE_HEAD
void	MTX33_CLASS_HEAD::SetAxisZ(const TVec3& _vec)
{
	m[0][2] = _vec.x, m[1][2] = _vec.y, m[2][2] = _vec.z;
}
MTX_TEMPLATE_HEAD
void	MTX33_CLASS_HEAD::SetAxis(const TVec3& _x, const TVec3& _y, const TVec3& _z)
{
	SetAxisX(_x);
	SetAxisY(_y);
	SetAxisZ(_z);
}

///-------------------------------------------------------------------------------------------------
/// GetTrans
MTX_TEMPLATE_HEAD
typename MTX33_CLASS_HEAD::TVec2&	MTX33_CLASS_HEAD::GetTrans(TVec2& _trans) const
{
	_trans.Set(m[0][2], m[1][2]);
	return _trans;
}
///-------------------------------------------------------------------------------------------------
/// SetTrans
MTX_TEMPLATE_HEAD
void	MTX33_CLASS_HEAD::SetTrans(const TVec2& _trans)
{
	m[0][3] = _trans.x, m[1][3] = _trans.y;
}

///-------------------------------------------------------------------------------------------------
/// Get22
MTX_TEMPLATE_HEAD
typename MTX33_CLASS_HEAD::TMtx22&	MTX33_CLASS_HEAD::GetMtx22()
{
	return *PCast<TMtx22*>(this);
}
///-------------------------------------------------------------------------------------------------
/// Get23
MTX_TEMPLATE_HEAD
typename MTX33_CLASS_HEAD::TMtx23&	MTX33_CLASS_HEAD::GetMtx23()
{
	return *PCast<TMtx23*>(this);
}

///-------------------------------------------------------------------------------------------------
// Set
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	MTX33_CLASS_HEAD::SetMtx22(const TMtx22& _mtx22)
{
	m[0][0] = _mtx22[0][0], m[0][1] = _mtx22[0][1];
	m[1][0] = _mtx22[1][0], m[1][1] = _mtx22[1][1];
	return *this;
}
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	MTX33_CLASS_HEAD::SetMtx23(const TMtx23& _mtx23)
{
	m[0][0] = _mtx23[0][0], m[0][1] = _mtx23[0][1], m[0][2] = _mtx23[0][2];
	m[1][0] = _mtx23[1][0], m[1][1] = _mtx23[1][1], m[1][2] = _mtx23[1][2];
	return *this;
}

///-------------------------------------------------------------------------------------------------
// Copy
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	MTX33_CLASS_HEAD::Copy(const TMtx22& _mtx22)
{
	m[0][0] = _mtx22[0][0], m[0][1] = _mtx22[0][1], m[0][2] = 0.0f;
	m[1][0] = _mtx22[1][0], m[1][1] = _mtx22[1][1], m[1][2] = 0.0f;
	m[2][0] = 0.0f,			m[2][1] = 0.0f,			m[2][2] = 1.0f;
	return *this;
}
MTX_TEMPLATE_HEAD
MTX33_CLASS_HEAD&	MTX33_CLASS_HEAD::Copy(const TMtx23& _mtx23)
{
	m[0][0] = _mtx23[0][0], m[0][1] = _mtx23[0][1], m[0][2] = _mtx23[0][2];
	m[1][0] = _mtx23[1][0], m[1][1] = _mtx23[1][1], m[1][2] = _mtx23[1][2];
	m[2][0] = 0.0f,			m[2][1] = 0.0f,			m[2][2] = 1.0f;
	return *this;
}

///-------------------------------------------------------------------------------------------------
/// 定数値
MTX_TEMPLATE_HEAD
const MTX33_CLASS_HEAD	MTX33_CLASS_HEAD::Identity(
	1.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 1.0f
	);



///*************************************************************************************************
/// ３×４行列クラス
///*************************************************************************************************
///-------------------------------------------------------------------------------------------------
// コンストラクタ
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD::TMtx34()
{
	m[0][0] = m[1][1] = m[2][2] = 1.0f;
	m[0][1] = m[0][2] = m[0][3] = m[1][0] = m[1][2] = m[1][3] = m[2][0] = m[2][1] = m[2][3] = 0.0f;
}

MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD::TMtx34(
	FLOAT _00, FLOAT _01, FLOAT _02, FLOAT _03,
	FLOAT _10, FLOAT _11, FLOAT _12, FLOAT _13,
	FLOAT _20, FLOAT _21, FLOAT _22, FLOAT _23
	)
{
	m[0][0] = _00, m[0][1] = _01, m[0][2] = _02, m[0][3] = _03;
	m[1][0] = _10, m[1][1] = _11, m[1][2] = _12, m[1][3] = _13;
	m[2][0] = _20, m[2][1] = _21, m[2][2] = _22, m[2][3] = _23;
}
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD::TMtx34(const FLOAT* _pF)
{
	m[0][0] = _pF[0], m[0][1] = _pF[1], m[0][2] = _pF[2], m[0][3] = _pF[3];
	m[1][0] = _pF[4], m[1][1] = _pF[5], m[1][2] = _pF[6], m[1][3] = _pF[7];
	m[2][0] = _pF[8], m[2][1] = _pF[9], m[2][2] = _pF[10], m[2][3] = _pF[11];
}

MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD::TMtx34(const TVec4& _v0, const TVec4& _v1, const TVec4& _v2)
{
	v[0] = _v0; v[1] = _v1; v[2] = _v2;
}
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD::TMtx34(const TVec4* _pV)
{
	v[0] = _pV[0]; v[1] = _pV[1]; v[2] = _pV[2];
}

MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD::TMtx34(const TMtx33& _mtx33)
{
	Copy(_mtx33);
}

MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD::TMtx34(const TVec3& _scale)
{
	m[0][0] = _scale[0];  m[1][1] = _scale[1];  m[2][2] = _scale[2];
	m[0][1] = m[0][2] = m[0][3] = m[1][0] = m[1][2] = m[1][3] = m[2][0] = m[2][1] = m[2][3] = 0.0f;
}

///-------------------------------------------------------------------------------------------------
/// compar operators
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	MTX34_CLASS_HEAD::operator=(const TMtx34& _mtx)
{
	m[0][0] = _mtx[0][0], m[0][1] = _mtx[0][1], m[0][2] = _mtx[0][2], m[0][3] = _mtx[0][3];
	m[1][0] = _mtx[1][0], m[1][1] = _mtx[1][1], m[1][2] = _mtx[1][2], m[1][3] = _mtx[1][3];
	m[2][0] = _mtx[2][0], m[2][1] = _mtx[2][1], m[2][2] = _mtx[2][2], m[2][3] = _mtx[2][3];
	return *this;
}
MTX_TEMPLATE_HEAD
b8					MTX34_CLASS_HEAD::operator==(const TMtx34& _mtx) const
{
	return (
		m[0][0] == _mtx[0][0] && m[0][1] == _mtx[0][1] && m[0][2] == _mtx[0][2] && m[0][3] == _mtx[0][3] &&
		m[1][0] == _mtx[1][0] && m[1][1] == _mtx[1][1] && m[1][2] == _mtx[1][2] && m[1][3] == _mtx[1][3] &&
		m[2][0] == _mtx[2][0] && m[2][1] == _mtx[2][1] && m[2][2] == _mtx[2][2] && m[2][3] == _mtx[2][3]
		);
}
MTX_TEMPLATE_HEAD
b8					MTX34_CLASS_HEAD::operator!=(const TMtx34& _mtx) const
{
	return (
		m[0][0] != _mtx[0][0] || m[0][1] != _mtx[0][1] || m[0][2] != _mtx[0][2] || m[0][3] != _mtx[0][3] ||
		m[1][0] != _mtx[1][0] || m[1][1] != _mtx[1][1] || m[1][2] != _mtx[1][2] || m[1][3] != _mtx[1][3] ||
		m[2][0] != _mtx[2][0] || m[2][1] != _mtx[2][1] || m[2][2] != _mtx[2][2] || m[2][3] != _mtx[2][3]
		);
}

///-------------------------------------------------------------------------------------------------
// assignment operators
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	MTX34_CLASS_HEAD::operator+=(const TMtx34& _mtx)
{
	m[0][0] += _mtx[0][0], m[0][1] += _mtx[0][1], m[0][2] += _mtx[0][2], m[0][3] += _mtx[0][3];
	m[1][0] += _mtx[1][0], m[1][1] += _mtx[1][1], m[1][2] += _mtx[1][2], m[1][3] += _mtx[1][3];
	m[2][0] += _mtx[2][0], m[2][1] += _mtx[2][1], m[2][2] += _mtx[2][2], m[2][3] += _mtx[2][3];
	return *this;
}

MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	MTX34_CLASS_HEAD::operator*=(const TMtx34& _mtx)
{
	TMtx34 temp;
	temp = *this;
	m[0][0] = (temp[0][0] * _mtx[0][0]) + (temp[0][1] * _mtx[1][0]) + (temp[0][2] * _mtx[2][0]);
	m[0][1] = (temp[0][0] * _mtx[0][1]) + (temp[0][1] * _mtx[1][1]) + (temp[0][2] * _mtx[2][1]);
	m[0][2] = (temp[0][0] * _mtx[0][2]) + (temp[0][1] * _mtx[1][2]) + (temp[0][2] * _mtx[2][2]);
	m[0][3] = (temp[0][0] * _mtx[0][3]) + (temp[0][1] * _mtx[1][3]) + (temp[0][2] * _mtx[2][3]) + (temp[0][3]);

	m[1][0] = (temp[1][0] * _mtx[0][0]) + (temp[1][1] * _mtx[1][0]) + (temp[1][2] * _mtx[2][0]);
	m[1][1] = (temp[1][0] * _mtx[0][1]) + (temp[1][1] * _mtx[1][1]) + (temp[1][2] * _mtx[2][1]);
	m[1][2] = (temp[1][0] * _mtx[0][2]) + (temp[1][1] * _mtx[1][2]) + (temp[1][2] * _mtx[2][2]);
	m[1][3] = (temp[1][0] * _mtx[0][3]) + (temp[1][1] * _mtx[1][3]) + (temp[1][2] * _mtx[2][3]) + (temp[1][3]);

	m[2][0] = (temp[2][0] * _mtx[0][0]) + (temp[2][1] * _mtx[1][0]) + (temp[2][2] * _mtx[2][0]);
	m[2][1] = (temp[2][0] * _mtx[0][1]) + (temp[2][1] * _mtx[1][1]) + (temp[2][2] * _mtx[2][1]);
	m[2][2] = (temp[2][0] * _mtx[0][2]) + (temp[2][1] * _mtx[1][2]) + (temp[2][2] * _mtx[2][2]);
	m[2][3] = (temp[2][0] * _mtx[0][3]) + (temp[2][1] * _mtx[1][3]) + (temp[2][2] * _mtx[2][3]) + (temp[2][3]);

	return *this;
}

MTX_TEMPLATE_HEAD
typename MTX34_CLASS_HEAD::TVec3	MTX34_CLASS_HEAD::operator*(const TVec3& _vec) const
{
	TVec3 temp;
	temp.x = (_vec.x*m[0][0]) + (_vec.y*m[0][1]) + (_vec.z*m[0][2]) + m[0][3];
	temp.y = (_vec.x*m[1][0]) + (_vec.y*m[1][1]) + (_vec.z*m[1][2]) + m[1][3];
	temp.z = (_vec.x*m[2][0]) + (_vec.y*m[2][1]) + (_vec.z*m[2][2]) + m[2][3];
	return temp;
}

MTX_TEMPLATE_HEAD
typename MTX34_CLASS_HEAD::TVec4	MTX34_CLASS_HEAD::operator*(const TVec4& _vec) const
{
	TVec4 temp;
	temp.x = (_vec.x*m[0][0]) + (_vec.y*m[0][1]) + (_vec.z*m[0][2]);
	temp.y = (_vec.x*m[1][0]) + (_vec.y*m[1][1]) + (_vec.z*m[1][2]);
	temp.z = (_vec.x*m[2][0]) + (_vec.y*m[2][1]) + (_vec.z*m[2][2]);
	temp.w = 1.0f;
	return temp;
}

///-------------------------------------------------------------------------------------------------
// scale operators
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	MTX34_CLASS_HEAD::operator*=(const TVec3& _vec)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0].GetXYZ() *= _vec;
	v[1].GetXYZ() *= _vec;
	v[2].GetXYZ() *= _vec;
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0].GetXYZ() *= _vec[0];
	v[1].GetXYZ() *= _vec[1];
	v[2].GetXYZ() *= _vec[2];
#endif
	return *this;
}

MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	MTX34_CLASS_HEAD::operator/=(const TVec3& _vec)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0].GetXYZ() /= _vec;
	v[1].GetXYZ() /= _vec;
	v[2].GetXYZ() /= _vec;
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0].GetXYZ() /= _vec[0];
	v[1].GetXYZ() /= _vec[1];
	v[2].GetXYZ() /= _vec[2];
#endif
	return *this;
}

MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	MTX34_CLASS_HEAD::operator*=(FLOAT _val)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0].GetXYZ() *= TVec3(_val, 0.0f, 0.0f);
	v[1].GetXYZ() *= TVec3(0.0f, _val, 0.0f);
	v[2].GetXYZ() *= TVec3(0.0f, 0.0f, _val);
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0].GetXYZ() *= _val;
	v[1].GetXYZ() *= _val;
	v[2].GetXYZ() *= _val;
#endif
	return *this;
}

MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	MTX34_CLASS_HEAD::operator/=(FLOAT _val)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0].GetXYZ() /= TVec3(_val, 0.0f, 0.0f);
	v[1].GetXYZ() /= TVec3(0.0f, _val, 0.0f);
	v[2].GetXYZ() /= TVec3(0.0f, 0.0f, _val);
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0].GetXYZ() /= _val;
	v[1].GetXYZ() /= _val;
	v[2].GetXYZ() /= _val;
#endif
	return *this;
}

///-------------------------------------------------------------------------------------------------
// binary operators
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD	MTX34_CLASS_HEAD::operator+(const TMtx34& _mtx) const
{
	TMtx34 temp;
	temp[0][0] = (m[0][0] + _mtx[0][0]), temp[0][1] = (m[0][1] + _mtx[0][1]), temp[0][2] = (m[0][2] + _mtx[0][2]), temp[0][3] = (m[0][3] + _mtx[0][3]);
	temp[1][0] = (m[1][0] + _mtx[1][0]), temp[1][1] = (m[1][1] + _mtx[1][1]), temp[1][2] = (m[1][2] + _mtx[1][2]), temp[1][3] = (m[1][3] + _mtx[1][3]);
	temp[2][0] = (m[2][0] + _mtx[2][0]), temp[2][1] = (m[2][1] + _mtx[2][1]), temp[2][2] = (m[2][2] + _mtx[2][2]), temp[2][3] = (m[2][3] + _mtx[2][3]);
	return temp;
}

MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD	MTX34_CLASS_HEAD::operator*(const TMtx34& _mtx) const
{
	TMtx34 temp;
	temp[0][0] = (m[0][0] * _mtx[0][0]) + (m[0][1] * _mtx[1][0]) + (m[0][2] * _mtx[2][0]);
	temp[0][1] = (m[0][0] * _mtx[0][1]) + (m[0][1] * _mtx[1][1]) + (m[0][2] * _mtx[2][1]);
	temp[0][2] = (m[0][0] * _mtx[0][2]) + (m[0][1] * _mtx[1][2]) + (m[0][2] * _mtx[2][2]);
	temp[0][3] = (m[0][0] * _mtx[0][3]) + (m[0][1] * _mtx[1][3]) + (m[0][2] * _mtx[2][3]) + (m[0][3]);

	temp[1][0] = (m[1][0] * _mtx[0][0]) + (m[1][1] * _mtx[1][0]) + (m[1][2] * _mtx[2][0]);
	temp[1][1] = (m[1][0] * _mtx[0][1]) + (m[1][1] * _mtx[1][1]) + (m[1][2] * _mtx[2][1]);
	temp[1][2] = (m[1][0] * _mtx[0][2]) + (m[1][1] * _mtx[1][2]) + (m[1][2] * _mtx[2][2]);
	temp[1][3] = (m[1][0] * _mtx[0][3]) + (m[1][1] * _mtx[1][3]) + (m[1][2] * _mtx[2][3]) + (m[1][3]);

	temp[2][0] = (m[2][0] * _mtx[0][0]) + (m[2][1] * _mtx[1][0]) + (m[2][2] * _mtx[2][0]);
	temp[2][1] = (m[2][0] * _mtx[0][1]) + (m[2][1] * _mtx[1][1]) + (m[2][2] * _mtx[2][1]);
	temp[2][2] = (m[2][0] * _mtx[0][2]) + (m[2][1] * _mtx[1][2]) + (m[2][2] * _mtx[2][2]);
	temp[2][3] = (m[2][0] * _mtx[0][3]) + (m[2][1] * _mtx[1][3]) + (m[2][2] * _mtx[2][3]) + (m[2][3]);

	return temp;
}

///-------------------------------------------------------------------------------------------------
/// GetAxis
MTX_TEMPLATE_HEAD
typename MTX34_CLASS_HEAD::TVec3&	MTX34_CLASS_HEAD::GetAxisX(TVec3& _vec) const
{
	_vec.Set(m[0][0], m[1][0], m[2][0]);
	return _vec;
}
MTX_TEMPLATE_HEAD
typename MTX34_CLASS_HEAD::TVec3&	MTX34_CLASS_HEAD::GetAxisY(TVec3& _vec) const
{
	_vec.Set(m[0][1], m[1][1], m[2][1]);
	return _vec;
}
MTX_TEMPLATE_HEAD
typename MTX34_CLASS_HEAD::TVec3&	MTX34_CLASS_HEAD::GetAxisZ(TVec3& _vec) const
{
	_vec.Set(m[0][2], m[1][2], m[2][2]);
	return _vec;
}
MTX_TEMPLATE_HEAD
typename MTX34_CLASS_HEAD::TVec3&	MTX34_CLASS_HEAD::GetAxisW(TVec3& _vec) const
{
	_vec.Set(m[0][3], m[1][3], m[2][3]);
	return _vec;
}
MTX_TEMPLATE_HEAD
void	MTX34_CLASS_HEAD::GetAxis(TVec3& _x, TVec3& _y, TVec3& _z) const
{
	GetAxisX(_x);
	GetAxisY(_y);
	GetAxisZ(_z);
}
MTX_TEMPLATE_HEAD
void	MTX34_CLASS_HEAD::GetAxis(TVec3& _x, TVec3& _y, TVec3& _z, TVec3& _w) const
{
	GetAxisX(_x);
	GetAxisY(_y);
	GetAxisZ(_z);
	GetAxisW(_w);
}

///-------------------------------------------------------------------------------------------------
/// SetAxis
MTX_TEMPLATE_HEAD
void	MTX34_CLASS_HEAD::SetAxisX(const TVec3& _vec)
{
	m[0][0] = _vec.x, m[1][0] = _vec.y, m[2][0] = _vec.z;
}
MTX_TEMPLATE_HEAD
void	MTX34_CLASS_HEAD::SetAxisY(const TVec3& _vec)
{
	m[0][1] = _vec.x, m[1][1] = _vec.y, m[2][1] = _vec.z;
}
MTX_TEMPLATE_HEAD
void	MTX34_CLASS_HEAD::SetAxisZ(const TVec3& _vec)
{
	m[0][2] = _vec.x, m[1][2] = _vec.y, m[2][2] = _vec.z;
}
MTX_TEMPLATE_HEAD
void	MTX34_CLASS_HEAD::SetAxisW(const TVec3& _vec)
{
	m[0][3] = _vec.x, m[1][3] = _vec.y, m[2][3] = _vec.z;
}
MTX_TEMPLATE_HEAD
void	MTX34_CLASS_HEAD::SetAxis(const TVec3& _x, const TVec3& _y, const TVec3& _z)
{
	SetAxisX(_x);
	SetAxisY(_y);
	SetAxisZ(_z);
}
MTX_TEMPLATE_HEAD
void	MTX34_CLASS_HEAD::SetAxis(const TVec3& _x, const TVec3& _y, const TVec3& _z, const TVec3& _w)
{
	SetAxisX(_x);
	SetAxisY(_y);
	SetAxisZ(_z);
	SetAxisW(_w);
}

///-------------------------------------------------------------------------------------------------
/// GetTrans
MTX_TEMPLATE_HEAD
typename MTX34_CLASS_HEAD::TVec3&	MTX34_CLASS_HEAD::GetTrans(TVec3& _trans) const
{
	_trans.Set(m[0][3], m[1][3], m[2][3]);
	return _trans;
}
///-------------------------------------------------------------------------------------------------
/// SetTrans
MTX_TEMPLATE_HEAD
void	MTX34_CLASS_HEAD::SetTrans(const TVec3& _trans)
{
	m[0][3] = _trans.x, m[1][3] = _trans.y, m[2][3] = _trans.z;
}

///-------------------------------------------------------------------------------------------------
/// Get33
MTX_TEMPLATE_HEAD
typename MTX34_CLASS_HEAD::TMtx33&	MTX34_CLASS_HEAD::GetMtx33()
{
	return *PCast<TMtx33*>(this);
}

///-------------------------------------------------------------------------------------------------
// Set33
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	MTX34_CLASS_HEAD::SetMtx33(const TMtx33& _mtx33)
{
	m[0][0] = _mtx33[0][0], m[0][1] = _mtx33[0][1], m[0][2] = _mtx33[0][2];
	m[1][0] = _mtx33[1][0], m[1][1] = _mtx33[1][1], m[1][2] = _mtx33[1][2];
	m[2][0] = _mtx33[2][0], m[2][1] = _mtx33[2][1], m[2][2] = _mtx33[2][2];
	return *this;
}

///-------------------------------------------------------------------------------------------------
// Copy
MTX_TEMPLATE_HEAD
MTX34_CLASS_HEAD&	MTX34_CLASS_HEAD::Copy(const TMtx33& _mtx33)
{
	m[0][0] = _mtx33[0][0], m[0][1] = _mtx33[0][1], m[0][2] = _mtx33[0][2], m[0][3] = 0.0f;
	m[1][0] = _mtx33[1][0], m[1][1] = _mtx33[1][1], m[1][2] = _mtx33[1][2], m[1][3] = 0.0f;
	m[2][0] = _mtx33[2][0], m[2][1] = _mtx33[2][1], m[2][2] = _mtx33[2][2], m[2][3] = 0.0f;
	return *this;
}

///-------------------------------------------------------------------------------------------------
/// 定数値
MTX_TEMPLATE_HEAD
const MTX34_CLASS_HEAD	MTX34_CLASS_HEAD::Identity(
	1.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 1.0f, 0.0f
	);



///*************************************************************************************************
/// ４×４行列クラス
///*************************************************************************************************
///-------------------------------------------------------------------------------------------------
// コンストラクタ
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD::TMtx44()
{
	m[0][0]	= m[1][1] = m[2][2] = m[3][3] = 1.0f;
	m[0][1]	= m[0][2] = m[0][3] = m[1][0] = m[1][2] = m[1][3] = m[2][0]	= m[2][1] = m[2][3] = m[3][0] = m[3][1] = m[3][2] = 0.0f;
}
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD::TMtx44(
	FLOAT _00,FLOAT _01,FLOAT _02,FLOAT _03,
	FLOAT _10,FLOAT _11,FLOAT _12,FLOAT _13,
	FLOAT _20,FLOAT _21,FLOAT _22,FLOAT _23,
	FLOAT _30,FLOAT _31,FLOAT _32,FLOAT _33
	)
{
	m[0][0]	= _00, m[0][1]	= _01, m[0][2]	= _02, m[0][3]	= _03;
	m[1][0]	= _10, m[1][1]	= _11, m[1][2]	= _12, m[1][3]	= _13;
	m[2][0]	= _20, m[2][1]	= _21, m[2][2]	= _22, m[2][3]	= _23;
	m[3][0]	= _30, m[3][1]	= _31, m[3][2]	= _32, m[3][3]	= _33;
}

MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD::TMtx44(const FLOAT* _pF)
{
	m[0][0] = _pF[ 0], m[0][1] = _pF[ 1], m[0][2] = _pF[ 2], m[0][3] = _pF[ 3];
	m[1][0] = _pF[ 4], m[1][1] = _pF[ 5], m[1][2] = _pF[ 6], m[1][3] = _pF[ 7];
	m[2][0] = _pF[ 8], m[2][1] = _pF[ 9], m[2][2] = _pF[10], m[2][3] = _pF[11];
	m[3][0] = _pF[12], m[3][1] = _pF[13], m[3][2] = _pF[14], m[3][3] = _pF[15];
}
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD::TMtx44(const TVec4& _v0, const TVec4& _v1, const TVec4& _v2, const TVec4& _v3)
{
	v[0] = _v0; v[1] = _v1; v[2] = _v2; v[3] = _v3;
}
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD::TMtx44(const TVec4* _pV)
{
	v[0] = _pV[0]; v[1] = _pV[1]; v[2] = _pV[2]; v[3] = _pV[3];
}
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD::TMtx44(const TMtx33& _mtx33)
{
	Copy(_mtx33);
}
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD::TMtx44(const TMtx33& _mtx33, const TVec3& _trans)
{
	Copy(_mtx33);
	SetTrans(_trans);
}
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD::TMtx44(const TMtx34& _mtx34)
{
	Copy(_mtx34);
}
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD::TMtx44(const TVec3& _scale)
{
	m[0][0] = _scale[0];  m[1][1] = _scale[1];  m[2][2] = _scale[2];  m[3][3] = 1.0f;
	m[0][1] = m[0][2] = m[0][3] = m[1][0] = m[1][2] = m[1][3] = m[2][0] = m[2][1] = m[2][3] = m[3][0] = m[3][1] = m[3][2] = 0.0f;
}
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD::TMtx44(const TVec4& _scale)
{
	m[0][0] = _scale[0];  m[1][1] = _scale[1];  m[2][2] = _scale[2];  m[3][3] = _scale[3];
	m[0][1] = m[0][2] = m[0][3] = m[1][0] = m[1][2] = m[1][3] = m[2][0] = m[2][1] = m[2][3] = m[3][0] = m[3][1] = m[3][2] = 0.0f;
}

///-------------------------------------------------------------------------------------------------
/// compar operators
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	MTX44_CLASS_HEAD::operator=(const TMtx44& _mtx)
{
	m[0][0] = _mtx[0][0], m[0][1] = _mtx[0][1], m[0][2] = _mtx[0][2], m[0][3] = _mtx[0][3];
	m[1][0] = _mtx[1][0], m[1][1] = _mtx[1][1], m[1][2] = _mtx[1][2], m[1][3] = _mtx[1][3];
	m[2][0] = _mtx[2][0], m[2][1] = _mtx[2][1], m[2][2] = _mtx[2][2], m[2][3] = _mtx[2][3];
	m[3][0] = _mtx[3][0], m[3][1] = _mtx[3][1], m[3][2] = _mtx[3][2], m[3][3] = _mtx[3][3];
	return *this;
}
MTX_TEMPLATE_HEAD
b8					MTX44_CLASS_HEAD::operator==(const TMtx44& _mtx) const
{
	return (
		m[0][0] == _mtx[0][0] && m[0][1] == _mtx[0][1] && m[0][2] == _mtx[0][2] && m[0][3] == _mtx[0][3] &&
		m[1][0] == _mtx[1][0] && m[1][1] == _mtx[1][1] && m[1][2] == _mtx[1][2] && m[1][3] == _mtx[1][3] &&
		m[2][0] == _mtx[2][0] && m[2][1] == _mtx[2][1] && m[2][2] == _mtx[2][2] && m[2][3] == _mtx[2][3] &&
		m[3][0] == _mtx[3][0] && m[3][1] == _mtx[3][1] && m[3][2] == _mtx[3][2] && m[3][3] == _mtx[3][3]
		);
}
MTX_TEMPLATE_HEAD
b8					MTX44_CLASS_HEAD::operator!=(const TMtx44& _mtx) const
{
	return (
		m[0][0] != _mtx[0][0] || m[0][1] != _mtx[0][1] || m[0][2] != _mtx[0][2] || m[0][3] != _mtx[0][3] ||
		m[1][0] != _mtx[1][0] || m[1][1] != _mtx[1][1] || m[1][2] != _mtx[1][2] || m[1][3] != _mtx[1][3] ||
		m[2][0] != _mtx[2][0] || m[2][1] != _mtx[2][1] || m[2][2] != _mtx[2][2] || m[2][3] != _mtx[2][3] ||
		m[3][0] != _mtx[3][0] || m[3][1] != _mtx[3][1] || m[3][2] != _mtx[3][2] || m[3][3] != _mtx[3][3]
		);
}

///-------------------------------------------------------------------------------------------------
// assignment operators
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	MTX44_CLASS_HEAD::operator+=(const TMtx44& _mtx)
{
	m[0][0]	+= _mtx[0][0], m[0][1]	+= _mtx[0][1], m[0][2]	+= _mtx[0][2], m[0][3]	+= _mtx[0][3];
	m[1][0]	+= _mtx[1][0], m[1][1]	+= _mtx[1][1], m[1][2]	+= _mtx[1][2], m[1][3]	+= _mtx[1][3];
	m[2][0]	+= _mtx[2][0], m[2][1]	+= _mtx[2][1], m[2][2]	+= _mtx[2][2], m[2][3]	+= _mtx[2][3];
	m[3][0]	+= _mtx[3][0], m[3][1]	+= _mtx[3][1], m[3][2]	+= _mtx[3][2], m[3][3]	+= _mtx[3][3];
	return *this;
}

MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	MTX44_CLASS_HEAD::operator*=(const TMtx44& _mtx)
{
	TMtx44 temp;
	temp = *this;
	m[0][0] = (temp[0][0] * _mtx[0][0]) + (temp[0][1] * _mtx[1][0]) + (temp[0][2] * _mtx[2][0]) + (temp[0][3] * _mtx[3][0]);
	m[0][1] = (temp[0][0] * _mtx[0][1]) + (temp[0][1] * _mtx[1][1]) + (temp[0][2] * _mtx[2][1]) + (temp[0][3] * _mtx[3][1]);
	m[0][2] = (temp[0][0] * _mtx[0][2]) + (temp[0][1] * _mtx[1][2]) + (temp[0][2] * _mtx[2][2]) + (temp[0][3] * _mtx[3][2]);
	m[0][3] = (temp[0][0] * _mtx[0][3]) + (temp[0][1] * _mtx[1][3]) + (temp[0][2] * _mtx[2][3]) + (temp[0][3] * _mtx[3][3]);

	m[1][0] = (temp[1][0] * _mtx[0][0]) + (temp[1][1] * _mtx[1][0]) + (temp[1][2] * _mtx[2][0]) + (temp[1][3] * _mtx[3][0]);
	m[1][1] = (temp[1][0] * _mtx[0][1]) + (temp[1][1] * _mtx[1][1]) + (temp[1][2] * _mtx[2][1]) + (temp[1][3] * _mtx[3][1]);
	m[1][2] = (temp[1][0] * _mtx[0][2]) + (temp[1][1] * _mtx[1][2]) + (temp[1][2] * _mtx[2][2]) + (temp[1][3] * _mtx[3][2]);
	m[1][3] = (temp[1][0] * _mtx[0][3]) + (temp[1][1] * _mtx[1][3]) + (temp[1][2] * _mtx[2][3]) + (temp[1][3] * _mtx[3][3]);

	m[2][0] = (temp[2][0] * _mtx[0][0]) + (temp[2][1] * _mtx[1][0]) + (temp[2][2] * _mtx[2][0]) + (temp[2][3] * _mtx[3][0]);
	m[2][1] = (temp[2][0] * _mtx[0][1]) + (temp[2][1] * _mtx[1][1]) + (temp[2][2] * _mtx[2][1]) + (temp[2][3] * _mtx[3][1]);
	m[2][2] = (temp[2][0] * _mtx[0][2]) + (temp[2][1] * _mtx[1][2]) + (temp[2][2] * _mtx[2][2]) + (temp[2][3] * _mtx[3][2]);
	m[2][3] = (temp[2][0] * _mtx[0][3]) + (temp[2][1] * _mtx[1][3]) + (temp[2][2] * _mtx[2][3]) + (temp[2][3] * _mtx[3][3]);

	m[3][0] = (temp[3][0] * _mtx[0][0]) + (temp[3][1] * _mtx[1][0]) + (temp[3][2] * _mtx[2][0]) + (temp[3][3] * _mtx[3][0]);
	m[3][1] = (temp[3][0] * _mtx[0][1]) + (temp[3][1] * _mtx[1][1]) + (temp[3][2] * _mtx[2][1]) + (temp[3][3] * _mtx[3][1]);
	m[3][2] = (temp[3][0] * _mtx[0][2]) + (temp[3][1] * _mtx[1][2]) + (temp[3][2] * _mtx[2][2]) + (temp[3][3] * _mtx[3][2]);
	m[3][3] = (temp[3][0] * _mtx[0][3]) + (temp[3][1] * _mtx[1][3]) + (temp[3][2] * _mtx[2][3]) + (temp[3][3] * _mtx[3][3]);
	return *this;
}

///-------------------------------------------------------------------------------------------------
// scale operators
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	MTX44_CLASS_HEAD::operator*=(const TVec3& _vec)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0].GetXYZ() *= _vec;
	v[1].GetXYZ() *= _vec;
	v[2].GetXYZ() *= _vec;
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0].GetXYZ() *= _vec[0];
	v[1].GetXYZ() *= _vec[1];
	v[2].GetXYZ() *= _vec[2];
#endif
	return *this;
}

MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	MTX44_CLASS_HEAD::operator/=(const TVec3& _vec)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0].GetXYZ() /= _vec;
	v[1].GetXYZ() /= _vec;
	v[2].GetXYZ() /= _vec;
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0].GetXYZ() /= _vec[0];
	v[1].GetXYZ() /= _vec[1];
	v[2].GetXYZ() /= _vec[2];
#endif
	return *this;
}

MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	MTX44_CLASS_HEAD::operator*=(const TVec4& _vec)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0] *= _vec;
	v[1] *= _vec;
	v[2] *= _vec;
	v[3] *= _vec;
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0] *= _vec[0];
	v[1] *= _vec[1];
	v[2] *= _vec[2];
	v[3] *= _vec[3];
#endif
	return *this;
}

MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	MTX44_CLASS_HEAD::operator/=(const TVec4& _vec)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0] /= _vec;
	v[1] /= _vec;
	v[2] /= _vec;
	v[3] /= _vec;
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0] /= _vec[0];
	v[1] /= _vec[1];
	v[2] /= _vec[2];
	v[3] /= _vec[3];
#endif
	return *this;
}

MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	MTX44_CLASS_HEAD::operator*=(FLOAT _val)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0] *= TVec4(_val, 0.0f, 0.0f, 0.0f);
	v[1] *= TVec4(0.0f, _val, 0.0f, 0.0f);
	v[2] *= TVec4(0.0f, 0.0f, _val, 0.0f);
	v[3] *= TVec4(0.0f, 0.0f, 0.0f, _val);
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0] *= _val;
	v[1] *= _val;
	v[2] *= _val;
	v[3] *= _val;
#endif
	return *this;
}

MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	MTX44_CLASS_HEAD::operator/=(FLOAT _val)
{
#if defined( MATRIX_COLUMN_MAJOR )	// 列優先
	v[0] /= TVec4(_val, 0.0f, 0.0f, 0.0f);
	v[1] /= TVec4(0.0f, _val, 0.0f, 0.0f);
	v[2] /= TVec4(0.0f, 0.0f, _val, 0.0f);
	v[3] /= TVec4(0.0f, 0.0f, 0.0f, _val);
#elif defined( MATRIX_ROW_MAJOR )	// 行優先
	v[0] /= _val;
	v[1] /= _val;
	v[2] /= _val;
	v[3] /= _val;
#endif
	return *this;
}

///-------------------------------------------------------------------------------------------------
// binary operators
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD	MTX44_CLASS_HEAD::operator+(const TMtx44& _mtx) const
{
	TMtx44 temp;
	temp[0][0] = (m[0][0]+_mtx[0][0]), temp[0][1] = (m[0][1]+_mtx[0][1]), temp[0][2] = (m[0][2]+_mtx[0][2]), temp[0][3] = (m[0][3]+_mtx[0][3]);
	temp[1][0] = (m[1][0]+_mtx[1][0]), temp[1][1] = (m[1][1]+_mtx[1][1]), temp[1][2] = (m[1][2]+_mtx[1][2]), temp[1][3] = (m[1][3]+_mtx[1][3]);
	temp[2][0] = (m[2][0]+_mtx[2][0]), temp[2][1] = (m[2][1]+_mtx[2][1]), temp[2][2] = (m[2][2]+_mtx[2][2]), temp[2][3] = (m[2][3]+_mtx[2][3]);
	temp[3][0] = (m[3][0]+_mtx[3][0]), temp[3][1] = (m[3][1]+_mtx[3][1]), temp[3][2] = (m[3][2]+_mtx[3][2]), temp[3][3] = (m[3][3]+_mtx[3][3]);
	return temp;
}

MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD	MTX44_CLASS_HEAD::operator*(const TMtx44& _mtx) const
{
	TMtx44 temp;
	temp[0][0] = (m[0][0] * _mtx[0][0]) + (m[0][1] * _mtx[1][0]) + (m[0][2] * _mtx[2][0]) + (m[0][3] * _mtx[3][0]);
	temp[0][1] = (m[0][0] * _mtx[0][1]) + (m[0][1] * _mtx[1][1]) + (m[0][2] * _mtx[2][1]) + (m[0][3] * _mtx[3][1]);
	temp[0][2] = (m[0][0] * _mtx[0][2]) + (m[0][1] * _mtx[1][2]) + (m[0][2] * _mtx[2][2]) + (m[0][3] * _mtx[3][2]);
	temp[0][3] = (m[0][0] * _mtx[0][3]) + (m[0][1] * _mtx[1][3]) + (m[0][2] * _mtx[2][3]) + (m[0][3] * _mtx[3][3]);

	temp[1][0] = (m[1][0] * _mtx[0][0]) + (m[1][1] * _mtx[1][0]) + (m[1][2] * _mtx[2][0]) + (m[1][3] * _mtx[3][0]);
	temp[1][1] = (m[1][0] * _mtx[0][1]) + (m[1][1] * _mtx[1][1]) + (m[1][2] * _mtx[2][1]) + (m[1][3] * _mtx[3][1]);
	temp[1][2] = (m[1][0] * _mtx[0][2]) + (m[1][1] * _mtx[1][2]) + (m[1][2] * _mtx[2][2]) + (m[1][3] * _mtx[3][2]);
	temp[1][3] = (m[1][0] * _mtx[0][3]) + (m[1][1] * _mtx[1][3]) + (m[1][2] * _mtx[2][3]) + (m[1][3] * _mtx[3][3]);

	temp[2][0] = (m[2][0] * _mtx[0][0]) + (m[2][1] * _mtx[1][0]) + (m[2][2] * _mtx[2][0]) + (m[2][3] * _mtx[3][0]);
	temp[2][1] = (m[2][0] * _mtx[0][1]) + (m[2][1] * _mtx[1][1]) + (m[2][2] * _mtx[2][1]) + (m[2][3] * _mtx[3][1]);
	temp[2][2] = (m[2][0] * _mtx[0][2]) + (m[2][1] * _mtx[1][2]) + (m[2][2] * _mtx[2][2]) + (m[2][3] * _mtx[3][2]);
	temp[2][3] = (m[2][0] * _mtx[0][3]) + (m[2][1] * _mtx[1][3]) + (m[2][2] * _mtx[2][3]) + (m[2][3] * _mtx[3][3]);

	temp[3][0] = (m[3][0] * _mtx[0][0]) + (m[3][1] * _mtx[1][0]) + (m[3][2] * _mtx[2][0]) + (m[3][3] * _mtx[3][0]);
	temp[3][1] = (m[3][0] * _mtx[0][1]) + (m[3][1] * _mtx[1][1]) + (m[3][2] * _mtx[2][1]) + (m[3][3] * _mtx[3][1]);
	temp[3][2] = (m[3][0] * _mtx[0][2]) + (m[3][1] * _mtx[1][2]) + (m[3][2] * _mtx[2][2]) + (m[3][3] * _mtx[3][2]);
	temp[3][3] = (m[3][0] * _mtx[0][3]) + (m[3][1] * _mtx[1][3]) + (m[3][2] * _mtx[2][3]) + (m[3][3] * _mtx[3][3]);
	return temp;
}

MTX_TEMPLATE_HEAD
typename MTX44_CLASS_HEAD::TVec3	MTX44_CLASS_HEAD::operator*(const TVec3& _vec) const
{
	TVec3 temp;
	temp.x = (_vec.x*m[0][0]) + (_vec.y*m[0][1]) + (_vec.z*m[0][2]) + m[0][3];
	temp.y = (_vec.x*m[1][0]) + (_vec.y*m[1][1]) + (_vec.z*m[1][2]) + m[1][3];
	temp.z = (_vec.x*m[2][0]) + (_vec.y*m[2][1]) + (_vec.z*m[2][2]) + m[2][3];
	return temp;
}

MTX_TEMPLATE_HEAD
typename MTX44_CLASS_HEAD::TVec4	MTX44_CLASS_HEAD::operator*(const TVec4& _vec) const
{
	TVec4 temp;
	temp.x = (_vec.x*m[0][0]) + (_vec.y*m[0][1]) + (_vec.z*m[0][2]) + (_vec.w*m[0][3]);
	temp.y = (_vec.x*m[1][0]) + (_vec.y*m[1][1]) + (_vec.z*m[1][2]) + (_vec.w*m[1][3]);
	temp.z = (_vec.x*m[2][0]) + (_vec.y*m[2][1]) + (_vec.z*m[2][2]) + (_vec.w*m[2][3]);
	temp.w = (_vec.x*m[3][0]) + (_vec.y*m[3][1]) + (_vec.z*m[3][2]) + (_vec.w*m[3][3]);
	return temp;
}


///-------------------------------------------------------------------------------------------------
/// GetAxis
MTX_TEMPLATE_HEAD
typename MTX44_CLASS_HEAD::TVec4&	MTX44_CLASS_HEAD::GetAxisX(TVec4& _vec) const
{
	_vec.Set(m[0][0], m[1][0], m[2][0], m[3][0]);
	return _vec;
}
MTX_TEMPLATE_HEAD
typename MTX44_CLASS_HEAD::TVec4&	MTX44_CLASS_HEAD::GetAxisY(TVec4& _vec) const
{
	_vec.Set(m[0][1], m[1][1], m[2][1], m[3][1]);
	return _vec;
}
MTX_TEMPLATE_HEAD
typename MTX44_CLASS_HEAD::TVec4&	MTX44_CLASS_HEAD::GetAxisZ(TVec4& _vec) const
{
	_vec.Set(m[0][2], m[1][2], m[2][2], m[3][2]);
	return _vec;
}
MTX_TEMPLATE_HEAD
typename MTX44_CLASS_HEAD::TVec4&	MTX44_CLASS_HEAD::GetAxisW(TVec4& _vec) const
{
	_vec.Set(m[0][3], m[1][3], m[2][3], m[3][3]);
	return _vec;
}
MTX_TEMPLATE_HEAD
void	MTX44_CLASS_HEAD::GetAxis(TVec4& _x, TVec4& _y, TVec4& _z, TVec4& _w) const
{
	GetAxisX(_x);
	GetAxisY(_y);
	GetAxisZ(_z);
	GetAxisW(_w);
}

///-------------------------------------------------------------------------------------------------
/// SetAxis
MTX_TEMPLATE_HEAD
void	MTX44_CLASS_HEAD::SetAxisX(const TVec4& _vec)
{
	m[0][0] = _vec.x, m[1][0] = _vec.y, m[2][0] = _vec.z, m[3][0] = _vec.w;
}
MTX_TEMPLATE_HEAD
void	MTX44_CLASS_HEAD::SetAxisY(const TVec4& _vec)
{
	m[0][1] = _vec.x, m[1][1] = _vec.y, m[2][1] = _vec.z, m[3][1] = _vec.w;
}
MTX_TEMPLATE_HEAD
void	MTX44_CLASS_HEAD::SetAxisZ(const TVec4& _vec)
{
	m[0][2] = _vec.x, m[1][2] = _vec.y, m[2][2] = _vec.z, m[3][2] = _vec.w;
}
MTX_TEMPLATE_HEAD
void	MTX44_CLASS_HEAD::SetAxisW(const TVec4& _vec)
{
	m[0][3] = _vec.x, m[1][3] = _vec.y, m[2][3] = _vec.z, m[3][3] = _vec.w;
}
MTX_TEMPLATE_HEAD
void	MTX44_CLASS_HEAD::SetAxis(const TVec4& _x, const TVec4& _y, const TVec4& _z, const TVec4& _w)
{
	SetAxisX(_x);
	SetAxisY(_y);
	SetAxisZ(_z);
	SetAxisW(_w);
}

///-------------------------------------------------------------------------------------------------
/// GetTrans
MTX_TEMPLATE_HEAD
typename MTX44_CLASS_HEAD::TVec3&	MTX44_CLASS_HEAD::GetTrans(TVec3& _trans) const
{
	_trans.Set(m[0][3], m[1][3], m[2][3]);
	return _trans;
}
MTX_TEMPLATE_HEAD
typename MTX44_CLASS_HEAD::TVec4&	MTX44_CLASS_HEAD::GetTrans(TVec4& _trans) const
{
	_trans.Set(m[0][3], m[1][3], m[2][3], m[3][3]);
	return _trans;
}

///-------------------------------------------------------------------------------------------------
/// SetTrans
MTX_TEMPLATE_HEAD
void	MTX44_CLASS_HEAD::SetTrans(const TVec3& _trans)
{
	m[0][3] = _trans.x, m[1][3] = _trans.y, m[2][3] = _trans.z;
}
MTX_TEMPLATE_HEAD
void	MTX44_CLASS_HEAD::SetTrans(const TVec4& _trans)
{
	m[0][3] = _trans.x, m[1][3] = _trans.y, m[2][3] = _trans.z, m[3][3] = _trans.w;
}

///-------------------------------------------------------------------------------------------------
/// Get33
MTX_TEMPLATE_HEAD
typename MTX44_CLASS_HEAD::TMtx33&	MTX44_CLASS_HEAD::GetMtx33()
{
	return *PCast<TMtx33*>(this);
}

///-------------------------------------------------------------------------------------------------
/// Get34
MTX_TEMPLATE_HEAD
typename MTX44_CLASS_HEAD::TMtx34&	MTX44_CLASS_HEAD::GetMtx34()
{
	return *PCast<TMtx34*>(this);
}

///-------------------------------------------------------------------------------------------------
// Set
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	MTX44_CLASS_HEAD::SetMtx33(const TMtx33& _mtx33)
{
	m[0][0] = _mtx33[0][0], m[0][1] = _mtx33[0][1], m[0][2] = _mtx33[0][2];
	m[1][0] = _mtx33[1][0], m[1][1] = _mtx33[1][1], m[1][2] = _mtx33[1][2];
	m[2][0] = _mtx33[2][0], m[2][1] = _mtx33[2][1], m[2][2] = _mtx33[2][2];
	return *this;
}
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	MTX44_CLASS_HEAD::SetMtx34(const TMtx34& _mtx34)
{
	m[0][0] = _mtx34[0][0], m[0][1] = _mtx34[0][1], m[0][2] = _mtx34[0][2], m[0][3] = _mtx34[0][3];
	m[1][0] = _mtx34[1][0], m[1][1] = _mtx34[1][1], m[1][2] = _mtx34[1][2], m[1][3] = _mtx34[1][3];
	m[2][0] = _mtx34[2][0], m[2][1] = _mtx34[2][1], m[2][2] = _mtx34[2][2], m[2][3] = _mtx34[2][3];
	return *this;
}

///-------------------------------------------------------------------------------------------------
// Copy
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	MTX44_CLASS_HEAD::Copy(const TMtx33& _mtx33)
{
	m[0][0] = _mtx33[0][0], m[0][1] = _mtx33[0][1], m[0][2] = _mtx33[0][2], m[0][3] = 0.0f;
	m[1][0] = _mtx33[1][0], m[1][1] = _mtx33[1][1], m[1][2] = _mtx33[1][2], m[1][3] = 0.0f;
	m[2][0] = _mtx33[2][0], m[2][1] = _mtx33[2][1], m[2][2] = _mtx33[2][2], m[2][3] = 0.0f;
	m[3][0] = 0.0f,			m[3][1] = 0.0f,			m[3][2] = 0.0f,			m[3][3] = 1.0f;
	return *this;
}
MTX_TEMPLATE_HEAD
MTX44_CLASS_HEAD&	MTX44_CLASS_HEAD::Copy(const TMtx34& _mtx34)
{
	m[0][0] = _mtx34[0][0], m[0][1] = _mtx34[0][1], m[0][2] = _mtx34[0][2], m[0][3] = _mtx34[0][3];
	m[1][0] = _mtx34[1][0], m[1][1] = _mtx34[1][1], m[1][2] = _mtx34[1][2], m[1][3] = _mtx34[1][3];
	m[2][0] = _mtx34[2][0], m[2][1] = _mtx34[2][1], m[2][2] = _mtx34[2][2], m[2][3] = _mtx34[2][3];
	m[3][0] = 0.0f,			m[3][1] = 0.0f,			m[3][2] = 0.0f,			m[3][3] = 1.0f;
	return *this;
}

///-------------------------------------------------------------------------------------------------
/// 定数値
MTX_TEMPLATE_HEAD
const MTX44_CLASS_HEAD	MTX44_CLASS_HEAD::Identity(
	1.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f
	);




POISON_END

#endif	// __MATRIX_INL__

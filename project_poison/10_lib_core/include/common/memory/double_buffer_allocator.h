﻿//#pragma once
#ifndef __DOUBLE_BUFFER_ALLOCATOR_H__
#define __DOUBLE_BUFFER_ALLOCATOR_H__


//-------------------------------------------------------------------------------------------------
// include
#include "memory/allocator.h"


POISON_BGN

//-------------------------------------------------------------------------------------------------
//@brief	ダブルバッファメモリ
//@noto
// 主に並列で動作する描画などの一時的なバッファとして利用可能なメモリ。\n
// スタック式で、メモリ確保を順番に行っていき、個別解放はできない。\n
// 一つのバッファを2フレーム分のメモリ領域としてとらえ、\n
// Update関数を呼び出すごとに1フレーム目2フレーム目とバッファを切り替える。\n
// 1フレーム目で確保されたメモリは次のフレームまではメモリ領域が保障され、\n
// 2回目のUpdate関数の呼び出し(3フレーム目の開始)のときに保障されなくなる(3フレーム目が領域を上書きして使う)\n
// Updateを呼ぶごとにメモリを切り替えていき、2フレーム前の領域は自動で再利用していくメモリ構成となる。\n
class CDoubleBufferAllocatorBase
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ・デストラクタ
	CDoubleBufferAllocatorBase();
	~CDoubleBufferAllocatorBase();

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	//@return	初期化成功、失敗
	//@param[in]	_pBuf	管理するメモリ領域
	//@param[in]	_size	管理するバッファサイズ
	b8		Initialize(void* _pBuf, u32 _size);

	//-------------------------------------------------------------------------------------------------
	//@brief	メモリ更新、バッファ切り替え呼び出し
	void	Update();

	//-------------------------------------------------------------------------------------------------
	//@brief	終了処理
	void	Finalize();
	
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	メモリ確保
	//@return	確保したメモリのポインタ、失敗ならNULL
	//@param[in]	_param	Mallocパラメータ
	void*	Malloc(const MemoryMallocParam& _param);
	
	//-------------------------------------------------------------------------------------------------
	//@brief	メモリ破棄
	///@note
	/// 解放できないため何もしない。
	u32		Free(void* _ptr) { return 0; };

public:
	//-------------------------------------------------------------------------------------------------
	///@brief	スタック開始
	///@note
	/// 何もしない。
	void*	StackBegin(const MemoryStackBeginParam& _param) { return 0; }

	//-------------------------------------------------------------------------------------------------
	///@brief	親メモリーを指定してスタック開始
	///@note
	/// 何もしない。
	b8		StackParentBegin(void* _parent, const MemoryStackBeginParam& _param) { return false; }

	//-------------------------------------------------------------------------------------------------
	///@brief	スタック終了
	///@note
	/// 何もしない。
	u32		StackEnd() { return 0; }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	バッファ取得
	const void* GetBuffer() const { return m_pBuf; }
	//@brief	バッファサイズ取得
	const u32	GetSize() const { return m_size; }
	//@brief	メモリの全体容量取得
	u32		GetTotalSize() const;
	//@brief	メモリの使用中容量取得
	u32		GetUsedSize() const;
	//@brief	メモリの取得数取得
	u32		GetUsedCount() const;
	//@brief	メモリの全体の空き容量取得
	u32		GetTotalRemainSize() const;
	//@brief	メモリの一番大きい連続領域の空き容量取得
	u32		GetMaxRemainSize(const u32 _align) const;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	現在のスタックサイズ取得
	u32		GetStackTotalSize() const { return 0; }
	//@brief	現在のスタックの使用サイズ
	u32		GetStackUsedSize() const { return 0; }
	//@brief	現在のスタックの取得数取得
	u32		GetStackUsedCount() const { return 0; }

private:
	//-------------------------------------------------------------------------------------------------
	//@brief	バッファ位置取得
	inline void* GetBuffer(u32 _idx) const { return PCast<u8*>(m_pBuf) + _idx; }

	//-------------------------------------------------------------------------------------------------
	//@brief	バッファ位置取得
	//inline u32	GetBufferIdx(void* _ptr) const { return SCast<u32>(PCast<u8*>(_ptr) - PCast<u8*>(m_pBuf)); }
	inline u32	GetBufferIdx(void* _ptr) const { return SCast<u32>((u64)(_ptr) - (u64)(m_pBuf)); }

	//-------------------------------------------------------------------------------------------------
	//@brief	アライメントした結果のインデックスを返す
	inline u32	GetAlignIdx(u32 _idx, u32 _align) const { return GetBufferIdx((void*)(ALIGN_PTR(GetBuffer(_idx), _align))); }

	//-------------------------------------------------------------------------------------------------
	//@brief	アライメントを考慮したフリーサイズ取得
	inline u32	GetFreeBufferSize(u32 _align) const
	{
		if (m_uEndIndex > GetAlignIdx(m_uBgnIndex, _align))
		{
			return m_uEndIndex - GetAlignIdx(m_uBgnIndex, _align);
		}
		return 0;
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	前方メモリ確保
	void*	BeginMalloc(size_t _size, u32 _align);
	
	//-------------------------------------------------------------------------------------------------
	//@brief	後方メモリ確保
	void*	EndMalloc(size_t _size, u32 _align);

private:
	void*	m_pBuf = NULL;		//< バッファ
	u32		m_size = 0;			//< バッファサイズ

	u32		m_uBgnIndex = 0;	//< メモリ前方確保位置
	u32		m_uEndIndex = 0;	//< メモリ後方確保位置
	b8		m_isBgn = false;

	u32		m_uUsedSize = 0;	//< 使用サイズ
	u32		m_uPrevUsedSize = 0;//< 前回使用サイズ
	u32		m_uNowCount = 0;	//< 確保数
	u32		m_uPrevCount = 0;	//< 前回確保数
};

// 型宣言
#ifdef POISON_RELEASE
typedef	TAllocator<CDoubleBufferAllocatorBase, CNoLock, CMemNoDebug, false>	CDoubleBufferAllocator;
#else
typedef	TAllocator<CDoubleBufferAllocatorBase, CNoLock, CMemDirtyDebug, false>	CDoubleBufferAllocator;
#endif // POISON_RELEASE


POISON_END

#endif  // __DOUBLE_BUFFER_ALLOCATOR_H__

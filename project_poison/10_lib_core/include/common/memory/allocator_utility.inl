﻿//#pragma once
#ifndef __ALLOCATOR_UTILITY_INL__
#define __ALLOCATOR_UTILITY_INL__


POISON_BGN

///-------------------------------------------------------------------------------------------------
/// メモリ確保
inline void*		Malloc(MEM_HANDLE _hdl, const MemoryMallocParam& _param)
{
	return CAllocatorUtility::Malloc(_hdl, _param);
}

///-------------------------------------------------------------------------------------------------
/// メモリ確保(バッファ０クリア)
inline void*		Calloc(MEM_HANDLE _hdl, const MemoryMallocParam& _param)
{
	void* p = CAllocatorUtility::Malloc(_hdl, _param);
	memset(p, 0, _param.size);
	return p;
}

///-------------------------------------------------------------------------------------------------
/// メモリ解放
inline u32			Free(MEM_HANDLE  _hdl, void* _p)
{
	return CAllocatorUtility::Free(_hdl, _p);
}

///-------------------------------------------------------------------------------------------------
/// メモリスタック開始
inline void*		StackBegin(MEM_HANDLE _hdl, const MemoryStackBeginParam& _param)
{
	return CAllocatorUtility::StackBegin(_hdl, _param);
}

///-------------------------------------------------------------------------------------------------
/// メモリスタック終了
inline u32			StackEnd(MEM_HANDLE _hdl)
{
	return CAllocatorUtility::StackEnd(_hdl);
}

///-------------------------------------------------------------------------------------------------
/// アロケータ取得
inline IAllocator*	GetAllocator(MEM_HANDLE _hdl)
{
	return CAllocatorUtility::GetAllocator(_hdl);
}

POISON_END

///-------------------------------------------------------------------------------------------------
/// アロケータテンプレートnew
inline void*	operator new(size_t _size, POISON::MEM_HANDLE _hdl, const POISON::MemoryNewParam& _param) MEM_THROW()
{
	void* ptr = POISON::CAllocatorUtility::Malloc(_hdl, MEM_NEW_PARAM_TO_MALLOC(_size, _param));
	return ptr;
}

///-------------------------------------------------------------------------------------------------
/// テンプレートアロケータdelete
template <class T>
inline void		operator delete(void* _p, POISON::MEM_HANDLE _hdl, const POISON::MemoryNewParam& _param, T* _pT) MEM_THROW()
{
	(void)_param;
	AssertBase(_p);
	_pT->~T();
	POISON::CAllocatorUtility::Free(_hdl, _p);
}

///-------------------------------------------------------------------------------------------------
/// Warning対策用delete
inline void		operator delete(void* _p, POISON::MEM_HANDLE _hdl, const POISON::MemoryNewParam& _param) MEM_THROW()
{
	(void)_param;
	POISON::CAllocatorUtility::Free(_hdl, _p);
}


///-------------------------------------------------------------------------------------------------
/// 普通のnew deleteは禁止します
#if 0
void*	operator new(size_t _size) MEM_THROW();
void	operator delete(void* _p) MEM_THROW();
void*	operator new[](size_t _size) MEM_THROW();
void	operator delete[](void* _p) MEM_THROW();
#endif



#endif	// __ALLOCATOR_UTILITY_INL__

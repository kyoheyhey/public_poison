﻿//#pragma once
#ifndef __ALLOCATOR_UTILITY_H__
#define __ALLOCATOR_UTILITY_H__

#include "memory/allocator.h"


POISON_BGN

//	メモリハンドル
typedef u32	MEM_HANDLE;
//	無効メモリＩＤ
static MEM_HANDLE	MEM_HDL_INVALID = 0;


//*************************************************************************************************
//@brief	アロケータユーティリティクラス
//*************************************************************************************************
class CAllocatorUtility
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	セットアップ
	static void		Initialize(IAllocator* _ppAllocator[], u32 _num);

	//-------------------------------------------------------------------------------------------------
	//@brief	生成
	static void*	Malloc(MEM_HANDLE _hdl, const MemoryMallocParam& _param);

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄
	static u32		Free(MEM_HANDLE _hdl, void* _ptr);

	//-------------------------------------------------------------------------------------------------
	//@brief	スタック開始
	static void*	StackBegin(MEM_HANDLE _hdl, const MemoryStackBeginParam& _param);

	//-------------------------------------------------------------------------------------------------
	//@brief	スタック終了
	static u32		StackEnd(MEM_HANDLE _hdl);

	//-------------------------------------------------------------------------------------------------
	//@brief	アロケータ取得
	static IAllocator*		GetAllocator(MEM_HANDLE _hdl);

private:
	static IAllocator**		s_allocatorList;	//< アロケータリスト
	static u32				s_allocatorNum;		//< アロケータ数
};


//-------------------------------------------------------------------------------------------------
//@brief	メモリハンドルとアロケータの手順の違いを吸収するMalloc関数
inline void*		Malloc(MEM_HANDLE _hdl, const MemoryMallocParam& _param);

//@brief	メモリハンドルとアロケータの手順の違いを吸収するcalloc関数
inline void*		Calloc(MEM_HANDLE  _hdl, const MemoryMallocParam& _param);

//@brief	メモリハンドルとアロケータの手順の違いを吸収するFree関数
inline u32			Free(MEM_HANDLE  _hdl, void* _p);

//@brief	メモリハンドルとアロケータの手順の違いを吸収するStackBegin関数
inline void*		StackBegin(MEM_HANDLE _hdl, const MemoryStackBeginParam& _param);

//@brief	メモリハンドルとアロケータの手順の違いを吸収するStackEnd関数
inline u32			StackEnd(MEM_HANDLE _hdl);

//@brief	メモリハンドルとアロケータの手順の違いを吸収するGetAllocator関数
inline IAllocator*	GetAllocator(MEM_HANDLE _hdl);


POISON_END

#include "allocator_utility.inl"


#endif	// __ALLOCATOR_UTILITY_H__

﻿//#pragma once
#ifndef __ALLOCATOR_INL__
#define __ALLOCATOR_INL__


POISON_BGN

///-------------------------------------------------------------------------------------------------
/// メモリ確保
inline void*	Malloc(IAllocator* _pAllocator, const MemoryMallocParam& _param)
{
	return _pAllocator->Malloc(_param);
}

///-------------------------------------------------------------------------------------------------
/// メモリ確保(バッファ０クリア)
inline void*	Calloc(IAllocator*  _pAllocator, const MemoryMallocParam& _param)
{
	void* p = _pAllocator->Malloc(_param);
	memset(p, 0, _param.size);
	return p;
}

///-------------------------------------------------------------------------------------------------
/// メモリ解放
inline u32		Free(IAllocator*  _pAllocator, void* _p)
{
	return _pAllocator->Free(_p);
}

///-------------------------------------------------------------------------------------------------
/// メモリスタック開始
inline void*	StackBegin(IAllocator* _pAllocator, const MemoryStackBeginParam& _param)
{
	return _pAllocator->StackBegin(_param);
}

///-------------------------------------------------------------------------------------------------
/// メモリスタック終了
inline u32		StackEnd(IAllocator* _pMem)
{
	return _pMem->StackEnd();
}

POISON_END

///-------------------------------------------------------------------------------------------------
/// アロケータテンプレートnew
inline void*	operator new(size_t _size, POISON::IAllocator* _pAllocator, const POISON::MemoryNewParam& _param) MEM_THROW()
{
	return _pAllocator->Malloc(MEM_NEW_PARAM_TO_MALLOC(_size, _param));
}

///-------------------------------------------------------------------------------------------------
/// テンプレートアロケータdelete
template <class T>
inline void		operator delete(void* _ptr, POISON::IAllocator* _pAllocator, const POISON::MemoryNewParam& _param, T* _pT) MEM_THROW()
{
	(void)_param;
	AssertBase(_ptr && _pAllocator);
	_pT->~T();
	_pAllocator->Free(_ptr);
}

///-------------------------------------------------------------------------------------------------
// Warning対策用delete
inline void		operator delete(void* _ptr, POISON::IAllocator* _pAllocator, const POISON::MemoryNewParam& _param) MEM_THROW()
{
	(void)_param;
	_pAllocator->Free(_ptr);
}




POISON_BGN

///*************************************************************************************************
/// テンプレートアロケータ
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// 初期化
template < class MEMORY, class LOCK, class DEBUG, bool ENABLE_STACK >
b8		TAllocator<MEMORY, LOCK, DEBUG, ENABLE_STACK>::Initialize(void* _pBuf, u32 _size)
{
	return m_memory.Initialize(_pBuf, _size);
}

///-------------------------------------------------------------------------------------------------
/// 更新処理
template < class MEMORY, class LOCK, class DEBUG, bool ENABLE_STACK >
void	TAllocator<MEMORY, LOCK, DEBUG, ENABLE_STACK>::Update()
{
	m_lock.Lock();
	m_memory.Update();
	m_lock.Unlock();
}

///-------------------------------------------------------------------------------------------------
/// 後処理
template < class MEMORY, class LOCK, class DEBUG, bool ENABLE_STACK >
void	TAllocator<MEMORY, LOCK, DEBUG, ENABLE_STACK>::Finalize()
{
	m_memory.Finalize();
}


///-------------------------------------------------------------------------------------------------
/// ロック
template < class MEMORY, class LOCK, class DEBUG, bool ENABLE_STACK >
void	TAllocator<MEMORY, LOCK, DEBUG, ENABLE_STACK>::Lock()
{
	m_lock.Lock();
}

///-------------------------------------------------------------------------------------------------
/// アンロック
template < class MEMORY, class LOCK, class DEBUG, bool ENABLE_STACK >
void	TAllocator<MEMORY, LOCK, DEBUG, ENABLE_STACK>::Unlock()
{
	m_lock.Unlock();
}


///-------------------------------------------------------------------------------------------------
/// メモリ確保
template < class MEMORY, class LOCK, class DEBUG, bool ENABLE_STACK >
void*	TAllocator<MEMORY, LOCK, DEBUG, ENABLE_STACK>::Malloc(const MemoryMallocParam& _param)
{
	// 確保サイズが０の時はスルーでいいかぁ
	if (_param.size <= 0) { return NULL; }
	// ベースアライメントの保障
	MemoryMallocParam param = _param;
	if (param.align < GetAlignBase()) { param.align = GetAlignBase(); }

	m_lock.Lock();		// ロック
	// メモリ確保
	void* p = m_memory.Malloc(param);
	if (IsStackMode() == false)
	{
		// 仕様サイズの最大値更新
		m_debug.UpdateMaxUsed(m_memory.GetUsedSize(), m_memory.GetUsedCount());
	}
	m_lock.Unlock();	// アンロック

	// デバッグ処理(メモリ汚しなど)
	m_debug.Malloc(p, param.size);

	return p;
}

///-------------------------------------------------------------------------------------------------
/// メモリ破棄
template < class MEMORY, class LOCK, class DEBUG, bool ENABLE_STACK >
u32		TAllocator<MEMORY, LOCK, DEBUG, ENABLE_STACK>::Free(void* _ptr)
{
	m_lock.Lock();		// ロック

	// メモリ確保
	u32 size = m_memory.Free(_ptr);
	// デバッグ処理(メモリ汚しなど)
	m_debug.Free(_ptr, size);

	m_lock.Unlock();	// アンロック
	return size;
}


///-------------------------------------------------------------------------------------------------
/// スタック開始
template < class MEMORY, class LOCK, class DEBUG, bool ENABLE_STACK >
void*	TAllocator<MEMORY, LOCK, DEBUG, ENABLE_STACK>::StackBegin(const MemoryStackBeginParam& _param)
{
	libAssert(IsEnableStack());

	m_lock.Lock();
	libAssert(IsStackMode() == false);
	void* ret = m_memory.StackBegin(_param);
	SetStackMode(ret != NULL);
	if (!IsStackMode()) { m_lock.Unlock(); } // 失敗したらロック解除
	return ret;
}

///-------------------------------------------------------------------------------------------------
/// 親メモリーを指定してスタック開始
template < class MEMORY, class LOCK, class DEBUG, bool ENABLE_STACK >
b8		TAllocator<MEMORY, LOCK, DEBUG, ENABLE_STACK>::StackParentBegin(void* _parent, const MemoryStackBeginParam& _param)
{
	libAssert(IsEnableStack());

	m_lock.Lock();
	libAssert(IsStackMode() == false);
	if (!m_memory.StackParentBegin(_parent, _param))
	{
		SetStackMode(false);
		m_lock.Unlock();	// 失敗したらロック解除
		return false;
	}
	SetStackMode(true);
	return true;
}

///-------------------------------------------------------------------------------------------------
/// スタック終了
template < class MEMORY, class LOCK, class DEBUG, bool ENABLE_STACK >
u32		TAllocator<MEMORY, LOCK, DEBUG, ENABLE_STACK>::StackEnd()
{
	libAssert(IsEnableStack());

	libAssert(IsStackMode() != false);
	u32 ret = m_memory.StackEnd();
	SetStackMode(false);
	m_lock.Unlock();
	// 仕様サイズの最大値更新
	m_debug.UpdateMaxUsed(m_memory.GetUsedSize(), m_memory.GetUsedCount());
	return ret;
}


POISON_END

#endif	// __ALLOCATOR_INL__

﻿//#pragma once
#ifndef __BOUNDARY_TAG_ALLOCATOR_H__
#define __BOUNDARY_TAG_ALLOCATOR_H__


//-------------------------------------------------------------------------------------------------
// include
#include "memory/allocator.h"

POISON_BGN

///*************************************************************************************************
//@brief	バウンダリータグメモリ
//@noto
// 可変長で汎用的に利用可能なメモリ。\n
// |u32[ヘッダー]|b8[使用フラグ]|..アライメント!..|u8[ヘッダーオフセット]|..実データ！..|u32[フッダー]|\n
///*************************************************************************************************
class CBoundaryTagAllocatorBase
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ・デストラクタ
	CBoundaryTagAllocatorBase();
	~CBoundaryTagAllocatorBase();

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	//@return   初期化成功、失敗
	//@param[in]	_pBuf	管理するメモリ領域
	//@param[in]	_size	管理するバッファサイズ
	b8		Initialize(void* _pBuf, u32 _size);

	//-------------------------------------------------------------------------------------------------
	//@brief	メモリ更新、何もしない
	void	Update() {};

	//-------------------------------------------------------------------------------------------------
	//@brief	終了処理
	void	Finalize();

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	メモリ確保
	//@return	確保したメモリのポインタ、失敗ならNULL
	//@param[in]	_param	Mallocパラメータ
	void*	Malloc(const MemoryMallocParam& _param);

	//-------------------------------------------------------------------------------------------------
	//@brief	メモリ破棄
	//@return	戻り値で解放したサイズを返す(わからない場合、失敗した場合は0)
	//@param[in]	_ptr	解放ポインタ
	u32		Free(void* _ptr);

public:
	//-------------------------------------------------------------------------------------------------
	///@brief	スタック開始
	///@return	スタック生成が成功した場合は解放のためのポインタが帰る
	///@param[in]	_param	StackBeginパラメータ
	void*	StackBegin(const MemoryStackBeginParam& _param);

	//-------------------------------------------------------------------------------------------------
	///@brief	親メモリーを指定してスタック開始
	///@return	スタック生成が成功した場合はTRUEが返る
	///@param[in]	_parent		親のポインタ
	///@param[in]	_param		StackBeginパラメータ
	b8		StackParentBegin(void* _parent, const MemoryStackBeginParam& _param);

	//-------------------------------------------------------------------------------------------------
	///@brief	スタック終了
	///@return	スタックで確保した合計値のサイズ
	u32		StackEnd();

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	バッファ取得
	const void* GetBuffer() const { return m_pBuf; }
	//@brief	バッファサイズ取得
	const u32	GetSize() const { return m_size; }
	//@brief	メモリの全体容量取得
	u32		GetTotalSize() const;
	//@brief	メモリの使用中容量取得
	u32		GetUsedSize() const;
	//@brief	メモリの取得数取得
	u32		GetUsedCount() const;
	//@brief	メモリの全体の空き容量取得
	u32		GetTotalRemainSize() const;
	//@brief	メモリの一番大きい連続領域の空き容量取得
	u32		GetMaxRemainSize(const u32 _align) const;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	現在のスタックサイズ取得
	u32		GetStackTotalSize() const;
	//@brief	現在のスタックの使用サイズ
	u32		GetStackUsedSize() const;
	//@brief	現在のスタックの取得数取得
	u32		GetStackUsedCount() const;

private:
	static const u32	HEADER_SIZE = sizeof(u32);	//<! ヘッダーサイズ取得
	static const u32	FLAG_SIZE = sizeof(b8);		//<! フリーフラグサイズ取得
	static const u32	OFFSET_SIZE = sizeof(u8);	//<! ヘッダーオフセットサイズ取得
	static const u32	FOOTER_SIZE = sizeof(u32);	//<! フッダーサイズ取得

	//-------------------------------------------------------------------------------------------------
	//@brief	バッファ位置取得
	inline void* GetBuffer(u32 _idx) const { return PCast<u8*>(m_pBuf) + _idx; }

	//-------------------------------------------------------------------------------------------------
	//@brief	バッファ位置取得
	inline u32	GetBufferIdx(void* _ptr) const { return SCast<u32>((u64)(_ptr) - (u64)(m_pBuf)); }

	//-------------------------------------------------------------------------------------------------
	//@brief	ヘッダー取得
	inline u32&	GetHeader(u32 _headerIdx) const { return *PCast<u32*>(GetBuffer(_headerIdx)); }

	//-------------------------------------------------------------------------------------------------
	//@brief	使用フラグ取得
	inline b8&	GetUseFlag(u32 _headerIdx) const { return *PCast<b8*>(GetBuffer(_headerIdx + HEADER_SIZE)); }

	//-------------------------------------------------------------------------------------------------
	//@brief	フッダー取得
	inline u32&	GetFooter(u32 _headerIdx) const { return *PCast<u32*>(GetBuffer(GetHeader(_headerIdx) - FOOTER_SIZE)); }

	//-------------------------------------------------------------------------------------------------
	//@brief	前方フッダー取得
	inline u32&	GetPrevFooter(u32 _headerIdx) const { return *PCast<u32*>(GetBuffer(_headerIdx - FOOTER_SIZE)); }

	//-------------------------------------------------------------------------------------------------
	//@brief	オフセット取得
	inline u8&	GetOffset(u32 _bufferIdx) const { return *PCast<u8*>(GetBuffer(_bufferIdx - OFFSET_SIZE)); }

	//-------------------------------------------------------------------------------------------------
	//@brief	前方ヘッダーインデックス取得
	inline u32	GetPrevHeaderIdx(u32 _headerIdx) const { return GetPrevFooter(_headerIdx); }

	//-------------------------------------------------------------------------------------------------
	//@brief	後方ヘッダーインデックス取得
	inline u32	GetNextHeaderIdx(u32 _headerIdx) const { return GetHeader(_headerIdx); }

	//-------------------------------------------------------------------------------------------------
	//@brief	アライメントした結果のインデックスを返す
	inline u32	GetAlignIdx(u32 _headerIdx, u32 _align) const { return GetBufferIdx((void*)(ALIGN_PTR(GetBuffer(_headerIdx + HEADER_SIZE + FLAG_SIZE + OFFSET_SIZE), _align))); }

	//-------------------------------------------------------------------------------------------------
	//@brief	アライメントを考慮したフリーサイズ取得
	inline u32	GetFreeBufferSize(u32 _headerIdx, u32 _align) const
	{
		u32 alignIdx = GetAlignIdx(_headerIdx, _align);
		if (alignIdx + FOOTER_SIZE <= GetHeader(_headerIdx))
		{
			return GetHeader(_headerIdx) - alignIdx - FOOTER_SIZE;
		}
		return 0;
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	ブロック分割
	//@return	確保したメモリのインデックス
	u32		DivisionBlock(u32 _headerIdx, size_t _size, u32 _align);

	//-------------------------------------------------------------------------------------------------
	//@brief	ブロックマージ
	//@return	マージしたブロックのヘッダインデックス
	u32		MergeBlock(u32 _headerIdx);

private:
	void*	m_pBuf = NULL;			//< バッファ
	u32		m_size = 0;				//< バッファサイズ

	u32		m_uEndHeaderIdx = 0;	//< 終端ヘッダーインデックス位置

	u32		m_uUsedSize = 0;		//< 使用サイズ
	u32		m_uNowCount = 0;		//< 確保数
};

// 型宣言
#ifdef POISON_RELEASE
typedef	TAllocator<CBoundaryTagAllocatorBase, CNoLock, CMemNoDebug, false>	CBoundaryTagAllocator;
#else
typedef	TAllocator<CBoundaryTagAllocatorBase, CNoLock, CMemDirtyDebug, false>	CBoundaryTagAllocator;
#endif // POISON_RELEASE


POISON_END

#endif  // __BOUNDARY_TAG_ALLOCATOR_H__

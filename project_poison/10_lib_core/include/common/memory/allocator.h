﻿//#pragma once
#ifndef __ALLOCATOR_H__
#define __ALLOCATOR_H__

POISON_BGN

//-------------------------------------------------------------------------------------------------
//	デフォルトのメモリアライメント
#define MEMORY_ALIGN_DEFAULT	(16u)

//-------------------------------------------------------------------------------------------------
//	メモリ確保デバッグ有効化
#ifndef POISON_RELEASE
#define ENABLE_MEMORY_ALLOC_DEBUG
#endif // !POISON_RELEASE


//*************************************************************************************************
//@brief	New確保構造体
//*************************************************************************************************
struct MemoryNewParam
{
	u32	align;				//< アライメント
	u32	attr;				//< 属性
#ifdef ENABLE_MEMORY_ALLOC_DEBUG
	const c8*	fileName;	//< デバッグ情報：ファイル名
	const c8*	funcName;	//< デバッグ情報：関数名
	u32			fileLine;	//< デバッグ情報：行数
#endif	// ENABLE_MEMORY_ALLOC_DEBUG

#ifdef ENABLE_MEMORY_ALLOC_DEBUG
	MemoryNewParam(u32 _align, u32 _attr, const c8* _fileName, const c8* _funcName, u32 _fileLine)
		: align(_align)
		, attr(_attr)
		, fileName(_fileName)
		, funcName(_funcName)
		, fileLine(_fileLine)
	{}
#else	// ENABLE_MEMORY_ALLOC_DEBUG
	MemoryNewParam(u32 _align = MEMORY_ALIGN_DEFAULT, u32 _attr = 0)
		: align(_align)
		, attr(_attr)
	{}
#endif	// !ENABLE_MEMORY_ALLOC_DEBUG
};

//*************************************************************************************************
//@brief	Malloc確保構造体
//*************************************************************************************************
struct MemoryMallocParam
{
	size_t	size;			//< サイズ
	u32		align;			//< アライメント
	u32		attr;			//< 属性
#ifdef ENABLE_MEMORY_ALLOC_DEBUG
	const c8*	fileName;	//< デバッグ情報：ファイル名
	const c8*	funcName;	//< デバッグ情報：関数名
	u32			fileLine;	//< デバッグ情報：行数
#endif	// ENABLE_MEMORY_ALLOC_DEBUG

#ifdef ENABLE_MEMORY_ALLOC_DEBUG
	MemoryMallocParam(size_t _size, u32 _align, u32 _attr, const c8* _fileName, const c8* _funcName, u32 _fileLine)
		: size(_size)
		, align(_align)
		, attr(_attr)
		, fileName(_fileName)
		, funcName(_funcName)
		, fileLine(_fileLine)
	{}
	MemoryMallocParam(size_t _size, const MemoryNewParam& _param)
		: size(_size)
		, align(_param.align)
		, attr(_param.attr)
		, fileName(_param.fileName)
		, funcName(_param.funcName)
		, fileLine(_param.fileLine)
	{}
#else	// ENABLE_MEMORY_ALLOC_DEBUG
	MemoryMallocParam(size_t _size, u32 _align = MEMORY_ALIGN_DEFAULT, u32 _attr = 0)
		: size(_size)
		, align(_align)
		, attr(_attr)
	{}
	MemoryMallocParam(size_t _size, const MemoryNewParam& _param)
		: size(_size)
		, align(_param.align)
		, attr(_param.attr)
	{}
#endif	// !ENABLE_MEMORY_ALLOC_DEBUG
};

//*************************************************************************************************
//@brief	StackBegin確保構造体
//*************************************************************************************************
struct MemoryStackBeginParam
{
	u32	reserve;			//< 確保するメモリ量、0指定の場合はアロケータ内の最大のメモリ領域を取る
	u32	align;				//< アライメント
#ifdef ENABLE_MEMORY_ALLOC_DEBUG
	const c8*	fileName;	//< デバッグ情報：ファイル名
	const c8*	funcName;	//< デバッグ情報：関数名
	u32			fileLine;	//< デバッグ情報：行数
#endif	// ENABLE_MEMORY_ALLOC_DEBUG

#ifdef ENABLE_MEMORY_ALLOC_DEBUG
	MemoryStackBeginParam(u32 _reserve, u32 _align, const c8* _fileName, const c8* _funcName, u32 _fileLine)
		: reserve(_reserve)
		, align(_align)
		, fileName(_fileName)
		, funcName(_funcName)
		, fileLine(_fileLine)
	{}
#else	// ENABLE_MEMORY_ALLOC_DEBUG
	MemoryStackBeginParam(u32 _reserve, u32 _align = MEMORY_ALIGN_DEFAULT)
		: reserve(_reserve)
		, align(_align)
	{}
#endif	// !ENABLE_MEMORY_ALLOC_DEBUG
};

#ifdef ENABLE_MEMORY_ALLOC_DEBUG
#define MEM_NEW_PARAM_ALIGN_ATTR(_align, _attr)				POISON::MemoryNewParam(_align, _attr, __FILE__, __FUNCTION__, __LINE__)
#define MEM_MALLOC_PARAM_ALIGN_ATTR(_size, _align, _attr)	POISON::MemoryMallocParam(_size, _align, _attr, __FILE__, __FUNCTION__, __LINE__)
#define MEM_STACK_BEGIN_PARAM_ALIGN(_reserve, _align)		POISON::MemoryStackBeginParam(_reserve, _align, __FILE__, __FUNCTION__, __LINE__)
#else	// ENABLE_MEMORY_ALLOC_DEBUG
#define MEM_NEW_PARAM_ALIGN_ATTR(_align, _attr)				POISON::MemoryNewParam(_align, _attr)
#define MEM_MALLOC_PARAM_ALIGN_ATTR(_size, _align, _attr)	POISON::MemoryMallocParam(_size, _align, _attr)
#define MEM_STACK_BEGIN_PARAM_ALIGN(_reserve, _align)		POISON::MemoryStackBeginParam(_reserve, _align)
#endif	// !ENABLE_MEMORY_ALLOC_DEBUG

#define	MEM_NEW_PARAM_ALIGN(_align)							MEM_NEW_PARAM_ALIGN_ATTR(_align, 0)
#define	MEM_MALLOC_PARAM_ALIGN(_size, _align)				MEM_MALLOC_PARAM_ALIGN_ATTR(_size, _align, 0)

#define	MEM_NEW_PARAM_ATTR(_attr)							MEM_NEW_PARAM_ALIGN_ATTR(MEMORY_ALIGN_DEFAULT, _attr)
#define	MEM_MALLOC_PARAM_ATTR(_size, _attr)					MEM_MALLOC_PARAM_ALIGN_ATTR(_size, MEMORY_ALIGN_DEFAULT, _attr)

#define	MEM_NEW_PARAM()										MEM_NEW_PARAM_ALIGN_ATTR(MEMORY_ALIGN_DEFAULT, 0)
#define	MEM_MALLOC_PARAM(_size)								MEM_MALLOC_PARAM_ALIGN_ATTR(_size, MEMORY_ALIGN_DEFAULT, 0)
#define	MEM_STACK_BEGIN_PARAM()								MEM_STACK_BEGIN_PARAM_ALIGN(0, MEMORY_ALIGN_DEFAULT)
#define	MEM_STACK_BEGIN_PARAM_RESERVE(_reserve)				MEM_STACK_BEGIN_PARAM_ALIGN(_reserve, MEMORY_ALIGN_DEFAULT)

#define	MEM_NEW_PARAM_TO_MALLOC(_size, _param)				POISON::MemoryMallocParam(_size, _param)



//*************************************************************************************************
//@brief	何もしないロッククラス
//*************************************************************************************************
class CNoLock
{
public:
	void	Lock() {};
	void	Unlock() {};
};


//*************************************************************************************************
//@brief	何もしないデバッグクラス
//*************************************************************************************************
class CMemNoDebug
{
public:
	void	Malloc(void* _ptr, size_t _size) {}
	void	Free(void* _ptr, size_t _size) {}

	//-------------------------------------------------------------------------------------------------
	//@brief	最大使用量更新
	void	UpdateMaxUsed(u32 _size, u32 _count) {}
	u32		GetMaxUsedSize() const { return 0; }
	u32		GetMaxUsedCount() const { return 0; }
};

//*************************************************************************************************
//@brief	メモリ汚しデバッグクラス
//*************************************************************************************************
class CMemDirtyDebug
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	生成汚し
	void	Malloc(void* _ptr, size_t _size)
	{
		if (_ptr) { memset(_ptr, 0x55, _size); }
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄汚し
	void	Free(void* _ptr, size_t _size)
	{
		if (_size > 0) { memset(_ptr, 0xcc, _size); }
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	最大使用量更新
	void	UpdateMaxUsed(u32 _size, u32 _count)
	{
		if (maxUsedSize < _size) { maxUsedSize = _size; }
		if (maxUsedCount < _count) { maxUsedCount = _count; }
	}
	u32		GetMaxUsedSize() const { return maxUsedSize; }
	u32		GetMaxUsedCount() const { return maxUsedCount; }

private:
	u32		maxUsedSize = 0;
	u32		maxUsedCount = 0;
};



//*************************************************************************************************
//@brief	アロケータクラス
//*************************************************************************************************
class IAllocator
{
public:
	IAllocator();
	virtual ~IAllocator();

	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	virtual b8		Initialize(void* _pBuf, u32 _size) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	更新
	virtual void	Update() = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	virtual void	Finalize() = 0;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	生成
	///@return	確保したメモリのポインタ、失敗ならNULL
	///@param[in]	_param	Mallocパラメータ
	virtual void*	Malloc(const MemoryMallocParam& _param) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄
	///@return			戻り値で解放したサイズを返す(わからない場合、失敗した場合は0)
	///@param[in]	_ptr	解放するポインタ
	///@note
	/// 確保していたメモリを破棄します。
	///	破棄が成功した場合、可能な限り、破棄したメモリのサイズを返します。\n
	///	これは外部がメモリの統計を取るために使用されます。\n
	virtual u32		Free(void* _ptr) = 0;

public:
	//-------------------------------------------------------------------------------------------------
	///@brief	スタック開始
	///@return	スタック生成が成功した場合は解放のためのポインタが帰る
	///@param[in]	_param	StavkBeginパラメータ
	///@note
	/// スタックメモリ確保を開始します。\n
	///	スタック開始が成功すると、以後このアロケータでのメモリ確保がすべてスタックから取られます。\n
	///	StackEnd()を呼ぶことでスタック状態が終了し、\n
	///	スタックで予約して使用しなかった残りの領域を空きメモリに戻し通常に戻ります。\n
	///	スタックでメモリを確保した場合、StackBeginの戻り値のポインタでまとめてメモリ解放などが行えます。\n
	///	スタック確保中はその他のメモリ取得、解放が行えません。\n
	///	なお、スタックはアロケータによっては実装されていません。\n
	///	その場合StackBeginはNULLを返し、無視されます。\n
	///	スタックの実装は必須でないため、実装がわからない場合はIsEnableStack()で必ず確認してください。\n
	virtual	void*	StackBegin(const MemoryStackBeginParam& _param) = 0;

	//-------------------------------------------------------------------------------------------------
	///@brief	親メモリーを指定してスタック開始
	///@return	スタック生成が成功した場合はTRUEが返る
	///@param[in]	_parent		親のポインタ
	///@param[in]	_param		StavkBeginパラメータ
	///@note
	/// スタックメモリ確保を開始します。\n
	///	スタック開始が成功すると、以後このアロケータでのメモリ確保がすべてスタックから取られます。\n
	///	StackEnd()を呼ぶことでスタック状態が終了し、\n
	///	スタックで予約して使用しなかった残りの領域を空きメモリに戻し通常に戻ります。\n
	///	スタックでメモリを確保した場合、親のポインタでまとめてメモリ解放などが行えます。\n
	///	スタック確保中はその他のメモリ取得、解放が行えません。\n
	///	なお、スタックはアロケータによっては実装されていません。\n
	///	その場合StackBeginはNULLを返し、無視されます。\n
	///	スタックの実装は必須でないため、実装がわからない場合はIsEnableStack()で必ず確認してください。\n
	virtual	b8		StackParentBegin(void* _parent, const MemoryStackBeginParam& _param) = 0;

	//-------------------------------------------------------------------------------------------------
	///@brief	スタック終了
	///@return	スタックで確保した合計値のサイズ
	///@note
	/// スタックの受付を終了します。\n
	///	戻り値は、スタックで確保されたメモリ数を返します。\n
	///	スタックの実装は必須でないため、実装がわからない場合はIsEnableStack()で必ず確認してください。
	virtual	u32		StackEnd() = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	スタック機能が使用可能かどうか
	virtual	b8		IsEnableStack() const = 0;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	ロック
	virtual	void	Lock() = 0;

	//@brief	アンロック
	virtual	void	Unlock() = 0;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	バッファ取得
	virtual const void*	GetBuffer() const = 0;
	//@brief	バッファサイズ取得
	virtual const u32	GetSize() const = 0;
	//@brief	メモリの全体容量取得
	virtual u32		GetTotalSize() const = 0;
	//@brief	メモリの使用中容量取得
	virtual u32		GetUsedSize() const = 0;
	//@brief	メモリの取得数取得
	virtual u32		GetUsedCount() const = 0;
	//@brief	メモリの全体の空き容量取得
	virtual u32		GetTotalRemainSize() const = 0;
	//@brief	メモリの一番大きい連続領域の空き容量取得
	virtual u32		GetMaxRemainSize(const u32 _align) const = 0;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	現在のスタックサイズ取得
	virtual	u32		GetStackTotalSize() const = 0;
	//@brief	現在のスタックの使用サイズ
	virtual	u32		GetStackUsedSize() const = 0;
	//@brief	現在のスタックの取得数取得
	virtual	u32		GetStackUsedCount() const = 0;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	今までのメモリの最大使用量
	virtual u32		GetMaxUsedSize() const = 0;
	//@brief	今までのメモリの最大使用数
	virtual u32		GetMaxUsedCount() const = 0;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	ハンドル取得
	inline u32		GetHandle() const { return m_handle; }
	//@brief	ハンドル設定
	inline void		SetHandle(u32 _handle) { m_handle = _handle; }

	//-------------------------------------------------------------------------------------------------
	//@brief	最低限のアライメント取得
	inline u32		GetAlignBase() const { return m_alignBase; }
	//@brief	最低限のアライメント設定
	inline void		SetAlignBase(u32 _align) { m_alignBase = _align; }

	//-------------------------------------------------------------------------------------------------
	//@brief	アロケータ名取得
	inline const c8*	GetName() const { return m_name; }
	//@brief	アロケータ名設定
	inline void			SetName(const c8* _pName) { strcpy_s(m_name, _pName); }

	//-------------------------------------------------------------------------------------------------
	//@brief	スタックモード中か
	inline b8		IsStackMode() const { return m_stackMode; }
	//@brief	スタックモード設定
	inline void		SetStackMode(b8 _isStack) { m_stackMode = _isStack; }

private:
	u32		m_handle;		//< ハンドル
	u32		m_alignBase;	//< 最低限のアライメント
	c8		m_name[32];		//< 名前
	b8		m_stackMode;	//< スタックモード
};


//*************************************************************************************************
//@brief	テンプレートアロケータ
//*************************************************************************************************
template< class MEMORY, class LOCK, class DEBUG, b8 ENABLE_STACK >
class TAllocator : public IAllocator
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ
	TAllocator()
		: m_memory()
		, m_lock()
		, m_debug()
	{}
	/// デストラクタ
	virtual ~TAllocator() {}

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	virtual b8		Initialize(void* _pBuf, u32 _size);

	//-------------------------------------------------------------------------------------------------
	//@brief	更新処理
	virtual	void	Update();

	//-------------------------------------------------------------------------------------------------
	//@brief	後処理
	virtual	void	Finalize();

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	ロック
	virtual	void	Lock();

	//-------------------------------------------------------------------------------------------------
	//@brief	アンロック
	virtual	void	Unlock();

public:
	//-------------------------------------------------------------------------------------------------
	///@brief	メモリ確保
	///@return	確保したメモリのポインタ、失敗ならNULL
	///@param[in]	_param	Mallocパラメータ
	virtual	void*	Malloc(const MemoryMallocParam& _param);

	//-------------------------------------------------------------------------------------------------
	///@brief	メモリ破棄
	///@return			戻り値で解放したサイズを返す(わからない場合、失敗した場合は0)
	///@param[in]	_ptr	解放するポインタ
	virtual u32		Free(void* _ptr);

public:
	//-------------------------------------------------------------------------------------------------
	///@brief	スタック開始
	///@return	スタック生成が成功した場合は解放のためのポインタが帰る
	///@param[in]	_param	StackBeginパラメータ
	virtual	void*	StackBegin(const MemoryStackBeginParam& _param);

	//-------------------------------------------------------------------------------------------------
	///@brief	親メモリーを指定してスタック開始
	///@return	スタック生成が成功した場合はTRUEが返る
	///@param[in]	_parent		親のポインタ
	///@param[in]	_param		StackBeginパラメータ
	virtual	b8		StackParentBegin(void* _parent, const MemoryStackBeginParam& _param);

	//-------------------------------------------------------------------------------------------------
	///@brief	スタック終了
	///@return	スタックで確保した合計値のサイズ
	virtual	u32		StackEnd();

	//-------------------------------------------------------------------------------------------------
	//@brief	スタック機能が使用可能かどうか
	virtual	b8		IsEnableStack() const { return ENABLE_STACK; }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	メモリのトップアドレス取得
	virtual const void*	GetBuffer() const { return m_memory.GetBuffer(); }
	//@brief	バッファサイズ取得
	virtual const u32	GetSize() const { return m_memory.GetSize(); }
	//@brief	メモリの全体容量取得
	virtual	u32		GetTotalSize() const { return m_memory.GetTotalSize(); }
	//@brief	メモリの使用中容量取得
	virtual	u32		GetUsedSize() const { return m_memory.GetUsedSize(); }
	//@brief	メモリの取得数取得
	virtual	u32		GetUsedCount() const { return m_memory.GetUsedCount(); }
	//@brief	メモリの全体の空き容量取得
	virtual u32		GetTotalRemainSize() const { return m_memory.GetTotalRemainSize(); }
	//@brief	メモリの一番大きい連続領域の空き容量取得
	virtual	u32		GetMaxRemainSize(u32 _align) const
	{
		if (_align < GetAlignBase()) { _align = GetAlignBase(); }
		return m_memory.GetMaxRemainSize(_align);
	}

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	現在のスタックサイズ取得
	virtual	u32		GetStackTotalSize() const { return m_memory.GetStackTotalSize(); }
	//@brief	現在のスタックの使用サイズ
	virtual	u32		GetStackUsedSize() const { return m_memory.GetStackUsedSize(); }
	//@brief	現在のスタックの取得数取得
	virtual	u32		GetStackUsedCount() const { return m_memory.GetStackUsedCount(); }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	今までのメモリの最大使用量
	virtual	u32		GetMaxUsedSize() const { return m_debug.GetMaxUsedSize(); }
	//@brief	今までのメモリの最大使用数
	virtual	u32		GetMaxUsedCount() const { return m_debug.GetMaxUsedCount(); }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	メモリクラス取得
	MEMORY&		GetMemory() { return m_memory; }
	//@brief	ロッククラス取得
	LOCK&		GetLock() { return m_lock; }
	//@brief	デバッグクラス取得
	DEBUG&		GetDebug() { return m_debug; }

private:
	MEMORY		m_memory;		//< メモリクラス
	LOCK		m_lock;			//< ロッククラス
	DEBUG		m_debug;		//< デバッグクラス
};



//-------------------------------------------------------------------------------------------------
//@brief	メモリハンドルとアロケータの手順の違いを吸収するMalloc関数
inline void*	Malloc(IAllocator* _pAllocator, const MemoryMallocParam& _param);

//@brief	メモリハンドルとアロケータの手順の違いを吸収するcalloc関数
inline void*	Calloc(IAllocator*  _pAllocator, const MemoryMallocParam& _param);

//@brief	メモリハンドルとアロケータの手順の違いを吸収するFree関数
inline u32		Free(IAllocator*  _pAllocator, void* _p);

//@brief	メモリハンドルとアロケータの手順の違いを吸収するStackBegin関数
inline void*	StackBegin(IAllocator* _pAllocator, const MemoryStackBeginParam& _param);

//@brief	メモリハンドルとアロケータの手順の違いを吸収するStackEnd関数
inline u32		StackEnd(IAllocator* _pAllocator);


POISON_END

#include "allocator.inl"

#endif	// __ALLOCATOR_H__

﻿//#pragma once
#ifndef __MEMORY_H__
#define __MEMORY_H__


#include "memory/allocator_utility.h"

POISON_BGN

//-------------------------------------------------------------------------------------------------
//@brief	メモリハンドルでのクラスニュー
#define MEM_NEW( _memHdl )					::new( _memHdl, MEM_NEW_PARAM() )
//@brief	メモリハンドルでの属性付きクラスニュー
#define MEM_NEW_ATTR( _memHdl, _attr )		::new( _memHdl, MEM_NEW_PARAM_ATTR(_attr) )

//-------------------------------------------------------------------------------------------------
//@brief	メモリハンドルでのクラスデリート     
#define MEM_DELETE( _memHdl, _ptr )			::operator delete( _ptr, _memHdl, MEM_NEW_PARAM(), _ptr )
//@brief	メモリハンドルでのセーフデリート
#define MEM_SAFE_DELETE( _memHdl, _ptr )	if( _ptr ){ MEM_DELETE( _memHdl, _ptr ); _ptr = NULL; }

//-------------------------------------------------------------------------------------------------
//@brief	メモリハンドルでのメモリ確保
#define MEM_MALLOC( _memHdl, _size )								POISON::Malloc( _memHdl, MEM_MALLOC_PARAM(_size) )
//@brief	メモリハンドルでの属性付きメモリ確保
#define MEM_MALLOC_ATTR( _memHdl, _attr, _size )					POISON::Malloc( _memHdl, MEM_MALLOC_PARAM_ATTR(_size, _attr) )
//@brief	メモリハンドルでの型指定メモリ確保
#define MEM_MALLOC_TYPE( _memHdl, _class )							PCast<_class*>(POISON::Malloc( _memHdl, MEM_MALLOC_PARAM(sizeof(_class)) ))
//@brief	メモリハンドルでの属性付き型指定メモリ確保
#define MEM_MALLOC_ATTR_TYPE( _memHdl, _attr, _class )				PCast<_class*>(POISON::Malloc( _memHdl, MEM_MALLOC_PARAM_ATTR(sizeof(_class), _attr) ))
//@brief	メモリハンドルでの型指定個数指定メモリ確保
#define MEM_MALLOC_TYPES( _memHdl, _class, _num )					PCast<_class*>(POISON::Malloc( _memHdl, MEM_MALLOC_PARAM(sizeof(_class) * _num) ))
//@brief	メモリハンドルでの属性付き型指定個数指定メモリ確保
#define MEM_MALLOC_ATTR_TYPES( _memHdl, _attr, _class, _num )		PCast<_class*>(POISON::Malloc( _memHdl, MEM_MALLOC_PARAM_ATTR(sizeof(_class) * _num, _attr) ))
//@brief	メモリハンドルでのアライメント指定メモリ確保
#define MEM_MALLOC_ALIGN( _memHdl, _align, _size )					POISON::Malloc( _memHdl, MEM_MALLOC_PARAM_ALIGN(_size, _align) )
//@brief	メモリハンドルでのアライメント指定属性付きメモリ確保
#define MEM_MALLOC_ALIGN_ATTR( _memHdl, _align, _attr, _size )		POISON::Malloc( _memHdl, MEM_MALLOC_PARAM_ALIGN_ATTR(_size, _align, _attr) )

//-------------------------------------------------------------------------------------------------
//@brief	メモリハンドルでのメモリ確保(バッファ０クリア)
#define MEM_CALLOC( _memHdl, _size )								POISON::Calloc( _memHdl, MEM_MALLOC_PARAM(_size) )
//@brief	メモリハンドルでの属性付きメモリ確保
#define MEM_CALLOC_ATTR( _memHdl, _attr, _size )					POISON::Calloc( _memHdl, MEM_MALLOC_PARAM_ATTR(_size, _attr) )
//@brief	メモリハンドルでの型指定メモリ確保
#define MEM_CALLOC_TYPE( _memHdl, _class )							PCast<_class*>(POISON::Calloc( _memHdl, MEM_MALLOC_PARAM(sizeof(_class)) ))
//@brief	メモリハンドルでの属性付き型指定メモリ確保
#define MEM_CALLOC_ATTR_TYPE( _memHdl, _attr, _class )				PCast<_class*>(POISON::Calloc( _memHdl, MEM_MALLOC_PARAM_ATTR(sizeof(_class), _attr) ))
//@brief	メモリハンドルでの型指定個数指定メモリ確保
#define MEM_CALLOC_TYPES( _memHdl, _class, _num )					PCast<_class*>(POISON::Calloc( _memHdl, MEM_MALLOC_PARAM(sizeof(_class) * _num) ))
//@brief	メモリハンドルでの属性付き型指定個数指定メモリ確保
#define MEM_CALLOC_ATTR_TYPES( _memHdl, _attr, _class, _num )		PCast<_class*>(POISON::Calloc( _memHdl, MEM_MALLOC_PARAM_ATTR(sizeof(_class) * _num, _attr) ))
//@brief	メモリハンドルでのアライメント指定メモリ確保
#define MEM_CALLOC_ALIGN( _memHdl, _align, _size )					POISON::Calloc( _memHdl, MEM_MALLOC_PARAM_ALIGN(_size, _align) )
//@brief	メモリハンドルでのアライメント指定属性付きメモリ確保
#define MEM_CALLOC_ALIGN_ATTR( _memHdl, _align, _attr, _size )		POISON::Calloc( _memHdl, MEM_MALLOC_PARAM_ALIGN_ATTR(_size, _align, _attr) )

//-------------------------------------------------------------------------------------------------
//@brief	メモリハンドルでのメモリ解放
#ifdef MEM_FREE	// すでにMEM_FREEがあれば消す(Windowsのせい)
	#undef MEM_FREE
#endif
#define MEM_FREE( _memHdl, _ptr )						POISON::Free( _memHdl, _ptr )
//@brief	メモリハンドルでのメモリ安全解放
#define MEM_SAFE_FREE( _memHdl, _ptr )					if( _ptr ){ MEM_FREE( _memHdl, _ptr ); _ptr = NULL; }

//-------------------------------------------------------------------------------------------------
//@brief	メモリハンドルでのスタックメモリ開始
#define MEM_STACK_BEGIN( _memHdl )						POISON::StackBegin(_memHdl, MEM_STACK_BEGIN_PARAM())
//@brief	メモリハンドルでの予約領域指定スタックメモリ開始
#define MEM_STACK_BEGIN_RESERVE( _memHdl, _reserve )	POISON::StackBegin(_memHdl, MEM_STACK_BEGIN_PARAM_RESERVE( _reserve ))

//-------------------------------------------------------------------------------------------------
//@brief	メモリハンドルでのスタックメモリ終了
#define MEM_STACK_END( _memHdl )						POISON::StackEnd(_memHdl)

//-------------------------------------------------------------------------------------------------
//@brief	メモリハンドルでのアロケータの取得
#define MEM_ALLOCATOR( _memHdl )						POISON::GetAllocator(_memHdl)

POISON_END

#endif	/@brief __MEMORY_H__

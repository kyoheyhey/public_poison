﻿//#pragma once
#ifndef __WIN_ALLOCATOR_H__
#define __WIN_ALLOCATOR_H__

#include "memory/allocator.h"

POISON_BGN


//*************************************************************************************************
//@brief	ヒープアロケータクラスwin
//*************************************************************************************************
class CHeapAllocatorWinBase
{
public:
	CHeapAllocatorWinBase();
	~CHeapAllocatorWinBase();

	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	b8		Initialize(void* _pBuf, u32 _size);

	//-------------------------------------------------------------------------------------------------
	//@brief	更新
	void	Update() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	void	Finalize();

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	生成
	///@return	確保したメモリのポインタ、失敗ならNULL
	///@param[in]	_param	Mallocパラメータ
	void*	Malloc(const MemoryMallocParam& _param);

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄
	///@return			戻り値で解放したサイズを返す(わからない場合、失敗した場合は0)
	///@param[in]	_ptr	解放するポインタ
	u32		Free(void* _ptr);

public:
	//-------------------------------------------------------------------------------------------------
	///@brief	スタック開始
	///@note
	/// 何もしない。
	void*	StackBegin(const MemoryStackBeginParam& _param) { return 0; }

	//-------------------------------------------------------------------------------------------------
	///@brief	親メモリーを指定してスタック開始
	///@note
	/// 何もしない。
	b8		StackParentBegin(void* _parent, const MemoryStackBeginParam& _param) { return false; }

	//-------------------------------------------------------------------------------------------------
	///@brief	スタック終了
	///@note
	/// 何もしない。
	u32		StackEnd() { return 0; }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	バッファ取得
	const void*	GetBuffer() const { return NULL; }
	//@brief	バッファサイズ取得
	const u32	GetSize() const { return 0; }
	//@brief	メモリの全体容量取得
	u32		GetTotalSize() const { return 0; }
	//@brief	メモリの使用中容量取得
	u32		GetUsedSize() const { return 0; }
	//@brief	メモリの取得数取得
	u32		GetUsedCount() const { return 0; }
	//@brief	メモリの全体の空き容量取得
	u32		GetTotalRemainSize() const { return 0; }
	//@brief	メモリの一番大きい連続領域の空き容量取得
	u32		GetMaxRemainSize(const u32 _align) const { return 0; }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	現在のスタックサイズ取得
	u32		GetStackTotalSize() const { return 0; }
	//@brief	現在のスタックの使用サイズ
	u32		GetStackUsedSize() const { return 0; }
	//@brief	現在のスタックの取得数取得
	u32		GetStackUsedCount() const { return 0; }

private:
	u32		m_useCount;		//< 使用カウンター

};

// 型宣言
typedef	TAllocator<CHeapAllocatorWinBase, CNoLock, CMemNoDebug, false>	CHeapAllocatorWin;


POISON_END


#endif	// __WIN_ALLOCATOR_H__

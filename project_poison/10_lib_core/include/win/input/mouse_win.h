﻿//#pragma once
#ifndef __MOUSE_WIN_H__
#define __MOUSE_WIN_H__

//-------------------------------------------------------------------------------------------------
// include
#include "input/mouse.h"


POISON_BGN

//*************************************************************************************************
//@brief	Winマウスクラス
//*************************************************************************************************
class CMouseWin : public CMouse
{
public:
	CMouseWin() {}
	virtual ~CMouseWin() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	更新
	virtual void	Step() override;

protected:
	CRect	m_rect;		///< ウィンドウクライアントレクト
};


POISON_END

#endif	// __MOUSE_WIN_H__

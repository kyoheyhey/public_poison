﻿//#pragma once
#ifndef __KEYBOARD_WIN_H__
#define __KEYBOARD_WIN_H__

//-------------------------------------------------------------------------------------------------
// include
#include "input/keyboard.h"


POISON_BGN

//*************************************************************************************************
//@brief	Winキーボードクラス
//*************************************************************************************************
class CKeyboardWin : public CKeyboard
{
public:
	CKeyboardWin() {}
	virtual ~CKeyboardWin() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	更新
	virtual void	Step() override;

};


POISON_END

#endif	// __KEYBOARD_WIN_H__

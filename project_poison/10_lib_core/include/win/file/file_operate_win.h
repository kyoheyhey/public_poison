﻿//#pragma once
#ifndef __FILE_OPERATE_WIN_H__
#define __FILE_OPERATE_WIN_H__

#include "file/file_operate.h"

POISON_BGN

//-------------------------------------------------------------------------------------------------
//@brief	ファイルハンドル構造体
struct FileOperateHandle
{
};

//-------------------------------------------------------------------------------------------------
//@brief	Windowsファイル操作クラスインターフェース
class CFileOperateWin : public IFileOperate
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルハンドルが有効化どうか
	virtual b8	IsValidFileHandle(FoFileHn _hn);

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルが存在するか？
	virtual b8	IsFileExist(const c8* _path);

	//-------------------------------------------------------------------------------------------------
	//@brief	ディレクトリが存在するか？
	virtual b8	IsDirectoryExist(const c8* _path);

	//-------------------------------------------------------------------------------------------------
	//@brief	読み込み専用か？
	virtual b8	IsReadOnly() const;

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルのオープン
	virtual FoFileHn	Open(const c8* _path, u32 _mode, b8 _async, u32 _attr = 0);

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルのクローズ
	virtual void	Close(FoFileHn _hn, FILE_ASYNC_DATA* _asdata = 0);

	//-------------------------------------------------------------------------------------------------
	//@brief	ディレクトリを作成する
	virtual b8	MakeDirectory(const c8* _path);

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルを削除する
	virtual b8	DeleteFile(const c8* _path);

	//-------------------------------------------------------------------------------------------------
	//@brief	ディレクトリを削除する
	virtual b8	DeleteDirectory(const c8* _path);

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルへのデータ書き込み
	virtual b8	Write(FoFileHn _hn, const void* _buf, const u32 _size, u32* _sizeWritten, u32* _status, FILE_ASYNC_DATA* _asdata = 0);

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルからのデータ読み込み
	virtual b8	Read(FoFileHn _hn, void* _buf, const u32 _size, u32* _sizeRead, u32* _status, FILE_ASYNC_DATA* _asdata = 0);

	//-------------------------------------------------------------------------------------------------
	//@brief	ファイルサイズの取得
	virtual u32	GetFileSize(FoFileHn _hn, s32* _size);

	//-------------------------------------------------------------------------------------------------
	//@brief	指定フォルダ直下にあるファイルを条件指定して探す
	virtual u32	GetFiles(const c8* _path, const c8* _cond, c8 _out[][FO_FILENAME_MAX_LEN], u32 _outMaxNum);

	//-------------------------------------------------------------------------------------------------
	//@brief	エラーコード取得
	virtual u32	GetLastErrorCode() { return m_error; }

	//-------------------------------------------------------------------------------------------------
	//@brief	指定フォルダ直下にあるフォルダを探します.
	//@param	_path		探すパス.
	//@param	_config		検索設定.
	//@param	_out[][]	出力先配列.
	//@param	_outMaxNum	最大の配列サイズ.
	//@return	見つかったフォルダの数.
	virtual u32 GetDirs(const c8* _path, const FileDirInfoConfig* _config, c8 _out[][FO_FILENAME_MAX_LEN], u32 _outMaxNum);

private:
	u32	m_error = 0;	//< エラー変数
};

POISON_END

#endif	// __FILE_OPERATE_WIN_H__

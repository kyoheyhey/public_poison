@ECHO OFF
SET BAT_PATH=%~dp0
SET TEXT_PATH=%~1
SET OUT_PATH=%~2
SET BIN_DIR=%3
SET COPY_DIR=%~4
SET OUT_TMP_PATH=%~dp1bin

rem 変換処理（binフォルダがなければ作る）
IF "%OUT_PATH%"=="" ( 
	IF NOT EXIST %OUT_TMP_PATH% md "%OUT_TMP_PATH%"
	SET OUT_PATH=%OUT_TMP_PATH%\%~nx1
)

ECHO textバイナリ変換  srcFile "%TEXT_PATH%" outPath "%OUT_PATH%.bin"
%BAT_PATH%\..\exe\text2bin.exe -I "%TEXT_PATH%" -O "%OUT_PATH%.bin" -CRC
IF NOT %ERRORLEVEL%==0 (pause)

rem 変換したものをdataにコピーします
setlocal ENABLEDELAYEDEXPANSION
SET CURRENT_PATH=%~dp3
SET OUT_DATA_PATH=!CURRENT_PATH:%LV5_YW-Y1_BUILD_SVN_DIR%=%LV5_YW-Y1_DATA_DIR%\common!
rem コピー先指定が有る場合はそちらで上書き
IF NOT "%COPY_DIR%"=="" (
	SET OUT_DATA_PATH=%COPY_DIR%
)

ECHO --------------------------------------------------------------------
ECHO "%CURRENT_PATH%bin\" から 
ECHO --- "%OUT_DATA_PATH%" にファイルをコピーします。
ECHO --------------------------------------------------------------------

IF NOT EXIST "%OUT_DATA_PATH%" md "%OUT_DATA_PATH%"
copy "%CURRENT_PATH%\bin\%~nx1.bin"  "%OUT_DATA_PATH%\"  

ENDLOCAL



@ECHO OFF

rem --------------------------------------------------------------------
rem 指定したフォルダにある cfg ファイルに cpp32 を実行して出力
rem 第一引数 検索フォルダパス
rem 第二引数 出力フォルダパス
rem --------------------------------------------------------------------

SET BAT_PATH=%~dp0

SET BASE_DIR=%~1\
SET BASE_NAME=%~n1

rem 検索パスの設定
SET SRC_PATH=%BASE_DIR%

rem 出力パスの指定
IF NOT "%2"=="" (SET OUT_PATH=%~2\) ELSE (SET OUT_PATH=%BASE_DIR%)

rem Include検索ディレクトリ
SET INCLUDE_DIR=%~3

rem ファイル拡張子
SET EXT=.cfg

ECHO --------------------------------------------------------------------
ECHO フォルダ内のcfgデータにcpp32を実行して出力
ECHO --ソースフォルダ "%SRC_PATH%"  
ECHO --出力フォルダ   "%OUT_PATH%"
ECHO --出力名         "%BASE_NAME%"
ECHO --INCLUDEパス 　 "%INCLUDE_DIR%"
ECHO --------------------------------------------------------------------

rem outフォルダ作成
IF EXIST "%OUT_PATH%" (DEL /q "%OUT_PATH%") ELSE md "%OUT_PATH%"

rem cfgファイルをすべて並べて変換
FOR /F %%A in ('dir /b %SRC_PATH%') do IF "%%~xA"=="%EXT%" ( 
IF NOT "%INCLUDE_DIR%"=="" (CALL %LV5_YW-Y1_TOOLS_DIR%\exe\cpp32.exe -+ -P -I "%INCLUDE_DIR%" "%SRC_PATH%%%A" "%OUT_PATH%%%A") ELSE (
CALL %LV5_YW-Y1_TOOLS_DIR%\exe\cpp32.exe -+ -P "%SRC_PATH%%%A" "%OUT_PATH%%%A"))

rem 終了処理
rem pause
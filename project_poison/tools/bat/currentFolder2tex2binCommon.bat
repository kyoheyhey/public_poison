@ECHO OFF

rem 第一引数を取得
SET COMMON_OUT_PATH=%1

rem 一時的に cpp32 で置き換えた ファイルを作成
CALL %LV5_YW-Y1_TOOLS_DIR%\bat\convert_cpp32.bat %COMMON_OUT_PATH% %COMMON_OUT_PATH%\temp

rem カレントフォルダで出力呼び出し
CALL %LV5_YW-Y1_TOOLS_DIR%\bat\folder2tex2bin.bat %COMMON_OUT_PATH% %COMMON_OUT_PATH%\temp

rem ワークフォルダの削除
IF EXIST "%COMMON_OUT_PATH%\temp" RD /s /q "%COMMON_OUT_PATH%\temp"
IF EXIST "%COMMON_OUT_PATH%\bin" RD /s /q "%COMMON_OUT_PATH%\bin"

rem 終了
rem pause
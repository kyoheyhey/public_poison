@ECHO OFF

SET BAT_PATH=%~dp0

SET BASE_DIR=%~1\
SET BASE_NAME=%~n1

rem 検索元パスの指定
IF NOT "%2"=="" (SET SRC_PATH=%~2\) ELSE (SET SRC_PATH=%BASE_DIR%)

rem 出力フォルダパス
IF NOT "%3"=="" (SET OUT_PATH=%~3\) ELSE (SET OUT_PATH=%BASE_DIR%bin\)

rem コピー先フォルダパスの指定
SET COPY_PATH=%~4

rem ファイル拡張子
SET EXT=.cfg

ECHO --------------------------------------------------------------------
ECHO フォルダ内のcfgデータ出力します。 
ECHO --ソースフォルダ "%SRC_PATH%"  
ECHO --出力フォルダ   "%OUT_PATH%"
ECHO --出力名         "%BASE_NAME%"
ECHO --データコピー先 "%COPY_PATH%"
ECHO --------------------------------------------------------------------

rem outフォルダ作成
IF EXIST "%OUT_PATH%" (DEL /q "%OUT_PATH%") ELSE md "%OUT_PATH%"

SETLOCAL enabledelayedexpansion

rem cfgファイルをすべて並べて変換
FOR /F %%A in ('dir /b %SRC_PATH%') do IF "%%~xA"=="%EXT%" ( CALL %BAT_PATH%text2bin.bat "%SRC_PATH%%%A" "%OUT_PATH%%%A" "%BASE_DIR%" "%COPY_PATH%" )

ENDLOCAL

rem 終了処理
rem pause

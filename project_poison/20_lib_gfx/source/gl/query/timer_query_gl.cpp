﻿
#ifdef LIB_GFX_OPENGL

///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "query/timer_query.h"

#include "device/device_utility.h"
#include "gl/device/win/device_gl.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///*************************************************************************************************
///	タイマークエリー
///*************************************************************************************************

CTimerQuery::CTimerQuery()
{
}

CTimerQuery::~CTimerQuery()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		CTimerQuery::IsEnable() const
{
	return (m_counter.query[0] != 0);
}

///-------------------------------------------------------------------------------------------------
/// 初期化
b8		CTimerQuery::Initialize()
{
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	m_counter.Init();
	// クエリ生成
	pContext->GenQueries(QUERY_PREF_NUM, m_counter.query);
	return (m_counter.query[0] != 0);
}

///-------------------------------------------------------------------------------------------------
/// 終了
void	CTimerQuery::Finalize()
{
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	// クエリ破棄
	pContext->DeleteQueries(QUERY_PREF_NUM, m_counter.query);
	m_counter.Init();
}

///-------------------------------------------------------------------------------------------------
/// タイマー取得（秒）
f64		CTimerQuery::GetResultTimeSec() const
{
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	u64 gpuTimer = 0;
	u8 queryIdx = m_counter.queryIdx;
	u8 waitCount = 0;
	while (waitCount < QUERY_PREF_NUM)
	{
		pContext->GetQueryObjectui64v(&m_counter.query[queryIdx], GL_QUERY_RESULT_NO_WAIT, &gpuTimer);
		if (gpuTimer != 0){ break; }
		queryIdx = GFX_COUNTER::CountUp(queryIdx);
		waitCount++;
	}
	return SCast<f64>(gpuTimer) / CTimerQuery::GetFrequency();
}



///-------------------------------------------------------------------------------------------------
/// 周波数取得
f64		CTimerQuery::GetFrequency()
{
	// nano秒らしい
	return 1000000000.0;
}

///-------------------------------------------------------------------------------------------------
/// 周波数計測開始
void	CTimerQuery::FrequencyBgn()
{
	// openGLでは使わない
	libAlert(0);
}

///-------------------------------------------------------------------------------------------------
/// 周波数計測終了
void	CTimerQuery::FrequencyEnd()
{
	// openGLでは使わない
	libAlert(0);
}

POISON_END

#endif // LIB_GFX_OPENGL

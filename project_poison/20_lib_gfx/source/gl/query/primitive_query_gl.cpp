﻿
#ifdef LIB_GFX_OPENGL

///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "query/primitive_query.h"

#include "device/device_utility.h"
#include "gl/device/win/device_gl.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///*************************************************************************************************
///	プリミティブクエリー
///*************************************************************************************************

CPrimitiveQuery::CPrimitiveQuery()
{
}

CPrimitiveQuery::~CPrimitiveQuery()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		CPrimitiveQuery::IsEnable() const
{
	return (m_counter.query[0] != 0);
}

///-------------------------------------------------------------------------------------------------
/// 初期化
b8		CPrimitiveQuery::Initialize()
{
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	m_counter.Init();
	// クエリ生成
	pContext->GenQueries(QUERY_PREF_NUM, m_counter.query);
	return (m_counter.query[0] != 0);
}

///-------------------------------------------------------------------------------------------------
/// 終了
void	CPrimitiveQuery::Finalize()
{
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	// クエリ破棄
	pContext->DeleteQueries(QUERY_PREF_NUM, m_counter.query);
	m_counter.Init();
}

///-------------------------------------------------------------------------------------------------
/// プリミティブ結果取得
u64		CPrimitiveQuery::GetResultPrimitive() const
{
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	u64 primitiveCount = 0;
	u8 queryIdx = m_counter.queryIdx;
	u8 waitCount = 0;
	while (waitCount < QUERY_PREF_NUM)
	{
		pContext->GetQueryObjectui64v(&m_counter.query[queryIdx], GL_QUERY_RESULT_NO_WAIT, &primitiveCount);
		if (primitiveCount != 0){ break; }
		queryIdx = GFX_COUNTER::CountUp(queryIdx);
		waitCount++;
	}
	return primitiveCount;
}


POISON_END

#endif // LIB_GFX_OPENGL

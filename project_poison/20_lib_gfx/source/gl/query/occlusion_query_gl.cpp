﻿
#ifdef LIB_GFX_OPENGL

///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "query/occlusion_query.h"

#include "device/device_utility.h"
#include "gl/device/win/device_gl.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///*************************************************************************************************
///	オクルージョンクエリー
///*************************************************************************************************

COcclusionQuery::COcclusionQuery()
	: m_type(GFX_PREDICATE_OCCL_CHEKER)
{
}

COcclusionQuery::~COcclusionQuery()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		COcclusionQuery::IsEnable() const
{
	return (m_counter.predicate[0] != 0);
}

///-------------------------------------------------------------------------------------------------
/// 初期化
b8		COcclusionQuery::Initialize(GFX_PREDICATE_QUERY_TYPE _type)
{
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	m_counter.Init();
	m_type = _type;
	// クエリ生成
	pContext->GenQueries(QUERY_PREF_NUM, m_counter.predicate);
	return (m_counter.predicate[0] != 0);
}

///-------------------------------------------------------------------------------------------------
/// 終了
void	COcclusionQuery::Finalize()
{
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	// クエリ破棄
	pContext->DeleteQueries(QUERY_PREF_NUM, m_counter.predicate);
	m_counter.Init();
}

///-------------------------------------------------------------------------------------------------
/// オクルージョン結果取得
b8		COcclusionQuery::GetResultOcclusion() const
{
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	libAssert(m_type == GFX_PREDICATE_OCCL_CHEKER);
	u32 enable = 0;
	u32 occlusion = 0;
	u8 queryIdx = m_counter.predicateIdx;
	u8 waitCount = 0;
	while (waitCount < QUERY_PREF_NUM)
	{
		pContext->GetQueryObjectuiv(&m_counter.predicate[queryIdx], GL_QUERY_RESULT_AVAILABLE, &enable);
		if (enable != 0)
		{
			pContext->GetQueryObjectuiv(&m_counter.predicate[queryIdx], GL_QUERY_RESULT, &occlusion);
			break;
		}
		queryIdx = GFX_COUNTER::CountUp(queryIdx);
		waitCount++;
	}
	return (occlusion == 0);
}

///-------------------------------------------------------------------------------------------------
/// ピクセルカウント結果取得
u64		COcclusionQuery::GetResultPixelCount() const
{
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	libAssert(m_type == GFX_PREDICATE_PIXEL_COUNTER);
	u32 enable = 0;
	//s32 pixelCounts32 = 0;
	//u32 pixelCountu32 = 0;
	//s64 pixelCounts64 = 0;
	u64 pixelCountu64 = 0;
	u8 queryIdx = m_counter.predicateIdx;
	u8 waitCount = 0;
	while (waitCount < QUERY_PREF_NUM)
	{
		pContext->GetQueryObjectuiv(&m_counter.predicate[queryIdx], GL_QUERY_RESULT_AVAILABLE, &enable);
		if (enable != 0)
		{
			//pContext->GetQueryObjectiv(&m_counter.predicate[queryIdx], GL_QUERY_RESULT, &pixelCounts32);
			//pContext->GetQueryObjectuiv(&m_counter.predicate[queryIdx], GL_QUERY_RESULT, &pixelCountu32);
			//pContext->GetQueryObjecti64v(&m_counter.predicate[queryIdx], GL_QUERY_RESULT, &pixelCounts64);
			pContext->GetQueryObjectui64v(&m_counter.predicate[queryIdx], GL_QUERY_RESULT, &pixelCountu64);
			break;
		}
		queryIdx = GFX_COUNTER::CountUp(queryIdx);
		waitCount++;
	}
	//return pixelCounts32;
	//return pixelCountu32;
	//return pixelCounts64;
	return pixelCountu64;
}


POISON_END

#endif // LIB_GFX_OPENGL

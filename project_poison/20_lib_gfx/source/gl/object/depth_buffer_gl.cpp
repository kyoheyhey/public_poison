﻿
#ifdef LIB_GFX_OPENGL

///-------------------------------------------------------------------------------------------------
// include
#include "object/depth_buffer.h"

#include "device/device_utility.h"
#include "gl/device/win/device_gl.h"

#include "format/gfx_format_utility.h"
#include "gl/format/gfx_format_utility_gl.h"

#include "gl/utility/gfx_utility_gl.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
// static



///*************************************************************************************************
///	デプスバッファ
///*************************************************************************************************
CDepthBuffer::CDepthBuffer()
	: m_width(0)
	, m_height(0)
	, m_arrayNum(0)
	, m_format(GFX_FORMAT_UNKNOWN)
	, m_aa(GFX_AA_UNKNOWN)
{
}
CDepthBuffer::~CDepthBuffer()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		CDepthBuffer::IsEnable() const
{
	return (m_object.objectID != 0);
}

///-------------------------------------------------------------------------------------------------
///	生成
b8		CDepthBuffer::Create(u16 _width, u16 _height, GFX_IMAGE_FORMAT _format, GFX_ANTI_ALIASE _aa, u32 _num/* = 1*/, GFX_USAGE _usage/* = GFX_USAGE_NONE*/, GFX_BUFFER_ATTR _attr/* = GFX_BUFFER_ATTR_RO*/)
{
	u32 glFormat;
	CGfxFormatUtilityGL::ConvertFormat(glFormat, _format);
	u32 glAA = CGfxFormatUtility::ConvertAA(_aa);

	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	if (_attr == GFX_BUFFER_ATTR_CONSTANT)
	{
		// 生成
		pContext->GenRenderbuffers(1, &m_object.objectID);
		m_object.target = GL_RENDERBUFFER;
		// バインド
		pContext->BindRenderbuffer(m_object.target, &m_object.objectID);
		// ストレージ確保
		if (_aa <= GFX_AA_1X)
		{
			pContext->RenderbufferStorage(m_object.target, glFormat, _width, _height);
		}
		// MSAA用レンダーバッファ
		else
		{
			pContext->RenderbufferStorageMultisample(m_object.target, glAA, glFormat, _width, _height);
		}
		// アンバインド
		pContext->UnBindRenderbuffer(m_object.target);
	}
	else
	{
		// 生成
		pContext->GenTextures(1, &m_object.objectID);

		if (_aa <= GFX_AA_1X)
		{
			// バインド
			m_object.target = GL_TEXTURE_2D;
			pContext->BindTexture(m_object.target, &m_object.objectID);

			u32 glTexFormat;
			CGfxFormatUtilityGL::ConvertFormatToTexFormat(glTexFormat, _format);
			u32 glValType;
			CGfxFormatUtilityGL::ConvertFormatToValueType(glValType, _format);
			// ストレージ確保
			pContext->TexImage2D(
				m_object.target,// ターゲット
				0,				// ミットマップレベル
				glFormat,		// フォーマット
				_width,			// 幅
				_height,		// 高さ
				glTexFormat,	// データフォーマット
				glValType,		// データ型
				NULL			// データ
				);
		}
		// MSAA用レンダーバッファ
		else
		{
			// バインド
			m_object.target = GL_TEXTURE_2D_MULTISAMPLE;
			pContext->BindTexture(m_object.target, &m_object.objectID);

			// ストレージ確保
#if 0
			pContext->TexStorage2DMultisample(
				m_object.target,// ターゲット
				glAA,			// AA数
				glFormat,		// フォーマット
				_width,			// 幅
				_height			// 高さ
				);
#else
			pContext->TexImage2DMultisample(
				m_object.target,// ターゲット
				glAA,			// AA数
				glFormat,		// フォーマット
				_width,			// 幅
				_height			// 高さ
				);
#endif // 0
		}

		// デフォルトテクスチャパラメータ設定
		CGfxUtilityGL::SetDefaultTexParameter(m_object.target, _format, 1);

		// アンバインド
		pContext->UnBindTexture(m_object.target);
	}

	m_width = _width;
	m_height = _height;
	m_format = _format;
	m_aa = _aa;

	return (m_object.objectID != 0);
}

///-------------------------------------------------------------------------------------------------
///	破棄
void	CDepthBuffer::Release()
{
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	// 破棄
	if (m_object.target == GL_RENDERBUFFER)
	{
		pContext->DeleteRenderbuffers(1, &m_object.objectID);
	}
	else
	{
		pContext->DeleteTextures(1, &m_object.objectID);
	}
}


POISON_END


#endif // LIB_GFX_OPENGL

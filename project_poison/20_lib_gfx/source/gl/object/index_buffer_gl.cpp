﻿
#ifdef LIB_GFX_OPENGL

///-------------------------------------------------------------------------------------------------
// include
#include "object/index_buffer.h"

#include "device/device_utility.h"
#include "gl/device/win/device_gl.h"

#include "format/gfx_format_utility.h"
#include "gl/format/gfx_format_utility_gl.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
// static




///*************************************************************************************************
///	インデックスバッファ
///*************************************************************************************************
CIndexBuffer::CIndexBuffer()
	: m_elemSize(0)
	, m_elemNum(0)
{
}
CIndexBuffer::~CIndexBuffer()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		CIndexBuffer::IsEnable() const
{
	return (m_object.objectID != 0);
}

///-------------------------------------------------------------------------------------------------
///	生成
b8		CIndexBuffer::Create(const IDX_DESC& _desc, DRAW_CONTEXT* _pDrawContext/* = NULL*/)
{
	m_object.target = GL_ELEMENT_ARRAY_BUFFER;
	u32 usage;
	CGfxFormatUtilityGL::ConvertUsage(usage, _desc.usage);

	DRAW_CONTEXT* pContext = NULL;
	if (_pDrawContext)
	{
		pContext = _pDrawContext;
	}
	else
	{
		CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
		libAssert(pDevice);
		pContext = pDevice->GetContext();
	}
	libAssert(pContext);

	// 生成
	pContext->GenBuffers(1, &m_object.objectID);
	// バインド
	pContext->BindBuffer(m_object.target, &m_object.objectID);
	// データ設定
	pContext->BufferData(m_object.target, _desc.elementSize* _desc.elementNum, _desc.data, usage);
	// アンバインド
	pContext->UnBindBuffer(m_object.target);

	m_elemNum = _desc.elementNum;
	m_elemSize = _desc.elementSize;
	return true;
}

///-------------------------------------------------------------------------------------------------
///	破棄
void	CIndexBuffer::Release()
{
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	// 破棄
	pContext->DeleteBuffers(1, &m_object.objectID);
}

///-------------------------------------------------------------------------------------------------
///	更新
void	CIndexBuffer::Update(u32 _size, const void* _data, DRAW_CONTEXT* _pDrawContext/* = NULL*/)
{
	DRAW_CONTEXT* pContext = NULL;
	if (_pDrawContext)
	{
		pContext = _pDrawContext;
	}
	else
	{
		CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
		libAssert(pDevice);
		pContext = pDevice->GetContext();
	}
	libAssert(pContext);

	pContext->BindBuffer(m_object.target, &m_object.objectID);
	pContext->MapBuffer(m_object.target, _size, _data);
	pContext->UnBindBuffer(m_object.target);
}


POISON_END


#endif // LIB_GFX_OPENGL

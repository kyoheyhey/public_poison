﻿
#ifdef LIB_GFX_OPENGL

///-------------------------------------------------------------------------------------------------
// include
#include "object/sampler_state.h"

#include "device/device_utility.h"
#include "gl/device/win/device_gl.h"

#include "gl/format/gfx_format_utility_gl.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
// static



///*************************************************************************************************
///	サンプラーバッファ
///*************************************************************************************************
CSampler::CSampler()
{
}
CSampler::~CSampler()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		CSampler::IsEnable() const
{
	return (m_object.objectID != 0);
}

///-------------------------------------------------------------------------------------------------
/// 生成
b8		CSampler::Create(const SAMPLER_DESC& _desc, DRAW_CONTEXT* _pDrawContext/* = NULL*/)
{
	// ラップ指定
	u32 wrapX;
	CGfxFormatUtilityGL::ConvertSamplerWrap(wrapX, _desc.wrapX);
	u32 wrapY;
	CGfxFormatUtilityGL::ConvertSamplerWrap(wrapY, _desc.wrapY);
	u32 wrapZ;
	CGfxFormatUtilityGL::ConvertSamplerWrap(wrapZ, _desc.wrapZ);
	// フィルター指定
	u32 minFilter, magFilter;
	CGfxFormatUtilityGL::ConvertSamplerFilter(minFilter, magFilter, _desc.minFilter, _desc.magFilter, _desc.mipFilter);
	// 比較式指定
	u32 test;
	CGfxFormatUtilityGL::ConvertTestFunc(test, _desc.testFunc);

	DRAW_CONTEXT* pContext = NULL;
	if (_pDrawContext)
	{
		pContext = _pDrawContext;
	}
	else
	{
		CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
		libAssert(pDevice);
		pContext = pDevice->GetContext();
	}
	libAssert(pContext);

	// オブジェクトID生成
	pContext->GenSamplers(1, &m_object.objectID);

	// ラップ指定
	pContext->SamplerParameteri(&m_object.objectID, GL_TEXTURE_WRAP_S, wrapX);
	pContext->SamplerParameteri(&m_object.objectID, GL_TEXTURE_WRAP_T, wrapY);
	pContext->SamplerParameteri(&m_object.objectID, GL_TEXTURE_WRAP_R, wrapZ);

	// フィルター指定
	pContext->SamplerParameteri(&m_object.objectID, GL_TEXTURE_MIN_FILTER, minFilter);
	pContext->SamplerParameteri(&m_object.objectID, GL_TEXTURE_MAG_FILTER, magFilter);

	// LOD指定
	pContext->SamplerParameterf(&m_object.objectID, GL_TEXTURE_MIN_LOD, _desc.LODMin);
	pContext->SamplerParameterf(&m_object.objectID, GL_TEXTURE_MAX_LOD, _desc.LODMax);
	pContext->SamplerParameterf(&m_object.objectID, GL_TEXTURE_LOD_BIAS, _desc.LODBias);

	// 比較式指定
	pContext->SamplerParameteri(&m_object.objectID, GL_TEXTURE_COMPARE_MODE, GL_NONE);
	//pContext->SamplerParameteri(&m_object.objectID, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
	pContext->SamplerParameteri(&m_object.objectID, GL_TEXTURE_COMPARE_FUNC, test);

	// ボーダーカラー指定
	pContext->SamplerParameterfv(&m_object.objectID, GL_TEXTURE_BORDER_COLOR, _desc.borderColor);

	// 異方性フィルター数
	pContext->SamplerParameterf(&m_object.objectID, GL_TEXTURE_MAX_ANISOTROPY_EXT, SCast<f32>(_desc.anisotropy));

	return true;
}

///-------------------------------------------------------------------------------------------------
/// 破棄
void	CSampler::Release()
{
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	// オブジェクトID破棄
	pContext->DeleteSamplers(1, &m_object.objectID);
	new(this) CSampler();
}


POISON_END


#endif // LIB_GFX_OPENGL

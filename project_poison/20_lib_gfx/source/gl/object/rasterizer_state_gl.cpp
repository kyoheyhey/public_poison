﻿
#ifdef LIB_GFX_OPENGL

///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "object/rasterrizer_state.h"

#include "gl/format/gfx_format_utility_gl.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant



///*************************************************************************************************
///	ラスタライザーステート
///*************************************************************************************************
CRasterizer::CRasterizer()
{
}

CRasterizer::~CRasterizer()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		CRasterizer::IsEnable() const
{
	return m_object.isEnable;
}

///-------------------------------------------------------------------------------------------------
/// 生成
b8		CRasterizer::Create(const RASTERIZER_DESC& _desc)
{
	CGfxFormatUtilityGL::ConvertFrontFace(m_object.frontFace, _desc.frontFace);
	CGfxFormatUtilityGL::ConvertCullFace(m_object.cullFace, _desc.cullFace);
	m_object.depthBias = _desc.bepthBias;
	m_object.slopeScaledDepthBias = _desc.slopeScaledDepthBias;
	m_object.isMSAA = _desc.isMSAA;
	m_object.isEnable = true;
	m_desc = _desc;
	return true;
}

///-------------------------------------------------------------------------------------------------
/// 破棄
void	CRasterizer::Release()
{
	new(this) CRasterizer();
}

POISON_END

#endif // LIB_GFX_OPENGL

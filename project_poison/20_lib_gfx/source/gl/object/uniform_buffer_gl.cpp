﻿
#ifdef LIB_GFX_OPENGL

///-------------------------------------------------------------------------------------------------
// include
#include "object/uniform_buffer.h"

#include "device/device_utility.h"
#include "gl/device/win/device_gl.h"

#include "gl/format/gfx_format_utility_gl.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
// static



///*************************************************************************************************
///	定数バッファ
///*************************************************************************************************
CUniformBuffer::CUniformBuffer()
	: m_elemSize(0)
	, m_elemNum(0)
	, m_attr(GFX_BUFFER_ATTR_CONSTANT)
{
}

CUniformBuffer::~CUniformBuffer()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		CUniformBuffer::IsEnable() const
{
	return (m_object.objectID != 0);
}

///-------------------------------------------------------------------------------------------------
///	生成
b8		CUniformBuffer::Create(const UNIFORM_DESC& _desc, DRAW_CONTEXT* _pDrawContext/* = NULL*/)
{
	//u32 attr;
	//CGfxFormatUtilityGL::ConvertBufferAttr(attr, _desc.attr);
	u32 usage;
	CGfxFormatUtilityGL::ConvertUsage(usage, _desc.usage);

	DRAW_CONTEXT* pContext = NULL;
	if (_pDrawContext)
	{
		pContext = _pDrawContext;
	}
	else
	{
		CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
		libAssert(pDevice);
		pContext = pDevice->GetContext();
	}
	libAssert(pContext);

	m_object.target = GL_UNIFORM_BUFFER;
	pContext->GenBuffers(1, &m_object.objectID);
	pContext->BindBuffer(m_object.target, &m_object.objectID);
	pContext->BufferData(m_object.target, _desc.elementSize * _desc.elementNum, _desc.data, usage);
	pContext->UnBindBuffer(m_object.target);

	if (_desc.attr == GFX_BUFFER_ATTR_RO)
	{
		m_roObject.target = GL_TEXTURE_BUFFER;
		pContext->GenBuffers(1, &m_roObject.objectID);
		pContext->BindBuffer(m_roObject.target, &m_roObject.objectID);
		pContext->BufferData(m_roObject.target, _desc.elementSize * _desc.elementNum, _desc.data, usage);
		pContext->UnBindBuffer(m_roObject.target);
	}

	if (_desc.attr == GFX_BUFFER_ATTR_RW)
	{
		m_rwObject.target = GL_SHADER_STORAGE_BUFFER;
		pContext->GenBuffers(1, &m_rwObject.objectID);
		pContext->BindBuffer(m_rwObject.target, &m_rwObject.objectID);
		pContext->BufferData(m_rwObject.target, _desc.elementSize * _desc.elementNum, _desc.data, usage);
		pContext->UnBindBuffer(m_rwObject.target);
	}

	m_elemNum = _desc.elementNum;
	m_elemSize = _desc.elementSize;
	m_attr = _desc.attr;
	return true;
}

///-------------------------------------------------------------------------------------------------
///	破棄
void	CUniformBuffer::Release()
{
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	// 破棄
	pContext->DeleteBuffers(1, &m_object.objectID);
	pContext->DeleteBuffers(1, &m_roObject.objectID);
	pContext->DeleteBuffers(1, &m_rwObject.objectID);
}

///-------------------------------------------------------------------------------------------------
///	更新
void	CUniformBuffer::Update(u32 _size, const void* _data, DRAW_CONTEXT* _pDrawContext/* = NULL*/)
{
	DRAW_CONTEXT* pContext = NULL;
	if (_pDrawContext)
	{
		pContext = _pDrawContext;
	}
	else
	{
		CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
		libAssert(pDevice);
		pContext = pDevice->GetContext();
	}
	libAssert(pContext);

	pContext->BindBuffer(m_object.target, &m_object.objectID);
	pContext->MapBuffer(m_object.target, _size, _data);
	pContext->UnBindBuffer(m_object.target);
	if (m_attr == GFX_BUFFER_ATTR_RO)
	{
		pContext->BindBuffer(m_roObject.target, &m_roObject.objectID);
		pContext->MapBuffer(m_roObject.target, _size, _data);
		pContext->UnBindBuffer(m_roObject.target);
	}
	if (m_attr == GFX_BUFFER_ATTR_RW)
	{
		pContext->BindBuffer(m_rwObject.target, &m_rwObject.objectID);
		pContext->MapBuffer(m_rwObject.target, _size, _data);
		pContext->UnBindBuffer(m_rwObject.target);
	}
}


POISON_END


#endif // LIB_GFX_OPENGL

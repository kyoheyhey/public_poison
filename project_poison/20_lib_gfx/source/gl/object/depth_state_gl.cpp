﻿
#ifdef LIB_GFX_OPENGL

///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "object/depth_state.h"

#include "gl/format/gfx_format_utility_gl.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///*************************************************************************************************
///	デプスステンシルステート
///*************************************************************************************************
CDepthState::CDepthState()
{
}

CDepthState::~CDepthState()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		CDepthState::IsEnable() const
{
	return m_object.isEnable;
}

///-------------------------------------------------------------------------------------------------
/// 生成
b8		CDepthState::Create(const DEPTH_STATE_DESC& _desc, u32 _stencilRef/* = 0*/)
{
	m_object.isDepthTest = _desc.isDepthTest;
	m_object.isDepthWrite = _desc.isDepthWrite;
	CGfxFormatUtilityGL::ConvertTestFunc(m_object.depthTest, _desc.depthTest);

	m_object.isStencilTest = _desc.isStencilTest;
	m_object.isStencilWrite = _desc.isStencilWrite;
	m_object.stencilReadMask = _desc.stencilReadMask;
	m_object.stencilWriteMask = _desc.stencilWriteMask;
	CGfxFormatUtilityGL::ConvertTestFunc(m_object.stencilTest, _desc.stencilTest);
	CGfxFormatUtilityGL::ConvertStencilOp(m_object.stencilFailOp, _desc.stencilFailOp);
	CGfxFormatUtilityGL::ConvertStencilOp(m_object.stencilZFailOp, _desc.stencilZFailOp);
	CGfxFormatUtilityGL::ConvertStencilOp(m_object.stencilPassOp, _desc.stencilPassOp);
	m_object.isEnable = true;
	m_desc = _desc;
	return true;
}

///-------------------------------------------------------------------------------------------------
/// 破棄
void	CDepthState::Release()
{
	// 破棄
	new(this) CDepthState();
}



POISON_END

#endif // LIB_GFX_OPENGL

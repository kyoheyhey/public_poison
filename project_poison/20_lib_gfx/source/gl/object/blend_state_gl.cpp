﻿
#ifdef LIB_GFX_OPENGL

///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "object/blend_state.h"

#include "device/device_utility.h"
#include "gl/device/win/device_gl.h"
#include "gl/format/gfx_format_utility_gl.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///*************************************************************************************************
///	ブレンドステート
///*************************************************************************************************
CBlendState::CBlendState()
{
}

CBlendState::~CBlendState()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		CBlendState::IsEnable() const
{
	return m_object.isEnable;
}

///-------------------------------------------------------------------------------------------------
/// ステート生成関数
b8		BlendStateCreate(BLEND_STATE_OBJECT& _object, const BLEND_STATE_DESC* _pDesc, u32 _num)
{
	_object.isIndependent = (_num > 1);
	for (u32 slot = 0; slot < _num; slot++)
	{
		_object.state[slot].enable = _pDesc[slot].enable;
		CGfxFormatUtilityGL::ConvertBlendOp(_object.state[slot].alphaOp, _pDesc[slot].colorOp);
		CGfxFormatUtilityGL::ConvertBlendFunc(_object.state[slot].colorSrcFunc, _pDesc[slot].colorSrcFunc);
		CGfxFormatUtilityGL::ConvertBlendFunc(_object.state[slot].colorDstFunc, _pDesc[slot].colorDstFunc);
		CGfxFormatUtilityGL::ConvertBlendOp(_object.state[slot].alphaSrcFunc, _pDesc[slot].alphaOp);
		CGfxFormatUtilityGL::ConvertBlendFunc(_object.state[slot].alphaSrcFunc, _pDesc[slot].alphaSrcFunc);
		CGfxFormatUtilityGL::ConvertBlendFunc(_object.state[slot].alphaDstFunc, _pDesc[slot].alphaDstFunc);
		CGfxFormatUtilityGL::ConvertColorMask(_object.state[slot].colorMask, _pDesc[slot].colorMask);
	}
	_object.isEnable = true;
	return true;
}

///-------------------------------------------------------------------------------------------------
/// 生成
b8		CBlendState::Create(const BLEND_STATE_DESC& _desc)
{
	BlendStateCreate(m_object, &_desc, 1);
	memcpy(m_desc, &_desc, sizeof(_desc));
	return true;
}

b8		CBlendState::Create(const BLEND_STATE_DESC(&_desc)[RENDER_BUFFER_SLOT_NUM])
{
	BlendStateCreate(m_object, _desc, ARRAYOF(_desc));
	memcpy(m_desc, _desc, sizeof(_desc));
	return true;
}

///-------------------------------------------------------------------------------------------------
/// 破棄
void	CBlendState::Release()
{
	// 破棄
	new(this) CBlendState();
}



POISON_END

#endif // LIB_GFX_OPENGL

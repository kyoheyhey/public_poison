﻿
#ifdef LIB_GFX_OPENGL

///-------------------------------------------------------------------------------------------------
/// define
//#define USE_GL_GET_FUNC	// GL関数を使ったパラメータ取得


///-------------------------------------------------------------------------------------------------
/// include
#include "object/texture_buffer.h"

#include "object/color_buffer.h"
#include "object/depth_buffer.h"

#include "device/device_utility.h"
#include "gl/device/win/device_gl.h"

#include "format/gfx_format_utility.h"
#include "gl/format/gfx_format_utility_gl.h"

#include "gl/utility/gfx_utility_gl.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// static



///*************************************************************************************************
///	テクスチャバッファ
///*************************************************************************************************
CTextureBuffer::CTextureBuffer()
	: m_textureType(GFX_TEXTURE_UNKNOWN)
	, m_width(0)
	, m_height(0)
	, m_depth(0)
	, m_arrayNum(0)
	, m_mipLevels(0)
	, m_format(GFX_FORMAT_UNKNOWN)
{
}

CTextureBuffer::~CTextureBuffer()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		CTextureBuffer::IsEnable() const
{
	return (m_object.objectID != 0);
}

///-------------------------------------------------------------------------------------------------
///	取得
u32		CTextureBuffer::GetWidth(u32 _level/* = 0*/) const
{
#ifdef USE_GL_GET_FUNC
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	// バインド
	pContext->BindTexture(m_object.target, &m_object.objectID);
	// 値取得（※重たいからこれやめよぉ）
	s32 param = 0;
	pContext->GetTexLevelParameteriv(
		m_object.target,
		_level,
		GL_TEXTURE_WIDTH,
		&param
		);
	// アンバインド
	pContext->UnBindTexture(m_object.target);
	return SCast<u32>(param);
#else
	return max(1, m_width >> _level);
#endif // USE_GL_GET_FUNC
}
u32		CTextureBuffer::GetWidth(GFX_CUBEMAP_FACE _face, u32 _level/* = 0*/) const
{
#ifdef USE_GL_GET_FUNC
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	// フェイスタイプ取得
	u32 glFace;
	CGfxFormatUtilityGL::ConvertCubemapFace(glFace, _face);
	// バインド
	pContext->BindTexture(m_object.target, &m_object.objectID);
	// 値取得（※重たいからこれやめよぉ）
	s32 param = 0;
	pContext->GetTexLevelParameteriv(
		glFace,
		_level,
		GL_TEXTURE_WIDTH,
		&param
		);
	// アンバインド
	pContext->UnBindTexture(m_object.target);
	return SCast<u32>(param);
#else
	return max(1, m_width >> _level);
#endif // USE_GL_GET_FUNC
}

u32		CTextureBuffer::GetHeight(u32 _level/* = 0*/) const
{
#ifdef USE_GL_GET_FUNC
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	// バインド
	pContext->BindTexture(m_object.target, &m_object.objectID);
	// 値取得（※重たいからこれやめよぉ）
	s32 param = 0;
	pContext->GetTexLevelParameteriv(
		GL_TEXTURE_2D,
		_level,
		GL_TEXTURE_HEIGHT,
		&param
		);
	// アンバインド
	pContext->UnBindTexture(m_object.target);
	return SCast<u32>(param);
#else
	return max(1, m_height >> _level);
#endif // USE_GL_GET_FUNC
}
u32		CTextureBuffer::GetHeight(GFX_CUBEMAP_FACE _face, u32 _level/* = 0*/) const
{
#ifdef USE_GL_GET_FUNC
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	// フェイスタイプ取得
	u32 glFace;
	CGfxFormatUtilityGL::ConvertCubemapFace(glFace, _face);
	// バインド
	pContext->BindTexture(m_object.target, &m_object.objectID);
	// 値取得（※重たいからこれやめよぉ）
	s32 param = 0;
	pContext->GetTexLevelParameteriv(
		glFace,
		_level,
		GL_TEXTURE_HEIGHT,
		&param
		);
	// アンバインド
	pContext->UnBindTexture(m_object.target);
	return SCast<u32>(param);
#else
	return max(1, m_height >> _level);
#endif // USE_GL_GET_FUNC
}

GFX_IMAGE_FORMAT	CTextureBuffer::GetFormat(u32 _level/* = 0*/) const
{
#ifdef USE_GL_GET_FUNC
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	// バインド
	pContext->BindTexture(m_object.target, &m_object.objectID);
	// 値取得（※重たいからこれやめよぉ）
	s32 param = 0;
	pContext->GetTexLevelParameteriv(
		GL_TEXTURE_2D,
		_level,
		GL_TEXTURE_INTERNAL_FORMAT,
		&param
		);
	// アンバインド
	pContext->UnBindTexture(m_object.target);
	GFX_IMAGE_FORMAT format;
	CGfxFormatUtilityGL::ConvertFormat(format, SCast<u32>(param));
	return format;
#else
	return m_format;
#endif // USE_GL_GET_FUNC
}
GFX_IMAGE_FORMAT	CTextureBuffer::GetFormat(GFX_CUBEMAP_FACE _face, u32 _level/* = 0*/) const
{
#ifdef USE_GL_GET_FUNC
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	// フェイスタイプ取得
	u32 glFace;
	CGfxFormatUtilityGL::ConvertCubemapFace(glFace, _face);
	// バインド
	pContext->BindTexture(m_object.target, &m_object.objectID);
	// 値取得（※重たいからこれやめよぉ）
	s32 param = 0;
	pContext->GetTexLevelParameteriv(
		glFace,
		_level,
		GL_TEXTURE_INTERNAL_FORMAT,
		&param
		);
	// アンバインド
	pContext->UnBindTexture(m_object.target);
	GFX_IMAGE_FORMAT format;
	CGfxFormatUtilityGL::ConvertFormat(format, SCast<u32>(param));
	return format;
#else
	return m_format;
#endif // USE_GL_GET_FUNC
}


///-------------------------------------------------------------------------------------------------
///	２Ｄテクスチャ生成
b8		CTextureBuffer::Create2D(const TEX_DESC& _desc)
{
	u32 glFormat;
	CGfxFormatUtilityGL::ConvertFormat(glFormat, _desc.format);
	u32 glTexFormat;
	CGfxFormatUtilityGL::ConvertFormatToTexFormat(glTexFormat, _desc.format);
	u32 glValType;
	CGfxFormatUtilityGL::ConvertFormatToValueType(glValType, _desc.format);

	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	// 生成
	pContext->GenTextures(1, &m_object.objectID);

	// 2Dテクスチャ
	if (_desc.arrayNum <= 1)
	{
		m_object.target = GL_TEXTURE_2D;
		// バインド
		pContext->BindTexture(m_object.target, &m_object.objectID);

		// ストアに情報設定
		pContext->TexStorage2D(
			m_object.target,	// ターゲット
			_desc.mipLevels - 1,// ミットマップレベル
			glFormat,			// フォーマット
			_desc.width,		// 幅
			_desc.height		// 高さ
			);

		u32 idxCount = 0;
		for (u32 level = 0; level < _desc.mipLevels; level++)
		{
			// mipmapのサイズは２の乗数なので2で割ってく
			u32 mipWidth = max(1, _desc.width >> level);
			u32 mipHeight = max(1, _desc.height >> level);
			TEX_DATA_DESC dataDesc = _desc.pData[idxCount];
			// データ更新
			pContext->TexSubImage2D(
				m_object.target,// ターゲット
				level,			// ミットマップレベル
				0, 0,			// オフセット
				mipWidth,		// 幅
				mipHeight,		// 高さ
				glTexFormat,	// データフォーマット
				glValType,		// データ型
				dataDesc.data	// データ
				);
			idxCount++;
		}
	}
	// 2D配列テクスチャ
	else
	{
		m_object.target = GL_TEXTURE_2D_ARRAY;
		// バインド
		pContext->BindTexture(m_object.target, &m_object.objectID);

		// ストアに情報設定
		pContext->TexStorage3D(
			m_object.target,	// ターゲット
			_desc.mipLevels - 1,// ミットマップレベル
			glFormat,			// フォーマット
			_desc.width,		// 幅
			_desc.height,		// 高さ
			_desc.arrayNum		// 奥行き（配列数）
			);

		u32 idxCount = 0;
		for (u32 arrayIdx = 0; arrayIdx < _desc.arrayNum; arrayIdx++)
		{
			// mipmap設定
			for (u32 level = 0; level < _desc.mipLevels; level++)
			{
				// mipmapのサイズは２の乗数なので2で割ってく
				u32 mipWidth = max(1, _desc.width >> level);
				u32 mipHeight = max(1, _desc.height >> level);
				TEX_DATA_DESC dataDesc = _desc.pData[idxCount];
				// データ更新
				pContext->TexSubImage3D(
					m_object.target,// ターゲット
					level,			// ミットマップレベル
					0, 0, arrayIdx,	// オフセット
					mipWidth,		// 幅
					mipHeight,		// 高さ
					1,				// 奥行き（配列テクスチャなので１）
					glTexFormat,	// データフォーマット
					glValType,		// データ型
					dataDesc.data	// データ
					);
				idxCount++;
			}
		}
	}

	// デフォルトテクスチャパラメータ設定
	CGfxUtilityGL::SetDefaultTexParameter(m_object.target, _desc.format, _desc.mipLevels);

	// アンバインド
	pContext->UnBindTexture(m_object.target);


	m_textureType = _desc.arrayNum <= 1 ? GFX_TEXTURE_2D : GFX_TEXTURE_2DARRAY;
	m_width = _desc.width;
	m_height = _desc.height;
	m_arrayNum = _desc.arrayNum;
	m_mipLevels = _desc.mipLevels;
	m_format = _desc.format;

	return (m_object.objectID != 0);
}

///-------------------------------------------------------------------------------------------------
///	キューブマップ生成
b8		CTextureBuffer::CreateCube(const TEX_DESC& _desc)
{
	u32 glFormat;
	CGfxFormatUtilityGL::ConvertFormat(glFormat, _desc.format);
	u32 glTexFormat;
	CGfxFormatUtilityGL::ConvertFormatToTexFormat(glTexFormat, _desc.format);
	u32 glValType;
	CGfxFormatUtilityGL::ConvertFormatToValueType(glValType, _desc.format);

	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	// 生成
	pContext->GenTextures(1, &m_object.objectID);

	// キューブマップテクスチャ
	if (_desc.arrayNum <= 1)
	{
		m_object.target = GL_TEXTURE_CUBE_MAP;
		// バインド
		pContext->BindTexture(m_object.target, &m_object.objectID);

		for (u32 cubeIdx = 0; cubeIdx < GFX_CUBEMAP_FACE_TYPE_NUM; cubeIdx++)
		{
			u32 cubeFace;
			CGfxFormatUtilityGL::ConvertCubemapFace(cubeFace, SCast<GFX_CUBEMAP_FACE>(cubeIdx));
			// ストアに情報設定
			pContext->TexStorage2D(
				cubeFace,			// ターゲット
				_desc.mipLevels - 1,// ミットマップレベル
				glFormat,			// フォーマット
				_desc.width,		// 幅
				_desc.height		// 高さ
				);

			u32 idxCount = 0;
			for (u32 level = 0; level < _desc.mipLevels; level++)
			{
				// mipmapのサイズは２の乗数なので2で割ってく
				u32 mipWidth = max(1, _desc.width >> level);
				u32 mipHeight = max(1, _desc.height >> level);
				TEX_DATA_DESC dataDesc = _desc.pData[idxCount];
				// データ更新
				pContext->TexSubImage2D(
					cubeFace,		// ターゲット
					level,			// ミットマップレベル
					0, 0,			// オフセット
					mipWidth,		// 幅
					mipHeight,		// 高さ
					glTexFormat,	// データフォーマット
					glValType,		// データ型
					dataDesc.data	// データ
					);
				idxCount++;
			}
		}
	}
	// キューブマップ配列テクスチャ
	else
	{
		m_object.target = GL_TEXTURE_CUBE_MAP;
		// バインド
		pContext->BindTexture(m_object.target, &m_object.objectID);

		for (u32 cubeIdx = 0; cubeIdx < GFX_CUBEMAP_FACE_TYPE_NUM; cubeIdx++)
		{
			u32 cubeFace;
			CGfxFormatUtilityGL::ConvertCubemapFace(cubeFace, SCast<GFX_CUBEMAP_FACE>(cubeIdx));
			// ストアに情報設定
			pContext->TexStorage3D(
				cubeFace,			// ターゲット
				_desc.mipLevels - 1,// ミットマップレベル
				glFormat,			// フォーマット
				_desc.width,		// 幅
				_desc.height,		// 高さ
				_desc.arrayNum		// 奥行き（配列数）
				);

			u32 idxCount = 0;
			for (u32 arrayIdx = 0; arrayIdx < _desc.arrayNum; arrayIdx++)
			{
				// mipmap設定
				for (u32 level = 0; level < _desc.mipLevels; level++)
				{
					// mipmapのサイズは２の乗数なので2で割ってく
					u32 mipWidth = max(1, _desc.width >> level);
					u32 mipHeight = max(1, _desc.height >> level);
					TEX_DATA_DESC dataDesc = _desc.pData[idxCount];
					// データ更新
					pContext->TexSubImage3D(
						cubeFace,		// ターゲット
						level,			// ミットマップレベル
						0, 0, arrayIdx,	// オフセット
						mipWidth,		// 幅
						mipHeight,		// 高さ
						1,				// 奥行き（配列テクスチャなので１）
						glTexFormat,	// データフォーマット
						glValType,		// データ型
						dataDesc.data	// データ
						);
					idxCount++;
				}
			}
		}
	}

	// デフォルトテクスチャパラメータ設定
	CGfxUtilityGL::SetDefaultTexParameter(m_object.target, _desc.format, _desc.mipLevels);

	// アンバインド
	pContext->UnBindTexture(m_object.target);

	
	m_textureType = _desc.arrayNum <= 1 ? GFX_TEXTURE_CUBEMAP : GFX_TEXTURE_CUBEMAPARRAY;
	m_width = _desc.width;
	m_height = _desc.height;
	m_arrayNum = _desc.arrayNum;
	m_mipLevels = _desc.mipLevels;
	m_format = _desc.format;

	return (m_object.objectID != 0);
}


b8		CTextureBuffer::InitReferenceTarget(const CColorBuffer* _pClr, GFX_BUFFER_ATTR _attr/* = GFX_BUFFER_ATTR_RO*/)
{
	const COLOR_OBJECT& clrObj = _pClr->GetObject();
	if (clrObj.target != GL_RENDERBUFFER)
	{
		m_object.objectID = clrObj.objectID;
		m_object.target = clrObj.target;
		m_textureType = GFX_TEXTURE_COLORBUFFER;
		m_width = _pClr->GetWidth();
		m_height = _pClr->GetHeight();
		m_arrayNum = _pClr->GetAA();	// AA数をぶち込んどく
		m_mipLevels = 0;
		m_format = _pClr->GetFormat();
	}

	return (m_object.objectID != 0);
}

b8		CTextureBuffer::InitReferenceTarget(const CDepthBuffer* _pDep, GFX_BUFFER_ATTR _attr/* = GFX_BUFFER_ATTR_RO*/)
{
	const DEPTH_OBJECT& depObj = _pDep->GetObject();
	if (depObj.target != GL_RENDERBUFFER)
	{
		m_object.objectID = depObj.objectID;
		m_object.target = depObj.target;
		m_textureType = GFX_TEXTURE_DEPTHBUFFER;
		m_width = _pDep->GetWidth();
		m_height = _pDep->GetHeight();
		m_arrayNum = _pDep->GetAA();	// AA数をぶち込んどく
		m_mipLevels = 0;
		m_format = _pDep->GetFormat();
	}

	return (m_object.objectID != 0);
}


///-------------------------------------------------------------------------------------------------
///	破棄
void	CTextureBuffer::Release()
{
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	DRAW_CONTEXT* pContext = pDevice->GetContext();
	libAssert(pContext);

	// 破棄
	if (m_textureType != GFX_TEXTURE_COLORBUFFER && m_textureType != GFX_TEXTURE_DEPTHBUFFER)
	{
		pContext->DeleteTextures(1, &m_object.objectID);
	}
}


///-------------------------------------------------------------------------------------------------
///	更新
void	CTextureBuffer::Update(
	u32 _array/* = 0*/, u32 _level/* = 0*/,
	u32 _xoff/* = 0*/, u32 _yoff/* = 0*/, u32 _zoff/* = 0*/,
	const void* _pData/* = NULL*/,
	DRAW_CONTEXT* _pDrawContext/* = NULL*/
)
{
	// フォーマット取得
	u32 glTexFormat;
	CGfxFormatUtilityGL::ConvertFormatToTexFormat(glTexFormat, m_format);
	u32 glValType;
	CGfxFormatUtilityGL::ConvertFormatToValueType(glValType, m_format);

	// mipmapのサイズは２の乗数なので2で割ってく
	u32 mipWidth = max(1, m_width >> _level);
	u32 mipHeight = max(1, m_height >> _level);

	DRAW_CONTEXT* pContext = NULL;
	if (_pDrawContext)
	{
		pContext = _pDrawContext;
	}
	else
	{
		CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
		libAssert(pDevice);
		pContext = pDevice->GetContext();
	}
	libAssert(pContext);

	// バインド
	pContext->BindTexture(m_object.target, &m_object.objectID);
	if (_array <= 1)
	{
		pContext->TexSubImage2D(
			m_object.target,// ターゲット
			_level,			// ミットマップレベル
			_xoff, _yoff,	// オフセット
			mipWidth,		// 幅
			mipHeight,		// 高さ
			glTexFormat,	// データフォーマット
			glValType,		// データ型
			_pData			// データ
		);
	}
	else
	{
		pContext->TexSubImage3D(
			m_object.target,// ターゲット
			_level,			// ミットマップレベル
			_xoff, _yoff,	// オフセット
			_array,			// 配列オフセット
			mipWidth,		// 幅
			mipHeight,		// 高さ
			1,				// 奥行き（配列テクスチャなので１）
			glTexFormat,	// データフォーマット
			glValType,		// データ型
			_pData			// データ
		);
	}
	// アンバインド
	pContext->UnBindTexture(m_object.target);
}

void	CTextureBuffer::Update(
	GFX_CUBEMAP_FACE _face, u32 _level/* = 0*/,
	u32 _xoff/* = 0*/, u32 _yoff/* = 0*/, u32 _zoff/* = 0*/,
	const void* _pData/* = NULL*/,
	DRAW_CONTEXT* _pDrawContext/* = NULL*/
)
{
	// キューブマップターゲット取得
	u32 glFace;
	CGfxFormatUtilityGL::ConvertCubemapFace(glFace, _face);
	// フォーマット取得
	u32 glTexFormat;
	CGfxFormatUtilityGL::ConvertFormatToTexFormat(glTexFormat, m_format);
	u32 glValType;
	CGfxFormatUtilityGL::ConvertFormatToValueType(glValType, m_format);

	// mipmapのサイズは２の乗数なので2で割ってく
	u32 mipWidth = max(1, m_width >> _level);
	u32 mipHeight = max(1, m_height >> _level);

	DRAW_CONTEXT* pContext = NULL;
	if (_pDrawContext)
	{
		pContext = _pDrawContext;
	}
	else
	{
		CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
		libAssert(pDevice);
		pContext = pDevice->GetContext();
	}
	libAssert(pContext);

	// バインド
	pContext->BindTexture(m_object.target, &m_object.objectID);
	if (_zoff <= 1)
	{
		pContext->TexSubImage2D(
			glFace,			// ターゲット
			_level,			// ミットマップレベル
			_xoff, _yoff,	// オフセット
			mipWidth,		// 幅
			mipHeight,		// 高さ
			glTexFormat,	// データフォーマット
			glValType,		// データ型
			_pData			// データ
		);
	}
	else
	{
		pContext->TexSubImage3D(
			glFace,			// ターゲット
			_level,			// ミットマップレベル
			_xoff, _yoff,	// オフセット
			_zoff,			// 配列オフセット
			mipWidth,		// 幅
			mipHeight,		// 高さ
			1,				// 奥行き（配列テクスチャなので１）
			glTexFormat,	// データフォーマット
			glValType,		// データ型
			_pData			// データ
		);
	}
	// アンバインド
	pContext->UnBindTexture(m_object.target);
}


POISON_END


#endif // LIB_GFX_OPENGL

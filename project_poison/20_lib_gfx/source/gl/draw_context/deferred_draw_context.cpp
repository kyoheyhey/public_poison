﻿
//#ifdef LIB_GFX_DX11
#ifdef LIB_GFX_OPENGL

///-------------------------------------------------------------------------------------------------
/// define
#define DRAW_CMD_ALIGN	16
//#define DRAW_CMD_PRINT(...)		PRINT(__VA_ARGS__)
#define DRAW_CMD_PRINT(...)		(__VA_ARGS__)



///-------------------------------------------------------------------------------------------------
/// include
#include "device/device_def.h"
#include "gl/draw_context/deferred_draw_context.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant



CDeferredContext::CDeferredContext()
{
}

CDeferredContext::~CDeferredContext()
{
}


///-------------------------------------------------------------------------------------------------
/// 初期化
b8		CDeferredContext::Initialize(IAllocator* _pAllocator)
{
	m_pAllocator = _pAllocator;
	return true;
}

///-------------------------------------------------------------------------------------------------
/// 終了
void	CDeferredContext::Finalize()
{
	m_list.Clear();
	m_pAllocator = NULL;
}

///-------------------------------------------------------------------------------------------------
/// コマンドフラッシュ
void	CDeferredContext::ExecuteCommandList()
{
	CMD_DRAW_PACKET* pObj = m_list.GetTop();
	while (pObj)
	{
		pObj->func(pObj->argc, pObj->argv);
		
		pObj = pObj->GetNext();
	}
	m_list.Clear();

	DRAW_CMD_PRINT("Command Reset\n");
	DRAW_CMD_PRINT("***********************************\n");
}


///*************************************************************************************************
///	描画コマンド
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// その他
void	CDeferredContext::Enable(u32 _cap)
{
	u32 argc = 1;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_cap);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _cap;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->func = Cmd_Enable;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup Enable\n");
}

void	CDeferredContext::Enablei(u32 _slot, u32 _cap)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_slot) + sizeof(_cap);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _cap;
	pBufU32[1] = _slot;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->func = Cmd_Enablei;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup Enablei\n");
}

void	CDeferredContext::Disable(u32 _cap)
{
	u32 argc = 1;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_cap);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _cap;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->func = Cmd_Disable;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup Disable\n");
}

void	CDeferredContext::Disablei(u32 _slot, u32 _cap)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_slot) + sizeof(_cap);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _cap;
	pBufU32[1] = _slot;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->func = Cmd_Disablei;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup Disablei\n");
}


///-------------------------------------------------------------------------------------------------
/// バインド
void	CDeferredContext::BindBuffer(u32 _target, const u32* _pObjectID)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_target);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = _pObjectID;
	pPacket->func = Cmd_BindBuffer;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup BindBuffer\n");
}

void	CDeferredContext::BindBufferBase(u32 _target, u32 _slot, const u32* _pObjectID)
{
	u32 argc = 3;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_target) + sizeof(_slot);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;
	pBufU32[1] = _slot;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = _pObjectID;
	pPacket->func = Cmd_BindBufferBase;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup BindBufferBase\n");
}

void	CDeferredContext::BindFramebuffer(u32 _target, const u32* _pObjectID)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_target);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = _pObjectID;
	pPacket->func = Cmd_BindFramebuffer;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup BindFramebuffer\n");
}

void	CDeferredContext::BindRenderbuffer(u32 _target, const u32* _pObjectID)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_target);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = _pObjectID;
	pPacket->func = Cmd_BindRenderbuffer;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup BindRenderbuffer\n");
}

void	CDeferredContext::BindTexture(u32 _target, const u32* _pObjectID)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_target);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = _pObjectID;
	pPacket->func = Cmd_BindTexture;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup BindTexture\n");
}

void	CDeferredContext::ActiveTexture(u32 _slot)
{
	u32 argc = 1;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_slot);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _slot;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->func = Cmd_ActiveTexture;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup ActiveTexture\n");
}

void	CDeferredContext::BindSampler(u32 _slot, const u32* _pObjectID)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_slot);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _slot;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = _pObjectID;
	pPacket->func = Cmd_BindSampler;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup BindSampler\n");
}


///-------------------------------------------------------------------------------------------------
/// アンバインド
void	CDeferredContext::UnBindBuffer(u32 _target)
{
	u32 argc = 1;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_target);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->func = Cmd_UnBindBuffer;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup UnBindBuffer\n");
}

void	CDeferredContext::UnBindFramebuffer(u32 _target)
{
	u32 argc = 1;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_target);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->func = Cmd_UnBindFramebuffer;
	m_list.PushBack(pPacket);
}

void	CDeferredContext::UnBindRenderbuffer(u32 _target)
{
	u32 argc = 1;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_target);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->func = Cmd_UnBindRenderbuffer;
	m_list.PushBack(pPacket);
}

void	CDeferredContext::UnBindTexture(u32 _target)
{
	u32 argc = 1;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_target);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->func = Cmd_UnBindTexture;
	m_list.PushBack(pPacket);
}


///-------------------------------------------------------------------------------------------------
/// 生成
void	CDeferredContext::GenBuffers(u32 _num, u32* _pObjectID)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_num);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _num;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = _pObjectID;
	pPacket->func = Cmd_GenBuffers;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup GenBuffers\n");
}

void	CDeferredContext::GenFramebuffers(u32 _num, u32* _pObjectID)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_num);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _num;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = _pObjectID;
	pPacket->func = Cmd_GenFramebuffers;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup GenFramebuffers\n");
}

void	CDeferredContext::GenRenderbuffers(u32 _num, u32* _pObjectID)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_num);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _num;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = _pObjectID;
	pPacket->func = Cmd_GenRenderbuffers;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup GenRenderbuffers\n");
}

void	CDeferredContext::GenTextures(u32 _num, u32* _pObjectID)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_num);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _num;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = _pObjectID;
	pPacket->func = Cmd_GenTextures;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup GenTextures\n");
}

void	CDeferredContext::GenSamplers(u32 _num, u32* _pObjectID)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_num);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _num;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = _pObjectID;
	pPacket->func = Cmd_GenSamplers;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup GenSamplers\n");
}

void	CDeferredContext::GenQueries(u32 _num, u32* _pQueryID)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_num);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _num;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = _pQueryID;
	pPacket->func = Cmd_GenQueries;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup GenQueries\n");
}


///-------------------------------------------------------------------------------------------------
/// 破棄
void	CDeferredContext::DeleteBuffers(u32 _num, u32* _pObjectID)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_num);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _num;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = _pObjectID;
	pPacket->func = Cmd_DeleteBuffers;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup DeleteBuffers\n");
}

void	CDeferredContext::DeleteFramebuffers(u32 _num, u32* _pObjectID)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_num);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _num;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = _pObjectID;
	pPacket->func = Cmd_DeleteFramebuffers;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup DeleteFramebuffers\n");
}

void	CDeferredContext::DeleteRenderbuffers(u32 _num, u32* _pObjectID)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_num);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _num;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = _pObjectID;
	pPacket->func = Cmd_DeleteRenderbuffers;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup DeleteRenderbuffers\n");
}

void	CDeferredContext::DeleteTextures(u32 _num, u32* _pObjectID)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_num);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _num;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = _pObjectID;
	pPacket->func = Cmd_DeleteTextures;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup DeleteTextures\n");
}

void	CDeferredContext::DeleteSamplers(u32 _num, u32* _pObjectID)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_num);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _num;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = _pObjectID;
	pPacket->func = Cmd_DeleteSamplers;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup DeleteSamplers\n");
}

void	CDeferredContext::DeleteQueries(u32 _num, u32* _pQueryID)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_num);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _num;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = _pQueryID;
	pPacket->func = Cmd_DeleteQueries;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup DeleteQueries\n");
}


///-------------------------------------------------------------------------------------------------
/// ストレージ確保
void	CDeferredContext::BufferData(u32 _target, u32 _size, const void* _pData, u32 _usage)
{
	u32 argc = 4;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_target) + sizeof(_size) + sizeof(_usage) + _size;
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;
	pBufU32[1] = _size;
	pBufU32[2] = _usage;
	void* pBufVOID = PCast<void*>(&pBufU32[3]);
	memcpy(pBufVOID, _pData, _size);

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = pBufVOID;
	pPacket->argv[3] = &pBufU32[2];
	pPacket->func = Cmd_BufferData;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup BufferData\n");
}

void	CDeferredContext::RenderbufferStorage(u32 _target, u32 _format, u32 _width, u32 _height)
{
	u32 argc = 4;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_target) + sizeof(_format) + sizeof(_width) + sizeof(_height);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;
	pBufU32[1] = _format;
	pBufU32[2] = _width;
	pBufU32[3] = _height;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = &pBufU32[2];
	pPacket->argv[3] = &pBufU32[3];
	pPacket->func = Cmd_RenderbufferStorage;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup RenderbufferStorage\n");
}

void	CDeferredContext::RenderbufferStorageMultisample(u32 _target, u32 _aa, u32 _format, u32 _width, u32 _height)
{
	u32 argc = 5;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_target) + sizeof(_aa) + sizeof(_format) + sizeof(_width) + sizeof(_height);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;
	pBufU32[1] = _aa;
	pBufU32[2] = _format;
	pBufU32[3] = _width;
	pBufU32[4] = _height;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = &pBufU32[2];
	pPacket->argv[3] = &pBufU32[3];
	pPacket->argv[4] = &pBufU32[4];
	pPacket->func = Cmd_RenderbufferStorageMultisample;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup RenderbufferStorageMultisample\n");
}

void	CDeferredContext::TexImage2D(u32 _target, u32 _level, u32 _format, u32 _width, u32 _height, u32 _texFormat, u32 _valType, const void* _pData)
{
	u32 argc = 8;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize =
		sizeof(_target) + sizeof(_level) + sizeof(_format) +
		sizeof(_width) + sizeof(_height) +
		sizeof(_texFormat) + sizeof(_valType);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;
	pBufU32[1] = _level;
	pBufU32[2] = _format;
	pBufU32[3] = _width;
	pBufU32[4] = _height;
	pBufU32[5] = _texFormat;
	pBufU32[6] = _valType;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = &pBufU32[2];
	pPacket->argv[3] = &pBufU32[3];
	pPacket->argv[4] = &pBufU32[4];
	pPacket->argv[5] = &pBufU32[5];
	pPacket->argv[6] = &pBufU32[6];
	pPacket->argv[7] = _pData;	// テクスチャデータはデカすぎるのでちゃんとどっかで保持して
	pPacket->func = Cmd_TexImage2D;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup TexImage2D\n");
}

void	CDeferredContext::TexImage2DMultisample(u32 _target, u32 _aa, u32 _format, u32 _width, u32 _height)
{
	u32 argc = 5;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize =
		sizeof(_target) + sizeof(_aa) + sizeof(_format) +
		sizeof(_width) + sizeof(_height);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;
	pBufU32[1] = _aa;
	pBufU32[2] = _format;
	pBufU32[3] = _width;
	pBufU32[4] = _height;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = &pBufU32[2];
	pPacket->argv[3] = &pBufU32[3];
	pPacket->argv[4] = &pBufU32[4];
	pPacket->func = Cmd_TexImage2DMultisample;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup TexImage2DMultisample\n");
}

void	CDeferredContext::TexImage3D(u32 _target, u32 _level, u32 _format, u32 _width, u32 _height, u32 _depth, u32 _texFormat, u32 _valType, const void* _pData)
{
	u32 argc = 9;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize =
		sizeof(_target) + sizeof(_level) + sizeof(_format) +
		sizeof(_width) + sizeof(_height) + sizeof(_depth) +
		sizeof(_texFormat) + sizeof(_valType);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;
	pBufU32[1] = _level;
	pBufU32[2] = _format;
	pBufU32[3] = _width;
	pBufU32[4] = _height;
	pBufU32[5] = _depth;
	pBufU32[6] = _texFormat;
	pBufU32[7] = _valType;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = &pBufU32[2];
	pPacket->argv[3] = &pBufU32[3];
	pPacket->argv[4] = &pBufU32[4];
	pPacket->argv[5] = &pBufU32[5];
	pPacket->argv[6] = &pBufU32[6];
	pPacket->argv[7] = &pBufU32[7];
	pPacket->argv[8] = _pData;	// テクスチャデータはデカすぎるのでちゃんとどっかで保持して
	pPacket->func = Cmd_TexImage3D;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup TexImage3D\n");
}

void	CDeferredContext::TexStorage2D(u32 _target, u32 _level, u32 _format, u32 _width, u32 _height)
{
	u32 argc = 5;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize =
		sizeof(_target) + sizeof(_level) + sizeof(_format) +
		sizeof(_width) + sizeof(_height);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;
	pBufU32[1] = _level;
	pBufU32[2] = _format;
	pBufU32[3] = _width;
	pBufU32[4] = _height;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = &pBufU32[2];
	pPacket->argv[3] = &pBufU32[3];
	pPacket->argv[4] = &pBufU32[4];
	pPacket->func = Cmd_TexStorage2D;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup TexStorage2D\n");
}

void	CDeferredContext::TexStorage2DMultisample(u32 _target, u32 _aa, u32 _format, u32 _width, u32 _height)
{
	u32 argc = 5;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize =
		sizeof(_target) + sizeof(_aa) + sizeof(_format) +
		sizeof(_width) + sizeof(_height);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;
	pBufU32[1] = _aa;
	pBufU32[2] = _format;
	pBufU32[3] = _width;
	pBufU32[4] = _height;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = &pBufU32[2];
	pPacket->argv[3] = &pBufU32[3];
	pPacket->argv[4] = &pBufU32[4];
	pPacket->func = Cmd_TexStorage2DMultisample;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup TexStorage2DMultisample\n");
}

void	CDeferredContext::TexStorage3D(u32 _target, u32 _level, u32 _format, u32 _width, u32 _height, u32 _depth)
{
	u32 argc = 6;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize =
		sizeof(_target) + sizeof(_level) + sizeof(_format) +
		sizeof(_width) + sizeof(_height) + sizeof(_depth);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;
	pBufU32[1] = _level;
	pBufU32[2] = _format;
	pBufU32[3] = _width;
	pBufU32[4] = _height;
	pBufU32[5] = _depth;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = &pBufU32[2];
	pPacket->argv[3] = &pBufU32[3];
	pPacket->argv[4] = &pBufU32[4];
	pPacket->argv[5] = &pBufU32[5];
	pPacket->func = Cmd_TexStorage3D;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup TexStorage3D\n");
}


///-------------------------------------------------------------------------------------------------
/// 更新
void	CDeferredContext::MapBuffer(u32 _target, u32 _size, const void* _pData)
{
	u32 argc = 3;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_target) + sizeof(_size) + _size;
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;
	pBufU32[1] = _size;
	void* pBufVOID = PCast<u8*>(&pBufU32[2]);
	memcpy(pBufVOID, _pData, _size);

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = pBufVOID;
	pPacket->func = Cmd_MapBuffer;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup MapBuffer\n");
}

void	CDeferredContext::TexSubImage2D(u32 _target, u32 _level, u32 _xoff, u32 _yoff, u32 _mipWidth, u32 _mipHeight, u32 _texFormat, u32 _valType, const void* _pData)
{
	u32 argc = 9;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize =
		sizeof(_target) + sizeof(_level) +
		sizeof(_xoff) + sizeof(_yoff) +
		sizeof(_mipWidth) + sizeof(_mipHeight) +
		sizeof(_texFormat) + sizeof(_valType);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;
	pBufU32[1] = _level;
	pBufU32[2] = _xoff;
	pBufU32[3] = _yoff;
	pBufU32[4] = _mipWidth;
	pBufU32[5] = _mipHeight;
	pBufU32[6] = _texFormat;
	pBufU32[7] = _valType;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = &pBufU32[2];
	pPacket->argv[3] = &pBufU32[3];
	pPacket->argv[4] = &pBufU32[4];
	pPacket->argv[5] = &pBufU32[5];
	pPacket->argv[6] = &pBufU32[6];
	pPacket->argv[7] = &pBufU32[7];
	pPacket->argv[8] = _pData;	// テクスチャデータはデカすぎるのでちゃんとどっかで保持して
	pPacket->func = Cmd_TexSubImage2D;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup TexSubImage2D\n");
}

void	CDeferredContext::TexSubImage3D(u32 _target, u32 _level, u32 _xoff, u32 _yoff, u32 _zoff, u32 _mipWidth, u32 _mipHeight, u32 _mipDepth, u32 _texFormat, u32 _valType, const void* _pData)
{
	u32 argc = 11;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize =
		sizeof(_target) + sizeof(_level) +
		sizeof(_xoff) + sizeof(_yoff) + sizeof(_zoff) +
		sizeof(_mipWidth) + sizeof(_mipHeight) + sizeof(_mipDepth) +
		sizeof(_texFormat) + sizeof(_valType);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;
	pBufU32[1] = _level;
	pBufU32[2] = _xoff;
	pBufU32[3] = _yoff;
	pBufU32[4] = _zoff;
	pBufU32[5] = _mipWidth;
	pBufU32[6] = _mipHeight;
	pBufU32[7] = _mipDepth;
	pBufU32[8] = _texFormat;
	pBufU32[9] = _valType;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = &pBufU32[2];
	pPacket->argv[3] = &pBufU32[3];
	pPacket->argv[4] = &pBufU32[4];
	pPacket->argv[5] = &pBufU32[5];
	pPacket->argv[6] = &pBufU32[6];
	pPacket->argv[7] = &pBufU32[7];
	pPacket->argv[8] = &pBufU32[8];
	pPacket->argv[9] = &pBufU32[9];
	pPacket->argv[10] = _pData;	// テクスチャデータはデカすぎるのでちゃんとどっかで保持して
	pPacket->func = Cmd_TexSubImage3D;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup TexSubImage3D\n");
}


///-------------------------------------------------------------------------------------------------
/// 情報取得
void	CDeferredContext::CheckFramebufferStatus(const u32* _pObjectID)
{
	u32 argc = 1;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = 0;
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = _pObjectID;
	pPacket->func = Cmd_CheckFramebufferStatus;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup CheckFramebufferStatus\n");
}

void	CDeferredContext::GetTexLevelParameteriv(u32 _target, u32 _level, u32 _name, s32* _pParam)
{
	// 未対応
	libAlert(0);
}

void	CDeferredContext::GetQueryObjectiv(const u32* _pQueryID, u32 _name, s32* _pParam)
{
	// 未対応
	libAlert(0);
}

void	CDeferredContext::GetQueryObjectuiv(const u32* _pQueryID, u32 _name, u32* _pParam)
{
	// 未対応
	libAlert(0);
}

void	CDeferredContext::GetQueryObjecti64v(const u32* _pQueryID, u32 _name, s64* _pParam)
{
	// 未対応
	libAlert(0);
}

void	CDeferredContext::GetQueryObjectui64v(const u32* _pQueryID, u32 _name, u64* _pParam)
{
	// 未対応
	libAlert(0);
}


///-------------------------------------------------------------------------------------------------
/// レンダーターゲット周り
void	CDeferredContext::FramebufferRenderbuffer(u32 _frameTrg, u32 _slot, u32 _colorTrg, const u32* _pObjectID)
{
	u32 argc = 4;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_frameTrg) + sizeof(_slot) + sizeof(_colorTrg);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _frameTrg;
	pBufU32[1] = _slot;
	pBufU32[2] = _colorTrg;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = &pBufU32[2];
	pPacket->argv[3] = _pObjectID;
	pPacket->func = Cmd_FramebufferRenderbuffer;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup FramebufferRenderbuffer\n");
}

void	CDeferredContext::FramebufferTexture2D(u32 _frameTrg, u32 _slot, u32 _textureTrg, const u32* _pObjectID, u32 _level)
{
	u32 argc = 5;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_frameTrg) + sizeof(_slot) + sizeof(_textureTrg) + sizeof(_level);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _frameTrg;
	pBufU32[1] = _slot;
	pBufU32[2] = _textureTrg;
	pBufU32[3] = _level;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = &pBufU32[2];
	pPacket->argv[3] = _pObjectID;
	pPacket->argv[4] = &pBufU32[3];
	pPacket->func = Cmd_FramebufferTexture2D;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup FramebufferTexture2D\n");
}

void	CDeferredContext::DrawBuffers(u32 _num, const u32* _pColorTrg)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_num) + sizeof(u32) * _num;
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _num;
	u32* pClrTrg = PCast<u32*>(&pBufU32[1]);
	memcpy(pClrTrg, _pColorTrg, sizeof(u32) * _num);

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = pClrTrg;
	pPacket->func = Cmd_DrawBuffers;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup DrawBuffers\n");
}


///-------------------------------------------------------------------------------------------------
/// クリアバッファ
void	CDeferredContext::ClearBufferColor(f32 _r, f32 _g, f32 _b, f32 _a)
{
	u32 argc = 4;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(f32) * 4;
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	f32* pBufF32 = PCast<f32*>(pTop + packetSize + argvSize);
	pBufF32[0] = _r;
	pBufF32[1] = _g;
	pBufF32[2] = _b;
	pBufF32[3] = _a;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufF32[0];
	pPacket->func = Cmd_ClearBufferColor;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup ClearBufferColor\n");
}

void	CDeferredContext::ClearBufferDepth(f32 _depth)
{
	u32 argc = 1;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_depth);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	f32* pBufF32 = PCast<f32*>(pTop + packetSize + argvSize);
	pBufF32[0] = _depth;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufF32[0];
	pPacket->func = Cmd_ClearBufferDepth;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup ClearBufferDepth\n");
}

void	CDeferredContext::ClearBufferStencil(s32 _stencil)
{
	u32 argc = 1;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_stencil);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	s32* pBufS32 = PCast<s32*>(pTop + packetSize + argvSize);
	pBufS32[0] = _stencil;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufS32[0];
	pPacket->func = Cmd_ClearBufferStencil;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup ClearBufferStencil\n");
}

void	CDeferredContext::ClearColor(f32 _r, f32 _g, f32 _b, f32 _a)
{
	u32 argc = 4;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_r) + sizeof(_g) + sizeof(_b) + sizeof(_a);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	f32* pBufF32 = PCast<f32*>(pTop + packetSize + argvSize);
	pBufF32[0] = _r;
	pBufF32[1] = _g;
	pBufF32[2] = _b;
	pBufF32[3] = _a;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufF32[0];
	pPacket->argv[1] = &pBufF32[1];
	pPacket->argv[2] = &pBufF32[2];
	pPacket->argv[3] = &pBufF32[3];
	pPacket->func = Cmd_ClearColor;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup ClearColor\n");
}

void	CDeferredContext::ClearDepth(f32 _depth)
{
	u32 argc = 1;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_depth);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	f32* pBufF32 = PCast<f32*>(pTop + packetSize + argvSize);
	pBufF32[0] = _depth;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufF32[0];
	pPacket->func = Cmd_ClearDepth;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup ClearDepth\n");
}

void	CDeferredContext::ClearStencil(s32 _stencil)
{
	u32 argc = 1;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_stencil);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	s32* pBufS32 = PCast<s32*>(pTop + packetSize + argvSize);
	pBufS32[0] = _stencil;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufS32[0];
	pPacket->func = Cmd_ClearStencil;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup ClearStencil\n");
}

void	CDeferredContext::Clear(u32 _bit)
{
	u32 argc = 1;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_bit);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _bit;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->func = Cmd_Clear;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup Clear\n");
}


///-------------------------------------------------------------------------------------------------
/// ラスタライザー
void	CDeferredContext::Viewport(s32 _posX, s32 _posY, u32 _width, u32 _height)
{
	u32 argc = 4;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_posX) + sizeof(_posY) + sizeof(_width) + sizeof(_height);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	s32* pBufS32 = PCast<s32*>(pTop + packetSize + argvSize);
	pBufS32[0] = _posX;
	pBufS32[1] = _posY;
	u32* pBufU32 = PCast<u32*>(&pBufS32[2]);
	pBufU32[0] = _width;
	pBufU32[1] = _height;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufS32[0];
	pPacket->argv[1] = &pBufS32[1];
	pPacket->argv[2] = &pBufU32[0];
	pPacket->argv[3] = &pBufU32[1];
	pPacket->func = Cmd_Viewport;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup Viewport\n");
}

void	CDeferredContext::Scissor(s32 _posX, s32 _posY, u32 _width, u32 _height)
{
	u32 argc = 4;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_posX) + sizeof(_posY) + sizeof(_width) + sizeof(_height);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	s32* pBufS32 = PCast<s32*>(pTop + packetSize + argvSize);
	pBufS32[0] = _posX;
	pBufS32[1] = _posY;
	u32* pBufU32 = PCast<u32*>(&pBufS32[2]);
	pBufU32[0] = _width;
	pBufU32[1] = _height;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufS32[0];
	pPacket->argv[1] = &pBufS32[1];
	pPacket->argv[2] = &pBufU32[0];
	pPacket->argv[3] = &pBufU32[1];
	pPacket->func = Cmd_Scissor;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup Scissor\n");
}

void	CDeferredContext::DepthRange(f32 _near, f32 _far)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_near) + sizeof(_far);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	f32* pBufF32 = PCast<f32*>(pTop + packetSize + argvSize);
	pBufF32[0] = _near;
	pBufF32[1] = _far;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufF32[0];
	pPacket->argv[1] = &pBufF32[1];
	pPacket->func = Cmd_DepthRange;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup DepthRange\n");
}

void	CDeferredContext::FrontFace(u32 _faceType)
{
	u32 argc = 1;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_faceType);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _faceType;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->func = Cmd_FrontFace;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup FrontFace\n");
}

void	CDeferredContext::CullFace(u32 _cullType)
{
	u32 argc = 1;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_cullType);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _cullType;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->func = Cmd_CullFace;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup CullFace\n");
}

void	CDeferredContext::PolygonMode(u32 _face, u32 _mode)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_face) + sizeof(_mode);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _face;
	pBufU32[1] = _mode;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->func = Cmd_PolygonMode;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup PolygonMode\n");
}

void	CDeferredContext::PolygonOffset(f32 _slopeScale, f32 _bias)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_slopeScale) + sizeof(_bias);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	f32* pBufF32 = PCast<f32*>(pTop + packetSize + argvSize);
	pBufF32[0] = _slopeScale;
	pBufF32[1] = _bias;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufF32[0];
	pPacket->argv[1] = &pBufF32[1];
	pPacket->func = Cmd_PolygonOffset;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup PolygonOffset\n");
}


///-------------------------------------------------------------------------------------------------
/// ブレンドステート
void	CDeferredContext::BlendEquationSeparate(u32 _colorOp, u32 _alphaOp)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_colorOp) + sizeof(_alphaOp);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _colorOp;
	pBufU32[1] = _alphaOp;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->func = Cmd_BlendEquationSeparate;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup BlendEquationSeparate\n");
}

void	CDeferredContext::BlendEquationSeparatei(u32 _slot, u32 _colorOp, u32 _alphaOp)
{
	u32 argc = 3;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_slot) + sizeof(_colorOp) + sizeof(_alphaOp);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _slot;
	pBufU32[1] = _colorOp;
	pBufU32[2] = _alphaOp;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = &pBufU32[2];
	pPacket->func = Cmd_BlendEquationSeparatei;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup BlendEquationSeparatei\n");
}

void	CDeferredContext::BlendFuncSeparate(u32 _colorSrcFunc, u32 _colorDstFunc, u32 _alphaSrcFunc, u32 _alphaDstFunc)
{
	u32 argc = 4;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_colorSrcFunc) + sizeof(_colorDstFunc) + sizeof(_alphaSrcFunc) + sizeof(_alphaDstFunc);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _colorSrcFunc;
	pBufU32[1] = _colorDstFunc;
	pBufU32[2] = _alphaSrcFunc;
	pBufU32[3] = _alphaDstFunc;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = &pBufU32[2];
	pPacket->argv[3] = &pBufU32[3];
	pPacket->func = Cmd_BlendFuncSeparate;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup BlendFuncSeparate\n");
}

void	CDeferredContext::BlendFuncSeparatei(u32 _slot, u32 _colorSrcFunc, u32 _colorDstFunc, u32 _alphaSrcFunc, u32 _alphaDstFunc)
{
	u32 argc = 5;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_slot) + sizeof(_colorSrcFunc) + sizeof(_colorDstFunc) + sizeof(_alphaSrcFunc) + sizeof(_alphaDstFunc);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _slot;
	pBufU32[1] = _colorSrcFunc;
	pBufU32[2] = _colorDstFunc;
	pBufU32[3] = _alphaSrcFunc;
	pBufU32[4] = _alphaDstFunc;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = &pBufU32[2];
	pPacket->argv[3] = &pBufU32[3];
	pPacket->argv[4] = &pBufU32[4];
	pPacket->func = Cmd_BlendFuncSeparatei;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup BlendFuncSeparatei\n");
}

void	CDeferredContext::ColorMask(b8 _r, b8 _g, b8 _b, b8 _a)
{
	u32 argc = 4;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_r) + sizeof(_g) + sizeof(_b) + sizeof(_a);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	b8* pBufB8 = PCast<b8*>(pTop + packetSize + argvSize);
	pBufB8[0] = _r;
	pBufB8[1] = _g;
	pBufB8[2] = _b;
	pBufB8[3] = _a;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufB8[0];
	pPacket->argv[1] = &pBufB8[1];
	pPacket->argv[2] = &pBufB8[2];
	pPacket->argv[3] = &pBufB8[3];
	pPacket->func = Cmd_ColorMask;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup ColorMask\n");
}

void	CDeferredContext::ColorMaski(u32 _slot, b8 _r, b8 _g, b8 _b, b8 _a)
{
	u32 argc = 5;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_slot) + sizeof(_r) + sizeof(_g) + sizeof(_b) + sizeof(_a);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _slot;
	b8* pBufB8 = PCast<b8*>(&pBufU32[1]);
	pBufB8[0] = _r;
	pBufB8[1] = _g;
	pBufB8[2] = _b;
	pBufB8[3] = _a;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufB8[1];
	pPacket->argv[2] = &pBufB8[2];
	pPacket->argv[3] = &pBufB8[3];
	pPacket->argv[4] = &pBufB8[4];
	pPacket->func = Cmd_ColorMaski;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup ColorMaski\n");
}


///-------------------------------------------------------------------------------------------------
/// デプスステンシルステート
void	CDeferredContext::DepthMask(b8 _isWrite)
{
	u32 argc = 1;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_isWrite);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	b8* pBufB8 = PCast<b8*>(pTop + packetSize + argvSize);
	pBufB8[0] = _isWrite;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufB8[0];
	pPacket->func = Cmd_DepthMask;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup DepthMask\n");
}

void	CDeferredContext::DepthFunc(u32 _depthTest)
{
	u32 argc = 1;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_depthTest);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _depthTest;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->func = Cmd_DepthFunc;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup DepthFunc\n");
}

void	CDeferredContext::StencilMaskSeparate(u32 _face, u32 _mask)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_face) + sizeof(_mask);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _face;
	pBufU32[1] = _mask;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->func = Cmd_StencilMaskSeparate;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup StencilMaskSeparate\n");
}

void	CDeferredContext::StencilFuncSeparate(u32 _face, u32 _test, u32 _ref, u32 _readMask)
{
	u32 argc = 4;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_face) + sizeof(_test) + sizeof(_ref) + sizeof(_readMask);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _face;
	pBufU32[1] = _test;
	pBufU32[2] = _ref;
	pBufU32[3] = _readMask;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = &pBufU32[2];
	pPacket->argv[3] = &pBufU32[3];
	pPacket->func = Cmd_StencilFuncSeparate;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup StencilFuncSeparate\n");
}

void	CDeferredContext::StencilOpSeparate(u32 _face, u32 _failOp, u32 _ZFailOp, u32 _passOp)
{
	u32 argc = 4;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_face) + sizeof(_failOp) + sizeof(_ZFailOp) + sizeof(_passOp);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _face;
	pBufU32[1] = _failOp;
	pBufU32[2] = _ZFailOp;
	pBufU32[3] = _passOp;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = &pBufU32[2];
	pPacket->argv[3] = &pBufU32[3];
	pPacket->func = Cmd_StencilOpSeparate;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup StencilOpSeparate\n");
}


///-------------------------------------------------------------------------------------------------
/// サンプラーステート
void	CDeferredContext::SamplerParameteri(const u32* _pObjectID, u32 _name, u32 _param)
{
	u32 argc = 3;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_name) + sizeof(_param);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _name;
	pBufU32[1] = _param;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = _pObjectID;
	pPacket->argv[1] = &pBufU32[0];
	pPacket->argv[2] = &pBufU32[1];
	pPacket->func = Cmd_SamplerParameteri;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup SamplerParameteri\n");
}

void	CDeferredContext::SamplerParameterf(const u32* _pObjectID, u32 _name, f32 _param)
{
	u32 argc = 3;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_name) + sizeof(_param);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _name;
	f32* pBufF32 = PCast<f32*>(&pBufU32[1]);
	pBufF32[0] = _param;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = _pObjectID;
	pPacket->argv[1] = &pBufU32[0];
	pPacket->argv[2] = &pBufF32[0];
	pPacket->func = Cmd_SamplerParameterf;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup SamplerParameterf\n");
}

void	CDeferredContext::SamplerParameterfv(const u32* _pObjectID, u32 _name, const f32* _param)
{
	u32 argc = 3;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_name) + sizeof(f32) * 4;
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _name;
	f32* pBufF32 = PCast<f32*>(&pBufU32[1]);
	memcpy(pBufF32, _param, sizeof(f32) * 4);

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = _pObjectID;
	pPacket->argv[1] = &pBufU32[0];
	pPacket->argv[2] = &pBufF32[0];
	pPacket->func = Cmd_SamplerParameterfv;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup SamplerParameterfv\n");
}


///-------------------------------------------------------------------------------------------------
/// シェーダ
void	CDeferredContext::BindProgram(u32 _programID)
{
	u32 argc = 1;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_programID);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _programID;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->func = Cmd_BindProgram;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup BindProgram\n");
}

void	CDeferredContext::BindProgramPipeline(u32 _pipelineID)
{
	u32 argc = 1;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_pipelineID);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _pipelineID;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->func = Cmd_BindProgramPipeline;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup BindProgramPipeline\n");
}

void	CDeferredContext::ActiveShaderProgram(u32 _pipelineID, u32 _programID)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_pipelineID) + sizeof(_programID);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _pipelineID;
	pBufU32[1] = _programID;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->func = Cmd_ActiveShaderProgram;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup ActiveShaderProgram\n");
}


///-------------------------------------------------------------------------------------------------
/// 頂点バッファ
void	CDeferredContext::EnableVertexAttribArray(u32 _attr)
{
	u32 argc = 1;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_attr);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _attr;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->func = Cmd_EnableVertexAttribArray;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup EnableVertexAttribArray\n");
}

void	CDeferredContext::VertexAttribPointer(u32 _attr, u32 _elemNum, u32 _valType, b8 _normalized, u32 _stride, u32 _offset)
{
	u32 argc = 6;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_attr) + sizeof(_elemNum) + sizeof(_valType) + sizeof(_normalized) + sizeof(_stride) + sizeof(_offset);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _attr;
	pBufU32[1] = _elemNum;
	pBufU32[2] = _valType;
	pBufU32[3] = _stride;
	pBufU32[4] = _offset;
	b8* pBufB8 = PCast<b8*>(&pBufU32[5]);
	pBufB8[0] = _normalized;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = &pBufU32[2];
	pPacket->argv[3] = &pBufB8[0];
	pPacket->argv[4] = &pBufU32[3];
	pPacket->argv[5] = &pBufU32[4];
	pPacket->func = Cmd_VertexAttribPointer;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup VertexAttribPointer\n");
}

void	CDeferredContext::DrawArrays(u32 _primType, u32 _vtxOffset, u32 _vtxNum)
{
	u32 argc = 3;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_primType) + sizeof(_vtxOffset) + sizeof(_vtxNum);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _primType;
	pBufU32[1] = _vtxOffset;
	pBufU32[2] = _vtxNum;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = &pBufU32[2];
	pPacket->func = Cmd_DrawArrays;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup DrawArrays\n");
}

void	CDeferredContext::DrawIndexed(u32 _primType, u32 _valType, u32 _idxOffset, u32 _vtxOffset, u32 _idxNum)
{
	u32 argc = 5;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_primType) + sizeof(_valType) + sizeof(_idxOffset) + sizeof(_vtxOffset) + sizeof(_idxNum);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _primType;
	pBufU32[1] = _idxNum;
	pBufU32[2] = _valType;
	pBufU32[3] = _idxOffset;
	pBufU32[4] = _vtxOffset;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = &pBufU32[1];
	pPacket->argv[2] = &pBufU32[2];
	pPacket->argv[3] = &pBufU32[3];
	pPacket->argv[4] = &pBufU32[4];
	pPacket->func = Cmd_DrawIndexed;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup DrawIndexed\n");
}


///-------------------------------------------------------------------------------------------------
/// フラッシュ
void	CDeferredContext::Flush()
{
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	CMD_DRAW_PACKET* pPacket = PCast<CMD_DRAW_PACKET*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize));
	pPacket = new(pPacket) CMD_DRAW_PACKET;
	pPacket->func = Cmd_Flush;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup Flush\n");
}


///-------------------------------------------------------------------------------------------------
/// クエリー
void	CDeferredContext::BeginQuery(u32 _target, const u32* _pQueryID)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_target);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = PCast<const u32*>(_pQueryID);
	pPacket->func = Cmd_BeginQuery;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup BeginQuery\n");
}

void	CDeferredContext::EndQuery(u32 _target)
{
	u32 argc = 1;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_target);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _target;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->func = Cmd_EndQuery;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup EndQuery\n");
}

void	CDeferredContext::BeginConditionalRender(const u32* _pQueryID, u32 _mode)
{
	u32 argc = 2;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_mode);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _mode;

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = PCast<const u32*>(_pQueryID);
	pPacket->argv[1] = &pBufU32[0];
	pPacket->func = Cmd_BeginConditionalRender;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup BeginConditionalRender\n");
}

void	CDeferredContext::EndConditionalRender()
{
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	CMD_DRAW_PACKET* pPacket = PCast<CMD_DRAW_PACKET*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize));
	pPacket = new(pPacket) CMD_DRAW_PACKET;
	pPacket->func = Cmd_EndConditionalRender;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup EndConditionalRender\n");
}

void	CDeferredContext::PushDebugGroup(u32 _source, const u32* _pQueryID, u32 _srtLen, const c8* _pStr)
{
	u32 argc = 4;
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	u32 argvSize = sizeof(void*) * argc;
	u32 bufSize = sizeof(_source) + + sizeof(_srtLen) + (sizeof(c8) * _srtLen);
	u8* pTop = PCast<u8*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize + argvSize + bufSize));
	libAssert(pTop);
	u32* pBufU32 = PCast<u32*>(pTop + packetSize + argvSize);
	pBufU32[0] = _source;
	pBufU32[1] = _srtLen;
	c8* pStr = PCast<c8*>(&pBufU32[2]);
	//strcpy(pStr, _pStr);
	//strcpy_s(pStr, _srtLen, _pStr);
	memcpy(pStr, _pStr, sizeof(c8) * _srtLen);

	CMD_DRAW_PACKET* pPacket = new(pTop) CMD_DRAW_PACKET;
	pPacket->argc = argc;
	pPacket->argv = PCast<const void**>(pTop + packetSize);
	pPacket->argv[0] = &pBufU32[0];
	pPacket->argv[1] = PCast<const u32*>(_pQueryID);
	pPacket->argv[2] = &pBufU32[1];
	pPacket->argv[3] = pStr;
	pPacket->func = Cmd_PushDebugGroup;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup PushDebugGroup\n");
}

void	CDeferredContext::PopDebugGroup()
{
	u32 packetSize = sizeof(CMD_DRAW_PACKET);
	CMD_DRAW_PACKET* pPacket = PCast<CMD_DRAW_PACKET*>(MEM_MALLOC_ALIGN(m_pAllocator, DRAW_CMD_ALIGN, packetSize));
	pPacket = new(pPacket) CMD_DRAW_PACKET;
	pPacket->func = Cmd_PopDebugGroup;
	m_list.PushBack(pPacket);

	DRAW_CMD_PRINT("Setup PopDebugGroup\n");
}



///*************************************************************************************************
///	描画コマンド
///*************************************************************************************************

void	CDeferredContext::Cmd_Enable(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	u32 cap = *PCast<const u32*>(_argv[0]);
	::glEnable(cap);

	DRAW_CMD_PRINT("Command glEnable\n");
}
void	CDeferredContext::Cmd_Enablei(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 cap = *PCast<const u32*>(_argv[0]);
	u32 slot = *PCast<const u32*>(_argv[1]);
	::glEnablei(cap, slot);

	DRAW_CMD_PRINT("Command glEnablei\n");
}
void	CDeferredContext::Cmd_Disable(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	u32 cap = *PCast<const u32*>(_argv[0]);
	::glDisable(cap);

	DRAW_CMD_PRINT("Command glDisable\n");
}
void	CDeferredContext::Cmd_Disablei(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 cap = *PCast<const u32*>(_argv[0]);
	u32 slot = *PCast<const u32*>(_argv[1]);
	::glDisablei(cap, slot);

	DRAW_CMD_PRINT("Command glDisablei\n");
}

void	CDeferredContext::Cmd_BindBuffer(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 target = *PCast<const u32*>(_argv[0]);
	const u32* pObjectID = PCast<const u32*>(_argv[1]);
	libAssert(pObjectID);
	::glBindBuffer(target, *pObjectID);

	DRAW_CMD_PRINT("Command glBindBuffer\n");
}
void	CDeferredContext::Cmd_BindBufferBase(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 3);
	u32 target = *PCast<const u32*>(_argv[0]);
	u32 slot = *PCast<const u32*>(_argv[1]);
	const u32* pObjectID = PCast<const u32*>(_argv[2]);
	libAssert(pObjectID);
	::glBindBufferBase(target, slot, *pObjectID);

	DRAW_CMD_PRINT("Command glBindBufferBase\n");
}
void	CDeferredContext::Cmd_BindFramebuffer(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 target = *PCast<const u32*>(_argv[0]);
	const u32* pObjectID = PCast<const u32*>(_argv[1]);
	libAssert(pObjectID);
	::glBindFramebuffer(target, *pObjectID);

	DRAW_CMD_PRINT("Command glBindFramebuffer\n");
}
void	CDeferredContext::Cmd_BindRenderbuffer(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 target = *PCast<const u32*>(_argv[0]);
	const u32* pObjectID = PCast<const u32*>(_argv[1]);
	libAssert(pObjectID);
	::glBindRenderbuffer(target, *pObjectID);

	DRAW_CMD_PRINT("Command glBindRenderbuffer\n");
}
void	CDeferredContext::Cmd_BindTexture(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 target = *PCast<const u32*>(_argv[0]);
	const u32* pObjectID = PCast<const u32*>(_argv[1]);
	libAssert(pObjectID);
	::glBindTexture(target, *pObjectID);

	DRAW_CMD_PRINT("Command glBindTexture\n");
}
void	CDeferredContext::Cmd_ActiveTexture(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	u32 slot = *PCast<const u32*>(_argv[0]);
	::glActiveTexture(slot);

	DRAW_CMD_PRINT("Command glActiveTexture\n");
}
void	CDeferredContext::Cmd_BindSampler(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 slot = *PCast<const u32*>(_argv[0]);
	const u32* pObjectID = PCast<const u32*>(_argv[1]);
	libAssert(pObjectID);
	::glBindSampler(slot, *pObjectID);

	DRAW_CMD_PRINT("Command glBindSampler\n");
}

void	CDeferredContext::Cmd_UnBindBuffer(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	u32 target = *PCast<const u32*>(_argv[0]);
	::glBindBuffer(target, NULL);

	DRAW_CMD_PRINT("Command glBindBuffer\n");
}
void	CDeferredContext::Cmd_UnBindFramebuffer(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	u32 target = *PCast<const u32*>(_argv[0]);
	::glBindFramebuffer(target, NULL);

	DRAW_CMD_PRINT("Command glBindFramebuffer\n");
}
void	CDeferredContext::Cmd_UnBindRenderbuffer(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	u32 target = *PCast<const u32*>(_argv[0]);
	::glBindRenderbuffer(target, NULL);

	DRAW_CMD_PRINT("Command glBindRenderbuffer\n");
}
void	CDeferredContext::Cmd_UnBindTexture(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	u32 target = *PCast<const u32*>(_argv[0]);
	::glBindTexture(target, NULL);

	DRAW_CMD_PRINT("Command glBindTexture\n");
}

void	CDeferredContext::Cmd_GenBuffers(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 num = *PCast<const u32*>(_argv[0]);
	u32* pObjectID = CCast<u32*>(PCast<const u32*>(_argv[1]));	// constはずし
	libAssert(pObjectID);
	::glGenBuffers(num, pObjectID);

	DRAW_CMD_PRINT("Command glGenBuffers\n");
}
void	CDeferredContext::Cmd_GenFramebuffers(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 num = *PCast<const u32*>(_argv[0]);
	u32* pObjectID = CCast<u32*>(PCast<const u32*>(_argv[1]));	// constはずし
	libAssert(pObjectID);
	::glGenFramebuffers(num, pObjectID);

	DRAW_CMD_PRINT("Command glGenFramebuffers\n");
}
void	CDeferredContext::Cmd_GenRenderbuffers(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 num = *PCast<const u32*>(_argv[0]);
	u32* pObjectID = CCast<u32*>(PCast<const u32*>(_argv[1]));	// constはずし
	libAssert(pObjectID);
	::glGenRenderbuffers(num, pObjectID);

	DRAW_CMD_PRINT("Command glGenRenderbuffers\n");
}
void	CDeferredContext::Cmd_GenTextures(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 num = *PCast<const u32*>(_argv[0]);
	u32* pObjectID = CCast<u32*>(PCast<const u32*>(_argv[1]));	// constはずし
	libAssert(pObjectID);
	::glGenTextures(num, pObjectID);

	DRAW_CMD_PRINT("Command glGenTextures\n");
}
void	CDeferredContext::Cmd_GenSamplers(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 num = *PCast<const u32*>(_argv[0]);
	u32* pObjectID = CCast<u32*>(PCast<const u32*>(_argv[1]));	// constはずし
	libAssert(pObjectID);
	::glGenSamplers(num, pObjectID);

	DRAW_CMD_PRINT("Command glGenSamplers\n");
}
void	CDeferredContext::Cmd_GenQueries(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 num = *PCast<const u32*>(_argv[0]);
	u32* pQueryID = CCast<u32*>(PCast<const u32*>(_argv[1]));	// constはずし
	libAssert(pQueryID);
	::glGenQueries(num, pQueryID);

	DRAW_CMD_PRINT("Command glGenQueries\n");
}

void	CDeferredContext::Cmd_DeleteBuffers(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 num = *PCast<const u32*>(_argv[0]);
	u32* pObjectID = CCast<u32*>(PCast<const u32*>(_argv[1]));	// constはずし
	libAssert(pObjectID);
	::glDeleteBuffers(num, pObjectID);

	DRAW_CMD_PRINT("Command glDeleteBuffers\n");
}
void	CDeferredContext::Cmd_DeleteFramebuffers(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 num = *PCast<const u32*>(_argv[0]);
	u32* pObjectID = CCast<u32*>(PCast<const u32*>(_argv[1]));	// constはずし
	libAssert(pObjectID);
	::glDeleteFramebuffers(num, pObjectID);

	DRAW_CMD_PRINT("Command glDeleteFramebuffers\n");
}
void	CDeferredContext::Cmd_DeleteRenderbuffers(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 num = *PCast<const u32*>(_argv[0]);
	u32* pObjectID = CCast<u32*>(PCast<const u32*>(_argv[1]));	// constはずし
	libAssert(pObjectID);
	::glDeleteRenderbuffers(num, pObjectID);

	DRAW_CMD_PRINT("Command glDeleteRenderbuffers\n");
}
void	CDeferredContext::Cmd_DeleteTextures(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 num = *PCast<const u32*>(_argv[0]);
	u32* pObjectID = CCast<u32*>(PCast<const u32*>(_argv[1]));	// constはずし
	libAssert(pObjectID);
	::glDeleteTextures(num, pObjectID);

	DRAW_CMD_PRINT("Command glDeleteTextures\n");
}
void	CDeferredContext::Cmd_DeleteSamplers(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 num = *PCast<const u32*>(_argv[0]);
	u32* pObjectID = CCast<u32*>(PCast<const u32*>(_argv[1]));	// constはずし
	libAssert(pObjectID);
	::glDeleteSamplers(num, pObjectID);

	DRAW_CMD_PRINT("Command glDeleteSamplers\n");
}
void	CDeferredContext::Cmd_DeleteQueries(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 num = *PCast<const u32*>(_argv[0]);
	u32* pQueryID = CCast<u32*>(PCast<const u32*>(_argv[1]));	// constはずし
	libAssert(pQueryID);
	::glDeleteQueries(num, pQueryID);

	DRAW_CMD_PRINT("Command glDeleteQueries\n");
}

void	CDeferredContext::Cmd_BufferData(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 4);
	u32 target = *PCast<const u32*>(_argv[0]);
	u32 size = *PCast<const u32*>(_argv[1]);
	const void* pData = _argv[2];
	u32 usage = *PCast<const u32*>(_argv[3]);
	::glBufferData(target, size, pData, usage);

	DRAW_CMD_PRINT("Command glBufferData\n");
}
void	CDeferredContext::Cmd_RenderbufferStorage(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 4);
	u32 _target = *PCast<const u32*>(_argv[0]);
	u32 _format = *PCast<const u32*>(_argv[1]);
	u32 _width = *PCast<const u32*>(_argv[2]);
	u32 _height = *PCast<const u32*>(_argv[3]);
	::glRenderbufferStorage(_target, _format, _width, _height);

	DRAW_CMD_PRINT("Command glRenderbufferStorage\n");
}
void	CDeferredContext::Cmd_RenderbufferStorageMultisample(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 5);
	u32 _target = *PCast<const u32*>(_argv[0]);
	u32 _aa = *PCast<const u32*>(_argv[1]);
	u32 _format = *PCast<const u32*>(_argv[2]);
	u32 _width = *PCast<const u32*>(_argv[3]);
	u32 _height = *PCast<const u32*>(_argv[4]);
	::glRenderbufferStorageMultisample(_target, _aa, _format, _width, _height);

	DRAW_CMD_PRINT("Command glRenderbufferStorageMultisample\n");
}
void	CDeferredContext::Cmd_TexImage2D(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 8);
	u32 _target = *PCast<const u32*>(_argv[0]);
	u32 _level = *PCast<const u32*>(_argv[1]);
	u32 _format = *PCast<const u32*>(_argv[2]);
	u32 _width = *PCast<const u32*>(_argv[3]);
	u32 _height = *PCast<const u32*>(_argv[4]);
	u32 _texFormat = *PCast<const u32*>(_argv[5]);
	u32 _valType = *PCast<const u32*>(_argv[6]);
	const void* _pData = _argv[7];
	::glTexImage2D(
		_target,		// ターゲット
		_level,			// ミットマップレベル
		_format,		// フォーマット
		_width,			// 幅
		_height,		// 高さ
		0,				// よくわからん
		_texFormat,		// データフォーマット
		_valType,		// データ型
		_pData			// データ
	);

	DRAW_CMD_PRINT("Command glTexImage2D\n");
}
void	CDeferredContext::Cmd_TexImage2DMultisample(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 5);
	u32 _target = *PCast<const u32*>(_argv[0]);
	u32 _aa = *PCast<const u32*>(_argv[1]);
	u32 _format = *PCast<const u32*>(_argv[2]);
	u32 _width = *PCast<const u32*>(_argv[3]);
	u32 _height = *PCast<const u32*>(_argv[4]);
	::glTexImage2DMultisample(
		_target,		// ターゲット
		_aa,			// AA数
		_format,		// フォーマット
		_width,			// 幅
		_height,		// 高さ
		GL_TRUE			// 全テクセルでのサンプル数固定フラグ？
	);

	DRAW_CMD_PRINT("Command glTexImage2DMultisample\n");
}
void	CDeferredContext::Cmd_TexImage3D(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 9);
	u32 _target = *PCast<const u32*>(_argv[0]);
	u32 _level = *PCast<const u32*>(_argv[1]);
	u32 _format = *PCast<const u32*>(_argv[2]);
	u32 _width = *PCast<const u32*>(_argv[3]);
	u32 _height = *PCast<const u32*>(_argv[4]);
	u32 _depth = *PCast<const u32*>(_argv[5]);
	u32 _texFormat = *PCast<const u32*>(_argv[6]);
	u32 _valType = *PCast<const u32*>(_argv[7]);
	const void* _pData = _argv[8];
	glTexImage3D(
		_target,		// ターゲット
		_level,			// ミットマップレベル
		_format,		// フォーマット
		_width,			// 幅
		_height,		// 高さ
		_depth,			// 奥行き
		0,				// よくわからん
		_texFormat,		// データフォーマット
		_valType,		// データ型
		_pData			// データ
	);

	DRAW_CMD_PRINT("Command glTexImage3D\n");
}
void	CDeferredContext::Cmd_TexStorage2D(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 5);
	u32 _target = *PCast<const u32*>(_argv[0]);
	u32 _level = *PCast<const u32*>(_argv[1]);
	u32 _format = *PCast<const u32*>(_argv[2]);
	u32 _width = *PCast<const u32*>(_argv[3]);
	u32 _height = *PCast<const u32*>(_argv[4]);
	::glTexStorage2D(
		_target,		// ターゲット
		_level,			// ミットマップレベル
		_format,		// フォーマット
		_width,			// 幅
		_height			// 高さ
	);

	DRAW_CMD_PRINT("Command glTexStorage2D\n");
}
void	CDeferredContext::Cmd_TexStorage2DMultisample(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 5);
	u32 _target = *PCast<const u32*>(_argv[0]);
	u32 _aa = *PCast<const u32*>(_argv[1]);
	u32 _format = *PCast<const u32*>(_argv[2]);
	u32 _width = *PCast<const u32*>(_argv[3]);
	u32 _height = *PCast<const u32*>(_argv[4]);
	::glTexStorage2DMultisample(
		_target,		// ターゲット
		_aa,			// AA数
		_format,		// フォーマット
		_width,			// 幅
		_height,		// 高さ
		GL_TRUE			// 全テクセルでのサンプル数固定フラグ？
	);

	DRAW_CMD_PRINT("Command glTexStorage2DMultisample\n");
}
void	CDeferredContext::Cmd_TexStorage3D(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 6);
	u32 _target = *PCast<const u32*>(_argv[0]);
	u32 _level = *PCast<const u32*>(_argv[1]);
	u32 _format = *PCast<const u32*>(_argv[2]);
	u32 _width = *PCast<const u32*>(_argv[3]);
	u32 _height = *PCast<const u32*>(_argv[4]);
	u32 _depth = *PCast<const u32*>(_argv[5]);
	::glTexStorage3D(
		_target,		// ターゲット
		_level,			// ミットマップレベル
		_format,		// フォーマット
		_width,			// 幅
		_height,		// 高さ
		_depth			// 奥行き（配列数）
	);

	DRAW_CMD_PRINT("Command glTexStorage3D\n");
}

void	CDeferredContext::Cmd_MapBuffer(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 3);
	u32 target = *PCast<const u32*>(_argv[0]);
	u32 size = *PCast<const u32*>(_argv[1]);
	const void* pData = _argv[2];
#ifdef USE_SUBDATA_BUFFER
	::glBufferSubData(target, 0, size, pData);
#else
	void* ptr = ::glMapBuffer(target, GL_WRITE_ONLY);
	memcpy(ptr, pData, size);
	::glUnmapBuffer(target);
#endif // USE_SUBDATA_BUFFER

	DRAW_CMD_PRINT("Command glBufferSubData\n");
}
void	CDeferredContext::Cmd_TexSubImage2D(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 9);
	u32 face = *PCast<const u32*>(_argv[0]);
	u32 level = *PCast<const u32*>(_argv[1]);
	u32 xoff = *PCast<const u32*>(_argv[2]);
	u32 yoff = *PCast<const u32*>(_argv[3]);
	u32 mipWidth = *PCast<const u32*>(_argv[4]);
	u32 mipHeight = *PCast<const u32*>(_argv[5]);
	u32 texFormat = *PCast<const u32*>(_argv[6]);
	u32 valType = *PCast<const u32*>(_argv[7]);
	const void* pData = _argv[8];
	::glTexSubImage2D(
		face,			// ターゲット
		level,			// ミットマップレベル
		xoff, yoff,		// オフセット
		mipWidth,		// 幅
		mipHeight,		// 高さ
		texFormat,		// データフォーマット
		valType,		// データ型
		pData			// データ
	);

	DRAW_CMD_PRINT("Command glTexSubImage2D\n");
}
void	CDeferredContext::Cmd_TexSubImage3D(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 11);
	u32 face = *PCast<const u32*>(_argv[0]);
	u32 level = *PCast<const u32*>(_argv[1]);
	u32 xoff = *PCast<const u32*>(_argv[2]);
	u32 yoff = *PCast<const u32*>(_argv[3]);
	u32 zoff = *PCast<const u32*>(_argv[4]);
	u32 mipWidth = *PCast<const u32*>(_argv[4]);
	u32 mipHeight = *PCast<const u32*>(_argv[5]);
	u32 arrayNum = *PCast<const u32*>(_argv[6]);
	u32 texFormat = *PCast<const u32*>(_argv[7]);
	u32 valType = *PCast<const u32*>(_argv[8]);
	const void* pData = _argv[9];
	::glTexSubImage3D(
		face,				// ターゲット
		level,				// ミットマップレベル
		xoff, yoff, zoff,	// オフセット
		mipWidth,			// 幅
		mipHeight,			// 高さ
		arrayNum,			// 奥行き（配列テクスチャなので１）
		texFormat,			// データフォーマット
		valType,			// データ型
		pData				// データ
	);

	DRAW_CMD_PRINT("Command glTexSubImage3D\n");
}

void	CDeferredContext::Cmd_CheckFramebufferStatus(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	const u32* pObjectID = PCast<const u32*>(_argv[0]);
	libAssert(pObjectID);

	// 現在バインドされているフレームバッファオブジェクトを取得
	s32 currentFrameBufferObject = 0;
	::glGetIntegerv(GL_FRAMEBUFFER_BINDING, &currentFrameBufferObject);

	// バインド
	::glBindFramebuffer(GL_FRAMEBUFFER, *pObjectID);

	// フレームバッファステータスチェック
	u32 status = ::glCheckFramebufferStatusEXT(GL_FRAMEBUFFER);
	switch (status)
	{
	case GL_FRAMEBUFFER_COMPLETE:
		//PRINT("Framebuffer complete\n");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
		PRINT_ERROR("*****[ERROR] Framebuffer incomplete: Attachment is NOT complete\n");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
		PRINT_ERROR("*****[ERROR] Framebuffer incomplete: No image is attached to FBO\n");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT:
		PRINT_ERROR("*****[ERROR] Framebuffer incomplete: Attached images have different dimensions\n");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT:
		PRINT_ERROR("*****[ERROR] Framebuffer incomplete: Color attached images have different internal formats\n");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
		PRINT_ERROR("*****[ERROR] Framebuffer incomplete: Draw buffer\n");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
		PRINT_ERROR("*****[ERROR] Framebuffer incomplete: Read buffer\n");
		break;

	case GL_FRAMEBUFFER_UNSUPPORTED:
		PRINT_ERROR("*****[ERROR] Unsupported by FBO implementation\n");
		break;

	default:
		PRINT_ERROR("*****[ERROR] Unknow error\n");
		break;
	}

	//	アンバインド
	::glBindFramebuffer(GL_FRAMEBUFFER, currentFrameBufferObject);

	DRAW_CMD_PRINT("Command glCheckFramebufferStatusEXT\n");
}

void	CDeferredContext::Cmd_FramebufferRenderbuffer(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 4);
	u32 frameTarget = *PCast<const u32*>(_argv[0]);
	u32 slot = *PCast<const u32*>(_argv[1]);
	u32 clrTarget = *PCast<const u32*>(_argv[2]);
	const u32* pObjectID = PCast<const u32*>(_argv[3]);
	libAssert(pObjectID);
	::glFramebufferRenderbuffer(
		frameTarget,
		slot,
		clrTarget,
		*pObjectID
	);

	DRAW_CMD_PRINT("Command glFramebufferRenderbuffer\n");
}
void	CDeferredContext::Cmd_FramebufferTexture2D(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 5);
	u32 frameTarget = *PCast<const u32*>(_argv[0]);
	u32 slot = *PCast<const u32*>(_argv[1]);
	u32 texTarget = *PCast<const u32*>(_argv[2]);
	const u32* pObjectID = PCast<const u32*>(_argv[3]);
	libAssert(pObjectID);
	u32 level = *PCast<const u32*>(_argv[4]);
	::glFramebufferTexture2D(
		frameTarget,
		slot,
		texTarget,
		*pObjectID,
		level
	);

	DRAW_CMD_PRINT("Command glFramebufferTexture2D\n");
}
void	CDeferredContext::Cmd_DrawBuffers(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 clrSetNum = *PCast<const u32*>(_argv[0]);
	const u32* clrTargets = PCast<const u32*>(_argv[1]);
	::glDrawBuffers(clrSetNum, clrTargets);

	DRAW_CMD_PRINT("Command glDrawBuffers\n");
}

void	CDeferredContext::Cmd_ClearBufferColor(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	const f32* pClearClr = PCast<const f32*>(_argv[0]);
	::glClearBufferfv(
		GL_COLOR,
		0,
		pClearClr
	);

	DRAW_CMD_PRINT("Command glClearBufferfv\n");
}
void	CDeferredContext::Cmd_ClearBufferDepth(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	const f32* pClearDep = PCast<const f32*>(_argv[0]);
	::glClearBufferfv(
		GL_DEPTH,
		0,
		pClearDep
	);

	DRAW_CMD_PRINT("Command glClearBufferfv\n");
}
void	CDeferredContext::Cmd_ClearBufferStencil(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	const s32* pSlearStencil = PCast<const s32*>(_argv[0]);
	::glClearBufferiv(
		GL_STENCIL,
		0,
		pSlearStencil
	);

	DRAW_CMD_PRINT("Command glClearBufferiv\n");
}
void	CDeferredContext::Cmd_ClearColor(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 4);
	const f32* pClr = PCast<const f32*>(_argv);
	::glClearColor(pClr[0], pClr[1], pClr[2], pClr[3]);

	DRAW_CMD_PRINT("Command glClearColor\n");
}
void	CDeferredContext::Cmd_ClearDepth(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	f32 pVal = *PCast<const f32*>(_argv[0]);
	::glClearDepth(pVal);

	DRAW_CMD_PRINT("Command glClearDepth\n");
}
void	CDeferredContext::Cmd_ClearStencil(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	s32 pVal = *PCast<const s32*>(_argv[0]);
	::glClearStencil(pVal);

	DRAW_CMD_PRINT("Command glClearStencil\n");
}
void	CDeferredContext::Cmd_Clear(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	u32 mask = *PCast<const u32*>(_argv[0]);
	::glClear(mask);

	DRAW_CMD_PRINT("Command glClear\n");
}

void	CDeferredContext::Cmd_Viewport(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 4);
	s32 posX = *PCast<const s32*>(_argv[0]);
	s32 posY = *PCast<const s32*>(_argv[1]);
	u32 width = *PCast<const u32*>(_argv[2]);
	u32 height = *PCast<const u32*>(_argv[3]);
	::glViewport(
		posX,
		posY,
		width,
		height
	);

	DRAW_CMD_PRINT("Command glViewport\n");
}
void	CDeferredContext::Cmd_Scissor(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 4);
	s32 posX = *PCast<const s32*>(_argv[0]);
	s32 posY = *PCast<const s32*>(_argv[1]);
	u32 width = *PCast<const u32*>(_argv[2]);
	u32 height = *PCast<const u32*>(_argv[3]);
	::glScissor(
		posX,
		posY,
		width,
		height
	);

	DRAW_CMD_PRINT("Command glScissor\n");
}
void	CDeferredContext::Cmd_DepthRange(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	f32 fNear = *PCast<const f32*>(_argv[0]);
	f32 fFar = *PCast<const f32*>(_argv[1]);
	::glDepthRangef(fNear, fFar);

	DRAW_CMD_PRINT("Command glDepthRangef\n");
}
void	CDeferredContext::Cmd_FrontFace(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	u32 frontFace = *PCast<const u32*>(_argv[0]);
	::glFrontFace(frontFace);

	DRAW_CMD_PRINT("Command glFrontFace\n");
}
void	CDeferredContext::Cmd_CullFace(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	u32 cullFace = *PCast<const u32*>(_argv[0]);
	::glCullFace(cullFace);

	DRAW_CMD_PRINT("Command glCullFace\n");
}
void	CDeferredContext::Cmd_PolygonMode(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 face = *PCast<const u32*>(_argv[0]);
	u32 mode = *PCast<const u32*>(_argv[1]);
	::glPolygonMode(face, mode);

	DRAW_CMD_PRINT("Command glPolygonMode\n");
}
void	CDeferredContext::Cmd_PolygonOffset(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	f32 slopeScaledDepthBias = *PCast<const f32*>(_argv[0]);
	f32 depthBias = *PCast<const f32*>(_argv[1]);
	::glPolygonOffset(slopeScaledDepthBias, depthBias);

	DRAW_CMD_PRINT("Command glPolygonOffset\n");
}

void	CDeferredContext::Cmd_BlendEquationSeparate(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 colorOp = *PCast<const u32*>(_argv[0]);
	u32 alphaOp = *PCast<const u32*>(_argv[1]);
	::glBlendEquationSeparate(colorOp, alphaOp);

	DRAW_CMD_PRINT("Command glBlendEquationSeparate\n");
}
void	CDeferredContext::Cmd_BlendEquationSeparatei(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 3);
	u32 slot = *PCast<const u32*>(_argv[0]);
	u32 colorOp = *PCast<const u32*>(_argv[1]);
	u32 alphaOp = *PCast<const u32*>(_argv[2]);
	::glBlendEquationSeparatei(slot, colorOp, alphaOp);
}
void	CDeferredContext::Cmd_BlendFuncSeparate(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 4);
	u32 colorSrcFunc = *PCast<const u32*>(_argv[0]);
	u32 colorDstFunc = *PCast<const u32*>(_argv[1]);
	u32 alphaSrcFunc = *PCast<const u32*>(_argv[2]);
	u32 alphaDstFunc = *PCast<const u32*>(_argv[3]);
	::glBlendFuncSeparate(
		colorSrcFunc, colorDstFunc,
		alphaSrcFunc, alphaDstFunc
	);
}
void	CDeferredContext::Cmd_BlendFuncSeparatei(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 5);
	u32 slot = *PCast<const u32*>(_argv[0]);
	u32 colorSrcFunc = *PCast<const u32*>(_argv[1]);
	u32 colorDstFunc = *PCast<const u32*>(_argv[2]);
	u32 alphaSrcFunc = *PCast<const u32*>(_argv[3]);
	u32 alphaDstFunc = *PCast<const u32*>(_argv[4]);
	::glBlendFuncSeparatei(
		slot,
		colorSrcFunc, colorDstFunc,
		alphaSrcFunc, alphaDstFunc
	);
}
void	CDeferredContext::Cmd_ColorMask(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 4);
	const b8* colorMask = PCast<const b8*>(_argv[0]);
	::glColorMask(
		colorMask[0],
		colorMask[1],
		colorMask[2],
		colorMask[3]
	);
}
void	CDeferredContext::Cmd_ColorMaski(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 5);
	u32 slot = *PCast<const u32*>(_argv[0]);
	const b8* colorMask = PCast<const b8*>(_argv[1]);
	::glColorMaski(
		slot,
		colorMask[0],
		colorMask[1],
		colorMask[2],
		colorMask[3]
	);
}

void	CDeferredContext::Cmd_DepthMask(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	b8 isDepthWrite = *PCast<const b8*>(_argv[0]);
	::glDepthMask(isDepthWrite);
}
void	CDeferredContext::Cmd_DepthFunc(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	u32 depthTest = *PCast<const u32*>(_argv[0]);
	::glDepthFunc(depthTest);

	DRAW_CMD_PRINT("Command glDepthFunc\n");
}
void	CDeferredContext::Cmd_StencilMaskSeparate(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 face = *PCast<const u32*>(_argv[0]);
	u32 stencilWriteMask = *PCast<const u32*>(_argv[1]);
	::glStencilMaskSeparate(face, stencilWriteMask);

	DRAW_CMD_PRINT("Command glStencilMaskSeparate\n");
}
void	CDeferredContext::Cmd_StencilFuncSeparate(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 4);
	u32 face = *PCast<const u32*>(_argv[0]);
	u32 stencilTest = *PCast<const u32*>(_argv[1]);
	u32 stencilRef = *PCast<const u32*>(_argv[2]);
	u32 stencilReadMask = *PCast<const u32*>(_argv[3]);
	::glStencilFuncSeparate(face, stencilTest, stencilRef, stencilReadMask);

	DRAW_CMD_PRINT("Command glStencilFuncSeparate\n");
}
void	CDeferredContext::Cmd_StencilOpSeparate(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 4);
	u32 face = *PCast<const u32*>(_argv[0]);
	u32 stencilFailOp = *PCast<const u32*>(_argv[1]);
	u32 stencilZFailOp = *PCast<const u32*>(_argv[2]);
	u32 stencilPassOp = *PCast<const u32*>(_argv[3]);
	::glStencilOpSeparate(face, stencilFailOp, stencilZFailOp, stencilPassOp);

	DRAW_CMD_PRINT("Command glStencilOpSeparate\n");
}

void	CDeferredContext::Cmd_SamplerParameteri(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 3);
	const u32* pObjectID = PCast<const u32*>(_argv[0]);
	libAssert(pObjectID);
	u32 name = *PCast<const u32*>(_argv[1]);
	u32 param = *PCast<const u32*>(_argv[2]);
	::glSamplerParameteri(*pObjectID, name, param);

	DRAW_CMD_PRINT("Command glSamplerParameteri\n");
}
void	CDeferredContext::Cmd_SamplerParameterf(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 3);
	const u32* pObjectID = PCast<const u32*>(_argv[0]);
	libAssert(pObjectID);
	u32 name = *PCast<const u32*>(_argv[1]);
	f32 param = *PCast<const f32*>(_argv[2]);
	::glSamplerParameterf(*pObjectID, name, param);

	DRAW_CMD_PRINT("Command glSamplerParameterf\n");
}
void	CDeferredContext::Cmd_SamplerParameterfv(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 3);
	const u32* pObjectID = PCast<const u32*>(_argv[0]);
	libAssert(pObjectID);
	u32 name = *PCast<const u32*>(_argv[1]);
	const f32* param = PCast<const f32*>(_argv[2]);
	::glSamplerParameterfv(*pObjectID, name, param);

	DRAW_CMD_PRINT("Command glSamplerParameterfv\n");
}

void	CDeferredContext::Cmd_BindProgram(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	u32 programID = *PCast<const u32*>(_argv[0]);
	::glUseProgram(programID);

	DRAW_CMD_PRINT("Command glUseProgram\n");
}
void	CDeferredContext::Cmd_BindProgramPipeline(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	u32 pipelineID = *PCast<const u32*>(_argv[0]);
	::glBindProgramPipeline(pipelineID);

	DRAW_CMD_PRINT("Command glBindProgramPipeline\n");
}
void	CDeferredContext::Cmd_ActiveShaderProgram(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 pipelineID = *PCast<const u32*>(_argv[0]);
	u32 programID = *PCast<const u32*>(_argv[1]);
	::glActiveShaderProgram(pipelineID, programID);

	DRAW_CMD_PRINT("Command glActiveShaderProgram\n");
}

void	CDeferredContext::Cmd_EnableVertexAttribArray(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	u32 attr = *PCast<const u32*>(_argv[0]);
	::glEnableVertexAttribArray(attr);

	DRAW_CMD_PRINT("Command glEnableVertexAttribArray\n");
}
void	CDeferredContext::Cmd_VertexAttribPointer(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 6);
	u32 attr = *PCast<const u32*>(_argv[0]);
	u32 elemNum = *PCast<const u32*>(_argv[1]);
	u32 valType = *PCast<const u32*>(_argv[2]);
	b8 normalized = *PCast<const b8*>(_argv[3]);
	u32 stride = *PCast<const u32*>(_argv[4]);
	u32 offset = *PCast<const u32*>(_argv[5]);
	::glVertexAttribPointer(attr, elemNum, valType, normalized, stride, (void*)offset);

	DRAW_CMD_PRINT("Command glVertexAttribPointer\n");
}
void	CDeferredContext::Cmd_DrawArrays(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 3);
	u32 prim = *PCast<const u32*>(_argv[0]);
	u32 offset = *PCast<const u32*>(_argv[1]);
	u32 num = *PCast<const u32*>(_argv[2]);
	::glDrawArrays(prim, offset, num);

	DRAW_CMD_PRINT("Command glDrawArrays\n");
}
void	CDeferredContext::Cmd_DrawIndexed(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 5);
	u32 prim = *PCast<const u32*>(_argv[0]);
	u32 num = *PCast<const u32*>(_argv[1]);
	u32 valType = *PCast<const u32*>(_argv[2]);
	u32 idxOffset = *PCast<const u32*>(_argv[3]);
	u32 vtxOffset = *PCast<const u32*>(_argv[4]);
	::glDrawElementsBaseVertex(prim, num, valType, (void*)idxOffset, vtxOffset);

	DRAW_CMD_PRINT("Command glDrawElementsBaseVertex\n");
}

void	CDeferredContext::Cmd_Flush(u32 _argc, const void* _argv[])
{
	::glFlush();

	DRAW_CMD_PRINT("Command glFlush\n");
}

void	CDeferredContext::Cmd_BeginQuery(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	u32 target = *PCast<const u32*>(_argv[0]);
	const u32* pQueryID = PCast<const u32*>(_argv[1]);
	libAssert(pQueryID);
	::glBeginQuery(target, *pQueryID);

	DRAW_CMD_PRINT("Command glBeginQuery\n");
}
void	CDeferredContext::Cmd_EndQuery(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 1);
	u32 target = *PCast<const u32*>(_argv[0]);
	::glEndQuery(target);

	DRAW_CMD_PRINT("Command glEndQuery\n");
}
void	CDeferredContext::Cmd_BeginConditionalRender(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 2);
	const u32* pQueryID = PCast<const u32*>(_argv[0]);
	libAssert(pQueryID);
	u32 mode = *PCast<const u32*>(_argv[1]);
	::glBeginConditionalRender(*pQueryID, mode);

	DRAW_CMD_PRINT("Command glBeginConditionalRender\n");
}
void	CDeferredContext::Cmd_EndConditionalRender(u32 _argc, const void* _argv[])
{
	::glEndConditionalRender();

	DRAW_CMD_PRINT("Command glEndConditionalRender\n");
}
void	CDeferredContext::Cmd_PushDebugGroup(u32 _argc, const void* _argv[])
{
	libAssert(_argc >= 4);
	u32 source = *PCast<const u32*>(_argv[0]);
	const u32* pQueryID = PCast<const u32*>(_argv[1]);
	//libAssert(pQueryID);
	u32 queryID = pQueryID ? *pQueryID : NULL;
	u32 strLen = *PCast<const u32*>(_argv[2]);
	const c8* pStr = PCast<const c8*>(_argv[3]);
	::glPushDebugGroup(source, queryID, strLen, pStr);

	DRAW_CMD_PRINT("Command glPushDebugGroup\n");
}
void	CDeferredContext::Cmd_PopDebugGroup(u32 _argc, const void* _argv[])
{
	::glPopDebugGroup();

	DRAW_CMD_PRINT("Command glPopDebugGroup\n");
}


POISON_END

//#endif // LIB_GFX_DX11
#endif // LIB_GFX_OPENGL

﻿
//#ifdef LIB_GFX_DX11
#ifdef LIB_GFX_OPENGL

///-------------------------------------------------------------------------------------------------
/// define
#define DRAW_CMD_ALIGN	16
//#define DRAW_CMD_PRINT(...)		__PRINT_ATTR(POISON::libPrintAttrDefault,__VA_ARGS__)
#define DRAW_CMD_PRINT(...)		(__VA_ARGS__)



///-------------------------------------------------------------------------------------------------
/// include
#include "device/device_def.h"
#include "gl/draw_context/immediate_draw_context.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant



CImmediateContext::CImmediateContext()
{
}

CImmediateContext::~CImmediateContext()
{
}


///*************************************************************************************************
///	描画コマンド
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// その他
void	CImmediateContext::Enable(u32 _cap)
{
	::glEnable(_cap);
}

void	CImmediateContext::Enablei(u32 _slot, u32 _cap)
{
	::glEnablei(_cap, _slot);
}

void	CImmediateContext::Disable(u32 _cap)
{
	::glDisable(_cap);
}

void	CImmediateContext::Disablei(u32 _slot, u32 _cap)
{
	::glDisablei(_cap, _slot);
}


///-------------------------------------------------------------------------------------------------
/// バインド
void	CImmediateContext::BindFramebuffer(u32 _target, const u32* _pObjectID)
{
	libAssert(_pObjectID);
	::glBindFramebuffer(_target, *_pObjectID);
}

void	CImmediateContext::BindRenderbuffer(u32 _target, const u32* _pObjectID)
{
	libAssert(_pObjectID);
	::glBindRenderbuffer(_target, *_pObjectID);
}

void	CImmediateContext::BindTexture(u32 _target, const u32* _pObjectID)
{
	libAssert(_pObjectID);
	::glBindTexture(_target, *_pObjectID);
}

void	CImmediateContext::BindBuffer(u32 _target, const u32* _pObjectID)
{
	libAssert(_pObjectID);
	::glBindBuffer(_target, *_pObjectID);
}

void	CImmediateContext::BindBufferBase(u32 _target, u32 _slot, const u32* _pObjectID)
{
	libAssert(_pObjectID);
	::glBindBufferBase(_target, _slot, *_pObjectID);
}

void	CImmediateContext::ActiveTexture(u32 _slot)
{
	::glActiveTexture(_slot);
}

void	CImmediateContext::BindSampler(u32 _slot, const u32* _pObjectID)
{
	libAssert(_pObjectID);
	::glBindSampler(_slot, *_pObjectID);
}


///-------------------------------------------------------------------------------------------------
/// アンバインド
void	CImmediateContext::UnBindBuffer(u32 _target)
{
	::glBindBuffer(_target, NULL);
}

void	CImmediateContext::UnBindFramebuffer(u32 _target)
{
	::glBindFramebuffer(_target,  NULL);
}

void	CImmediateContext::UnBindRenderbuffer(u32 _target)
{
	::glBindRenderbuffer(_target, NULL);
}

void	CImmediateContext::UnBindTexture(u32 _target)
{
	::glBindTexture(_target, NULL);
}


///-------------------------------------------------------------------------------------------------
/// 生成
void	CImmediateContext::GenBuffers(u32 _num, u32* _pObjectID)
{
	::glGenBuffers(_num, _pObjectID);
}

void	CImmediateContext::GenFramebuffers(u32 _num, u32* _pObjectID)
{
	::glGenFramebuffers(_num, _pObjectID);
}

void	CImmediateContext::GenRenderbuffers(u32 _num, u32* _pObjectID)
{
	::glGenRenderbuffers(_num, _pObjectID);
}

void	CImmediateContext::GenTextures(u32 _num, u32* _pObjectID)
{
	::glGenTextures(_num, _pObjectID);
}

void	CImmediateContext::GenSamplers(u32 _num, u32* _pObjectID)
{
	::glGenSamplers(_num, _pObjectID);
}

void	CImmediateContext::GenQueries(u32 _num, u32* _pQueryID)
{
	::glGenQueries(_num, _pQueryID);
}


///-------------------------------------------------------------------------------------------------
/// 破棄
void	CImmediateContext::DeleteBuffers(u32 _num, u32* _pObjectID)
{
	::glDeleteBuffers(_num, _pObjectID);
}

void	CImmediateContext::DeleteFramebuffers(u32 _num, u32* _pObjectID)
{
	::glDeleteFramebuffers(_num, _pObjectID);
}

void	CImmediateContext::DeleteRenderbuffers(u32 _num, u32* _pObjectID)
{
	::glDeleteRenderbuffers(_num, _pObjectID);
}

void	CImmediateContext::DeleteTextures(u32 _num, u32* _pObjectID)
{
	::glDeleteTextures(_num, _pObjectID);
}

void	CImmediateContext::DeleteSamplers(u32 _num, u32* _pObjectID)
{
	::glDeleteSamplers(_num, _pObjectID);
}

void	CImmediateContext::DeleteQueries(u32 _num, u32* _pQueryID)
{
	::glDeleteQueries(_num, _pQueryID);
}


///-------------------------------------------------------------------------------------------------
/// ストレージ確保
void	CImmediateContext::BufferData(u32 _target, u32 _size, const void* _pData, u32 _usage)
{
	::glBufferData(_target, _size, _pData, _usage);
}

void	CImmediateContext::RenderbufferStorage(u32 _target, u32 _format, u32 _width, u32 _height)
{
	::glRenderbufferStorage(_target, _format, _width, _height);
}

void	CImmediateContext::RenderbufferStorageMultisample(u32 _target, u32 _aa, u32 _format, u32 _width, u32 _height)
{
	::glRenderbufferStorageMultisample(_target, _aa, _format, _width, _height);
}

void	CImmediateContext::TexImage2D(u32 _target, u32 _level, u32 _format, u32 _width, u32 _height, u32 _texFormat, u32 _valType, const void* _pData)
{
	::glTexImage2D(
		_target,		// ターゲット
		_level,			// ミットマップレベル
		_format,		// フォーマット
		_width,			// 幅
		_height,		// 高さ
		0,				// よくわからん
		_texFormat,		// データフォーマット
		_valType,		// データ型
		_pData			// データ
	);
}

void	CImmediateContext::TexImage2DMultisample(u32 _target, u32 _aa, u32 _format, u32 _width, u32 _height)
{
	::glTexImage2DMultisample(
		_target,		// ターゲット
		_aa,			// AA数
		_format,		// フォーマット
		_width,			// 幅
		_height,		// 高さ
		GL_TRUE			// 全テクセルでのサンプル数固定フラグ？
	);
}

void	CImmediateContext::TexImage3D(u32 _target, u32 _level, u32 _format, u32 _width, u32 _height, u32 _depth, u32 _texFormat, u32 _valType, const void* _pData)
{
	::glTexImage3D(
		_target,		// ターゲット
		_level,			// ミットマップレベル
		_format,		// フォーマット
		_width,			// 幅
		_height,		// 高さ
		_depth,			// 奥行き
		0,				// よくわからん
		_texFormat,		// データフォーマット
		_valType,		// データ型
		_pData			// データ
	);
}

void	CImmediateContext::TexStorage2D(u32 _target, u32 _level, u32 _format, u32 _width, u32 _height)
{
	::glTexStorage2D(
		_target,		// ターゲット
		_level,			// ミットマップレベル
		_format,		// フォーマット
		_width,			// 幅
		_height			// 高さ
	);
}

void	CImmediateContext::TexStorage2DMultisample(u32 _target, u32 _aa, u32 _format, u32 _width, u32 _height)
{
	::glTexStorage2DMultisample(
		_target,		// ターゲット
		_aa,			// AA数
		_format,		// フォーマット
		_width,			// 幅
		_height,		// 高さ
		GL_TRUE			// 全テクセルでのサンプル数固定フラグ？
	);
}

void	CImmediateContext::TexStorage3D(u32 _target, u32 _level, u32 _format, u32 _width, u32 _height, u32 _depth)
{
	::glTexStorage3D(
		_target,		// ターゲット
		_level,			// ミットマップレベル
		_format,		// フォーマット
		_width,			// 幅
		_height,		// 高さ
		_depth			// 奥行き（配列数）
	);
}


///-------------------------------------------------------------------------------------------------
/// 更新
void	CImmediateContext::MapBuffer(u32 _target, u32 _size, const void* _pData)
{
#ifdef USE_SUBDATA_BUFFER
	::glBufferSubData(_target, 0, _size, _pData);
#else
	void* ptr = ::glMapBuffer(_target, GL_WRITE_ONLY);
	memcpy(ptr, _pData, _size);
	::glUnmapBuffer(_target);
#endif // USE_SUBDATA_BUFFER
}

void	CImmediateContext::TexSubImage2D(u32 _target, u32 _level, u32 _xoff, u32 _yoff, u32 _mipWidth, u32 _mipHeight, u32 _texFormat, u32 _valType, const void* _pData)
{
	::glTexSubImage2D(
		_target,		// ターゲット
		_level,			// ミットマップレベル
		_xoff, _yoff,	// オフセット
		_mipWidth,		// 幅
		_mipHeight,		// 高さ
		_texFormat,		// データフォーマット
		_valType,		// データ型
		_pData			// データ
	);
}

void	CImmediateContext::TexSubImage3D(u32 _target, u32 _level, u32 _xoff, u32 _yoff, u32 _zoff, u32 _mipWidth, u32 _mipHeight, u32 _mipDepth, u32 _texFormat, u32 _valType, const void* _pData)
{
	::glTexSubImage3D(
		_target,			// ターゲット
		_level,				// ミットマップレベル
		_xoff, _yoff, _zoff,// オフセット
		_mipWidth,			// 幅
		_mipHeight,			// 高さ
		_mipDepth,			// 奥行き（配列テクスチャなので１）
		_texFormat,			// データフォーマット
		_valType,			// データ型
		_pData				// データ
	);
}


///-------------------------------------------------------------------------------------------------
/// 情報取得
void	CImmediateContext::CheckFramebufferStatus(const u32* _pObjectID)
{
	// 現在バインドされているフレームバッファオブジェクトを取得
	s32 currentFrameBufferObject = 0;
	::glGetIntegerv(GL_FRAMEBUFFER_BINDING, &currentFrameBufferObject);

	// バインド
	libAssert(_pObjectID);
	::glBindFramebuffer(GL_FRAMEBUFFER, *_pObjectID);

	// フレームバッファステータスチェック
	u32 status = ::glCheckFramebufferStatusEXT(GL_FRAMEBUFFER);
	switch (status)
	{
	case GL_FRAMEBUFFER_COMPLETE:
		//PRINT("Framebuffer complete\n");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
		PRINT_ERROR("*****[ERROR] Framebuffer incomplete: Attachment is NOT complete\n");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
		PRINT_ERROR("*****[ERROR] Framebuffer incomplete: No image is attached to FBO\n");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT:
		PRINT_ERROR("*****[ERROR] Framebuffer incomplete: Attached images have different dimensions\n");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT:
		PRINT_ERROR("*****[ERROR] Framebuffer incomplete: Color attached images have different internal formats\n");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
		PRINT_ERROR("*****[ERROR] Framebuffer incomplete: Draw buffer\n");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
		PRINT_ERROR("*****[ERROR] Framebuffer incomplete: Read buffer\n");
		break;

	case GL_FRAMEBUFFER_UNSUPPORTED:
		PRINT_ERROR("*****[ERROR] Unsupported by FBO implementation\n");
		break;

	default:
		PRINT_ERROR("*****[ERROR] Unknow error\n");
		break;
	}

	//	アンバインド
	::glBindFramebuffer(GL_FRAMEBUFFER, currentFrameBufferObject);
}

void	CImmediateContext::GetTexLevelParameteriv(u32 _target, u32 _level, u32 _name, s32* _pParam)
{
	::glGetTexLevelParameteriv(
		_target,
		_level,
		_name,
		_pParam
	);
}

void	CImmediateContext::GetQueryObjectiv(const u32* _pQueryID, u32 _name, s32* _pParam)
{
	libAssert(_pQueryID);
	::glGetQueryObjectiv(
		*_pQueryID,
		_name,
		_pParam
	);
}

void	CImmediateContext::GetQueryObjectuiv(const u32* _pQueryID, u32 _name, u32* _pParam)
{
	libAssert(_pQueryID);
	::glGetQueryObjectuiv(
		*_pQueryID,
		_name,
		_pParam
	);
}

void	CImmediateContext::GetQueryObjecti64v(const u32* _pQueryID, u32 _name, s64* _pParam)
{
	libAssert(_pQueryID);
	::glGetQueryObjecti64v(
		*_pQueryID,
		_name,
		_pParam
	);
}

void	CImmediateContext::GetQueryObjectui64v(const u32* _pQueryID, u32 _name, u64* _pParam)
{
	libAssert(_pQueryID);
	::glGetQueryObjectui64v(
		*_pQueryID,
		_name,
		_pParam
	);
}


///-------------------------------------------------------------------------------------------------
/// レンダーターゲット周り
void	CImmediateContext::FramebufferRenderbuffer(u32 _frameTrg, u32 _slot, u32 _colorTrg, const u32* _pObjectID)
{
	libAssert(_pObjectID);
	::glFramebufferRenderbuffer(
		_frameTrg,
		_slot,
		_colorTrg,
		*_pObjectID
	);
}

void	CImmediateContext::FramebufferTexture2D(u32 _frameTrg, u32 _slot, u32 _textureTrg, const u32* _pObjectID, u32 _level)
{
	libAssert(_pObjectID);
	::glFramebufferTexture2D(
		_frameTrg,
		_slot,
		_textureTrg,
		*_pObjectID,
		_level
	);
}

void	CImmediateContext::DrawBuffers(u32 _num, const u32* _pColorTrg)
{
	::glDrawBuffers(_num, _pColorTrg);
}


///-------------------------------------------------------------------------------------------------
/// クリアバッファ
void	CImmediateContext::ClearBufferColor(f32 _r, f32 _g, f32 _b, f32 _a)
{
	f32 clr[] = { _r, _g, _b, _a };
	::glClearBufferfv(
		GL_COLOR,
		0,
		clr
	);
}

void	CImmediateContext::ClearBufferDepth(f32 _depth)
{
	::glClearBufferfv(
		GL_DEPTH,
		0,
		&_depth
	);
}

void	CImmediateContext::ClearBufferStencil(s32 _stencil)
{
	::glClearBufferiv(
		GL_STENCIL,
		0,
		&_stencil
	);
}

void	CImmediateContext::ClearColor(f32 _r, f32 _g, f32 _b, f32 _a)
{
	::glClearColor(_r, _g, _b, _a);
}

void	CImmediateContext::ClearDepth(f32 _depth)
{
	::glClearDepth(_depth);
}

void	CImmediateContext::ClearStencil(s32 _stencil)
{
	::glClearStencil(_stencil);
}

void	CImmediateContext::Clear(u32 _bit)
{
	::glClear(_bit);
}


///-------------------------------------------------------------------------------------------------
/// ラスタライザー
void	CImmediateContext::Viewport(s32 _posX, s32 _posY, u32 _width, u32 _height)
{
	::glViewport(
		_posX,
		_posY,
		_width,
		_height
	);
}

void	CImmediateContext::Scissor(s32 _posX, s32 _posY, u32 _width, u32 _height)
{
	::glScissor(
		_posX,
		_posY,
		_width,
		_height
	);
}

void	CImmediateContext::DepthRange(f32 _near, f32 _far)
{
	::glDepthRangef(_near, _far);
}

void	CImmediateContext::FrontFace(u32 _faceType)
{
	::glFrontFace(_faceType);
}

void	CImmediateContext::CullFace(u32 _cullType)
{
	::glCullFace(_cullType);
}

void	CImmediateContext::PolygonMode(u32 _face, u32 _mode)
{
	::glPolygonMode(_face, _mode);
}

void	CImmediateContext::PolygonOffset(f32 _slopeScale, f32 _bias)
{
	::glPolygonOffset(_slopeScale, _bias);
}


///-------------------------------------------------------------------------------------------------
/// ブレンドステート
void	CImmediateContext::BlendEquationSeparate(u32 _colorOp, u32 _alphaOp)
{
	::glBlendEquationSeparate(_colorOp, _alphaOp);
}

void	CImmediateContext::BlendEquationSeparatei(u32 _slot, u32 _colorOp, u32 _alphaOp)
{
	::glBlendEquationSeparatei(_slot, _colorOp, _alphaOp);
}

void	CImmediateContext::BlendFuncSeparate(u32 _colorSrcFunc, u32 _colorDstFunc, u32 _alphaSrcFunc, u32 _alphaDstFunc)
{
	::glBlendFuncSeparate(
		_colorSrcFunc, _colorDstFunc,
		_alphaSrcFunc, _alphaDstFunc
	);
}

void	CImmediateContext::BlendFuncSeparatei(u32 _slot, u32 _colorSrcFunc, u32 _colorDstFunc, u32 _alphaSrcFunc, u32 _alphaDstFunc)
{
	::glBlendFuncSeparatei(
		_slot,
		_colorSrcFunc, _colorDstFunc,
		_alphaSrcFunc, _alphaDstFunc
	);
}

void	CImmediateContext::ColorMask(b8 _r, b8 _g, b8 _b, b8 _a)
{
	::glColorMask(_r, _g, _b, _a);
}

void	CImmediateContext::ColorMaski(u32 _slot, b8 _r, b8 _g, b8 _b, b8 _a)
{
	::glColorMaski(_slot, _r, _g, _b, _a);
}


///-------------------------------------------------------------------------------------------------
/// デプスステンシルステート
void	CImmediateContext::DepthMask(b8 _isWrite)
{
	::glDepthMask(_isWrite);
}

void	CImmediateContext::DepthFunc(u32 _depthTest)
{
	::glDepthFunc(_depthTest);
}

void	CImmediateContext::StencilMaskSeparate(u32 _face, u32 _mask)
{
	::glStencilMaskSeparate(_face, _mask);
}

void	CImmediateContext::StencilFuncSeparate(u32 _face, u32 _test, u32 _ref, u32 _readMask)
{
	::glStencilFuncSeparate(_face, _test, _ref, _readMask);
}

void	CImmediateContext::StencilOpSeparate(u32 _face, u32 _failOp, u32 _ZFailOp, u32 _passOp)
{
	::glStencilOpSeparate(_face, _failOp, _ZFailOp, _passOp);
}


///-------------------------------------------------------------------------------------------------
/// サンプラーステート
void	CImmediateContext::SamplerParameteri(const u32* _pObjectID, u32 _name, u32 _param)
{
	libAssert(_pObjectID);
	::glSamplerParameteri(*_pObjectID, _name, _param);
}

void	CImmediateContext::SamplerParameterf(const u32* _pObjectID, u32 _name, f32 _param)
{
	libAssert(_pObjectID);
	::glSamplerParameterf(*_pObjectID, _name, _param);
}

void	CImmediateContext::SamplerParameterfv(const u32* _pObjectID, u32 _name, const f32* _param)
{
	libAssert(_pObjectID);
	::glSamplerParameterfv(*_pObjectID, _name, _param);
}


///-------------------------------------------------------------------------------------------------
/// シェーダ
void	CImmediateContext::BindProgram(u32 _programID)
{
	::glUseProgram(_programID);
}

void	CImmediateContext::BindProgramPipeline(u32 _pipelineID)
{
	::glBindProgramPipeline(_pipelineID);
}

void	CImmediateContext::ActiveShaderProgram(u32 _pipelineID, u32 _programID)
{
	::glActiveShaderProgram(_pipelineID, _programID);
}


///-------------------------------------------------------------------------------------------------
/// 頂点バッファ
void	CImmediateContext::EnableVertexAttribArray(u32 _attr)
{
	::glEnableVertexAttribArray(_attr);
}

void	CImmediateContext::VertexAttribPointer(u32 _attr, u32 _elemNum, u32 _valType, b8 _normalized, u32 _stride, u32 _offset)
{
	::glVertexAttribPointer(_attr, _elemNum, _valType, _normalized, _stride, (void*)_offset);
}

void	CImmediateContext::DrawArrays(u32 _primType, u32 _vtxOffset, u32 _vtxNum)
{
	::glDrawArrays(_primType, _vtxOffset, _vtxNum);
}

void	CImmediateContext::DrawIndexed(u32 _primType, u32 _valType, u32 _idxOffset, u32 _vtxOffset, u32 _idxNum)
{
	::glDrawElementsBaseVertex(_primType, _idxNum, _valType, (void*)_idxOffset, _vtxOffset);
}


///-------------------------------------------------------------------------------------------------
/// フラッシュ
void	CImmediateContext::Flush()
{
	::glFlush();
}


///-------------------------------------------------------------------------------------------------
/// クエリー
void	CImmediateContext::BeginQuery(u32 _target, const u32* _pQueryID)
{
	libAssert(_pQueryID);
	::glBeginQuery(_target, *_pQueryID);
}

void	CImmediateContext::EndQuery(u32 _target)
{
	::glEndQuery(_target);
}

void	CImmediateContext::BeginConditionalRender(const u32* _pQueryID, u32 _mode)
{
	libAssert(_pQueryID);
	::glBeginConditionalRender(*_pQueryID, _mode);
}

void	CImmediateContext::EndConditionalRender()
{
	::glEndConditionalRender();
}

void	CImmediateContext::PushDebugGroup(u32 _source, const u32* _pQueryID, u32 _srtLen, const c8* _pStr)
{
	::glPushDebugGroup(_source, _pQueryID  ? *_pQueryID : NULL, _srtLen, _pStr);
}

void	CImmediateContext::PopDebugGroup()
{
	::glPopDebugGroup();
}



POISON_END

//#endif // LIB_GFX_DX11
#endif // LIB_GFX_OPENGL

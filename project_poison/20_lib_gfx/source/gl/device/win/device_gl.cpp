﻿
#ifdef LIB_GFX_OPENGL

///-------------------------------------------------------------------------------------------------
/// include
#include "gl/device/win/device_gl.h"

#include "utility/gfx_utility.h"
#include "gl/utility/gfx_utility_gl.h"

#include "format/gfx_format_utility.h"
#include "gl/format/gfx_format_utility_gl.h"

// shader
#include "shader/shader.h"

// object
#include "object/color_buffer.h"
#include "object/depth_buffer.h"
#include "object/texture_buffer.h"
#include "object/uniform_buffer.h"
#include "object/vertex_buffer.h"

#include "object/sampler_state.h"
#include "object/rasterrizer_state.h"
#include "object/blend_state.h"
#include "object/depth_state.h"


POISON_BGN


///*************************************************************************************************
///	グラフィックデバイスクラス
///*************************************************************************************************
CDeviceGL::CDeviceGL()
	: m_hDc(NULL)
	, m_hGlrc(NULL)
	, m_push_hDc(NULL)
{
}
CDeviceGL::~CDeviceGL()
{
}

///-------------------------------------------------------------------------------------------------
///	初期化開始
b8		CDeviceGL::InitBegin(MEM_HANDLE _memHdl)
{
	// 基底クラスたちの初期化
	if (CDeviceWin::InitBegin(_memHdl) == false){ return false; }

	// 即時コンテキスト設定
	m_pDrawContext = &m_immediateContext;

	return true;
}

///-------------------------------------------------------------------------------------------------
///	初期化終了
b8		CDeviceGL::InitEnd()
{
	// 基底クラスたちの初期化
	if (CDeviceWin::InitEnd() == false){ return false; }

	// openGLの初期化
	return InitOpenGL();
}

///-------------------------------------------------------------------------------------------------
///	終了
void	CDeviceGL::Finalize()
{
	// カレントコンテキストを無効にする
	::wglMakeCurrent(NULL, NULL);

	// カレントコンテキストを削除
	if (m_hGlrc)
	{
		::wglDeleteContext(m_hGlrc);
	}

	// デバイスコンテキスト解放
	if (m_push_hDc)
	{
		::ReleaseDC(m_hWnd, m_push_hDc);
	}
	if (m_hDc)
	{
		::ReleaseDC(m_hWnd, m_hDc);
	}


	// 基底クラスたちの終了
	CDeviceWin::Finalize();
}


///-------------------------------------------------------------------------------------------------
///	キャンバス生成
b8		CDeviceGL::CreateCanvas(const CANVAS_DESC& _desc)
{
	// デバイスコンテキスト生成
	m_hDc = ::GetDC(m_hWnd);

	// 現在対応しているフォーマットを列挙する
#if 0
	{
		// 現在対応しているフォーマットの数を参照する
		int format_count = ::DescribePixelFormat(m_hDc, 0, 0, NULL);
		for (int fi = 1; fi <= format_count; fi++)
		{
			::PIXELFORMATDESCRIPTOR pformat;
			::DescribePixelFormat(m_hDc, fi, sizeof(PIXELFORMATDESCRIPTOR), &pformat);
			PRINT("----------------------------------------------------------------------------\n");
			PRINT("version : %d\n", pformat.nVersion);
			PRINT("dwFlags : %d\n", pformat.dwFlags);
			PRINT("iPixelType : %d\n", pformat.iPixelType);
			PRINT("cColorBits : %d\n", pformat.cColorBits);
			PRINT("RGBA : R[%d][%d] G[%d][%d] B[%d][%d] A[%d][%d]\n",
				pformat.cRedBits, pformat.cRedShift,
				pformat.cGreenBits, pformat.cGreenShift,
				pformat.cBlueBits, pformat.cBlueShift,
				pformat.cAlphaBits, pformat.cAlphaBits
				);
			PRINT("cAccumBits : %d\n", pformat.cAccumBits);
			PRINT("cAccumRGBABits : R[%d] G[%d] B[%d] A[%d]\n",
				pformat.cAccumRedBits, pformat.cAccumGreenBits, pformat.cAccumBlueBits, pformat.cAccumAlphaBits
				);
			PRINT("cDepthBits : %d\n", pformat.cDepthBits);
			PRINT("cStencilBits : %d\n", pformat.cStencilBits);
			PRINT("cAuxBuffers : %d\n", pformat.cAuxBuffers);
			PRINT("iLayerType : %d\n", pformat.iLayerType);
			PRINT("bReserved : %d\n", pformat.bReserved);
			PRINT("dwLayerMask : %d\n", pformat.dwLayerMask);
			PRINT("dwVisibleMask : %d\n", pformat.dwVisibleMask);
			PRINT("dwDamageMask : %d\n", pformat.dwDamageMask);
			PRINT("[%d / %d]\n", fi, format_count);
			PRINT("----------------------------------------------------------------------------\n");
			PRINT("\n");
		}
	}
#endif // 0

	// 指定がなければクライアントサイズで設定
	u32 width = _desc.width;
	u32 height = _desc.height;
	// OpenGLの場合強制的にウィンドウに合わせる
	//if (width <= 0 && height <= 0)
	{
		GetClientSize(width, height);
	}

	// フォーマットフラグ
	u32 flags = (PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_SWAP_LAYER_BUFFERS);
	// カラービットサイズ
	u32 colorBitSize = CGfxFormatUtility::GetImageFormatSize(_desc.clrFormat);
	// デプス・ステンシルビットサイズ
	u32 depthBitSize = 0, stencilBitSize = 0;
	if (_desc.depFormat != GFX_FORMAT_UNKNOWN)
	{
		CGfxFormatUtility::GetDepthStencilFormat(depthBitSize, stencilBitSize, _desc.depFormat);
	}
	else
	{
		flags |= PFD_DEPTH_DONTCARE;	// デプスなし
	}

	// ピクセルフォーマット初期化
	::PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(::PIXELFORMATDESCRIPTOR),
		1,							// Version number 
		flags,						// Flags
		PFD_TYPE_RGBA,				// The kind of framebuffer. RGBA or palette.
		SCast<BYTE>(colorBitSize),	// Colordepth of the framebuffer.
		0, 0, 0, 0, 0, 0, 0, 0,
		0,							// Number of bits for the accumulationbuffer（よくわからん）
		0, 0, 0, 0,
		SCast<BYTE>(depthBitSize),	// Number of bits for the depthbuffer
		SCast<BYTE>(stencilBitSize),// Number of bits for the stencilbuffer
		0,							// Number of Aux buffers in the framebuffer.（使用されないっぽい）
		0,							// レイヤー指定（使用されないっぽい）
		0,							// オーバーレイ、アンダーレイのプレーン数を指定（よくわからん）
		0,							// レイヤーマスク指定（使用されないっぽい）
		0,							// アンダーレイプレーンの透過色又はインデックス指定（よくわからん）
		0							// よくわからん（使用されないっぽい）
	};

	// フォーマット自動選定
	int format = ::ChoosePixelFormat(m_hDc, &pfd);
	if (format == 0)
	{
		// 該当するピクセルフォーマットが無い
		PRINT("************[Error] Failed : Not Find Pixel Format\n");
		return false;
	}

	// OpenGLが使うデバイスコンテキストに指定のピクセルフォーマットをセット
	if (!::SetPixelFormat(m_hDc, format, &pfd))
	{
		// DCへフォーマットを設定するのに失敗
		PRINT("************[Error] Failed : ::SetPixelFormat()\n");
		return false;
	}

	m_width = width;
	m_height = height;
	m_swapNum = _desc.swapNum;
	m_refreshRate = _desc.refreshRate;
	m_viewport.posX = m_viewport.posY = 0;
	m_viewport.width = width;
	m_viewport.height = height;
	m_clearClr = _desc.clearColor;

	return (m_hDc != NULL);
}

///-------------------------------------------------------------------------------------------------
/// キャンパスバインド
void	CDeviceGL::BindCanvas()
{
	DRAW_CONTEXT* pContext = GetContext();
	// フレームアンバインド
	pContext->UnBindFramebuffer(GL_FRAMEBUFFER);
}

///-------------------------------------------------------------------------------------------------
/// クリアサーフェイス
void	CDeviceGL::ClearSurface(const CVec4& _clearClr)
{
	DRAW_CONTEXT* pContext = GetContext();
	// キャンパスバインド
	BindCanvas();
	// ビューポート設定
	pContext->Viewport(0, 0, GetWidth(), GetHeight());
	// シザリング設定
	pContext->Enable(GL_SCISSOR_TEST);
	pContext->Scissor(0, 0, GetWidth(), GetHeight());
	pContext->ClearColor(_clearClr[0], _clearClr[1], _clearClr[2], _clearClr[3]);
	pContext->ClearDepth(CLEAR_DEPTH);
	pContext->ClearStencil(CLEAR_STENCIL);
	pContext->Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);	// 全部クリアしてまえ
}

///-------------------------------------------------------------------------------------------------
///	描画開始
void	CDeviceGL::DrawBegin()
{
}

///-------------------------------------------------------------------------------------------------
///	描画終了
void	CDeviceGL::DrawEnd()
{
}

///-------------------------------------------------------------------------------------------------
///	スワップバッファ
void	CDeviceGL::SwapBuffer(const CTextureBuffer* _pTexture, SCREEN_ALIGN _align/* = SCREEN_ALIGN_CENTER_MIDDLE*/, SCREEN_STRETCH _stretch/* = SCREEN_STRETCH_FIT*/)
{
	// ビューポート更新
	UpdateScreenViewprt(_pTexture->GetWidth(), _pTexture->GetHeight(), _align, _stretch);
	const CRect& screenViewport = GetViewport();

	// はみ出し領域があるなら画面クリア
	if (screenViewport.posX > 0 || screenViewport.posY > 0 ||
		screenViewport.width < GetWidth() || screenViewport.height < GetHeight())
	{
		ClearSurface(GetClearColor());
	}

	// キャンパスバインド
	BindCanvas();

	DRAW_CONTEXT* pContext = GetContext();

	// シェーダ設定
	const PassShader* pPassShader = CGfxUtility::GetCopyShader();
	libAssert(pPassShader);
	{
#ifdef USE_LINK_PROGRAM
		pContext->UseProgram(pPassShader->GetObject().programID);
#else
		pContext->BindProgram(NULL);
		pContext->BindProgramPipeline(pPassShader->GetObject().pipelineID);
#endif // USE_LINK_PROGRAM
	}

	// 頂点バッファ設定
	{
		const VTX_DECL* pDecl = pPassShader->GetVtxDecl();
		u32 declNum = pPassShader->GetDeclNum();
		const CVertexBuffer& vtxBuffer = CGfxUtility::GetScreenVtx();
		u32 vtxStride = vtxBuffer.GetVtxStride();
		u32 valType;
		CGfxFormatUtilityGL::ConvertValueType(valType, pDecl->valType);
		const RENDER_OBJECT& vtxObj = vtxBuffer.GetObject();
		pContext->BindBuffer(vtxObj.target, &vtxObj.objectID);
		pContext->EnableVertexAttribArray(pDecl->attr);
		pContext->VertexAttribPointer(pDecl->attr, pDecl->elemNum, valType, false, vtxStride, 0);
		pDecl++;
		for (u32 idx = 1; idx < declNum; idx++, pDecl++)
		{
			CGfxFormatUtilityGL::ConvertValueType(valType, pDecl->valType);
			u32 offset = pDecl->offset;
			pContext->EnableVertexAttribArray(pDecl->attr);
			pContext->VertexAttribPointer(pDecl->attr, pDecl->elemNum, valType, false, vtxStride, offset);
		}
	}

	// テクスチャ設定
	{
		const TEXTURE_OBJECT& texObj = _pTexture->GetObject();
#ifndef USE_LINK_PROGRAM
		const PShader* pPShader = pPassShader->GetPShader();
		pContext->ActiveShaderProgram(pPassShader->GetObject().pipelineID, pPShader->GetObject().programID);
#endif // !USE_LINK_PROGRAM
		u32 glSlot;
		CGfxFormatUtilityGL::ConvertTextureSlot(glSlot, 0);
		pContext->ActiveTexture(glSlot);
		pContext->BindTexture(texObj.target, &texObj.objectID);
	}

	// サンプラー設定
	{
		const CSampler& sampler = CGfxUtility::GetDefaultSampler();
		pContext->BindSampler(0, &sampler.GetObject().objectID);
	}

	// ビューポート設定
	{
		pContext->Viewport(
			screenViewport.posX,
#if defined( OPENGL_VIEWPORT )
			screenViewport.posY,
#elif defined( DIRECTX_VIEWPORT )
			GetHeight() - screenViewport.height - screenViewport.posY,
#endif
			screenViewport.width,
			screenViewport.height
			);
	}
	// シザリング設定
	{
		pContext->Enable(GL_SCISSOR_TEST);
		pContext->Scissor(
			screenViewport.posX,
#if defined( OPENGL_VIEWPORT )
			screenViewport.posY,
#elif defined( DIRECTX_VIEWPORT )
			GetHeight() - screenViewport.height - screenViewport.posY,
#endif
			screenViewport.width,
			screenViewport.height
			);
	}

	// ラスタライザ設定
	{
		const CRasterizer& state = CGfxUtility::GetDefaultRasterizer();
		const RASTERIZER_OBJECT& object = state.GetObject();
		pContext->Enable(GL_CULL_FACE);
		pContext->Enable(GL_POLYGON_OFFSET_FILL);
		pContext->Enable(GL_POLYGON_OFFSET_LINE);
		pContext->Enable(GL_POLYGON_OFFSET_POINT);

		pContext->CullFace(object.cullFace);
		pContext->PolygonMode(GL_FRONT_AND_BACK, GL_FILL);	// 塗りつぶしタイプは固定でええやろ
		pContext->PolygonOffset(object.slopeScaledDepthBias, object.depthBias);
	}

	// ブレンドステート設定
	{
		const CBlendState& state = CGfxUtility::GetDefaultBlendState();
		const BLEND_STATE_OBJECT& object = state.GetObject();
		//f32 blendFactor[] = { 1.0f, 1.0f, 1.0f, 1.0f };	// !!!! ブレンド係数を外部より指定させる必要は？
		//u32 sampleMask = 0xffffffff;					// !!!! サンプル マスクの指定は必要ない？
		if (!object.isIndependent)
		{
			const BLEND_STATE_OBJECT::TRG_STATE& state = object.state[0];
			if (state.enable)
			{
				pContext->Enable(GL_BLEND);
				pContext->BlendEquationSeparate(state.colorOp, state.alphaOp);
				pContext->BlendFuncSeparate(
					state.colorSrcFunc, state.colorDstFunc,
					state.alphaSrcFunc, state.alphaDstFunc
					);
				pContext->ColorMask(
					state.colorMask[0],
					state.colorMask[1],
					state.colorMask[2],
					state.colorMask[3]
					);
			}
			else
			{
				pContext->Disable(GL_BLEND);
			}
		}
		else
		{
			for (u32 trgSlot = 0; trgSlot < RENDER_BUFFER_SLOT_NUM; trgSlot++)
			{
				const BLEND_STATE_OBJECT::TRG_STATE& state = object.state[trgSlot];
				if (state.enable)
				{
					pContext->Enablei(GL_BLEND, trgSlot);
					pContext->BlendEquationSeparatei(trgSlot, state.colorOp, state.alphaOp);
					pContext->BlendFuncSeparatei(
						trgSlot,
						state.colorSrcFunc, state.colorDstFunc,
						state.alphaSrcFunc, state.alphaDstFunc
						);
					pContext->ColorMaski(
						trgSlot,
						state.colorMask[0],
						state.colorMask[1],
						state.colorMask[2],
						state.colorMask[3]
						);
				}
				else
				{
					pContext->Disablei(GL_BLEND, trgSlot);
				}
			}
		}
	}

	// デプスステンシルステート設定
	{
		const CDepthState& state = CGfxUtility::GetDefaultDepthState();
		const DEPTH_STATE_OBJECT& object = state.GetObject();
		u32 stencilRef = 0x0;					// !!!! サンプル ステンシル参照値を設定させるようにしなきゃなぁ
		if (object.isDepthTest)
		{
			pContext->Enable(GL_DEPTH_TEST);
			pContext->DepthMask(object.isDepthWrite);
			pContext->DepthFunc(object.depthTest);
		}
		else
		{
			pContext->Disable(GL_DEPTH_TEST);
		}

		if (object.isStencilTest)
		{
			pContext->Enable(GL_STENCIL_TEST);
			pContext->StencilMaskSeparate(GL_FRONT_AND_BACK, object.stencilWriteMask);
			pContext->StencilFuncSeparate(GL_FRONT_AND_BACK, object.stencilTest, stencilRef, object.stencilReadMask);
			pContext->StencilOpSeparate(GL_FRONT_AND_BACK, object.stencilFailOp, object.stencilZFailOp, object.stencilPassOp);
		}
		else
		{
			pContext->Disable(GL_STENCIL_TEST);
		}
	}

	// 描画
	{
		u32 glPrim;
		CGfxFormatUtilityGL::ConvertPrimitive(glPrim, GFX_PRIMITIVE_QUADS);
		pContext->DrawArrays(glPrim, 0, 4);
	}
}

///-------------------------------------------------------------------------------------------------
///	垂直同期
b8		CDeviceGL::WaitVsync(u32 _syncInterval)
{
	b8 bRet = true;
	bRet &= (::wglSwapLayerBuffers(m_hDc, WGL_SWAP_MAIN_PLANE) == TRUE);
	bRet &= (::wglSwapIntervalEXT(_syncInterval) == TRUE);
	return bRet;
}

///-------------------------------------------------------------------------------------------------
///	ターゲットのリサイズ
b8		CDeviceGL::ResizeTarget(u32 _width, u32 _height)
{
	// 変更なし
	if (GetWidth() == _width && GetHeight() == _height) { return true; }
	// 勝手にウィンドウサイズに変更されるのでこのまま
	m_width = _width;
	m_height = _height;
	return true;
}


///-------------------------------------------------------------------------------------------------
///	OpenGLの初期化
b8		CDeviceGL::InitOpenGL()
{
	// グラフィックスコンテキストを作成
	m_hGlrc = ::wglCreateContext(m_hDc);

	// 作成されたコンテキストがカレント（現在使用中のコンテキスト）か？
	if (!::wglMakeCurrent(m_hDc, m_hGlrc))
	{
		// 何か正しくないみたい…
		PRINT("************[Error] Failed : ::wglMakeCurrent()\n");
		return false;
	}

	// openGL関数群の初期化
	if (CGfxUtilityGL::LoadProc() == false)
	{
		PRINT("************[Error] Failed : ::glewInit()\n");
		return false;
	}

	// バージョンチェック
	PRINT("----------------------------------------------------------------------------\n");
	PRINT("Vendor : %s\n", ::glGetString(GL_VENDOR));
	PRINT("GPU : %s\n", ::glGetString(GL_RENDERER));
	PRINT("OpenGL ver. %s\n", ::glGetString(GL_VERSION));
	PRINT("GLSL ver. %s\n", ::glGetString(GL_SHADING_LANGUAGE_VERSION));
	//PRINT("Extension List : %s\n", glGetString(GL_EXTENSIONS));
	PRINT("----------------------------------------------------------------------------\n");
	PRINT("\n");

#ifndef POISON_RELEASE
	// デバッグコールバック設定
	CGfxUtilityGL::SetupDebugCallback();
#endif	// !POISON_RELEASE


	// プラットフォーム互換API
	// 本当はGL_UPPER_LEFT使いたかったけど挙動謎過ぎるのでスルーで
	//::glClipControl(GL_UPPER_LEFT, GL_ZERO_TO_ONE);
	::glClipControl(GL_LOWER_LEFT, GL_ZERO_TO_ONE);

	return (m_hGlrc != NULL);
}

///-------------------------------------------------------------------------------------------------
///	デバイスコンテキストプッシュ
void	CDeviceGL::PushDevice()
{
	if (m_push_hDc == NULL)
	{
		// デバイスプッシュ
		m_push_hDc = ::GetDC(m_hWnd);
		libAssert(m_push_hDc);
		// デバイスコンテキストにグラフィックスコンテキスト設定
		::wglMakeCurrent(m_push_hDc, m_hGlrc);
	}
}

///-------------------------------------------------------------------------------------------------
///	デバイスコンテキストポップ
void	CDeviceGL::PopDevice()
{
	if (m_push_hDc)
	{
		// デバイスポップ
		::ReleaseDC(m_hWnd, m_push_hDc);
		m_push_hDc = NULL;

		// グラフィックスコンテキストを戻す
		::wglMakeCurrent(m_hDc, m_hGlrc);
	}
}


POISON_END

#endif // LIB_GFX_OPENGL

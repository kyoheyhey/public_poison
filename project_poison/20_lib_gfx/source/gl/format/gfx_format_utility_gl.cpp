﻿
#ifdef LIB_GFX_OPENGL

///-------------------------------------------------------------------------------------------------
/// define



///-------------------------------------------------------------------------------------------------
/// include
#include "format/gfx_format_utility.h"
#include "gl/format/gfx_format_utility_gl.h"
#include "shader/shader.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


//-------------------------------------------------------------------------------------------------
//@brief	論理型変換
void	CGfxFormatUtilityGL::ConvertBool(u32& _retBool, b8 _bool)
{
	_retBool = _bool ? GL_TRUE : GL_FALSE;
}


//-------------------------------------------------------------------------------------------------
// データアクセス変換
void	CGfxFormatUtilityGL::ConvertUsage(u32& _retUsage, GFX_USAGE _usage)
{
	static const u32 s_usage[] =
	{
		GL_READ_WRITE,
		GL_STATIC_DRAW,
		GL_DYNAMIC_DRAW,
		GL_STREAM_COPY,		// 何がいいのかさっぱりわからん
	};
	StaticAssert(ARRAYOF(s_usage) == GFX_USAGE_TYPE_NUM);
	_retUsage = s_usage[_usage];
}


///-------------------------------------------------------------------------------------------------
/// バッファ属性変換
void	CGfxFormatUtilityGL::ConvertBufferAttr(u32& _retAttr, GFX_BUFFER_ATTR _attr)
{
	static const u32 s_attr[] =
	{
		GL_UNIFORM_BUFFER,
		GL_TEXTURE_BUFFER,
		GL_SHADER_STORAGE_BUFFER,
		//GL_TRANSFORM_FEEDBACK_BUFFER,
		//GL_ATOMIC_COUNTER_BUFFER,
		//GL_DRAW_INDIRECT_BUFFER,
		//GL_DISPATCH_INDIRECT_BUFFER,
	};
	StaticAssert(ARRAYOF(s_attr) == GFX_BUFFER_ATTR_TYPE_NUM);
	_retAttr = s_attr[_attr];
}

///-------------------------------------------------------------------------------------------------
/// 値タイプ変換
void	CGfxFormatUtilityGL::ConvertValueType(u32& _retValType, GFX_VALUE _valType)
{
	static const u32 s_valueType[] =
	{
		GL_NONE,
		GL_BYTE,
		GL_UNSIGNED_BYTE,
		GL_SHORT,
		GL_UNSIGNED_SHORT,
		GL_INT,
		GL_UNSIGNED_INT,
		GL_FLOAT,
		GL_INT,				// 対応してない
		GL_UNSIGNED_INT,	// 対応してない
		GL_DOUBLE,
	};
	StaticAssert(ARRAYOF(s_valueType) == GFX_VALUE_TYPE_NUM);
	_retValType = s_valueType[_valType];
}


//-------------------------------------------------------------------------------------------------
// フォーマット変換
void	CGfxFormatUtilityGL::ConvertFormat(u32& _retFormat, GFX_IMAGE_FORMAT _format)
{
	static const u32 s_format[] =
	{
		GL_NONE,
		GL_RGBA32UI,
		GL_RGBA32F,
		GL_RGBA32UI,
		GL_RGBA32I,
		GL_RGB32UI,
		GL_RGB32F,
		GL_RGB32UI,
		GL_RGB32I,
		GL_RGBA16,
		GL_RGBA16F,
		GL_RGBA16,
		GL_RGBA16UI,
		GL_RGBA16_SNORM,
		GL_RGBA16I,
		GL_RG32UI,
		GL_RG32F,
		GL_RG32UI,
		GL_RG32I,
		GL_DEPTH32F_STENCIL8,	// デプス以外でねぇ
		GL_DEPTH32F_STENCIL8,	// 合ってるか微妙
		GL_DEPTH32F_STENCIL8,	// デプス以外でねぇ
		GL_DEPTH32F_STENCIL8,	// デプス以外でねぇ
		GL_RGB10_A2,
		GL_RGB10_A2,
		GL_RGB10_A2UI,
		GL_R11F_G11F_B10F,
		GL_RGBA8,
		GL_RGBA8,
		GL_SRGB8_ALPHA8,
		GL_RGBA8UI,
		GL_RGBA8_SNORM,
		GL_RGBA8I,
		GL_RG16,
		GL_RG16F,
		GL_RG16,
		GL_RG16UI,
		GL_RG16_SNORM,
		GL_RG16I,
		GL_R32UI,
		GL_DEPTH_COMPONENT32,
		GL_R32F,
		GL_R32UI,
		GL_R32I,
		GL_DEPTH24_STENCIL8,	// デプス以外でねぇ
		GL_DEPTH24_STENCIL8,
		GL_DEPTH24_STENCIL8,	// デプス以外でねぇ
		GL_DEPTH24_STENCIL8,	// デプス以外でねぇ
		GL_RG8,
		GL_RG8,
		GL_RG8UI,
		GL_RG8_SNORM,
		GL_RG8I,
		GL_R16,
		GL_R16F,
		GL_DEPTH_COMPONENT16,
		GL_R16UI,
		GL_R16,
		GL_R16_SNORM,
		GL_R16I,
		GL_R8,
		GL_R8,
		GL_R8UI,
		GL_R8_SNORM,
		GL_R8I,
		GL_ALPHA8,
	};
	StaticAssert(ARRAYOF(s_format) == GFX_FORMAT_TYPE_NUM);
	_retFormat = s_format[_format];
}

void	CGfxFormatUtilityGL::ConvertFormat(GFX_IMAGE_FORMAT& _retFormat, u32 _format)
{
	switch (_format)
	{
	case GL_NONE:
		_retFormat = GFX_FORMAT_UNKNOWN;
		return;
	case GL_RGBA32UI:
		_retFormat = GFX_FORMAT_R32G32B32A32_UINT;
		return;
	case GL_RGBA32I:
		_retFormat = GFX_FORMAT_R32G32B32A32_SINT;
		return;
	case GL_RGBA32F:
		_retFormat = GFX_FORMAT_R32G32B32A32_FLOAT;
		return;
	case GL_RGB32UI:
		_retFormat = GFX_FORMAT_R32G32B32_UINT;
		return;
	case GL_RGB32I:
		_retFormat = GFX_FORMAT_R32G32B32_SINT;
		return;
	case GL_RGB32F:
		_retFormat = GFX_FORMAT_R32G32B32_FLOAT;
		return;
	case GL_RGBA16:
		_retFormat = GFX_FORMAT_R16G16B16A16_UNORM;
		return;
	case GL_RGBA16_SNORM:
		_retFormat = GFX_FORMAT_R16G16B16A16_SNORM;
		return;
	case GL_RGBA16UI:
		_retFormat = GFX_FORMAT_R16G16B16A16_UINT;
		return;
	case GL_RGBA16I:
		_retFormat = GFX_FORMAT_R16G16B16A16_SINT;
		return;
	case GL_RGBA16F:
		_retFormat = GFX_FORMAT_R16G16B16A16_FLOAT;
		return;
	case GL_RG32UI:
		_retFormat = GFX_FORMAT_R32G32_UINT;
		return;
	case GL_RG32I:
		_retFormat = GFX_FORMAT_R32G32_SINT;
		return;
	case GL_RG32F:
		_retFormat = GFX_FORMAT_R32G32_FLOAT;
		return;
	case GL_DEPTH32F_STENCIL8:
		_retFormat = GFX_FORMAT_D32_FLOAT_S8X24_UINT;
		return;
	case GL_RGB10_A2:
		_retFormat = GFX_FORMAT_R10G10B10A2_UNORM;
		return;
	case GL_RGB10_A2UI:
		_retFormat = GFX_FORMAT_R10G10B10A2_UINT;
		return;
	case GL_R11F_G11F_B10F:
		_retFormat = GFX_FORMAT_R11G11B10_FLOAT;
		return;
	case GL_RGBA8:
		_retFormat = GFX_FORMAT_R8G8B8A8_UNORM;
		return;
	case GL_SRGB8_ALPHA8:
		_retFormat = GFX_FORMAT_R8G8B8A8_UNORM_SRGB;
		return;
	case GL_RGBA8_SNORM:
		_retFormat = GFX_FORMAT_R8G8B8A8_SNORM;
		return;
	case GL_RGBA8UI:
		_retFormat = GFX_FORMAT_R8G8B8A8_UINT;
		return;
	case GL_RGBA8I:
		_retFormat = GFX_FORMAT_R8G8B8A8_SINT;
		return;
	case GL_RG16:
		_retFormat = GFX_FORMAT_R16G16_UNORM;
		return;
	case GL_RG16_SNORM:
		_retFormat = GFX_FORMAT_R16G16_SNORM;
		return;
	case GL_RG16UI:
		_retFormat = GFX_FORMAT_R16G16_UINT;
		return;
	case GL_RG16I:
		_retFormat = GFX_FORMAT_R16G16_SINT;
		return;
	case GL_RG16F:
		_retFormat = GFX_FORMAT_R16G16_FLOAT;
		return;
	case GL_R32UI:
		_retFormat = GFX_FORMAT_R32_UINT;
		return;
	case GL_R32I:
		_retFormat = GFX_FORMAT_R32_SINT;
		return;
	case GL_R32F:
		_retFormat = GFX_FORMAT_R32_FLOAT;
		return;
	case GL_DEPTH_COMPONENT32:
		_retFormat = GFX_FORMAT_D32_FLOAT;
		return;
	case GL_DEPTH24_STENCIL8:
		_retFormat = GFX_FORMAT_D24_UNORM_S8_UINT;
		return;
	case GL_RG8:
		_retFormat = GFX_FORMAT_R8G8_UNORM;
		return;
	case GL_RG8_SNORM:
		_retFormat = GFX_FORMAT_R8G8_SNORM;
		return;
	case GL_RG8UI:
		_retFormat = GFX_FORMAT_R8G8_UINT;
		return;
	case GL_RG8I:
		_retFormat = GFX_FORMAT_R8G8_SINT;
		return;
	case GL_R16:
		_retFormat = GFX_FORMAT_R16_UNORM;
		return;
	case GL_R16_SNORM:
		_retFormat = GFX_FORMAT_R16_SNORM;
		return;
	case GL_R16UI:
		_retFormat = GFX_FORMAT_R16_UINT;
		return;
	case GL_R16I:
		_retFormat = GFX_FORMAT_R16_SINT;
		return;
	case GL_R16F:
		_retFormat = GFX_FORMAT_R16_FLOAT;
		return;
	case GL_DEPTH_COMPONENT16:
		_retFormat = GFX_FORMAT_D16_UNORM;
		return;
	case GL_R8:
		_retFormat = GFX_FORMAT_R8_UNORM;
		return;
	case GL_R8_SNORM:
		_retFormat = GFX_FORMAT_R8_SNORM;
		return;
	case GL_R8UI:
		_retFormat = GFX_FORMAT_R8_UINT;
		return;
	case GL_R8I:
		_retFormat = GFX_FORMAT_R8_SINT;
		return;
	default:
		_retFormat = GFX_FORMAT_UNKNOWN;
		return;
	}
}


///-------------------------------------------------------------------------------------------------
/// フォーマットからテクスチャフォーマット変換
void	CGfxFormatUtilityGL::ConvertFormatToTexFormat(u32& _retTexFormat, GFX_IMAGE_FORMAT _format)
{
	switch (_format)
	{
	case GFX_FORMAT_R32G32B32A32_TYPELESS:
	case GFX_FORMAT_R32G32B32A32_FLOAT:
	case GFX_FORMAT_R32G32B32A32_UINT:
	case GFX_FORMAT_R32G32B32A32_SINT:
	case GFX_FORMAT_R16G16B16A16_TYPELESS:
	case GFX_FORMAT_R16G16B16A16_FLOAT:
	case GFX_FORMAT_R16G16B16A16_UNORM:
	case GFX_FORMAT_R16G16B16A16_UINT:
	case GFX_FORMAT_R16G16B16A16_SNORM:
	case GFX_FORMAT_R16G16B16A16_SINT:
	case GFX_FORMAT_R10G10B10A2_TYPELESS:
	case GFX_FORMAT_R10G10B10A2_UNORM:
	case GFX_FORMAT_R10G10B10A2_UINT:
	case GFX_FORMAT_R8G8B8A8_TYPELESS:
	case GFX_FORMAT_R8G8B8A8_UNORM:
	case GFX_FORMAT_R8G8B8A8_UNORM_SRGB:
	case GFX_FORMAT_R8G8B8A8_UINT:
	case GFX_FORMAT_R8G8B8A8_SNORM:
	case GFX_FORMAT_R8G8B8A8_SINT:
		_retTexFormat = GL_RGBA;
		break;
	case GFX_FORMAT_R32G32B32_TYPELESS:
	case GFX_FORMAT_R32G32B32_FLOAT:
	case GFX_FORMAT_R32G32B32_UINT:
	case GFX_FORMAT_R32G32B32_SINT:
	case GFX_FORMAT_R11G11B10_FLOAT:
		_retTexFormat = GL_RGB;
		break;
	case GFX_FORMAT_R32G32_TYPELESS:
	case GFX_FORMAT_R32G32_FLOAT:
	case GFX_FORMAT_R32G32_UINT:
	case GFX_FORMAT_R32G32_SINT:
	case GFX_FORMAT_R16G16_TYPELESS:
	case GFX_FORMAT_R16G16_FLOAT:
	case GFX_FORMAT_R16G16_UNORM:
	case GFX_FORMAT_R16G16_UINT:
	case GFX_FORMAT_R16G16_SNORM:
	case GFX_FORMAT_R16G16_SINT:
	case GFX_FORMAT_R8G8_TYPELESS:
	case GFX_FORMAT_R8G8_UNORM:
	case GFX_FORMAT_R8G8_UINT:
	case GFX_FORMAT_R8G8_SNORM:
	case GFX_FORMAT_R8G8_SINT:
		_retTexFormat = GL_RG;
		break;
	case GFX_FORMAT_R32_TYPELESS:
	case GFX_FORMAT_R32_FLOAT:
	case GFX_FORMAT_R32_UINT:
	case GFX_FORMAT_R32_SINT:
	case GFX_FORMAT_R16_TYPELESS:
	case GFX_FORMAT_R16_FLOAT:
	case GFX_FORMAT_R16_UNORM:
	case GFX_FORMAT_R16_UINT:
	case GFX_FORMAT_R16_SNORM:
	case GFX_FORMAT_R16_SINT:
	case GFX_FORMAT_R8_TYPELESS:
	case GFX_FORMAT_R8_UNORM:
	case GFX_FORMAT_R8_UINT:
	case GFX_FORMAT_R8_SNORM:
	case GFX_FORMAT_R8_SINT:
		_retTexFormat = GL_R;
		break;
	case GFX_FORMAT_R32G8X24_TYPELESS:
	case GFX_FORMAT_D32_FLOAT_S8X24_UINT:
	case GFX_FORMAT_R32_FLOAT_X8X24_TYPELESS:
	case GFX_FORMAT_X32_TYPELESS_G8X24_UINT:
	case GFX_FORMAT_R24G8_TYPELESS:
	case GFX_FORMAT_D24_UNORM_S8_UINT:
	case GFX_FORMAT_R24_UNORM_G8_TYPELESS:
	case GFX_FORMAT_X24_TYPELESS_G8_UINT:
		_retTexFormat = GL_DEPTH_STENCIL;
		break;
	case GFX_FORMAT_D32_FLOAT:
	case GFX_FORMAT_D16_UNORM:
		_retTexFormat = GL_DEPTH_COMPONENT;
		break;
	case GFX_FORMAT_A8_UNORM:
		_retTexFormat = GL_ALPHA;
		break;
	default:
		_retTexFormat = GL_NONE;
		break;
	}
}


///-------------------------------------------------------------------------------------------------
/// フォーマットから値タイプ変換
void	CGfxFormatUtilityGL::ConvertFormatToValueType(u32& _retValType, GFX_IMAGE_FORMAT _format)
{
	switch (_format)
	{
	case GFX_FORMAT_R32G32B32A32_TYPELESS:
	case GFX_FORMAT_R32G32B32_TYPELESS:
	case GFX_FORMAT_R32G32_TYPELESS:
	case GFX_FORMAT_R32_TYPELESS:
		_retValType = GL_UNSIGNED_INT;	// これでいいのかわからん
		break;
	case GFX_FORMAT_R32G32B32A32_FLOAT:
	case GFX_FORMAT_R32G32B32_FLOAT:
	case GFX_FORMAT_R32G32_FLOAT:
	case GFX_FORMAT_D32_FLOAT:
	case GFX_FORMAT_R32_FLOAT:
	case GFX_FORMAT_R11G11B10_FLOAT:
		_retValType = GL_FLOAT;
		break;
	case GFX_FORMAT_R32G32B32A32_UINT:
	case GFX_FORMAT_R32G32B32_UINT:
	case GFX_FORMAT_R32G32_UINT:
	case GFX_FORMAT_R32_UINT:
		_retValType = GL_UNSIGNED_INT;
		break;
	case GFX_FORMAT_R32G32B32A32_SINT:
	case GFX_FORMAT_R32G32B32_SINT:
	case GFX_FORMAT_R32G32_SINT:
	case GFX_FORMAT_R32_SINT:
		_retValType = GL_INT;
		break;
	case GFX_FORMAT_R16G16B16A16_TYPELESS:
	case GFX_FORMAT_R16G16_TYPELESS:
	case GFX_FORMAT_R16_TYPELESS:
		_retValType = GL_UNSIGNED_SHORT;	// これでいいのかわからん
		break;
	case GFX_FORMAT_R16G16B16A16_FLOAT:
	case GFX_FORMAT_R16G16_FLOAT:
	case GFX_FORMAT_R16_FLOAT:
		_retValType = GL_HALF_FLOAT;
		break;
	case GFX_FORMAT_R16G16B16A16_UNORM:
	case GFX_FORMAT_R16G16_UNORM:
	case GFX_FORMAT_D16_UNORM:
	case GFX_FORMAT_R16_UNORM:
	case GFX_FORMAT_R16G16B16A16_UINT:
	case GFX_FORMAT_R16G16_UINT:
	case GFX_FORMAT_R16_UINT:
		_retValType = GL_UNSIGNED_SHORT;
		break;
	case GFX_FORMAT_R16G16B16A16_SNORM:
	case GFX_FORMAT_R16G16_SNORM:
	case GFX_FORMAT_R16_SNORM:
	case GFX_FORMAT_R16G16B16A16_SINT:
	case GFX_FORMAT_R16G16_SINT:
	case GFX_FORMAT_R16_SINT:
		_retValType = GL_SHORT;
		break;
	case GFX_FORMAT_R32G8X24_TYPELESS:
	case GFX_FORMAT_D32_FLOAT_S8X24_UINT:
	case GFX_FORMAT_R32_FLOAT_X8X24_TYPELESS:
	case GFX_FORMAT_X32_TYPELESS_G8X24_UINT:
		_retValType = GL_FLOAT_32_UNSIGNED_INT_24_8_REV;
		break;
	case GFX_FORMAT_R10G10B10A2_TYPELESS:
	case GFX_FORMAT_R10G10B10A2_UNORM:
	case GFX_FORMAT_R10G10B10A2_UINT:
		_retValType = GL_UNSIGNED_INT_10_10_10_2;
		break;
	case GFX_FORMAT_R8G8B8A8_TYPELESS:
	case GFX_FORMAT_R8G8_TYPELESS:
	case GFX_FORMAT_R8_TYPELESS:
		_retValType = GL_UNSIGNED_BYTE;
		break;
	case GFX_FORMAT_R8G8B8A8_UNORM:
	case GFX_FORMAT_R8G8B8A8_UNORM_SRGB:
	case GFX_FORMAT_R8G8_UNORM:
	case GFX_FORMAT_R8_UNORM:
	case GFX_FORMAT_A8_UNORM:
	case GFX_FORMAT_R8G8B8A8_UINT:
	case GFX_FORMAT_R8G8_UINT:
	case GFX_FORMAT_R8_UINT:
		_retValType = GL_UNSIGNED_BYTE;
		break;
	case GFX_FORMAT_R8G8B8A8_SNORM:
	case GFX_FORMAT_R8G8_SNORM:
	case GFX_FORMAT_R8_SNORM:
	case GFX_FORMAT_R8G8B8A8_SINT:
	case GFX_FORMAT_R8G8_SINT:
	case GFX_FORMAT_R8_SINT:
		_retValType = GL_BYTE;
		break;
	case GFX_FORMAT_R24G8_TYPELESS:
	case GFX_FORMAT_D24_UNORM_S8_UINT:
	case GFX_FORMAT_R24_UNORM_G8_TYPELESS:
	case GFX_FORMAT_X24_TYPELESS_G8_UINT:
		_retValType = GL_UNSIGNED_INT_24_8;
		break;
	default:
		_retValType = GL_NONE;
		break;
	}
}


///-------------------------------------------------------------------------------------------------
/// テクスチャタイプ変換
void	CGfxFormatUtilityGL::ConvertTextureType(u32& _retTexType, GFX_TEXTURE_TYPE _texType)
{
	static const u32 s_texType[] =
	{
		GL_NONE,
		GL_TEXTURE_1D,
		GL_TEXTURE_2D,
		GL_TEXTURE_3D,
		GL_TEXTURE_CUBE_MAP,
		GL_TEXTURE_2D_ARRAY,
		GL_TEXTURE_CUBE_MAP_ARRAY,
	};
	StaticAssert(ARRAYOF(s_texType) == GFX_TEXTURE_RES_TYPE_NUM);
	_retTexType = s_texType[_texType];
}

///-------------------------------------------------------------------------------------------------
/// キューブマップ面変換
void	CGfxFormatUtilityGL::ConvertCubemapFace(u32& _retCubemapFace, GFX_CUBEMAP_FACE _cubemapFace)
{
	static const u32 s_cubemapFace[] = 
	{
		GL_TEXTURE_CUBE_MAP_POSITIVE_X,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
	};
	StaticAssert(ARRAYOF(s_cubemapFace) == GFX_CUBEMAP_FACE_TYPE_NUM);
	_retCubemapFace = s_cubemapFace[_cubemapFace];
}


//-------------------------------------------------------------------------------------------------
// サンプラーラップ変換
void	CGfxFormatUtilityGL::ConvertSamplerWrap(u32& _retWrap, GFX_SAMPLER_WRAP _wrap)
{
	static const u32 s_wrap[] =
	{
		GL_NONE,
		GL_CLAMP_TO_EDGE,
		GL_REPEAT,
		GL_MIRRORED_REPEAT,
		GL_MIRROR_CLAMP_TO_EDGE,
		GL_CLAMP_TO_BORDER,
	};
	StaticAssert(ARRAYOF(s_wrap) == GFX_SAMPLER_WRAP_TYPE_NUM);
	_retWrap = s_wrap[_wrap];
}

//-------------------------------------------------------------------------------------------------
// サンプラーフィルター変換
void	CGfxFormatUtilityGL::ConvertSamplerFilter(u32& _retMinFilter, u32& _retMaxFilter, GFX_SAMPLER_FILTER _min, GFX_SAMPLER_FILTER _mag, GFX_SAMPLER_FILTER _mip)
{
	static const u32 s_filter[] =
	{
		GL_NEAREST_MIPMAP_NEAREST,
		GL_NEAREST_MIPMAP_LINEAR,
		GL_LINEAR_MIPMAP_NEAREST,
		GL_LINEAR_MIPMAP_LINEAR,
	};
	{
		u32 idx = 0;
		idx += _min == GFX_SAMPLER_FILTER_LINEAR ? 2 : 0;
		idx += _mip == GFX_SAMPLER_FILTER_LINEAR ? 1 : 0;
		_retMinFilter = s_filter[idx];
	}
	{
		u32 idx = 0;
		idx += _mag == GFX_SAMPLER_FILTER_LINEAR ? 2 : 0;
		idx += _mip == GFX_SAMPLER_FILTER_LINEAR ? 1 : 0;
		_retMaxFilter = s_filter[idx];
	}
}


//-------------------------------------------------------------------------------------------------
// プリミティブ変換
void	CGfxFormatUtilityGL::ConvertPrimitive(u32& _retPrim, GFX_PRIMITIVE _prim)
{
	static const u32 s_prim[] =
	{
		GL_NONE,

		GL_POINTS,

		GL_LINES,
		GL_LINE_LOOP,
		GL_LINE_STRIP,

		GL_TRIANGLES,
		GL_TRIANGLE_FAN,
		GL_TRIANGLE_STRIP,

		//GL_TRIANGLE_STRIP,
		//GL_TRIANGLE_STRIP,
		GL_QUADS,
		GL_QUAD_STRIP,

		GL_LINES_ADJACENCY,
		GL_LINE_STRIP_ADJACENCY,

		GL_TRIANGLES_ADJACENCY,
		GL_TRIANGLE_STRIP_ADJACENCY,
	};
	StaticAssert(ARRAYOF(s_prim) == GFX_PRIMITIVE_TYPE_NUM);
	_retPrim = s_prim[_prim];
}


//-------------------------------------------------------------------------------------------------
// 前面変換
void	CGfxFormatUtilityGL::ConvertFrontFace(u32& _retFace, GFX_FRONT_FACE _face)
{
	static const u32 s_frontFace[] =
	{
		GL_CCW,
		GL_CW,
	};
	StaticAssert(ARRAYOF(s_frontFace) == GFX_FRONT_FACE_TYPE_NUM);
	_retFace = s_frontFace[_face];
}

//-------------------------------------------------------------------------------------------------
// カリング面変換
void	CGfxFormatUtilityGL::ConvertCullFace(u32& _retFace, GFX_CULL_FACE _face)
{
	static const u32 s_cullFace[] =
	{
		GL_NONE,//GL_FRONT_AND_BACK,
		GL_FRONT,
		GL_BACK,
	};
	StaticAssert(ARRAYOF(s_cullFace) == GFX_CULL_FACE_TYPE_NUM);
	_retFace = s_cullFace[_face];
}


//-------------------------------------------------------------------------------------------------
// 比較式変換
void	CGfxFormatUtilityGL::ConvertTestFunc(u32& _retTest, GFX_TEST _test)
{
	static const u32 s_test[] =
	{
		GL_NONE,
		GL_NEVER,
		GL_LESS,
		GL_EQUAL,
		GL_LEQUAL,
		GL_GREATER,
		GL_GEQUAL,
		GL_NOTEQUAL,
		GL_ALWAYS,
	};
	StaticAssert(ARRAYOF(s_test) == GFX_TEST_TYPE_NUM);
	_retTest = s_test[_test];
}


//-------------------------------------------------------------------------------------------------
// ブレンドオペレーション変換
void	CGfxFormatUtilityGL::ConvertBlendOp(u32& _retBlendOp, GFX_BLEND_OP _blendOp)
{
	static const u32 s_blendOp[] =
	{
		GL_NONE,
		GL_FUNC_ADD,
		GL_FUNC_SUBTRACT,
		GL_FUNC_REVERSE_SUBTRACT,
		GL_MIN,
		GL_MAX,
	};
	StaticAssert(ARRAYOF(s_blendOp) == GFX_BLEND_OP_TYPE_NUM);
	_retBlendOp = s_blendOp[_blendOp];
}


//-------------------------------------------------------------------------------------------------
// ブレンド式変換
void	CGfxFormatUtilityGL::ConvertBlendFunc(u32& _retBlendFunc, GFX_BLEND_FUNC _blendFunc)
{
	static const u32 s_blendFunc[] =
	{
		GL_NONE,
		GL_ZERO,
		GL_ONE,
		GL_SRC_COLOR,
		GL_ONE_MINUS_SRC_COLOR,
		GL_SRC_ALPHA,
		GL_ONE_MINUS_SRC_ALPHA,
		GL_DST_COLOR,
		GL_ONE_MINUS_DST_COLOR,
		GL_DST_ALPHA,
		GL_ONE_MINUS_DST_ALPHA,

		GL_SRC_ALPHA_SATURATE,

		GL_CONSTANT_COLOR,
		GL_ONE_MINUS_CONSTANT_COLOR,
		GL_CONSTANT_ALPHA,
		GL_ONE_MINUS_CONSTANT_ALPHA,

		GL_SRC1_COLOR,
		GL_ONE_MINUS_SRC1_COLOR,
		GL_SRC1_ALPHA,
		GL_ONE_MINUS_SRC1_ALPHA,
	};
	StaticAssert(ARRAYOF(s_blendFunc) == GFX_BLEND_FUNC_TYPE_NUM);
	_retBlendFunc = s_blendFunc[_blendFunc];
}


//-------------------------------------------------------------------------------------------------
// カラーマスク変換
void	CGfxFormatUtilityGL::ConvertColorMask(b8(&_rgba)[4], GFX_COLOR_MASK _colorMask)
{
	memset(_rgba, 0, sizeof(_rgba));
	u32 idx = 0;
	u32 bit = _colorMask;
	while (bit)
	{
		if (bit & 0x01)
		{
			_rgba[idx] = true;
		}
		idx++;
		bit >>= 1;
	}
}


//-------------------------------------------------------------------------------------------------
// ステンシルオペレーション変換
void	CGfxFormatUtilityGL::ConvertStencilOp(u32& _retStencilOp, GFX_STENCIL_OP _stencilOp)
{
	static const u32 s_stencilOp[] =
	{
		GL_NONE,
		GL_KEEP,
		GL_ZERO,
		GL_REPLACE,
		GL_INCR,
		GL_DECR,
		GL_INCR_WRAP,
		GL_DECR_WRAP,
		GL_INVERT,
	};
	StaticAssert(ARRAYOF(s_stencilOp) == GFX_STENCIL_OP_TYPE_NUM);
	_retStencilOp = s_stencilOp[_stencilOp];
}


///-------------------------------------------------------------------------------------------------
/// クリアレンダーターゲットビット変換
void	CGfxFormatUtilityGL::ConvertClearRenderTargetBit(u32& _retClearRtBit, RENDER_BUFFER_BIT _rtBit)
{
	_retClearRtBit = 0;
	_retClearRtBit |= (_rtBit & RENDER_BUFFER_BIT_COLOR_ALL) ? GL_COLOR_BUFFER_BIT : 0;
	_retClearRtBit |= (_rtBit & RENDER_BUFFER_BIT_DEPTH) ? GL_DEPTH_BUFFER_BIT : 0;
	_retClearRtBit |= (_rtBit & RENDER_BUFFER_BIT_STENCIL) ? GL_STENCIL_BUFFER_BIT : 0;
}


///-------------------------------------------------------------------------------------------------
/// レンダーターゲットスロット変換
void		CGfxFormatUtilityGL::ConvertRenderTargetSlot(u32& _retRtSlot, u32 _rtSlot)
{
	static const u32 s_RTSlot[] =
	{
		GL_COLOR_ATTACHMENT0,
		GL_COLOR_ATTACHMENT1,
		GL_COLOR_ATTACHMENT2,
		GL_COLOR_ATTACHMENT3,
		GL_COLOR_ATTACHMENT4,
		GL_COLOR_ATTACHMENT5,
		GL_COLOR_ATTACHMENT6,
		GL_COLOR_ATTACHMENT7,
	};
	StaticAssert(ARRAYOF(s_RTSlot) == RENDER_BUFFER_SLOT_NUM);
	_retRtSlot = s_RTSlot[_rtSlot];
}


///-------------------------------------------------------------------------------------------------
/// テクスチャスロット変換
void		CGfxFormatUtilityGL::ConvertTextureSlot(u32& _retTexSlot, u32 _texSlot)
{
	static const u32 s_texSlot[] =
	{
		GL_TEXTURE0,
		GL_TEXTURE1,
		GL_TEXTURE2,
		GL_TEXTURE3,
		GL_TEXTURE4,
		GL_TEXTURE5,
		GL_TEXTURE6,
		GL_TEXTURE7,
		GL_TEXTURE8,
		GL_TEXTURE9,
		GL_TEXTURE10,
		GL_TEXTURE11,
		GL_TEXTURE12,
		GL_TEXTURE13,
		GL_TEXTURE14,
		GL_TEXTURE15,
	};
	StaticAssert(ARRAYOF(s_texSlot) == TEXTURE_SLOT_NUM);
	_retTexSlot = s_texSlot[_texSlot];
}


///-------------------------------------------------------------------------------------------------
/// シェーダタイプ変換
void		CGfxFormatUtilityGL::ConvertShaderType(u32& _retShaderType, GFX_SHADER _shaderType)
{
	static const u32 s_shaderType[] =
	{
		GL_VERTEX_SHADER,
		GL_GEOMETRY_SHADER,
		GL_FRAGMENT_SHADER,
		GL_COMPUTE_SHADER,
	};
	StaticAssert(ARRAYOF(s_shaderType) == GFX_SHADER_TYPE_NUM);
	_retShaderType = s_shaderType[_shaderType];
}


///-------------------------------------------------------------------------------------------------
/// 頂点シェーダロケーション名取得
const c8*	CGfxFormatUtilityGL::GetVertexLocationName(VERTEX_ATTRIBUTE _vtxAttr)
{
	static const c8* s_semanticseName[] =
	{
		"POSTION",
		"NORMAL",
		"TANGENT",
		"BINORMAL",
		"BLENDWEIGHT0",
		"BLENDWEIGHT1",
		"BLENDINDICES0",
		"BLENDINDICES1",
		"COLOR0",
		"COLOR1",
		"TEXCOORD0",
		"TEXCOORD1",
		"TEXCOORD2",
		"TEXCOORD3",
	};
	StaticAssert(ARRAYOF(s_semanticseName) == VERTEX_ATTRIBUTE_TYPE_NUM);
	return s_semanticseName[_vtxAttr];
}


///-------------------------------------------------------------------------------------------------
/// フラグメントシェーダロケーション名取得
const c8*	CGfxFormatUtilityGL::GetFragmentLocationName(RENDER_BUFFER_SLOT _outSlot)
{
	static const c8* s_fragmentLocationName[] =
	{
		"OUT_CLR0",
		"OUT_CLR1",
		"OUT_CLR2",
		"OUT_CLR3",
		"OUT_CLR4",
		"OUT_CLR5",
		"OUT_CLR6",
		"OUT_CLR7",
	};
	StaticAssert(ARRAYOF(s_fragmentLocationName) == RENDER_BUFFER_SLOT_NUM);
	return s_fragmentLocationName[_outSlot];
}


///-------------------------------------------------------------------------------------------------
/// シェーダユニフォームロケーション名取得
const c8*	CGfxFormatUtilityGL::GetUniformLocationName(UNIFORM_SLOT _uniSlot)
{
	static const c8* s_uniformLocationName[] =
	{
		"UNIFORM_SLOT_0",
		"UNIFORM_SLOT_1",
		"UNIFORM_SLOT_2",
		"UNIFORM_SLOT_3",
		"UNIFORM_SLOT_4",
		"UNIFORM_SLOT_5",
		"UNIFORM_SLOT_6",
		"UNIFORM_SLOT_7",
		"UNIFORM_SLOT_8",
		"UNIFORM_SLOT_9",
		"UNIFORM_SLOT_10",
		"UNIFORM_SLOT_11",
		"UNIFORM_SLOT_12",
		"UNIFORM_SLOT_13",
		"UNIFORM_SLOT_14",
		"UNIFORM_SLOT_15",
	};
	StaticAssert(ARRAYOF(s_uniformLocationName) == UNIFORM_SLOT_NUM);
	return s_uniformLocationName[_uniSlot];
}


///-------------------------------------------------------------------------------------------------
/// シェーダテクスチャロケーション名取得
const c8*	CGfxFormatUtilityGL::GetTextureLocationName(TEXTURE_SLOT _texSlot)
{
	static const c8* s_textureLocationName[] =
	{
		"TEXTURE_SLOT_0",
		"TEXTURE_SLOT_1",
		"TEXTURE_SLOT_2",
		"TEXTURE_SLOT_3",
		"TEXTURE_SLOT_4",
		"TEXTURE_SLOT_5",
		"TEXTURE_SLOT_6",
		"TEXTURE_SLOT_7",
		"TEXTURE_SLOT_8",
		"TEXTURE_SLOT_9",
		"TEXTURE_SLOT_10",
		"TEXTURE_SLOT_11",
		"TEXTURE_SLOT_12",
		"TEXTURE_SLOT_13",
		"TEXTURE_SLOT_14",
		"TEXTURE_SLOT_15",
	};
	StaticAssert(ARRAYOF(s_textureLocationName) == TEXTURE_SLOT_NUM);
	return s_textureLocationName[_texSlot];
}



POISON_END

#endif // LIB_GFX_OPENGL

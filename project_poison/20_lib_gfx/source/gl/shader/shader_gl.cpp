﻿
#ifdef LIB_GFX_OPENGL

///-------------------------------------------------------------------------------------------------
/// define
#define ENTRY_POINT_NAME	"main"
//#define USE_PIXEL_LOCATION_BIND
//#define USE_UNIFORM_LOCATION_BIND
//#define USE_TEXTURE_LOCATION_BIND

///-------------------------------------------------------------------------------------------------
/// include
#include "shader/shader.h"

#include "shader/shader_utility.h"

#include "format/gfx_format_utility.h"
#include "gl/format/gfx_format_utility_gl.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
// constans


///-------------------------------------------------------------------------------------------------
// struct
struct GL_SHADER_OBJECT
{
	u32	shaderID = 0;		///< シェーダID
	u32	programID = 0;		///< プログラムID
};


#ifndef POISON_RELEASE
///-------------------------------------------------------------------------------------------------
/// ログ出力
void	ShaderLogInfo(u32 _prgId)
{
	const u32 LOG_LENGHT = 1024;
	s32 bufSize;
	::glGetShaderiv(_prgId, GL_INFO_LOG_LENGTH, &bufSize);

	if (bufSize > 1 && bufSize <= LOG_LENGHT)
	{
		c8 logBuf[LOG_LENGHT];
		s32 length;
		::glGetShaderInfoLog(_prgId, bufSize, &length, logBuf);
		PRINT("Shader Info Log\n%s\n", logBuf);
	}
}

void	ProgramLogInfo(u32 _prgId)
{
	const u32 LOG_LENGHT = 1024;
	s32 bufSize;
	::glGetProgramiv(_prgId, GL_INFO_LOG_LENGTH, &bufSize);

	if (bufSize > 1 && bufSize <= LOG_LENGHT)
	{
		c8 logBuf[LOG_LENGHT];
		s32 length;
		::glGetProgramInfoLog(_prgId, bufSize, &length, logBuf);
		PRINT("Program Info Log\n%s\n", logBuf);
	}
}

void	PipelineLogInfo(u32 _prgId)
{
	const u32 LOG_LENGHT = 1024;
	s32 bufSize;
	::glGetProgramPipelineiv(_prgId, GL_INFO_LOG_LENGTH, &bufSize);

	if (bufSize > 1 && bufSize <= LOG_LENGHT)
	{
		c8 logBuf[LOG_LENGHT];
		s32 length;
		::glGetProgramPipelineInfoLog(_prgId, bufSize, &length, logBuf);
		PRINT("Pipeline Info Log\n%s\n", logBuf);
	}
}

///-------------------------------------------------------------------------------------------------
/// シェーダリフレクション
b8	ShaderReflection(GL_SHADER_OBJECT& _object, ShaderSlotCheckSum& _checkSum)
{
	// ユニフォームロケーションチェック
	_checkSum.uniformBlockMask = 0;
	for (u32 idx = 0; idx < UNIFORM_SLOT_NUM; idx++)
	{
		s32 blockIdx = -1;
		UNIFORM_SLOT slot = SCast<UNIFORM_SLOT>(idx);
		blockIdx = ::glGetUniformBlockIndex(_object.programID, CGfxFormatUtilityGL::GetUniformLocationName(slot));
		if (blockIdx < 0) { continue; }
		_checkSum.uniformBlockMask |= (1u << blockIdx);
	}

	// テクスチャロケーションチェック
	_checkSum.texAndRoBufMask = 0;
	for (u32 idx = 0; idx < TEXTURE_SLOT_NUM; idx++)
	{
		s32 texIdx = -1;
		TEXTURE_SLOT slot = SCast<TEXTURE_SLOT>(idx);
		texIdx = ::glGetAttribLocation(_object.programID, CGfxFormatUtilityGL::GetTextureLocationName(slot));
		if (texIdx < 0) { continue; }
		_checkSum.texAndRoBufMask |= (1u << texIdx);
	}
	return true;
}
#endif//!POISON_RELEASE

///-------------------------------------------------------------------------------------------------
///	セマンティクスロケーションバインド
b8		SemanticseLocationBind(u32 _prgId, b8 _isVS, b8 _isPS)
{
	// 頂点セマンティクスロケーションバインド（※リンクする前に行う必要あり）
	if (_isVS)
	{
		for (u32 idx = 0; idx < VERTEX_ATTRIBUTE_TYPE_NUM; idx++)
		{
			VERTEX_ATTRIBUTE slot = SCast<VERTEX_ATTRIBUTE>(idx);
			::glBindAttribLocation(
				_prgId,
				slot,
				CGfxFormatUtilityGL::GetVertexLocationName(slot)
				);
		}
	}
#ifdef USE_PIXEL_LOCATION_BIND
	// フラグメント出力ロケーションバインド
	if (_isPS)
	{
		for (u32 idx = 0; idx < RENDER_BUFFER_SLOT_NUM; idx++)
		{
			RENDER_BUFFER_SLOT slot = SCast<RENDER_BUFFER_SLOT>(idx);
			::glBindFragDataLocation(
				_prgId,
				idx,
				CGfxFormatUtilityGL::GetFragmentLocationName(slot)
				);
		}
	}
#endif // USE_PIXEL_LOCATION_BIND
	return true;
}

///-------------------------------------------------------------------------------------------------
///	バッファロケーションバインド
b8		BufferLocationBind(u32 _prgId)
{
#ifdef USE_UNIFORM_LOCATION_BIND
	// ユニフォームロケーションバインド
	//s32 size;
	//glGetProgramiv(m_object.programObject, GL_ACTIVE_UNIFORM_BLOCKS, &size);
	//if (size != _num){ return false; }
	for (u32 idx = 0; idx < UNIFORM_SLOT_NUM; idx++)
	{
		s32 blockIdx = -1;
		UNIFORM_SLOT slot = SCast<UNIFORM_SLOT>(idx);
		blockIdx = ::glGetUniformBlockIndex(_prgId, CGfxFormatUtilityGL::GetUniformLocationName(slot));
		if (blockIdx < 0) { continue; }
		//s32 blockSize = 0;
		//glGetActiveUniformBlockiv(
		//	m_object.programObject,
		//	blockIdx,
		//	GL_UNIFORM_BLOCK_DATA_SIZE,
		//	&blockSize
		//	);
		//if (blockSize != _layout.size){ continue; }
		::glUniformBlockBinding(_prgId, blockIdx, idx);
	}
#endif // USE_UNIFORM_LOCATION_BIND

#ifdef USE_TEXTURE_LOCATION_BIND
	// テクスチャロケーションバインド
	//s32 size;
	//glGetProgramiv(m_object.programObject, GL_MAX_TEXTURE_UNITS, &size);
	//if (size != _num){ return false; }
	for (u32 idx = 0; idx < TEXTURE_SLOT_NUM; idx++)
	{
		s32 texIdx = -1;
		TEXTURE_SLOT slot = SCast<TEXTURE_SLOT>(idx);
		texIdx = ::glGetAttribLocation(_prgId, CGfxFormatUtilityGL::GetTextureLocationName(slot));
		if (texIdx < 0) { continue; }
		::glUniform1i(texIdx, idx);
	}
#endif // USE_TEXTURE_LOCATION_BIND
	return true;
}

///-------------------------------------------------------------------------------------------------
///	GLSLシェーダコンパイル
b8		GLSLCompile(GL_SHADER_OBJECT& _object, const c8* _pSrc, u32 _size, GFX_SHADER _type)
{
	u32 glShaderType;
	CGfxFormatUtilityGL::ConvertShaderType(glShaderType, _type);
	// シェーダオブジェクトの作成
	_object.shaderID = ::glCreateShader(glShaderType);
	// シェーダオブジェクトにソース設定
	::glShaderSource(_object.shaderID, 1, &_pSrc, PCast<s32*>(&_size));
	// シェーダコンパイル
	::glCompileShader(_object.shaderID);

#ifndef POISON_RELEASE
	// コンパイルチェック
	s32 compiled;
	::glGetShaderiv(_object.shaderID, GL_COMPILE_STATUS, &compiled);
	if (compiled == GL_FALSE)
	{
		libAlert("[ERROR] Compile Error\n");
		// ログ出力
		ShaderLogInfo(_object.shaderID);
		libAssert(0);
		return false;
	}
#endif // !POISON_RELEASE
	return true;
}

///-------------------------------------------------------------------------------------------------
///	Spir-Vシェーダコンパイル（厳密にはコンパイル済みをロードするだけ）
b8		SpirVCompile(GL_SHADER_OBJECT& _object, const void* _pBuf, u32 _size, GFX_SHADER _type)
{
	u32 glShaderType;
	CGfxFormatUtilityGL::ConvertShaderType(glShaderType, _type);
	// シェーダオブジェクトの作成
	_object.shaderID = ::glCreateShader(glShaderType);
	// spir-V読み込み
	::glShaderBinary(1, &_object.shaderID, GL_SHADER_BINARY_FORMAT_SPIR_V, _pBuf, SCast<s32>(_size));
	::glSpecializeShader(_object.shaderID, ENTRY_POINT_NAME, 0, NULL, NULL);

#ifndef POISON_RELEASE
	// シェーダコンパイル
	::glCompileShader(_object.shaderID);
	s32 compiled;
	// コンパイルチェック
	::glGetShaderiv(_object.shaderID, GL_COMPILE_STATUS, &compiled);
	if (compiled == GL_FALSE)
	{
		libAlert("[ERROR] Compile Error\n");
		// ログ出力
		ShaderLogInfo(_object.shaderID);
		libAssert(0);
		return false;
	}
#endif // !POISON_RELEASE
	return true;
}

///-------------------------------------------------------------------------------------------------
///	プログラムリンク
b8		LinkProgram(GL_SHADER_OBJECT& _object, GFX_SHADER _type)
{
	// プログラムオブジェクトの作成
	_object.programID = ::glCreateProgram();
#ifndef USE_LINK_PROGRAM
	::glProgramParameteri(_object.programID, GL_PROGRAM_SEPARABLE, GL_TRUE);
#endif // !USE_LINK_PROGRAM

	// セマンティクスロケーションバインド
	SemanticseLocationBind(_object.programID, (_type == GFX_SHADER_VERTEX), (_type == GFX_SHADER_PIXEL));

	// リンク
	::glAttachShader(_object.programID, _object.shaderID);
	::glLinkProgram(_object.programID);
#ifndef POISON_RELEASE
	// リンクチェック
	s32 linked;
	::glGetProgramiv(_object.programID, GL_LINK_STATUS, &linked);
	if (linked == GL_FALSE)
	{
		libAlert("[ERROR] Link Error\n");
		// リンクログ出力
		ProgramLogInfo(_object.programID);
		libAssert(0);
		return false;
	}
#endif // !POISON_RELEASE

	// バッファロケーションバインド
	BufferLocationBind(_object.programID);

	return true;
}

///-------------------------------------------------------------------------------------------------
///	破棄
void	DeleteShader(GL_SHADER_OBJECT& _object)
{
	// シェーダオブジェクトの削除
	::glDeleteShader(_object.shaderID);
	::glDeleteProgram(_object.programID);
}

///-------------------------------------------------------------------------------------------------
///	シェーダラベル設定
void	SetShaderLabel(GL_SHADER_OBJECT& _object, const c8* _pName)
{
	// オブジェクトラベルに名前設定
	::glObjectLabel(GL_SHADER, _object.shaderID, SCast<u32>(::strlen(_pName)), _pName);
}

///-------------------------------------------------------------------------------------------------
///	パイプラインラベル設定
void	SetPipelineLabel(LAYOUT_OBJECT& _object, const c8* _pName)
{
	// オブジェクトラベルに名前設定
#ifdef USE_LINK_PROGRAM
	::glObjectLabel(GL_PROGRAM, _object.programID, SCast<u32>(::strlen(_pName)), _pName);
#else
	::glObjectLabel(GL_PROGRAM_PIPELINE, _object.pipelineID, SCast<u32>(::strlen(_pName)), _pName);
#endif // USE_LINK_PROGRAM
}


///*************************************************************************************************
///	頂点シェーダクラス
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// 名前設定
void	VShader::SetName(const c8* _name)
{
	strcpy_s(m_name, _name);
#ifndef POISON_RELEASE
	if (IsEnable())
	{
		// オブジェクトラベルに名前設定
		SetShaderLabel(*PCast<GL_SHADER_OBJECT*>(&m_object), _name);
	}
#endif // !POISON_RELEASE
}

///-------------------------------------------------------------------------------------------------
/// 有効かどうか
b8		VShader::IsEnable() const
{
	return (m_object.shaderID != 0);
}

///-------------------------------------------------------------------------------------------------
///	生成
b8		VShader::Create(const void* _pBuf, u32 _size, const c8* _pName/* = NULL*/)
{
	b8 bRet = false;
	GL_SHADER_OBJECT& sObj = *PCast<GL_SHADER_OBJECT*>(&m_object);
	if (SpirVCompile(sObj, _pBuf, _size, GFX_SHADER_VERTEX))
	{
#ifndef USE_LINK_PROGRAM
		if (LinkProgram(sObj, GFX_SHADER_VERTEX))
		{
			bRet = true;
		}
#else
		bRet = true;
#endif // !USE_LINK_PROGRAM

		// 名前保持
		if (_pName)
		{
			SetName(_pName);
		}

#ifndef POISON_RELEASE
		// シェーダリソースチェックサム設定
		if (!ShaderReflection(sObj, m_checkSum))
		{
			PRINT_WARNING("*****[WARNING] ShaderReflection()");
		}
#endif//!POISON_RELEASE
	}
	return bRet;
}

///-------------------------------------------------------------------------------------------------
///	コンパイルして生成
b8		VShader::Compile(const c8* _pSrc, u32 _size, const c8* _pName/* = NULL*/)
{
	b8 bRet = false;
	GL_SHADER_OBJECT& sObj = *PCast<GL_SHADER_OBJECT*>(&m_object);
	if (GLSLCompile(sObj, _pSrc, _size, GFX_SHADER_VERTEX))
	{
#ifndef USE_LINK_PROGRAM
		if (LinkProgram(sObj, GFX_SHADER_VERTEX))
		{
			bRet = true;
		}
#else
		bRet = true;
#endif // !USE_LINK_PROGRAM

		// 名前保持
		if (_pName)
		{
			SetName(_pName);
		}

#ifndef POISON_RELEASE
		// シェーダリソースチェックサム設定
		if (!ShaderReflection(sObj, m_checkSum))
		{
			PRINT_WARNING("*****[WARNING] ShaderReflection()");
		}
#endif//!POISON_RELEASE
	}

	return bRet;
}

///-------------------------------------------------------------------------------------------------
///	破棄
void	VShader::Release()
{
	// シェーダオブジェクトの削除
	DeleteShader(*PCast<GL_SHADER_OBJECT*>(&m_object));
	new(this)VShader;
}


///*************************************************************************************************
///	ジオメトリシェーダクラス
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// 名前設定
void	GShader::SetName(const c8* _name)
{
	strcpy_s(m_name, _name);
#ifndef POISON_RELEASE
	if (IsEnable())
	{
		// オブジェクトラベルに名前設定
		SetShaderLabel(*PCast<GL_SHADER_OBJECT*>(&m_object), _name);
	}
#endif // !POISON_RELEASE
}

///-------------------------------------------------------------------------------------------------
/// 有効かどうか
b8		GShader::IsEnable() const
{
	return (m_object.shaderID != 0);
}

///-------------------------------------------------------------------------------------------------
///	生成
b8		GShader::Create(const void* _pBuf, u32 _size, const c8* _pName/* = NULL*/)
{
	b8 bRet = false;
	GL_SHADER_OBJECT& sObj = *PCast<GL_SHADER_OBJECT*>(&m_object);
	if (SpirVCompile(sObj, _pBuf, _size, GFX_SHADER_GEOMETRY))
	{
#ifndef USE_LINK_PROGRAM
		if (LinkProgram(sObj, GFX_SHADER_GEOMETRY))
		{
			bRet = true;
		}
#else
		bRet = true;
#endif // !USE_LINK_PROGRAM

		// 名前保持
		if (_pName)
		{
			SetName(_pName);
		}

#ifndef POISON_RELEASE
		// シェーダリソースチェックサム設定
		if (!ShaderReflection(sObj, m_checkSum))
		{
			PRINT_WARNING("*****[WARNING] ShaderReflection()");
		}
#endif//!POISON_RELEASE
	}
	return bRet;
}

///-------------------------------------------------------------------------------------------------
///	コンパイル
b8		GShader::Compile(const c8* _pSrc, u32 _size, const c8* _pName/* = NULL*/)
{
	b8 bRet = false;
	GL_SHADER_OBJECT& sObj = *PCast<GL_SHADER_OBJECT*>(&m_object);
	if (GLSLCompile(sObj, _pSrc, _size, GFX_SHADER_GEOMETRY))
	{
#ifndef USE_LINK_PROGRAM
		if (LinkProgram(sObj, GFX_SHADER_GEOMETRY))
		{
			bRet = true;
		}
#else
		bRet = true;
#endif // !USE_LINK_PROGRAM

		// 名前保持
		if (_pName)
		{
			SetName(_pName);
		}

#ifndef POISON_RELEASE
		// シェーダリソースチェックサム設定
		if (!ShaderReflection(sObj, m_checkSum))
		{
			PRINT_WARNING("*****[WARNING] ShaderReflection()");
		}
#endif//!POISON_RELEASE
	}
	return bRet;
}

///-------------------------------------------------------------------------------------------------
///	破棄
void	GShader::Release()
{
	// シェーダオブジェクトの削除
	DeleteShader(*PCast<GL_SHADER_OBJECT*>(&m_object));
	new(this)GShader;
}


///*************************************************************************************************
///	ピクセルシェーダクラス
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// 名前設定
void	PShader::SetName(const c8* _name)
{
	strcpy_s(m_name, _name);
#ifndef POISON_RELEASE
	if (IsEnable())
	{
		// オブジェクトラベルに名前設定
		SetShaderLabel(*PCast<GL_SHADER_OBJECT*>(&m_object), _name);
	}
#endif // !POISON_RELEASE
}

///-------------------------------------------------------------------------------------------------
/// 有効かどうか
b8		PShader::IsEnable() const
{
	return (m_object.shaderID != 0);
}

///-------------------------------------------------------------------------------------------------
///	生成
b8		PShader::Create(const void* _pBuf, u32 _size, const c8* _pName/* = NULL*/)
{
	b8 bRet = false;
	GL_SHADER_OBJECT& sObj = *PCast<GL_SHADER_OBJECT*>(&m_object);
	if (SpirVCompile(sObj, _pBuf, _size, GFX_SHADER_PIXEL))
	{
#ifndef USE_LINK_PROGRAM
		if (LinkProgram(sObj, GFX_SHADER_PIXEL))
		{
			bRet = true;
		}
#else
		bRet = true;
#endif // !USE_LINK_PROGRAM

		// 名前保持
		if (_pName)
		{
			SetName(_pName);
		}

#ifndef POISON_RELEASE
		// シェーダリソースチェックサム設定
		if (!ShaderReflection(sObj, m_checkSum))
		{
			PRINT_WARNING("*****[WARNING] ShaderReflection()");
		}
#endif//!POISON_RELEASE
	}
	return bRet;
}

///-------------------------------------------------------------------------------------------------
///	コンパイル
b8		PShader::Compile(const c8* _pSrc, u32 _size, const c8* _pName/* = NULL*/)
{
	b8 bRet = false;
	GL_SHADER_OBJECT& sObj = *PCast<GL_SHADER_OBJECT*>(&m_object);
	if (GLSLCompile(sObj, _pSrc, _size, GFX_SHADER_PIXEL))
	{
#ifndef USE_LINK_PROGRAM
		if (LinkProgram(sObj, GFX_SHADER_PIXEL))
		{
			bRet = true;
		}
#else
		bRet = true;
#endif // !USE_LINK_PROGRAM

		// 名前保持
		if (_pName)
		{
			SetName(_pName);
		}

#ifndef POISON_RELEASE
		// シェーダリソースチェックサム設定
		if (!ShaderReflection(sObj, m_checkSum))
		{
			PRINT_WARNING("*****[WARNING] ShaderReflection()");
		}
#endif//!POISON_RELEASE
	}
	return bRet;
}

///-------------------------------------------------------------------------------------------------
///	破棄
void	PShader::Release()
{
	// シェーダオブジェクトの削除
	DeleteShader(*PCast<GL_SHADER_OBJECT*>(&m_object));
	new(this)PShader;
}



///*************************************************************************************************
///	パスシェーダクラス
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// 名前設定
void	PassShader::SetName(const c8* _name)
{
	strcpy_s(m_name, _name);
#ifndef POISON_RELEASE
	if (IsEnable())
	{
		// オブジェクトラベルに名前設定
		SetPipelineLabel(m_object, _name);
	}
#endif // !POISON_RELEASE
}

///-------------------------------------------------------------------------------------------------
/// 有効かどうか
b8		PassShader::IsEnable() const
{
	return (m_object.programID != 0 || m_object.pipelineID != 0);
}

////-------------------------------------------------------------------------------------------------
// 生成
b8		PassShader::Create(const VTX_DECL* _pDecl, u32 _declNum, const INIT_DESC& _desc, const c8* _pName/* = NULL*/)
{
#ifdef USE_LINK_PROGRAM
	m_object.programID = ::glCreateProgram();

	// セマンティクスロケーションバインド
	SemanticseLocationBind(m_object.programID, true, true);

	// シェーダアタッチ
	if (_desc.pVshader)
	{
		::glAttachShader(m_object.programID, _desc.pVshader->GetObject().shaderID);
	}
	if (_desc.pGshader)
	{
		::glAttachShader(m_object.programID, _desc.pGshader->GetObject().shaderID);
	}
	if (_desc.pPshader)
	{
		::glAttachShader(m_object.programID, _desc.pPshader->GetObject().shaderID);
	}

	// リンク
	::glLinkProgram(m_object.programID);
#ifndef POISON_RELEASE
	// リンクチェック
	s32 linked;
	::glGetProgramiv(m_object.programID, GL_LINK_STATUS, &linked);
	if (linked == GL_FALSE)
	{
		PRINT("[ERROR] Link Error\n");
		// リンクログ出力
		ProgramLogInfo(m_object.programID);
		libAssert(0);
		return false;
	}
#endif // !POISON_RELEASE

	// バッファロケーションバインド
	BufferLocationBind(m_object.programID);

#else
	// シェーダパイプライン生成
	::glGenProgramPipelines(1, &m_object.pipelineID);
	// パイプラインバインド
	::glBindProgramPipeline(m_object.pipelineID);
	if (_desc.pVshader)
	{
		::glUseProgramStages(m_object.pipelineID, GL_VERTEX_SHADER_BIT, _desc.pVshader->GetObject().programID);
	}
	if (_desc.pGshader)
	{
		::glUseProgramStages(m_object.pipelineID, GL_GEOMETRY_SHADER_BIT, _desc.pGshader->GetObject().programID);
	}
	if (_desc.pPshader)
	{
		::glUseProgramStages(m_object.pipelineID, GL_FRAGMENT_SHADER_BIT, _desc.pPshader->GetObject().programID);
	}
	// パイプラインアンバインド
	::glBindProgramPipeline(NULL);

#ifndef POISON_RELEASE
	// パイプラインチェック
	//s32 params[GFX_SHADER_GRAPHICS_TYPE_NUM];
	//::glGetProgramPipelineiv(m_object.pipelineID, GL_ACTIVE_PROGRAM, params);
	//if (params == GL_FALSE)
	{
		//libAlert("[ERROR] Pipeline Error\n");
		// パイプラインログ出力
		PipelineLogInfo(m_object.pipelineID);
		//return false;
	}
#endif // !POISON_RELEASE

#endif // USE_LINK_PROGRAM

	// 頂点情報セットアップ
	VtxDeclSetUp(_pDecl, _declNum);
	m_vshader = _desc.pVshader;
	m_gshader = _desc.pGshader;
	m_pshader = _desc.pPshader;

	// 名前設定
	if (_pName)
	{
		SetName(_pName);
	}

	return (m_object.programID != 0 || m_object.pipelineID != 0);
}

////-------------------------------------------------------------------------------------------------
///	破棄
void	PassShader::Release()
{
#ifdef USE_LINK_PROGRAM
	// プログラムオブジェクトの削除
	::glDeleteProgram(m_object.programID);
#else
	// パイプラインオブジェクトの削除
	::glDeleteProgramPipelines(1, &m_object.pipelineID);
#endif // USE_LINK_PROGRAM
	new(this)PassShader;
}



POISON_END


#endif // LIB_GFX_OPENGL

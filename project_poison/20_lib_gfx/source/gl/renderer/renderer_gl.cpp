﻿
#ifdef LIB_GFX_OPENGL

///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
// include
#include "gl/renderer/renderer_gl.h"


#include "device/device_utility.h"
#include "gl/device/win/device_gl.h"

#include "format/gfx_format_utility.h"
#include "gl/format/gfx_format_utility_gl.h"

#include "renderer/render_target.h"

// shader
#include "shader/shader.h"
#include "shader/shader_utility.h"

// object
#include "object/color_buffer.h"
#include "object/depth_buffer.h"
#include "object/vertex_buffer.h"
#include "object/index_buffer.h"
#include "object/uniform_buffer.h"
#include "object/texture_buffer.h"

#include "object/sampler_state.h"
#include "object/rasterrizer_state.h"
#include "object/blend_state.h"
#include "object/depth_state.h"

// query
#include "query/occlusion_query.h"
#include "query/primitive_query.h"
#include "query/timer_query.h"


POISON_BGN


CRendererGL::CRendererGL()
{
}

CRendererGL::~CRendererGL()
{
}

///-------------------------------------------------------------------------------------------------
///	初期化
b8		CRendererGL::Initialize(IAllocator* _pAllocator, const c8* _pName)
{
	// 基底クラスたちの初期化
	if (!CRenderer::Initialize(_pAllocator, _pName)) { return false; }

	m_pDrawContext = &m_drawContext;
#ifdef USE_IMMEDIATE
	return true;
#else
	return m_drawContext.Initialize(_pAllocator);
#endif // USE_IMMEDIATE
}

///-------------------------------------------------------------------------------------------------
///	終了
void	CRendererGL::Finalize()
{
#ifdef USE_IMMEDIATE
#else
	m_drawContext.Finalize();
#endif // USE_IMMEDIATE

	// 基底クラスたちの終了
	CRenderer::Finalize();
}

///-------------------------------------------------------------------------------------------------
/// バッファクリア
void	CRendererGL::ClearSurface(const CVec4& _clr, RENDER_BUFFER_BIT _bit/*=RENDER_BUFFER_ALL*/)
{
	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	// ビューポートフラッシュ
	FlushViewport();
	// シザレクトフラッシュ
	FlushScissorRect();

	const CRenderTarget* pCacheRT = m_pCacheRenderTarget[m_currentRenderTargetIdx];
	if (pCacheRT && pCacheRT->IsBind())
	{
		COLOR_BIT cacheBit = m_pCacheColorBufferIdxBit[m_currentRenderTargetIdx];
		u32 cacheDepIdx = m_pCacheDepthBufferIdx[m_currentRenderTargetIdx];
		const FRAME_OBJECT& frameObj = pCacheRT->GetObject();
		// フレームバインド
		pContext->BindFramebuffer(frameObj.target, &frameObj.objectID);

		// カラーバッファクリア
		u32 clrIdx = 0;
		u32 slotIdx = 0;
		while (cacheBit)
		{
			u32 glSlot = 0;
			CGfxFormatUtilityGL::ConvertRenderTargetSlot(glSlot, slotIdx);
			const CColorBuffer* pClrBuf = pCacheRT->GetColorRender(clrIdx);
			if ((cacheBit & 0x01) && pClrBuf)
			{
				const COLOR_OBJECT& clrObj = pClrBuf->GetObject();
				if ((_bit >> slotIdx) & 0x01)
				{
					// レンダーバッファ
					if (clrObj.target == GL_RENDERBUFFER)
					{
						pContext->BindRenderbuffer(clrObj.target, &clrObj.objectID);
						pContext->FramebufferRenderbuffer(
							frameObj.target,
							glSlot,
							clrObj.target,
							&clrObj.objectID
							);
					}
					// テクスチャバッファ
					else
					{
						pContext->BindTexture(clrObj.target, &clrObj.objectID);
						pContext->FramebufferTexture2D(
							frameObj.target,
							glSlot,
							clrObj.target,
							&clrObj.objectID,
							0
							);
					}
					// 描画するカラーバッファリスト指定
					pContext->DrawBuffers(1, &clrObj.target);
					// バッファクリア
					pContext->ClearBufferColor(_clr[0], _clr[1], _clr[2], _clr[3]);
				}
				slotIdx++;
			}
			
			cacheBit >>= 1;
			clrIdx++;
		}

		// デプスステンシルクリア
		if (_bit & RENDER_BUFFER_BIT_DEPTH_STENCIL)
		{
			const CDepthBuffer* pDepBuf = pCacheRT->GetDepthRender(cacheDepIdx);
			if (pDepBuf)
			{
				b8 isDepth = false, isStencil = false;
				CGfxFormatUtility::CheckDepthStencilFormat(isDepth, isStencil, pDepBuf->GetFormat());
				isDepth &= (_bit & RENDER_BUFFER_BIT_DEPTH) != 0;
				isStencil &= (_bit & RENDER_BUFFER_BIT_STENCIL) != 0;

				const DEPTH_OBJECT& depObj = pDepBuf->GetObject();
				// レンダーバッファ
				if (depObj.target == GL_RENDERBUFFER)
				{
					pContext->BindRenderbuffer(depObj.target, &depObj.objectID);
					// デプスバッファ設定
					if (isDepth)
					{
						pContext->FramebufferRenderbuffer(
							frameObj.target,
							GL_DEPTH_ATTACHMENT,
							depObj.target,
							&depObj.objectID
							);
					}
					// ステンシルバッファ設定
					if (isStencil)
					{
						pContext->FramebufferRenderbuffer(
							frameObj.target,
							GL_STENCIL_ATTACHMENT,
							depObj.target,
							&depObj.objectID
							);
					}
				}
				// テクスチャバッファ
				else
				{
					pContext->BindTexture(depObj.target, &depObj.objectID);
					// デプスバッファ設定
					if (isDepth)
					{
						pContext->FramebufferTexture2D(
							frameObj.target,
							GL_DEPTH_ATTACHMENT,
							depObj.target,
							&depObj.objectID,
							0
							);
					}
					// ステンシルバッファ設定
					if (isStencil)
					{
						pContext->FramebufferTexture2D(
							frameObj.target,
							GL_STENCIL_ATTACHMENT,
							depObj.target,
							&depObj.objectID,
							0
							);
					}
				}
				
				// バッファクリア
				if (isDepth)
				{
					pContext->ClearBufferDepth(CLEAR_DEPTH);
				}
				if (isStencil)
				{
					pContext->ClearBufferStencil(CLEAR_STENCIL);
				}
			}
		}
#ifndef POISON_RELEASE
		// ステータスチェック
		pContext->CheckFramebufferStatus(&pCacheRT->GetObject().objectID);
#endif // !POISON_RELEASE
	}
	// ディスプレイクリア
	else
	{
		u32 glBit;
		CGfxFormatUtilityGL::ConvertClearRenderTargetBit(glBit, _bit);
		pContext->ClearColor(_clr[0], _clr[1], _clr[2], _clr[3]);
		pContext->ClearDepth(CLEAR_DEPTH);
		pContext->ClearStencil(CLEAR_STENCIL);
		pContext->Clear(glBit);
	}
}
void	CRendererGL::ClearSurface(const CVec4& _clr, const CRenderTarget* _pRenderTarget, COLOR_BIT _clrBit, u32 _depIdx)
{
	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	// ビューポートフラッシュ
	FlushViewport();
	// シザレクトフラッシュ
	FlushScissorRect();

	if (_pRenderTarget && _pRenderTarget->IsBind())
	{
		const FRAME_OBJECT& frameObj = _pRenderTarget->GetObject();
		// フレームバインド
		pContext->BindFramebuffer(frameObj.target, &frameObj.objectID);

		// カラーバッファクリア
		u32 clrIdx = 0;
		u32 slotIdx = 0;
		while (_clrBit)
		{
			u32 glSlot = 0;
			CGfxFormatUtilityGL::ConvertRenderTargetSlot(glSlot, slotIdx);
			const CColorBuffer* pClrBuf = _pRenderTarget->GetColorRender(clrIdx);
			if ((_clrBit & 0x01) && pClrBuf)
			{
				const COLOR_OBJECT& clrObj = pClrBuf->GetObject();
				// レンダーバッファ
				if (clrObj.target == GL_RENDERBUFFER)
				{
					pContext->BindRenderbuffer(clrObj.target, &clrObj.objectID);
					pContext->FramebufferRenderbuffer(
						frameObj.target,
						glSlot,
						clrObj.target,
						&clrObj.objectID
						);
				}
				// テクスチャバッファ
				else
				{
					pContext->BindTexture(clrObj.target, &clrObj.objectID);
					pContext->FramebufferTexture2D(
						frameObj.target,
						glSlot,
						clrObj.target,
						&clrObj.objectID,
						0
						);
				}
				// 描画するカラーバッファリスト指定
				pContext->DrawBuffers(1, &clrObj.target);
				// バッファクリア
				pContext->ClearBufferColor(_clr[0], _clr[1], _clr[2], _clr[3]);
				slotIdx++;
			}
			_clrBit >>= 1;
			clrIdx++;
		}

		// デプスステンシルクリア
		const CDepthBuffer* pDepBuf = _pRenderTarget->GetDepthRender(_depIdx);
		if (pDepBuf)
		{
			b8 isDepth, isStencil;
			CGfxFormatUtility::CheckDepthStencilFormat(isDepth, isStencil, pDepBuf->GetFormat());
			const DEPTH_OBJECT& depObj = pDepBuf->GetObject();
			// レンダーバッファ
			if (depObj.target == GL_RENDERBUFFER)
			{
				pContext->BindRenderbuffer(depObj.target, &depObj.objectID);
				// デプスバッファ設定
				if (isDepth)
				{
					pContext->FramebufferRenderbuffer(
						frameObj.target,
						GL_DEPTH_ATTACHMENT,
						depObj.target,
						&depObj.objectID
						);
				}
				// ステンシルバッファ設定
				if (isStencil)
				{
					pContext->FramebufferRenderbuffer(
						frameObj.target,
						GL_STENCIL_ATTACHMENT,
						depObj.target,
						&depObj.objectID
						);
				}
			}
			// テクスチャバッファ
			else
			{
				pContext->BindTexture(depObj.target, &depObj.objectID);
				// デプスバッファ設定
				if (isDepth)
				{
					pContext->FramebufferTexture2D(
						frameObj.target,
						GL_DEPTH_ATTACHMENT,
						depObj.target,
						&depObj.objectID,
						0
						);
				}
				// ステンシルバッファ設定
				if (isStencil)
				{
					pContext->FramebufferTexture2D(
						frameObj.target,
						GL_STENCIL_ATTACHMENT,
						depObj.target,
						&depObj.objectID,
						0
						);
				}
			}

			// バッファクリア
			if (isDepth)
			{
				pContext->ClearBufferDepth(CLEAR_DEPTH);
			}
			if (isStencil)
			{
				pContext->ClearBufferStencil(CLEAR_STENCIL);
			}
		}
#ifndef POISON_RELEASE
		// ステータスチェック
		pContext->CheckFramebufferStatus(&_pRenderTarget->GetObject().objectID);
#endif // !POISON_RELEASE
	}
	// ディスプレイクリア
	else
	{
		pContext->ClearColor(_clr[0], _clr[1], _clr[2], _clr[3]);
		pContext->ClearDepth(CLEAR_DEPTH);
		pContext->ClearStencil(CLEAR_STENCIL);
		pContext->Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);	// 全部クリアしてまえ
	}
}

///-------------------------------------------------------------------------------------------------
///	描画
void	CRendererGL::DrawPrim(GFX_PRIMITIVE _primType, u32 _num, u32 _offset/* = 0*/)
{
	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	// 描画フラッシュ開始
	FlushDrawBegin();

	// プリミティブタイプ変換
	u32 glPrim;
	CGfxFormatUtilityGL::ConvertPrimitive(glPrim, _primType);
	pContext->DrawArrays(glPrim, _offset, _num);

	// 描画フラッシュ終了
	FlushDrawEnd();
}

///-------------------------------------------------------------------------------------------------
///	描画
void	CRendererGL::DrawIndexPrim(GFX_PRIMITIVE _primType, u32 _num, u32 _vtxOffset/* = 0*/, u32 _idxOffset/* = 0*/)
{
	if (m_pCacheIndex == NULL){ return; }

	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	// 描画フラッシュ開始
	FlushDrawBegin();

	// プリミティブタイプ変換
	u32 glPrim;
	CGfxFormatUtilityGL::ConvertPrimitive(glPrim, _primType);
	GFX_VALUE valType = CGfxFormatUtility::GetValueFormatU(m_pCacheIndex->GetElemSize());
	u32 glValType;
	CGfxFormatUtilityGL::ConvertValueType(glValType, valType);
	pContext->DrawIndexed(glPrim, glValType, _idxOffset, _vtxOffset, _num);

	// 描画フラッシュ終了
	FlushDrawEnd();
}


///-------------------------------------------------------------------------------------------------
/// クエリー開始
void	CRendererGL::BeginQuery(const COcclusionQuery* _pQuery)
{
	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	libAssert(_pQuery && _pQuery->IsEnable());
	GFX_PREDICATE_COUNTER& rQuery = *CCast<GFX_PREDICATE_COUNTER*>(&_pQuery->GetCounter());
	switch (_pQuery->GetType())
	{
	case GFX_PREDICATE_OCCL_CHEKER:
		rQuery.CountUp();
		pContext->BeginQuery(GL_ANY_SAMPLES_PASSED, &rQuery.GetPredicate());
		break;
	case GFX_PREDICATE_PIXEL_COUNTER:
		rQuery.CountUp();
		pContext->BeginQuery(GL_SAMPLES_PASSED, &rQuery.GetPredicate());
		break;
	default:
		break;
	}
}

void	CRendererGL::BeginQuery(const CPrimitiveQuery*_pQuery)
{
	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	libAssert(_pQuery && _pQuery->IsEnable());
	GFX_COUNTER& rQuery = *CCast<GFX_COUNTER*>(&_pQuery->GetCounter());
	rQuery.CountUp();
	pContext->BeginQuery(GL_PRIMITIVES_GENERATED, &rQuery.GetQuery());
}

void	CRendererGL::BeginQuery(const CTimerQuery* _pQuery)
{
	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	libAssert(_pQuery && _pQuery->IsEnable());
	GFX_COUNTER& rQuery = *CCast<GFX_COUNTER*>(&_pQuery->GetCounter());
	rQuery.CountUp();
	pContext->BeginQuery(GL_TIME_ELAPSED, &rQuery.GetQuery());
}

///-------------------------------------------------------------------------------------------------
/// クエリー終了
void	CRendererGL::EndQuery(const COcclusionQuery* _pQuery)
{
	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	libAssert(_pQuery && _pQuery->IsEnable());
	switch (_pQuery->GetType())
	{
	case GFX_PREDICATE_OCCL_CHEKER:
		pContext->EndQuery(GL_ANY_SAMPLES_PASSED);
		break;
	case GFX_PREDICATE_PIXEL_COUNTER:
		pContext->EndQuery(GL_SAMPLES_PASSED);
		break;
	default:
		break;
	}
}

void	CRendererGL::EndQuery(const CPrimitiveQuery* _pQuery)
{
	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	libAssert(_pQuery && _pQuery->IsEnable());
	pContext->EndQuery(GL_PRIMITIVES_GENERATED);
}

void	CRendererGL::EndQuery(const CTimerQuery* _pQuery)
{
	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	libAssert(_pQuery && _pQuery->IsEnable());
	pContext->EndQuery(GL_TIME_ELAPSED);
}

///-------------------------------------------------------------------------------------------------
/// 事前描画遮蔽カリング設定
void	CRendererGL::SetDrawingPredictionQuery(const COcclusionQuery* _pQuery)
{
	m_pDrawingPredication = _pQuery;
}


///-------------------------------------------------------------------------------------------------
/// マーカープッシュ設定
void	CRendererGL::PushMarker(const c8* _pMessage)
{
	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	pContext->PushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, NULL, SCast<u32>(::strlen(_pMessage) + 1), _pMessage);
}

///-------------------------------------------------------------------------------------------------
/// マーカーポップ設定
void	CRendererGL::PopMarker()
{
	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	pContext->PopDebugGroup();
}



///-------------------------------------------------------------------------------------------------
/// レンダーターゲットフラッシュ
void	CRendererGL::FlushRenderTarget()
{
	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	u32 clrSetNum = 0;
	u32 clrTargets[RENDER_BUFFER_SLOT_NUM] = {};
	RENDER_BUFFER_BIT& updateBit = m_updateRenderTargetBit;
	const CRenderTarget* pCacheRT = m_pCacheRenderTarget[m_currentRenderTargetIdx];
	if (pCacheRT && pCacheRT->IsBind())
	{
		COLOR_BIT cacheBit = m_pCacheColorBufferIdxBit[m_currentRenderTargetIdx];
		u32 cacheDepIdx = m_pCacheDepthBufferIdx[m_currentRenderTargetIdx];
		const FRAME_OBJECT& frameObj = pCacheRT->GetObject();
		// フレームバインド
		pContext->BindFramebuffer(frameObj.target, &frameObj.objectID);
		
		// カラーバッファ設定
		u32 clrIdx = 0;
		u32 slotIdx = 0;
		while (cacheBit)
		{
			if (cacheBit & 0x01)
			{
				u32 glSlot = 0;
				CGfxFormatUtilityGL::ConvertRenderTargetSlot(glSlot, slotIdx);
				const u32* pObjectID = NULL;
				u32 target = GL_RENDERBUFFER;
				const CColorBuffer* pClrBuf = pCacheRT->GetColorRender(clrIdx);
				if (pClrBuf)
				{
					target = pClrBuf->GetObject().target;
					pObjectID = &pClrBuf->GetObject().objectID;
					clrTargets[slotIdx] = target;
					// テクスチャ被りチェック
					const CTextureBuffer* pClrTex = pCacheRT->GetColorTexture(clrIdx);
					if (pClrTex && ClearTextureRenderTarget(&pClrTex->GetObject()))
					{
						PRINT("*****[WARRING] Conflict Textures RenderTarget. RenderTargetName[%s] slot[%d]\n", pCacheRT->GetName(), slotIdx);
					}
					slotIdx++;

					// レンダーバッファ
					if (target == GL_RENDERBUFFER)
					{
						pContext->BindRenderbuffer(target, pObjectID);
						pContext->FramebufferRenderbuffer(
							frameObj.target,
							glSlot,
							target,
							pObjectID
						);
					}
					// テクスチャバッファ
					else
					{
						pContext->BindTexture(target, pObjectID);
						pContext->FramebufferTexture2D(
							frameObj.target,
							glSlot,
							target,
							pObjectID,
							0
						);
					}
				}
			}
			cacheBit >>= 1;
			clrIdx++;
		}
		clrSetNum = slotIdx;

		// デプス・ステンシルバッファ設定
		if (updateBit & RENDER_BUFFER_BIT_DEPTH_STENCIL)
		{
			const u32* pObjectID = NULL;
			u32 target = GL_RENDERBUFFER;
			b8 isDepth = false, isStencil = false;
			const CDepthBuffer* pDepBuf = pCacheRT->GetDepthRender(cacheDepIdx);
			if (pDepBuf)
			{
				target = pDepBuf->GetObject().target;
				pObjectID = &pDepBuf->GetObject().objectID;
				// テクスチャ被りチェック
				const CTextureBuffer* pDepTex = pCacheRT->GetDepthTexture(clrIdx);
				if (pDepTex && ClearTextureRenderTarget(&pDepTex->GetObject()))
				{
					PRINT("*****[WARRING] Conflict Textures RenderTarget. RenderTargetName[%s] depth\n", pCacheRT->GetName());
				}
				CGfxFormatUtility::CheckDepthStencilFormat(isDepth, isStencil, pDepBuf->GetFormat());

				// レンダーバッファ
				if (target == GL_RENDERBUFFER)
				{
					pContext->BindRenderbuffer(target, pObjectID);
					if (isDepth)
					{
						pContext->FramebufferRenderbuffer(
							frameObj.target,
							GL_DEPTH_ATTACHMENT,
							target,
							pObjectID
						);
					}
					if (isStencil)
					{
						pContext->FramebufferRenderbuffer(
							frameObj.target,
							GL_STENCIL_ATTACHMENT,
							target,
							pObjectID
						);
					}
				}
				// テクスチャバッファ
				else
				{
					pContext->BindTexture(target, pObjectID);
					if (isDepth)
					{
						pContext->FramebufferTexture2D(
							frameObj.target,
							GL_DEPTH_ATTACHMENT,
							target,
							pObjectID,
							0
						);
					}
					if (isStencil)
					{
						pContext->FramebufferTexture2D(
							frameObj.target,
							GL_STENCIL_ATTACHMENT,
							target,
							pObjectID,
							0
						);
					}
				}
			}
		}
		if (updateBit)
		{
			// 描画するカラーバッファリスト指定
			pContext->DrawBuffers(clrSetNum, clrTargets);
		}

#ifndef POISON_RELEASE
		// ステータスチェック
		pContext->CheckFramebufferStatus(&pCacheRT->GetObject().objectID);
#endif // !POISON_RELEASE
	}
	else
	{
		// フレームアンバインド
		pContext->UnBindFramebuffer(GL_FRAMEBUFFER);
	}

	updateBit = 0;
}

///-------------------------------------------------------------------------------------------------
/// シェーダフラッシュ
void	CRendererGL::FlushShader(const PassShader* _pPassShader)
{
	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	const VShader* pVShader = NULL;
	const GShader* pGShader = NULL;
	const PShader* pPShader = NULL;
	if (_pPassShader)
	{
		pVShader = _pPassShader->GetVShader();
		pGShader = _pPassShader->GetGShader();
		pPShader = _pPassShader->GetPShader();
	}

	if (m_pCacheVShader != pVShader)
	{
		// 頂点シェーダー更新
		m_pCacheVShader = pVShader;
	}
	if (m_pCacheGShader != pGShader)
	{
		// ジオメトリシェーダー更新
		m_pCacheGShader = pGShader;
	}
	if (m_pCachePShader != pPShader)
	{
		// ピクセルシェーダー更新
		m_pCachePShader = pPShader;
	}

	if (_pPassShader)
	{
#ifdef USE_LINK_PROGRAM
		pContext->UseProgram(_pPassShader->GetObject().programID);
#else
		// パイプライン設定
		pContext->BindProgram(NULL);
		pContext->BindProgramPipeline(_pPassShader->GetObject().pipelineID);
#endif // USE_LINK_PROGRAM
		m_pCachePassShader = _pPassShader;
	}
}

///-------------------------------------------------------------------------------------------------
/// 頂点フラッシュ
void	CRendererGL::FlushVertex()
{
	// 先にシェーダをフラッシュする
	if (m_pCachePassShader == NULL) { return; }

	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	b8 isUpdate = false;
	u32 vtxStride = 0;
	const VTX_DECL* pDecl = m_pCachePassShader->GetVtxDecl();
	u32 declNum = m_pCachePassShader->GetDeclNum();
	for (u32 idx = 0; idx < declNum; idx++, pDecl++)
	{
		u32 valType;
		CGfxFormatUtilityGL::ConvertValueType(valType, pDecl->valType);
		u32 offset = pDecl->offset;

		if (m_updateVertexBit & (1u << pDecl->attr))
		{
			libAssert(pDecl->attr < VERTEX_ATTRIBUTE_TYPE_NUM);
			const CVertexBuffer* pVtxBuffer = m_pCacheVertex[pDecl->attr];
			if (pVtxBuffer)
			{
				const RENDER_OBJECT& vtxObj = pVtxBuffer->GetObject();
				const u32* pObjectID = &vtxObj.objectID;
				u32 target = vtxObj.target;
				// 頂点バッファがあればそちらで上書き
				vtxStride = pVtxBuffer->GetVtxStride();
				offset = 0;

				// 頂点バッファ設定
				pContext->BindBuffer(target, pObjectID);
				isUpdate |= true;
			}
			m_updateVertexBit ^= (1u << pDecl->attr);	// 更新フラグおろし
		}

		// 頂点バッファが更新されたら
		if (isUpdate)
		{
			pContext->EnableVertexAttribArray(pDecl->attr);
			pContext->VertexAttribPointer(pDecl->attr, pDecl->elemNum, valType, false, vtxStride, offset);
		}
	}
}

///-------------------------------------------------------------------------------------------------
/// インデックスフラッシュ
void	CRendererGL::FlushIndex()
{
	if (!(m_updateRanderStateBit & UPDATE_INDEX)) { return; }

	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	const u32* pObjectID = NULL;
	u32 target = GL_ELEMENT_ARRAY_BUFFER;
	if (m_pCacheIndex)
	{
		const RENDER_OBJECT& idxObj = m_pCacheIndex->GetObject();
		pObjectID = &idxObj.objectID;
		target = idxObj.target;

		// インデックスバッファ設定
		pContext->BindBuffer(target, pObjectID);
	}

	m_updateRanderStateBit &= ~UPDATE_INDEX;	// 更新フラグおろし
}

///-------------------------------------------------------------------------------------------------
/// ユニフォームフラッシュ
void	CRendererGL::FlushUniform()
{
	const u32 shaderProgramID[] =
	{
		(m_pCacheVShader ? m_pCacheVShader->GetObject().programID : NULL),
		(m_pCacheGShader ? m_pCacheGShader->GetObject().programID : NULL),
		(m_pCachePShader ? m_pCachePShader->GetObject().programID : NULL),
	};
#ifndef POISON_RELEASE
	const ShaderSlotCheckSum* s_slotCheckSum[] =
	{
		(m_pCacheVShader ? &m_pCacheVShader->GetCheckSum() : NULL),
		(m_pCacheGShader ? &m_pCacheGShader->GetCheckSum() : NULL),
		(m_pCachePShader ? &m_pCachePShader->GetCheckSum() : NULL),
	};
	StaticAssert(ARRAYOF(s_slotCheckSum) == GFX_SHADER_GRAPHICS_TYPE_NUM);
	b8 bError = false;
#endif // !POISON_RELEASE

	// 先にシェーダをフラッシュする
	if (m_pCachePassShader == NULL) { return; }

	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	for (u32 shaderIdx = 0; shaderIdx < GFX_SHADER_GRAPHICS_TYPE_NUM; shaderIdx++)
	{
		u32 slotIdx = 0;
		auto flag = m_updateUniformBit[shaderIdx];
		if (flag)
		{
#ifndef USE_LINK_PROGRAM
			// アクティベーションシェーダ
			pContext->ActiveShaderProgram(m_pCachePassShader->GetObject().pipelineID, shaderProgramID[shaderIdx]);
#endif // !USE_LINK_PROGRAM
		}
		while (flag)
		{
			const u32* pObjectID = NULL;
			u32 target = GL_UNIFORM_BUFFER;
			// 更新があれば
			if (flag & 0x01)
			{
				const RENDERER_BUFFER_DATA& data = m_cacheUniform[shaderIdx][slotIdx];
				const RENDER_OBJECT* pObj = PCast<const RENDER_OBJECT*>(data.pBuffer);
				if (pObj)
				{
					pObjectID = &pObj->objectID;
					target = pObj->target;

					// ユニフォーム設定
					pContext->BindBufferBase(target, slotIdx, pObjectID);
				}
#ifndef POISON_RELEASE
				// リソースがNULLの時、
				if (pObjectID == NULL)
				{
					if (s_slotCheckSum[shaderIdx] && s_slotCheckSum[shaderIdx]->uniformBlockMask & (1 << slotIdx))
					{
						if (bError == false)
						{
							const c8* shaderName = m_pCachePassShader->GetName();
							PRINT("*****[ERROR] No Setting ConstantBuffer. Shader[%s]\n", shaderName);
							bError = true;
						}
						const c8* shaderStageName = CGfxFormatUtility::GetShaderStageName(SCast<GFX_SHADER>(shaderIdx));
						PRINT("     ShaderStage[%s] ConstantBuffer[%2d]\n", shaderStageName, slotIdx);
					}
				}
#endif // !POISON_RELEASE
			}
			++slotIdx;
			// シフトしていく
			flag >>= 1;
		}
		m_updateUniformBit[shaderIdx] = 0;	// 更新フラグおろし

#ifndef POISON_RELEASE
		// 更新なしリソースのチェック
		for (u32 remainderIdx = slotIdx; remainderIdx < UNIFORM_SLOT_NUM; remainderIdx++)
		{
			const RENDERER_BUFFER_DATA& data = m_cacheUniform[shaderIdx][remainderIdx];
			if (!data.pBuffer && s_slotCheckSum[shaderIdx] && s_slotCheckSum[shaderIdx]->uniformBlockMask & (1 << remainderIdx))
			{
				if (bError == false)
				{
					const c8* shaderName = m_pCachePassShader->GetName();
					PRINT("*****[ERROR] No Setting ConstantBuffer. Shader[%s]\n", shaderName);
					bError = true;
				}
				const c8* shaderStageName = CGfxFormatUtility::GetShaderStageName(SCast<GFX_SHADER>(shaderIdx));
				PRINT("     ShaderStage[%s] ConstantBuffer[%2d]\n", shaderStageName, remainderIdx);
			}
		}
#endif // !POISON_RELEASE
	}

#ifndef POISON_RELEASE
	libAssert(!bError);
#endif // !POISON_RELEASE
}

///-------------------------------------------------------------------------------------------------
/// テクスチャフラッシュ
void	CRendererGL::FlushTexture()
{
	const u32 shaderProgramID[] =
	{
		(m_pCacheVShader ? m_pCacheVShader->GetObject().programID : NULL),
		(m_pCacheGShader ? m_pCacheGShader->GetObject().programID : NULL),
		(m_pCachePShader ? m_pCachePShader->GetObject().programID : NULL),
	};
#ifndef POISON_RELEASE
	const ShaderSlotCheckSum* s_slotCheckSum[] =
	{
		(m_pCacheVShader ? &m_pCacheVShader->GetCheckSum() : NULL),
		(m_pCacheGShader ? &m_pCacheGShader->GetCheckSum() : NULL),
		(m_pCachePShader ? &m_pCachePShader->GetCheckSum() : NULL),
	};
	StaticAssert(ARRAYOF(s_slotCheckSum) == GFX_SHADER_GRAPHICS_TYPE_NUM);
	b8 bError = false;
#endif // !POISON_RELEASE

	// 先にシェーダをフラッシュする
	if (m_pCachePassShader == NULL) { return; }

	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	for (u32 shaderIdx = 0; shaderIdx < GFX_SHADER_GRAPHICS_TYPE_NUM; shaderIdx++)
	{
		u32 slotIdx = 0;
		auto flag = m_updateTextureBit[shaderIdx];
		if (flag)
		{
#ifndef USE_LINK_PROGRAM
			// アクティベーションシェーダ
			pContext->ActiveShaderProgram(m_pCachePassShader->GetObject().pipelineID, shaderProgramID[shaderIdx]);
#endif // !USE_LINK_PROGRAM
		}
		while (flag)
		{
			const u32* pObjectID = NULL;
			u32 target = GL_TEXTURE_2D;
			// 更新があれば
			if (flag & 0x01)
			{
				const RENDERER_BUFFER_DATA& data = m_cacheTexAndRoBuf[shaderIdx][slotIdx];
				const TEXTURE_OBJECT* pObj = PCast<const TEXTURE_OBJECT*>(data.pBuffer);
				if (pObj)
				{
					pObjectID = &pObj->objectID;
					target = pObj->target;

					// テクスチャ設定
					u32 glSlot = 0;
					CGfxFormatUtilityGL::ConvertTextureSlot(glSlot, slotIdx);
					pContext->ActiveTexture(glSlot);
					pContext->BindTexture(target, pObjectID);
				}
#ifndef POISON_RELEASE
				// リソースがNULLの時、
				if (pObjectID == NULL)
				{
					if (s_slotCheckSum[shaderIdx] && s_slotCheckSum[shaderIdx]->texAndRoBufMask & (1 << slotIdx))
					{
						if (bError == false)
						{
							const c8* shaderName = m_pCachePassShader->GetName();
							PRINT("*****[ERROR] No Setting Textures. Shader[%s]\n", shaderName);
							bError = true;
						}
						const c8* shaderStageName = CGfxFormatUtility::GetShaderStageName(SCast<GFX_SHADER>(shaderIdx));
						PRINT("     ShaderStage[%s] Textures[%2d]\n", shaderStageName, slotIdx);
					}
				}
#endif // !POISON_RELEASE
			}
			++slotIdx;
			// シフトしていく
			flag >>= 1;
		}
		m_updateTextureBit[shaderIdx] = 0;	// 更新フラグおろし

#ifndef POISON_RELEASE
		// 更新なしリソースのチェック
		for (u32 remainderIdx = slotIdx; remainderIdx < UNIFORM_SLOT_NUM; remainderIdx++)
		{
			const RENDERER_BUFFER_DATA& data = m_cacheTexAndRoBuf[shaderIdx][remainderIdx];
			if (!data.pBuffer && s_slotCheckSum[shaderIdx] && s_slotCheckSum[shaderIdx]->texAndRoBufMask & (1 << remainderIdx))
			{
				if (bError == false)
				{
					const c8* shaderName = m_pCachePassShader->GetName();
					PRINT("*****[ERROR] No Setting Textures. Shader[%s]\n", shaderName);
					bError = true;
				}
				const c8* shaderStageName = CGfxFormatUtility::GetShaderStageName(SCast<GFX_SHADER>(shaderIdx));
				PRINT("     ShaderStage[%s] Textures[%2d]\n", shaderStageName, remainderIdx);
			}
		}
#endif // !POISON_RELEASE
	}

#ifndef POISON_RELEASE
	libAssert(!bError);
#endif // !POISON_RELEASE
}

///-------------------------------------------------------------------------------------------------
/// サンプラーステートフラッシュ
void	CRendererGL::FlushSamplerState()
{
	const u32 shaderProgramID[] =
	{
		(m_pCacheVShader ? m_pCacheVShader->GetObject().programID : NULL),
		(m_pCacheGShader ? m_pCacheGShader->GetObject().programID : NULL),
		(m_pCachePShader ? m_pCachePShader->GetObject().programID : NULL),
	};

	// 先にシェーダをフラッシュする
	if (m_pCachePassShader == NULL) { return; }

	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	for (u32 shaderIdx = 0; shaderIdx < GFX_SHADER_GRAPHICS_TYPE_NUM; shaderIdx++)
	{
		u32 slotIdx = 0;
		auto flag = m_updateSamplerBit[shaderIdx];
		if (flag)
		{
#ifndef USE_LINK_PROGRAM
			// アクティベーションシェーダ
			pContext->ActiveShaderProgram(m_pCachePassShader->GetObject().pipelineID, shaderProgramID[shaderIdx]);
#endif // !USE_LINK_PROGRAM
		}
		while (flag)
		{
			// 更新があれば
			if (flag & 0x01)
			{
				const CSampler* pSampler = m_pCacheSampler[m_currentSamplerIdx][shaderIdx][slotIdx];
				const u32* pObjectID = NULL;
				if (pSampler)
				{
					pObjectID = &pSampler->GetObject().objectID;

					// テクスチャステート設定
					pContext->BindSampler(slotIdx, pObjectID);
				}
			}
			++slotIdx;
			// シフトしていく
			flag >>= 1;
		}
		m_updateSamplerBit[shaderIdx] = 0;	// 更新フラグおろし
	}
}

///-------------------------------------------------------------------------------------------------
/// ビューポートフラッシュ
void	CRendererGL::FlushViewport()
{
	if (!(m_updateRanderStateBit & UPDATE_VIEWPORT)) { return; }

	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	// ビューポート設定
#if defined( OPENGL_VIEWPORT )
	pContext->Viewport(m_cacheViewport.posX, m_cacheViewport.posY, m_cacheViewport.width, m_cacheViewport.height);
#elif defined( DIRECTX_VIEWPORT )
	const CRenderTarget* pCacheRT = m_pCacheRenderTarget[m_currentRenderTargetIdx];
	libAssert(pCacheRT);
	pContext->Viewport(
		m_cacheViewport.posX,
		pCacheRT->GetHeight() - m_cacheViewport.height - m_cacheViewport.posY,
		m_cacheViewport.width,
		m_cacheViewport.height
		);
#endif
	// デプス範囲設定
	pContext->Enable(GL_DEPTH_CLAMP);
	pContext->DepthRange(0.0f, 1.0f);

	m_updateRanderStateBit &= ~UPDATE_VIEWPORT;	// 更新フラグおろし
}

///-------------------------------------------------------------------------------------------------
/// シザリングの領域設定
void	CRendererGL::FlushScissorRect()
{
	if (!(m_updateRanderStateBit & UPDATE_SCISSOR)) { return; }

	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	// シザリング設定
	pContext->Enable(GL_SCISSOR_TEST);
#if defined( OPENGL_VIEWPORT )
	pContext->Scissor(m_cacheScissor.posX, m_cacheScissor.posY, m_cacheScissor.width, m_cacheScissor.height);
#elif defined( DIRECTX_VIEWPORT )
	const CRenderTarget* pCacheRT = m_pCacheRenderTarget[m_currentRenderTargetIdx];
	libAssert(pCacheRT);
	pContext->Scissor(
		m_cacheScissor.posX,
		pCacheRT->GetHeight() - m_cacheScissor.height - m_cacheScissor.posY,
		m_cacheScissor.width,
		m_cacheScissor.height
		);
#endif
	
	m_updateRanderStateBit &= ~UPDATE_SCISSOR;	// 更新フラグおろし
}

///-------------------------------------------------------------------------------------------------
/// ラストライザステートフラッシュ
void	CRendererGL::FlushRasterrizerState()
{
	if (!(m_updateRanderStateBit & UPDATE_RASTERRIZER)) { return; }

	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	if (m_pCacheRasterizer[m_currentRasterizerIdx])
	{
		const RASTERIZER_OBJECT& object = m_pCacheRasterizer[m_currentRasterizerIdx]->GetObject();
		if (object.cullFace != GL_NONE) {
			pContext->Enable(GL_CULL_FACE);
		}
		else {
			pContext->Disable(GL_CULL_FACE);
		}
		pContext->Enable(GL_POLYGON_OFFSET_FILL);
		pContext->Enable(GL_POLYGON_OFFSET_LINE);
		pContext->Enable(GL_POLYGON_OFFSET_POINT);

		pContext->FrontFace(object.frontFace);
		pContext->CullFace(object.cullFace);
		pContext->PolygonMode(GL_FRONT_AND_BACK, GL_FILL);	// 塗りつぶしタイプは固定でええやろ
		pContext->PolygonOffset(object.slopeScaledDepthBias, object.depthBias);

	}
	else
	{
		pContext->Disable(GL_CULL_FACE);
		pContext->Disable(GL_POLYGON_OFFSET_FILL);
		pContext->Disable(GL_POLYGON_OFFSET_LINE);
		pContext->Disable(GL_POLYGON_OFFSET_POINT);
	}

	m_updateRanderStateBit &= ~UPDATE_RASTERRIZER;	// 更新フラグおろし
}

///-------------------------------------------------------------------------------------------------
/// ブレンドステートフラッシュ
void	CRendererGL::FlushBrendState()
{
	if (!(m_updateRanderStateBit & UPDATE_BLEND_STATE)) { return; }

	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	if (m_pCacheBlendState[m_currentBlendStateIdx])
	{
		const BLEND_STATE_OBJECT& object = m_pCacheBlendState[m_currentBlendStateIdx]->GetObject();
		if (!object.isIndependent)
		{
			const BLEND_STATE_OBJECT::TRG_STATE& state = object.state[0];
			if (state.enable)
			{
				pContext->Enable(GL_BLEND);
				pContext->BlendEquationSeparate(state.colorOp, state.alphaOp);
				pContext->BlendFuncSeparate(
					state.colorSrcFunc, state.colorDstFunc,
					state.alphaSrcFunc, state.alphaDstFunc
					);
			}
			else
			{
				pContext->Disable(GL_BLEND);
			}
			pContext->ColorMask(
				state.colorMask[0],
				state.colorMask[1],
				state.colorMask[2],
				state.colorMask[3]
			);
		}
		else
		{
			for (u32 trgSlot = 0; trgSlot < RENDER_BUFFER_SLOT_NUM; trgSlot++)
			{
				const BLEND_STATE_OBJECT::TRG_STATE& state = object.state[trgSlot];
				if (state.enable)
				{
					pContext->Enablei(GL_BLEND, trgSlot);
					pContext->BlendEquationSeparatei(trgSlot, state.colorOp, state.alphaOp);
					pContext->BlendFuncSeparatei(
						trgSlot,
						state.colorSrcFunc, state.colorDstFunc,
						state.alphaSrcFunc, state.alphaDstFunc
						);
				}
				else
				{
					pContext->Disablei(GL_BLEND, trgSlot);
				}
				pContext->ColorMaski(
					trgSlot,
					state.colorMask[0],
					state.colorMask[1],
					state.colorMask[2],
					state.colorMask[3]
				);
			}
		}
	}
	else
	{
		pContext->Disable(GL_BLEND);
		pContext->ColorMask(
			true,
			true,
			true,
			true
		);
	}

	m_updateRanderStateBit &= ~UPDATE_BLEND_STATE;	// 更新フラグおろし
}

///-------------------------------------------------------------------------------------------------
/// デプスステンシルステートフラッシュ
void	CRendererGL::FlushDepthStencilState()
{
	if (!(m_updateRanderStateBit & UPDATE_DEPTH_STATE)) { return; }

	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	if (m_pCacheDepthState[m_currentDepthStateIdx])
	{
		const DEPTH_STATE_OBJECT& object = m_pCacheDepthState[m_currentDepthStateIdx]->GetObject();
		if (object.isDepthTest)
		{
			pContext->Enable(GL_DEPTH_TEST);
			pContext->DepthMask(object.isDepthWrite);
			pContext->DepthFunc(object.depthTest);
		}
		else
		{
			pContext->Disable(GL_DEPTH_TEST);
		}

		if (object.isStencilTest)
		{
			pContext->Enable(GL_STENCIL_TEST);
			u32 stencilRef = 0x0;					// !!!! サンプル ステンシル参照値を設定させるようにしなきゃなぁ
			pContext->StencilMaskSeparate(GL_FRONT_AND_BACK, object.stencilWriteMask);
			pContext->StencilFuncSeparate(GL_FRONT_AND_BACK, object.stencilTest, stencilRef, object.stencilReadMask);
			pContext->StencilOpSeparate(GL_FRONT_AND_BACK, object.stencilFailOp, object.stencilZFailOp, object.stencilPassOp);
		}
		else
		{
			pContext->Disable(GL_STENCIL_TEST);
		}
	}
	else
	{
		pContext->Disable(GL_DEPTH_TEST);
		pContext->Disable(GL_STENCIL_TEST);
	}

	m_updateRanderStateBit &= ~UPDATE_DEPTH_STATE;	// 更新フラグおろし
}

///-------------------------------------------------------------------------------------------------
///	コマンドキューフラッシュ
void	CRendererGL::FlushCommandQueue()
{
	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	pContext->Flush();	// キューフラッシュ

#ifdef USE_IMMEDIATE
#else
	if (m_drawContext.GetCommandListNum() > 0)
	{
		m_drawContext.ExecuteCommandList();
	}
#endif // USE_IMMEDIATE
}

///-------------------------------------------------------------------------------------------------
///	事前描画プリディケーションフラッシュ開始
void	CRendererGL::FlushDrawingPredictionQueryBegin()
{
	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	if (m_pDrawingPredication)
	{
		libAssert(m_pDrawingPredication->IsEnable() && m_pDrawingPredication->GetType() == GFX_PREDICATE_OCCL_CHEKER);
		GFX_PREDICATE_COUNTER& rQuery = *CCast<GFX_PREDICATE_COUNTER*>(&m_pDrawingPredication->GetCounter());
		pContext->BeginConditionalRender(&rQuery.GetPredicate(), GL_QUERY_BY_REGION_WAIT);
	}
}

///-------------------------------------------------------------------------------------------------
///	事前描画プリディケーションフラッシュ終了
void	CRendererGL::FlushDrawingPredictionQueryEnd()
{
	// コンテキスト
	DRAW_CONTEXT* pContext = GetContext();
	libAssert(pContext);

	if (m_pDrawingPredication)
	{
		pContext->EndConditionalRender();
		m_pDrawingPredication = NULL;
	}
}


POISON_END


#endif // LIB_GFX_OPENGL

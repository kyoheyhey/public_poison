﻿
#ifdef LIB_GFX_OPENGL

///-------------------------------------------------------------------------------------------------
/// define
#define OPENGL_DLL	"opengl32.dll"


//-------------------------------------------------------------------------------------------------
// pragma
#pragma comment(lib, "openGL32.lib")
#if defined(USE_OPNEGL_PROC_GLEW)
#pragma comment(lib, "glew32.lib")
//#pragma comment(lib, "glew32s.lib")
#elif defined(USE_OPNEGL_PROC_GL_POISON)

#endif

///-------------------------------------------------------------------------------------------------
/// include
#include "utility/gfx_utility.h"
#include "gl/utility/gfx_utility_gl.h"

#include "format/gfx_format_utility.h"
#include "gl/format/gfx_format_utility_gl.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant
static HMODULE s_libgl = NULL;		///< ライブラリハンドル



///*************************************************************************************************
/// OpenGLユーティリティ
///*************************************************************************************************


///-------------------------------------------------------------------------------------------------
/// OpenGLライブラリロード
void	OpenLibgl()
{
	libAssert(s_libgl == NULL);
	s_libgl = LoadLibraryA(OPENGL_DLL);
}
void	CloseLibgl()
{
	if (s_libgl)
	{
		FreeLibrary(s_libgl);
	}
	s_libgl = NULL;
}

///-------------------------------------------------------------------------------------------------
/// OpenGL関数アドレス取得
void*	GetProcAddressGL(const char* _pProcName)
{
	void* pRes = wglGetProcAddress(_pProcName);
	if (pRes == NULL)
	{
		libAssert(s_libgl);
		pRes = GetProcAddress(s_libgl, _pProcName);
	}
	libAssertMsg(pRes, _pProcName);
	return pRes;
}

///-------------------------------------------------------------------------------------------------
/// OpenGL関数ロード
b8		CGfxUtilityGL::LoadProc()
{
	OpenLibgl();
#if defined(USE_OPNEGL_PROC_GLEW)
	// glew初期化
	if (::glewInit() != GLEW_OK)
	{
		PRINT("************[Error] Failed : ::glewInit()\n");
		return false;
	}

	// RenderBuffer Extensions
	__glewIsRenderbuffer = PCast<PFNGLISRENDERBUFFERPROC>(GetProcAddressGL("glIsRenderbuffer"));
	__glewBindRenderbuffer = PCast<PFNGLBINDRENDERBUFFERPROC>(GetProcAddressGL("glBindRenderbuffer"));
	__glewDeleteRenderbuffers = PCast<PFNGLDELETERENDERBUFFERSPROC>(GetProcAddressGL("glDeleteRenderbuffers"));
	__glewGenRenderbuffers = PCast<PFNGLGENRENDERBUFFERSPROC>(GetProcAddressGL("glGenRenderbuffers"));
	__glewRenderbufferStorage = PCast<PFNGLRENDERBUFFERSTORAGEPROC>(GetProcAddressGL("glRenderbufferStorage"));
	__glewRenderbufferStorageMultisample = PCast<PFNGLRENDERBUFFERSTORAGEMULTISAMPLEPROC>(GetProcAddressGL("glRenderbufferStorageMultisample"));
	__glewGetRenderbufferParameteriv = PCast<PFNGLGETRENDERBUFFERPARAMETERIVPROC>(GetProcAddressGL("glGetRenderbufferParameteriv"));

	// Framebuffer Extensions
	__glewIsFramebuffer = PCast<PFNGLISFRAMEBUFFERPROC>(GetProcAddressGL("glIsFramebuffer"));
	__glewBindFramebuffer = PCast<PFNGLBINDFRAMEBUFFERPROC>(GetProcAddressGL("glBindFramebuffer"));
	__glewDeleteFramebuffers = PCast<PFNGLDELETEFRAMEBUFFERSPROC>(GetProcAddressGL("glDeleteFramebuffers"));
	__glewGenFramebuffers = PCast<PFNGLGENFRAMEBUFFERSPROC>(GetProcAddressGL("glGenFramebuffers"));
	__glewCheckFramebufferStatusEXT = PCast<PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC>(GetProcAddressGL("glCheckFramebufferStatusEXT"));
	__glewFramebufferTexture1D = PCast<PFNGLFRAMEBUFFERTEXTURE1DPROC>(GetProcAddressGL("glFramebufferTexture1D"));
	__glewFramebufferTexture2D = PCast<PFNGLFRAMEBUFFERTEXTURE2DPROC>(GetProcAddressGL("glFramebufferTexture2D"));
	__glewFramebufferTexture3D = PCast<PFNGLFRAMEBUFFERTEXTURE3DPROC>(GetProcAddressGL("glFramebufferTexture3D"));
	//__glewFramebufferTexture2DMultisampleEXT = PCast<PFNGLFRAMEBUFFERTEXTURE2DMULTISAMPLEEXTPROC>(GetProcAddressGL("glFramebufferTexture2DMultisampleEXT"));
	__glewFramebufferTextureLayer = PCast<PFNGLFRAMEBUFFERTEXTURELAYERPROC>(GetProcAddressGL("glFramebufferTextureLayer"));
	__glewFramebufferRenderbuffer = PCast<PFNGLFRAMEBUFFERRENDERBUFFERPROC>(GetProcAddressGL("glFramebufferRenderbuffer"));
	__glewGetFramebufferAttachmentParameteriv = PCast<PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVPROC>(GetProcAddressGL("glGetFramebufferAttachmentParameteriv"));
	__glewGenerateMipmap = PCast<PFNGLGENERATEMIPMAPPROC>(GetProcAddressGL("glGenerateMipmap"));
	__glewBlitFramebuffer = PCast<PFNGLBLITFRAMEBUFFERPROC>(GetProcAddressGL("glBlitFramebuffer"));

	__glewClearBufferfv = PCast<PFNGLCLEARBUFFERFVPROC>(GetProcAddressGL("glClearBufferfv"));
	__glewClearBufferiv = PCast<PFNGLCLEARBUFFERIVPROC>(GetProcAddressGL("glClearBufferiv"));
	__glewClearBufferfi = PCast<PFNGLCLEARBUFFERFIPROC>(GetProcAddressGL("glClearBufferfi"));

	// VertexBuffer Object Extension
	__glewIsBuffer = PCast<PFNGLISBUFFERPROC>(GetProcAddressGL("glIsBuffer"));
	__glewBindBuffer = PCast<PFNGLBINDBUFFERPROC>(GetProcAddressGL("glBindBuffer"));
	__glewDeleteBuffers = PCast<PFNGLDELETEBUFFERSPROC>(GetProcAddressGL("glDeleteBuffers"));
	__glewGenBuffers = PCast<PFNGLGENBUFFERSPROC>(GetProcAddressGL("glGenBuffers"));
	__glewBufferData = PCast<PFNGLBUFFERDATAPROC>(GetProcAddressGL("glBufferData"));
	__glewBufferSubData = PCast<PFNGLBUFFERSUBDATAPROC>(GetProcAddressGL("glBufferSubData"));
	__glewGetBufferSubData = PCast<PFNGLGETBUFFERSUBDATAPROC>(GetProcAddressGL("glGetBufferSubData"));
	__glewMapBuffer = PCast<PFNGLMAPBUFFERPROC>(GetProcAddressGL("glMapBuffer"));
	__glewUnmapBuffer = PCast<PFNGLUNMAPBUFFERPROC>(GetProcAddressGL("glUnmapBuffer"));
	__glewMapBufferRange = PCast<PFNGLMAPBUFFERRANGEPROC>(GetProcAddressGL("glMapBufferRange"));
	__glewGetBufferParameteriv = PCast<PFNGLGETBUFFERPARAMETERIVPROC>(GetProcAddressGL("glGetBufferParameteriv"));
	__glewGetBufferPointerv = PCast<PFNGLGETBUFFERPOINTERVPROC>(GetProcAddressGL("glGetBufferPointerv"));

	// Bind Buffer Extension
	//__glewBindBufferBase = PCast<PFNGLBINDBUFFERBASEPROC>(GetProcAddressGL("glBindBufferBase"));
	//__glewBindBufferOffset = PCast<PFNGLBINDBUFFEROFFSETPROC>(GetProcAddressGL("glBindBufferOffset"));
	//__glewBindBufferRange = PCast<PFNGLBINDBUFFERRANGEPROC>(GetProcAddressGL("glBindBufferRange"));
	__glewBindBufferBase = PCast<PFNGLBINDBUFFERBASEPROC>(GetProcAddressGL("glBindBufferBase"));
	__glewBindBufferOffsetNV = PCast<PFNGLBINDBUFFEROFFSETNVPROC>(GetProcAddressGL("glBindBufferOffsetNV"));
	__glewBindBufferRange = PCast<PFNGLBINDBUFFERRANGEPROC>(GetProcAddressGL("glBindBufferRange"));

	// TranseformFeedback Buffer Extension
	__glewIsTransformFeedback = PCast<PFNGLISTRANSFORMFEEDBACKPROC>(GetProcAddressGL("glIsTransformFeedback"));
	__glewBindTransformFeedback = PCast<PFNGLBINDTRANSFORMFEEDBACKPROC>(GetProcAddressGL("glBindTransformFeedback"));
	__glewDrawTransformFeedback = PCast<PFNGLDRAWTRANSFORMFEEDBACKPROC>(GetProcAddressGL("glDrawTransformFeedback"));
	__glewDeleteTransformFeedbacks = PCast<PFNGLDELETETRANSFORMFEEDBACKSPROC>(GetProcAddressGL("glDeleteTransformFeedbacks"));
	__glewGenTransformFeedbacks = PCast<PFNGLGENTRANSFORMFEEDBACKSPROC>(GetProcAddressGL("glGenTransformFeedbacks"));
	__glewPauseTransformFeedback = PCast<PFNGLPAUSETRANSFORMFEEDBACKPROC>(GetProcAddressGL("glPauseTransformFeedback"));
	__glewResumeTransformFeedback = PCast<PFNGLRESUMETRANSFORMFEEDBACKPROC>(GetProcAddressGL("glResumeTransformFeedback"));

	//__glewBeginTransformFeedbackEXT = PCast<PFNGLBEGINTRANSFORMFEEDBACKEXTPROC>(GetProcAddressGL("glBeginTransformFeedbackEXT"));
	//__glewEndTransformFeedbackEXT = PCast<PFNGLENDTRANSFORMFEEDBACKEXTPROC>(GetProcAddressGL("glEndTransformFeedbackEXT"));
	//__glewGetTransformFeedbackVaryingEXT = PCast<PFNGLGETTRANSFORMFEEDBACKVARYINGEXTPROC>(GetProcAddressGL("glGetTransformFeedbackVaryingEXT"));
	//__glewTransformFeedbackVaryingsEXT = PCast<PFNGLTRANSFORMFEEDBACKVARYINGSEXTPROC>(GetProcAddressGL("glTransformFeedbackVaryingsEXT"));
	__glewBeginTransformFeedback = PCast<PFNGLBEGINTRANSFORMFEEDBACKPROC>(GetProcAddressGL("glBeginTransformFeedback"));
	__glewEndTransformFeedback = PCast<PFNGLENDTRANSFORMFEEDBACKPROC>(GetProcAddressGL("glEndTransformFeedback"));
	__glewGetTransformFeedbackVarying = PCast<PFNGLGETTRANSFORMFEEDBACKVARYINGPROC>(GetProcAddressGL("glGetTransformFeedbackVarying"));
	__glewTransformFeedbackVaryings = PCast<PFNGLTRANSFORMFEEDBACKVARYINGSPROC>(GetProcAddressGL("glTransformFeedbackVaryings"));

	// Draw Buffers Extension
	__glewDrawBuffers = PCast<PFNGLDRAWBUFFERSPROC>(GetProcAddressGL("glDrawBuffers"));

	// Draw Arrays Extension
	//__glewDrawArraysEXT = PCast<PFNGLDRAWARRAYSEXTPROC>(GetProcAddressGL("glDrawArraysEXT"));
	__glewDrawArraysInstancedBaseInstance = PCast<PFNGLDRAWARRAYSINSTANCEDBASEINSTANCEPROC>(GetProcAddressGL("glDrawArraysInstancedBaseInstance"));
	__glewDrawArraysIndirect = PCast<PFNGLDRAWARRAYSINDIRECTPROC>(GetProcAddressGL("glDrawArraysIndirect"));
	__glewMultiDrawArrays = PCast<PFNGLMULTIDRAWARRAYSPROC>(GetProcAddressGL("glMultiDrawArrays"));
	__glewMultiDrawArraysIndirectCount = PCast<PFNGLMULTIDRAWARRAYSINDIRECTCOUNTPROC>(GetProcAddressGL("glMultiDrawArraysIndirectCount"));

	// Draw Elements Extension
	__glewDrawElementsBaseVertex = PCast<PFNGLDRAWELEMENTSBASEVERTEXPROC>(GetProcAddressGL("glDrawElementsBaseVertex"));
	__glewDrawElementsInstancedBaseVertexBaseInstance = PCast<PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXBASEINSTANCEPROC>(GetProcAddressGL("glDrawElementsInstancedBaseVertexBaseInstance"));
	__glewDrawElementsIndirect = PCast<PFNGLDRAWELEMENTSINDIRECTPROC>(GetProcAddressGL("glDrawElementsIndirect"));
	__glewDrawRangeElementsBaseVertex = PCast<PFNGLDRAWRANGEELEMENTSBASEVERTEXPROC>(GetProcAddressGL("glDrawRangeElementsBaseVertex"));
	//__glewMultiDrawElementsBaseVertex = PCast<PFNGLMULTIDRAWELEMENTSBASEVERTEXPROC>(GetProcAddressGL("glMultiDrawElementsBaseVertex"));
	__glewMultiDrawElementsBaseVertex = PCast<PFNGLMULTIDRAWELEMENTSBASEVERTEXPROC>(GetProcAddressGL("glMultiDrawElementsBaseVertex"));
	__glewMultiDrawElementsIndirectCount = PCast<PFNGLMULTIDRAWELEMENTSINDIRECTCOUNTPROC>(GetProcAddressGL("glMultiDrawElementsIndirectCount"));

	// Texture Extension
	//__glewIsTextureEXT = PCast<PFNGLISTEXTUREEXTPROC>(GetProcAddressGL("glIsTextureEXT"));
	//__glewBindTextureEXT = PCast<PFNGLBINDTEXTUREEXTPROC>(GetProcAddressGL("glBindTextureEXT"));
	//__glewDeleteTexturesEXT = PCast<PFNGLDELETETEXTURESEXTPROC>(GetProcAddressGL("glDeleteTexturesEXT"));
	//__glewGenTexturesEXT = PCast<PFNGLGENTEXTURESEXTPROC>(GetProcAddressGL("glGenTexturesEXT"));

	__glewTexImage3D = PCast<PFNGLTEXIMAGE3DPROC>(GetProcAddressGL("glTexImage3D"));

	__glewTexImage2DMultisample = PCast<PFNGLTEXIMAGE2DMULTISAMPLEPROC>(GetProcAddressGL("glTexImage2DMultisample"));
	__glewTexImage3DMultisample = PCast<PFNGLTEXIMAGE3DMULTISAMPLEPROC>(GetProcAddressGL("glTexImage3DMultisample"));

	__glewTexStorage1D = PCast<PFNGLTEXSTORAGE1DPROC>(GetProcAddressGL("glTexStorage1D"));
	__glewTexStorage2D = PCast<PFNGLTEXSTORAGE2DPROC>(GetProcAddressGL("glTexStorage2D"));
	__glewTexStorage3D = PCast<PFNGLTEXSTORAGE3DPROC>(GetProcAddressGL("glTexStorage3D"));
	__glewTexStorage2DMultisample = PCast<PFNGLTEXSTORAGE2DMULTISAMPLEPROC>(GetProcAddressGL("glTexStorage2DMultisample"));
	__glewTexStorage3DMultisample = PCast<PFNGLTEXSTORAGE3DMULTISAMPLEPROC>(GetProcAddressGL("glTexStorage3DMultisample"));

	//__glewTexSubImage1DEXT = PCast<PFNGLTEXSUBIMAGE1DEXTPROC>(GetProcAddressGL("glTexSubImage1DEXT"));
	//__glewTexSubImage2DEXT = PCast<PFNGLTEXSUBIMAGE2DEXTPROC>(GetProcAddressGL("glTexSubImage2DEXT"));
	//__glewTexSubImage3DEXT = PCast<PFNGLTEXSUBIMAGE3DEXTPROC>(GetProcAddressGL("glTexSubImage3DEXT"));
	__glewTexSubImage3D = PCast<PFNGLTEXSUBIMAGE3DPROC>(GetProcAddressGL("glTexSubImage3D"));

	__glewActiveTexture = PCast<PFNGLACTIVETEXTUREPROC>(GetProcAddressGL("glActiveTexture"));
	__glewClientActiveTexture = PCast<PFNGLCLIENTACTIVETEXTUREPROC>(GetProcAddressGL("glClientActiveTexture"));

	// Sampler Extension
	__glewIsSampler = PCast<PFNGLISSAMPLERPROC>(GetProcAddressGL("glIsSampler"));
	__glewBindSampler = PCast<PFNGLBINDSAMPLERPROC>(GetProcAddressGL("glBindSampler"));
	__glewDeleteSamplers = PCast<PFNGLDELETESAMPLERSPROC>(GetProcAddressGL("glDeleteSamplers"));
	__glewGenSamplers = PCast<PFNGLGENSAMPLERSPROC>(GetProcAddressGL("glGenSamplers"));

	__glewSamplerParameteri = PCast<PFNGLSAMPLERPARAMETERIPROC>(GetProcAddressGL("glSamplerParameteri"));
	__glewSamplerParameteriv = PCast<PFNGLSAMPLERPARAMETERIVPROC>(GetProcAddressGL("glSamplerParameteriv"));
	__glewSamplerParameterf = PCast<PFNGLSAMPLERPARAMETERFPROC>(GetProcAddressGL("glSamplerParameterf"));
	__glewSamplerParameterfv = PCast<PFNGLSAMPLERPARAMETERFVPROC>(GetProcAddressGL("glSamplerParameterfv"));
	__glewSamplerParameterIiv = PCast<PFNGLSAMPLERPARAMETERIIVPROC>(GetProcAddressGL("glSamplerParameterIiv"));
	__glewSamplerParameterIuiv = PCast<PFNGLSAMPLERPARAMETERIUIVPROC>(GetProcAddressGL("glSamplerParameterIuiv"));

	__glewGetSamplerParameteriv = PCast<PFNGLGETSAMPLERPARAMETERIVPROC>(GetProcAddressGL("glGetSamplerParameteriv"));
	__glewGetSamplerParameterfv = PCast<PFNGLGETSAMPLERPARAMETERFVPROC>(GetProcAddressGL("glGetSamplerParameterfv"));
	__glewGetSamplerParameterIiv = PCast<PFNGLGETSAMPLERPARAMETERIIVPROC>(GetProcAddressGL("glGetSamplerParameterIiv"));
	__glewGetSamplerParameterIuiv = PCast<PFNGLGETSAMPLERPARAMETERIUIVPROC>(GetProcAddressGL("glGetSamplerParameterIuiv"));

	// Viewport Extension
	__glewViewportArrayv = PCast<PFNGLVIEWPORTARRAYVPROC>(GetProcAddressGL("glViewportArrayv"));
	__glewViewportIndexedf = PCast<PFNGLVIEWPORTINDEXEDFPROC>(GetProcAddressGL("glViewportIndexedf"));
	__glewViewportIndexedfv = PCast<PFNGLVIEWPORTINDEXEDFVPROC>(GetProcAddressGL("glViewportIndexedfv"));

	// Scissor Extension
	__glewScissorArrayv = PCast<PFNGLSCISSORARRAYVPROC>(GetProcAddressGL("glScissorArrayv"));
	__glewScissorIndexed = PCast<PFNGLSCISSORINDEXEDPROC>(GetProcAddressGL("glScissorIndexed"));
	__glewScissorIndexedv = PCast<PFNGLSCISSORINDEXEDVPROC>(GetProcAddressGL("glScissorIndexedv"));

	// rasterizer Extension
	__glewDepthRangef = PCast<PFNGLDEPTHRANGEFPROC>(GetProcAddressGL("glDepthRangef"));
	__glewDepthRangeArrayv = PCast<PFNGLDEPTHRANGEARRAYVPROC>(GetProcAddressGL("glDepthRangeArrayv"));
	__glewDepthRangeIndexed = PCast<PFNGLDEPTHRANGEINDEXEDPROC>(GetProcAddressGL("glDepthRangeIndexed"));
	__glewClipControl = PCast<PFNGLCLIPCONTROLPROC>(GetProcAddressGL("glClipControl"));

	// Blend Extension
	__glewClampColor = PCast<PFNGLCLAMPCOLORPROC>(GetProcAddressGL("glClampColor"));
	__glewBlendFuncSeparate = PCast<PFNGLBLENDFUNCSEPARATEPROC>(GetProcAddressGL("glBlendFuncSeparate"));
	__glewBlendEquationSeparate = PCast<PFNGLBLENDEQUATIONSEPARATEPROC>(GetProcAddressGL("glBlendEquationSeparate"));

	__glewBlendFuncSeparatei = PCast<PFNGLBLENDFUNCSEPARATEIPROC>(GetProcAddressGL("glBlendFuncSeparatei"));
	__glewBlendEquationSeparatei = PCast<PFNGLBLENDEQUATIONSEPARATEIPROC>(GetProcAddressGL("glBlendEquationSeparatei"));
	__glewColorMaski = PCast<PFNGLCOLORMASKIPROC>(GetProcAddressGL("glColorMaski"));
	__glewIsEnabledi = PCast<PFNGLISENABLEDIPROC>(GetProcAddressGL("glIsEnabledi"));
	__glewEnablei = PCast<PFNGLENABLEIPROC>(GetProcAddressGL("glEnablei"));
	__glewDisablei = PCast<PFNGLDISABLEIPROC>(GetProcAddressGL("glDisablei"));

	// DepthStencil Extension
	__glewStencilFuncSeparate = PCast<PFNGLSTENCILFUNCSEPARATEPROC>(GetProcAddressGL("glStencilFuncSeparate"));
	__glewStencilOpSeparate = PCast<PFNGLSTENCILOPSEPARATEPROC>(GetProcAddressGL("glStencilOpSeparate"));
	__glewStencilMaskSeparate = PCast<PFNGLSTENCILMASKSEPARATEPROC>(GetProcAddressGL("glStencilMaskSeparate"));


	// Shader Extension
	__glewIsShader = PCast<PFNGLISSHADERPROC>(GetProcAddressGL("glIsShader"));
	__glewAttachShader = PCast<PFNGLATTACHSHADERPROC>(GetProcAddressGL("glAttachShader"));
	__glewDetachShader = PCast<PFNGLDETACHSHADERPROC>(GetProcAddressGL("glDetachShader"));
	__glewDeleteShader = PCast<PFNGLDELETESHADERPROC>(GetProcAddressGL("glDeleteShader"));
	__glewCreateShader = PCast<PFNGLCREATESHADERPROC>(GetProcAddressGL("glCreateShader"));
	__glewCompileShader = PCast<PFNGLCOMPILESHADERPROC>(GetProcAddressGL("glCompileShader"));
	__glewShaderSource = PCast<PFNGLSHADERSOURCEPROC>(GetProcAddressGL("glShaderSource"));
	__glewShaderBinary = PCast<PFNGLSHADERBINARYPROC>(GetProcAddressGL("glShaderBinary"));
	__glewSpecializeShader = PCast<PFNGLSPECIALIZESHADERPROC>(GetProcAddressGL("glSpecializeShader"));

	__glewIsProgram = PCast<PFNGLISPROGRAMPROC>(GetProcAddressGL("glIsProgram"));
	__glewUseProgram = PCast<PFNGLUSEPROGRAMPROC>(GetProcAddressGL("glUseProgram"));
	__glewDeleteProgram = PCast<PFNGLDELETEPROGRAMPROC>(GetProcAddressGL("glDeleteProgram"));
	__glewCreateProgram = PCast<PFNGLCREATEPROGRAMPROC>(GetProcAddressGL("glCreateProgram"));
	__glewLinkProgram = PCast<PFNGLLINKPROGRAMPROC>(GetProcAddressGL("glLinkProgram"));

	__glewEnableVertexAttribArray = PCast<PFNGLENABLEVERTEXATTRIBARRAYPROC>(GetProcAddressGL("glEnableVertexAttribArray"));
	__glewDisableVertexAttribArray = PCast<PFNGLDISABLEVERTEXATTRIBARRAYPROC>(GetProcAddressGL("glDisableVertexAttribArray"));
	__glewVertexAttribPointer = PCast<PFNGLVERTEXATTRIBPOINTERPROC>(GetProcAddressGL("glVertexAttribPointer"));
	__glewBindAttribLocation = PCast<PFNGLBINDATTRIBLOCATIONPROC>(GetProcAddressGL("glBindAttribLocation"));
	__glewBindFragDataLocation = PCast<PFNGLBINDFRAGDATALOCATIONPROC>(GetProcAddressGL("glBindFragDataLocation"));
	__glewUniformBlockBinding = PCast<PFNGLUNIFORMBLOCKBINDINGPROC>(GetProcAddressGL("glUniformBlockBinding"));
	__glewGetActiveAttrib = PCast<PFNGLGETACTIVEATTRIBPROC>(GetProcAddressGL("glGetActiveAttrib"));
	__glewGetActiveUniform = PCast<PFNGLGETACTIVEUNIFORMPROC>(GetProcAddressGL("glGetActiveUniform"));
	__glewGetActiveUniformName = PCast<PFNGLGETACTIVEUNIFORMNAMEPROC>(GetProcAddressGL("glGetActiveUniformName"));
	__glewGetActiveUniformBlockiv = PCast<PFNGLGETACTIVEUNIFORMBLOCKIVPROC>(GetProcAddressGL("glGetActiveUniformBlockiv"));
	__glewGetActiveUniformBlockName = PCast<PFNGLGETACTIVEUNIFORMBLOCKNAMEPROC>(GetProcAddressGL("glGetActiveUniformBlockName"));
	__glewGetAttachedShaders = PCast<PFNGLGETATTACHEDSHADERSPROC>(GetProcAddressGL("glGetAttachedShaders"));
	__glewGetAttribLocation = PCast<PFNGLGETATTRIBLOCATIONPROC>(GetProcAddressGL("glGetAttribLocation"));
	__glewGetUniformLocation = PCast<PFNGLGETUNIFORMLOCATIONPROC>(GetProcAddressGL("glGetUniformLocation"));
	__glewGetUniformfv = PCast<PFNGLGETUNIFORMFVPROC>(GetProcAddressGL("glGetUniformfv"));
	__glewGetUniformiv = PCast<PFNGLGETUNIFORMIVPROC>(GetProcAddressGL("glGetUniformiv"));
	__glewGetUniformIndices = PCast<PFNGLGETUNIFORMINDICESPROC>(GetProcAddressGL("glGetUniformIndices"));
	__glewGetUniformBlockIndex = PCast<PFNGLGETUNIFORMBLOCKINDEXPROC>(GetProcAddressGL("glGetUniformBlockIndex"));
	__glewGetVertexAttribdv = PCast<PFNGLGETVERTEXATTRIBDVPROC>(GetProcAddressGL("glGetVertexAttribdv"));
	__glewGetVertexAttribfv = PCast<PFNGLGETVERTEXATTRIBFVPROC>(GetProcAddressGL("glGetVertexAttribfv"));
	__glewGetVertexAttribiv = PCast<PFNGLGETVERTEXATTRIBIVPROC>(GetProcAddressGL("glGetVertexAttribiv"));
	__glewGetVertexAttribPointerv = PCast<PFNGLGETVERTEXATTRIBPOINTERVPROC>(GetProcAddressGL("glGetVertexAttribPointerv"));
	__glewGetShaderiv = PCast<PFNGLGETSHADERIVPROC>(GetProcAddressGL("glGetShaderiv"));
	__glewGetProgramiv = PCast<PFNGLGETPROGRAMIVPROC>(GetProcAddressGL("glGetProgramiv"));
	__glewGetProgramInfoLog = PCast<PFNGLGETPROGRAMINFOLOGPROC>(GetProcAddressGL("glGetProgramInfoLog"));
	__glewGetShaderSource = PCast<PFNGLGETSHADERSOURCEPROC>(GetProcAddressGL("glGetShaderSource"));

	__glewIsProgramPipeline = PCast<PFNGLISPROGRAMPIPELINEPROC>(GetProcAddressGL("glIsProgramPipeline"));
	__glewBindProgramPipeline = PCast<PFNGLBINDPROGRAMPIPELINEPROC>(GetProcAddressGL("glBindProgramPipeline"));
	__glewDeleteProgramPipelines = PCast<PFNGLDELETEPROGRAMPIPELINESPROC>(GetProcAddressGL("glDeleteProgramPipelines"));
	__glewGenProgramPipelines = PCast<PFNGLGENPROGRAMPIPELINESPROC>(GetProcAddressGL("glGenProgramPipelines"));
	__glewActiveShaderProgram = PCast<PFNGLDISABLEIEXTPROC>(GetProcAddressGL("glActiveShaderProgram"));
	__glewCreateShaderProgramv = PCast<PFNGLCREATESHADERPROGRAMVPROC>(GetProcAddressGL("glCreateShaderProgramv"));
	__glewUseProgramStages = PCast<PFNGLUSEPROGRAMSTAGESPROC>(GetProcAddressGL("glUseProgramStages"));
	__glewProgramParameteri = PCast<PFNGLPROGRAMPARAMETERIPROC>(GetProcAddressGL("glProgramParameteri"));
	__glewValidateProgramPipeline = PCast<PFNGLVALIDATEPROGRAMPIPELINEPROC>(GetProcAddressGL("glValidateProgramPipeline"));
	__glewGetProgramPipelineiv = PCast<PFNGLGETPROGRAMPIPELINEIVPROC>(GetProcAddressGL("glGetProgramPipelineiv"));
	__glewGetProgramPipelineInfoLog = PCast<PFNGLGETPROGRAMPIPELINEINFOLOGPROC>(GetProcAddressGL("glGetProgramPipelineInfoLog"));


	// Query Objects Extension
	__glewIsQuery = PCast<PFNGLISQUERYPROC>(GetProcAddressGL("glIsQuery"));
	__glewBeginQuery = PCast<PFNGLBEGINQUERYPROC>(GetProcAddressGL("glBeginQuery"));
	__glewEndQuery = PCast<PFNGLENDQUERYPROC>(GetProcAddressGL("glEndQuery"));
	__glewDeleteQueries = PCast<PFNGLDELETEQUERIESPROC>(GetProcAddressGL("glDeleteQueries"));
	__glewGenQueries = PCast<PFNGLGENQUERIESPROC>(GetProcAddressGL("glGenQueries"));
	__glewGetQueryObjectiv = PCast<PFNGLGETQUERYOBJECTIVPROC>(GetProcAddressGL("glGetQueryObjectiv"));
	__glewGetQueryObjectuiv = PCast<PFNGLGETQUERYOBJECTUIVPROC>(GetProcAddressGL("glGetQueryObjectuiv"));
	__glewGetQueryiv = PCast<PFNGLGETQUERYIVPROC>(GetProcAddressGL("glGetQueryiv"));
	__glewGetQueryObjecti64v = PCast<PFNGLGETQUERYOBJECTI64VPROC>(GetProcAddressGL("glGetQueryObjecti64v"));
	__glewGetQueryObjectui64v = PCast<PFNGLGETQUERYOBJECTUI64VPROC>(GetProcAddressGL("glGetQueryObjectui64v"));

	// Query Counter
	__glewQueryCounter = PCast<PFNGLQUERYCOUNTERPROC>(GetProcAddressGL("glQueryCounter"));

	// ConditionalRender
	__glewBeginConditionalRender = PCast<PFNGLBEGINCONDITIONALRENDERPROC>(GetProcAddressGL("glBeginConditionalRender"));
	__glewEndConditionalRender = PCast<PFNGLENDCONDITIONALRENDERPROC>(GetProcAddressGL("glEndConditionalRender"));

	// String Extension
	__glewGetStringi = PCast<PFNGLGETSTRINGIPROC>(GetProcAddressGL("glGetStringi"));

	// Debug Extension
	__glewDebugMessageCallback = PCast<PFNGLDEBUGMESSAGECALLBACKPROC>(GetProcAddressGL("glDebugMessageCallback"));
	__glewDebugMessageControl = PCast<PFNGLDEBUGMESSAGECONTROLPROC>(GetProcAddressGL("glDebugMessageControl"));
	__glewDebugMessageInsert = PCast<PFNGLDEBUGMESSAGEINSERTPROC>(GetProcAddressGL("glDebugMessageInsert"));
	__glewGetDebugMessageLog = PCast<PFNGLGETDEBUGMESSAGELOGPROC>(GetProcAddressGL("glGetDebugMessageLog"));

	__glewPushDebugGroup = PCast<PFNGLPUSHDEBUGGROUPPROC>(GetProcAddressGL("glPushDebugGroup"));
	__glewPopDebugGroup = PCast<PFNGLPOPDEBUGGROUPPROC>(GetProcAddressGL("glPopDebugGroup"));
	__glewGetObjectLabel = PCast<PFNGLGETOBJECTLABELPROC>(GetProcAddressGL("glGetObjectLabel"));
	__glewGetObjectPtrLabel = PCast<PFNGLGETOBJECTPTRLABELPROC>(GetProcAddressGL("glGetObjectPtrLabel"));
	__glewObjectLabel = PCast<PFNGLOBJECTLABELPROC>(GetProcAddressGL("glObjectLabel"));
	__glewObjectPtrLabel = PCast<PFNGLOBJECTPTRLABELPROC>(GetProcAddressGL("glObjectPtrLabel"));

	//__glewDebugMessageCallbackAMD = PCast<PFNGLDEBUGMESSAGECALLBACKAMDPROC>(GetProcAddressGL("glDebugMessageCallbackAMD"));
	//__glewDebugMessageEnableAMD = PCast<PFNGLDEBUGMESSAGEENABLEAMDPROC>(GetProcAddressGL("glDebugMessageEnableAMD"));
	//__glewDebugMessageInsertAMD = PCast<PFNGLDEBUGMESSAGEINSERTAMDPROC>(GetProcAddressGL("glDebugMessageInsertAMD"));
	//__glewGetDebugMessageLogAMD = PCast<PFNGLGETDEBUGMESSAGELOGAMDPROC>(GetProcAddressGL("glGetDebugMessageLogAMD"));


	// WGL Extension
	__wglewChoosePixelFormatARB = PCast<PFNWGLCHOOSEPIXELFORMATARBPROC>(GetProcAddressGL("wglChoosePixelFormatARB"));
	__wglewGetPixelFormatAttribfvARB = PCast<PFNWGLGETPIXELFORMATATTRIBFVARBPROC>(GetProcAddressGL("wglGetPixelFormatAttribfvARB"));
	__wglewGetPixelFormatAttribivARB = PCast<PFNWGLGETPIXELFORMATATTRIBIVARBPROC>(GetProcAddressGL("wglGetPixelFormatAttribivARB"));
	__wglewCreateContextAttribsARB = PCast<PFNWGLCREATECONTEXTATTRIBSARBPROC>(GetProcAddressGL("wglCreateContextAttribsARB"));

	__wglewSwapIntervalEXT = PCast<PFNWGLSWAPINTERVALEXTPROC>(GetProcAddressGL("wglSwapIntervalEXT"));
	__wglewGetSwapIntervalEXT = PCast<PFNWGLGETSWAPINTERVALEXTPROC>(GetProcAddressGL("wglGetSwapIntervalEXT"));

#elif defined(USE_OPNEGL_PROC_GL_POISON)


#endif
	CloseLibgl();
	return true;
}



///-------------------------------------------------------------------------------------------------
/// 拡張機能チェックコールバック
void WINAPI	DebugCallback(
	GLenum _source​,
	GLenum _type​,
	GLuint _id​,
	GLenum _severity​,
	GLsizei _length​,
	const GLchar* _message​,
	const GLvoid* _userParam​
	)
{
	static const c8* kSourceStrings[] =
	{
		"OpenGL API",
		"Window System",
		"Shader Compiler",
		"Third Party",
		"Application",
		"Other",
	};
	u32 sourceNo = (GL_DEBUG_SOURCE_API <= _source​ && _source​ <= GL_DEBUG_SOURCE_OTHER) ?
		(_source​ - GL_DEBUG_SOURCE_API) : (GL_DEBUG_SOURCE_OTHER - GL_DEBUG_SOURCE_API);
	libAssert(sourceNo < ARRAYOF(kSourceStrings));

	static const c8* kTypeStrings[] =
	{
		"Error",
		"Deprecated behavior",
		"Undefined behavior",
		"Portability",
		"Performance",
		"Other",
	};
	u32 typeNo = (GL_DEBUG_TYPE_ERROR <= _type​ && _type​ <= GL_DEBUG_TYPE_OTHER) ?
		(_type​ - GL_DEBUG_TYPE_ERROR) : (GL_DEBUG_TYPE_OTHER - GL_DEBUG_TYPE_ERROR);
	libAssert(typeNo < ARRAYOF(kTypeStrings));

	static const c8* kSeverityStrings[] =
	{
		"High",
		"Medium",
		"Low",
	};
	u32 severityNo = (GL_DEBUG_SEVERITY_HIGH <= _type​ && _type​ <= GL_DEBUG_SEVERITY_LOW) ?
		(_type​ - GL_DEBUG_SEVERITY_HIGH) : (GL_DEBUG_SEVERITY_LOW - GL_DEBUG_SEVERITY_HIGH);
	libAssert(severityNo < ARRAYOF(kSeverityStrings));

	// とりあえずエラーの時だけプリント
	if (typeNo == 0)
	{
		PRINT("Source : %s    Type : %s    ID : %d    Severity : %s    Message : %s\n",
			kSourceStrings[sourceNo], kTypeStrings[typeNo], _id​, kSeverityStrings[severityNo], _message​);
	}
}

///-------------------------------------------------------------------------------------------------
/// 拡張機能チェックコールバックAMD
void WINAPI	DebugCallbackAMD(GLuint id,
	GLenum _category,
	GLenum _severity,
	GLsizei _length,
	const GLchar* _message,
	GLvoid* _userParam
	)
{

}

///-------------------------------------------------------------------------------------------------
/// OpenGLデバッグチェックコールバック設定
void	CGfxUtilityGL::SetupDebugCallback()
{
	::glDebugMessageCallback(DebugCallback, NULL);
	//::glDebugMessageCallbackAMD(DebugCallbackAMD, NULL);
}


///-------------------------------------------------------------------------------------------------
/// デフォルトテクスチャパラメータ設定
/// ※glBindTextureで囲って呼ぶこと
b8		CGfxUtilityGL::SetDefaultTexParameter(u32 _target, GFX_IMAGE_FORMAT _format, u32 _mipLevel)
{
	u32 glTexFormat;
	CGfxFormatUtilityGL::ConvertFormatToTexFormat(glTexFormat, _format);
	// デプスフォーマットの場合
	if (glTexFormat == GL_DEPTH_STENCIL || glTexFormat == GL_DEPTH_COMPONENT)
	{
		::glTexParameteri(_target, GL_DEPTH_STENCIL_TEXTURE_MODE, glTexFormat);
	}
	else
	{
		::glTexParameteri(_target, GL_DEPTH_STENCIL_TEXTURE_MODE, GL_NONE);
	}
	// 比較式設定
	//::glTexParameteri(_target, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
	//::glTexParameteri(_target, GL_TEXTURE_COMPARE_FUNC, GL_ALWAYS);
	::glTexParameteri(_target, GL_TEXTURE_COMPARE_MODE, GL_NONE);
	::glTexParameteri(_target, GL_TEXTURE_COMPARE_FUNC, GL_NONE);
	// LODバイアス・最小値・最大値
	::glTexParameterf(_target, GL_TEXTURE_LOD_BIAS, 0.0f);
	::glTexParameterf(_target, GL_TEXTURE_MIN_LOD, 0.0f);
	::glTexParameterf(_target, GL_TEXTURE_MAX_LOD, 1000.0f);
	// LODフィルター
	::glTexParameteri(_target, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	::glTexParameteri(_target, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	// mipmapレベル指定
	::glTexParameteri(_target, GL_TEXTURE_BASE_LEVEL, 0);
	::glTexParameteri(_target, GL_TEXTURE_MAX_LEVEL, _mipLevel - 1);
	// ラップ指定
	::glTexParameteri(_target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	::glTexParameteri(_target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	::glTexParameteri(_target, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	// ボーダーカラー指定
	f32 clr[] = { 0.0f, 0.0f, 0.0f, 0.0f };
	::glTexParameterfv(_target, GL_TEXTURE_BORDER_COLOR, clr);

	return true;
}


POISON_END

#endif // LIB_GFX_OPENGL

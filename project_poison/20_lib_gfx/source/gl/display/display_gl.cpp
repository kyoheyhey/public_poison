﻿
#ifdef LIB_GFX_OPENGL

///-------------------------------------------------------------------------------------------------
/// define
//#define	USE_SWAP_BUFFER


///-------------------------------------------------------------------------------------------------
// include
#include "display/display.h"

#include "device/device_utility.h"
#include "gl/device/win/device_gl.h"

#include "format/gfx_format_utility.h"
#include "gl/format/gfx_format_utility_gl.h"

#include "object/color_buffer.h"
#include "object/depth_buffer.h"
#include "object/texture_buffer.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
// static


///*************************************************************************************************
///	ディスプレイ
///*************************************************************************************************

CDisplay::CDisplay()
	: m_refreshRate(60)
{
}
CDisplay::~CDisplay()
{
}


///-------------------------------------------------------------------------------------------------
/// バインドするかどうか
b8		CDisplay::IsBind() const
{
	return false;
}


///-------------------------------------------------------------------------------------------------
///	スワップバッファ生成
b8		CDisplay::CreateSwap(CColorBuffer* _pRetClrBuf, u16 _width, u16 _height, const COLOR_DESC& _clrDesc, u32 _swapNum, u32 _refreshRate)
{
	return true;
}


///-------------------------------------------------------------------------------------------------
///	生成
b8		CDisplay::Create(const DISPLAY_DESC& _desc)
{
	libAssert(_desc.swapNum <= MAX_RT_NUM);

	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	HDC& pDC = pDevice->GetDevice();
	libAssert(pDC);

	// 指定がなければクライアントサイズで設定
	u32 width = _desc.width;
	u32 height = _desc.height;
	// OpenGLの場合強制的にウィンドウに合わせる
	//if (width <= 0 && height <= 0)
	{
		pDevice->GetClientSize(width, height);
	}

	// フォーマットフラグ
	u32 flags = (PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_SWAP_LAYER_BUFFERS);
	// カラービットサイズ
	u32 colorBitSize = CGfxFormatUtility::GetImageFormatSize(_desc.color.format);
	// デプス・ステンシルビットサイズ
	u32 depthBitSize = 0, stencilBitSize = 0;
	if (_desc.pDepth)
	{
		CGfxFormatUtility::GetDepthStencilFormat(depthBitSize, stencilBitSize, _desc.pDepth->format);
	}
	else
	{
		flags |= PFD_DEPTH_DONTCARE;	// デプスなし
	}

	// ピクセルフォーマット初期化
	::PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(::PIXELFORMATDESCRIPTOR),
		1,							// Version number 
		flags,						// Flags
		PFD_TYPE_RGBA,				// The kind of framebuffer. RGBA or palette.
		SCast<BYTE>(colorBitSize),	// Colordepth of the framebuffer.
		0, 0, 0, 0, 0, 0, 0, 0,
		0,							// Number of bits for the accumulationbuffer（よくわからん）
		0, 0, 0, 0,
		SCast<BYTE>(depthBitSize),	// Number of bits for the depthbuffer
		SCast<BYTE>(stencilBitSize),// Number of bits for the stencilbuffer
		0,							// Number of Aux buffers in the framebuffer.（使用されないっぽい）
		0,							// レイヤー指定（使用されないっぽい）
		0,							// オーバーレイ、アンダーレイのプレーン数を指定（よくわからん）
		0,							// レイヤーマスク指定（使用されないっぽい）
		0,							// アンダーレイプレーンの透過色又はインデックス指定（よくわからん）
		0							// よくわからん（使用されないっぽい）
	};

	// フォーマット自動選定
	int format = ::ChoosePixelFormat(pDC, &pfd);
	if (format == 0)
	{
		// 該当するピクセルフォーマットが無い
		PRINT("************[Error] Failed : Not Find Pixel Format\n");
		return false;
	}

	// OpenGLが使うデバイスコンテキストに指定のピクセルフォーマットをセット
	if (!::SetPixelFormat(pDC, format, &pfd))
	{
		// DCへフォーマットを設定するのに失敗
		PRINT("************[Error] Failed : ::SetPixelFormat()\n");
		return false;
	}


	strcpy_s(m_name, _desc.name);
	m_memHdl = _desc.memHdl;
	m_colorNum = _desc.swapNum;
	m_depthNum = _desc.pDepth ? 1 : 0;
	m_width = width;
	m_height = height;
	m_refreshRate = _desc.refreshRate;
	return true;
}

///-------------------------------------------------------------------------------------------------
///	破棄
void	CDisplay::Release()
{
	MEM_SAFE_FREE(m_memHdl, m_pColor);
	MEM_SAFE_FREE(m_memHdl, m_pDepth);
	MEM_SAFE_FREE(m_memHdl, m_pClrTex);
	MEM_SAFE_FREE(m_memHdl, m_pDepTex);
	*this = CDisplay();
}

///-------------------------------------------------------------------------------------------------
///	ターゲットのリサイズ
b8		CDisplay::ResizeTarget(u16 _width, u16 _height)
{
	// 変更なし
	if (GetWidth() == _width && GetHeight() == _height) { return true; }

	m_width = _width;
	m_height = _height;

	return true;
}

///-------------------------------------------------------------------------------------------------
///	垂直同期
b8		CDisplay::WaitVsync(u32 _syncInterval)
{
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	HDC& pDC = pDevice->GetDevice();
	libAssert(pDC);

	b8 bRet = true;
	bRet &= (::wglSwapLayerBuffers(pDC, WGL_SWAP_MAIN_PLANE) == TRUE);
	bRet &= (::wglSwapIntervalEXT(_syncInterval) == TRUE);
	return bRet;
}

///-------------------------------------------------------------------------------------------------
///	バインド
//void	CDisplay::Bind() const
//{
//	glBindFramebufferEXT(GL_FRAMEBUFFER, m_bufferObject);
//}

///-------------------------------------------------------------------------------------------------
///	アンバインド
//void	CDisplay::UnBind() const
//{
//	glBindFramebufferEXT(GL_FRAMEBUFFER, NULL);
//}


POISON_END


#endif // LIB_GFX_OPENGL

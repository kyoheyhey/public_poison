﻿
#ifdef LIB_GFX_DX11

///-------------------------------------------------------------------------------------------------
/// include
#include "dx11/device/win/device_dx11.h"

#include "utility/gfx_utility.h"
#include "dx11/utility/gfx_utility_dx11.h"

#include "format/gfx_format_utility.h"
#include "dx11/format/gfx_format_utility_dx11.h"

// shader
#include "shader/shader.h"

// object
#include "object/color_buffer.h"
#include "object/depth_buffer.h"
#include "object/texture_buffer.h"
#include "object/vertex_buffer.h"

#include "object/sampler_state.h"
#include "object/rasterrizer_state.h"
#include "object/blend_state.h"
#include "object/depth_state.h"

// query
#include "query/occlusion_query.h"
#include "query/primitive_query.h"
#include "query/timer_query.h"

#if 0
// AMD
#include "amd_ags.h"
// NVIDIA
#include "nvapi.h"
#endif // 0


POISON_BGN

///*************************************************************************************************
///	グラフィックデバイスクラス
///*************************************************************************************************
CDeviceDX11::CDeviceDX11()
	: m_pDXGIFactory(NULL)
	, m_pDXGIAdapter(NULL)
	, m_pDXGIDevice(NULL)
	, m_pD3D11Device(NULL)
	, m_pD3D11ImmediateContext(NULL)
	, m_pD3D11Device1(NULL)
	, m_pD3D11ImmediateContext1(NULL)
	, m_pD3D11Device2(NULL)
	, m_pD3D11ImmediateContext2(NULL)
	, m_pD3D11Device3(NULL)
	, m_pD3D11ImmediateContext3(NULL)
	, m_pColor(NULL)
	, m_pDepth(NULL)
{
}
CDeviceDX11::~CDeviceDX11()
{
}

///-------------------------------------------------------------------------------------------------
///	初期化開始
b8		CDeviceDX11::InitBegin(MEM_HANDLE _memHdl)
{
	// 基底クラスたちの初期化
	if (CDeviceWin::InitBegin(_memHdl) == false){ return false; }

	// GPUメモリチェック
	CheckGPU();

	// DirectX11の初期化
	return InitDX11();
}

///-------------------------------------------------------------------------------------------------
///	初期化終了
b8		CDeviceDX11::InitEnd()
{
	// 基底クラスたちの初期化
	if (CDeviceWin::InitEnd() == false){ return false; }

	// 周波数カウンター初期化
	CTimerQuery::InitFrequency();

	return true;
}

///-------------------------------------------------------------------------------------------------
///	終了
void	CDeviceDX11::Finalize()
{
	// 周波数カウンター終了
	CTimerQuery::FinFrequency();

	SAFE_RELEASE(m_object.pBuf);
	for (u32 idx = 0; idx < m_swapNum; idx++)
	{
		m_pColor[idx].Release();
	}
	if (m_pDepth)
	{
		m_pDepth->Release();
	}
	MEM_SAFE_FREE(GetMemHdl(), m_pColor);
	MEM_SAFE_FREE(GetMemHdl(), m_pDepth);

#ifndef POISON_RELEASE
#if 0
	// 描画情報取得クエリ作成
	{
		for (u32 i = 0; i < ARRAYOF(m_gpuCounter); ++i)
		{
			SAFE_RELEASE(m_gpuCounter[i]);
		}
	}
#endif // 0
#endif // !POISON_RELEASE

	SAFE_RELEASE(m_pDXGIFactory);
	SAFE_RELEASE(m_pDXGIAdapter);
	SAFE_RELEASE(m_pDXGIDevice);

	if (m_pD3D11ImmediateContext3)
	{
		m_pD3D11ImmediateContext3->ClearState();
		m_pD3D11ImmediateContext3->Release();
		m_pD3D11ImmediateContext3 = NULL;
	}
	SAFE_RELEASE(m_pD3D11Device3);

	if (m_pD3D11ImmediateContext2)
	{
		m_pD3D11ImmediateContext2->ClearState();
		m_pD3D11ImmediateContext2->Release();
		m_pD3D11ImmediateContext2 = NULL;
	}
	SAFE_RELEASE(m_pD3D11Device2);

	if (m_pD3D11ImmediateContext1)
	{
		m_pD3D11ImmediateContext1->ClearState();
		m_pD3D11ImmediateContext1->Release();
		m_pD3D11ImmediateContext1 = NULL;
	}
	SAFE_RELEASE(m_pD3D11Device1);

	if (m_pD3D11ImmediateContext)
	{
		m_pD3D11ImmediateContext->ClearState();
		m_pD3D11ImmediateContext->Release();
		m_pD3D11ImmediateContext = NULL;
	}
	SAFE_RELEASE(m_pD3D11Device);

#if 0
	if (m_isInitNvAPI)
	{
		::NvAPI_Unload();
		m_isInitNvAPI = false;
	}

	if (m_pAgsContext)
	{
		// コンテキストクリア
		::agsDeInit(m_pAgsContext);
		m_pAgsContext = NULL;
	}
#endif // 0

	// 基底クラスたちの終了
	CDeviceWin::Finalize();
}


///-------------------------------------------------------------------------------------------------
///	キャンバス生成
b8		CDeviceDX11::CreateCanvas(const CANVAS_DESC& _desc)
{
	// 指定がなければクライアントサイズで設定
	u32 width = _desc.width;
	u32 height = _desc.height;
	if (width <= 0 && height <= 0)
	{
		GetClientSize(width, height);
	}

	// レンダーバッファ生成
	CColorBuffer* pColor = NULL;
	if (_desc.swapNum > 0)
	{
		pColor = MEM_CALLOC_TYPES(GetMemHdl(), CColorBuffer, _desc.swapNum);
		libAssert(pColor);
	}
	CDepthBuffer* pDepth = NULL;
	if (_desc.depFormat != GFX_FORMAT_UNKNOWN)
	{
		pDepth = MEM_CALLOC_TYPE(GetMemHdl(), CDepthBuffer);
		libAssert(pDepth);
	}

	b8 bRet = true;
	// スワップバッファ生成
	if (pColor)
	{
		bRet &= CreateSwap(pColor, width, height, _desc.clrFormat, _desc.aa, _desc.swapNum, _desc.refreshRate);
	}
	// デプスバッファ生成
	if (pDepth)
	{
		bRet &= pDepth->Create(width, height, _desc.depFormat, _desc.aa);
	}

	if (bRet)
	{
		m_pColor = pColor;
		m_pDepth = pDepth;
		m_width = width;
		m_height = height;
		m_swapNum = _desc.swapNum;
		m_refreshRate = _desc.refreshRate;
		m_viewport.posX = m_viewport.posY = 0;
		m_viewport.width = width;
		m_viewport.height = height;
		m_clearClr = _desc.clearColor;
	}
	return bRet;
}

///-------------------------------------------------------------------------------------------------
/// キャンパスバインド
void	CDeviceDX11::BindCanvas()
{
	ID3D11DeviceContext* pContext = GetContext();
	libAssert(pContext);

	// ディスプレイバインド
	ID3D11RenderTargetView* pClrView[RENDER_BUFFER_SLOT_NUM] = {};
	ID3D11DepthStencilView* pDepView = NULL;
	for (u32 clrIdx = 0; clrIdx < m_swapNum; clrIdx++)
	{
		pClrView[clrIdx] = m_pColor[clrIdx].GetObject().pView;
	}
	if (m_pDepth)
	{
		pDepView = m_pDepth->GetObject().pView;
	}
	pContext->OMSetRenderTargets(m_swapNum, pClrView, pDepView);
}

///-------------------------------------------------------------------------------------------------
/// クリアサーフェイス
void	CDeviceDX11::ClearSurface(const CVec4& _clearClr)
{
	ID3D11DeviceContext* pContext = GetContext();
	libAssert(pContext);

	// カラーバッファクリア
	for (u32 clrIdx = 0; clrIdx < m_swapNum; clrIdx++)
	{
		ID3D11RenderTargetView* pClrView = m_pColor[clrIdx].GetObject().pView;
		pContext->ClearRenderTargetView(pClrView, _clearClr.v);
	}
	// デプスステンシルクリア
	if (m_pDepth)
	{
		ID3D11DepthStencilView* pDepView = m_pDepth->GetObject().pView;
		u32 flag = D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL;
		// クリア
		pContext->ClearDepthStencilView(pDepView, flag, CLEAR_DEPTH, CLEAR_STENCIL);
	}
}

///-------------------------------------------------------------------------------------------------
///	描画開始
void	CDeviceDX11::DrawBegin()
{
	ID3D11DeviceContext* pContext = GetContext();
	libAssert(pContext);

	// コンテキストクリア
	pContext->ClearState();

	// 周波数計測開始
	CTimerQuery::FrequencyBgn();
}

///-------------------------------------------------------------------------------------------------
///	描画終了
void	CDeviceDX11::DrawEnd()
{
	// 周波数計測終了
	CTimerQuery::FrequencyEnd();
}

///-------------------------------------------------------------------------------------------------
///	スワップバッファ
void	CDeviceDX11::SwapBuffer(const CTextureBuffer* _pTexture, SCREEN_ALIGN _align/* = SCREEN_ALIGN_CENTER_MIDDLE*/, SCREEN_STRETCH _stretch/* = SCREEN_STRETCH_FIT*/)
{
	// ビューポート更新
	UpdateScreenViewprt(_pTexture->GetWidth(), _pTexture->GetHeight(), _align, _stretch);
	const CRect& screenViewport = GetViewport();

	// はみ出し領域があるなら画面クリア
	if (screenViewport.posX > 0 || screenViewport.posY > 0 ||
		screenViewport.width < GetWidth() || screenViewport.height < GetHeight())
	{
		ClearSurface(GetClearColor());
	}

	// キャンパスバインド
	BindCanvas();

	ID3D11DeviceContext* pContext = GetContext();
	libAssert(pContext);

	// シェーダ設定
	const PassShader* pPassShader = CGfxUtility::GetCopyShader();
	libAssert(pPassShader);
	{
		const VShader* pVShader = pPassShader->GetVShader();
		const GShader* pGShader = pPassShader->GetGShader();
		const PShader* pPShader = pPassShader->GetPShader();
		if (pVShader)
		{
			// 頂点シェーダー設定
			pContext->VSSetShader(pVShader->GetObject().pShader, NULL, 0);
		}
		if (pGShader)
		{
			// ジオメトリシェーダー設定
			pContext->GSSetShader(pGShader->GetObject().pShader, NULL, 0);
		}
		if (pPShader)
		{
			// ピクセルシェーダー設定
			pContext->PSSetShader(pPShader->GetObject().pShader, NULL, 0);
		}
		// インプットデータ設定
		pContext->IASetInputLayout(pPassShader->GetObject().pLayout);
	}

	// 頂点バッファ設定
	{
		const CVertexBuffer& vtxBuffer = CGfxUtility::GetScreenVtx();
		ID3D11Buffer* pVtxBuf = NULL;
		u32 vtxStride = 0;
		const VTX_DECL* pDecl = pPassShader->GetVtxDecl();
		u32 declNum = pPassShader->GetDeclNum();
		for (u32 idx = 0; idx < declNum; idx++, pDecl++)
		{
			// シェーダのDECLから取得
			u32 offset = pDecl->offset;
			if (idx == 0)
			{
				const CVertexBuffer& vtxBuffer = CGfxUtility::GetScreenVtx();
				pVtxBuf = vtxBuffer.GetObject().pBuf;
				vtxStride = vtxBuffer.GetVtxStride();
				offset = 0;
			}
			// 頂点設定
			if (pVtxBuf)
			{
				pContext->IASetVertexBuffers(pDecl->attr, 1, &pVtxBuf, &vtxStride, &offset);
			}
		}
	}

	// テクスチャ設定
	{
		const TEXTURE_OBJECT& texObj = _pTexture->GetObject();
		pContext->PSSetShaderResources(0, 1, &texObj.pSRV);
	}

	// サンプラー設定
	{
		const CSampler& sampler = CGfxUtility::GetDefaultSampler();
		pContext->PSSetSamplers(0, 1, &sampler.GetObject().pState);
	}

	// ビューポート設定
	{
		D3D11_VIEWPORT vp;
#if defined( OPENGL_VIEWPORT )
		vp.TopLeftX = SCast<f32>(screenViewport.posX);
		vp.TopLeftY = SCast<f32>(GetHeight() - screenViewport.height - screenViewport.posY);
		vp.Width = SCast<f32>(screenViewport.width);
		vp.Height = SCast<f32>(screenViewport.height);
#elif defined( DIRECTX_VIEWPORT )
		vp.TopLeftX = SCast<f32>(screenViewport.posX);
		vp.TopLeftY = SCast<f32>(screenViewport.posY);
		vp.Width = SCast<f32>(screenViewport.width);
		vp.Height = SCast<f32>(screenViewport.height);
#endif
		vp.MinDepth = 0.0f;
		vp.MaxDepth = 1.0f;
		// ジオメトリシェーダーが SV_ViewportArrayIndex セマンティクスを利用していないので矩形は一つしか現在使えない
		pContext->RSSetViewports(1, &vp);
	}
	// シザリング設定
	{
		// 切り取る矩形を設定
		D3D11_RECT rect;
#if defined( OPENGL_VIEWPORT )
		rect.left = screenViewport.posX;
		rect.top = GetHeight() - screenViewport.height - screenViewport.posY;
		rect.right = screenViewport.posX + screenViewport.width;
		rect.bottom = GetHeight() - screenViewport.posY;
#elif defined( DIRECTX_VIEWPORT )
		rect.left = screenViewport.posX;
		rect.top = screenViewport.posY;
		rect.right = screenViewport.posX + screenViewport.width;
		rect.bottom = screenViewport.posY + screenViewport.height;
#endif
		// ジオメトリシェーダーが SV_ViewportArrayIndex セマンティクスを利用していないので矩形は一つしか現在使えない
		pContext->RSSetScissorRects(1, &rect);
	}

	// ラスタライザ設定
	{
		const CRasterizer& state = CGfxUtility::GetDefaultRasterizer();
		pContext->RSSetState(state.GetObject().pState);
	}

	// ブレンドステート設定
	{
		const CBlendState& state = CGfxUtility::GetDefaultBlendState();
		f32 blendFactor[] = { 1.0f, 1.0f, 1.0f, 1.0f };	// !!!! ブレンド係数を外部より指定させる必要は？
		u32 sampleMask = 0xffffffff;					// !!!! サンプル マスクの指定は必要ない？
		pContext->OMSetBlendState(state.GetObject().pState, blendFactor, sampleMask);
	}

	// デプスステンシルステート設定
	{
		const CDepthState& state = CGfxUtility::GetDefaultDepthState();
		u32 stencilRef = 0x0;					// !!!! サンプル ステンシル参照値を設定させるようにしなきゃなぁ
		pContext->OMSetDepthStencilState(state.GetObject().pState, stencilRef);
	}

	// 描画
	{
		// プリミティブ(ポリゴンの形状)をコンテキストに設定
		D3D11_PRIMITIVE_TOPOLOGY dxPrim;
		CGfxFormatUtilityDX11::ConvertPrimitive(dxPrim, GFX_PRIMITIVE_QUADS);
		pContext->IASetPrimitiveTopology(dxPrim);
		pContext->Draw(4, 0);
	}
}

///-------------------------------------------------------------------------------------------------
///	垂直同期
b8		CDeviceDX11::WaitVsync(u32 _syncInterval)
{
	libAssert(m_object.pBuf);
	return SUCCEEDED(m_object.pBuf->Present(_syncInterval, 0));
}

///-------------------------------------------------------------------------------------------------
///	ターゲットのリサイズ
b8		CDeviceDX11::ResizeTarget(u32 _width, u32 _height)
{
	// 変更なし
	if (GetWidth() == _width && GetHeight() == _height) { return true; }
	
	b8 bRet = true;
	// スワップバッファ作り直し
	if (m_swapNum > 0)
	{
		GFX_IMAGE_FORMAT format = m_pColor[0].GetFormat();
		GFX_ANTI_ALIASE aa = m_pColor[0].GetAA();
		SAFE_RELEASE(m_object.pBuf);
		for (u32 idx = 0; idx < m_swapNum; idx++)
		{
			m_pColor[idx].Release();
		}
		bRet &= CreateSwap(m_pColor, _width, _height, format, aa, m_swapNum, m_refreshRate);
	}

	// デプスバッファ作り直し
	if (m_pDepth)
	{
		GFX_IMAGE_FORMAT format = m_pDepth->GetFormat();
		GFX_ANTI_ALIASE aa = m_pDepth->GetAA();
		m_pDepth->Release();
		bRet &= m_pDepth->Create(_width, _height, format, aa, 1);
	}

	if (bRet)
	{
		m_width = _width;
		m_height = _height;
	}
	return bRet;
}



///-------------------------------------------------------------------------------------------------
/// ディファードコンテキスト生成
ID3D11DeviceContext*	CDeviceDX11::CreateDeferredContext()
{
	libAssert(GetContext());
	ID3D11DeviceContext* pContext = NULL;
	// DirectX11DifferedContext作成
	HRESULT hr = m_pD3D11Device->CreateDeferredContext(0, &pContext);
	if (FAILED(hr))
	{
		PRINT_ERROR("*****[ERROR][0x%08x] CDviceUnitDX11::CreateDeferredContext() Failed CreateDeferredContext\n", hr);
		return NULL;
	}
	return pContext;
}

///-------------------------------------------------------------------------------------------------
/// マルチサンプリングクオリティレベルチェック
void	CDeviceDX11::CheckMultisampleQualityLevels(DXGI_SAMPLE_DESC& _retDesc, DXGI_FORMAT _format, u32 _aa)
{
	// マルチサンプル
	_retDesc.Count = 1;
	_retDesc.Quality = 0;
	while (_aa > 0)
	{
		// AAのクオリティチェック
		if (SUCCEEDED(GetDevice()->CheckMultisampleQualityLevels(_format, _aa, &_retDesc.Quality)))
		{
			if (_retDesc.Quality > 0)
			{
				// マルチサンプル設定
				_retDesc.Count = _aa;
				_retDesc.Quality--;
				break;
			}
		}
		_aa >>= 1;
	}
}


///-------------------------------------------------------------------------------------------------
///	GPUメモリチェック
void	CDeviceDX11::CheckGPU()
{
#if 0
	// AMD
	{
		AGSGPUInfo& gpuInfo = s_agsGpuInfo;

		AGSConfiguration config = {};
		config.crossfireMode = AGS_CROSSFIRE_MODE_DRIVER_AFR;

		// ags初期化
		if (::agsInit(&m_pAgsContext, &config, &gpuInfo) == AGS_SUCCESS)
		{
#ifndef POISON_RELEASE
			PRINT("-----------------------------------------------------------------\n");
			PRINT("AGS Library initialized: v%d.%d.%d\n", gpuInfo.agsVersionMajor, gpuInfo.agsVersionMinor, gpuInfo.agsVersionPatch);
			PRINT("Is%s WACK compliant for use in UWP apps\n", gpuInfo.isWACKCompliant ? "" : " *not*");

			PRINT("Radeon Software Version:   %s\n", gpuInfo.radeonSoftwareVersion);
			PRINT("Driver Version:            %s\n", gpuInfo.driverVersion);

			{
				s32 numCrossfireGPUs = 0;
				if (::agsGetCrossfireGPUCount(m_pAgsContext, &numCrossfireGPUs) == AGS_SUCCESS)
				{
					PRINT("Crossfire GPU count = %d\n", numCrossfireGPUs);
				}
				else
				{
					PRINT("Failed to get Crossfire GPU count\n");
				}
			}
#endif// POISON_RELEASE

			for (s32 gpuIndex = 0; gpuIndex < gpuInfo.numDevices; gpuIndex++)
			{
				const AGSDeviceInfo& device = gpuInfo.devices[gpuIndex];

				PRINT("\n---------- Device %d%s, %s\n", gpuIndex, device.isPrimaryDevice ? " [primary]" : "", device.adapterString);

				PRINT("Vendor id:   0x%04X (%s)\n", device.vendorId, getVendorName(device.vendorId));
				PRINT("Device id:   0x%04X\n", device.deviceId);
				PRINT("Revision id: 0x%04X\n\n", device.revisionId);

				for (s32 i = 0; i < device.numDisplays; i++)
				{
					const AGSDisplayInfo& display = device.displays[i];
					PRINT("\t---------- Display %d %s----------------------------------------\n", i, display.displayFlags & AGS_DISPLAYFLAG_PRIMARY_DISPLAY ? "[primary]" : "---------");

					PRINT("\tdevice name: %s\n", display.displayDeviceName);
					PRINT("\tmonitor name: %s\n\n", display.name);
					PRINT("\tMax resolution:             %d x %d, %.1f Hz\n", display.maxResolutionX, display.maxResolutionY, display.maxRefreshRate);
					PRINT("\tCurrent resolution:         %d x %d, Offset (%d, %d), %.1f Hz\n", display.currentResolution.width, display.currentResolution.height, display.currentResolution.offsetX, display.currentResolution.offsetY, display.currentRefreshRate);

					if (display.displayFlags & AGS_DISPLAYFLAG_HDR10)
					{
						// HDRディスプレイが存在
						m_isCanHdrMode = true;
						PRINT("\tHDR10 supported\n");
					}

#ifndef POISON_RELEASE
					if (display.displayFlags & AGS_DISPLAYFLAG_DOLBYVISION)
						PRINT("\tDolby Vision supported\n");

					if (display.displayFlags & AGS_DISPLAYFLAG_FREESYNC)
						PRINT("\tFreesync supported\n");

					if (display.displayFlags & AGS_DISPLAYFLAG_FREESYNC_2)
						PRINT("\tFreesync 2 supported\n");

					PRINT("\tlogical display index: %d\n", display.logicalDisplayIndex);
					PRINT("\tADL adapter index: %d\n\n", display.adlAdapterIndex);
#endif // POISON_RELEASE
				}
			}

			PRINT("-----------------------------------------------------------------\n");
		}
	}


	// NVIDIA
	do
	{
		NvAPI_Status ret = NVAPI_OK;
		ret = ::NvAPI_Initialize();
		if (ret != NVAPI_OK)
		{
			break;
		}

		m_isInitNvAPI = true;

#ifndef POISON_RELEASE
		{
			NvAPI_ShortString strName;
			if (::NvAPI_GetInterfaceVersionString(strName) == NVAPI_OK)
			{
				PRINT("-----------------------------------------------------------------\n");
				PRINT("NVAPI Library initialized: [%s]\n", strName);
			}
		}
#endif// POISON_RELEASE

		NvU32 gpuCount = 0;
		NvPhysicalGpuHandle ahGPU[NVAPI_MAX_PHYSICAL_GPUS] = {};
		// GPU情報取得
		if (::NvAPI_EnumPhysicalGPUs(ahGPU, &gpuCount) != NVAPI_OK)
		{
			break;
		}

		// GPUループ
		for (NvU32 i = 0; i < gpuCount; ++i)
		{

#ifndef POISON_RELEASE
			{
				NvAPI_ShortString strName;
				if (NvAPI_GPU_GetFullName(ahGPU[i], strName) != NVAPI_OK)
				{
					strName[0] = 0;
				}
				PRINT("\n---------- Device %d, %s\n", i, strName);

				NvU32 deviceId = 0;
				NvU32 subSystemId = 0;
				NvU32 revisionId = 0;
				NvU32 extDeviceId = 0;
				if (NvAPI_GPU_GetPCIIdentifiers(ahGPU[i], &deviceId, &subSystemId, &revisionId, &extDeviceId) == NVAPI_OK)
				{
					//	PRINT("Vendor id:   0x%04X (%s)\n", device.vendorId, getVendorName(device.vendorId));
					PRINT("Device id:    0x%04X\n", deviceId);
					PRINT("SubSystem id: 0x%04X\n", subSystemId);
					PRINT("Revision id:  0x%04X\n", revisionId);
					PRINT("ExtDevice id: 0x%04X\n\n", extDeviceId);
				}
			}
#endif// POISON_RELEASE


			NvU32 displayIdCount = 16;
			NvU32 flags = 0;
			NV_GPU_DISPLAYIDS displayIdArray[16] = {};
			displayIdArray[0].version = NV_GPU_DISPLAYIDS_VER;
			// ディスプレイリスト取得
			if (::NvAPI_GPU_GetConnectedDisplayIds(ahGPU[i], displayIdArray,
				&displayIdCount, flags) != NVAPI_OK){
				continue;
			}

			for (NvU32 j = 0; j < displayIdCount; ++j)
			{
#ifndef POISON_RELEASE
				PRINT("\t---------- Display %d ----------------------------------------\n", j);

				PRINT("\tdisplayId: %d\n", displayIdArray[j].displayId);
				PRINT("\tisDynamic: %d\n", displayIdArray[j].isDynamic);
				PRINT("\tisActive: %d\n", displayIdArray[j].isActive);
				PRINT("\tisCluster: %d\n", displayIdArray[j].isCluster);
				PRINT("\tisConnected: %d\n", displayIdArray[j].isConnected);
#endif// POISON_RELEASE

				NV_HDR_CAPABILITIES hdrCapabilities = {};
				hdrCapabilities.version = NV_HDR_CAPABILITIES_VER;
				ret = ::NvAPI_Disp_GetHdrCapabilities(displayIdArray[j].displayId, &hdrCapabilities);
				// HDR情報取得
				if (ret != NVAPI_OK)
				{
#ifndef POISON_RELEASE
					NvAPI_ShortString szDesc;
					::NvAPI_GetErrorMessage(ret, szDesc);
					PRINT("\tFailed NvAPI_Disp_GetHdrCapabilities %s (%x) Gpu[%d] Display[%d]\n", szDesc, ret, i, j);
#endif// POISON_RELEASE
					continue;
				}

				if (hdrCapabilities.isST2084EotfSupported)
				{
					// HDRディスプレイが存在
					m_isCanHdrMode = true;
					PRINT("\tHDR10 supported\n");
				}

			}
		}
	} while (false);
#endif // 0
}

///-------------------------------------------------------------------------------------------------
///	DirectX11の初期化
b8		CDeviceDX11::InitDX11()
{
	// アダプターチェック
	{
		// ファクトリー取得
		::CreateDXGIFactory(__uuidof(IDXGIFactory), (void **)(&m_pDXGIFactory));

		u32 count = 0;
		u64 memMaxSize = 0;
		s32 index = -1;
		// アダプター列挙
		while (SUCCEEDED(m_pDXGIFactory->EnumAdapters(count, &m_pDXGIAdapter)))
		{
			b8 isEnableAdapter = false;
			UINT monitorIndex = 0;
			IDXGIOutput* pOutput = NULL;
			// モニタに接続されているアダプターか調べる
			while (SUCCEEDED(m_pDXGIAdapter->EnumOutputs(monitorIndex, &pOutput)))
			{
				DXGI_OUTPUT_DESC desc;
				if (SUCCEEDED(pOutput->GetDesc(&desc)))
				{
					isEnableAdapter |= desc.AttachedToDesktop != FALSE;
				}
				pOutput->Release();
				pOutput = NULL;
				++monitorIndex;
			}

			if (isEnableAdapter)
			{
				DXGI_ADAPTER_DESC desc;
				// ビデオメモリが一番多いものを探してみる
				if (SUCCEEDED(m_pDXGIAdapter->GetDesc(&desc)))
				{
					if (desc.DedicatedVideoMemory > memMaxSize)
					{
						memMaxSize = desc.DedicatedVideoMemory;
						index = SCast<u32>(count);
					}
					//PRINT("GpuAdapter[%u]:%s VideoMemory[%u MB]\n", count, desc.Description, (u32)(desc.DedicatedVideoMemory / 1024 / 1024));
					wprintf(L"GpuAdapter[%u]:%s VideoMemory[%u MB]\n", count, desc.Description, (u32)(desc.DedicatedVideoMemory / 1024 / 1024));
				}
			}
			m_pDXGIAdapter->Release();
			m_pDXGIAdapter = NULL;
			++count;
		}

		// アダプターが複数存在
		if (count > 1 && index >= 0)
		{
			// 手動で選ぶ
			if (FAILED(m_pDXGIFactory->EnumAdapters(SCast<u32>(index), &m_pDXGIAdapter)))
			{
				libAssert(0);
				return false;
			}
		}
		else
		{
			// 手動で選んでもいいけどデフォルトの挙動にお任せする
			m_pDXGIFactory->Release();
			m_pDXGIFactory = NULL;
			SAFE_RELEASE(m_pDXGIFactory);
		}
	}

	// 初期化順
	D3D_FEATURE_LEVEL featureLevels[] =
	{
		//D3D_FEATURE_LEVEL_11_2,
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
		//D3D_FEATURE_LEVEL_10_1,
		//D3D_FEATURE_LEVEL_10_0,
	};
	D3D_FEATURE_LEVEL resultLevel;

	UINT createDeviceFlag = 0;
#ifndef POISON_RELEASE
	// win10からだとグラフィックツールってのをインストールしないといけないらしい
	//createDeviceFlag = D3D11_CREATE_DEVICE_DEBUG;
#endif // POISON_RELEASE

	// DirectX11デバイス作成
	HRESULT hr = ::D3D11CreateDevice(
		m_pDXGIAdapter,
		m_pDXGIAdapter ? D3D_DRIVER_TYPE_UNKNOWN : D3D_DRIVER_TYPE_HARDWARE,
		NULL,
		createDeviceFlag,
		featureLevels,
		ARRAYOF(featureLevels),
		D3D11_SDK_VERSION,
		&m_pD3D11Device,
		&resultLevel,
		&m_pD3D11ImmediateContext
		);
	if (FAILED(hr))
	{
		PRINT_ERROR("*****[ERROR] CDviceUnitDX11::InitBegin() Failed CreateDevice\n");
		Alert("GPUが DirectX 11.1 ShaderModel 5.0 に対応していません。");
		return false;
	}

	// 11.1
	if (resultLevel >= D3D_FEATURE_LEVEL_11_1)
	{
		if (FAILED(m_pD3D11Device->QueryInterface(IID_ID3D11Device1, (void**)&m_pD3D11Device1)))
		{
			PRINT_ERROR("*****[ERROR] CDviceUnitDX11::InitBegin() Failed QueryInterface IID_ID3D11Device1\n");
			return false;
		}
		if (m_pD3D11Device1)
		{
			m_pD3D11Device1->GetImmediateContext1(&m_pD3D11ImmediateContext1);
		}
	}

	// 11.2
	if (resultLevel >= D3D_FEATURE_LEVEL_11_1)
	{
		if (FAILED(m_pD3D11Device->QueryInterface(IID_ID3D11Device2, (void**)&m_pD3D11Device2)))
		{
			PRINT_ERROR("*****[ERROR] CDviceUnitDX11::InitBegin() Failed QueryInterface IID_ID3D11Device2\n");
			return false;
		}
		if (m_pD3D11Device2)
		{
			m_pD3D11Device2->GetImmediateContext2(&m_pD3D11ImmediateContext2);
		}
	}

	// 11.3
	if (resultLevel >= D3D_FEATURE_LEVEL_11_1)
	{
		if (FAILED(m_pD3D11Device->QueryInterface(IID_ID3D11Device3, (void**)&m_pD3D11Device3)))
		{
			PRINT_ERROR("*****[ERROR] CDviceUnitDX11::InitBegin() Failed QueryInterface IID_ID3D11Device3\n");
			return false;
		}
		if (m_pD3D11Device3)
		{
			m_pD3D11Device3->GetImmediateContext3(&m_pD3D11ImmediateContext3);
		}
	}

	// 使用可能なMSAAを取得
	for (u32 i = 1; i <= D3D11_MAX_MULTISAMPLE_SAMPLE_COUNT; i <<= 1)
	{
		UINT Quality;
		if (SUCCEEDED(m_pD3D11Device->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, i, &Quality)))
		{
			if (0 < Quality)
			{
				PRINT("MSAA08 : Count[%d] Quality[%d]\n", i, Quality);
				//m_sampleDesc.Count = i;
				//m_sampleDesc.Quality = Quality - 1;
			}
			else{ break; }
		}
		else{ break; }

		if (SUCCEEDED(m_pD3D11Device->CheckMultisampleQualityLevels(DXGI_FORMAT_R16G16B16A16_UNORM, i, &Quality)))
		{
			if (0 < Quality)
			{
				PRINT("MSAA16 : Count[%d] Quality[%d]\n", i, Quality);
			}
		}
	}

	// インターフェース取得
	if (FAILED(m_pD3D11Device->QueryInterface(__uuidof(IDXGIDevice), (void**)&m_pDXGIDevice)))
	{
		PRINT_ERROR("*****[ERROR] CDviceUnitDX11::InitBegin() Failed QueryInterface IDXGIDevice\n");
		return false;
	}

	// アダプター取得
	if (m_pDXGIAdapter == NULL && FAILED(m_pDXGIDevice->GetAdapter(&m_pDXGIAdapter)))
	{
		PRINT_ERROR("*****[ERROR] CDviceUnitDX11::InitBegin() Failed GetAdapter\n");
		return false;
	}

	// アダプターの情報を取得
	DXGI_ADAPTER_DESC desc;
	m_pDXGIAdapter->GetDesc(&desc);
	//PRINT("Use Gpu:%s\n", desc.Description);
	wprintf(L"Use Gpu:%s\n", desc.Description);
	if (m_pDXGIFactory == NULL)
	{
		//ファクトリー取得
		m_pDXGIAdapter->GetParent(__uuidof(IDXGIFactory), (void**)&m_pDXGIFactory);
		if (m_pDXGIFactory == NULL)
		{
			PRINT_ERROR("*****[ERROR] CDviceUnitDX11::InitBegin() Failed GetParent\n");
			return false;
		}
	}

	// ディスプレイビットフラグ更新
	//CheckUpdateDisplayBitFlag();

	// 即時コンテキスト設定
	m_pDrawContext = m_pD3D11ImmediateContext;
	//m_pDrawContext = m_pD3D11ImmediateContext1;
	//m_pDrawContext = m_pD3D11ImmediateContext2;
	//m_pDrawContext = m_pD3D11ImmediateContext3;

	return true;
}


///-------------------------------------------------------------------------------------------------
///	スワップバッファ生成
b8		CDeviceDX11::CreateSwap(CColorBuffer* _pRetClrBuf, u16 _width, u16 _height, GFX_IMAGE_FORMAT _format, GFX_ANTI_ALIASE _aa, u32 _swapNum, u32 _refreshRate)
{
	ID3D11Device* pDeviceDX11 = GetDevice();
	libAssert(pDeviceDX11);

	// フォーマットコンバート
	DXGI_FORMAT dxFormat = DXGI_FORMAT_UNKNOWN;
	CGfxFormatUtilityDX11::ConvertFormat(dxFormat, _format);
	u32 dxAA = CGfxFormatUtility::ConvertAA(_aa);

	// スワップバッファ生成
	if (_swapNum > 0)
	{
		// マルチサンプル
		DXGI_SAMPLE_DESC sampleDesc;
		CheckMultisampleQualityLevels(sampleDesc, dxFormat, dxAA);

		IDXGISwapChain* pSwapChain = NULL;
		// スワップ作成
		{
			// スワップチェイン作成
			DXGI_SWAP_CHAIN_DESC swapChianDesc;
			swapChianDesc.BufferDesc.Width = _width;
			swapChianDesc.BufferDesc.Height = _height;
			swapChianDesc.BufferDesc.RefreshRate.Numerator = _refreshRate;	// FPS(Denominator/Numerator)
			swapChianDesc.BufferDesc.RefreshRate.Denominator = 1;			// FPS(Denominator/Numerator)
			swapChianDesc.BufferDesc.Format = dxFormat;
			swapChianDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
			swapChianDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;	//DXGI_MODE_SCALING_STRETCHED(スケーリングする場合だが、うまく動かん...)
			swapChianDesc.SampleDesc = sampleDesc;
			swapChianDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
			swapChianDesc.BufferCount = _swapNum;
			swapChianDesc.OutputWindow = GetHWND();
			swapChianDesc.Windowed = TRUE;
			swapChianDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
			swapChianDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
#ifdef USE_SWAP_BUFFER
			swapChianDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_CENTERED;
			swapChianDesc.SampleDesc.Count = 1;
			swapChianDesc.SampleDesc.Quality = 0;
#endif // USE_SWAP_BUFFER

			if (FAILED(GetFactory()->CreateSwapChain(pDeviceDX11, &swapChianDesc, &pSwapChain)))
			{
				PRINT_ERROR("*****[ERROR] CDisplay::Create() Failed CreateSwapChain\n");
				libAssert(0);
				return false;
			}

			// ポインタ保存
			m_object.pBuf = pSwapChain;
			m_object.param = swapChianDesc.OutputWindow;

			// ALT+ENTERでのフルスクリーン禁止
			GetFactory()->MakeWindowAssociation(m_object.param, DXGI_MWA_NO_ALT_ENTER | DXGI_MWA_NO_WINDOW_CHANGES);
		}

		// スワップバッファ数分レンダーバッファ生成
		for (u32 idx = 0; idx < _swapNum; idx++)
		{
			if (_pRetClrBuf[idx].InitReferenceSwap(m_object, idx, _width, _height, _format, _aa) == false)
			{
				libAssert(0);
				return false;
			}
		}
	}
	return true;
}


#ifndef POISON_RELEASE
///-------------------------------------------------------------------------------------------------
/// PrintError
void	CDeviceDX11::PrintError(const HRESULT& _result, const c8* _pFuncName, const c8* _format /*= NULL */)
{
	const c8* pFormat = "*****[ERROR][DX11] %s [0x%08x][%s]\n";
	if (_format){ pFormat = _format; }

#define CASE_PRINT( __def )	case __def: PRINT_ERROR(pFormat, _pFuncName, _result, #__def ); break
	// 
	switch (_result)
	{
		CASE_PRINT(D3D11_ERROR_FILE_NOT_FOUND);									// ファイルが見つかりませんでした。
		CASE_PRINT(D3D11_ERROR_TOO_MANY_UNIQUE_STATE_OBJECTS);					// 特定の種類のステート オブジェクトの一意のインスタンスが多すぎます。
		CASE_PRINT(D3D11_ERROR_TOO_MANY_UNIQUE_VIEW_OBJECTS);					// 特定の種類のビュー オブジェクトの一意のインスタンスが多すぎます。
		CASE_PRINT(D3D11_ERROR_DEFERRED_CONTEXT_MAP_WITHOUT_INITIAL_DISCARD);	// リソースごとの ID3D11Device::CreateDeferredContext の呼び出しまたは ID3D11DeviceContext::FinishCommandList の呼び出しの後で最初に呼び出した ID3D11DeviceContext::Map が D3D11_MAP_WRITE_DISCARD ではありませんでした。
		//CASE_PRINT( D3DERR_INVALIDCALL );		// メソッドの呼び出しが無効です。たとえば、メソッドのパラメーターが有効なポインターでない可能性があります。
		//CASE_PRINT( D3DERR_WASSTILLDRAWING );	// このサーフェスとの間で情報を転送している以前のビット演算が不完全です。
		CASE_PRINT(E_FAIL);						// デバッグ レイヤーを有効にしてデバイスを作成しようとしましたが、該当するレイヤーがインストールされていません。
		CASE_PRINT(E_INVALIDARG);				// 戻り関数に無効なパラメーターが渡されました。
		CASE_PRINT(E_OUTOFMEMORY);				// Direct3D が呼び出しを完了するうえで十分なメモリーを割り当てることができませんでした。
		CASE_PRINT(S_FALSE);					// 正常に処理されたものの、非標準の完了を示す代替成功値です (正確な意味はコンテキストによって異なります)。
		CASE_PRINT(S_OK);						// エラーは発生していません。
	}
}
#endif // !POISON_RELEASE


POISON_END


#endif // LIB_GFX_DX11

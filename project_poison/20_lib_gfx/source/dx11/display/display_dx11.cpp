﻿
#ifdef LIB_GFX_DX11

///-------------------------------------------------------------------------------------------------
/// define
//#define	USE_SWAP_BUFFER


///-------------------------------------------------------------------------------------------------
// include
#include "display/display.h"

#include "device/device_utility.h"
#include "dx11/device/win/device_dx11.h"

#include "format/gfx_format_utility.h"
#include "dx11/format/gfx_format_utility_dx11.h"

#include "object/color_buffer.h"
#include "object/depth_buffer.h"
#include "object/texture_buffer.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
// static


///*************************************************************************************************
///	ディスプレイ
///*************************************************************************************************

CDisplay::CDisplay()
	: m_refreshRate(60)
{
}
CDisplay::~CDisplay()
{
}


///-------------------------------------------------------------------------------------------------
/// バインドするかどうか
b8		CDisplay::IsBind() const
{
	return false;
}



///-------------------------------------------------------------------------------------------------
///	スワップバッファ生成
b8		CDisplay::CreateSwap(CColorBuffer* _pRetClrBuf, u16 _width, u16 _height, const COLOR_DESC& _clrDesc, u32 _swapNum, u32 _refreshRate)
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	ID3D11Device* pDeviceDX11 = pDevice->GetDevice();
	libAssert(pDeviceDX11);

	// フォーマットコンバート
	DXGI_FORMAT dxFormat = DXGI_FORMAT_UNKNOWN;
	CGfxFormatUtilityDX11::ConvertFormat(dxFormat, _clrDesc.format);
	u32 dxAA = CGfxFormatUtility::ConvertAA(_clrDesc.aa);

	// スワップバッファ生成
	if (_swapNum > 0)
	{
		// マルチサンプル
		DXGI_SAMPLE_DESC sampleDesc;
		pDevice->CheckMultisampleQualityLevels(sampleDesc, dxFormat, dxAA);

		IDXGISwapChain* pSwapChain = NULL;
		// スワップ作成
		{
			// スワップチェイン作成
			DXGI_SWAP_CHAIN_DESC swapChianDesc;
			swapChianDesc.BufferDesc.Width = _width;
			swapChianDesc.BufferDesc.Height = _height;
			swapChianDesc.BufferDesc.RefreshRate.Numerator = _refreshRate;	// FPS(Denominator/Numerator)
			swapChianDesc.BufferDesc.RefreshRate.Denominator = 1;			// FPS(Denominator/Numerator)
			swapChianDesc.BufferDesc.Format = dxFormat;
			swapChianDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
			swapChianDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;	//DXGI_MODE_SCALING_STRETCHED(スケーリングする場合)
			swapChianDesc.SampleDesc = sampleDesc;
			swapChianDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
			swapChianDesc.BufferCount = _swapNum;
			swapChianDesc.OutputWindow = pDevice->GetHWND();
			swapChianDesc.Windowed = TRUE;
			swapChianDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
			swapChianDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
#ifdef USE_SWAP_BUFFER
			swapChianDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_CENTERED;
			swapChianDesc.SampleDesc.Count = 1;
			swapChianDesc.SampleDesc.Quality = 0;
#endif // USE_SWAP_BUFFER

			if (FAILED(pDevice->GetFactory()->CreateSwapChain(pDeviceDX11, &swapChianDesc, &pSwapChain)))
			{
				PRINT_ERROR("*****[ERROR] CDisplay::Create() Failed CreateSwapChain\n");
				libAssert(0);
				return false;
			}

			// ポインタ保存
			m_object.pBuf = pSwapChain;
			m_object.param = swapChianDesc.OutputWindow;

			// ALT+ENTERでのフルスクリーン禁止
			pDevice->GetFactory()->MakeWindowAssociation(m_object.param, DXGI_MWA_NO_ALT_ENTER | DXGI_MWA_NO_WINDOW_CHANGES);
		}

		// スワップバッファ数分レンダーバッファ生成
		for (u32 idx = 0; idx < _swapNum; idx++)
		{
			if (_pRetClrBuf[idx].InitReferenceSwap(m_object, idx, _width, _height, _clrDesc.format, _clrDesc.aa) == false)
			{
				libAssert(0);
				return false;
			}
		}
	}
	return true;
}


///-------------------------------------------------------------------------------------------------
///	生成
b8		CDisplay::Create(const DISPLAY_DESC& _desc)
{
	libAssert(_desc.swapNum <= MAX_RT_NUM);

	// レンダーバッファ生成
	CColorBuffer* pColor = NULL;
	if (_desc.swapNum > 0)
	{
		pColor = MEM_CALLOC_TYPES(_desc.memHdl, CColorBuffer, _desc.swapNum);
		libAssert(pColor);
	}
	CDepthBuffer* pDepth = NULL;
	if (_desc.pDepth)
	{
		pDepth = MEM_CALLOC_TYPE(_desc.memHdl, CDepthBuffer);
		libAssert(pDepth);
		m_pDepTex = MEM_CALLOC_TYPE(_desc.memHdl, CTextureBuffer);
		libAssert(m_pDepTex);
	}
	m_memHdl = _desc.memHdl;


	// 指定がなければクライアントサイズで設定
	u32 width = _desc.width;
	u32 height = _desc.height;
	if (width <= 0 && height <= 0)
	{
		CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
		libAssert(pDevice);
		pDevice->GetClientSize(width, height);
	}

	b8 bRet = true;
	// スワップバッファ生成
	if (_desc.swapNum > 0)
	{
		bRet &= CreateSwap(pColor, width, height, _desc.color, _desc.swapNum, _desc.refreshRate);
	}
	
	// デプスバッファ生成
	if (_desc.pDepth)
	{
		bRet &= pDepth->Create(_desc.width, _desc.height, _desc.pDepth->format, _desc.pDepth->aa, _desc.pDepth->num, _desc.pDepth->usage, _desc.pDepth->attr);
		bRet &= m_pDepTex->InitReferenceTarget(pDepth);
	}

	if (bRet)
	{
		strcpy_s(m_name, _desc.name);
		m_pColor = pColor;
		m_colorNum = _desc.swapNum;
		m_pDepth = pDepth;
		m_depthNum = pDepth ? 1 : 0;
		m_width = width;
		m_height = height;
		m_refreshRate = _desc.refreshRate;
	}

	return bRet;
}

///-------------------------------------------------------------------------------------------------
///	破棄
void	CDisplay::Release()
{
	SAFE_RELEASE(m_object.pBuf);
	for (u32 idx = 0; idx < m_colorNum; idx++)
	{
		m_pColor[idx].Release();
	}
	for (u32 idx = 0; idx < m_depthNum; idx++)
	{
		m_pDepth[idx].Release();
		m_pDepTex[idx].Release();
	}
	MEM_SAFE_FREE(m_memHdl, m_pColor);
	MEM_SAFE_FREE(m_memHdl, m_pDepth);
	MEM_SAFE_FREE(m_memHdl, m_pClrTex);
	MEM_SAFE_FREE(m_memHdl, m_pDepTex);
	*this = CDisplay();
}

///-------------------------------------------------------------------------------------------------
///	ターゲットのリサイズ
b8		CDisplay::ResizeTarget(u16 _width, u16 _height)
{
	// 変更なし
	if (GetWidth() == _width && GetHeight() == _height) { return true; }

	b8 bRet = true;
	// スワップバッファ作り直し
	if (m_colorNum > 0)
	{
		COLOR_DESC clrDesc;
		clrDesc.format = m_pColor[0].GetFormat();
		clrDesc.aa = m_pColor[0].GetAA();
		SAFE_RELEASE(m_object.pBuf);
		for (u32 idx = 0; idx < m_colorNum; idx++)
		{
			m_pColor[idx].Release();
		}
		bRet &= CreateSwap(m_pColor, _width, _height, clrDesc, m_colorNum, m_refreshRate);
	}

	// デプスバッファ作り直し
	for (u32 idx = 0; idx < m_depthNum; idx++)
	{
		GFX_IMAGE_FORMAT format = m_pDepth[idx].GetFormat();
		GFX_ANTI_ALIASE aa = m_pDepth[idx].GetAA();
		u32 arrayNum = m_pDepth[idx].GetArrayNum();
		m_pDepth[idx].Release();
		bRet &= m_pDepth[idx].Create(_width, _height, format, aa, arrayNum);
	}

	if (bRet)
	{
		m_width = _width;
		m_height = _height;
	}

	return bRet;
}

///-------------------------------------------------------------------------------------------------
///	垂直同期
b8		CDisplay::WaitVsync(u32 _syncInterval)
{
	libAssert(m_object.pBuf);
	return SUCCEEDED(m_object.pBuf->Present(_syncInterval, 0));
}



POISON_END


#endif // LIB_GFX_DX11

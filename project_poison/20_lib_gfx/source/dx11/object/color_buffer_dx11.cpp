﻿
#ifdef LIB_GFX_DX11

///-------------------------------------------------------------------------------------------------
// include
#include "object/color_buffer.h"

#include "device/device_utility.h"
#include "dx11/device/win/device_dx11.h"
#include "format/gfx_format_utility.h"
#include "dx11/format/gfx_format_utility_dx11.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
// static



///*************************************************************************************************
///	カラーバッファ
///*************************************************************************************************
CColorBuffer::CColorBuffer()
	: m_width(0)
	, m_height(0)
	, m_format(GFX_FORMAT_UNKNOWN)
	, m_aa(GFX_AA_UNKNOWN)
{
}
CColorBuffer::~CColorBuffer()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		CColorBuffer::IsEnable() const
{
	return (m_object.pTex != NULL);
}

///-------------------------------------------------------------------------------------------------
///	生成
b8		CColorBuffer::Create(u16 _width, u16 _height, GFX_IMAGE_FORMAT _format, GFX_ANTI_ALIASE _aa, GFX_USAGE _usage/* = GFX_USAGE_NONE*/, GFX_BUFFER_ATTR _attr/* = GFX_BUFFER_ATTR_RO*/)
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	ID3D11Device* pDeviceDX11 = pDevice->GetDevice();
	libAssert(pDeviceDX11);

	// フォーマットコンバート
	DXGI_FORMAT dxFormat = DXGI_FORMAT_UNKNOWN;
	CGfxFormatUtilityDX11::ConvertFormat(dxFormat, _format);
	u32 dxAA = CGfxFormatUtility::ConvertAA(_aa);

	// マルチサンプル
	DXGI_SAMPLE_DESC sampleDesc;
	pDevice->CheckMultisampleQualityLevels(sampleDesc, dxFormat, dxAA);

	// テクスチャ生成
	{
		D3D11_TEXTURE2D_DESC desc;
		::memset(&desc, 0, sizeof(desc));
		desc.Width = _width;
		desc.Height = _height;
		desc.MipLevels = 1;
		desc.ArraySize = 1;
		// 型無しフォーマット変換
		GFX_IMAGE_FORMAT texFormat = CGfxFormatUtility::ConvertTypelessFormat(_format);
		CGfxFormatUtilityDX11::ConvertFormat(desc.Format, texFormat);
		desc.SampleDesc = sampleDesc;
		// データアクセス方からフラグ設定
		CGfxFormatUtilityDX11::ConvertColorBufferUsage(
			desc.Usage,
			*PCast<D3D11_BIND_FLAG*>(&desc.BindFlags),
			*PCast<D3D11_CPU_ACCESS_FLAG*>(&desc.CPUAccessFlags),
			*PCast<D3D11_RESOURCE_MISC_FLAG*>(&desc.MiscFlags),
			GFX_USAGE_NONE, (sampleDesc.Count > 1)
			);
		//そのバックバッファから描画ターゲット生成
		if (FAILED(pDeviceDX11->CreateTexture2D(&desc, NULL, &m_object.pTex)))
		{
			PRINT_ERROR("*****[ERROR] CColorBuffer::Create() Failed CreateTexture2D\n");
			libAssert(0);
			return false;
		}
	}

	// レンダーターゲットビュー作成
	{
		D3D11_RENDER_TARGET_VIEW_DESC desc;
		::memset(&desc, 0, sizeof(desc));
		desc.Format = dxFormat;
		desc.ViewDimension = sampleDesc.Count > 1 ? D3D11_RTV_DIMENSION_TEXTURE2DMS : D3D11_RTV_DIMENSION_TEXTURE2D;
		desc.Texture2D.MipSlice = 0;
		//そのバックバッファから描画ターゲット生成
		if (FAILED(pDeviceDX11->CreateRenderTargetView(m_object.pTex, &desc, &m_object.pView)))
		{
			PRINT_ERROR("*****[ERROR] CColorBuffer::Create() Failed CreateRenderTargetView\n");
			libAssert(0);
			return false;
		}
	}

	m_width = _width;
	m_height = _height;
	m_format = _format;
	m_aa = _aa;

	return (m_object.pView != NULL && m_object.pTex != NULL);
}


///-------------------------------------------------------------------------------------------------
/// スワップバッファから生成
b8		CColorBuffer::InitReferenceSwap(SWAP_OBJECT& _swap, u32 _swapIdx, u16 _width, u16 _height, GFX_IMAGE_FORMAT _format, GFX_ANTI_ALIASE _aa)
{
	libAssert(_swap.pBuf);

	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	ID3D11Device* pDeviceDX11 = pDevice->GetDevice();
	libAssert(pDeviceDX11);

	// そのスワップチェインのバックバッファ取得
	ID3D11Texture2D* pTex = NULL;
	if (FAILED(_swap.pBuf->GetBuffer(_swapIdx, __uuidof(ID3D11Texture2D), RCast<void**>(&pTex))))
	{
		PRINT_ERROR("*****[ERROR] CDisplay::Create() Failed GetBuffer\n");
		libAssert(0);
		return false;
	}

	// ターゲットビューを保存
	ID3D11RenderTargetView* pRTV = NULL;
	// レンダーターゲットビュー作成
	{
		//そのバックバッファから描画ターゲット生成
		D3D11_RENDER_TARGET_VIEW_DESC* pdesc = NULL;
		if (FAILED(pDeviceDX11->CreateRenderTargetView(pTex, pdesc, &pRTV)))
		{
			PRINT_ERROR("*****[ERROR] CDisplay::Create() Failed CreateRenderTargetView\n");
			libAssert(0);
			return false;
		}
	}

	m_object.pView = pRTV;
	m_object.pTex = pTex;
	m_width = _width;
	m_height = _height;
	m_format = _format;
	m_aa = _aa;
	return true;
}


///-------------------------------------------------------------------------------------------------
///	破棄
void	CColorBuffer::Release()
{
	SAFE_RELEASE(m_object.pView);
	SAFE_RELEASE(m_object.pTex);
	*this = CColorBuffer();
}


POISON_END


#endif // LIB_GFX_DX11

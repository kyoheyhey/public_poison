﻿
#ifdef LIB_GFX_DX11

///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "object/blend_state.h"

#include "device/device_utility.h"
#include "dx11/device/win/device_dx11.h"
#include "dx11/format/gfx_format_utility_dx11.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///*************************************************************************************************
///	ブレンドステート
///*************************************************************************************************
CBlendState::CBlendState()
{
}

CBlendState::~CBlendState()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		CBlendState::IsEnable() const
{
	return (m_object.pState != NULL);
}

///-------------------------------------------------------------------------------------------------
/// ステート生成関数
ID3D11BlendState*	BlendStateCreate(const BLEND_STATE_DESC* _pDesc, u32 _num)
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);

	ID3D11BlendState* pBlendState = NULL;
	// desc取得
	D3D11_BLEND_DESC desc;
	// デフォルト値
	static const CD3D11_BLEND_DESC s_defaultDesc = CD3D11_BLEND_DESC(CD3D11_DEFAULT());
	// デフォルト値で初期化
	desc = s_defaultDesc;
#if 0
	// 現在の設定を取得
	{
		f32 factor[4];
		u32 sampleMask;
		pDevice->GetContext()->OMGetBlendState(&_pBlendState, factor, &sampleMask);
	}
	if (_pBlendState)
	{
		_pBlendState->GetDesc(&desc);
		// 取得したらカウンタ上がるので解放
		SAFE_RELEASE(_pBlendState);
	}
#endif // 0
	// ピクセルをレンダーターゲットに設定するときに、アルファトゥカバレッジをマルチサンプリングテクニックとして使用するかどうか
	desc.AlphaToCoverageEnable = false;
	// 同時処理のレンダーターゲットで独立したブレンディングを有効にする際はTRUEにする
	desc.IndependentBlendEnable = (_num > 1);
	for (u32 slot = 0; slot < _num; slot++)
	{
		desc.RenderTarget[slot].BlendEnable = _pDesc[slot].enable;
		CGfxFormatUtilityDX11::ConvertBlendOp(desc.RenderTarget[slot].BlendOp, _pDesc[slot].colorOp);
		CGfxFormatUtilityDX11::ConvertBlendFunc(desc.RenderTarget[slot].SrcBlend, _pDesc[slot].colorSrcFunc);
		CGfxFormatUtilityDX11::ConvertBlendFunc(desc.RenderTarget[slot].DestBlend, _pDesc[slot].colorDstFunc);
		CGfxFormatUtilityDX11::ConvertBlendOp(desc.RenderTarget[slot].BlendOpAlpha, _pDesc[slot].alphaOp);
		CGfxFormatUtilityDX11::ConvertBlendFunc(desc.RenderTarget[slot].SrcBlendAlpha, _pDesc[slot].alphaSrcFunc);
		CGfxFormatUtilityDX11::ConvertBlendFunc(desc.RenderTarget[slot].DestBlendAlpha, _pDesc[slot].alphaDstFunc);
		CGfxFormatUtilityDX11::ConvertColorMask(*PCast<D3D11_COLOR_WRITE_ENABLE*>(&desc.RenderTarget[slot].RenderTargetWriteMask), _pDesc[slot].colorMask);
	}

	// ブレンドステート生成
	HRESULT hr = pDevice->GetDevice()->CreateBlendState(&desc, &pBlendState);
	if (FAILED(hr))
	{
#ifndef POISON_RELEASE
		CDeviceDX11::PrintError(hr, "CreateBlendState()");
#endif //not POISON_RELEASE
	}
	return pBlendState;
}

///-------------------------------------------------------------------------------------------------
/// 生成
b8		CBlendState::Create(const BLEND_STATE_DESC& _desc)
{
	// ブレンドステート
	ID3D11BlendState* pBlendState = BlendStateCreate(&_desc, 1);
	if (pBlendState == NULL)
	{
		return false;
	}

	m_object.pState = pBlendState;
	memcpy(m_desc, &_desc, sizeof(_desc));
	return true;
}

b8		CBlendState::Create(const BLEND_STATE_DESC(&_desc)[RENDER_BUFFER_SLOT_NUM])
{
	// ブレンドステート
	ID3D11BlendState* pBlendState = BlendStateCreate(_desc, ARRAYOF(_desc));
	if (pBlendState == NULL)
	{
		return false;
	}

//	// そのブレンディングをコンテキストに設定
//	// blendFactor : D3D11_BLEND_BLEND_FACTOR または D3D11_BLEND_INV_BLEND_FACTOR を設定しない限り無効な値。RGBA値を設定する。
//	f32 blendFactor[] = { 1.0f, 1.0f, 1.0f, 1.0f };	// !!!! ブレンド係数を外部より指定させる必要は？
//	u32 sampleMask = 0xffffffff;					// !!!! サンプル マスクの指定は必要ない？
//	pDevice->GetContext()->OMSetBlendState(pBlendState, blendFactor, sampleMask);

	m_object.pState = pBlendState;
	memcpy(m_desc, _desc, sizeof(_desc));
	return true;
}

///-------------------------------------------------------------------------------------------------
/// 破棄
void	CBlendState::Release()
{
	// 破棄
	SAFE_RELEASE(m_object.pState);
	*this = CBlendState();
}



POISON_END

#endif // LIB_GFX_DX11

﻿
#ifdef LIB_GFX_DX11

///-------------------------------------------------------------------------------------------------
// include
#include "object/depth_buffer.h"

#include "device/device_utility.h"
#include "dx11/device/win/device_dx11.h"
#include "format/gfx_format_utility.h"
#include "dx11/format/gfx_format_utility_dx11.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
// static



///*************************************************************************************************
///	デプスバッファ
///*************************************************************************************************
CDepthBuffer::CDepthBuffer()
	: m_width(0)
	, m_height(0)
	, m_arrayNum(0)
	, m_format(GFX_FORMAT_UNKNOWN)
	, m_aa(GFX_AA_UNKNOWN)
{
}
CDepthBuffer::~CDepthBuffer()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		CDepthBuffer::IsEnable() const
{
	return (m_object.pTex != NULL);
}

///-------------------------------------------------------------------------------------------------
///	生成
b8		CDepthBuffer::Create(u16 _width, u16 _height, GFX_IMAGE_FORMAT _format, GFX_ANTI_ALIASE _aa, u32 _num/* = 1*/, GFX_USAGE _usage/* = GFX_USAGE_NONE*/, GFX_BUFFER_ATTR _attr/* = GFX_BUFFER_ATTR_RO*/)
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);

	// フォーマットコンバート
	DXGI_FORMAT dxFormat = DXGI_FORMAT_UNKNOWN;
	CGfxFormatUtilityDX11::ConvertFormat(dxFormat, _format);
	u32 dxAA = CGfxFormatUtility::ConvertAA(_aa);

	// マルチサンプル
	DXGI_SAMPLE_DESC sampleDesc;
	pDevice->CheckMultisampleQualityLevels(sampleDesc, dxFormat, dxAA);

	//デプスステンシル用テクスチャー作成
	{
		D3D11_TEXTURE2D_DESC desc;
		::memset(&desc, 0, sizeof(desc));
		desc.Width = _width;
		desc.Height = _height;
		desc.MipLevels = 1;
		desc.ArraySize = _num;
		// 型無しフォーマット変換
		GFX_IMAGE_FORMAT texFormat = CGfxFormatUtility::ConvertTypelessFormat(_format);
		CGfxFormatUtilityDX11::ConvertFormat(desc.Format, texFormat);
		desc.SampleDesc = sampleDesc;
		// データアクセス方からフラグ設定
		CGfxFormatUtilityDX11::ConvertDepthBufferUsage(
			desc.Usage,
			*PCast<D3D11_BIND_FLAG*>(&desc.BindFlags),
			*PCast<D3D11_CPU_ACCESS_FLAG*>(&desc.CPUAccessFlags),
			*PCast<D3D11_RESOURCE_MISC_FLAG*>(&desc.MiscFlags),
			GFX_USAGE_NONE, (sampleDesc.Count > 1)
			);
		//MEMO: バッファの生成が出来ない。D3D11_BIND_DEPTH_STENCILとUAVの共存不可能？その記述が見つからない。暗黙の了解？サンプルも見つからない
		// UAVはDXGI_FORMAT_R32_FLOAT等でないと生成できないので、UAV作るとき変換する必要有り。
		// [フォーマット対応表] https://msdn.microsoft.com/en-us/library/windows/desktop/ff471325(v=vs.85).aspx
		// ここに書いてある限りでは、マルチサンプルをしないバッファかテクスチャかテクスチャ配列ならいいよって書いてあるのになぁ…。。
		// [新しいリソースの紹介] https://msdn.microsoft.com/ja-jp/library/windows/desktop/ff476335(v=vs.85).aspx#Unordered_Access
		// デプステクスチャ生成
		if (FAILED(pDevice->GetDevice()->CreateTexture2D(&desc, NULL, &m_object.pTex)))
		{
			PRINT_ERROR("*****[ERROR] CDepthBuffer::Create() Failed CreateTexture2D\n");
			libAssert(0);
			return false;
		}
	}

	//ステンシルターゲット作成
	{
		D3D11_DEPTH_STENCIL_VIEW_DESC desc;
		desc.Format = dxFormat;
		desc.Flags = 0;
		D3D11_TEXTURE2D_DESC srcDesc;
		m_object.pTex->GetDesc(&srcDesc);
		// デプステクスチャがtexture配列かどうかでパラメタ設定
		if (srcDesc.ArraySize > 1)
		{
			desc.ViewDimension = srcDesc.SampleDesc.Count > 1 ? D3D11_DSV_DIMENSION_TEXTURE2DMSARRAY : D3D11_DSV_DIMENSION_TEXTURE2DARRAY;
			desc.Texture2DArray.ArraySize = _num;
			desc.Texture2DArray.FirstArraySlice = 0;
			desc.Texture2DArray.MipSlice = 0;
		}
		else
		{
			desc.ViewDimension = srcDesc.SampleDesc.Count > 1 ? D3D11_DSV_DIMENSION_TEXTURE2DMS : D3D11_DSV_DIMENSION_TEXTURE2D;
			desc.Texture2D.MipSlice = 0;
		}

		// ステンシルビュー作成
		if (FAILED(pDevice->GetDevice()->CreateDepthStencilView(m_object.pTex, &desc, &m_object.pView)))
		{
			// srcDesc.ArraySize を _useArrayIndex+_useNum がオーバーすると CreateDepthStencilView に失敗するので注意
			PRINT_ERROR("*****[ERROR] CDepthBuffer::Create() Failed CreateDepthStencilView\n");
			libAssert(0);
			return false;
		}
	}

	m_width = _width;
	m_height = _height;
	m_arrayNum = _num;
	m_format = _format;
	m_aa = _aa;

	return (m_object.pView != NULL && m_object.pTex != NULL);
}

///-------------------------------------------------------------------------------------------------
///	破棄
void	CDepthBuffer::Release()
{
	// 破棄
	SAFE_RELEASE(m_object.pView);
	SAFE_RELEASE(m_object.pTex);
	*this = CDepthBuffer();
}


POISON_END


#endif // LIB_GFX_DX11

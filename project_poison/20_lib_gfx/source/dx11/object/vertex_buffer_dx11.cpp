﻿
#ifdef LIB_GFX_DX11

///---------------------------------------------------------------------------------------------------------------
// include
#include "object/vertex_buffer.h"

#include "device/device_utility.h"
#include "dx11/device/win/device_dx11.h"
#include "dx11/format/gfx_format_utility_dx11.h"

///---------------------------------------------------------------------------------------------------------------
// static


POISON_BGN

///*************************************************************************************************
///	頂点バッファ
///*************************************************************************************************
CVertexBuffer::CVertexBuffer()
	: m_elemSize(0)
	, m_elemNum(0)
{
}
CVertexBuffer::~CVertexBuffer()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		CVertexBuffer::IsEnable() const
{
	return (m_object.pBuf != NULL);
}

///---------------------------------------------------------------------------------------------------------------
///	生成
b8		CVertexBuffer::Create(const VTX_DESC& _desc, DRAW_CONTEXT* _pDrawContext/* = NULL*/)
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);

	D3D11_BUFFER_DESC bufferDesc;
	bufferDesc.ByteWidth = _desc.elementSize * _desc.elementNum;
	// データアクセス方からフラグ設定
	CGfxFormatUtilityDX11::ConvertVertexUsage(
		bufferDesc.Usage,
		*PCast<D3D11_BIND_FLAG*>(&bufferDesc.BindFlags),
		*PCast<D3D11_CPU_ACCESS_FLAG*>(&bufferDesc.CPUAccessFlags),
		*PCast<D3D11_RESOURCE_MISC_FLAG*>(&bufferDesc.MiscFlags),
		_desc.usage
		);
	bufferDesc.StructureByteStride = _desc.elementSize;

	D3D11_SUBRESOURCE_DATA subResourceData;
	subResourceData.pSysMem = _desc.data;
	subResourceData.SysMemPitch = 0;
	subResourceData.SysMemSlicePitch = 0;

	// バッファ生成
	HRESULT hr = pDevice->GetDevice()->CreateBuffer(&bufferDesc, (_desc.data ? &subResourceData : NULL), &m_object.pBuf);
	if (FAILED(hr))
	{
#ifndef POISON_RELEASE
		CDeviceDX11::PrintError(hr, "CreateBuffer()");
		libAssert(0);
#endif//not POISON_RELEASE
		return false;
	}

	m_elemNum = _desc.elementNum;
	m_elemSize = _desc.elementSize;

	return true;
}

///---------------------------------------------------------------------------------------------------------------
///	破棄
void	CVertexBuffer::Release()
{
	// 破棄
	SAFE_RELEASE(m_object.pBuf);
	*this = CVertexBuffer();
}

///---------------------------------------------------------------------------------------------------------------
///	更新
void	CVertexBuffer::Update(u32 _size, const void* _data, DRAW_CONTEXT* _pDrawContext/* = NULL*/)
{
	DRAW_CONTEXT* pContext = NULL;
	if (_pDrawContext)
	{
		pContext = _pDrawContext;
	}
	else
	{
		CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
		libAssert(pDevice);
		pContext = pDevice->GetContext();
	}
	libAssert(pContext);

	// マップ
	D3D11_MAP mapType = _data ? D3D11_MAP_WRITE_DISCARD : D3D11_MAP_READ;
	D3D11_MAPPED_SUBRESOURCE mapRes;
	if (SUCCEEDED(pContext->Map(m_object.pBuf, 0, mapType, 0, &mapRes)))
	{
		// 更新
		memcpy(mapRes.pData, _data, _size);
		// アンマップ
		pContext->Unmap(m_object.pBuf, 0);
	}
}

///---------------------------------------------------------------------------------------------------------------
///	バインド
//void	CVertexBuffer::Bind() const
//{
//	glBindBuffer(VERTEX_BUFFER, m_buffer.bufferObject);
//}

////---------------------------------------------------------------------------------------------------------------
///	アンバインド
//void	CVertexBuffer::UnBind() const
//{
//	glBindBuffer(VERTEX_BUFFER, NULL);
//}

///---------------------------------------------------------------------------------------------------------------
///	頂点属性配列有効
//void	CVertexBuffer::Enable(u32 _slot) const
//{
//	glEnableVertexAttribArray(_slot);
//}

///---------------------------------------------------------------------------------------------------------------
///	頂点属性配列無効
//void	CVertexBuffer::Disable(u32 _slot) const
//{
//	glDisableVertexAttribArray(_slot);
//}


POISON_END


#endif // LIB_GFX_DX11

﻿
#ifdef LIB_GFX_DX11

///---------------------------------------------------------------------------------------------------------------
// include
#include "object/uniform_buffer.h"

#include "device/device_utility.h"
#include "dx11/device/win/device_dx11.h"

#include "dx11/renderer/renderer_dx11.h"

#include "format/gfx_format_utility.h"
#include "dx11/format/gfx_format_utility_dx11.h"

///---------------------------------------------------------------------------------------------------------------
// static


POISON_BGN

///*************************************************************************************************
///	定数バッファ
///*************************************************************************************************
CUniformBuffer::CUniformBuffer()
	: m_elemSize(0)
	, m_elemNum(0)
	, m_attr(GFX_BUFFER_ATTR_CONSTANT)
{
}

CUniformBuffer::~CUniformBuffer()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		CUniformBuffer::IsEnable() const
{
	return (m_object.pBuf != NULL);
}

///---------------------------------------------------------------------------------------------------------------
///	生成
b8		CUniformBuffer::Create(const UNIFORM_DESC& _desc, DRAW_CONTEXT* _pDrawContext/* = NULL*/)
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	ID3D11Device* pDeviceDX11 = pDevice->GetDevice();
	libAssert(pDeviceDX11);

	{
		D3D11_BUFFER_DESC bufDesc;
		// 初期化
		bufDesc.ByteWidth = _desc.elementSize * _desc.elementNum;
		// データアクセス方からフラグ設定
		CGfxFormatUtilityDX11::ConvertUniformUsage(
			bufDesc.Usage,
			*PCast<D3D11_BIND_FLAG*>(&bufDesc.BindFlags),
			*PCast<D3D11_CPU_ACCESS_FLAG*>(&bufDesc.CPUAccessFlags),
			*PCast<D3D11_RESOURCE_MISC_FLAG*>(&bufDesc.MiscFlags),
			_desc.usage, _desc.attr
			);
		bufDesc.StructureByteStride = _desc.elementSize;

		D3D11_SUBRESOURCE_DATA subResourceData;
		subResourceData.pSysMem = _desc.data;
		subResourceData.SysMemPitch = 0;
		subResourceData.SysMemSlicePitch = 0;

		// バッファ生成
		HRESULT hr = pDeviceDX11->CreateBuffer(&bufDesc, (_desc.data ? &subResourceData : NULL), &m_object.pBuf);
		if (FAILED(hr))
		{
#ifndef POISON_RELEASE
			CDeviceDX11::PrintError(hr, "CreateBuffer()");
			libAssert(0);
#endif//not POISON_RELEASE
			return false;
		}
	}

	if (_desc.attr == GFX_BUFFER_ATTR_RO)
	{
		// SRV(読み込みバッファ)生成準備
		D3D11_SHADER_RESOURCE_VIEW_DESC srDesc;
		srDesc.Format = DXGI_FORMAT_UNKNOWN;
		srDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
		srDesc.Buffer.ElementOffset = 0;
		srDesc.Buffer.ElementWidth = _desc.elementNum;

		// 生成
		HRESULT hr = pDeviceDX11->CreateShaderResourceView(m_object.pBuf, &srDesc, &m_roObject.pBuf);
		if (FAILED(hr))
		{
#ifndef POISON_RELEASE
			CDeviceDX11::PrintError(hr, "CreateShaderResourceView()");
#endif //not POISON_RELEASE
			return false;
		}
	}

	if (_desc.attr == GFX_BUFFER_ATTR_RW)
	{
		// UAV(読み書きバッファ)生成準備
		D3D11_UNORDERED_ACCESS_VIEW_DESC uaDesc;
		uaDesc.Format = DXGI_FORMAT_UNKNOWN;
		uaDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
		uaDesc.Buffer.FirstElement = 0;
		uaDesc.Buffer.Flags = 0;
		uaDesc.Buffer.NumElements = _desc.elementNum;

		// 生成
		HRESULT hr = pDeviceDX11->CreateUnorderedAccessView(m_object.pBuf, &uaDesc, &m_rwObject.pBuf);
		if (FAILED(hr))
		{
#ifndef POISON_RELEASE
			CDeviceDX11::PrintError(hr, "CreateUnorderedAccessView()");
#endif //not POISON_RELEASE
			return false;
		}
	}

	m_elemNum = _desc.elementNum;
	m_elemSize = _desc.elementSize;
	m_attr = _desc.attr;

	return true;
}

///---------------------------------------------------------------------------------------------------------------
///	破棄
void	CUniformBuffer::Release()
{
	// 破棄
	SAFE_RELEASE(m_object.pBuf);
	SAFE_RELEASE(m_roObject.pBuf);
	SAFE_RELEASE(m_rwObject.pBuf);
	*this = CUniformBuffer();
}

///---------------------------------------------------------------------------------------------------------------
///	更新
void	CUniformBuffer::Update(u32 _size, const void* _data, DRAW_CONTEXT* _pDrawContext/* = NULL*/)
{
	DRAW_CONTEXT* pContext = NULL;
	if (_pDrawContext)
	{
		pContext = _pDrawContext;
	}
	else
	{
		CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
		libAssert(pDevice);
		pContext = pDevice->GetContext();
	}
	libAssert(pContext);

	// マップ
	D3D11_MAP mapType = _data ? D3D11_MAP_WRITE_DISCARD : D3D11_MAP_READ;
	D3D11_MAPPED_SUBRESOURCE mapRes;

	if (SUCCEEDED(pContext->Map(m_object.pBuf, 0, mapType, 0, &mapRes)))
	{
		// 更新
		memcpy(mapRes.pData, _data, _size);
		// アンマップ
		pContext->Unmap(m_object.pBuf, 0);
	}
}

///---------------------------------------------------------------------------------------------------------------
///	バインド
//void	CUniformBuffer::Bind(UNIFORM_SLOT _slot) const
//{
//	//glBindBuffer(UNIFORM_BUFFER, m_buffer.bufferObject);
//	glBindBufferBase(UNIFORM_BUFFER, _slot, m_buffer.bufferObject);
//}

///---------------------------------------------------------------------------------------------------------------
///	アンバインド
//void	CUniformBuffer::UnBind(UNIFORM_SLOT _slot) const
//{
//	//glBindBuffer(UNIFORM_BUFFER, NULL);
//	glBindBufferBase(UNIFORM_BUFFER, _slot, NULL);
//}

///---------------------------------------------------------------------------------------------------------------
///	バッファ未使用バインド
//void	CUniformBuffer::BindVec3(UNIFORM_SLOT _slot, const f32* _data)
//{
//	glUniform3fv(_slot, 1, _data);
//}
//void	CUniformBuffer::BindVec4(UNIFORM_SLOT _slot, const f32* _data)
//{
//	glUniform4fv(_slot, 1, _data);
//}
//void	CUniformBuffer::BindMtx33(UNIFORM_SLOT _slot, const f32* _data)
//{
//	glUniformMatrix3fv(_slot, 1, true, _data);
//}
//void	CUniformBuffer::BindMtx44(UNIFORM_SLOT _slot, const f32* _data)
//{
//	glUniformMatrix4fv(_slot, 1, true, _data);
//}


POISON_END


#endif // LIB_GFX_DX11

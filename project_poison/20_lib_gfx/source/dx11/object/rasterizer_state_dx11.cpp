﻿
#ifdef LIB_GFX_DX11

///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "object/rasterrizer_state.h"

#include "device/device_utility.h"
#include "dx11/device/win/device_dx11.h"
#include "dx11/format/gfx_format_utility_dx11.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///-------------------------------------------------------------------------------------------------
/// 半浮動小数変換(※ほかでも使うようなら汎用的なとこに持ってく)
typedef u16	f16;
f16	ConvertFloat32toFloat16(f32 _f)
{
	u32 n = *PCast<u32*>(&_f);

	// The sign bit is MSB in common.
	u16 sign_bit = (n >> 16) & 0x8000;

	// The exponent of IEEE 754's float 32 is biased +127 , so we change this bias into +15 and limited to 5-bit.
	u16 exponent = (((n >> 23) - 127 + 15) & 0x1f) << 10;

	// The fraction is limited to 10-bit.
	u16 fraction = (n >> (23 - 10)) & 0x3ff;

	f16 ret;
	ret = sign_bit | exponent | fraction;
	return ret;
}
f32	ConvertFloat16toFloat32(f16 _v)
{
	u32 sign_bit = (_v & 0x8000) << 16;
	u32 exponent = ((((_v >> 10) & 0x1f) - 15 + 127) & 0xff) << 23;
	u32 fraction = (_v & 0x3ff) << (23 - 10);

	u32 n = sign_bit | exponent | fraction;
	return *PCast<f32*>(&n);
}


///-------------------------------------------------------------------------------------------------
/// フォーマット別デプスバイアス値計算
s32		CalcDepthBias(f32 _bias, GFX_IMAGE_FORMAT _format)
{
	switch (_format)
	{
	case GFX_FORMAT_R32G32B32A32_TYPELESS:
	case GFX_FORMAT_R32G32B32A32_UINT:
	case GFX_FORMAT_R32G32B32_TYPELESS:
	case GFX_FORMAT_R32G32B32_UINT:
	case GFX_FORMAT_R32G32_TYPELESS:
	case GFX_FORMAT_R32G32_UINT:
	case GFX_FORMAT_R32G8X24_TYPELESS:
	case GFX_FORMAT_X32_TYPELESS_G8X24_UINT:
	case GFX_FORMAT_R32_TYPELESS:
	case GFX_FORMAT_R32_UINT:
		return SCast<u32>(_bias);

	case GFX_FORMAT_R32G32B32A32_SINT:
	case GFX_FORMAT_R32G32B32_SINT:
	case GFX_FORMAT_R32G32_SINT:
	case GFX_FORMAT_R32_SINT:
		return SCast<s32>(_bias);

	case GFX_FORMAT_R32G32B32A32_FLOAT:
	case GFX_FORMAT_R32G32B32_FLOAT:
	case GFX_FORMAT_R32G32_FLOAT:
	case GFX_FORMAT_D32_FLOAT_S8X24_UINT:
	case GFX_FORMAT_R32_FLOAT_X8X24_TYPELESS:
	case GFX_FORMAT_R32_FLOAT:
	case GFX_FORMAT_D32_FLOAT:
		return *PCast<s32*>(&_bias);

	case GFX_FORMAT_R16G16B16A16_TYPELESS:
	case GFX_FORMAT_R16G16B16A16_UINT:
	case GFX_FORMAT_R16G16_TYPELESS:
	case GFX_FORMAT_R16G16_UINT:
	case GFX_FORMAT_R16_TYPELESS:
	case GFX_FORMAT_R16_UINT:
		return SCast<u16>(_bias);

	case GFX_FORMAT_R16G16B16A16_SINT:
	case GFX_FORMAT_R16G16_SINT:
	case GFX_FORMAT_R16_SINT:
		return SCast<s16>(_bias);

	case GFX_FORMAT_R16G16B16A16_UNORM:
	case GFX_FORMAT_D16_UNORM:
	case GFX_FORMAT_R16_UNORM:
		return SCast<s32>(_bias * ((1u >> 16) - 1));

	case GFX_FORMAT_R16G16B16A16_SNORM:
	case GFX_FORMAT_R16G16_SNORM:
	case GFX_FORMAT_R16_SNORM:
		return SCast<s32>(_bias * ((1u >> 8) - 1));

	case GFX_FORMAT_R16G16B16A16_FLOAT:
	case GFX_FORMAT_R16G16_FLOAT:
	case GFX_FORMAT_R16_FLOAT:
	{
		f16 r16 = ConvertFloat32toFloat16(_bias);
		return *PCast<s32*>(&r16);
	}

	case GFX_FORMAT_R10G10B10A2_TYPELESS:
	case GFX_FORMAT_R10G10B10A2_UINT:
		return SCast<u16>(_bias);

	case GFX_FORMAT_R10G10B10A2_UNORM:
		return SCast<s32>(_bias * ((1u >> 10) - 1));

	case GFX_FORMAT_R11G11B10_FLOAT:
		break;	// わかりましぇん・・・

	case GFX_FORMAT_R8G8B8A8_TYPELESS:
	case GFX_FORMAT_R8G8B8A8_UINT:
	case GFX_FORMAT_R8G8_TYPELESS:
	case GFX_FORMAT_R8G8_UINT:
	case GFX_FORMAT_R8_TYPELESS:
	case GFX_FORMAT_R8_UINT:
		return SCast<u8>(_bias);

	case GFX_FORMAT_R8G8B8A8_SINT:
	case GFX_FORMAT_R8G8_SINT:
	case GFX_FORMAT_R8_SINT:
		return SCast<s8>(_bias);

	case GFX_FORMAT_R8G8B8A8_UNORM:
	case GFX_FORMAT_R8G8B8A8_UNORM_SRGB:
	case GFX_FORMAT_R8G8_UNORM:
	case GFX_FORMAT_R8_UNORM:
	case GFX_FORMAT_A8_UNORM:
		return SCast<s32>(_bias * ((1u >> 8) - 1));

	case GFX_FORMAT_R8G8B8A8_SNORM:
	case GFX_FORMAT_R8G8_SNORM:
	case GFX_FORMAT_R8_SNORM:
		return SCast<s32>(_bias * ((1u >> 4) - 1));

	case GFX_FORMAT_R24G8_TYPELESS:
	case GFX_FORMAT_X24_TYPELESS_G8_UINT:
		return SCast<s32>(_bias);

	case GFX_FORMAT_D24_UNORM_S8_UINT:
	case GFX_FORMAT_R24_UNORM_G8_TYPELESS:
		return SCast<s32>(_bias * ((1u >> 24) - 1));

	default:
		break;
	}
	return 0;
}

///*************************************************************************************************
///	ラスタライザーステート
///*************************************************************************************************
CRasterizer::CRasterizer()
{
}

CRasterizer::~CRasterizer()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		CRasterizer::IsEnable() const
{
	return (m_object.pState != NULL);
}

///-------------------------------------------------------------------------------------------------
/// 生成
b8		CRasterizer::Create(const RASTERIZER_DESC& _desc)
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);

	ID3D11RasterizerState* pRasterizerState = NULL;
	{
		D3D11_RASTERIZER_DESC desc;
		CGfxFormatUtilityDX11::ConvertCullFace(desc.CullMode, _desc.cullFace);
		desc.FrontCounterClockwise = (_desc.frontFace == GFX_FRONT_FACE_CCW);
		// フォーマット別バイアス値変換
		desc.DepthBias = CalcDepthBias(_desc.bepthBias, _desc.depthFormat);
		desc.DepthBiasClamp = _desc.bepthBiasClamp;
		desc.SlopeScaledDepthBias = _desc.slopeScaledDepthBias;
		desc.DepthClipEnable = true;
		desc.ScissorEnable = true;
		desc.FillMode = D3D11_FILL_SOLID;		// 塗りつぶしタイプは固定でええやろ
		desc.MultisampleEnable = _desc.isMSAA;
		desc.AntialiasedLineEnable = _desc.isMSAA;

		// ラスタライザー生成
		HRESULT hr = pDevice->GetDevice()->CreateRasterizerState(&desc, &pRasterizerState);
		if (FAILED(hr))
		{
#ifndef POISON_RELEASE
			CDeviceDX11::PrintError(hr, "CreateRasterizerState()");
#endif //not POISON_RELEASE
			return false;
		}
	}

//	// ラスタライザー設定
//	pDevice->GetContext()->RSSetState(pRasterizerState);

	m_object.pState = pRasterizerState;
	m_desc = _desc;

	return true;
}

///-------------------------------------------------------------------------------------------------
/// 破棄
void	CRasterizer::Release()
{
	// 破棄
	SAFE_RELEASE(m_object.pState);
	*this = CRasterizer();
}

POISON_END

#endif // LIB_GFX_DX11

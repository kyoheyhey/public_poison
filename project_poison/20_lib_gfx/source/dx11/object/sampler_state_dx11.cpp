﻿
#ifdef LIB_GFX_DX11

///-------------------------------------------------------------------------------------------------
// include
#include "object/sampler_state.h"

#include "device/device_utility.h"
#include "dx11/device/win/device_dx11.h"
#include "dx11/format/gfx_format_utility_dx11.h"

POISON_BGN
///-------------------------------------------------------------------------------------------------
// static



///*************************************************************************************************
///	サンプラーステート
///*************************************************************************************************
CSampler::CSampler()
{
}
CSampler::~CSampler()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		CSampler::IsEnable() const
{
	return (m_object.pState != NULL);
}

///-------------------------------------------------------------------------------------------------
/// 生成
b8		CSampler::Create(const SAMPLER_DESC& _desc, DRAW_CONTEXT* _pDrawContext/* = NULL*/)
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);

	D3D11_SAMPLER_DESC samplerDesc;
	CGfxFormatUtilityDX11::ConvertSamplerWrap(samplerDesc.AddressU, _desc.wrapX);
	CGfxFormatUtilityDX11::ConvertSamplerWrap(samplerDesc.AddressV, _desc.wrapY);
	CGfxFormatUtilityDX11::ConvertSamplerWrap(samplerDesc.AddressW, _desc.wrapZ);
	CGfxFormatUtilityDX11::ConvertSamplerFilter(
		samplerDesc.Filter,
		_desc.minFilter, _desc.magFilter, _desc.mipFilter,
		(_desc.anisotropy > 0)
		);
	samplerDesc.MaxAnisotropy = _desc.anisotropy;
	CGfxFormatUtilityDX11::ConvertTestFunc(samplerDesc.ComparisonFunc, _desc.testFunc);
	samplerDesc.MinLOD = _desc.LODMin;
	samplerDesc.MaxLOD = _desc.LODMax;
	samplerDesc.MipLODBias = _desc.LODBias;
	memcpy(samplerDesc.BorderColor, _desc.borderColor, sizeof(samplerDesc.BorderColor));

	// サンプラー生成
	HRESULT hr = pDevice->GetDevice()->CreateSamplerState(&samplerDesc, &m_object.pState);
	if (FAILED(hr))
	{
#ifndef POISON_RELEASE
		CDeviceDX11::PrintError(hr, "CreateSamplerState()");
#endif //not POISON_RELEASE
		return false;
	}
	return true;
}

///-------------------------------------------------------------------------------------------------
/// 破棄
void	CSampler::Release()
{
	// 破棄
	SAFE_RELEASE(m_object.pState);
	*this = CSampler();
}


///-------------------------------------------------------------------------------------------------
// バインド
//void	CSampler::Bind(SAMPLER_SLOT _slot) const
//{
//	glBindSampler(_slot, m_buffer.bufferObject);
//}

///-------------------------------------------------------------------------------------------------
// アンバインド
//void	CSampler::UnBind(SAMPLER_SLOT _slot) const
//{
//	glBindSampler(_slot, NULL);
//}

POISON_END


#endif // LIB_GFX_DX11

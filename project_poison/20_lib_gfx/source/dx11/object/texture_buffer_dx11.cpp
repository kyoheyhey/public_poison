﻿
#ifdef LIB_GFX_DX11

///-------------------------------------------------------------------------------------------------
/// define
//#define USE_DX_GET_FUNC	// DX関数を使ったパラメータ取得


///-------------------------------------------------------------------------------------------------
// include
#include "object/texture_buffer.h"

#include "object/color_buffer.h"
#include "object/depth_buffer.h"

#include "device/device_utility.h"
#include "dx11/device/win/device_dx11.h"

#include "format/gfx_format_utility.h"
#include "dx11/format/gfx_format_utility_dx11.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
// static


///*************************************************************************************************
///	テクスチャバッファ
///*************************************************************************************************
CTextureBuffer::CTextureBuffer()
	: m_textureType(GFX_TEXTURE_UNKNOWN)
	, m_width(0)
	, m_height(0)
	, m_depth(0)
	, m_arrayNum(0)
	, m_mipLevels(0)
	, m_format(GFX_FORMAT_UNKNOWN)
{
}

CTextureBuffer::~CTextureBuffer()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		CTextureBuffer::IsEnable() const
{
	return (m_object.pTex != NULL);
}

///-------------------------------------------------------------------------------------------------
///	取得
u32		CTextureBuffer::GetWidth(u32 _level/* = 0*/) const
{
#ifdef USE_DX_GET_FUNC
	libAssert(m_object.pTex);
	D3D11_TEXTURE2D_DESC desc;
	m_object.pTex->GetDesc(&desc);
	return desc.Width >> _level;
#else
	return m_width >> _level;
#endif // USE_DX_GET_FUNC
}
u32		CTextureBuffer::GetWidth(GFX_CUBEMAP_FACE _face, u32 _level/* = 0*/) const
{
#ifdef USE_DX_GET_FUNC
	libAssert(m_object.pTex);
	D3D11_TEXTURE2D_DESC desc;
	m_object.pTex->GetDesc(&desc);
	return desc.Width >> _level;
#else
	return m_width >> _level;
#endif // USE_DX_GET_FUNC
}

u32		CTextureBuffer::GetHeight(u32 _level/* = 0*/) const
{
#ifdef USE_DX_GET_FUNC
	libAssert(m_object.pTex);
	D3D11_TEXTURE2D_DESC desc;
	m_object.pTex->GetDesc(&desc);
	return desc.Height >> _level;
#else
	return m_height >> _level;
#endif // USE_DX_GET_FUNC
}
u32		CTextureBuffer::GetHeight(GFX_CUBEMAP_FACE _face, u32 _level/* = 0*/) const
{
#ifdef USE_DX_GET_FUNC
	libAssert(m_object.pTex);
	D3D11_TEXTURE2D_DESC desc;
	m_object.pTex->GetDesc(&desc);
	return desc.Height >> _level;
#else
	return m_height >> _level;
#endif // USE_DX_GET_FUNC
}

GFX_IMAGE_FORMAT	CTextureBuffer::GetFormat(u32 _level/* = 0*/) const
{
#ifdef USE_DX_GET_FUNC
	libAssert(m_object.pTex);
	D3D11_TEXTURE2D_DESC desc;
	m_object.pTex->GetDesc(&desc);
	GFX_IMAGE_FORMAT format = GFX_FORMAT_UNKNOWN;
	CGfxFormatUtilityDX11::ConvertFormat(format, desc.Format);
	return format;
#else
	return m_format;
#endif // USE_DX_GET_FUNC
}
GFX_IMAGE_FORMAT	CTextureBuffer::GetFormat(GFX_CUBEMAP_FACE _face, u32 _level/* = 0*/) const
{
#ifdef USE_DX_GET_FUNC
	libAssert(m_object.pTex);
	D3D11_TEXTURE2D_DESC desc;
	m_object.pTex->GetDesc(&desc);
	GFX_IMAGE_FORMAT format = GFX_FORMAT_UNKNOWN;
	CGfxFormatUtilityDX11::ConvertFormat(format, desc.Format);
	return format;
#else
	return m_format;
#endif // USE_DX_GET_FUNC
}

///-------------------------------------------------------------------------------------------------
///	２Ｄテクスチャ生成
b8		CTextureBuffer::Create2D(const TEX_DESC& _desc)
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	ID3D11Device* pDeviceDX11 = pDevice->GetDevice();
	libAssert(pDeviceDX11);

	// フォーマットコンバート
	DXGI_FORMAT dxFormat = DXGI_FORMAT_UNKNOWN;
	CGfxFormatUtilityDX11::ConvertFormat(dxFormat, _desc.format);

	// テクスチャ作成
	ID3D11Texture2D* pTex = NULL;
	{
		D3D11_TEXTURE2D_DESC desc;
		desc.Width = _desc.width;
		desc.Height = _desc.height;
		desc.MipLevels = _desc.mipLevels;
		desc.ArraySize = _desc.arrayNum;
		desc.Format = dxFormat;
		desc.SampleDesc.Count = 1;
		desc.SampleDesc.Quality = 0;
		// データアクセス方からフラグ設定
		CGfxFormatUtilityDX11::ConvertTextureUsage(
			desc.Usage,
			*PCast<D3D11_BIND_FLAG*>(&desc.BindFlags),
			*PCast<D3D11_CPU_ACCESS_FLAG*>(&desc.CPUAccessFlags),
			*PCast<D3D11_RESOURCE_MISC_FLAG*>(&desc.MiscFlags),
			_desc.usage
			);
		D3D11_SUBRESOURCE_DATA* pdata = NULL;
		D3D11_SUBRESOURCE_DATA data[32];	// とりあえず32でええやろ
		::memset(data, 0, sizeof(data));
		if (_desc.pData)
		{
			u32 idxCount = 0;
			// テクスチャ配列
			for (u32 arrayIdx = 0; arrayIdx < _desc.arrayNum; arrayIdx++)
			{
				// mipmap設定
				for (u32 level = 0; level < _desc.mipLevels; level++)
				{
					TEX_DATA_DESC dataDesc = _desc.pData[idxCount];
					if (dataDesc.data)
					{
						// mipmapのサイズは２の乗数なので2で割ってく
						u16 mipWidth = _desc.height >> level;
						u16 mipHeight = _desc.height >> level;
						// ピッチサイズはとりあえず高さを割った値
						u32 pitchSize = dataDesc.size / mipHeight;
						libAssert((pitchSize % mipWidth) == 0);
						data[idxCount].pSysMem = dataDesc.data;
						data[idxCount].SysMemPitch = pitchSize;
						data[idxCount].SysMemSlicePitch = pitchSize * mipHeight;
					}
					idxCount++;
				}
			}
		}
		pdata = data;

		HRESULT hr = pDeviceDX11->CreateTexture2D(&desc, pdata, &pTex);
		if (FAILED(hr))
		{
#ifndef POISON_RELEASE
			CDeviceDX11::PrintError(hr, "Create2D()");
			libAssert(0);
#endif//not POISON_RELEASE
			return false;
		}
	}

	// シェーダーリソースビュー作成
	ID3D11ShaderResourceView* pSRV = NULL;
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC* pdesc = NULL;
		D3D11_SHADER_RESOURCE_VIEW_DESC desc;
		desc.Format = dxFormat;
		if (_desc.arrayNum <= 1)
		{
			desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
			desc.Texture2D.MostDetailedMip = 0;
			desc.Texture2D.MipLevels = _desc.mipLevels;
		}
		else
		{
			desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
			desc.Texture2DArray.MostDetailedMip = 0;
			desc.Texture2DArray.MipLevels = _desc.mipLevels;
			desc.Texture2DArray.FirstArraySlice = 0;
			desc.Texture2DArray.ArraySize = _desc.arrayNum;
		}
		pdesc = &desc;
		HRESULT hr = pDeviceDX11->CreateShaderResourceView(pTex, pdesc, &pSRV);
		if (FAILED(hr))
		{
#ifndef POISON_RELEASE
			CDeviceDX11::PrintError(hr, "CreateShaderResourceView()");
#endif //not POISON_RELEASE
			return false;
		}
	}

	// 読み書き可能なとき
	// アンオーダードアクセスビュー
	ID3D11UnorderedAccessView* pUAV = NULL;
	if (_desc.attr == GFX_BUFFER_ATTR_RW)
	{
		D3D11_UNORDERED_ACCESS_VIEW_DESC desc = {};
		desc.Format = dxFormat;
		if (_desc.arrayNum <= 1)
		{
			desc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
			desc.Texture2D.MipSlice = 0;
		}
		else
		{
			desc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2DARRAY;
			desc.Texture2DArray.MipSlice = 0;
			desc.Texture2DArray.FirstArraySlice = 0;
			desc.Texture2DArray.ArraySize = _desc.arrayNum;
		}
		HRESULT hr = pDeviceDX11->CreateUnorderedAccessView(pTex, &desc, &pUAV);
		if (FAILED(hr))
		{
#ifndef POISON_RELEASE
			CDeviceDX11::PrintError(hr, "CreateUnorderedAccessView()");
#endif //not POISON_RELEASE
			return false;
		}
	}

	m_object.pTex = pTex;
	m_object.pSRV = pSRV;
	m_object.pUAV = pUAV;
	m_textureType = _desc.arrayNum <= 1 ? GFX_TEXTURE_2D : GFX_TEXTURE_2DARRAY;
	m_width = _desc.width;
	m_height = _desc.height;
	m_arrayNum = _desc.arrayNum;
	m_mipLevels = _desc.mipLevels;
	m_format = _desc.format;

	return true;
}

///-------------------------------------------------------------------------------------------------
///	キューブマップ生成
b8		CTextureBuffer::CreateCube(const TEX_DESC& _desc)
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	ID3D11Device* pDeviceDX11 = pDevice->GetDevice();
	libAssert(pDeviceDX11);

	// フォーマットコンバート
	DXGI_FORMAT dxFormat = DXGI_FORMAT_UNKNOWN;
	CGfxFormatUtilityDX11::ConvertFormat(dxFormat, _desc.format);

	// テクスチャ作成
	ID3D11Texture2D* pTex = NULL;
	{
		D3D11_TEXTURE2D_DESC desc;
		desc.Width = _desc.width;
		desc.Height = _desc.height;
		desc.MipLevels = _desc.mipLevels;
		desc.ArraySize = 6;		// 6固定
		desc.Format = dxFormat;
		desc.SampleDesc.Count = 1;
		desc.SampleDesc.Quality = 0;
		// データアクセス方からフラグ設定
		CGfxFormatUtilityDX11::ConvertTextureUsage(
			desc.Usage,
			*PCast<D3D11_BIND_FLAG*>(&desc.BindFlags),
			*PCast<D3D11_CPU_ACCESS_FLAG*>(&desc.CPUAccessFlags),
			*PCast<D3D11_RESOURCE_MISC_FLAG*>(&desc.MiscFlags),
			_desc.usage
			);
		D3D11_SUBRESOURCE_DATA* pdata = NULL;
		D3D11_SUBRESOURCE_DATA data[32];	// とりあえず32でええやろ
		::memset(data, 0, sizeof(data));
		if (_desc.pData)
		{
			u32 idxCount = 0;
			// テクスチャ配列
			for (u32 arrayIdx = 0; arrayIdx < 6; arrayIdx++)
			{
				// mipmap設定
				for (u32 level = 0; level < _desc.mipLevels; level++)
				{
					TEX_DATA_DESC dataDesc = _desc.pData[idxCount];
					if (dataDesc.data)
					{
						// mipmapのサイズは２の乗数なので2で割ってく
						u16 mipWidth = _desc.height >> level;
						u16 mipHeight = _desc.height >> level;
						// ピッチサイズはとりあえず高さを割った値
						u32 pitchSize = dataDesc.size / mipHeight;
						libAssert((pitchSize % mipWidth) == 0);
						data[idxCount].pSysMem = dataDesc.data;
						data[idxCount].SysMemPitch = pitchSize;
						data[idxCount].SysMemSlicePitch = pitchSize * mipHeight;
					}
					idxCount++;
				}
			}
		}
		pdata = data;

		HRESULT hr = pDeviceDX11->CreateTexture2D(&desc, pdata, &pTex);
		if (FAILED(hr))
		{
#ifndef POISON_RELEASE
			CDeviceDX11::PrintError(hr, "Create2D()");
			libAssert(0);
#endif//not POISON_RELEASE
			return false;
		}
	}

	// シェーダーリソースビュー作成
	ID3D11ShaderResourceView* pSRV = NULL;
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC* pdesc = NULL;
		D3D11_SHADER_RESOURCE_VIEW_DESC desc;
		desc.Format = dxFormat;
		desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
		desc.TextureCube.MostDetailedMip = 0;
		desc.TextureCube.MipLevels = _desc.mipLevels;
		pdesc = &desc;
		HRESULT hr = pDeviceDX11->CreateShaderResourceView(pTex, pdesc, &pSRV);
		if (FAILED(hr))
		{
#ifndef POISON_RELEASE
			CDeviceDX11::PrintError(hr, "CreateShaderResourceView()");
#endif //not POISON_RELEASE
			return false;
		}
	}

	// 読み書き可能なとき
	// アンオーダードアクセスビュー
	ID3D11UnorderedAccessView* pUAV = NULL;
	if (_desc.attr == GFX_BUFFER_ATTR_RW)
	{
		D3D11_UNORDERED_ACCESS_VIEW_DESC desc = {};
		desc.Format = dxFormat;
		desc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2DARRAY;
		desc.Texture2DArray.MipSlice = 0;
		desc.Texture2DArray.FirstArraySlice = 0;
		desc.Texture2DArray.ArraySize = 6;
		HRESULT hr = pDeviceDX11->CreateUnorderedAccessView(pTex, &desc, &pUAV);
		if (FAILED(hr))
		{
#ifndef POISON_RELEASE
			CDeviceDX11::PrintError(hr, "CreateUnorderedAccessView()");
#endif //not POISON_RELEASE
			return false;
		}
	}

	m_object.pTex = pTex;
	m_object.pSRV = pSRV;
	m_object.pUAV = pUAV;
	m_textureType = GFX_TEXTURE_CUBEMAP;
	m_width = _desc.width;
	m_height = _desc.height;
	m_arrayNum = 6;
	m_mipLevels = _desc.mipLevels;
	m_format = _desc.format;

	return true;
}

///-------------------------------------------------------------------------------------------------
/// ターゲット参照テクスチャ生成
b8		CTextureBuffer::InitReferenceTarget(const CColorBuffer* _pClr, GFX_BUFFER_ATTR _attr/* = GFX_BUFFER_ATTR_RO*/)
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	ID3D11Device* pDeviceDX11 = pDevice->GetDevice();
	libAssert(pDeviceDX11);

	const COLOR_OBJECT& trgObj = _pClr->GetObject();

	DXGI_FORMAT dxFormat = DXGI_FORMAT_UNKNOWN;
	CGfxFormatUtilityDX11::ConvertFormat(dxFormat, _pClr->GetFormat());
	u32 dxAA = CGfxFormatUtility::ConvertAA(_pClr->GetAA());

	// マルチサンプル
	DXGI_SAMPLE_DESC sampleDesc;
	pDevice->CheckMultisampleQualityLevels(sampleDesc, dxFormat, dxAA);

	// シェーダーリソースビュー作成
	ID3D11ShaderResourceView* pSRV = NULL;
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC* pdesc = NULL;
		D3D11_SHADER_RESOURCE_VIEW_DESC desc;
		desc.Format = dxFormat;
		desc.ViewDimension = sampleDesc.Count > 1 ? D3D11_SRV_DIMENSION_TEXTURE2DMS : D3D11_SRV_DIMENSION_TEXTURE2D;
		desc.Texture2D.MostDetailedMip = 0;
		desc.Texture2D.MipLevels = 1;
		pdesc = &desc;
		HRESULT hr = pDeviceDX11->CreateShaderResourceView(trgObj.pTex, pdesc, &pSRV);
		if (FAILED(hr))
		{
#ifndef POISON_RELEASE
			CDeviceDX11::PrintError(hr, "CreateShaderResourceView()");
#endif //not POISON_RELEASE
			return false;
		}
	}

	// 読み書き可能なとき
	// アンオーダードアクセスビュー
	ID3D11UnorderedAccessView* pUAV = NULL;
	if (_attr == GFX_BUFFER_ATTR_RW)
	{
		D3D11_UNORDERED_ACCESS_VIEW_DESC desc = {};
		desc.Format = dxFormat;
		desc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
		desc.Texture2D.MipSlice = 0;
		HRESULT hr = pDeviceDX11->CreateUnorderedAccessView(trgObj.pTex, &desc, &pUAV);
		if (FAILED(hr))
		{
#ifndef POISON_RELEASE
			CDeviceDX11::PrintError(hr, "CreateUnorderedAccessView()");
#endif //not POISON_RELEASE
			return false;
		}
	}

	m_object.pTex = trgObj.pTex;
	m_object.pSRV = pSRV;
	m_object.pUAV = pUAV;
	m_textureType = GFX_TEXTURE_COLORBUFFER;
	m_width = _pClr->GetWidth();
	m_height = _pClr->GetHeight();
	m_arrayNum = 1;
	m_mipLevels = 1;

	return true;
}

b8		CTextureBuffer::InitReferenceTarget(const CDepthBuffer* _pDep, GFX_BUFFER_ATTR _attr/* = GFX_BUFFER_ATTR_RO*/)
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	ID3D11Device* pDeviceDX11 = pDevice->GetDevice();
	libAssert(pDeviceDX11);

	const DEPTH_OBJECT& trgObj = _pDep->GetObject();

	// リソース用フォーマット変換
	GFX_IMAGE_FORMAT resFormat = CGfxFormatUtility::ConvertDepthToColorFormat(_pDep->GetFormat());
	DXGI_FORMAT dxFormat = DXGI_FORMAT_UNKNOWN;
	CGfxFormatUtilityDX11::ConvertFormat(dxFormat, resFormat);
	u32 dxAA = CGfxFormatUtility::ConvertAA(_pDep->GetAA());

	// マルチサンプル
	DXGI_SAMPLE_DESC sampleDesc;
	pDevice->CheckMultisampleQualityLevels(sampleDesc, dxFormat, dxAA);

	// シェーダーリソースビュー作成
	ID3D11ShaderResourceView* pSRV = NULL;
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC* pdesc = NULL;
		D3D11_SHADER_RESOURCE_VIEW_DESC desc;
		desc.Format = dxFormat;
		if (_pDep->GetArrayNum() <= 1)
		{
			desc.ViewDimension = sampleDesc.Count > 1 ? D3D11_SRV_DIMENSION_TEXTURE2DMS : D3D11_SRV_DIMENSION_TEXTURE2D;
			desc.Texture2D.MostDetailedMip = 0;
			desc.Texture2D.MipLevels = 1;
		}
		else
		{
			desc.ViewDimension = sampleDesc.Count > 1 ? D3D11_SRV_DIMENSION_TEXTURE2DMSARRAY : D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
			desc.Texture2DArray.MostDetailedMip = 0;
			desc.Texture2DArray.MipLevels = 1;
			desc.Texture2DArray.FirstArraySlice = 0;
			desc.Texture2DArray.ArraySize = _pDep->GetArrayNum();
		}
		pdesc = &desc;
		HRESULT hr = pDeviceDX11->CreateShaderResourceView(trgObj.pTex, pdesc, &pSRV);
		if (FAILED(hr))
		{
#ifndef POISON_RELEASE
			CDeviceDX11::PrintError(hr, "CreateShaderResourceView()");
#endif //not POISON_RELEASE
			return false;
		}
	}

	// 読み書き可能なとき
	// アンオーダードアクセスビュー
	ID3D11UnorderedAccessView* pUAV = NULL;
	if (_attr == GFX_BUFFER_ATTR_RW)
	{
		D3D11_UNORDERED_ACCESS_VIEW_DESC desc = {};
		desc.Format = dxFormat;
		if (_pDep->GetArrayNum() <= 1)
		{
			desc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
			desc.Texture2D.MipSlice = 0;
		}
		else
		{
			desc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2DARRAY;
			desc.Texture2DArray.MipSlice = 0;
			desc.Texture2DArray.FirstArraySlice = 0;
			desc.Texture2DArray.ArraySize = _pDep->GetArrayNum();
		}
		HRESULT hr = pDeviceDX11->CreateUnorderedAccessView(trgObj.pTex, &desc, &pUAV);
		if (FAILED(hr))
		{
#ifndef POISON_RELEASE
			CDeviceDX11::PrintError(hr, "CreateUnorderedAccessView()");
#endif //not POISON_RELEASE
			return false;
		}
	}

	m_object.pTex = trgObj.pTex;
	m_object.pSRV = pSRV;
	m_object.pUAV = pUAV;
	m_textureType = GFX_TEXTURE_DEPTHBUFFER;
	m_width = _pDep->GetWidth();
	m_height = _pDep->GetHeight();
	m_arrayNum = _pDep->GetArrayNum();
	m_mipLevels = 1;

	return true;
}

///-------------------------------------------------------------------------------------------------
///	破棄
void	CTextureBuffer::Release()
{
	// 破棄
	SAFE_RELEASE(m_object.pUAV);
	SAFE_RELEASE(m_object.pSRV);
	if (m_object.pTex && !(m_textureType == GFX_TEXTURE_COLORBUFFER || m_textureType == GFX_TEXTURE_DEPTHBUFFER))
	{
		m_object.pTex->Release();
	}
	m_object.pTex = NULL;
	*this = CTextureBuffer();
}

///-------------------------------------------------------------------------------------------------
///	更新
void	CTextureBuffer::Update(
	u32 _array/* = 0*/, u32 _level/* = 0*/,
	u32 _xoff/* = 0*/, u32 _yoff/* = 0*/, u32 _zoff/* = 0*/,
	const void* _pData/* = NULL*/,
	DRAW_CONTEXT* _pDrawContext/* = NULL*/
)
{
	DRAW_CONTEXT* pContext = NULL;
	if (_pDrawContext)
	{
		pContext = _pDrawContext;
	}
	else
	{
		CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
		libAssert(pDevice);
		pContext = pDevice->GetContext();
	}
	libAssert(pContext);

	// desc取得
	D3D11_TEXTURE2D_DESC desc;
	m_object.pTex->GetDesc(&desc);
	GFX_IMAGE_FORMAT format;
	CGfxFormatUtilityDX11::ConvertFormat(format, desc.Format);
	u32 formatSize = CGfxFormatUtility::GetImageFormatSize(format);

	D3D11_BOX destBox;
	destBox.left = _xoff;
	destBox.right = desc.Width >> _level;
	destBox.top = _yoff;
	destBox.bottom = desc.Height >> _level;
	destBox.front = _zoff;
	destBox.back = 1;

	u32 srcIdx = _array * m_mipLevels + _level;
	pContext->UpdateSubresource(
		m_object.pTex, srcIdx,
		&destBox,
		_pData, formatSize * desc.Width >> _level, 0
		);
}
void	CTextureBuffer::Update(
	GFX_CUBEMAP_FACE _face, u32 _level/* = 0*/,
	u32 _xoff/* = 0*/, u32 _yoff/* = 0*/, u32 _zoff/* = 0*/,
	const void* _pData/* = NULL*/,
	DRAW_CONTEXT* _pDrawContext/* = NULL*/
	)
{
	DRAW_CONTEXT* pContext = NULL;
	if (_pDrawContext)
	{
		pContext = _pDrawContext;
	}
	else
	{
		CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
		libAssert(pDevice);
		pContext = pDevice->GetContext();
	}
	libAssert(pContext);

	// desc取得
	D3D11_TEXTURE2D_DESC desc;
	m_object.pTex->GetDesc(&desc);
	GFX_IMAGE_FORMAT format;
	CGfxFormatUtilityDX11::ConvertFormat(format, desc.Format);
	u32 formatSize = CGfxFormatUtility::GetImageFormatSize(format);

	D3D11_BOX destBox;
	destBox.left = _xoff;
	destBox.right = desc.Width >> _level;
	destBox.top = _yoff;
	destBox.bottom = desc.Height >> _level;
	destBox.front = _zoff;
	destBox.back = 1;

	u32 srcIdx = _face * m_mipLevels + _level;
	pContext->UpdateSubresource(
		m_object.pTex, srcIdx,
		&destBox,
		_pData, formatSize * desc.Width >> _level, 0
		);
}



POISON_END


#endif // LIB_GFX_DX11

﻿
#ifdef LIB_GFX_DX11

///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "object/depth_state.h"

#include "device/device_utility.h"
#include "dx11/device/win/device_dx11.h"
#include "dx11/format/gfx_format_utility_dx11.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///*************************************************************************************************
///	デプスステンシルステート
///*************************************************************************************************
CDepthState::CDepthState()
{
}

CDepthState::~CDepthState()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		CDepthState::IsEnable() const
{
	return (m_object.pState != NULL);
}

///-------------------------------------------------------------------------------------------------
/// 生成
b8		CDepthState::Create(const DEPTH_STATE_DESC& _desc, u32 _stencilRef/* = 0*/)
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	ID3D11Device* pDeviceDX11 = pDevice->GetDevice();
	libAssert(pDeviceDX11);

	// デプスステンシルステート
	ID3D11DepthStencilState* pDepthState = NULL;
	{
		D3D11_DEPTH_STENCIL_DESC desc;
		//memset( &desc, 0, sizeof(desc) );

		desc.DepthEnable = _desc.isDepthTest;
		desc.DepthWriteMask = _desc.isDepthWrite ? D3D11_DEPTH_WRITE_MASK_ALL : D3D11_DEPTH_WRITE_MASK_ZERO;
		CGfxFormatUtilityDX11::ConvertTestFunc(desc.DepthFunc, _desc.depthTest);

		desc.StencilEnable = _desc.isStencilTest;
		desc.StencilReadMask = _desc.isStencilWrite ? _desc.stencilReadMask : 0;
		desc.StencilWriteMask = _desc.isStencilWrite ? _desc.stencilWriteMask : 0;

		CGfxFormatUtilityDX11::ConvertStencilOp(desc.FrontFace.StencilFailOp, _desc.stencilFailOp);
		CGfxFormatUtilityDX11::ConvertStencilOp(desc.FrontFace.StencilDepthFailOp, _desc.stencilZFailOp);
		CGfxFormatUtilityDX11::ConvertStencilOp(desc.FrontFace.StencilPassOp, _desc.stencilPassOp);
		CGfxFormatUtilityDX11::ConvertTestFunc(desc.FrontFace.StencilFunc, _desc.stencilTest);

		CGfxFormatUtilityDX11::ConvertStencilOp(desc.BackFace.StencilFailOp, _desc.stencilFailOp);
		CGfxFormatUtilityDX11::ConvertStencilOp(desc.BackFace.StencilDepthFailOp, _desc.stencilZFailOp);
		CGfxFormatUtilityDX11::ConvertStencilOp(desc.BackFace.StencilPassOp, _desc.stencilPassOp);
		CGfxFormatUtilityDX11::ConvertTestFunc(desc.BackFace.StencilFunc, _desc.stencilTest);

		// デプスステンシルステート生成
		HRESULT hr = pDeviceDX11->CreateDepthStencilState(&desc, &pDepthState);
		if (FAILED(hr))
		{
#ifndef POISON_RELEASE
			CDeviceDX11::PrintError(hr, "CreateDepthStencilState()");
#endif //not POISON_RELEASE
			return false;
		}
	}

//	// デプスステンシルステート設定
//	pDevice->GetContext()->OMSetDepthStencilState(pDepthState, _stencilRef);

	m_object.pState = pDepthState;
	m_desc = _desc;

	return true;
}

///-------------------------------------------------------------------------------------------------
/// 破棄
void	CDepthState::Release()
{
	// 破棄
	SAFE_RELEASE(m_object.pState);
	*this = CDepthState();
}



POISON_END

#endif // LIB_GFX_DX11

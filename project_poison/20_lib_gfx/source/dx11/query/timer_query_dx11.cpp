﻿
#ifdef LIB_GFX_DX11

///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "query/timer_query.h"

#include "device/device_utility.h"
#include "dx11/device/win/device_dx11.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant
static b8			s_isUpdateFrequency = false;	///< 更新したかフラグ
static GFX_COUNTER	s_counterDisjoint;				///< DX11用周波数カウンター


///*************************************************************************************************
///	タイマークエリー
///*************************************************************************************************

CTimerQuery::CTimerQuery()
{
}

CTimerQuery::~CTimerQuery()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		CTimerQuery::IsEnable() const
{
	return (m_counter.query[0] != NULL);
}

///-------------------------------------------------------------------------------------------------
/// 初期化
b8		CTimerQuery::Initialize()
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	ID3D11Device* pDeviceDX11 = pDevice->GetDevice();
	libAssert(pDeviceDX11);

	{
		m_counter.Init();
		m_counterEnd.Init();
		// クエリ生成
		D3D11_QUERY_DESC desc;
		desc.Query = D3D11_QUERY_TIMESTAMP;
		desc.MiscFlags = 0;
		for (u32 idx = 0; idx < QUERY_PREF_NUM; idx++)
		{
			HRESULT hr;
			hr = pDeviceDX11->CreateQuery(&desc, &(m_counter.query[idx]));
			if (FAILED(hr))
			{
#ifndef POISON_RELEASE
				CDeviceDX11::PrintError(hr, "CreateQuery()");
				libDebugAssert(0);
#endif	// !POISON_RELEASE
				return false;
			}

			hr = pDeviceDX11->CreateQuery(&desc, &(m_counterEnd.query[idx]));
			if (FAILED(hr))
			{
#ifndef POISON_RELEASE
				CDeviceDX11::PrintError(hr, "CreateQuery()");
				libDebugAssert(0);
#endif	// !POISON_RELEASE
				return false;
			}
		}
	}
	return (m_counter.query[0] != NULL);
}

///-------------------------------------------------------------------------------------------------
/// 終了
void	CTimerQuery::Finalize()
{
	// クエリ破棄
	for (u32 idx = 0; idx < QUERY_PREF_NUM; idx++)
	{
		SAFE_RELEASE(m_counter.query[idx]);
		SAFE_RELEASE(m_counterEnd.query[idx]);
	}
	m_counter.Init();
	m_counterEnd.Init();
}

///-------------------------------------------------------------------------------------------------
/// タイマー取得（秒）
f64		CTimerQuery::GetResultTimeSec() const
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	ID3D11DeviceContext* pContext = pDevice->GetContext();
	libAssert(pContext);

	// 周波数カウンター取得
	f64 frequency = CTimerQuery::GetFrequency();
	if (frequency <= 0.0){ return 0.0; }

	// 時間取得
	u64 time1 = 0, time2 = 0;
	u8 queryIdx1 = m_counter.queryIdx, queryIdx2 = m_counterEnd.queryIdx;
#if 0
	u8 waitCount1 = 0, waitCount2 = 0;
	while (waitCount1 < QUERY_PREF_NUM)
	{
		HRESULT hr = pContext->GetData(m_counter.query[queryIdx1], &time1, sizeof(time1), 0);
		if (hr == S_OK) { break; }
		//if (hr != S_FALSE){ return 0.0; }
		queryIdx1 = GFX_COUNTER::CountUp(queryIdx1);
		waitCount1++;
	}

	while (waitCount2 < QUERY_PREF_NUM)
	{
		HRESULT hr = pContext->GetData(m_counterEnd.query[queryIdx2], &time2, sizeof(time2), 0);
		if (hr == S_OK) { break; }
		//if (hr != S_FALSE){ return 0.0; }
		queryIdx2 = GFX_COUNTER::CountUp(queryIdx2);
		waitCount2++;
	}
#else
	u8 waitCount = 0;
	while (waitCount < QUERY_PREF_NUM)
	{
		HRESULT hr1 = pContext->GetData(m_counter.query[queryIdx1], &time1, sizeof(time1), 0);
		HRESULT hr2 = pContext->GetData(m_counterEnd.query[queryIdx2], &time2, sizeof(time2), 0);
		if (hr1 == S_OK && hr2 == S_OK)
		{
			if (queryIdx1 != queryIdx2)
			{
				queryIdx1 = queryIdx2;
			}
			break;
		}
		//if (hr != S_FALSE){ return 0.0; }
		queryIdx1 = GFX_COUNTER::CountUp(queryIdx1);
		queryIdx2 = GFX_COUNTER::CountUp(queryIdx2);
		waitCount++;
	}
#endif // 0

	if (time2 < time1)
	{
		//	負の値にならないよう対処、エラー値
		return 999999.0f;
	}
	// 秒に変換
	return (SCast<f64>(time2 - time1) / frequency);
}




///-------------------------------------------------------------------------------------------------
/// 周波数取得
f64		CTimerQuery::GetFrequency()
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	ID3D11DeviceContext* pContext = pDevice->GetContext();
	libAssert(pContext);
	static f64 s_frequency = 0.0;

	if (s_isUpdateFrequency)
	{
		D3D11_QUERY_DATA_TIMESTAMP_DISJOINT data;
		u8 queryIdx = s_counterDisjoint.queryIdx;
		u8 waitCount = 0;
		while (waitCount < QUERY_PREF_NUM)
		{
			HRESULT hr = pContext->GetData(s_counterDisjoint.query[queryIdx], &data, sizeof(data), 0);
			// 失敗以外
			if (hr != S_FALSE)
			{
				if (!data.Disjoint && data.Frequency != 0)
				{
					// 頻度保存
					s_frequency = SCast<f64>(data.Frequency);
					s_isUpdateFrequency = false;
				}
				break;
			}
			// 失敗してもすでに取得済みならいったんスルー
			else if (s_frequency != 0.0)
			{
				break;
			}
			queryIdx = GFX_COUNTER::CountUp(queryIdx);
			waitCount++;
		}
	}
	return s_frequency;
}

///-------------------------------------------------------------------------------------------------
/// 周波数計測開始
void	CTimerQuery::FrequencyBgn()
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	ID3D11DeviceContext* pContext = pDevice->GetContext();
	libAssert(pContext);

	pContext->Begin(s_counterDisjoint.GetQuery());
	s_isUpdateFrequency = true;
}

///-------------------------------------------------------------------------------------------------
/// 周波数計測終了
void	CTimerQuery::FrequencyEnd()
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	ID3D11DeviceContext* pContext = pDevice->GetContext();
	libAssert(pContext);

	pContext->End(s_counterDisjoint.GetQuery());
	s_counterDisjoint.CountUp();
}

///-------------------------------------------------------------------------------------------------
/// 周波数カウンター初期化
b8		CTimerQuery::InitFrequency()
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	ID3D11Device* pDeviceDX11 = pDevice->GetDevice();
	libAssert(pDeviceDX11);

	// 周波数カウンター生成
	{
		s_counterDisjoint.Init();
		// クエリ生成
		D3D11_QUERY_DESC desc;
		desc.Query = D3D11_QUERY_TIMESTAMP_DISJOINT;
		desc.MiscFlags = 0;
		for (u32 idx = 0; idx < QUERY_PREF_NUM; idx++)
		{
			HRESULT hr = pDeviceDX11->CreateQuery(&desc, &(s_counterDisjoint.query[idx]));
			if (FAILED(hr))
			{
#ifndef POISON_RELEASE
				CDeviceDX11::PrintError(hr, "CreateQuery()");
				libDebugAssert(0);
#endif	// !POISON_RELEASE
				return false;
			}
		}
	}
	return (s_counterDisjoint.query[0] != NULL);
}

///-------------------------------------------------------------------------------------------------
/// 周波数カウンター終了
void	CTimerQuery::FinFrequency()
{
	// クエリ破棄
	for (u32 idx = 0; idx < QUERY_PREF_NUM; idx++)
	{
		SAFE_RELEASE(s_counterDisjoint.query[idx]);
	}
	s_counterDisjoint.Init();
}


POISON_END

#endif // LIB_GFX_DX11

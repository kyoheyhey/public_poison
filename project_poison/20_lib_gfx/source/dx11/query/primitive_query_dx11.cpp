﻿
#ifdef LIB_GFX_DX11

///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "query/primitive_query.h"

#include "device/device_utility.h"
#include "dx11/device/win/device_dx11.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///*************************************************************************************************
///	プリミティブクエリー
///*************************************************************************************************

CPrimitiveQuery::CPrimitiveQuery()
{
}

CPrimitiveQuery::~CPrimitiveQuery()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		CPrimitiveQuery::IsEnable() const
{
	return (m_counter.query[0] != NULL);
}

///-------------------------------------------------------------------------------------------------
/// 初期化
b8		CPrimitiveQuery::Initialize()
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	ID3D11Device* pDeviceDX11 = pDevice->GetDevice();
	libAssert(pDeviceDX11);

	m_counter.Init();
	// クエリ生成
	D3D11_QUERY_DESC desc;
	desc.Query = D3D11_QUERY_PIPELINE_STATISTICS;
	desc.MiscFlags = 0;
	for (u32 idx = 0; idx < QUERY_PREF_NUM; idx++)
	{
		HRESULT hr = pDeviceDX11->CreateQuery(&desc, &(m_counter.query[idx]));
		if (FAILED(hr))
		{
#ifndef POISON_RELEASE
			CDeviceDX11::PrintError(hr, "CreateQuery()");
			libDebugAssert(0);
#endif	// !POISON_RELEASE
			return false;
		}
	}
	return (m_counter.query[0] != NULL);
}

///-------------------------------------------------------------------------------------------------
/// 終了
void	CPrimitiveQuery::Finalize()
{
	// クエリ破棄
	for (u32 idx = 0; idx < QUERY_PREF_NUM; idx++)
	{
		SAFE_RELEASE(m_counter.query[idx]);
	}
	m_counter.Init();
}

///-------------------------------------------------------------------------------------------------
/// プリミティブ結果取得
u64		CPrimitiveQuery::GetResultPrimitive() const
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	ID3D11DeviceContext* pContext = pDevice->GetContext();
	libAssert(pContext);

	D3D11_QUERY_DATA_PIPELINE_STATISTICS data;
	u8 queryIdx = m_counter.queryIdx;
	u8 waitCount = 0;
	while (waitCount < QUERY_PREF_NUM)
	{
		HRESULT hr = pContext->GetData(m_counter.query[queryIdx], &data, sizeof(data), 0);
		if (hr == S_OK){ break; }
		if (hr != S_FALSE){ return 0; }
		queryIdx = GFX_COUNTER::CountUp(queryIdx);
		waitCount++;
	}
#if 1
	return SCast<u64>(data.CInvocations);
#else
	return SCast<u64>(data.CPrimitives);
#endif // 1
}



POISON_END

#endif // LIB_GFX_DX11

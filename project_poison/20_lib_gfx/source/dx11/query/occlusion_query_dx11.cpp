﻿
#ifdef LIB_GFX_DX11

///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "query/occlusion_query.h"

#include "device/device_utility.h"
#include "dx11/device/win/device_dx11.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///*************************************************************************************************
///	オクルージョンクエリー
///*************************************************************************************************

COcclusionQuery::COcclusionQuery()
	: m_type(GFX_PREDICATE_OCCL_CHEKER)
{
}

COcclusionQuery::~COcclusionQuery()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効チェック
b8		COcclusionQuery::IsEnable() const
{
	return (m_counter.predicate[0] != NULL);
}

///-------------------------------------------------------------------------------------------------
/// 初期化
b8		COcclusionQuery::Initialize(GFX_PREDICATE_QUERY_TYPE _type)
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	ID3D11Device* pDeviceDX11 = pDevice->GetDevice();
	libAssert(pDeviceDX11);

	m_counter.Init();
	for (u32 idx = 0; idx < QUERY_PREF_NUM; idx++)
	{
		switch (_type)
		{
		case GFX_PREDICATE_OCCL_CHEKER:
		{
			D3D11_QUERY_DESC desc;
			desc.Query = D3D11_QUERY_OCCLUSION_PREDICATE;
			desc.MiscFlags = D3D11_QUERY_MISC_PREDICATEHINT;
			HRESULT hr = pDeviceDX11->CreatePredicate(&desc, &(m_counter.predicate[idx]));
			if (FAILED(hr))
			{
#ifndef POISON_RELEASE
				CDeviceDX11::PrintError(hr, "COcclusionQuery::Initialize()");
				libDebugAssert(0);
#endif	// !POISON_RELEASE
				return false;
			}
			break;
		}
		case GFX_PREDICATE_PIXEL_COUNTER:
		{
			D3D11_QUERY_DESC desc;
			desc.Query = D3D11_QUERY_OCCLUSION;
			desc.MiscFlags = 0;
			ID3D11Query* pQuery = NULL;
			HRESULT hr = pDeviceDX11->CreateQuery(&desc, &pQuery);
			if (FAILED(hr))
			{
#ifndef POISON_RELEASE
				CDeviceDX11::PrintError(hr, "COcclusionQuery::Initialize()");
				libDebugAssert(0);
#endif	// !POISON_RELEASE
				return false;
			}
			m_counter.predicate[idx] = PCast<ID3D11Predicate*>(pQuery);
			break;
		}
		default:
			break;
		}
	}
	m_type = _type;
	return (m_counter.predicate[0] != NULL);
}

///-------------------------------------------------------------------------------------------------
/// 終了
void	COcclusionQuery::Finalize()
{
	// クエリ破棄
	for (u32 idx = 0; idx < QUERY_PREF_NUM; idx++)
	{
		SAFE_RELEASE(m_counter.predicate[idx]);
	}
	m_counter.Init();
}

///-------------------------------------------------------------------------------------------------
/// オクルージョン結果取得
b8		COcclusionQuery::GetResultOcclusion() const
{
	//libAssert(m_type == GFX_PREDICATE_OCCL_CHEKER);
	if (m_type == GFX_PREDICATE_OCCL_CHEKER)
	{
		return false;
	}
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	ID3D11DeviceContext* pContext = pDevice->GetContext();
	libAssert(pContext);

	u32 result = 1;
	u8 queryIdx = m_counter.predicateIdx;
	u8 waitCount = 0;
	while (waitCount < QUERY_PREF_NUM)
	{
		HRESULT hr = pContext->GetData(m_counter.predicate[queryIdx], &result, sizeof(result), 0);
		if (hr == S_OK){ break; }
		if (hr != S_FALSE){ return 0; }
		queryIdx = GFX_COUNTER::CountUp(queryIdx);
		waitCount++;
	}
	return (result == 0);
}

///-------------------------------------------------------------------------------------------------
/// ピクセルカウント結果取得
u64		COcclusionQuery::GetResultPixelCount() const
{
	libAssert(m_type == GFX_PREDICATE_PIXEL_COUNTER);
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	ID3D11DeviceContext* pContext = pDevice->GetContext();
	libAssert(pContext);

	u64 result = 0;
	u8 queryIdx = m_counter.predicateIdx;
	u8 waitCount = 0;
	while (waitCount < QUERY_PREF_NUM)
	{
		HRESULT hr = pContext->GetData(m_counter.predicate[queryIdx], &result, sizeof(result), 0);
		if (hr == S_OK){ break; }
		if (hr != S_FALSE){ return 0; }
		queryIdx = GFX_COUNTER::CountUp(queryIdx);
		waitCount++;
	}
	return result;
}

POISON_END

#endif // LIB_GFX_DX11

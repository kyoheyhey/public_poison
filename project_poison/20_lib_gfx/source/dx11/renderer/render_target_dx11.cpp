﻿
#ifdef LIB_GFX_DX11

///-------------------------------------------------------------------------------------------------
// include
#include "renderer/render_target.h"

#include "object/color_buffer.h"
#include "object/depth_buffer.h"
#include "object/texture_buffer.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
// static


///*************************************************************************************************
///	レンダーターゲット
///*************************************************************************************************

CRenderTarget::CRenderTarget()
	: m_pColor(NULL)
	, m_pDepth(NULL)
	, m_pClrTex(NULL)
	, m_pDepTex(NULL)
	, m_colorNum(0)
	, m_depthNum(0)
	, m_memHdl(MEM_HDL_INVALID)
	, m_width(0)
	, m_height(0)
{
	memset(m_name, 0, sizeof(m_name));
}
CRenderTarget::~CRenderTarget()
{
}

///-------------------------------------------------------------------------------------------------
///	レンダーバッファ取得
CColorBuffer*	CRenderTarget::GetColorRender(u32 _idx)
{
	return _idx < m_colorNum ? m_pColor + _idx : NULL;
}

CDepthBuffer*	CRenderTarget::GetDepthRender(u32 _idx)
{
	return _idx < m_depthNum ? m_pDepth + _idx : NULL;
}

///-------------------------------------------------------------------------------------------------
///	テクスチャバッファ取得
CTextureBuffer*	CRenderTarget::GetColorTexture(u32 _idx)
{
	return _idx < m_colorNum ? m_pClrTex + _idx : NULL;
}

CTextureBuffer*	CRenderTarget::GetDepthTexture(u32 _idx)
{
	return _idx < m_depthNum ? m_pDepTex + _idx : NULL;
}

///-------------------------------------------------------------------------------------------------
///	生成
b8		CRenderTarget::Create(const RENDER_TARGET_DESC& _desc)
{
	libAssert(_desc.clrNum <= MAX_RT_NUM);
	libAssert(_desc.depNum <= MAX_RT_NUM);

	// レンダーバッファ生成
	CColorBuffer* pColor = NULL;
	CTextureBuffer* pClrTex = NULL;
	if (_desc.clrNum > 0)
	{
		pColor = MEM_CALLOC_TYPES(_desc.memHdl, CColorBuffer, _desc.clrNum);
		libAssert(pColor);
		pClrTex = MEM_CALLOC_TYPES(_desc.memHdl, CTextureBuffer, _desc.clrNum);
		libAssert(pClrTex);
	}
	CDepthBuffer* pDepth = NULL;
	CTextureBuffer* pDepTex = NULL;
	if (_desc.depNum > 0)
	{
		pDepth = MEM_CALLOC_TYPES(_desc.memHdl, CDepthBuffer, _desc.depNum);
		libAssert(pDepth);
		pDepTex = MEM_CALLOC_TYPES(_desc.memHdl, CTextureBuffer, _desc.depNum);
		libAssert(pDepTex);
	}
	m_memHdl = _desc.memHdl;

	b8 bRet = true;
	// カラーバッファ生成
	for (u32 idx = 0; idx < _desc.clrNum; idx++)
	{
		bRet &= pColor[idx].Create(_desc.width, _desc.height, _desc.pColor[idx].format, _desc.pColor[idx].aa, _desc.pColor[idx].usage, _desc.pColor[idx].attr);
		bRet &= pClrTex[idx].InitReferenceTarget(&pColor[idx]);
	}
	// デプスバッファ生成
	for (u32 idx = 0; idx < _desc.depNum; idx++)
	{
		bRet &= pDepth[idx].Create(_desc.width, _desc.height, _desc.pDepth[idx].format, _desc.pDepth[idx].aa, _desc.pDepth[idx].num, _desc.pDepth[idx].usage, _desc.pDepth[idx].attr);
		bRet &= pDepTex[idx].InitReferenceTarget(&pDepth[idx]);
	}
	if (bRet)
	{
		strcpy_s(m_name, _desc.name);
		m_pColor = pColor;
		m_pClrTex = pClrTex;
		m_colorNum = _desc.clrNum;
		m_pDepth = pDepth;
		m_pDepTex = pDepTex;
		m_depthNum = _desc.depNum;
		m_width = _desc.width;
		m_height = _desc.height;
	}

	return bRet;
}

///-------------------------------------------------------------------------------------------------
///	破棄
void	CRenderTarget::Release()
{
	for (u32 idx = 0; idx < m_colorNum; idx++)
	{
		m_pColor[idx].Release();
		m_pClrTex[idx].Release();
	}
	for (u32 idx = 0; idx < m_depthNum; idx++)
	{
		m_pDepth[idx].Release();
		m_pDepTex[idx].Release();
	}
	MEM_SAFE_FREE(m_memHdl, m_pColor);
	MEM_SAFE_FREE(m_memHdl, m_pDepth);
	MEM_SAFE_FREE(m_memHdl, m_pClrTex);
	MEM_SAFE_FREE(m_memHdl, m_pDepTex);
	*this = CRenderTarget();
}


///-------------------------------------------------------------------------------------------------
///	ターゲットのリサイズ
b8		CRenderTarget::ResizeTarget(u16 _width, u16 _height)
{
	// 変更なし
	if (GetWidth() == _width && GetHeight() == _height) { return true; }

	b8 bRet = true;
	// カラーバッファ作り直し
	for (u32 idx = 0; idx < m_colorNum; idx++)
	{
		GFX_IMAGE_FORMAT format = m_pColor[idx].GetFormat();
		GFX_ANTI_ALIASE aa = m_pColor[idx].GetAA();
		m_pColor[idx].Release();
		bRet &= m_pColor[idx].Create(_width, _height, format, aa);

		m_pClrTex[idx].Release();
		bRet &= m_pClrTex[idx].InitReferenceTarget(&m_pColor[idx]);
	}

	// デプスバッファ作り直し
	for (u32 idx = 0; idx < m_depthNum; idx++)
	{
		GFX_IMAGE_FORMAT format = m_pDepth[idx].GetFormat();
		GFX_ANTI_ALIASE aa = m_pDepth[idx].GetAA();
		u32 arrayNum = m_pDepth[idx].GetArrayNum();
		m_pDepth[idx].Release();
		bRet &= m_pDepth[idx].Create(_width, _height, format, aa, arrayNum);

		m_pDepTex[idx].Release();
		bRet &= m_pDepTex[idx].InitReferenceTarget(&m_pDepth[idx]);
	}

	if (bRet)
	{
		m_width = _width;
		m_height = _height;
	}

	return bRet;
}



POISON_END


#endif // LIB_GFX_DX11

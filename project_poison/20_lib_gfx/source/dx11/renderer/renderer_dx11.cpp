﻿
#ifdef LIB_GFX_DX11

///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "dx11/renderer/renderer_dx11.h"

#include <wchar.h>
#include <locale.h>

#include "device/device_utility.h"
#include "dx11/device/win/device_dx11.h"

#include "format/gfx_format_utility.h"
#include "dx11/format/gfx_format_utility_dx11.h"

#include "renderer/render_target.h"

#include "shader/shader.h"
#include "shader/shader_utility.h"

#include "object/color_buffer.h"
#include "object/depth_buffer.h"
#include "object/vertex_buffer.h"
#include "object/index_buffer.h"
#include "object/uniform_buffer.h"
#include "object/texture_buffer.h"

#include "object/sampler_state.h"
#include "object/rasterrizer_state.h"
#include "object/blend_state.h"
#include "object/depth_state.h"

// query
#include "query/occlusion_query.h"
#include "query/primitive_query.h"
#include "query/timer_query.h"


POISON_BGN


CRendererDX11::CRendererDX11()
	: m_pCommandList(NULL)
	, m_pUserAnotation(NULL)
{
}

CRendererDX11::~CRendererDX11()
{
}

///-------------------------------------------------------------------------------------------------
///	初期化
b8		CRendererDX11::Initialize(IAllocator* _pAllocator, const c8* _pName)
{
	// 基底クラスたちの初期化
	if (!CRenderer::Initialize(_pAllocator, _pName)){ return false; }

	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	m_pDrawContext = pDevice->CreateDeferredContext();
	libAssert(m_pDrawContext);

	// グラフィクス診断ツール生成
	m_pDrawContext->QueryInterface(__uuidof(ID3DUserDefinedAnnotation), (void**)&m_pUserAnotation);
	libAssert(m_pUserAnotation);

	return true;
}

///-------------------------------------------------------------------------------------------------
///	終了
void	CRendererDX11::Finalize()
{
	if (m_pDrawContext)
	{
		m_pDrawContext->ClearState();
		m_pDrawContext->Release();
		m_pDrawContext = NULL;
	}

	SAFE_RELEASE(m_pCommandList);
	SAFE_RELEASE(m_pUserAnotation);

	// 基底クラスたちの終了
	CRenderer::Finalize();
}


///-------------------------------------------------------------------------------------------------
///	描画終了
void	CRendererDX11::DrawEnd()
{
	// コマンドキュー生成
	CreateCommandQueue();

	// 基底クラス呼び出し
	CRenderer::DrawEnd();
}


///-------------------------------------------------------------------------------------------------
/// バッファクリア
void	CRendererDX11::ClearSurface(const CVec4& _clr, RENDER_BUFFER_BIT _bit/*=RENDER_BUFFER_ALL*/)
{
	libAssert(m_pDrawContext);

	const CRenderTarget* pCacheRT = m_pCacheRenderTarget[m_currentRenderTargetIdx];
	if (pCacheRT)
	{
		COLOR_BIT cacheBit = m_pCacheColorBufferIdxBit[m_currentRenderTargetIdx];
		u32 cacheDepIdx = m_pCacheDepthBufferIdx[m_currentRenderTargetIdx];

		// カラーバッファクリア
		u32 clrIdx = 0;
		u32 slotIdx = 0;
		while (cacheBit)
		{
			const CColorBuffer* pClrBuf = pCacheRT->GetColorRender(clrIdx);
			if ((cacheBit & 0x01) && pClrBuf)
			{
				if ((_bit >> slotIdx) & 0x01)
				{
					ID3D11RenderTargetView* pClrView = pClrBuf->GetObject().pView;
					m_pDrawContext->ClearRenderTargetView(pClrView, _clr.v);
				}
				slotIdx++;
			}
			cacheBit >>= 1;
			clrIdx++;
		}

		// デプスステンシルクリア
		if (_bit & RENDER_BUFFER_BIT_DEPTH_STENCIL)
		{
			const CDepthBuffer* pDepBuf = pCacheRT->GetDepthRender(cacheDepIdx);
			if (pDepBuf)
			{
				ID3D11DepthStencilView* pDepView = pDepBuf ? pDepBuf->GetObject().pView : NULL;

				u32 flag = 0;
				if (_bit & RENDER_BUFFER_BIT_DEPTH)
				{
					flag |= D3D11_CLEAR_DEPTH;
				}
				if (_bit & RENDER_BUFFER_BIT_STENCIL)
				{
					flag |= D3D11_CLEAR_STENCIL;
				}
				// クリア
				m_pDrawContext->ClearDepthStencilView(pDepView, flag, CLEAR_DEPTH, CLEAR_STENCIL);
			}
		}
	}
}

void	CRendererDX11::ClearSurface(const CVec4& _clr, const CRenderTarget* _pRenderTarget, COLOR_BIT _clrBit, u32 _depIdx)
{
	libAssert(m_pDrawContext);

	if (_pRenderTarget)
	{
		// カラーバッファクリア
		u32 clrIdx = 0;
		while (_clrBit)
		{
			const CColorBuffer* pClrBuf = _pRenderTarget->GetColorRender(clrIdx);
			if ((_clrBit & 0x01) && pClrBuf)
			{
				ID3D11RenderTargetView* pClrView = pClrBuf->GetObject().pView;
				m_pDrawContext->ClearRenderTargetView(pClrView, _clr.v);
			}
			_clrBit >>= 1;
			clrIdx++;
		}

		// デプスステンシルクリア
		const CDepthBuffer* pDepBuf = _pRenderTarget->GetDepthRender(_depIdx);
		if (pDepBuf)
		{
			ID3D11DepthStencilView* pDepView = pDepBuf->GetObject().pView;
			u32 flag = D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL;
			// クリア
			m_pDrawContext->ClearDepthStencilView(pDepView, flag, CLEAR_DEPTH, CLEAR_STENCIL);
		}
	}
}


///-------------------------------------------------------------------------------------------------
///	描画
void	CRendererDX11::DrawPrim(GFX_PRIMITIVE _primType, u32 _num, u32 _offset/* = 0*/)
{
	libAssert(m_pDrawContext);

	// 描画フラッシュ開始
	FlushDrawBegin();

	// プリミティブ(ポリゴンの形状)をコンテキストに設定
	D3D11_PRIMITIVE_TOPOLOGY dxPrim;
	CGfxFormatUtilityDX11::ConvertPrimitive(dxPrim, _primType);
	m_pDrawContext->IASetPrimitiveTopology(dxPrim);

	// 描画
	m_pDrawContext->Draw(_num, _offset);

	// 描画フラッシュ終了
	FlushDrawEnd();
}

///-------------------------------------------------------------------------------------------------
///	描画
void	CRendererDX11::DrawIndexPrim(GFX_PRIMITIVE _primType, u32 _num, u32 _vtxOffset/* = 0*/, u32 _idxOffset/* = 0*/)
{
	libAssert(m_pDrawContext);

	if (m_pCacheIndex == NULL){ return; }

	// 描画フラッシュ開始
	FlushDrawBegin();

	// プリミティブ(ポリゴンの形状)をコンテキストに設定
	D3D11_PRIMITIVE_TOPOLOGY dxPrim;
	CGfxFormatUtilityDX11::ConvertPrimitive(dxPrim, _primType);
	m_pDrawContext->IASetPrimitiveTopology(dxPrim);

	// 描画
	m_pDrawContext->DrawIndexed(_num, _idxOffset, _vtxOffset);

	// 描画フラッシュ終了
	FlushDrawEnd();
}


///-------------------------------------------------------------------------------------------------
/// クエリー開始
void	CRendererDX11::BeginQuery(const COcclusionQuery* _pQuery)
{
	libAssert(m_pDrawContext);
	libAssert(_pQuery && _pQuery->IsEnable());
	GFX_PREDICATE_COUNTER& rQuery = *CCast<GFX_PREDICATE_COUNTER*>(&_pQuery->GetCounter());
	rQuery.CountUp();
	m_pDrawContext->Begin(rQuery.GetPredicate());
}

void	CRendererDX11::BeginQuery(const CPrimitiveQuery* _pQuery)
{
	libAssert(m_pDrawContext);
	libAssert(_pQuery && _pQuery->IsEnable());
	GFX_COUNTER& rQuery = *CCast<GFX_COUNTER*>(&_pQuery->GetCounter());
	rQuery.CountUp();
	m_pDrawContext->Begin(rQuery.GetQuery());
}

void	CRendererDX11::BeginQuery(const CTimerQuery* _pQuery)
{
	libAssert(m_pDrawContext);
	libAssert(_pQuery && _pQuery->IsEnable());
	GFX_COUNTER& rQuery = *CCast<GFX_COUNTER*>(&_pQuery->GetCounter());
	rQuery.CountUp();
	m_pDrawContext->End(rQuery.GetQuery());
}

///-------------------------------------------------------------------------------------------------
/// クエリー終了
void	CRendererDX11::EndQuery(const COcclusionQuery* _pQuery)
{
	libAssert(m_pDrawContext);
	libAssert(_pQuery && _pQuery->IsEnable());
	GFX_PREDICATE_COUNTER& rQuery = *CCast<GFX_PREDICATE_COUNTER*>(&_pQuery->GetCounter());
	m_pDrawContext->End(rQuery.GetPredicate());
}

void	CRendererDX11::EndQuery(const CPrimitiveQuery* _pQuery)
{
	libAssert(m_pDrawContext);
	libAssert(_pQuery && _pQuery->IsEnable());
	GFX_COUNTER& rQuery = *CCast<GFX_COUNTER*>(&_pQuery->GetCounter());
	m_pDrawContext->End(rQuery.GetQuery());
}

void	CRendererDX11::EndQuery(const CTimerQuery* _pQuery)
{
	libAssert(m_pDrawContext);
	libAssert(_pQuery && _pQuery->IsEnable());
	GFX_COUNTER& rQuery = *CCast<GFX_COUNTER*>(&_pQuery->GetEndCounter());
	rQuery.CountUp();
	m_pDrawContext->End(rQuery.GetQuery());
}

///-------------------------------------------------------------------------------------------------
/// 事前描画遮蔽カリング設定
void	CRendererDX11::SetDrawingPredictionQuery(const COcclusionQuery* _pQuery)
{
	m_pDrawingPredication = _pQuery;
}


///-------------------------------------------------------------------------------------------------
/// マーカープッシュ設定
void	CRendererDX11::PushMarker(const c8* _pMessage)
{
	libAssert(m_pUserAnotation);

	// char->wchar_t変換
	setlocale(LC_CTYPE, "JPN");	// ロケールを変更
	u64 srcNum = strlen(_pMessage) + 1;
	u64 wSrcSize = sizeof(WCHAR) * srcNum;
	WCHAR* wSrc = PCast<WCHAR*>(alloca(wSrcSize));
	u64 convertedNum = 0;
	mbstowcs_s(&convertedNum, wSrc, srcNum, _pMessage, _TRUNCATE);

	m_pUserAnotation->BeginEvent(wSrc);
}

///-------------------------------------------------------------------------------------------------
/// マーカーポップ設定
void	CRendererDX11::PopMarker()
{
	libAssert(m_pUserAnotation);

	m_pUserAnotation->EndEvent();
}


///-------------------------------------------------------------------------------------------------
/// レンダーターゲットフラッシュ
void	CRendererDX11::FlushRenderTarget()
{
	libAssert(m_pDrawContext);

	u32 clrSetNum = 0;
	ID3D11RenderTargetView* pClrView[RENDER_BUFFER_SLOT_NUM] = {};
	ID3D11DepthStencilView* pDepView = NULL;

	RENDER_BUFFER_BIT& updateBit = m_updateRenderTargetBit;
	const CRenderTarget* pCacheRT = m_pCacheRenderTarget[m_currentRenderTargetIdx];
	if (pCacheRT)
	{
		COLOR_BIT cacheBit = m_pCacheColorBufferIdxBit[m_currentRenderTargetIdx];
		u32 cacheDepIdx = m_pCacheDepthBufferIdx[m_currentRenderTargetIdx];

		// カラーバッファ設定
		u32 clrIdx = 0;
		u32 slotIdx = 0;
		while (cacheBit)
		{
			if (cacheBit & 0x01)
			{
				const CColorBuffer* pClrBuf = pCacheRT->GetColorRender(clrIdx);
				if (pClrBuf)
				{
#if 0
					if ((updateBit >> slotIdx) & 0x01)
					{
						libAssert(pClrBuf);
						pClrView[slotIdx] = pClrBuf->GetObject().pView;
					}
					else
					{
						pClrView[slotIdx] = NULL;
					}
#else
					pClrView[slotIdx] = pClrBuf->GetObject().pView;
#endif // 0
					// テクスチャ被りチェック
					const CTextureBuffer* pTexBuf = pCacheRT->GetColorTexture(clrIdx);
					if (pTexBuf && ClearTextureRenderTarget(&pTexBuf->GetObject()))
					{
						PRINT("*****[WARRING] Conflict Textures RenderTarget. RenderTargetName[%s] slot[%d]\n", pCacheRT->GetName(), slotIdx);
					}
					slotIdx++;
				}
			}
			cacheBit >>= 1;
			clrIdx++;
		}
		clrSetNum = slotIdx;

		if (updateBit & RENDER_BUFFER_BIT_DEPTH_STENCIL)
		{
			const CDepthBuffer* pDepBuf = pCacheRT->GetDepthRender(cacheDepIdx);
			if (pDepBuf)
			{
				pDepView = pDepBuf->GetObject().pView;
				// テクスチャ被りチェック
				const CTextureBuffer* pTexBuf = pCacheRT->GetDepthTexture(cacheDepIdx);
				if (pTexBuf && ClearTextureRenderTarget(&pTexBuf->GetObject()))
				{
					PRINT("*****[WARRING] Conflict Textures RenderTarget. RenderTargetName[%s] depth\n", pCacheRT->GetName());
				}
			}
		}
	}
	
	if (updateBit)
	{
		// 描画ターゲットをコンテキストに設定
		m_pDrawContext->OMSetRenderTargets(clrSetNum, pClrView, pDepView);
	}
	updateBit = 0;
}

///-------------------------------------------------------------------------------------------------
/// シェーダフラッシュ
void	CRendererDX11::FlushShader(const PassShader* _pPassShader)
{
	const VShader* pVShader = NULL;
	const GShader* pGShader = NULL;
	const PShader* pPShader = NULL;
	if (_pPassShader)
	{
		pVShader = _pPassShader->GetVShader();
		pGShader = _pPassShader->GetGShader();
		pPShader = _pPassShader->GetPShader();
	}

	if (m_pCacheVShader != pVShader)
	{
		// 頂点シェーダー設定
		m_pDrawContext->VSSetShader(pVShader ? pVShader->GetObject().pShader : NULL, NULL, 0);
		m_pCacheVShader = pVShader;
	}
	if (m_pCacheGShader != pGShader)
	{
		// ジオメトリシェーダー設定
		m_pDrawContext->GSSetShader(pGShader ? pGShader->GetObject().pShader : NULL, NULL, 0);
		m_pCacheGShader = pGShader;
	}
	if (m_pCachePShader != pPShader)
	{
		// ピクセルシェーダー設定
		m_pDrawContext->PSSetShader(pPShader ? pPShader->GetObject().pShader : NULL, NULL, 0);
		m_pCachePShader = pPShader;
	}

	if (_pPassShader)
	{
		// インプットデータ設定
		m_pDrawContext->IASetInputLayout(_pPassShader ? _pPassShader->GetObject().pLayout : NULL);
		m_pCachePassShader = _pPassShader;
	}
}

///-------------------------------------------------------------------------------------------------
/// 頂点フラッシュ
void	CRendererDX11::FlushVertex()
{
	libAssert(m_pDrawContext);

	// 先にシェーダをフラッシュする
	if (m_pCachePassShader == NULL) { return; }

	ID3D11Buffer* pVtxBuf = NULL;
	u32 vtxStride = 0;
	const VTX_DECL* pDecl = m_pCachePassShader->GetVtxDecl();
	u32 declNum = m_pCachePassShader->GetDeclNum();
	for (u32 idx = 0; idx < declNum; idx++, pDecl++)
	{
		const CVertexBuffer* pVtxBuffer = NULL;
		u8 attr = pDecl->attr;
		libAssert(attr < VERTEX_ATTRIBUTE_TYPE_NUM);
		u32 offset = pDecl->offset;
		if (m_updateVertexBit & (1u << attr))
		{
			pVtxBuffer = m_pCacheVertex[attr];
			m_updateVertexBit ^= (1u << attr);	// 更新フラグおろし
		}

		// シェーダのDECLから取得
		if (pVtxBuffer)
		{
			// 頂点バッファがあればそちらで上書き
			pVtxBuf = pVtxBuffer->GetObject().pBuf;
			vtxStride = pVtxBuffer->GetVtxStride();
			offset = 0;
		}

		// 頂点設定
		if (pVtxBuf)
		{
			libAssert(attr < D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT);
			m_pDrawContext->IASetVertexBuffers(attr, 1, &pVtxBuf, &vtxStride, &offset);
		}
	}
}

///-------------------------------------------------------------------------------------------------
/// インデックスフラッシュ
void	CRendererDX11::FlushIndex()
{
	libAssert(m_pDrawContext);

	if (!(m_updateRanderStateBit & UPDATE_INDEX)) { return; }

	ID3D11Buffer* pIdxBuf = NULL;
	DXGI_FORMAT dxFormat = DXGI_FORMAT_R32_UINT;
	if (m_pCacheIndex)
	{
		pIdxBuf = m_pCacheIndex->GetObject().pBuf;
		GFX_VALUE valType = CGfxFormatUtility::GetValueFormatU(m_pCacheIndex->GetElemSize());
		GFX_IMAGE_FORMAT format = CGfxFormatUtility::ConvertImageFormat(valType, 1);
		CGfxFormatUtilityDX11::ConvertFormat(dxFormat, format);
	}

	// インデックスバッファ設定
	m_pDrawContext->IASetIndexBuffer(pIdxBuf, dxFormat, 0);

	m_updateRanderStateBit &= ~UPDATE_INDEX;	// 更新フラグおろし
}

///-------------------------------------------------------------------------------------------------
/// ユニフォームフラッシュ
void	CRendererDX11::FlushUniform()
{
	libAssert(m_pDrawContext);

	typedef void (STDMETHODCALLTYPE ID3D11DeviceContext::*FUNC)(UINT, UINT, ID3D11Buffer*const*);
	//typedef void (ID3D11DeviceContext::*FUNC)(UINT, UINT, ID3D11Buffer*const*);
	static const FUNC s_funcs[] =
	{
		&ID3D11DeviceContext::VSSetConstantBuffers,
		&ID3D11DeviceContext::GSSetConstantBuffers,
		//&ID3D11DeviceContext::DSSetConstantBuffers,
		//&ID3D11DeviceContext::HSSetConstantBuffers,
		&ID3D11DeviceContext::PSSetConstantBuffers,
	};
	StaticAssert(ARRAYOF(s_funcs) == GFX_SHADER_GRAPHICS_TYPE_NUM);

#ifndef POISON_RELEASE
	const ShaderSlotCheckSum* s_slotCheckSum[] =
	{
		(m_pCacheVShader ? &m_pCacheVShader->GetCheckSum() : NULL),
		(m_pCacheGShader ? &m_pCacheGShader->GetCheckSum() : NULL),
		(m_pCachePShader ? &m_pCachePShader->GetCheckSum() : NULL),
	};
	StaticAssert(ARRAYOF(s_slotCheckSum) == GFX_SHADER_GRAPHICS_TYPE_NUM);
	b8 bError = false;
#endif // !POISON_RELEASE

	// 先にシェーダをフラッシュする
	if (m_pCachePassShader == NULL) { return; }

	for (u32 shaderIdx = 0; shaderIdx < GFX_SHADER_GRAPHICS_TYPE_NUM; shaderIdx++)
	{
		// ユニフォーム設定関数
		FUNC func = s_funcs[shaderIdx];

		u32 slotIdx = 0;
		auto flag = m_updateUniformBit[shaderIdx];
		while (flag)
		{
			ID3D11Buffer* pUniBuf = NULL;
			// 更新があれば
			if (flag & 0x01)
			{
				const RENDERER_BUFFER_DATA& data = m_cacheUniform[shaderIdx][slotIdx];
				const RENDER_OBJECT* pObj = PCast<const RENDER_OBJECT*>(data.pBuffer);
				pUniBuf = pObj ? pObj->pBuf : NULL;
				// ユニフォーム設定
				(m_pDrawContext->*func)(slotIdx, 1, &pUniBuf);

#ifndef POISON_RELEASE
				// リソースがNULLの時、
				if (pUniBuf == NULL)
				{
					if (s_slotCheckSum[shaderIdx] && s_slotCheckSum[shaderIdx]->uniformBlockMask & (1 << slotIdx))
					{
						if (bError == false)
						{
							const c8* shaderName = m_pCachePassShader->GetName();
							PRINT("*****[ERROR] No Setting ConstantBuffer. Shader[%s]\n", shaderName);
							bError = true;
						}
						const c8* shaderStageName = CGfxFormatUtility::GetShaderStageName(SCast<GFX_SHADER>(shaderIdx));
						PRINT("     ShaderStage[%s] ConstantBuffer[%2d]\n", shaderStageName, slotIdx);
					}
				}
#endif // !POISON_RELEASE
			}
			++slotIdx;
			// シフトしていく
			flag >>= 1;
		}
		m_updateUniformBit[shaderIdx] = 0;	// 更新フラグおろし

#ifndef POISON_RELEASE
		// 更新なしリソースのチェック
		for (u32 remainderIdx = slotIdx; remainderIdx < UNIFORM_SLOT_NUM; remainderIdx++)
		{
			const RENDERER_BUFFER_DATA& data = m_cacheUniform[shaderIdx][remainderIdx];
			if (!data.pBuffer && s_slotCheckSum[shaderIdx] && s_slotCheckSum[shaderIdx]->uniformBlockMask & (1 << remainderIdx))
			{
				if (bError == false)
				{
					const c8* shaderName = m_pCachePassShader->GetName();
					PRINT("*****[ERROR] No Setting ConstantBuffer. Shader[%s]\n", shaderName);
					bError = true;
				}
				const c8* shaderStageName = CGfxFormatUtility::GetShaderStageName(SCast<GFX_SHADER>(shaderIdx));
				PRINT("     ShaderStage[%s] ConstantBuffer[%2d]\n", shaderStageName, remainderIdx);
			}
		}
#endif // !POISON_RELEASE
	}

#ifndef POISON_RELEASE
	libAssert(!bError);
#endif // !POISON_RELEASE
}

///-------------------------------------------------------------------------------------------------
/// テクスチャフラッシュ
void	CRendererDX11::FlushTexture()
{
	libAssert(m_pDrawContext);

	typedef void (STDMETHODCALLTYPE ID3D11DeviceContext::*FUNC)(UINT, UINT, ID3D11ShaderResourceView*const*);
	//typedef void (ID3D11DeviceContext::*FUNC)(UINT, UINT, ID3D11ShaderResourceView *const*);
	static const FUNC s_funcs[] =
	{
		&ID3D11DeviceContext::VSSetShaderResources,
		&ID3D11DeviceContext::GSSetShaderResources,
		//&ID3D11DeviceContext::DSSetShaderResources,
		//&ID3D11DeviceContext::HSSetShaderResources,
		&ID3D11DeviceContext::PSSetShaderResources,
	};
	StaticAssert(ARRAYOF(s_funcs) == GFX_SHADER_GRAPHICS_TYPE_NUM);

#ifndef POISON_RELEASE
	const ShaderSlotCheckSum* s_slotCheckSum[] =
	{
		(m_pCacheVShader ? &m_pCacheVShader->GetCheckSum() : NULL),
		(m_pCacheGShader ? &m_pCacheGShader->GetCheckSum() : NULL),
		(m_pCachePShader ? &m_pCachePShader->GetCheckSum() : NULL),
	};
	StaticAssert(ARRAYOF(s_slotCheckSum) == GFX_SHADER_GRAPHICS_TYPE_NUM);
	b8 bError = false;
#endif // !POISON_RELEASE

	// 先にシェーダをフラッシュする
	if (m_pCachePassShader == NULL) { return; }

	for (u32 shaderIdx = 0; shaderIdx < GFX_SHADER_GRAPHICS_TYPE_NUM; shaderIdx++)
	{
		// ユニフォーム設定関数
		FUNC func = s_funcs[shaderIdx];

		u32 slotIdx = 0;
		auto flag = m_updateTextureBit[shaderIdx];
		while (flag)
		{
			ID3D11ShaderResourceView* pView = NULL;
			// 更新があれば
			if (flag & 0x01)
			{
				const RENDERER_BUFFER_DATA& data = m_cacheTexAndRoBuf[shaderIdx][slotIdx];
				const TEXTURE_OBJECT* pObj = PCast<const TEXTURE_OBJECT*>(data.pBuffer);
				pView = pObj ? pObj->pSRV : NULL;
				// テクスチャ設定
				(m_pDrawContext->*func)(slotIdx, 1, &pView);

#ifndef POISON_RELEASE
				// リソースがNULLの時、
				if (pView == NULL)
				{
					if (s_slotCheckSum[shaderIdx] && s_slotCheckSum[shaderIdx]->texAndRoBufMask & (1 << slotIdx))
					{
						if (bError == false)
						{
							const c8* shaderName = m_pCachePassShader->GetName();
							PRINT("*****[ERROR] No Setting Textures. Shader[%s]\n", shaderName);
							bError = true;
						}
						const c8* shaderStageName = CGfxFormatUtility::GetShaderStageName(SCast<GFX_SHADER>(shaderIdx));
						PRINT("     ShaderStage[%s] Textures[%2d]\n", shaderStageName, slotIdx);
					}
				}
#endif // !POISON_RELEASE
			}
			++slotIdx;
			// シフトしていく
			flag >>= 1;
		}
		m_updateTextureBit[shaderIdx] = 0;	// 更新フラグおろし

#ifndef POISON_RELEASE
		// 更新なしリソースのチェック
		for (u32 remainderIdx = slotIdx; remainderIdx < UNIFORM_SLOT_NUM; remainderIdx++)
		{
			const RENDERER_BUFFER_DATA& data = m_cacheTexAndRoBuf[shaderIdx][remainderIdx];
			if (!data.pBuffer && s_slotCheckSum[shaderIdx] && s_slotCheckSum[shaderIdx]->texAndRoBufMask & (1 << remainderIdx))
			{
				if (bError == false)
				{
					const c8* shaderName = m_pCachePassShader->GetName();
					PRINT("*****[ERROR] No Setting Textures. Shader[%s]\n", shaderName);
					bError = true;
				}
				const c8* shaderStageName = CGfxFormatUtility::GetShaderStageName(SCast<GFX_SHADER>(shaderIdx));
				PRINT("     ShaderStage[%s] Textures[%2d]\n", shaderStageName, remainderIdx);
			}
		}
#endif // !POISON_RELEASE
	}

#ifndef POISON_RELEASE
	libAssert(!bError);
#endif // !POISON_RELEASE
}

///-------------------------------------------------------------------------------------------------
/// サンプラーステートフラッシュ
void	CRendererDX11::FlushSamplerState()
{
	libAssert(m_pDrawContext);

	typedef void (STDMETHODCALLTYPE ID3D11DeviceContext::*FUNC)(UINT, UINT, ID3D11SamplerState*const*);
	//typedef void (ID3D11DeviceContext::*FUNC)(UINT, UINT, ID3D11SamplerState*const*);
	static const FUNC s_funcs[] =
	{
		&ID3D11DeviceContext::VSSetSamplers,
		&ID3D11DeviceContext::GSSetSamplers,
		//&ID3D11DeviceContext::DSSetSamplers,
		//&ID3D11DeviceContext::HSSetSamplers,
		&ID3D11DeviceContext::PSSetSamplers,
	};
	StaticAssert(ARRAYOF(s_funcs) == GFX_SHADER_GRAPHICS_TYPE_NUM);

	for (u32 shaderIdx = 0; shaderIdx < GFX_SHADER_GRAPHICS_TYPE_NUM; shaderIdx++)
	{
		// ユニフォーム設定関数
		FUNC func = s_funcs[shaderIdx];

		u32 slotIdx = 0;
		auto flag = m_updateSamplerBit[shaderIdx];
		while (flag)
		{
			// 更新があれば
			if (flag & 0x01)
			{
				const CSampler* pSampler = m_pCacheSampler[m_currentSamplerIdx][shaderIdx][slotIdx];
				ID3D11SamplerState* pState = pSampler ? pSampler->GetObject().pState : NULL;
				// テクスチャステート設定
				(m_pDrawContext->*func)(slotIdx, 1, &pState);
			}
			++slotIdx;
			// シフトしていく
			flag >>= 1;
		}
		m_updateSamplerBit[shaderIdx] = 0;	// 更新フラグおろし
	}
}

///-------------------------------------------------------------------------------------------------
/// ビューポートフラッシュ
void	CRendererDX11::FlushViewport()
{
	libAssert(m_pDrawContext);

	if (!(m_updateRanderStateBit & UPDATE_VIEWPORT)) { return; }

	D3D11_VIEWPORT vp;
#if defined( OPENGL_VIEWPORT )
	const CRenderTarget* pCacheRT = m_pCacheRenderTarget[m_currentRenderTargetIdx];
	libAssert(pCacheRT);
	vp.TopLeftX = SCast<f32>(m_cacheViewport.posX);
	vp.TopLeftY = SCast<f32>(pCacheRT->GetHeight() - m_cacheViewport.height - m_cacheViewport.posY);
	vp.Width = SCast<f32>(m_cacheViewport.width);
	vp.Height = SCast<f32>(m_cacheViewport.height);
#elif defined( DIRECTX_VIEWPORT )
	vp.TopLeftX = SCast<f32>(m_cacheViewport.posX);
	vp.TopLeftY = SCast<f32>(m_cacheViewport.posY);
	vp.Width = SCast<f32>(m_cacheViewport.width);
	vp.Height = SCast<f32>(m_cacheViewport.height);
#endif
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	// ジオメトリシェーダーが SV_ViewportArrayIndex セマンティクスを利用していないので矩形は一つしか現在使えない
	m_pDrawContext->RSSetViewports(1, &vp);

	m_updateRanderStateBit &= ~UPDATE_VIEWPORT;	// 更新フラグおろし
}

///-------------------------------------------------------------------------------------------------
/// シザリングの領域設定
void	CRendererDX11::FlushScissorRect()
{
	libAssert(m_pDrawContext);

	if (!(m_updateRanderStateBit & UPDATE_SCISSOR)) { return; }

	// 切り取る矩形を設定
	D3D11_RECT rect;
#if defined( OPENGL_VIEWPORT )
	const CRenderTarget* pCacheRT = m_pCacheRenderTarget[m_currentRenderTargetIdx];
	libAssert(pCacheRT);
	rect.left = m_cacheScissor.posX;
	rect.top = pCacheRT->GetHeight() - m_cacheScissor.height - m_cacheScissor.posY;
	rect.right = m_cacheScissor.posX + m_cacheScissor.width;
	rect.bottom = pCacheRT->GetHeight() - m_cacheScissor.posY;
#elif defined( DIRECTX_VIEWPORT )
	rect.left = m_cacheScissor.posX;
	rect.top = m_cacheScissor.posY;
	rect.right = m_cacheScissor.posX + m_cacheScissor.width;
	rect.bottom = m_cacheScissor.posY + m_cacheScissor.height;
#endif
	// ジオメトリシェーダーが SV_ViewportArrayIndex セマンティクスを利用していないので矩形は一つしか現在使えない
	m_pDrawContext->RSSetScissorRects(1, &rect);

	m_updateRanderStateBit &= ~UPDATE_SCISSOR;	// 更新フラグおろし
}

///-------------------------------------------------------------------------------------------------
/// ラストライザステートフラッシュ
void	CRendererDX11::FlushRasterrizerState()
{
	libAssert(m_pDrawContext);

	if (!(m_updateRanderStateBit & UPDATE_RASTERRIZER)) { return; }

	ID3D11RasterizerState* pState = NULL;
	if (m_pCacheRasterizer[m_currentRasterizerIdx])
	{
		pState = m_pCacheRasterizer[m_currentRasterizerIdx]->GetObject().pState;
	}
	// ラスタライザー設定
	m_pDrawContext->RSSetState(pState);

	m_updateRanderStateBit &= ~UPDATE_RASTERRIZER;	// 更新フラグおろし
}

///-------------------------------------------------------------------------------------------------
/// ブレンドステートフラッシュ
void	CRendererDX11::FlushBrendState()
{
	libAssert(m_pDrawContext);

	if (!(m_updateRanderStateBit & UPDATE_BLEND_STATE)) { return; }

	ID3D11BlendState* pState = NULL;
	if (m_pCacheBlendState[m_currentBlendStateIdx])
	{
		pState = m_pCacheBlendState[m_currentBlendStateIdx]->GetObject().pState;
	}
	// ブレンドステート設定
	// blendFactor : D3D11_BLEND_BLEND_FACTOR または D3D11_BLEND_INV_BLEND_FACTOR を設定しない限り無効な値。RGBA値を設定する。
	f32 blendFactor[] = { 1.0f, 1.0f, 1.0f, 1.0f };	// !!!! ブレンド係数を外部より指定させる必要は？
	u32 sampleMask = 0xffffffff;					// !!!! サンプル マスクの指定は必要ない？
	m_pDrawContext->OMSetBlendState(pState, blendFactor, sampleMask);

	m_updateRanderStateBit &= ~UPDATE_BLEND_STATE;	// 更新フラグおろし
}

///-------------------------------------------------------------------------------------------------
/// デプスステンシルステートフラッシュ
void	CRendererDX11::FlushDepthStencilState()
{
	libAssert(m_pDrawContext);

	if (!(m_updateRanderStateBit & UPDATE_DEPTH_STATE)) { return; }

	ID3D11DepthStencilState* pState = NULL;
	if (m_pCacheDepthState[m_currentDepthStateIdx])
	{
		pState = m_pCacheDepthState[m_currentDepthStateIdx]->GetObject().pState;
	}
	// デプスステンシルステート設定
	u32 stencilRef = 0x0;					// !!!! サンプル ステンシル参照値を設定させるようにしなきゃなぁ
	m_pDrawContext->OMSetDepthStencilState(pState, stencilRef);

	m_updateRanderStateBit &= ~UPDATE_DEPTH_STATE;	// 更新フラグおろし
}

///-------------------------------------------------------------------------------------------------
///	事前描画プリディケーションフラッシュ開始
void	CRendererDX11::FlushDrawingPredictionQueryBegin()
{
	libAssert(m_pDrawContext);
	if (m_pDrawingPredication)
	{
		libAssert(m_pDrawingPredication->IsEnable() && m_pDrawingPredication->GetType() == GFX_PREDICATE_OCCL_CHEKER);
		GFX_PREDICATE_COUNTER& rQuery = *CCast<GFX_PREDICATE_COUNTER*>(&m_pDrawingPredication->GetCounter());
		m_pDrawContext->SetPredication(rQuery.GetPredicate(), FALSE);
	}
	else
	{
		m_pDrawContext->SetPredication(NULL, FALSE);
	}
}

///-------------------------------------------------------------------------------------------------
///	事前描画プリディケーションフラッシュ終了
void	CRendererDX11::FlushDrawingPredictionQueryEnd()
{
	m_pDrawingPredication = NULL;
}

///-------------------------------------------------------------------------------------------------
///	コマンドキューフラッシュ
void	CRendererDX11::FlushCommandQueue()
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	ID3D11DeviceContext* pContext = pDevice->GetContext();

	if (m_pCommandList)
	{
		// キューフラッシュ
		pContext->ExecuteCommandList(m_pCommandList, false);
	}

	// フラッシュした後は用済みなのでリリース
	SAFE_RELEASE(m_pCommandList);
}


///-------------------------------------------------------------------------------------------------
/// コマンドキュー生成
b8		CRendererDX11::CreateCommandQueue()
{
	libAssert(m_pDrawContext);
	libAssert(m_pCommandList == NULL);

	// キュー生成
	ID3D11CommandList* pCmdList = NULL;
	HRESULT hr = m_pDrawContext->FinishCommandList(false, &pCmdList);
	if (FAILED(hr))
	{
#ifndef POISON_RELEASE
		CDeviceDX11::PrintError(hr, "FinishCommandList()");
		libAssert(0);
#endif//not POISON_RELEASE
		SAFE_RELEASE(pCmdList);
		return false;
	}
	
	m_pCommandList = pCmdList;
	return true;
}


POISON_END


#endif // LIB_GFX_DX11

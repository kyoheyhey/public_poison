﻿
#ifdef LIB_GFX_DX11

///-------------------------------------------------------------------------------------------------
/// define
#define ENTRY_POINT_NAME	"main"
#define VS_MODEL_NAME		"vs_5_0"
#define GS_MODEL_NAME		"gs_5_0"
#define PS_MODEL_NAME		"ps_5_0"

///-------------------------------------------------------------------------------------------------
/// include
#include "shader/shader.h"

#include <wchar.h>
#include <locale.h>

#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "dxguid.lib")
#include <d3dcompiler.h>

#include "shader/shader_utility.h"

#include "device/device_utility.h"
#include "dx11/device/win/device_dx11.h"

#include "format/gfx_format_utility.h"
#include "dx11/format/gfx_format_utility_dx11.h"

POISON_BGN


///-------------------------------------------------------------------------------------------------
/// char->wchar_t変換
void	ConvertCharToWChar(WCHAR* _wSrc, const c8* _pSrc, u32 _size)
{
	setlocale(LC_CTYPE, "JPN");	// ロケールを変更
	u64 srcNum = strlen(_pSrc) + 1;
	u64 wSrcSize = sizeof(WCHAR)* srcNum;
	_wSrc = PCast<WCHAR*>(alloca(wSrcSize));
	u64 convertedNum = 0;
	mbstowcs_s(&convertedNum, _wSrc, srcNum, _pSrc, _TRUNCATE);
}

///-------------------------------------------------------------------------------------------------
/// シェーダをコンパイル
b8	CompileShader(ID3DBlob** _ppRetBlobOut, const c8* _pSrc, u32 _size, const c8* _szEntryPoint, const c8* _szShaderModel)
{
	// コンパイルフラグ
	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#ifndef POISON_RELEASE
	dwShaderFlags |= D3DCOMPILE_DEBUG;
#else
	dwShaderFlags |= D3DCOMPILE_OPTIMIZATION_LEVEL3;
#endif//!POISON_RELEASE

	ID3DBlob* pErrorBlob = NULL;
	// ファイルからシェーダをコンパイル
	b8 isError = FAILED(::D3DCompile(
		_pSrc,
		_size,
		NULL, NULL, NULL,
		_szEntryPoint,
		_szShaderModel,
		dwShaderFlags,
		0,
		_ppRetBlobOut,
		&pErrorBlob
		));

	// エラーチェック
	if (isError)
	{
		// エラーメッセージを出力
		if (pErrorBlob)
		{
			PRINT("*****[ERROR][DX11] ::D3DCompile[%s]\n", (char*)pErrorBlob->GetBufferPointer());
		}
	}

	// 破棄
	SAFE_RELEASE(pErrorBlob);

	return !isError;
}

#ifndef POISON_RELEASE
///-------------------------------------------------------------------------------------------------
/// シェーダリフレクション
/// dxguid.libのリンクと
/// d3dcompiler.libのリンクが必要
b8	ShaderReflection(ShaderSlotCheckSum& _checkSum, const void* _pBuf, const u32 _size)
{
	ID3D11ShaderReflection* pReflection = NULL;
	HRESULT hr = D3DReflect(_pBuf, _size, IID_ID3D11ShaderReflection, (void**)&pReflection);
	if (FAILED(hr)) { return false; }

	// シェーダ情報取得
	D3D11_SHADER_DESC shd_desc;
	hr = pReflection->GetDesc(&shd_desc);
	if (FAILED(hr))
	{
		pReflection->Release();
		return false;
	}

	for (u32 i = 0; i<shd_desc.BoundResources; i++)
	{
		D3D11_SHADER_INPUT_BIND_DESC bind_desc;
		hr = pReflection->GetResourceBindingDesc(i, &bind_desc);
		if (FAILED(hr)) { continue; }
		u32	bitNum = 0;
		for (u8 bc = 0; bc < bind_desc.BindCount; bc++)
		{
			bitNum |= 1 << bc;
		}
		// desc.BindPointが割り当てられたスロット番号
		switch (bind_desc.Type)
		{
		case D3D_SIT_CBUFFER:		// 定数バッファ
			libAssert(bind_desc.BindPoint < UNIFORM_SLOT_NUM);	// 最大数
			_checkSum.uniformBlockMask |= bitNum << bind_desc.BindPoint;
			break;
		case D3D_SIT_TBUFFER:		// テクスチャバッファ(使ってる？)
		case D3D_SIT_TEXTURE:		// テクスチャ
		case D3D_SIT_STRUCTURED:	// 構造体バッファ
		case D3D_SIT_BYTEADDRESS:	// バイトアドレスバッファ
			libAssert(bind_desc.BindPoint < TEXTURE_SLOT_NUM);	// 最大数
			_checkSum.texAndRoBufMask |= bitNum << bind_desc.BindPoint;
			break;
		case D3D_SIT_SAMPLER:		// サンプラ
			libAssert(bind_desc.BindPoint < SAMPLER_SLOT_NUM);	// 最大数
			_checkSum.samplerMask |= bitNum << bind_desc.BindPoint;
			break;
		case D3D_SIT_UAV_RWTYPED:		// 読み書き可能バッファ
		case D3D_SIT_UAV_RWSTRUCTURED:	// 読み書き可能構造体バッファ
		case D3D_SIT_UAV_RWBYTEADDRESS:	// 読み書き可能バイトアドレスバッファ
		case D3D_SIT_UAV_APPEND_STRUCTURED:			// UAV追加構造体バッファ
		case D3D_SIT_UAV_CONSUME_STRUCTURED:		// UAV消費構造体バッファ
		case D3D_SIT_UAV_RWSTRUCTURED_WITH_COUNTER:	// UAVカウンタ追加・消費可能の読み書き可能バッファ
			libAssert(bind_desc.BindPoint < UAV_SLOT_NUM);	// 最大数
			_checkSum.uavBufMask |= bitNum << bind_desc.BindPoint;
		default:
			break;
		}
	}
	pReflection->Release();
	return true;
}
///-------------------------------------------------------------------------------------------------
/// シェーダリフレクション
/// dxguid.libのリンクと
/// d3dcompiler.libのリンクが必要
b8	CheckInputLayout(const void* _pBuf, const u32 _size)
{
	ID3D11ShaderReflection* pReflection = NULL;
	HRESULT hr = D3DReflect(_pBuf, _size, IID_ID3D11ShaderReflection, (void**)&pReflection);
	if (FAILED(hr)) { return false; }
	// シェーダ情報取得
	D3D11_SHADER_DESC shd_desc;
	hr = pReflection->GetDesc(&shd_desc);
	if (FAILED(hr))
	{
		pReflection->Release();
		return false;
	}

	PRINT("  [CheckShaderSemantic]\n");
	for (u32 i = 0; i<shd_desc.InputParameters; i++)
	{
		D3D11_SIGNATURE_PARAMETER_DESC decl_desc;
		hr = pReflection->GetInputParameterDesc(i, &decl_desc);
		if (FAILED(hr))
		{
			pReflection->Release();
			return false;
		}

		PRINT("  ++Need VtxDecl[%s%d]\n", decl_desc.SemanticName, decl_desc.SemanticIndex);
	}
	pReflection->Release();
	return true;
}
#endif//!POISON_RELEASE



///*************************************************************************************************
///	頂点シェーダクラス
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// 名前設定
void	VShader::SetName(const c8* _name)
{
	strcpy_s(m_name, _name);
}

///-------------------------------------------------------------------------------------------------
/// 有効かどうか
b8		VShader::IsEnable() const
{
	return (m_object.pShader != NULL);
}

///-------------------------------------------------------------------------------------------------
// 生成
b8		VShader::Create(const void* _pBuf, u32 _size, const c8* _pName/* = NULL*/)
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);

	//頂点シェーダー生成
	HRESULT hr = pDevice->GetDevice()->CreateVertexShader(_pBuf, _size, NULL, &m_object.pShader);
	if (FAILED(hr))
	{
#ifndef POISON_RELEASE
		CDeviceDX11::PrintError(hr, "CreateVertexShader()");
#endif	// !POISON_RELEASE
		return false;
	}

	// ソースバッファコピー
	m_pSrcBuf = MEM_CALLOC_TYPES(CShaderUtility::GetMemHdl(), c8, _size);
	libAssert(m_pSrcBuf);
	memcpy(m_pSrcBuf, _pBuf, _size);
	m_srcBufSize = _size;

	// 名前保持
	if (_pName)
	{
		SetName(_pName);
	}

#ifndef POISON_RELEASE

	// シェーダリソースチェックサム設定
	if (!ShaderReflection(m_checkSum, _pBuf, _size))
	{
		PRINT_WARNING("*****[WARNING] ShaderReflection()");
	}
#endif//!POISON_RELEASE

	return true;
}

///-------------------------------------------------------------------------------------------------
///	コンパイルして生成
b8		VShader::Compile(const c8* _pSrc, u32 _size, const c8* _pName/* = NULL*/)
{
	ID3DBlob* pShaderBlob = NULL;
	if (!CompileShader(&pShaderBlob, _pSrc, _size, ENTRY_POINT_NAME, VS_MODEL_NAME))
	{
		return false;
	}
	b8 bRet = Create(pShaderBlob->GetBufferPointer(), SCast<u32>(pShaderBlob->GetBufferSize()), _pName);

	// 破棄
	SAFE_RELEASE(pShaderBlob);
	return bRet;
}

///-------------------------------------------------------------------------------------------------
// 破棄
void	VShader::Release()
{
	// 破棄
	SAFE_RELEASE(m_object.pShader);
	MEM_SAFE_FREE(CShaderUtility::GetMemHdl(), m_pSrcBuf);
	new(this)VShader;
}


///*************************************************************************************************
///	ジオメトリシェーダクラス
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// 名前設定
void	GShader::SetName(const c8* _name)
{
	strcpy_s(m_name, _name);
}

///-------------------------------------------------------------------------------------------------
/// 有効かどうか
b8		GShader::IsEnable() const
{
	return (m_object.pShader != NULL);
}

////-------------------------------------------------------------------------------------------------
// 生成

b8		GShader::Create(const void* _pBuf, u32 _size, const c8* _pName/* = NULL*/)
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);

	//頂点シェーダー生成
	HRESULT hr = pDevice->GetDevice()->CreateGeometryShader(_pBuf, _size, NULL, &m_object.pShader);
	if (FAILED(hr))
	{
#ifndef POISON_RELEASE
		CDeviceDX11::PrintError(hr, "CreateGeometryShader()");
#endif	// !POISON_RELEASE
		return false;
	}

	// 名前保持
	if (_pName)
	{
		SetName(_pName);
	}

#ifndef POISON_RELEASE
	// シェーダリソースチェックサム設定
	if (!ShaderReflection(m_checkSum, _pBuf, _size))
	{
		PRINT_WARNING("*****[WARNING] ShaderReflection()");
	}
#endif//!POISON_RELEASE

	return true;
}

///-------------------------------------------------------------------------------------------------
///	コンパイルして生成
b8		GShader::Compile(const c8* _pSrc, u32 _size, const c8* _pName/* = NULL*/)
{
	ID3DBlob* pShaderBlob = NULL;
	if (!CompileShader(&pShaderBlob, _pSrc, _size, ENTRY_POINT_NAME, GS_MODEL_NAME))
	{
		return false;
	}
	b8 bRet = Create(pShaderBlob->GetBufferPointer(), SCast<u32>(pShaderBlob->GetBufferSize()), _pName);

	// 破棄
	SAFE_RELEASE(pShaderBlob);
	return bRet;
}

///-------------------------------------------------------------------------------------------------
// 破棄
void	GShader::Release()
{
	// 破棄
	SAFE_RELEASE(m_object.pShader);
	new(this)GShader;
}


///*************************************************************************************************
///	ピクセルシェーダクラス
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// 名前設定
void	PShader::SetName(const c8* _name)
{
	strcpy_s(m_name, _name);
}

///-------------------------------------------------------------------------------------------------
/// 有効かどうか
b8		PShader::IsEnable() const
{
	return (m_object.pShader != NULL);
}

////-------------------------------------------------------------------------------------------------
// 生成
b8		PShader::Create(const void* _pBuf, u32 _size, const c8* _pName/* = NULL*/)
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);

	//頂点シェーダー生成
	HRESULT hr = pDevice->GetDevice()->CreatePixelShader(_pBuf, _size, NULL, &m_object.pShader);
	if (FAILED(hr))
	{
#ifndef POISON_RELEASE
		CDeviceDX11::PrintError(hr, "CreatePixelShader()");
#endif	// !POISON_RELEASE
		return false;
	}

	// 名前保持
	if (_pName)
	{
		SetName(_pName);
	}

#ifndef POISON_RELEASE
	// シェーダリソースチェックサム設定
	if (!ShaderReflection(m_checkSum, _pBuf, _size))
	{
		PRINT_WARNING("*****[WARNING] ShaderReflection()");
	}
#endif//!POISON_RELEASE

	return true;
}

///-------------------------------------------------------------------------------------------------
///	コンパイルして生成
b8		PShader::Compile(const c8* _pSrc, u32 _size, const c8* _pName/* = NULL*/)
{
	ID3DBlob* pShaderBlob = NULL;
	if (!CompileShader(&pShaderBlob, _pSrc, _size, ENTRY_POINT_NAME, PS_MODEL_NAME))
	{
		return false;
	}
	b8 bRet = Create(pShaderBlob->GetBufferPointer(), SCast<u32>(pShaderBlob->GetBufferSize()), _pName);

	// 破棄
	SAFE_RELEASE(pShaderBlob);
	return bRet;
}

////-------------------------------------------------------------------------------------------------
// 破棄
void	PShader::Release()
{
	// 破棄
	SAFE_RELEASE(m_object.pShader);
	new(this)PShader;
}


///*************************************************************************************************
///	パスシェーダクラス
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// 名前設定
void	PassShader::SetName(const c8* _name)
{
	strcpy_s(m_name, _name);
}

///-------------------------------------------------------------------------------------------------
/// 有効かどうか
b8		PassShader::IsEnable() const
{
	return (m_object.pLayout != NULL);
}

////-------------------------------------------------------------------------------------------------
// 生成
b8		PassShader::Create(const VTX_DECL* _pDecl, u32 _declNum, const INIT_DESC& _desc, const c8* _pName/* = NULL*/)
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);

	// 頂点フォーマットを解析
	libAssert(VERTEX_ATTRIBUTE_TYPE_NUM > _declNum);
	D3D11_INPUT_ELEMENT_DESC desc[VERTEX_ATTRIBUTE_TYPE_NUM];
	CGfxFormatUtilityDX11::ConvertVertexDecl(desc, _pDecl, _declNum);

	// 頂点シェーダは必須
	libAssert(_desc.pVshader);

	// 頂点レイアウト作成
	HRESULT hr = pDevice->GetDevice()->CreateInputLayout(
		desc, _declNum,
		_desc.pVshader->GetSrcBuf(), _desc.pVshader->GetSrcBufSize(),
		&m_object.pLayout
	);
	if (FAILED(hr))
	{
#ifndef POISON_RELEASE
		CDeviceDX11::PrintError(hr, "CreateInputLayout()");
		for (u32 i = 0; i < m_declNum; i++)
		{
			PRINT("  -VertexDecl[%2d] Type[%2d][%s][%d]\n", i, desc[i].InputSlot, desc[i].SemanticName, desc[i].SemanticIndex);
		}
		CheckInputLayout(_desc.pVshader->GetSrcBuf(), _desc.pVshader->GetSrcBufSize());
		libAssert("シェーダーに対して入力頂点の要素不足です" && false)
#endif	// !POISON_RELEASE
			return false;
	}

	// 頂点情報セットアップ
	VtxDeclSetUp(_pDecl, _declNum);
	m_vshader = _desc.pVshader;
	m_gshader = _desc.pGshader;
	m_pshader = _desc.pPshader;

	// 名前保持
	if (_pName)
	{
		SetName(_pName);
	}

	return true;
}

////-------------------------------------------------------------------------------------------------
///	破棄
void	PassShader::Release()
{
	// 破棄
	SAFE_RELEASE(m_object.pLayout);
	new(this)PassShader;
}


POISON_END


#endif // LIB_GFX_DX11

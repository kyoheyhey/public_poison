﻿
#ifdef LIB_GFX_DX11

///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "format/gfx_format_utility.h"
#include "dx11/format/gfx_format_utility_dx11.h"
#include "shader/shader.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant



//-------------------------------------------------------------------------------------------------
// データアクセス変換
void	CGfxFormatUtilityDX11::ConvertUsage(D3D11_USAGE& _retUsage, GFX_USAGE _usage)
{
	static const D3D11_USAGE s_usage[] =
	{
		D3D11_USAGE_DEFAULT,
		D3D11_USAGE_IMMUTABLE,
		D3D11_USAGE_DYNAMIC,
		D3D11_USAGE_STAGING,
	};
	StaticAssert(ARRAYOF(s_usage) == GFX_USAGE_TYPE_NUM);
	_retUsage = s_usage[_usage];
}


//-------------------------------------------------------------------------------------------------
// 頂点用データアクセス変換
void	CGfxFormatUtilityDX11::ConvertVertexUsage(
	D3D11_USAGE& _retUsage, D3D11_BIND_FLAG& _retBind, D3D11_CPU_ACCESS_FLAG& _retCPU, D3D11_RESOURCE_MISC_FLAG& _retMisc,
	GFX_USAGE _usage
)
{
	CGfxFormatUtilityDX11::ConvertUsage(_retUsage, _usage);

	u32 bind = D3D11_BIND_VERTEX_BUFFER;
	_retBind = SCast<D3D11_BIND_FLAG>(bind);

	_retCPU = (_usage != GFX_USAGE_DYNAMIC) ? SCast<D3D11_CPU_ACCESS_FLAG>(0) : D3D11_CPU_ACCESS_WRITE;

	// マニアックすぎるためとりあえず0で
	_retMisc = SCast<D3D11_RESOURCE_MISC_FLAG>(0);
}


//-------------------------------------------------------------------------------------------------
// インデックス用データアクセス変換
void	CGfxFormatUtilityDX11::ConvertIndexUsage(
	D3D11_USAGE& _retUsage, D3D11_BIND_FLAG& _retBind, D3D11_CPU_ACCESS_FLAG& _retCPU, D3D11_RESOURCE_MISC_FLAG& _retMisc,
	GFX_USAGE _usage
	)
{
	CGfxFormatUtilityDX11::ConvertUsage(_retUsage, _usage);

	u32 bind = D3D11_BIND_INDEX_BUFFER;
	_retBind = SCast<D3D11_BIND_FLAG>(bind);

	_retCPU = (_usage != GFX_USAGE_DYNAMIC) ? SCast<D3D11_CPU_ACCESS_FLAG>(0) : D3D11_CPU_ACCESS_WRITE;

	// マニアックすぎるためとりあえず0で
	_retMisc = SCast<D3D11_RESOURCE_MISC_FLAG>(0);
}


//-------------------------------------------------------------------------------------------------
//@brief	ユニフォーム用データアクセス変換
void	CGfxFormatUtilityDX11::ConvertUniformUsage(
	D3D11_USAGE& _retUsage, D3D11_BIND_FLAG& _retBind, D3D11_CPU_ACCESS_FLAG& _retCPU, D3D11_RESOURCE_MISC_FLAG& _retMisc,
	GFX_USAGE _usage, GFX_BUFFER_ATTR _attr
	)
{
	CGfxFormatUtilityDX11::ConvertUsage(_retUsage, _usage);

	static u32 s_uniformBind[] =
	{
		D3D11_BIND_CONSTANT_BUFFER,
		D3D11_BIND_SHADER_RESOURCE,
		D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS,
	};
	StaticAssert(ARRAYOF(s_uniformBind) == GFX_BUFFER_ATTR_TYPE_NUM);
	u32 bind = s_uniformBind[_attr];
	_retBind = SCast<D3D11_BIND_FLAG>(bind);

	_retCPU = (_usage != GFX_USAGE_DYNAMIC) ? SCast<D3D11_CPU_ACCESS_FLAG>(0) : D3D11_CPU_ACCESS_WRITE;

	_retMisc = _attr == GFX_BUFFER_ATTR_CONSTANT ? SCast<D3D11_RESOURCE_MISC_FLAG>(0) : D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
}


//-------------------------------------------------------------------------------------------------
// テクスチャ用データアクセス変換
void	CGfxFormatUtilityDX11::ConvertTextureUsage(
	D3D11_USAGE& _retUsage, D3D11_BIND_FLAG& _retBind, D3D11_CPU_ACCESS_FLAG& _retCPU, D3D11_RESOURCE_MISC_FLAG& _retMisc,
	GFX_USAGE _usage
)
{
	CGfxFormatUtilityDX11::ConvertUsage(_retUsage, _usage);

	u32 bind = D3D11_BIND_SHADER_RESOURCE;
	if (_usage == GFX_USAGE_DYNAMIC)
	{
		bind |= D3D11_BIND_UNORDERED_ACCESS;
	}
	_retBind = SCast<D3D11_BIND_FLAG>(bind);

	_retCPU = (_usage != GFX_USAGE_DYNAMIC) ? SCast<D3D11_CPU_ACCESS_FLAG>(0) : D3D11_CPU_ACCESS_WRITE;

	// マニアックすぎるためとりあえず0で
	_retMisc = SCast<D3D11_RESOURCE_MISC_FLAG>(0);
}


//-------------------------------------------------------------------------------------------------
// カラーバッファ用データアクセス変換
void	CGfxFormatUtilityDX11::ConvertColorBufferUsage(
	D3D11_USAGE& _retUsage, D3D11_BIND_FLAG& _retBind, D3D11_CPU_ACCESS_FLAG& _retCPU, D3D11_RESOURCE_MISC_FLAG& _retMisc,
	GFX_USAGE _usage, b8 _isMSAA
	)
{
	CGfxFormatUtilityDX11::ConvertUsage(_retUsage, _usage);

	u32 bind = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	if (_usage == GFX_USAGE_DYNAMIC)
	{
		bind |= D3D11_BIND_UNORDERED_ACCESS;
	}
	_retBind = SCast<D3D11_BIND_FLAG>(bind);

	_retCPU = (_usage != GFX_USAGE_DYNAMIC) ? SCast<D3D11_CPU_ACCESS_FLAG>(0) : D3D11_CPU_ACCESS_WRITE;

	// マニアックすぎるためとりあえず0で
	_retMisc = SCast<D3D11_RESOURCE_MISC_FLAG>(0);
}


//-------------------------------------------------------------------------------------------------
// デプスバッファ用データアクセス変換
void	CGfxFormatUtilityDX11::ConvertDepthBufferUsage(
	D3D11_USAGE& _retUsage, D3D11_BIND_FLAG& _retBind, D3D11_CPU_ACCESS_FLAG& _retCPU, D3D11_RESOURCE_MISC_FLAG& _retMisc,
	GFX_USAGE _usage, b8 _isMSAA
	)
{
	CGfxFormatUtilityDX11::ConvertUsage(_retUsage, _usage);

	u32 bind = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	if (_usage == GFX_USAGE_DYNAMIC)
	{
		bind |= D3D11_BIND_UNORDERED_ACCESS;
	}
	_retBind = SCast<D3D11_BIND_FLAG>(bind);

	_retCPU = (_usage != GFX_USAGE_DYNAMIC) ? SCast<D3D11_CPU_ACCESS_FLAG>(0) : D3D11_CPU_ACCESS_WRITE;

	// マニアックすぎるためとりあえず0で
	_retMisc = SCast<D3D11_RESOURCE_MISC_FLAG>(0);
}


//-------------------------------------------------------------------------------------------------
// フォーマット変換
void	CGfxFormatUtilityDX11::ConvertFormat(DXGI_FORMAT& _retFormat, GFX_IMAGE_FORMAT _format)
{
	static const DXGI_FORMAT s_format[] =
	{
		DXGI_FORMAT_UNKNOWN,
		DXGI_FORMAT_R32G32B32A32_TYPELESS,
		DXGI_FORMAT_R32G32B32A32_FLOAT,
		DXGI_FORMAT_R32G32B32A32_UINT,
		DXGI_FORMAT_R32G32B32A32_SINT,
		DXGI_FORMAT_R32G32B32_TYPELESS,
		DXGI_FORMAT_R32G32B32_FLOAT,
		DXGI_FORMAT_R32G32B32_UINT,
		DXGI_FORMAT_R32G32B32_SINT,
		DXGI_FORMAT_R16G16B16A16_TYPELESS,
		DXGI_FORMAT_R16G16B16A16_FLOAT,
		DXGI_FORMAT_R16G16B16A16_UNORM,
		DXGI_FORMAT_R16G16B16A16_UINT,
		DXGI_FORMAT_R16G16B16A16_SNORM,
		DXGI_FORMAT_R16G16B16A16_SINT,
		DXGI_FORMAT_R32G32_TYPELESS,
		DXGI_FORMAT_R32G32_FLOAT,
		DXGI_FORMAT_R32G32_UINT,
		DXGI_FORMAT_R32G32_SINT,
		DXGI_FORMAT_R32G8X24_TYPELESS,
		DXGI_FORMAT_D32_FLOAT_S8X24_UINT,
		DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS,
		DXGI_FORMAT_X32_TYPELESS_G8X24_UINT,
		DXGI_FORMAT_R10G10B10A2_TYPELESS,
		DXGI_FORMAT_R10G10B10A2_UNORM,
		DXGI_FORMAT_R10G10B10A2_UINT,
		DXGI_FORMAT_R11G11B10_FLOAT,
		DXGI_FORMAT_R8G8B8A8_TYPELESS,
		DXGI_FORMAT_R8G8B8A8_UNORM,
		DXGI_FORMAT_R8G8B8A8_UNORM_SRGB,
		DXGI_FORMAT_R8G8B8A8_UINT,
		DXGI_FORMAT_R8G8B8A8_SNORM,
		DXGI_FORMAT_R8G8B8A8_SINT,
		DXGI_FORMAT_R16G16_TYPELESS,
		DXGI_FORMAT_R16G16_FLOAT,
		DXGI_FORMAT_R16G16_UNORM,
		DXGI_FORMAT_R16G16_UINT,
		DXGI_FORMAT_R16G16_SNORM,
		DXGI_FORMAT_R16G16_SINT,
		DXGI_FORMAT_R32_TYPELESS,
		DXGI_FORMAT_D32_FLOAT,
		DXGI_FORMAT_R32_FLOAT,
		DXGI_FORMAT_R32_UINT,
		DXGI_FORMAT_R32_SINT,
		DXGI_FORMAT_R24G8_TYPELESS,
		DXGI_FORMAT_D24_UNORM_S8_UINT,
		DXGI_FORMAT_R24_UNORM_X8_TYPELESS,
		DXGI_FORMAT_X24_TYPELESS_G8_UINT,
		DXGI_FORMAT_R8G8_TYPELESS,
		DXGI_FORMAT_R8G8_UNORM,
		DXGI_FORMAT_R8G8_UINT,
		DXGI_FORMAT_R8G8_SNORM,
		DXGI_FORMAT_R8G8_SINT,
		DXGI_FORMAT_R16_TYPELESS,
		DXGI_FORMAT_R16_FLOAT,
		DXGI_FORMAT_D16_UNORM,
		DXGI_FORMAT_R16_UNORM,
		DXGI_FORMAT_R16_UINT,
		DXGI_FORMAT_R16_SNORM,
		DXGI_FORMAT_R16_SINT,
		DXGI_FORMAT_R8_TYPELESS,
		DXGI_FORMAT_R8_UNORM,
		DXGI_FORMAT_R8_UINT,
		DXGI_FORMAT_R8_SNORM,
		DXGI_FORMAT_R8_SINT,
		DXGI_FORMAT_A8_UNORM,
	};
	StaticAssert(ARRAYOF(s_format) == GFX_FORMAT_TYPE_NUM);
	_retFormat = s_format[_format];
}

void	CGfxFormatUtilityDX11::ConvertFormat(GFX_IMAGE_FORMAT& _retFormat, DXGI_FORMAT _format)
{
	switch (_format)
	{
	case DXGI_FORMAT_UNKNOWN:
		_retFormat = GFX_FORMAT_UNKNOWN;
		return;
	case DXGI_FORMAT_R32G32B32A32_TYPELESS:
		_retFormat = GFX_FORMAT_R32G32B32A32_TYPELESS;
		return;
	case DXGI_FORMAT_R32G32B32A32_FLOAT:
		_retFormat = GFX_FORMAT_R32G32B32A32_FLOAT;
		return;
	case DXGI_FORMAT_R32G32B32A32_UINT:
		_retFormat = GFX_FORMAT_R32G32B32A32_UINT;
		return;
	case DXGI_FORMAT_R32G32B32A32_SINT:
		_retFormat = GFX_FORMAT_R32G32B32A32_SINT;
		return;
	case DXGI_FORMAT_R32G32B32_TYPELESS:
		_retFormat = GFX_FORMAT_R32G32B32_TYPELESS;
		return;
	case DXGI_FORMAT_R32G32B32_FLOAT:
		_retFormat = GFX_FORMAT_R32G32B32_FLOAT;
		return;
	case DXGI_FORMAT_R32G32B32_UINT:
		_retFormat = GFX_FORMAT_R32G32B32_UINT;
		return;
	case DXGI_FORMAT_R32G32B32_SINT:
		_retFormat = GFX_FORMAT_R32G32B32_SINT;
		return;
	case DXGI_FORMAT_R16G16B16A16_TYPELESS:
		_retFormat = GFX_FORMAT_R16G16B16A16_TYPELESS;
		return;
	case DXGI_FORMAT_R16G16B16A16_FLOAT:
		_retFormat = GFX_FORMAT_R16G16B16A16_FLOAT;
		return;
	case DXGI_FORMAT_R16G16B16A16_UNORM:
		_retFormat = GFX_FORMAT_R16G16B16A16_UNORM;
		return;
	case DXGI_FORMAT_R16G16B16A16_UINT:
		_retFormat = GFX_FORMAT_R16G16B16A16_UINT;
		return;
	case DXGI_FORMAT_R16G16B16A16_SNORM:
		_retFormat = GFX_FORMAT_R16G16B16A16_SNORM;
		return;
	case DXGI_FORMAT_R16G16B16A16_SINT:
		_retFormat = GFX_FORMAT_R16G16B16A16_SINT;
		return;
	case DXGI_FORMAT_R32G32_TYPELESS:
		_retFormat = GFX_FORMAT_R32G32_TYPELESS;
		return;
	case DXGI_FORMAT_R32G32_FLOAT:
		_retFormat = GFX_FORMAT_R32G32_FLOAT;
		return;
	case DXGI_FORMAT_R32G32_UINT:
		_retFormat = GFX_FORMAT_R32G32_UINT;
		return;
	case DXGI_FORMAT_R32G32_SINT:
		_retFormat = GFX_FORMAT_R32G32_SINT;
		return;
	case DXGI_FORMAT_R32G8X24_TYPELESS:
		_retFormat = GFX_FORMAT_R32G8X24_TYPELESS;
		return;
	case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:
		_retFormat = GFX_FORMAT_D32_FLOAT_S8X24_UINT;
		return;
	case DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS:
		_retFormat = GFX_FORMAT_R32_FLOAT_X8X24_TYPELESS;
		return;
	case DXGI_FORMAT_X32_TYPELESS_G8X24_UINT:
		_retFormat = GFX_FORMAT_X32_TYPELESS_G8X24_UINT;
		return;
	case DXGI_FORMAT_R10G10B10A2_TYPELESS:
		_retFormat = GFX_FORMAT_R10G10B10A2_TYPELESS;
		return;
	case DXGI_FORMAT_R10G10B10A2_UNORM:
		_retFormat = GFX_FORMAT_R10G10B10A2_UNORM;
		return;
	case DXGI_FORMAT_R10G10B10A2_UINT:
		_retFormat = GFX_FORMAT_R10G10B10A2_UINT;
		return;
	case DXGI_FORMAT_R11G11B10_FLOAT:
		_retFormat = GFX_FORMAT_R11G11B10_FLOAT;
		return;
	case DXGI_FORMAT_R8G8B8A8_TYPELESS:
		_retFormat = GFX_FORMAT_R8G8B8A8_TYPELESS;
		return;
	case DXGI_FORMAT_R8G8B8A8_UNORM:
		_retFormat = GFX_FORMAT_R8G8B8A8_UNORM;
		return;
	case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
		_retFormat = GFX_FORMAT_R8G8B8A8_UNORM_SRGB;
		return;
	case DXGI_FORMAT_R8G8B8A8_UINT:
		_retFormat = GFX_FORMAT_R8G8B8A8_UINT;
		return;
	case DXGI_FORMAT_R8G8B8A8_SNORM:
		_retFormat = GFX_FORMAT_R8G8B8A8_SNORM;
		return;
	case DXGI_FORMAT_R8G8B8A8_SINT:
		_retFormat = GFX_FORMAT_R8G8B8A8_SINT;
		return;
	case DXGI_FORMAT_R16G16_TYPELESS:
		_retFormat = GFX_FORMAT_R16G16_TYPELESS;
		return;
	case DXGI_FORMAT_R16G16_FLOAT:
		_retFormat = GFX_FORMAT_R16G16_FLOAT;
		return;
	case DXGI_FORMAT_R16G16_UNORM:
		_retFormat = GFX_FORMAT_R16G16_UNORM;
		return;
	case DXGI_FORMAT_R16G16_UINT:
		_retFormat = GFX_FORMAT_R16G16_UINT;
		return;
	case DXGI_FORMAT_R16G16_SNORM:
		_retFormat = GFX_FORMAT_R16G16_SNORM;
		return;
	case DXGI_FORMAT_R16G16_SINT:
		_retFormat = GFX_FORMAT_R16G16_SINT;
		return;
	case DXGI_FORMAT_R32_TYPELESS:
		_retFormat = GFX_FORMAT_R32_TYPELESS;
		return;
	case DXGI_FORMAT_D32_FLOAT:
		_retFormat = GFX_FORMAT_D32_FLOAT;
		return;
	case DXGI_FORMAT_R32_FLOAT:
		_retFormat = GFX_FORMAT_R32_FLOAT;
		return;
	case DXGI_FORMAT_R32_UINT:
		_retFormat = GFX_FORMAT_R32_UINT;
		return;
	case DXGI_FORMAT_R32_SINT:
		_retFormat = GFX_FORMAT_R32_SINT;
		return;
	case DXGI_FORMAT_R24G8_TYPELESS:
		_retFormat = GFX_FORMAT_R24G8_TYPELESS;
		return;
	case DXGI_FORMAT_D24_UNORM_S8_UINT:
		_retFormat = GFX_FORMAT_D24_UNORM_S8_UINT;
		return;
	case DXGI_FORMAT_R24_UNORM_X8_TYPELESS:
		_retFormat = GFX_FORMAT_R24_UNORM_G8_TYPELESS;
		return;
	case DXGI_FORMAT_X24_TYPELESS_G8_UINT:
		_retFormat = GFX_FORMAT_X24_TYPELESS_G8_UINT;
		return;
	case DXGI_FORMAT_R8G8_TYPELESS:
		_retFormat = GFX_FORMAT_R8G8_TYPELESS;
		return;
	case DXGI_FORMAT_R8G8_UNORM:
		_retFormat = GFX_FORMAT_R8G8_UNORM;
		return;
	case DXGI_FORMAT_R8G8_UINT:
		_retFormat = GFX_FORMAT_R8G8_UINT;
		return;
	case DXGI_FORMAT_R8G8_SNORM:
		_retFormat = GFX_FORMAT_R8G8_SNORM;
		return;
	case DXGI_FORMAT_R8G8_SINT:
		_retFormat = GFX_FORMAT_R8G8_SINT;
		return;
	case DXGI_FORMAT_R16_TYPELESS:
		_retFormat = GFX_FORMAT_R16_TYPELESS;
		return;
	case DXGI_FORMAT_R16_FLOAT:
		_retFormat = GFX_FORMAT_R16_FLOAT;
		return;
	case DXGI_FORMAT_D16_UNORM:
		_retFormat = GFX_FORMAT_D16_UNORM;
		return;
	case DXGI_FORMAT_R16_UNORM:
		_retFormat = GFX_FORMAT_R16_UNORM;
		return;
	case DXGI_FORMAT_R16_UINT:
		_retFormat = GFX_FORMAT_R16_UINT;
		return;
	case DXGI_FORMAT_R16_SNORM:
		_retFormat = GFX_FORMAT_R16_SNORM;
		return;
	case DXGI_FORMAT_R16_SINT:
		_retFormat = GFX_FORMAT_R16_SINT;
		return;
	case DXGI_FORMAT_R8_TYPELESS:
		_retFormat = GFX_FORMAT_R8_TYPELESS;
		return;
	case DXGI_FORMAT_R8_UNORM:
		_retFormat = GFX_FORMAT_R8_UNORM;
		return;
	case DXGI_FORMAT_R8_UINT:
		_retFormat = GFX_FORMAT_R8_UINT;
		return;
	case DXGI_FORMAT_R8_SNORM:
		_retFormat = GFX_FORMAT_R8_SNORM;
		return;
	case DXGI_FORMAT_R8_SINT:
		_retFormat = GFX_FORMAT_R8_SINT;
		return;
	case DXGI_FORMAT_A8_UNORM:
		_retFormat = GFX_FORMAT_A8_UNORM;
		return;
	default:
		_retFormat = GFX_FORMAT_UNKNOWN;
		return;
	}
}


//-------------------------------------------------------------------------------------------------
// サンプラーラップ変換
void	CGfxFormatUtilityDX11::ConvertSamplerWrap(D3D11_TEXTURE_ADDRESS_MODE& _retWrap, GFX_SAMPLER_WRAP _wrap)
{
	static const D3D11_TEXTURE_ADDRESS_MODE s_wrap[] =
	{
		SCast<D3D11_TEXTURE_ADDRESS_MODE>(0),
		D3D11_TEXTURE_ADDRESS_CLAMP,
		D3D11_TEXTURE_ADDRESS_WRAP,
		D3D11_TEXTURE_ADDRESS_MIRROR,
		D3D11_TEXTURE_ADDRESS_MIRROR_ONCE,
		D3D11_TEXTURE_ADDRESS_BORDER,
	};
	StaticAssert(ARRAYOF(s_wrap) == GFX_SAMPLER_WRAP_TYPE_NUM);
	_retWrap = s_wrap[_wrap];
}

//-------------------------------------------------------------------------------------------------
// サンプラーフィルター変換
void	CGfxFormatUtilityDX11::ConvertSamplerFilter(D3D11_FILTER& _retFilter, GFX_SAMPLER_FILTER _min, GFX_SAMPLER_FILTER _mag, GFX_SAMPLER_FILTER _mip, b8 _isAniso)
{
	StaticAssert(GFX_SAMPLER_FILTER_TYPE_NUM == 2);
	if (_min == GFX_SAMPLER_FILTER_LINEAR)
	{
		if (_mag == GFX_SAMPLER_FILTER_LINEAR)
		{
			if (_mip == GFX_SAMPLER_FILTER_LINEAR)
			{
				_retFilter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
			}
			else
			{
				_retFilter = D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT;
			}
		}
		else
		{
			if (_mip == GFX_SAMPLER_FILTER_LINEAR)
			{
				_retFilter = D3D11_FILTER_MIN_LINEAR_MAG_POINT_MIP_LINEAR;
			}
			else
			{
				_retFilter = D3D11_FILTER_MIN_LINEAR_MAG_MIP_POINT;
			}
		}
	}
	else
	{
		if (_mag == GFX_SAMPLER_FILTER_LINEAR)
		{
			if (_mip == GFX_SAMPLER_FILTER_LINEAR)
			{
				_retFilter = D3D11_FILTER_MIN_POINT_MAG_MIP_LINEAR;
			}
			else
			{
				_retFilter = D3D11_FILTER_MIN_POINT_MAG_LINEAR_MIP_POINT;
			}
		}
		else
		{
			if (_mip == GFX_SAMPLER_FILTER_LINEAR)
			{
				_retFilter = D3D11_FILTER_MIN_MAG_POINT_MIP_LINEAR;
			}
			else
			{
				_retFilter = D3D11_FILTER_MIN_MAG_MIP_POINT;
			}
		}
	}
	// 異方性フィルター
	if (_isAniso)
	{
		_retFilter = D3D11_FILTER_ANISOTROPIC;
	}
}


//-------------------------------------------------------------------------------------------------
// 比較式変換
void	CGfxFormatUtilityDX11::ConvertTestFunc(D3D11_COMPARISON_FUNC& _retTest, GFX_TEST _test)
{
	static const D3D11_COMPARISON_FUNC s_test[] =
	{
		SCast<D3D11_COMPARISON_FUNC>(0),
		D3D11_COMPARISON_NEVER,
		D3D11_COMPARISON_LESS,
		D3D11_COMPARISON_EQUAL,
		D3D11_COMPARISON_LESS_EQUAL,
		D3D11_COMPARISON_GREATER,
		D3D11_COMPARISON_GREATER_EQUAL,
		D3D11_COMPARISON_NOT_EQUAL,
		D3D11_COMPARISON_ALWAYS,
	};
	StaticAssert(ARRAYOF(s_test) == GFX_TEST_TYPE_NUM);
	_retTest = s_test[_test];
}


//-------------------------------------------------------------------------------------------------
// 頂点レイアウト変換
void	CGfxFormatUtilityDX11::ConvertVertexDecl(D3D11_INPUT_ELEMENT_DESC(&_retDecl)[VERTEX_ATTRIBUTE_TYPE_NUM], const VTX_DECL* _pDecl, u32 _num)
{
	struct SEMANTICS
	{
		const c8* pName;
		u32	uIndex;
	};
	static const SEMANTICS s_semantics[] = 
	{
		{ "POSITION",		0, },
		{ "NORMAL",			0, },
		{ "TANGENT",		0, },
		{ "BINORMAL",		0, },
		{ "BLENDWEIGHT",	0, },
		{ "BLENDWEIGHT",	1, },
		{ "BLENDINDICES",	0, },
		{ "BLENDINDICES",	1, },
		{ "COLOR",			0, },
		{ "COLOR",			1, },
		{ "TEXCOORD",		0, },
		{ "TEXCOORD",		1, },
		{ "TEXCOORD",		2, },
		{ "TEXCOORD",		3, },
	};
	StaticAssert(ARRAYOF(s_semantics) == VERTEX_ATTRIBUTE_TYPE_NUM);

	for (u32 i = 0; i < _num; i++)
	{
		VERTEX_ATTRIBUTE attr = _pDecl[i].attr;
		_retDecl[i].SemanticName = s_semantics[attr].pName;
		_retDecl[i].SemanticIndex = s_semantics[attr].uIndex;
		_retDecl[i].InputSlot = _pDecl[i].slot;
		_retDecl[i].AlignedByteOffset = _pDecl[i].offset;
		GFX_IMAGE_FORMAT format = CGfxFormatUtility::ConvertImageFormat(_pDecl[i].valType, _pDecl[i].elemNum);
		 CGfxFormatUtilityDX11::ConvertFormat(_retDecl[i].Format, format);
		_retDecl[i].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
		_retDecl[i].InstanceDataStepRate = 0;
	}
}


//-------------------------------------------------------------------------------------------------
// プリミティブ変換
void	CGfxFormatUtilityDX11::ConvertPrimitive(D3D11_PRIMITIVE_TOPOLOGY& _retPrim, GFX_PRIMITIVE _prim)
{
	static const D3D11_PRIMITIVE_TOPOLOGY s_prim[] =
	{
		D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED,

		D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,

		D3D11_PRIMITIVE_TOPOLOGY_LINELIST,
		D3D11_PRIMITIVE_TOPOLOGY_LINELIST,
		D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP,

		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP,

		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP,
		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP,

		D3D11_PRIMITIVE_TOPOLOGY_LINELIST_ADJ,
		D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP_ADJ,

		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST_ADJ,
		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP_ADJ,
	};
	StaticAssert(ARRAYOF(s_prim) == GFX_PRIMITIVE_TYPE_NUM);
	_retPrim = s_prim[_prim];
}


//-------------------------------------------------------------------------------------------------
// カリング面変換
void	CGfxFormatUtilityDX11::ConvertCullFace(D3D11_CULL_MODE& _retFace, GFX_CULL_FACE _face)
{
	static const D3D11_CULL_MODE s_cullFace[] =
	{
		D3D11_CULL_NONE,
		D3D11_CULL_FRONT,
		D3D11_CULL_BACK,
	};
	StaticAssert(ARRAYOF(s_cullFace) == GFX_CULL_FACE_TYPE_NUM);
	_retFace = s_cullFace[_face];
}


//-------------------------------------------------------------------------------------------------
// ブレンドオペレーション変換
void	CGfxFormatUtilityDX11::ConvertBlendOp(D3D11_BLEND_OP& _retBlendOp, GFX_BLEND_OP _blendOp)
{
	static const D3D11_BLEND_OP s_blendOp[] =
	{
		SCast<D3D11_BLEND_OP>(0),
		D3D11_BLEND_OP_ADD,
		D3D11_BLEND_OP_SUBTRACT,
		D3D11_BLEND_OP_REV_SUBTRACT,
		D3D11_BLEND_OP_MIN,
		D3D11_BLEND_OP_MAX,
	};
	StaticAssert(ARRAYOF(s_blendOp) == GFX_BLEND_OP_TYPE_NUM);
	_retBlendOp = s_blendOp[_blendOp];
}


//-------------------------------------------------------------------------------------------------
// ブレンド式変換
void	CGfxFormatUtilityDX11::ConvertBlendFunc(D3D11_BLEND& _retBlendFunc, GFX_BLEND_FUNC _blendFunc)
{
	static const D3D11_BLEND s_blendFunc[] =
	{
		SCast<D3D11_BLEND>(0),
		D3D11_BLEND_ZERO,
		D3D11_BLEND_ONE,
		D3D11_BLEND_SRC_COLOR,
		D3D11_BLEND_INV_SRC_COLOR,
		D3D11_BLEND_SRC_ALPHA,
		D3D11_BLEND_INV_SRC_ALPHA,
		D3D11_BLEND_DEST_COLOR,
		D3D11_BLEND_INV_DEST_COLOR,
		D3D11_BLEND_DEST_ALPHA,
		D3D11_BLEND_INV_DEST_ALPHA,

		D3D11_BLEND_SRC_ALPHA_SAT,

		D3D11_BLEND_BLEND_FACTOR,		// 別れてないっぽい
		D3D11_BLEND_INV_BLEND_FACTOR,	// 別れてないっぽい
		D3D11_BLEND_BLEND_FACTOR,		// 別れてないっぽい
		D3D11_BLEND_INV_BLEND_FACTOR,	// 別れてないっぽい

		D3D11_BLEND_SRC1_COLOR,
		D3D11_BLEND_INV_SRC1_COLOR,
		D3D11_BLEND_SRC1_ALPHA,
		D3D11_BLEND_INV_SRC1_ALPHA,
	};
	StaticAssert(ARRAYOF(s_blendFunc) == GFX_BLEND_FUNC_TYPE_NUM);
	_retBlendFunc = s_blendFunc[_blendFunc];
}


//-------------------------------------------------------------------------------------------------
// カラーマスク変換
void	CGfxFormatUtilityDX11::ConvertColorMask(D3D11_COLOR_WRITE_ENABLE& _retColorMask, GFX_COLOR_MASK _colorMask)
{
	_retColorMask = SCast<D3D11_COLOR_WRITE_ENABLE>(_colorMask);
}


//-------------------------------------------------------------------------------------------------
// ステンシルオペレーション変換
void	CGfxFormatUtilityDX11::ConvertStencilOp(D3D11_STENCIL_OP& _retStencilOp, GFX_STENCIL_OP _stencilOp)
{
	static const D3D11_STENCIL_OP s_stencilOp[] =
	{
		SCast<D3D11_STENCIL_OP>(0),
		D3D11_STENCIL_OP_KEEP,
		D3D11_STENCIL_OP_ZERO,
		D3D11_STENCIL_OP_REPLACE,
		D3D11_STENCIL_OP_INCR_SAT,
		D3D11_STENCIL_OP_DECR_SAT,
		D3D11_STENCIL_OP_INCR,
		D3D11_STENCIL_OP_DECR,
		D3D11_STENCIL_OP_INVERT,
	};
	StaticAssert(ARRAYOF(s_stencilOp) == GFX_STENCIL_OP_TYPE_NUM);
	_retStencilOp = s_stencilOp[_stencilOp];
}


POISON_END

#endif // LIB_GFX_DX11

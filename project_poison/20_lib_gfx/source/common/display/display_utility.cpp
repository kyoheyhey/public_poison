﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "display/display_utility.h"

#include "renderer/render_target.h"
#include "display/display.h"

#if defined( LIB_SYSTEM_WIN )
#include "device/device_utility.h"
#include "device/device_win.h"
#endif // LIB_SYSTEM_WIN


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant
static CDisplay*		s_pDisplay = NULL;
static CRenderTarget*	s_pRenderTarget = NULL;
static u32				s_renderTargetNum = 0;
static MEM_HANDLE		s_memHdl = MEM_HDL_INVALID;


///-------------------------------------------------------------------------------------------------
/// ディスプレイ取得
CDisplay*		CDisplayUtility::GetDisplay()
{
	libAssert(s_pDisplay);
	return s_pDisplay;
}

///-------------------------------------------------------------------------------------------------
/// レンダーターゲット取得
CRenderTarget*	CDisplayUtility::GetRenderTarget(u32 _idx)
{
	libAssert(_idx < s_renderTargetNum);
	return &s_pRenderTarget[_idx];
}

///-------------------------------------------------------------------------------------------------
/// セットアップ
void	CDisplayUtility::Setup(CDisplay* _pDisplay, CRenderTarget* pRenderTarget, u32 _num)
{
	libAssert(_pDisplay);

	s_pDisplay = _pDisplay;
	libAssert(s_pDisplay);

	s_pRenderTarget = pRenderTarget;
	s_renderTargetNum = _num;
}

///-------------------------------------------------------------------------------------------------
/// 終了
void	CDisplayUtility::Finalize()
{
	SAFE_RELEASE(s_pDisplay);
	for (u32 idx = 0; idx < s_renderTargetNum; idx++)
	{
		s_pRenderTarget[idx].Release();
	}
	s_renderTargetNum = 0;
}

///-------------------------------------------------------------------------------------------------
/// 更新
void		CDisplayUtility::Step()
{
#if defined( LIB_SYSTEM_WIN )
	CDeviceWin* pDevice = SDCast<CDeviceWin*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);

	// ウィンドウがリサイズされていたら
	if (pDevice->IsResize())
	{
		u32 width, height;
		pDevice->GetClientSize(width, height);

		libAssert(s_pDisplay);
		s_pDisplay->ResizeTarget(width, height);
	}
#endif // LIB_SYSTEM_WIN
}

///-------------------------------------------------------------------------------------------------
/// 垂直同期
b8		CDisplayUtility::WaitVsync(u32 _syncInterval)
{
	libAssert(s_pDisplay);
	return s_pDisplay->WaitVsync(_syncInterval);
}



POISON_END

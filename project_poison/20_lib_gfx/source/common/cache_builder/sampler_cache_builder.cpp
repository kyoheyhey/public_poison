﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "cache_builder/sampler_cache_builder.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///*************************************************************************************************
///	デプスステンシルステートキャッシュビルダー
///*************************************************************************************************

CSamplerCacheBuilder::CSamplerCacheBuilder()
{
}

CSamplerCacheBuilder::~CSamplerCacheBuilder()
{
}

///-------------------------------------------------------------------------------------------------
/// 初期化
b8		CSamplerCacheBuilder::Initialize(MEM_HANDLE _mem, u16 _num)
{
	return m_cacheList.Initialize(_mem, _num);
}

///-------------------------------------------------------------------------------------------------
/// 終了
void	CSamplerCacheBuilder::Finalize()
{
	CACHE_LIST::ITR* pItr = m_cacheList.GetBgn();
	while (pItr)
	{
		CACHE_OBJECT& rObj = m_cacheList.GetDataFromItr(pItr);
		// オブジェクト破棄
		rObj.object.Release();

		pItr = m_cacheList.GetNext(pItr);
	}
	m_cacheList.Finalize();
}

///-------------------------------------------------------------------------------------------------
/// キャッシュビルド
const CSampler*		CSamplerCacheBuilder::Create(const SAMPLER_DESC& _desc, DRAW_CONTEXT* _pDrawContext/* = NULL*/)
{
	CACHE_OBJECT object;
	object.hash = CRC32(&_desc, sizeof(SAMPLER_DESC));
	CACHE_LIST::ITR* pItr = m_cacheList.PushBack(object);
	if (pItr)
	{
		CACHE_OBJECT& rObj = m_cacheList.GetDataFromItr(pItr);
		// 有効化されてなければ生成
		if (!rObj.object.IsEnable())
		{
			rObj.object.Create(_desc, _pDrawContext);
		}
		return &rObj.object;
	}
	return NULL;
}

///-------------------------------------------------------------------------------------------------
/// キャッシュクリア
void	CSamplerCacheBuilder::Clear()
{
	CACHE_LIST::ITR* pItr = m_cacheList.GetBgn();
	while (pItr)
	{
		CACHE_OBJECT& rObj = m_cacheList.GetDataFromItr(pItr);
		// オブジェクト破棄
		rObj.object.Release();
		rObj.hash = hash32::Invalid;
		pItr = m_cacheList.GetNext(pItr);
	}
	m_cacheList.Clear();
}


POISON_END


﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "cache_builder/vertex_cache_builder.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///*************************************************************************************************
///	ユニフォームバッファキャッシュビルダー
///*************************************************************************************************

CVertexCacheBuilder::CVertexCacheBuilder()
{
}

CVertexCacheBuilder::~CVertexCacheBuilder()
{
}

///-------------------------------------------------------------------------------------------------
/// 初期化
b8		CVertexCacheBuilder::Initialize(MEM_HANDLE _mem, u16 _num)
{
	return m_cacheList.Initialize(_mem, _num);
}

///-------------------------------------------------------------------------------------------------
/// 終了
void	CVertexCacheBuilder::Finalize()
{
	CACHE_SEARCH_LIST::ITR* pItr = m_cacheList.GetBgn();
	while (pItr)
	{
		CACHE_OBJECT& rObj = m_cacheList.GetDataFromItr(pItr);
		// オブジェクト破棄
		rObj.object.Release();

		pItr = m_cacheList.GetNext(pItr);
	}
	m_cacheList.Finalize();
}

///-------------------------------------------------------------------------------------------------
/// キャッシュビルド
const CVertexBuffer*	CVertexCacheBuilder::Create(u32 _elemSize, u32 _elemNum, const void* _data/* = NULL*/, DRAW_CONTEXT* _pDrawContext/* = NULL*/)
{
	CVertexBuffer* pUniBuffer = NULL;

	u32 keySize = _elemSize * _elemNum;
	CACHE_OBJECT* pObject = m_cacheList.CacheAndCreate(keySize);
	if (pObject)
	{
		pUniBuffer = &pObject->object;
		if (_data)
		{
			if (pUniBuffer->IsEnable())
			{
				// 更新
				pUniBuffer->Update(_elemSize * _elemNum, _data, _pDrawContext);
			}
			else
			{
				// 生成
				VTX_DESC desc(_data, _elemSize, _elemNum, GFX_USAGE_DYNAMIC);
				pUniBuffer->Create(desc, _pDrawContext);
			}
		}
	}
	return pUniBuffer;
}

///-------------------------------------------------------------------------------------------------
/// キャッシュクリア
void	CVertexCacheBuilder::Clear()
{
#if 0
	CACHE_LIST::ITR* pItr = m_cacheList.GetBgn();
	while (pItr)
	{
		CACHE_OBJECT& rObj = m_cacheList.GetDataFromItr(pItr);
		// オブジェクト破棄
		rObj.object.Release();
		rObj.hash = hash32::Invalid;
		pItr = m_cacheList.GetNext(pItr);
	}
#endif // 0
	m_cacheList.UsedReset();
}


POISON_END


﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "cache_builder/depth_state_cache_builder.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///*************************************************************************************************
///	デプスステンシルステートキャッシュビルダー
///*************************************************************************************************

CDepthStateCacheBuilder::CDepthStateCacheBuilder()
{
}

CDepthStateCacheBuilder::~CDepthStateCacheBuilder()
{
}


///-------------------------------------------------------------------------------------------------
/// 初期化
b8		CDepthStateCacheBuilder::Initialize(MEM_HANDLE _mem, u16 _num)
{
	return m_cacheList.Initialize(_mem, _num);
}

///-------------------------------------------------------------------------------------------------
/// 終了
void	CDepthStateCacheBuilder::Finalize()
{
	CACHE_LIST::ITR* pItr = m_cacheList.GetBgn();
	while (pItr)
	{
		CACHE_OBJECT& rObj = m_cacheList.GetDataFromItr(pItr);
		// オブジェクト破棄
		rObj.object.Release();

		pItr = m_cacheList.GetNext(pItr);
	}
	m_cacheList.Finalize();
}

///-------------------------------------------------------------------------------------------------
/// キャッシュビルド
const CDepthState*	CDepthStateCacheBuilder::Create(const DEPTH_STATE_DESC& _desc)
{
	CACHE_OBJECT object;
	object.hash = CRC32(&_desc, sizeof(DEPTH_STATE_DESC));
	CACHE_LIST::ITR* pItr = m_cacheList.PushBack(object);
	if (pItr)
	{
		CACHE_OBJECT& rObj = m_cacheList.GetDataFromItr(pItr);
		// 有効化されてなければ生成
		if (!rObj.object.IsEnable())
		{
			rObj.object.Create(_desc);
		}
		return &rObj.object;
	}
	return NULL;
}

///-------------------------------------------------------------------------------------------------
/// キャッシュクリア
void	CDepthStateCacheBuilder::Clear()
{
	CACHE_LIST::ITR* pItr = m_cacheList.GetBgn();
	while (pItr)
	{
		CACHE_OBJECT& rObj = m_cacheList.GetDataFromItr(pItr);
		// オブジェクト破棄
		rObj.object.Release();
		rObj.hash = hash32::Invalid;
		pItr = m_cacheList.GetNext(pItr);
	}
	m_cacheList.Clear();
}


POISON_END


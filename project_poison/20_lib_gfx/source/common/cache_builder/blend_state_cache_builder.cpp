﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "cache_builder/blend_state_cache_builder.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///*************************************************************************************************
///	ブレンドステートキャッシュビルダー
///*************************************************************************************************

CBlendStateCacheBuilder::CBlendStateCacheBuilder()
{
}

CBlendStateCacheBuilder::~CBlendStateCacheBuilder()
{
}


///-------------------------------------------------------------------------------------------------
/// 初期化
b8		CBlendStateCacheBuilder::Initialize(MEM_HANDLE _mem, u16 _num)
{
	return m_cacheList.Initialize(_mem, _num);
}

///-------------------------------------------------------------------------------------------------
/// 終了
void	CBlendStateCacheBuilder::Finalize()
{
	CACHE_LIST::ITR* pItr = m_cacheList.GetBgn();
	while (pItr)
	{
		CACHE_OBJECT& rObj = m_cacheList.GetDataFromItr(pItr);
		// オブジェクト破棄
		rObj.object.Release();

		pItr = m_cacheList.GetNext(pItr);
	}
	m_cacheList.Finalize();
}

///-------------------------------------------------------------------------------------------------
/// キャッシュビルド
const CBlendState*	CBlendStateCacheBuilder::Create(const BLEND_STATE_DESC& _desc)
{
	CACHE_OBJECT object;
	object.hash = CRC32(&_desc, sizeof(BLEND_STATE_DESC));
	CACHE_LIST::ITR* pItr = m_cacheList.PushBack(object);
	if (pItr)
	{
		CACHE_OBJECT& rObj = m_cacheList.GetDataFromItr(pItr);
		// 有効化されてなければ生成
		if (!rObj.object.IsEnable())
		{
			rObj.object.Create(_desc);
		}
		return &rObj.object;
	}
	return NULL;
}

///-------------------------------------------------------------------------------------------------
/// キャッシュクリア
void	CBlendStateCacheBuilder::Clear()
{
	CACHE_LIST::ITR* pItr = m_cacheList.GetBgn();
	while (pItr)
	{
		CACHE_OBJECT& rObj = m_cacheList.GetDataFromItr(pItr);
		// オブジェクト破棄
		rObj.object.Release();
		rObj.hash = hash32::Invalid;
		pItr = m_cacheList.GetNext(pItr);
	}
	m_cacheList.Clear();
}


POISON_END


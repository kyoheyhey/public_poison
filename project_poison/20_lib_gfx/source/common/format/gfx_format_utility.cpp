﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "format/gfx_format_utility.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant



///-------------------------------------------------------------------------------------------------
/// 値フォーマットサイズ取得
u32	CGfxFormatUtility::GetValueSize(GFX_VALUE _varType)
{
	static const u32 s_formatSize[] =
	{
		0,
		sizeof(c8),
		sizeof(u8),
		sizeof(s16),
		sizeof(u16),
		sizeof(s32),
		sizeof(u32),
		sizeof(f32),
		sizeof(s64),
		sizeof(u64),
		sizeof(f64),
	};
	StaticAssert(ARRAYOF(s_formatSize) == GFX_VALUE_TYPE_NUM);
	return s_formatSize[_varType];
}


//-------------------------------------------------------------------------------------------------
//@brief	サイズから値フォーマット取得
GFX_VALUE	CGfxFormatUtility::GetValueFormatS(u32 _size)
{
	switch (_size)
	{
	case 1:
		return GFX_VALUE_S8;
	case 2:
		return GFX_VALUE_S16;
	case 4:
		return GFX_VALUE_S32;
	case 8:
		return GFX_VALUE_S64;
	default:
		break;
	}
	return GFX_VALUE_UNKNOWN;
}
GFX_VALUE	CGfxFormatUtility::GetValueFormatU(u32 _size)
{
	switch (_size)
	{
	case 1:
		return GFX_VALUE_U8;
	case 2:
		return GFX_VALUE_U16;
	case 4:
		return GFX_VALUE_U32;
	case 8:
		return GFX_VALUE_U64;
	default:
		break;
	}
	return GFX_VALUE_UNKNOWN;
}
GFX_VALUE	CGfxFormatUtility::GetValueFormatF(u32 _size)
{
	switch (_size)
	{
	case 4:
		return GFX_VALUE_F32;
	case 8:
		return GFX_VALUE_F64;
	default:
		break;
	}
	return GFX_VALUE_UNKNOWN;
}


///-------------------------------------------------------------------------------------------------
/// シェーダステージ名取得
const c8*	CGfxFormatUtility::GetShaderStageName(GFX_SHADER _shader)
{
	static const c8* s_shaderStageName[] =
	{
		"vertex",
		"geometry",
		"pixel",
		"compute",
	};
	StaticAssert(ARRAYOF(s_shaderStageName) == GFX_SHADER_TYPE_NUM);
	return s_shaderStageName[_shader];
}


///-------------------------------------------------------------------------------------------------
/// イメージフォーマットサイズ取得
u32		CGfxFormatUtility::GetImageFormatSize(GFX_IMAGE_FORMAT _format)
{
	static const u32 s_formatSize[] =
	{
		0,		// DXGI_FORMAT_UNKNOWN,
		128u,	// DXGI_FORMAT_R32G32B32A32_TYPELESS,
		128u,	// DXGI_FORMAT_R32G32B32A32_FLOAT,
		128u,	// DXGI_FORMAT_R32G32B32A32_UINT,
		128u,	// DXGI_FORMAT_R32G32B32A32_SINT,
		96u,	// DXGI_FORMAT_R32G32B32_TYPELESS,
		96u,	// DXGI_FORMAT_R32G32B32_FLOAT,
		96u,	// DXGI_FORMAT_R32G32B32_UINT,
		96u,	// DXGI_FORMAT_R32G32B32_SINT,
		64u,	// DXGI_FORMAT_R16G16B16A16_TYPELESS,
		64u,	// DXGI_FORMAT_R16G16B16A16_FLOAT,
		64u,	// DXGI_FORMAT_R16G16B16A16_UNORM,
		64u,	// DXGI_FORMAT_R16G16B16A16_UINT,
		64u,	// DXGI_FORMAT_R16G16B16A16_SNORM,
		64u,	// DXGI_FORMAT_R16G16B16A16_SINT,
		64u,	// DXGI_FORMAT_R32G32_TYPELESS,
		64u,	// DXGI_FORMAT_R32G32_FLOAT,
		64u,	// DXGI_FORMAT_R32G32_UINT,
		64u,	// DXGI_FORMAT_R32G32_SINT,
		64u,	// DXGI_FORMAT_R32G8X24_TYPELESS,
		64u,	// DXGI_FORMAT_D32_FLOAT_S8X24_UINT,
		64u,	// DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS,
		64u,	// DXGI_FORMAT_X32_TYPELESS_G8X24_UINT,
		32u,	// DXGI_FORMAT_R10G10B10A2_TYPELESS,
		32u,	// DXGI_FORMAT_R10G10B10A2_UNORM,
		32u,	// DXGI_FORMAT_R10G10B10A2_UINT,
		32u,	// DXGI_FORMAT_R11G11B10_FLOAT,
		32u,	// DXGI_FORMAT_R8G8B8A8_TYPELESS,
		32u,	// DXGI_FORMAT_R8G8B8A8_UNORM,
		32u,	// DXGI_FORMAT_R8G8B8A8_UNORM_SRGB,
		32u,	// DXGI_FORMAT_R8G8B8A8_UINT,
		32u,	// DXGI_FORMAT_R8G8B8A8_SNORM,
		32u,	// DXGI_FORMAT_R8G8B8A8_SINT,
		32u,	// DXGI_FORMAT_R16G16_TYPELESS,
		32u,	// DXGI_FORMAT_R16G16_FLOAT,
		32u,	// DXGI_FORMAT_R16G16_UNORM,
		32u,	// DXGI_FORMAT_R16G16_UINT,
		32u,	// DXGI_FORMAT_R16G16_SNORM,
		32u,	// DXGI_FORMAT_R16G16_SINT,
		32u,	// DXGI_FORMAT_R32_TYPELESS,
		32u,	// DXGI_FORMAT_D32_FLOAT,
		32u,	// DXGI_FORMAT_R32_FLOAT,
		32u,	// DXGI_FORMAT_R32_UINT,
		32u,	// DXGI_FORMAT_R32_SINT,
		32u,	// DXGI_FORMAT_R24G8_TYPELESS,
		32u,	// DXGI_FORMAT_D24_UNORM_S8_UINT,
		32u,	// DXGI_FORMAT_R24_UNORM_X8_TYPELESS,
		32u,	// DXGI_FORMAT_X24_TYPELESS_G8_UINT,
		16u,	// DXGI_FORMAT_R8G8_TYPELESS,
		16u,	// DXGI_FORMAT_R8G8_UNORM,
		16u,	// DXGI_FORMAT_R8G8_UINT,
		16u,	// DXGI_FORMAT_R8G8_SNORM,
		16u,	// DXGI_FORMAT_R8G8_SINT,
		16u,	// DXGI_FORMAT_R16_TYPELESS,
		16u,	// DXGI_FORMAT_R16_FLOAT,
		16u,	// DXGI_FORMAT_D16_UNORM,
		16u,	// DXGI_FORMAT_R16_UNORM,
		16u,	// DXGI_FORMAT_R16_UINT,
		16u,	// DXGI_FORMAT_R16_SNORM,
		16u,	// DXGI_FORMAT_R16_SINT,
		8u,		// DXGI_FORMAT_R8_TYPELESS,
		8u,		// DXGI_FORMAT_R8_UNORM,
		8u,		// DXGI_FORMAT_R8_UINT,
		8u,		// DXGI_FORMAT_R8_SNORM,
		8u,		// DXGI_FORMAT_R8_SINT,
		8u,		// DXGI_FORMAT_A8_UNORM,
	};
	StaticAssert(ARRAYOF(s_formatSize) == GFX_FORMAT_TYPE_NUM);
	return s_formatSize[_format];
}


///-------------------------------------------------------------------------------------------------
/// イメージフォーマット変換
GFX_IMAGE_FORMAT	CGfxFormatUtility::ConvertImageFormat(GFX_VALUE _val, u32 _elemNum)
{
	// 64bitは対象外 && 要素数は1～4
	libAssert(_val != GFX_VALUE_UNKNOWN && _val <= GFX_VALUE_F32);
	libAssert(0 <_elemNum && _elemNum <= 4);

	static GFX_IMAGE_FORMAT s_imageFormat[] =
	{
		GFX_FORMAT_UNKNOWN,
		GFX_FORMAT_UNKNOWN,
		GFX_FORMAT_UNKNOWN,
		GFX_FORMAT_UNKNOWN,

		GFX_FORMAT_R8_SINT,
		GFX_FORMAT_R8G8_SINT,
		GFX_FORMAT_UNKNOWN,
		GFX_FORMAT_R8G8B8A8_SINT,

		GFX_FORMAT_R8_UINT,
		GFX_FORMAT_R8G8_UINT,
		GFX_FORMAT_UNKNOWN,
		GFX_FORMAT_R8G8B8A8_UINT,

		GFX_FORMAT_R16_SINT,
		GFX_FORMAT_R16G16_SINT,
		GFX_FORMAT_UNKNOWN,
		GFX_FORMAT_R16G16B16A16_SINT,

		GFX_FORMAT_R16_UINT,
		GFX_FORMAT_R16G16_UINT,
		GFX_FORMAT_UNKNOWN,
		GFX_FORMAT_R16G16B16A16_UINT,

		GFX_FORMAT_R32_SINT,
		GFX_FORMAT_R32G32_SINT,
		GFX_FORMAT_R32G32B32_SINT,
		GFX_FORMAT_R32G32B32A32_SINT,

		GFX_FORMAT_R32_UINT,
		GFX_FORMAT_R32G32_UINT,
		GFX_FORMAT_R32G32B32_UINT,
		GFX_FORMAT_R32G32B32A32_UINT,

		GFX_FORMAT_R32_FLOAT,
		GFX_FORMAT_R32G32_FLOAT,
		GFX_FORMAT_R32G32B32_FLOAT,
		GFX_FORMAT_R32G32B32A32_FLOAT,
	};
	u32 idx = _val * 4u + (_elemNum - 1);
	return s_imageFormat[idx];
}


///-------------------------------------------------------------------------------------------------
/// 型無しイメージフォーマット変換
GFX_IMAGE_FORMAT	CGfxFormatUtility::ConvertTypelessFormat(GFX_IMAGE_FORMAT _format)
{
	switch (_format)
	{
	case GFX_FORMAT_R32G32B32A32_FLOAT:
	case GFX_FORMAT_R32G32B32A32_UINT:
	case GFX_FORMAT_R32G32B32A32_SINT:
		return GFX_FORMAT_R32G32B32A32_TYPELESS;
	case GFX_FORMAT_R32G32B32_FLOAT:
	case GFX_FORMAT_R32G32B32_UINT:
	case GFX_FORMAT_R32G32B32_SINT:
		return GFX_FORMAT_R32G32B32_TYPELESS;
	case GFX_FORMAT_R16G16B16A16_FLOAT:
	case GFX_FORMAT_R16G16B16A16_UNORM:
	case GFX_FORMAT_R16G16B16A16_UINT:
	case GFX_FORMAT_R16G16B16A16_SNORM:
	case GFX_FORMAT_R16G16B16A16_SINT:
		return GFX_FORMAT_R16G16B16A16_TYPELESS;
	case GFX_FORMAT_R32G32_FLOAT:
	case GFX_FORMAT_R32G32_UINT:
	case GFX_FORMAT_R32G32_SINT:
		return GFX_FORMAT_R32G32_TYPELESS;
	case GFX_FORMAT_D32_FLOAT_S8X24_UINT:
	case GFX_FORMAT_X32_TYPELESS_G8X24_UINT:
	case GFX_FORMAT_R32_FLOAT_X8X24_TYPELESS:
		return GFX_FORMAT_R32G8X24_TYPELESS;
	case GFX_FORMAT_R10G10B10A2_UNORM:
	case GFX_FORMAT_R10G10B10A2_UINT:
		return GFX_FORMAT_R10G10B10A2_TYPELESS;
	case GFX_FORMAT_R8G8B8A8_UNORM:
	case GFX_FORMAT_R8G8B8A8_UNORM_SRGB:
	case GFX_FORMAT_R8G8B8A8_UINT:
	case GFX_FORMAT_R8G8B8A8_SNORM:
	case GFX_FORMAT_R8G8B8A8_SINT:
		return GFX_FORMAT_R8G8B8A8_TYPELESS;
	case GFX_FORMAT_R16G16_FLOAT:
	case GFX_FORMAT_R16G16_UINT:
	case GFX_FORMAT_R16G16_SNORM:
	case GFX_FORMAT_R16G16_SINT:
		return GFX_FORMAT_R16G16_TYPELESS;
	case GFX_FORMAT_D32_FLOAT:
	case GFX_FORMAT_R32_FLOAT:
	case GFX_FORMAT_R32_UINT:
	case GFX_FORMAT_R32_SINT:
		return GFX_FORMAT_R32_TYPELESS;
	case GFX_FORMAT_D24_UNORM_S8_UINT:
	case GFX_FORMAT_R24_UNORM_G8_TYPELESS:
	case GFX_FORMAT_X24_TYPELESS_G8_UINT:
		return GFX_FORMAT_R24G8_TYPELESS;
	case GFX_FORMAT_R8G8_UNORM:
	case GFX_FORMAT_R8G8_UINT:
	case GFX_FORMAT_R8G8_SNORM:
	case GFX_FORMAT_R8G8_SINT:
		return GFX_FORMAT_R8G8_TYPELESS;
	case GFX_FORMAT_R16_FLOAT:
	case GFX_FORMAT_D16_UNORM:
	case GFX_FORMAT_R16_UNORM:
	case GFX_FORMAT_R16_UINT:
	case GFX_FORMAT_R16_SNORM:
	case GFX_FORMAT_R16_SINT:
		return GFX_FORMAT_R16_TYPELESS;
	case GFX_FORMAT_R8_UNORM:
	case GFX_FORMAT_R8_UINT:
	case GFX_FORMAT_R8_SNORM:
	case GFX_FORMAT_R8_SINT:
	case GFX_FORMAT_A8_UNORM:
		return GFX_FORMAT_R8_TYPELESS;
	default:
		break;
	}
	return _format;
}


///-------------------------------------------------------------------------------------------------
/// デプスからカラーイメージフォーマット変換
GFX_IMAGE_FORMAT	CGfxFormatUtility::ConvertDepthToColorFormat(GFX_IMAGE_FORMAT _format)
{
	switch (_format)
	{
	case GFX_FORMAT_D32_FLOAT_S8X24_UINT:
		return GFX_FORMAT_R32_FLOAT_X8X24_TYPELESS;
	case GFX_FORMAT_D32_FLOAT:
		return GFX_FORMAT_R32_FLOAT;
	case GFX_FORMAT_D24_UNORM_S8_UINT:
		return GFX_FORMAT_R24_UNORM_G8_TYPELESS;
	case GFX_FORMAT_D16_UNORM:
		return GFX_FORMAT_R16_UNORM;
	default:
		break;
	}
	return _format;
}


///-------------------------------------------------------------------------------------------------
/// イメージフォーマットからデプス・ステンシルのビット数取得
void		CGfxFormatUtility::GetDepthStencilFormat(u32& _depthBit, u32& _stencilBit, GFX_IMAGE_FORMAT _format)
{
	_depthBit = 0;
	_stencilBit = 0;
	switch (_format)
	{
	case GFX_FORMAT_D32_FLOAT_S8X24_UINT:
		_depthBit = 32;
		_stencilBit = 0;
		break;
	case GFX_FORMAT_D32_FLOAT:
		_depthBit = 32;
		_stencilBit = 0;
		break;
	case GFX_FORMAT_D24_UNORM_S8_UINT:
		_depthBit = 24;
		_stencilBit = 8;
		break;
	case GFX_FORMAT_D16_UNORM:
		_depthBit = 16;
		_stencilBit = 0;
		break;
	default:
		break;
	}
}


///-------------------------------------------------------------------------------------------------
/// イメージフォーマットがデプス・ステンシル対応かチェック
void		CGfxFormatUtility::CheckDepthStencilFormat(b8& _isDepth, b8& _isStencil, GFX_IMAGE_FORMAT _format)
{
	_isDepth = false;
	_isStencil = false;
	switch (_format)
	{
	case GFX_FORMAT_D32_FLOAT_S8X24_UINT:
		_isDepth = true;
		_isStencil = true;
		break;
	case GFX_FORMAT_D32_FLOAT:
		_isDepth = true;
		_isStencil = false;
		break;
	case GFX_FORMAT_D24_UNORM_S8_UINT:
		_isDepth = true;
		_isStencil = true;
		break;
	case GFX_FORMAT_D16_UNORM:
		_isDepth = true;
		_isStencil = false;
		break;
	default:
		break;
	}
}

//-------------------------------------------------------------------------------------------------
// AAフォーマット変換
u32		CGfxFormatUtility::ConvertAA(GFX_ANTI_ALIASE _AA)
{
	static const u32 s_format[] =
	{
		0,
		(1 << 0),
		(1 << 1),
		(1 << 2),
		(1 << 3),
	};
	StaticAssert(ARRAYOF(s_format) == GFX_AA_TYPE_NUM);
	return s_format[_AA];
}


template<typename _VAL>
GFX_VALUE	CGfxFormatUtility::GetValueFormat(_VAL& _val)
{
	return GFX_VALUE_UNKNOWN;
}

template< >
GFX_VALUE	CGfxFormatUtility::GetValueFormat(s8& _val)
{
	return GFX_VALUE_S8;
}

template< >
GFX_VALUE	CGfxFormatUtility::GetValueFormat(u8& _val)
{
	return GFX_VALUE_U8;
}

template< >
GFX_VALUE	CGfxFormatUtility::GetValueFormat(s16& _val)
{
	return GFX_VALUE_S16;
}

template< >
GFX_VALUE	CGfxFormatUtility::GetValueFormat(u16& _val)
{
	return GFX_VALUE_U16;
}

template< >
GFX_VALUE	CGfxFormatUtility::GetValueFormat(s32& _val)
{
	return GFX_VALUE_S32;
}

template< >
GFX_VALUE	CGfxFormatUtility::GetValueFormat(u32& _val)
{
	return GFX_VALUE_U32;
}

template< >
GFX_VALUE	CGfxFormatUtility::GetValueFormat(f32& _val)
{
	return GFX_VALUE_F32;
}

template< >
GFX_VALUE	CGfxFormatUtility::GetValueFormat(s64& _val)
{
	return GFX_VALUE_S64;
}

template< >
GFX_VALUE	CGfxFormatUtility::GetValueFormat(u64& _val)
{
	return GFX_VALUE_U64;
}

template< >
GFX_VALUE	CGfxFormatUtility::GetValueFormat(f64& _val)
{
	return GFX_VALUE_F64;
}


POISON_END

﻿
///-------------------------------------------------------------------------------------------------
/// define

// 行列フラグ
#define MTX_BIT_FLAG( __type )		SCast<u32>(1u << __type)

///-------------------------------------------------------------------------------------------------
/// include
#include "renderer/renderer.h"

#include "shader/shader.h"
#include "shader/shader_utility.h"

#include "object/vertex_buffer.h"
#include "object/index_buffer.h"
#include "object/uniform_buffer.h"
#include "object/texture_buffer.h"
#include "object/sampler_state.h"

#include "object/color_buffer.h"
#include "object/depth_buffer.h"
#include "renderer/render_target.h"

#include "cache_builder/vertex_cache_builder.h"
#include "cache_builder/index_cache_builder.h"
#include "cache_builder/uniform_cache_builder.h"
#include "cache_builder/rasterizer_cache_builder.h"
#include "cache_builder/blend_state_cache_builder.h"
#include "cache_builder/depth_state_cache_builder.h"
#include "cache_builder/sampler_cache_builder.h"

#include "utility/gfx_utility.h"


POISON_BGN

CRenderer::CRenderer()
{
}

CRenderer::~CRenderer()
{
}

///-------------------------------------------------------------------------------------------------
///	初期化
b8		CRenderer::Initialize(IAllocator* _pAllocator, const c8* _pName)
{
	m_pAllocator = _pAllocator;

	// リソースリセット
	ResetResource();

	// カレントインデックス初期化
	m_currentSamplerIdx = 0;

	SetName(_pName);
	// イベント生成
	{
		c8 buf[256];
		sprintf_safe(buf, "%sBgn", GetName());
		CThreadUtility::CreateEvent(m_eventBgnHdl, buf);
	}
	{
		c8 buf[256];
		sprintf_safe(buf, "%sEnd", GetName());
		CThreadUtility::CreateEvent(m_eventEndHdl, buf);
	}

	return true;
}

///-------------------------------------------------------------------------------------------------
///	終了
void	CRenderer::Finalize()
{
	// スレッド終了
	m_isDrawing = false;
	CThreadUtility::SignalEvent(m_eventBgnHdl);
	m_thread.Delete();

	// イベント削除
	CThreadUtility::DeleteEvent(m_eventBgnHdl);
	CThreadUtility::DeleteEvent(m_eventEndHdl);
}

///-------------------------------------------------------------------------------------------------
///	描画開始
void	CRenderer::DrawBegin()
{
	// アロケータ更新
	m_pAllocator->Update();

	// リセットリソース
	ResetResource();


	// デフォルトステート設定
	{
		// ポストレンダーターゲット設定
		CRenderTarget* pPostRT = &CGfxUtility::GetRenderTarget(0);
		SetRenderTarget(pPostRT, 0xFF, 0);
		// ラスタライザー設定
		SetRasterizer(&CGfxUtility::GetDefaultRasterizer());
		// ブレンドステート設定
		SetBlendState(&CGfxUtility::GetDefaultBlendState());
		// デプスステンシルステート設定
		SetDepthState(&CGfxUtility::GetDefaultDepthState());
		// サンプラー設定
		for (u32 shaderIdx = 0; shaderIdx < GFX_SHADER_GRAPHICS_TYPE_NUM; shaderIdx++)
		{
			GFX_SHADER shaderType = SCast<GFX_SHADER>(shaderIdx);
			for (u32 sampSlot = 0; sampSlot < SAMPLER_SLOT_NUM; sampSlot++)
			{
				SAMPLER_SLOT slot = SCast<SAMPLER_SLOT>(sampSlot);
				SetSampler(shaderType, slot, &CGfxUtility::GetDefaultSampler());
			}
		}
	}


#ifndef POISON_RELEASE
	{
		if (GetPrimitiveQuery())
		{
			// 描画プリミティブ計測開始
			BeginQuery(GetPrimitiveQuery());
		}

		if (GetTimerQuery())
		{
			// タイマー計測開始
			BeginQuery(GetTimerQuery());
		}
	}
#endif // !POISON_RELEASE

}

///-------------------------------------------------------------------------------------------------
/// 描画終了
void	CRenderer::DrawEnd()
{
#ifndef POISON_RELEASE
	{
		if (GetTimerQuery())
		{
			// タイマー計測終了
			EndQuery(GetTimerQuery());
		}

		if (GetPrimitiveQuery())
		{
			// 描画プリミティブ計測終了
			EndQuery(GetPrimitiveQuery());
		}
	}
#endif // !POISON_RELEASE


	m_isDrawing = false;
}

///-------------------------------------------------------------------------------------------------
///	描画
void	CRenderer::Draw()
{
	libAssert(m_pDrawObjBgn);

	DRAW_OBJ* pObj = m_pDrawObjBgn;
	while (pObj)
	{
		pObj->renderFunc(this, pObj->p0, pObj->p1);

		// 終了位置に来たら抜ける
		if (pObj == m_pDrawObjEnd) { break; }

		pObj = pObj->GetNext();
	}

	// リストを初期化
	m_pDrawObjBgn = m_pDrawObjEnd = NULL;
}

///-------------------------------------------------------------------------------------------------
/// スレッド描画関数
THREAD_RESULT	CRenderer::ThreadDraw(void* _pRenderer)
{
	CRenderer* pRenderer = SCast<CRenderer*>(_pRenderer);
	while (CGfxUtility::IsThreadDraw())
	{
		// イベント待ち
		CThreadUtility::WaitEvent(pRenderer->m_eventBgnHdl);
		if (!pRenderer->m_isDrawing) { break; }
		// 描画
		pRenderer->DrawBegin();
		pRenderer->Draw();
		pRenderer->DrawEnd();

		// 終了通知
		CThreadUtility::SignalEvent(pRenderer->m_eventEndHdl);
	}
	return THREAD_FUNC_RESULT(0);
}


//-------------------------------------------------------------------------------------------------
/// 描画実行
void	CRenderer::ExcecuteRenderer(DRAW_OBJ* _pDrawBegin, DRAW_OBJ* _pDrawEnd, b8 _thread/* = true*/)
{
	libAssert(m_pDrawObjBgn == NULL && m_pDrawObjEnd == NULL);
	if (_pDrawBegin == NULL || _pDrawEnd == NULL) { return; }

	// ポインタ保存
	m_pDrawObjBgn = _pDrawBegin;
	m_pDrawObjEnd = _pDrawEnd;

	if (_thread)
	{
		m_isDrawing = true;

		// スレッド生成
		if (m_thread.IsDelete())
		{
			// スレッド生成
			if (!m_thread.Create(ThreadDraw, this, 0, GetThreadPriority(), GetName()))
			{
				PRINT_ERROR("*****[ERROR] Faild Create Renderer Thread\n");
				// 直接呼ぶ
				DrawBegin();
				Draw();
				DrawEnd();
			}
			else
			{
				if (GetThreadAffinityMask())
				{
					// スレッドのアフィニティ設定
					CThreadUtility::SetAffinityMask(m_thread.GetHandle(), GetThreadAffinityMask());
				}

				m_isDrawingThread = true;
				RunRenderEvent();
			}
		}
		// もうスレッド動作中
		else
		{
			m_isDrawingThread = true;
			RunRenderEvent();
		}
	}
	else
	{
		// 直接描画
		DrawBegin();
		Draw();
		DrawEnd();
	}
}

///-------------------------------------------------------------------------------------------------
/// 描画時ベント実行
void	CRenderer::RunRenderEvent()
{
	Assert(m_isDrawingThread);

	// すでに実行中なら何もしない
	if (m_isRunRenderEvent) { return; }
	m_isRunRenderEvent = true;
	// 開始通知
	CThreadUtility::SignalEvent(m_eventBgnHdl);
}

///-------------------------------------------------------------------------------------------------
/// 描画コマンド
b8		CRenderer::WaitDrawCommand(b8 _noWait)
{
	// 描画中
	if (m_isDrawing)
	{
		if (_noWait) { return false; }	// 待たない場合は抜ける
	}

	// スレッド待ち処理
	if (m_isDrawingThread)
	{
		RunRenderEvent();

		// スレッド終了待ち
		CThreadUtility::WaitEvent(m_eventEndHdl);
		m_isDrawingThread = false;
		m_isRunRenderEvent = false;
	}

	// コマンドキューフラッシュ
	FlushCommandQueue();
	return true;
}


///-------------------------------------------------------------------------------------------------
/// 頂点キャッシュビルド
const CVertexBuffer*	CRenderer::CreateAndCacheVertex(u32 _elemSize, u32 _elemNum, const void* _data/* = NULL*/)
{
	libAssert(m_pVertexCacheBuilder);
	return m_pVertexCacheBuilder->Create(_elemSize, _elemNum, _data, GetContext());
}

///-------------------------------------------------------------------------------------------------
/// インデックスキャッシュビルド
const CIndexBuffer*		CRenderer::CreateAndCacheIndex(u32 _elemSize, u32 _elemNum, const void* _data/* = NULL*/)
{
	libAssert(m_pIndexCacheBuilder);
	return m_pIndexCacheBuilder->Create(_elemSize, _elemNum, _data, GetContext());
}

///-------------------------------------------------------------------------------------------------
/// ユニフォームキャッシュビルド
const CUniformBuffer*	CRenderer::CreateAndCacheUniform(u32 _elemSize, u32 _elemNum, const void* _data/* = NULL*/)
{
	libAssert(m_pUniformCacheBuilder);
	return m_pUniformCacheBuilder->Create(_elemSize, _elemNum, _data, GetContext());
}

///-------------------------------------------------------------------------------------------------
/// ラスタライザーキャッシュビルド
const CRasterizer*		CRenderer::CreateAndCacheRasterizer(const RASTERIZER_DESC& _desc)
{
	libAssert(m_pRasterizerCacheBuilder);
	return m_pRasterizerCacheBuilder->Create(_desc);
}

///-------------------------------------------------------------------------------------------------
/// ブレンドステートキャッシュビルド
const CBlendState*		CRenderer::CreateAndCacheBlendState(const BLEND_STATE_DESC& _desc)
{
	libAssert(m_pBlendStateCacheBuilder);
	return m_pBlendStateCacheBuilder->Create(_desc);
}

///-------------------------------------------------------------------------------------------------
/// デプスステンシルステートキャッシュビルド
const CDepthState*		CRenderer::CreateAndCacheDepthState(const DEPTH_STATE_DESC& _desc)
{
	libAssert(m_pDepthStateCacheBuilder);
	return m_pDepthStateCacheBuilder->Create(_desc);
}

///-------------------------------------------------------------------------------------------------
/// サンプラーキャッシュビルド
const CSampler*		CRenderer::CreateAndCacheSampler(const SAMPLER_DESC& _desc)
{
	libAssert(m_pSamplerCacheBuilder);
	return m_pSamplerCacheBuilder->Create(_desc, GetContext());
}


///-------------------------------------------------------------------------------------------------
/// レンダーターゲット設定
void	CRenderer::SetRenderTarget(const CRenderTarget* _pRenderTarget, COLOR_BIT _clrBit, u32 _depIdx)
{
	const CRenderTarget* pCacheRT = m_pCacheRenderTarget[m_currentRenderTargetIdx];
	// 同レンダーターゲットの場合差分だけ更新する
	if (pCacheRT == _pRenderTarget)
	{
		u32 cacheSlot = 0;
		u8 cacheIdx[RENDER_BUFFER_SLOT_NUM];
		u32 setSlot = 0;
		u8 setIdx[RENDER_BUFFER_SLOT_NUM];
		memset(cacheIdx, INVALID_RT_IDX, sizeof(cacheIdx));
		memset(setIdx, INVALID_RT_IDX, sizeof(setIdx));
		COLOR_BIT clrOldBit = m_pCacheColorBufferIdxBit[m_currentRenderTargetIdx];
		COLOR_BIT clrNewBit = _clrBit;
		for (u32 idx = 0; idx < CRenderTarget::MAX_RT_NUM; idx++)
		{
			if (clrOldBit == 0 && clrNewBit == 0) { break; }
			if (clrOldBit & 0x01)
			{
				cacheIdx[cacheSlot] = idx;
				cacheSlot++;
				libAssert(cacheSlot <= RENDER_BUFFER_SLOT_NUM);
			}
			if (clrNewBit & 0x01)
			{
				setIdx[setSlot] = idx;
				setSlot++;
				libAssert(setSlot <= RENDER_BUFFER_SLOT_NUM);
			}
			clrOldBit >>= 1;
			clrNewBit >>= 1;
		}
		for (u32 slotIdx = 0; slotIdx < RENDER_BUFFER_SLOT_NUM; slotIdx++)
		{
			if (cacheIdx[slotIdx] != setIdx[slotIdx])
			{
				RENDER_BUFFER_BIT slot = slotIdx;
				m_updateRenderTargetBit |= 1u << slot;
			}
		}

		if (m_pCacheDepthBufferIdx[m_currentRenderTargetIdx] != _depIdx)
		{
			m_updateRenderTargetBit |= RENDER_BUFFER_BIT_DEPTH_STENCIL;
		}
	}
	// 別レンダーターゲットの場合全て更新
	else
	{
		m_updateRenderTargetBit = 0;
		u32 setSlot = 0;
		COLOR_BIT clrNewBit = _clrBit;
		for (u32 idx = 0; idx < CRenderTarget::MAX_RT_NUM; idx++)
		{
			if (clrNewBit == 0) { break; }
			if (clrNewBit & 0x01)
			{
				m_updateRenderTargetBit |= 1u << setSlot;
				setSlot++;
				libAssert(setSlot <= RENDER_BUFFER_SLOT_NUM);
			}
			clrNewBit >>= 1;
		}
		if (_depIdx != INVALID_RT_IDX)
		{
			m_updateRenderTargetBit |= RENDER_BUFFER_BIT_DEPTH_STENCIL;
		}
	}

	m_pCacheColorBufferIdxBit[m_currentRenderTargetIdx] = _clrBit;
	m_pCacheDepthBufferIdx[m_currentRenderTargetIdx] = _depIdx;
	m_pCacheRenderTarget[m_currentRenderTargetIdx] = _pRenderTarget;

	// ビューポート・シザリング更新
	ResetViewport();
	ResetScissorRect();
}

///-------------------------------------------------------------------------------------------------
/// レンダーターゲットプッシュ・ポップ
void	CRenderer::PushRenderTarget()
{
	libAssert(m_currentRenderTargetIdx < STACK_NUM - 1);
	// 前回スタックをコピー
	u32& currentIdx = m_currentRenderTargetIdx;
	m_pCacheRenderTarget[currentIdx + 1] = m_pCacheRenderTarget[currentIdx];
	m_pCacheColorBufferIdxBit[currentIdx + 1] = m_pCacheColorBufferIdxBit[currentIdx];
	m_pCacheDepthBufferIdx[currentIdx + 1] = m_pCacheDepthBufferIdx[currentIdx];
	++currentIdx;
}
void	CRenderer::PopRenderTarget()
{
	libAssert(m_currentRenderTargetIdx > 0);
	// 前回スタックを設定
	u32 currentIdx = m_currentRenderTargetIdx - 1;
	SetRenderTarget(m_pCacheRenderTarget[currentIdx], m_pCacheColorBufferIdxBit[currentIdx], m_pCacheDepthBufferIdx[currentIdx]);
	--m_currentRenderTargetIdx;
}

///-------------------------------------------------------------------------------------------------
/// ビューポート設定
void	CRenderer::SetViewport(const CRect& _rect)
{
	if (_rect != m_cacheViewport)
	{
		m_updateRanderStateBit |= UPDATE_VIEWPORT;
	}
	m_cacheViewport = _rect;
}

///-------------------------------------------------------------------------------------------------
/// シザリング設定
void	CRenderer::SetScissorRect(const CRect& _rect)
{
	if (_rect != m_cacheScissor)
	{
		m_updateRanderStateBit |= UPDATE_SCISSOR;
	}
	m_cacheScissor = _rect;
}

///-------------------------------------------------------------------------------------------------
/// レンダーターゲットにサイズにビューポートを設定
void	CRenderer::ResetViewport()
{
	const CRenderTarget* pCacheRT = m_pCacheRenderTarget[m_currentRenderTargetIdx];
	if (pCacheRT)
	{
		SetViewport(CRect(0, 0, pCacheRT->GetWidth(), pCacheRT->GetHeight()));
	}
}

///-------------------------------------------------------------------------------------------------
/// レンダーターゲットにサイズにシザリングを設定
void	CRenderer::ResetScissorRect()
{
	const CRenderTarget* pCacheRT = m_pCacheRenderTarget[m_currentRenderTargetIdx];
	if (pCacheRT)
	{
		SetScissorRect(CRect(0, 0, pCacheRT->GetWidth(), pCacheRT->GetHeight()));
	}
}


///-------------------------------------------------------------------------------------------------
/// シェーダ設定
void	CRenderer::SetShader(const PassShader* _pPassShader)
{
	if (m_pCachePassShader != _pPassShader)
	{
		m_pCachePassShader = _pPassShader;
		m_updateRanderStateBit |= UPDATE_SHADER;
	}
}
void	CRenderer::SetShader(const FxShader* _pFxShader, b8 _isError/* = true*/)
{
	if (m_pCacheFxShader != _pFxShader)
	{
		m_pCacheFxShader = _pFxShader;
		// テクニックリセット
		m_pCacheTecShader = NULL;
		m_tecNameCrc = hash32::Invalid;
		// 属性リセット
		m_attrCurrentIdx = 0;
		m_pCachePassShader = NULL;
		m_isError = _isError;
		m_updateRanderStateBit |= UPDATE_SHADER;
	}
}

///-------------------------------------------------------------------------------------------------
/// テクニックシェーダ設定
void	CRenderer::SetTechnic(hash32 _tecNameCrc)
{
	if (m_tecNameCrc != _tecNameCrc)
	{
		m_tecNameCrc = _tecNameCrc;
		m_pCacheTecShader = NULL;
		// 属性リセット
		m_attrCurrentIdx = 0;
		m_pCachePassShader = NULL;
		m_updateRanderStateBit |= UPDATE_SHADER;
	}
}

///-------------------------------------------------------------------------------------------------
/// 属性シェーダ設定
void	CRenderer::SetAttribute(hash32 _attrNameCrc)
{
	libAssert(m_attrCurrentIdx <= ARRAYOF(m_attrNameCrcList));
	m_attrNameCrcList[m_attrCurrentIdx] = _attrNameCrc;
	++m_attrCurrentIdx;
	m_pCachePassShader = NULL;
	m_updateRanderStateBit |= UPDATE_SHADER;
}


///-------------------------------------------------------------------------------------------------
/// 頂点設定
void	CRenderer::SetVertex(const CVertexBuffer* _pVtxBuf, VERTEX_ATTRIBUTE _slot/*= VTX_ATTR_INTERLEAVED*/)
{
	libAssert(_slot < VERTEX_ATTRIBUTE_TYPE_NUM);
	if (m_pCacheVertex[_slot] != _pVtxBuf)
	{
		m_pCacheVertex[_slot] = _pVtxBuf;
		m_updateVertexBit |= (1u << _slot);
	}
}

///-------------------------------------------------------------------------------------------------
/// インデックス設定
void	CRenderer::SetIndex(const CIndexBuffer* _pIdxBuf)
{
	if (m_pCacheIndex != _pIdxBuf)
	{
		m_pCacheIndex = _pIdxBuf;
		m_updateRanderStateBit |= UPDATE_INDEX;
	}
}

///-------------------------------------------------------------------------------------------------
/// ユニフォーム設定
void	CRenderer::SetUniform(GFX_SHADER _shader, UNIFORM_SLOT _slot, const CUniformBuffer* _pUniBuf)
{
	libAssert(_slot < UNIFORM_SLOT_NUM);
	auto pSrcBuf = _pUniBuf ? &_pUniBuf->GetObject() : NULL;
	auto pDstBuf = m_cacheUniform[_shader][_slot].pBuffer;
	if (pSrcBuf != pDstBuf)
	{
		m_cacheUniform[_shader][_slot].pBuffer = pSrcBuf;
		m_cacheUniform[_shader][_slot].elemSize = _pUniBuf->GetElemSize();
		m_cacheUniform[_shader][_slot].elemNum = _pUniBuf->GetElemNum();
		m_cacheTexAndRoBuf[_shader][_slot].type = GFX_TEXTURE_UNKNOWN;
		m_updateUniformBit[_shader] |= (1u << _slot);
	}
}

///-------------------------------------------------------------------------------------------------
/// テクスチャ設定
void	CRenderer::SetTexture(GFX_SHADER _shader, TEXTURE_SLOT _slot, const CTextureBuffer* _pTexBuf)
{
	libAssert(_slot < TEXTURE_SLOT_NUM);
	auto pSrcBuf = _pTexBuf ? &_pTexBuf->GetObject() : NULL;
	auto pDstBuf = m_cacheTexAndRoBuf[_shader][_slot].pBuffer;
	if (pSrcBuf != pDstBuf)
	{
		m_cacheTexAndRoBuf[_shader][_slot].pBuffer = pSrcBuf;
		m_cacheTexAndRoBuf[_shader][_slot].dataFormat = _pTexBuf->GetFormat();
		m_cacheTexAndRoBuf[_shader][_slot].type = _pTexBuf->GetTexType();
	}
	// レンダーターゲットでテクスチャをバインドしてくるため更新
	m_updateTextureBit[_shader] |= (1u << _slot);
}


///-------------------------------------------------------------------------------------------------
/// サンプラー設定
void	CRenderer::SetSampler(GFX_SHADER _shader, SAMPLER_SLOT _slot, const CSampler* _pSampler)
{
	libAssert(_slot < SAMPLER_SLOT_NUM);
	if (m_pCacheSampler[m_currentSamplerIdx][_shader][_slot] != _pSampler)
	{
		m_pCacheSampler[m_currentSamplerIdx][_shader][_slot] = _pSampler;
		m_updateSamplerBit[_shader] |= (1u << _slot);
	}
}

///-------------------------------------------------------------------------------------------------
/// サンプラープッシュ・ポップ
void	CRenderer::PushSampler()
{
	libAssert(m_currentSamplerIdx < STACK_NUM - 1);
	// 前回スタックをコピー
	u32& currentIdx = m_currentSamplerIdx;
	memcpy(m_pCacheSampler[currentIdx + 1], m_pCacheSampler[currentIdx], sizeof(CSampler*) * GFX_SHADER_GRAPHICS_TYPE_NUM * SAMPLER_SLOT_NUM);
	++currentIdx;
}
void	CRenderer::PopSampler()
{
	libAssert(m_currentSamplerIdx > 0);
	// 前回スタックを設定
	u32 currentIdx = m_currentSamplerIdx - 1;
	for (u32 shaderIdx = 0; shaderIdx < GFX_SHADER_GRAPHICS_TYPE_NUM; shaderIdx++)
	{
		for (u32 slot = 0; slot < SAMPLER_SLOT_NUM; slot++)
		{
			GFX_SHADER shadertype = SCast<GFX_SHADER>(shaderIdx);
			SAMPLER_SLOT sampSlot = SCast<SAMPLER_SLOT>(slot);
			SetSampler(shadertype, sampSlot, m_pCacheSampler[currentIdx][shaderIdx][slot]);
		}
	}
	--m_currentSamplerIdx;
}


///-------------------------------------------------------------------------------------------------
/// ラスタライザー設定
void	CRenderer::SetRasterizer(const CRasterizer* _pRasterizer)
{
	if (m_pCacheRasterizer[m_currentRasterizerIdx] != _pRasterizer)
	{
		m_pCacheRasterizer[m_currentRasterizerIdx] = _pRasterizer;
		m_updateRanderStateBit |= UPDATE_RASTERRIZER;
	}
}

///-------------------------------------------------------------------------------------------------
/// ラスタライザープッシュ・ポップ
void	CRenderer::PushRasterizer()
{
	libAssert(m_currentRasterizerIdx < STACK_NUM - 1);
	// 前回スタックをコピー
	u32& currentIdx = m_currentRasterizerIdx;
	m_pCacheRasterizer[currentIdx + 1] = m_pCacheRasterizer[currentIdx];
	++currentIdx;
}
void	CRenderer::PopRasterizer()
{
	libAssert(m_currentRasterizerIdx > 0);
	// 前回スタックを設定
	u32 currentIdx = m_currentRasterizerIdx - 1;
	SetRasterizer(m_pCacheRasterizer[currentIdx]);
	--m_currentRasterizerIdx;
}


///-------------------------------------------------------------------------------------------------
/// ブレンドステート設定
void	CRenderer::SetBlendState(const CBlendState* _pBlendState)
{
	if (m_pCacheBlendState[m_currentBlendStateIdx] != _pBlendState)
	{
		m_pCacheBlendState[m_currentBlendStateIdx] = _pBlendState;
		m_updateRanderStateBit |= UPDATE_BLEND_STATE;
	}
}

///-------------------------------------------------------------------------------------------------
/// ブレンドステートプッシュ・ポップ
void	CRenderer::PushBlendState()
{
	libAssert(m_currentBlendStateIdx < STACK_NUM - 1);
	// 前回スタックをコピー
	u32& currentIdx = m_currentBlendStateIdx;
	m_pCacheBlendState[currentIdx + 1] = m_pCacheBlendState[currentIdx];
	++currentIdx;
}
void	CRenderer::PopBlendState()
{
	libAssert(m_currentBlendStateIdx > 0);
	// 前回スタックを設定
	u32 currentIdx = m_currentBlendStateIdx - 1;
	SetBlendState(m_pCacheBlendState[currentIdx]);
	--m_currentBlendStateIdx;
}


///-------------------------------------------------------------------------------------------------
/// デプスステンシルステート設定
void	CRenderer::SetDepthState(const CDepthState* _pDepthState)
{
	if (m_pCacheDepthState[m_currentDepthStateIdx] != _pDepthState)
	{
		m_pCacheDepthState[m_currentDepthStateIdx] = _pDepthState;
		m_updateRanderStateBit |= UPDATE_DEPTH_STATE;
	}
}

///-------------------------------------------------------------------------------------------------
/// デプスステンシルステートプッシュ・ポップ
void	CRenderer::PushDepthState()
{
	libAssert(m_currentDepthStateIdx < STACK_NUM - 1);
	// 前回スタックをコピー
	u32& currentIdx = m_currentDepthStateIdx;
	m_pCacheDepthState[currentIdx + 1] = m_pCacheDepthState[currentIdx];
	++currentIdx;
}
void	CRenderer::PopDepthState()
{
	libAssert(m_currentDepthStateIdx > 0);
	// 前回スタックを設定
	u32 currentIdx = m_currentDepthStateIdx - 1;
	SetDepthState(m_pCacheDepthState[currentIdx]);
	--m_currentDepthStateIdx;
}


///-------------------------------------------------------------------------------------------------
/// テクスチャとレンダーターゲットが被ってたらクリア
b8		CRenderer::ClearTextureRenderTarget(const void* _pRtTex)
{
	b8 bRet = false;
	for (u32 shaderIdx = 0; shaderIdx < GFX_SHADER_GRAPHICS_TYPE_NUM; shaderIdx++)
	{
		for (u32 slotIdx = 0; slotIdx < TEXTURE_SLOT_NUM; slotIdx++)
		{
			RENDERER_BUFFER_DATA& data = m_cacheTexAndRoBuf[shaderIdx][slotIdx];
			if (data.pBuffer == _pRtTex)
			{
				data = RENDERER_BUFFER_DATA();
				m_updateTextureBit[shaderIdx] &= ~(1u << slotIdx);	// 更新フラグおろし
				bRet = true;
			}
		}
	}
	return bRet;
}


///-------------------------------------------------------------------------------------------------
/// 計算されたマトリクスの計算フラグ更新
u32		__CalcMatrixFlag(MATRIX_TYPE _type)
{
	// 更新が必要になるタイプを指定
	static const u32 s_calcMtxMaskFlag[] =
	{
		(MTX_BIT_FLAG(MATRIX_WORLD_PROJ) | MTX_BIT_FLAG(MATRIX_LOCAL_PROJ) | MTX_BIT_FLAG(MATRIX_PROJ_VIEW) | MTX_BIT_FLAG(MATRIX_PROJ_WORLD)),	// MATRIX_VIEW_PROJ
		(MTX_BIT_FLAG(MATRIX_WORLD_PROJ) | MTX_BIT_FLAG(MATRIX_LOCAL_VIEW) | MTX_BIT_FLAG(MATRIX_LOCAL_PROJ) | MTX_BIT_FLAG(MATRIX_VIEW_WORLD) | MTX_BIT_FLAG(MATRIX_PROJ_WORLD) | MTX_BIT_FLAG(MATRIX_VIEW_BILLBOARD)),	// MATRIX_WORLD_VIEW
		(MTX_BIT_FLAG(MATRIX_LOCAL_VIEW) | MTX_BIT_FLAG(MATRIX_LOCAL_PROJ) | MTX_BIT_FLAG(MATRIX_WORLD_LOCAL)),	// MATRIX_LOCAL_WORLD
		(MTX_BIT_FLAG(MATRIX_OLD_WORLD_PROJ) | MTX_BIT_FLAG(MATRIX_OLD_LOCAL_PROJ)),	// MATRIX_OLD_WORLD_PROJ
		(MTX_BIT_FLAG(MATRIX_OLD_WORLD_PROJ) | MTX_BIT_FLAG(MATRIX_OLD_LOCAL_VIEW)),	// MATRIX_OLD_LOCAL_VIEW
		(MTX_BIT_FLAG(MATRIX_OLD_LOCAL_VIEW) | MTX_BIT_FLAG(MATRIX_OLD_LOCAL_PROJ)),	// MATRIX_OLD_LOCAL_PROJ
	};
	StaticAssert(ARRAYOF(s_calcMtxMaskFlag) == MATRIX_BASE_NUM);
	libAssert(_type < MATRIX_BASE_NUM);
	return s_calcMtxMaskFlag[_type];
}

///-------------------------------------------------------------------------------------------------
/// タイプに合わせたマトリクス設定
void	CRenderer::LoadMatrix(const MATRIX_TYPE _type, const CMtx44& _mtx)
{
	libAssert(_type < MATRIX_BASE_NUM);

	// 同一のものが設定されてあればスキップ
	if (m_drawMtx[m_currentDrawMtxIdx][_type] == _mtx)
	{
		return;
	}

	m_drawMtx[m_currentDrawMtxIdx][_type] = _mtx;
	m_updateDrawMtxBit[m_currentDrawMtxIdx] |= MTX_BIT_FLAG(_type);
	m_updateDrawMtxBit[m_currentDrawMtxIdx] |= __CalcMatrixFlag(_type);

	m_updateRanderStateBit |= (_type < MATRIX_OLD_VIEW_PROJ ? UPDATE_MATRIX : UPDATE_OLD_MATRIX);
}

///-------------------------------------------------------------------------------------------------
/// タイプに合わせた現在のマトリクスに掛け算
void	CRenderer::MultMatrix(const MATRIX_TYPE _type, const CMtx44& _mtx)
{
	libAssert(_type < MATRIX_BASE_NUM);
	m_drawMtx[m_currentDrawMtxIdx][_type] = _mtx * m_drawMtx[m_currentDrawMtxIdx][_type];
	m_updateDrawMtxBit[m_currentDrawMtxIdx] |= MTX_BIT_FLAG(_type);
	m_updateDrawMtxBit[m_currentDrawMtxIdx] |= __CalcMatrixFlag(_type);

	m_updateRanderStateBit |= (_type < MATRIX_OLD_VIEW_PROJ ? UPDATE_MATRIX : UPDATE_OLD_MATRIX);
}

///-------------------------------------------------------------------------------------------------
/// 計算されたマトリクス取得
const CMtx44&	CRenderer::GetMatrix(MATRIX_TYPE _type)
{
	libAssert(_type < MATRIX_TYPE_NUM);
	// マトリクスの計算する必要がある
	CMtx44& mtx = m_drawMtx[m_currentDrawMtxIdx][_type];
	u32& updateBit = m_updateDrawMtxBit[m_currentDrawMtxIdx];
	if (updateBit & MTX_BIT_FLAG(_type))
	{
		// 計算マトリクス取得
		CMtx44* pMtx = m_drawMtx[m_currentDrawMtxIdx];
		// まだ計算されてない
		switch (_type)
		{
		case MATRIX_WORLD_PROJ:	// ワールド⇒プロジェクション
		{
			mtx = pMtx[MATRIX_VIEW_PROJ] * pMtx[MATRIX_WORLD_VIEW];
		}
		break;
		case MATRIX_LOCAL_VIEW:	// ローカル⇒ビュー
		{
			mtx = pMtx[MATRIX_WORLD_VIEW] * pMtx[MATRIX_LOCAL_WORLD];
		}
		break;
		case MATRIX_LOCAL_PROJ:	// ローカル⇒プロジェクション
		{
			mtx = pMtx[MATRIX_VIEW_PROJ] * pMtx[MATRIX_WORLD_VIEW] * pMtx[MATRIX_LOCAL_WORLD];
		}
		break;

		case MATRIX_OLD_WORLD_PROJ:	//	前回ワールド⇒プロジェクション
		{
			mtx = pMtx[MATRIX_OLD_VIEW_PROJ] * pMtx[MATRIX_OLD_WORLD_VIEW];
		}
		break;
		case MATRIX_OLD_LOCAL_VIEW:	//	前回ローカル⇒のビュー
		{
			mtx = pMtx[MATRIX_OLD_WORLD_VIEW] * pMtx[MATRIX_OLD_LOCAL_WORLD];
		}
		break;
		case MATRIX_OLD_LOCAL_PROJ:	//	前回ローカル⇒のプロジェクション
		{
			mtx = pMtx[MATRIX_OLD_VIEW_PROJ] * pMtx[MATRIX_OLD_WORLD_VIEW] * pMtx[MATRIX_OLD_LOCAL_WORLD];
		}
		break;

		case MATRIX_PROJ_VIEW:		// プロジェクション→⇒ビュー
		{
			Mtx::Inverse(mtx, pMtx[MATRIX_VIEW_PROJ]);	// 逆行列
		}
		break;
		case MATRIX_VIEW_WORLD:		// ビュー⇒ワールド
		{
			Mtx::Inverse(mtx, pMtx[MATRIX_WORLD_VIEW]);	// 逆行列
		}
		break;
		case MATRIX_PROJ_WORLD:		// プロジェクション⇒ワールド
		{
			const CMtx44& wpMtx = GetMatrix(MATRIX_WORLD_PROJ);
			Mtx::Inverse(mtx, wpMtx);	// 逆行列
		}
		break;
		case MATRIX_WORLD_LOCAL:	// ワールド⇒ローカル
		{
			Mtx::Inverse(mtx, pMtx[MATRIX_LOCAL_WORLD]);	// 逆行列
		}
		break;

		case MATRIX_VIEW_BILLBOARD:	// ビュービルボード
		{
			// viewの回転部分だけ転置(スケールがかかってたらダメだけど…)
			CMtx33 tmpMtx;
			Mtx::Transpose(tmpMtx, pMtx[MATRIX_WORLD_VIEW].GetMtx33());
			mtx.SetMtx33(tmpMtx);
		}
		break;
		default:
			//libAssert(0);	// 未定義
			break;
		}
		// 更新フラグおろし
		updateBit &= ~MTX_BIT_FLAG(_type);
	}
	return mtx;
}

///-------------------------------------------------------------------------------------------------
/// 行列のプッシュ
void	CRenderer::PushMatrix()
{
	libAssert(m_currentDrawMtxIdx < STACK_NUM);
	if (m_currentDrawMtxIdx >= STACK_NUM) { return; }

	// 現在のマトリクス保存
	memcpy16(m_drawMtx[m_currentDrawMtxIdx + 1], m_drawMtx[m_currentDrawMtxIdx], sizeof(CMtx44) * MATRIX_TYPE_NUM);
	memcpy32(&m_updateDrawMtxBit[m_currentDrawMtxIdx + 1], &m_updateDrawMtxBit[m_currentDrawMtxIdx], sizeof(u32));
	++m_currentDrawMtxIdx;
}

///-------------------------------------------------------------------------------------------------
/// 行列のポップ
void	CRenderer::PopMatrix()
{
	libAssert(m_currentDrawMtxIdx > 0);
	if (m_currentDrawMtxIdx <= 0) { return; }
	--m_currentDrawMtxIdx;
	// 更新があるかチェック
	if (memcmp(&m_updateDrawMtxBit[m_currentDrawMtxIdx + 1], &m_updateDrawMtxBit[m_currentDrawMtxIdx], sizeof(u32) * MATRIX_TYPE_NUM) != 0)
	{
		m_updateRanderStateBit |= (UPDATE_MATRIX | UPDATE_OLD_MATRIX);
	}
}



//-------------------------------------------------------------------------------------------------
//@brief	パスシェーダフラッシュ
const PassShader*	CRenderer::FlushPassShader()
{
	if (!(m_updateRanderStateBit & UPDATE_SHADER)) { return m_pCachePassShader; }

	libAssert(m_pDrawContext);

	const PassShader* pPassShader = NULL;
	// 直指定の方が優先
	if (m_pCachePassShader)
	{
		pPassShader = m_pCachePassShader;
	}
	// コンボシェーダから取得
	else if (m_pCacheFxShader)
	{
		if (m_pCacheTecShader == NULL)
		{
			m_pCacheTecShader = m_pCacheFxShader->GetTecShader(m_tecNameCrc);
		}
		libAssert(m_pCacheTecShader);
		u16 bit = 0;
		for (u32 attrIdx = 0; attrIdx < m_attrCurrentIdx; attrIdx++)
		{
			bit |= m_pCacheTecShader->GetAttrBit(m_attrNameCrcList[attrIdx]);
		}
		pPassShader = m_pCacheTecShader->GetPassShader(bit);

		// 属性リセット
		m_attrCurrentIdx = 0;
	}
	// ないときにエラーシェーダ設定
	if (m_isError && pPassShader == NULL)
	{
		pPassShader = CShaderUtility::GetError();
	}

	m_updateRanderStateBit &= ~UPDATE_SHADER;	// 更新フラグおろし

	return pPassShader;
}

///-------------------------------------------------------------------------------------------------
/// リセットリソース
void	CRenderer::ResetResource()
{
	memset(m_pCacheRenderTarget, 0, sizeof(m_pCacheRenderTarget));
	memset(m_pCacheColorBufferIdxBit, 0, sizeof(m_pCacheColorBufferIdxBit));
	memset(m_pCacheDepthBufferIdx, INVALID_RT_IDX, sizeof(m_pCacheDepthBufferIdx));
	m_currentRenderTargetIdx = 0;
	m_updateRenderTargetBit = 0;

	m_cacheViewport = CRect();
	m_cacheScissor = CRect();

	m_pCacheVShader = NULL;
	m_pCacheGShader = NULL;
	m_pCachePShader = NULL;
	m_pCachePassShader = NULL;
	m_pCacheTecShader = NULL;
	m_pCacheFxShader = NULL;
	m_tecNameCrc = hash32::Invalid;
	memset(m_attrNameCrcList, 0, sizeof(m_attrNameCrcList));
	m_attrCurrentIdx = 0;
	m_isError = false;

	memset(m_pCacheVertex, 0, sizeof(m_pCacheVertex));
	m_updateVertexBit = 0;

	m_pCacheIndex = NULL;
	m_pDrawingPredication = NULL;

	memset(m_cacheUniform, 0, sizeof(m_cacheUniform));
	memset(m_updateUniformBit, 0, sizeof(m_updateUniformBit));

	memset(m_cacheTexAndRoBuf, 0, sizeof(m_cacheTexAndRoBuf));
	memset(m_updateTextureBit, 0, sizeof(m_updateTextureBit));

	memset(m_pCacheSampler, 0, sizeof(m_pCacheSampler));
	memset(m_updateSamplerBit, 0, sizeof(m_updateSamplerBit));
	m_currentSamplerIdx = 0;

	memset(m_pCacheRasterizer, 0, sizeof(m_pCacheRasterizer));
	m_currentRasterizerIdx = 0;

	memset(m_pCacheBlendState, 0, sizeof(m_pCacheBlendState));
	m_currentBlendStateIdx = 0;

	memset(m_pCacheDepthState, 0, sizeof(m_pCacheDepthState));
	m_currentDepthStateIdx = 0;

	for (u8 mtxIdx = 0; mtxIdx < MATRIX_TYPE_NUM; ++mtxIdx)
	{
		Mtx::Identity(m_drawMtx[0][mtxIdx]);
	}
	memset(m_updateDrawMtxBit, 0, sizeof(m_updateDrawMtxBit));
	m_currentDrawMtxIdx = 0;

	m_updateRanderStateBit = 0;


	// キャッシュビルダークリア
	if (m_pVertexCacheBuilder)
	{
		m_pVertexCacheBuilder->Clear();
	}
	if (m_pIndexCacheBuilder)
	{
		m_pIndexCacheBuilder->Clear();
	}
	if (m_pUniformCacheBuilder)
	{
		m_pUniformCacheBuilder->Clear();
	}
	if (m_pRasterizerCacheBuilder)
	{
		m_pRasterizerCacheBuilder->Clear();
	}
	if (m_pBlendStateCacheBuilder)
	{
		m_pBlendStateCacheBuilder->Clear();
	}
	if (m_pDepthStateCacheBuilder)
	{
		m_pDepthStateCacheBuilder->Clear();
	}
	if (m_pSamplerCacheBuilder)
	{
		m_pSamplerCacheBuilder->Clear();
	}
}


///-------------------------------------------------------------------------------------------------
/// 描画フラッシュ開始
void	CRenderer::FlushDrawBegin()
{
	// 行列フラッシュ
	FlushCalcUniformBlockMatrix();

	// レンダーターゲットフラッシュ
	FlushRenderTarget();

	// シェーダフラッシュ
	const PassShader* pPassShader = FlushPassShader();
	FlushShader(pPassShader);

	// 頂点フラッシュ
	FlushVertex();

	// インデックスフラッシュ
	FlushIndex();

	// ユニフォームフラッシュ
	FlushUniform();

	// テクスチャフラッシュ
	FlushTexture();

	// サンプラーステートフラッシュ
	FlushSamplerState();

	// ビューポートフラッシュ
	FlushViewport();

	// シザリングフラッシュ
	FlushScissorRect();

	// ラスタライザーフラッシュ
	FlushRasterrizerState();

	// ブレンドステートフラッシュ
	FlushBrendState();

	// デプスステンシルステートフラッシュ
	FlushDepthStencilState();

	// 事前描画プリディケーションフラッシュ開始
	FlushDrawingPredictionQueryBegin();
}

///-------------------------------------------------------------------------------------------------
/// 描画フラッシュ終了
void	CRenderer::FlushDrawEnd()
{
	// 事前描画プリディケーションフラッシュ終了
	FlushDrawingPredictionQueryEnd();
}

///-------------------------------------------------------------------------------------------------
/// 行列計算と行列ユニフォームブロックのフラッシュ
void	CRenderer::FlushCalcUniformBlockMatrix()
{
	b8 isUpdate = false;

	if (m_updateRanderStateBit & UPDATE_MATRIX)
	{
		static const MATRIX_TYPE updateTypes[] =
		{
			MATRIX_WORLD_PROJ,
			MATRIX_LOCAL_VIEW,
			MATRIX_LOCAL_PROJ,
			MATRIX_PROJ_VIEW,
			MATRIX_VIEW_WORLD,
			MATRIX_PROJ_WORLD,
			//MATRIX_WORLD_LOCAL,
			//MATRIX_VIEW_BILLBOARD,
		};
		for (u8 mtxType = 0; mtxType < ARRAYOF(updateTypes); ++mtxType)
		{
			GetMatrix(updateTypes[mtxType]);
		}
		m_updateRanderStateBit &= ~UPDATE_MATRIX;
		isUpdate |= true;
	}

	if (m_updateRanderStateBit & UPDATE_OLD_MATRIX)
	{
		static const MATRIX_TYPE updateTypes[] =
		{
			MATRIX_OLD_WORLD_PROJ,
			MATRIX_OLD_LOCAL_VIEW,
			MATRIX_OLD_LOCAL_PROJ,
		};
		for (u8 mtxType = 0; mtxType < ARRAYOF(updateTypes); ++mtxType)
		{
			GetMatrix(updateTypes[mtxType]);
		}
		m_updateRanderStateBit &= ~UPDATE_OLD_MATRIX;
		isUpdate |= true;
	}

	if (isUpdate)
	{
		static const MATRIX_TYPE flushTypes[] =
		{
			MATRIX_LOCAL_WORLD,
			MATRIX_LOCAL_VIEW,
			MATRIX_LOCAL_PROJ,
			MATRIX_WORLD_VIEW,
			MATRIX_WORLD_PROJ,
			MATRIX_PROJ_VIEW,

			MATRIX_PROJ_VIEW,
			MATRIX_PROJ_WORLD,
			MATRIX_VIEW_WORLD,

			MATRIX_OLD_LOCAL_WORLD,
			MATRIX_OLD_LOCAL_VIEW,
			MATRIX_OLD_LOCAL_PROJ,
			MATRIX_OLD_WORLD_VIEW,
			MATRIX_OLD_WORLD_PROJ,
			MATRIX_OLD_VIEW_PROJ,
		};
		StaticAssert(ARRAYOF(m_flushMtx) == ARRAYOF(flushTypes));
		for (u8 mtxType = 0; mtxType < ARRAYOF(flushTypes); ++mtxType)
		{
			m_flushMtx[mtxType] = GetMatrix(flushTypes[mtxType]);
		}
		const CUniformBuffer* pMtxUB = CreateAndCacheUniform(sizeof(CMtx44), MATRIX_FLUSH_NUM, m_flushMtx);
		if (pMtxUB)
		{
			SetUniform(GFX_SHADER_VERTEX, SCast<UNIFORM_SLOT>(UNIFORM_MTX_SLOT), pMtxUB);
			SetUniform(GFX_SHADER_GEOMETRY, SCast<UNIFORM_SLOT>(UNIFORM_MTX_SLOT), pMtxUB);
			SetUniform(GFX_SHADER_PIXEL, SCast<UNIFORM_SLOT>(UNIFORM_MTX_SLOT), pMtxUB);
		}
	}
}


POISON_END

﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "utility/gfx_utility.h"

#include "device/gfx_device.h"
#include "renderer/renderer_list.h"
#include "renderer/renderer.h"
#include "renderer/render_target.h"

#include "shader/shader.h"
#include "shader/shader_utility.h"
#include "object/vertex_buffer.h"
#include "object/rasterrizer_state.h"
#include "object/blend_state.h"
#include "object/depth_state.h"
#include "object/sampler_state.h"

// query
//#include "query/occlusion_query.h"
#include "query/primitive_query.h"
#include "query/timer_query.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// constant
static IAllocator*		s_pAllocator = NULL;
static DRAW_LIST		s_drawList;
static DRAW_OBJ*		s_pRegistedDrawObjEnd = NULL;	///< 登録済みの描画オブジェクトの最後

static CRenderer*		s_pRenderer = NULL;
static u32				s_rendererNum = 0;
static u32				s_currentIndex = 0;

static b8				s_threadDraw = true;
//static b8				s_threadDraw = false;
static b8				s_isEnableDraw = false;
static b8				s_isDrawing = false;

static CRenderTarget*	s_pRenderTarget = NULL;
static u32				s_renderTargetNum = 0;

static MEM_HANDLE		s_memHdl = MEM_HDL_INVALID;

static VShader			vs_copy;
static PShader			ps_copy;
static PassShader			s_copyPassShader;

static CVertexBuffer	s_vtxBuffer;	// スクリーン頂点バッファ

static CRasterizer		s_rasterizer;	// デフォルトラスタライザー
static CBlendState		s_blendState;	// デフォルトブレンドステート
static CDepthState		s_depthState;	// デフォルトデプスステンシルステート
static CSampler			s_sampler;		// デフォルトサンプラー

#if defined( LIB_GFX_OPENGL )
static const c8			s_vsCopySource[] =
	"#version 460\n"
	"in vec4		POSTION0;\n"
	"in vec2		TEXCOORD0;\n"
	"in vec4		COLOR0;\n"
	"out gl_PerVertex\n"
	"{\n"
	"	vec4 gl_Position;\n"
	"};\n"
	"out vec2	v_uv;\n"
	"out vec4	v_clr;\n"
	"void main()\n"
	"{\n"
	"	gl_Position = POSTION0;\n"
	"	v_uv = vec2(TEXCOORD0.x, 1.0 - TEXCOORD0.y);\n"
	"	v_clr = COLOR0;\n"
	"}\n"
	;
static const c8			s_psCopySource[] =
	"#version 460\n"
	"in vec2	v_uv;\n"
	"in vec4	v_clr;\n"
	"layout(location = 0) out vec4	out_clr;\n"
	"layout(binding = 0) uniform sampler2D	TEXTURE_SLOT_0;\n"
	"void main()\n"
	"{\n"
	"	out_clr = texture(TEXTURE_SLOT_0, v_uv.xy) * v_clr;\n"
	"}\n"
	;
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
static const c8			s_vsCopySource[] =
	"struct VS_INPUT\n"
	"{\n"
	"	float4 pos : POSITION;\n"
	"	float2 uv  : TEXCOORD0;\n"
	"	float4 clr : COLOR0;\n"
	"};\n"
	"struct VS_OUTPUT\n"
	"{\n"
	"	float4 pos : SV_POSITION;\n"
	"	float2 uv  : TEXCOORD0;\n"
	"	float4 clr : COLOR0;\n"
	"};\n"
	"VS_OUTPUT main(VS_INPUT input)\n"
	"{\n"
	"	VS_OUTPUT output;\n"
	"	output.pos = input.pos;\n"
	"	output.uv  = input.uv;\n"
	"	output.clr = input.clr;\n"
	"	return output;\n"
	"}\n"
	;
static const c8			s_psCopySource[] =
	"struct PS_INPUT\n"
	"{\n"
	"	float4 pos : SV_Position;\n"
	"	float2 uv  : TEXCOORD0;\n"
	"	float4 clr : COLOR0;\n"
	"};\n"
	"sampler sampler0;\n"
	"Texture2D texture0;\n"
	"float4 main(PS_INPUT input) : SV_Target\n"
	"{\n"
	"	return texture0.Sample(sampler0, input.uv) * input.clr;\n"
	"}\n"
	;
#endif // LIB_GFX_DX11


///-------------------------------------------------------------------------------------------------
/// レンダラー設定・取得
CRenderer&		CGfxUtility::GetRenderer(u32 _idx)
{
	libAssert(_idx < s_rendererNum);
	return s_pRenderer[_idx];
}

//-------------------------------------------------------------------------------------------------
//@brief	レンダラー数取得
u32				CGfxUtility::GetRendererNum()
{
	return s_rendererNum;
}

///-------------------------------------------------------------------------------------------------
/// レンダーターゲット取得
CRenderTarget&	CGfxUtility::GetRenderTarget(u32 _idx)
{
	libAssert(_idx < s_renderTargetNum);
	return s_pRenderTarget[_idx];
}

///-------------------------------------------------------------------------------------------------
/// コピーシェーダ取得
const PassShader*		CGfxUtility::GetCopyShader()
{
	return &s_copyPassShader;
}

///-------------------------------------------------------------------------------------------------
/// スクリーン頂点バッファ取得
const CVertexBuffer&	CGfxUtility::GetScreenVtx()
{
	libAssert(s_vtxBuffer.IsEnable());
	return s_vtxBuffer;
}

///-------------------------------------------------------------------------------------------------
/// デフォルトパラメータ取得取得
const CRasterizer&	CGfxUtility::GetDefaultRasterizer()
{
	libAssert(s_rasterizer.IsEnable());
	return s_rasterizer;
}
const CBlendState&	CGfxUtility::GetDefaultBlendState()
{
	libAssert(s_blendState.IsEnable());
	return s_blendState;
}
const CDepthState&	CGfxUtility::GetDefaultDepthState()
{
	libAssert(s_depthState.IsEnable());
	return s_depthState;
}
const CSampler&		CGfxUtility::GetDefaultSampler()
{
	libAssert(s_sampler.IsEnable());
	return s_sampler;
}


///-------------------------------------------------------------------------------------------------
/// スクリーンビューポート取得
const CRect&	CGfxUtility::GetScreenViewport()
{
	CGfxDevice* pDevice = SDCast<CGfxDevice*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	return pDevice->GetViewport();
}

///-------------------------------------------------------------------------------------------------
/// ウィンドウ(クライアント)座標⇒スクリーン座標変換
void	CGfxUtility::ConvertWindowToScreenPos(CVec2& _screenPos, const CVec2& _windowPos)
{
	CGfxDevice* pDevice = SDCast<CGfxDevice*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	const CRect& viewPort = pDevice->GetViewport();
	CVec4 vViewPort(SCast<f32>(viewPort.posX), SCast<f32>(viewPort.posY), SCast<f32>(viewPort.width), SCast<f32>(viewPort.height));
	_screenPos = (CVec2(SCast<f32>(pDevice->GetWidth()), SCast<f32>(pDevice->GetHeight())) * _windowPos - vViewPort.GetXY()) / CVec2(vViewPort.z, vViewPort.w);
}


///-------------------------------------------------------------------------------------------------
/// セットアップ
void	CGfxUtility::SetUpRenderer(CRenderer* _pRenderer, u32 _num)
{
	s_pRenderer = _pRenderer;
	s_rendererNum = _num;
	s_currentIndex = 0;
}

void	CGfxUtility::SetUpRenderTarget(CRenderTarget* pRenderTarget, u32 _num)
{
	libAssert(pRenderTarget && _num > 0);
	s_pRenderTarget = pRenderTarget;
	s_renderTargetNum = _num;
}

///-------------------------------------------------------------------------------------------------
/// 描画スレッド取得・設定
b8		CGfxUtility::IsThreadDraw()
{
	return s_threadDraw;
}
void	CGfxUtility::SetThreadDraw(b8 _enable)
{
	s_threadDraw = _enable;
}

///-------------------------------------------------------------------------------------------------
/// 初期化
b8		CGfxUtility::Initialize(IAllocator* _pAllocator)
{
	b8 bRet = true;

	// 描画リスト初期化
	s_pAllocator = _pAllocator;

	//---------------------------------------------------------------------
	// シェーダ生成
	{
		const c8* vsName = "vs_copy";
		if (!vs_copy.Compile(s_vsCopySource, ARRAYOF(s_vsCopySource), vsName))
		{
			libAssertMsg(0, vsName);
		}
		const c8* psName = "ps_copy";
		if (!ps_copy.Compile(s_psCopySource, ARRAYOF(s_psCopySource), psName))
		{
			libAssertMsg(0, psName);
		}
		const c8* shaderName = "Copy";
		// 頂点DECL
		VTX_DECL vtxDecl[] =
		{
			{ VTX_ATTR_POS, 0, GFX_VALUE_F32, 4, },
			{ VTX_ATTR_CLR0, sizeof(f32) * 4, GFX_VALUE_F32, 4, },
			{ VTX_ATTR_UV0, sizeof(f32) * 8, GFX_VALUE_F32, 2, },
		};
		PassShader::INIT_DESC desc;
		desc.pVshader = &vs_copy;
		desc.pPshader = &ps_copy;
		if (!s_copyPassShader.Create(vtxDecl, ARRAYOF(vtxDecl), desc, shaderName))
		{
			libAssertMsg(0, shaderName);
		}
		bRet &= true;
	}

	//---------------------------------------------------------------------
	// 頂点バッファ生成
	{
		struct VERTEX
		{
			f32 pos[4];
			f32 clr[4];
			f32 uv[2];
		};
		static VERTEX vertex[] =
		{
#ifdef LIB_GFX_OPENGL
			{ { +1.0f, +1.0f, 0.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 1.0f, 0.0f } },
			{ { -1.0f, +1.0f, 0.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 0.0f, 0.0f } },
			{ { -1.0f, -1.0f, 0.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 0.0f, 1.0f } },
			{ { +1.0f, -1.0f, 0.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 1.0f, 1.0f } },
#else
			{ { +1.0f, +1.0f, 0.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 1.0f, 0.0f } },
			{ { -1.0f, +1.0f, 0.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 0.0f, 0.0f } },
			{ { +1.0f, -1.0f, 0.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 1.0f, 1.0f } },
			{ { -1.0f, -1.0f, 0.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 0.0f, 1.0f } },
#endif // LIB_GFX_OPENGL
		};
		VTX_DESC vtxDesc(vertex, sizeof(VERTEX), ARRAYOF(vertex), GFX_USAGE_STATIC);
		bRet &= s_vtxBuffer.Create(vtxDesc);
	}

	//---------------------------------------------------------------------
	// ラスタライザー生成
	{
		RASTERIZER_DESC desc;
		desc.bepthBias = 0.0f;
		desc.bepthBiasClamp = 0.0f;
		desc.slopeScaledDepthBias = 0.0f;
		desc.frontFace = GFX_FRONT_FACE_CCW;
		desc.cullFace = GFX_CULL_FACE_BACK;
		desc.isMSAA = false;
		bRet &= s_rasterizer.Create(desc);
	}

	//---------------------------------------------------------------------
	// ブレンドステート生成
	{
		BLEND_STATE_DESC desc;
		desc.enable = false;
		desc.colorOp = GFX_BLEND_OP_ADD;
		desc.colorSrcFunc = GFX_BLEND_FUNC_ONE;
		desc.colorDstFunc = GFX_BLEND_FUNC_ZERO;
		desc.alphaOp = GFX_BLEND_OP_ADD;
		desc.alphaSrcFunc = GFX_BLEND_FUNC_ONE;
		desc.alphaDstFunc = GFX_BLEND_FUNC_ZERO;
		desc.colorMask = GFX_COLOR_MASK_RGBA;
		bRet &= s_blendState.Create(desc);
	}

	//---------------------------------------------------------------------
	// デプスステンシルステート生成
	{
		DEPTH_STATE_DESC desc;
		desc.isDepthTest = false;
		desc.isDepthWrite = false;
		desc.depthTest = GFX_TEST_ALWAYS;
		desc.isStencilTest = false;
		desc.isStencilWrite = false;
		desc.stencilTest = GFX_TEST_ALWAYS;
		desc.stencilFailOp = GFX_STENCIL_OP_KEEP;
		desc.stencilZFailOp = GFX_STENCIL_OP_KEEP;
		desc.stencilPassOp = GFX_STENCIL_OP_KEEP;
		bRet &= s_depthState.Create(desc);
	}

	//---------------------------------------------------------------------
	// サンプラー生成
	{
		SAMPLER_DESC desc;
		desc.wrapX = GFX_SAMPLER_WRAP_CLAMP;
		desc.wrapY = GFX_SAMPLER_WRAP_CLAMP;
		desc.wrapZ = GFX_SAMPLER_WRAP_CLAMP;
		desc.minFilter = GFX_SAMPLER_FILTER_LINEAR;
		desc.magFilter = GFX_SAMPLER_FILTER_LINEAR;
		desc.mipFilter = GFX_SAMPLER_FILTER_LINEAR;
		desc.anisotropy = 0;
		desc.LODMin = 0;
		desc.LODMax = 0;
		desc.LODBias = 0;
		desc.testFunc = GFX_TEST_ALWAYS;
		memcpy(desc.borderColor, CVec4::Zero.v, sizeof(desc.borderColor));
		bRet &= s_sampler.Create(desc);
	}
	return bRet;
}

///-------------------------------------------------------------------------------------------------
/// 終了
void	CGfxUtility::Finalize()
{
	// レンダラー終了
	for (u32 idx = 0; idx < s_rendererNum; idx++)
	{
		s_pRenderer[idx].Finalize();
	}
	s_pRenderer = NULL;
	s_rendererNum = 0;
	s_currentIndex = 0;

	// レンダーターゲット破棄
	for (u32 idx = 0; idx < s_renderTargetNum; idx++)
	{
		s_pRenderTarget[idx].Release();
	}
	s_renderTargetNum = 0;

	// サンプラー破棄
	s_sampler.Release();
	// デプスステンシルステート破棄
	s_depthState.Release();
	// ブレンドステート破棄
	s_blendState.Release();
	// ラスタライザー破棄
	s_rasterizer.Release();
	// 頂点バッファ破棄
	s_vtxBuffer.Release();

	// 描画リストクリア
	s_drawList.Clear();
	s_pAllocator = NULL;

	vs_copy.Release();
	ps_copy.Release();
	s_copyPassShader.Release();
}


///-------------------------------------------------------------------------------------------------
/// 描画開始
void	CGfxUtility::DrawBegin()
{
	CGfxDevice* pDevice = SDCast<CGfxDevice*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	pDevice->DrawBegin();

	s_pAllocator->Update();
	s_pRegistedDrawObjEnd = NULL;

	// 描画登録開始
	s_isEnableDraw = true;
}

///-------------------------------------------------------------------------------------------------
/// 描画終了
void	CGfxUtility::DrawEnd()
{
	CGfxDevice* pDevice = SDCast<CGfxDevice*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);

	// 描画登録終了
	s_isEnableDraw = false;

	// 各レンダラー描画キック
	b8 isThread = IsThreadDraw();
	if (isThread)
	{
		for (u32 idx = 0; idx < s_rendererNum; idx++)
		{
			s_pRenderer[idx].RunRenderEvent();
		}
	}

	// 描画フラグON
	s_isDrawing = true;

	// 描画リストクリア
	s_drawList.Clear();
	s_pRegistedDrawObjEnd = NULL;

	pDevice->DrawEnd();
}

///-------------------------------------------------------------------------------------------------
/// 描画コマンド登録
void	CGfxUtility::AddDraw(const RENDER_FUNC _renderFunc, const void* _p0, const void* _p1)
{
	if (!s_isEnableDraw)
	{
		libAssert(0);
		return;
	}

	DRAW_OBJ* pDrawObj = MEM_MALLOC_TYPE(s_pAllocator, DRAW_OBJ);
	pDrawObj = new(pDrawObj) DRAW_OBJ;
	libAssert(pDrawObj);
	pDrawObj->renderFunc = _renderFunc;
	pDrawObj->p0 = _p0;
	pDrawObj->p1 = _p1;
	s_drawList.PushBack(pDrawObj);
}

///-------------------------------------------------------------------------------------------------
/// 描画
void	CGfxUtility::DrawFlush()
{
	DRAW_OBJ* pDrawBgn = s_pRegistedDrawObjEnd ? s_pRegistedDrawObjEnd->GetNext() : s_drawList.GetTop();
	DRAW_OBJ* pDrawEnd = s_drawList.GetEnd();

	b8 isThread = IsThreadDraw();

	// レンダラー実行
	CRenderer& rRenderer = s_pRenderer[s_currentIndex];
	rRenderer.ExcecuteRenderer(pDrawBgn, pDrawEnd, isThread);

	// レンダラー更新
	s_currentIndex = (s_currentIndex + 1) % s_rendererNum;

	s_pRegistedDrawObjEnd = pDrawEnd;
}

///-------------------------------------------------------------------------------------------------
/// 描画コマンド待機
b8	CGfxUtility::WaitDrawCommand(b8 _noWait)
{
	if (!s_isDrawing) { return false; }

	b8 bRet = true;
	// 各レンダラー描画待機
	for (u32 idx = 0; idx < s_rendererNum; idx++)
	{
		bRet &= s_pRenderer[idx].WaitDrawCommand(_noWait);
	}
	s_currentIndex = 0;

	// 描画フラグOFF
	s_isDrawing = false;

	return bRet;
}

///-------------------------------------------------------------------------------------------------
/// スワップバッファ
void	CGfxUtility::SwapBuffer(const CTextureBuffer* _pTexture/* = NULL*/, SCREEN_ALIGN _align/* = SCREEN_ALIGN_CENTER_MIDDLE*/, SCREEN_STRETCH _stretch/* = SCREEN_STRETCH_FIT*/)
{
	CGfxDevice* pDevice = SDCast<CGfxDevice*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	if (_pTexture == NULL)
	{
		CRenderTarget* pPostRT = &GetRenderTarget(0);
		libAssert(pPostRT && pPostRT->GetColorTexture(0));
		_pTexture = pPostRT->GetColorTexture(0);
	}
	pDevice->SwapBuffer(_pTexture, _align, _stretch);
}

///-------------------------------------------------------------------------------------------------
/// 垂直同期
b8		CGfxUtility::WaitVsync(u32 _syncInterval)
{
	CGfxDevice* pDevice = SDCast<CGfxDevice*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	return pDevice->WaitVsync(_syncInterval);
}



#ifndef POISON_RELEASE

///-------------------------------------------------------------------------------------------------
/// レンダラー描画プリミティブ数取得
u64		CGfxUtility::GetRenderPrimitiveCount(u32 _idx)
{
	libAssert(_idx < s_rendererNum);
	CPrimitiveQuery* pQuery = GetRenderer(_idx).GetPrimitiveQuery();
	if (pQuery)
	{
		return pQuery->GetResultPrimitive();
	}
	return 0;
}

///-------------------------------------------------------------------------------------------------
/// レンダラー描画時間取得
f64		CGfxUtility::GetRenderTimeMilliSec(u32 _idx)
{
	libAssert(_idx < s_rendererNum);
	CTimerQuery* pQuery = GetRenderer(_idx).GetTimerQuery();
	if (pQuery)
	{
		return pQuery->GetResultTimeMilliSec();
	}
	return 0.0;
}

#endif // !POISON_RELEASE


POISON_END

﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "shader/shader.h"

#include "shader/shader_utility.h"

#include "format/gfx_format_utility.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant



///*************************************************************************************************
///	パスシェーダクラス
///*************************************************************************************************
PassShader::PassShader()
{
}
PassShader::~PassShader()
{
}

////-------------------------------------------------------------------------------------------------
// 生成
b8		PassShader::Create(const VTX_DECL* _pDecl, u32 _declNum, const INIT_CRC_DESC& _desc, const c8* _pName/* = NULL*/)
{
	INIT_DESC desc;
	desc.pVshader = CShaderUtility::SearchVShader(_desc.vsNameCrc);
	desc.pGshader = CShaderUtility::SearchGShader(_desc.gsNameCrc);
	desc.pPshader = CShaderUtility::SearchPShader(_desc.psNameCrc);
	return Create(_pDecl, _declNum, desc, _pName);
}

///-------------------------------------------------------------------------------------------------
/// 頂点情報セットアップ
void	PassShader::VtxDeclSetUp(const VTX_DECL* _pDecl, u32 _declNum)
{
	libAssert(ARRAYOF(m_vtxDecl) > _declNum);
	// 頂点属性コピー
	memcpy(m_vtxDecl, _pDecl, sizeof(VTX_DECL) * _declNum);
	// 頂点属性数
	m_declNum = _declNum;
}



///*************************************************************************************************
///	テクニックシェーダクラス
///*************************************************************************************************
TecShader::TecShader()
{
}

TecShader::~TecShader()
{
}


///-------------------------------------------------------------------------------------------------
/// 属性bit取得
u16		TecShader::GetAttrBit(hash32 _nameCrc) const
{
	const ATTR* pBit = pBit = m_attrList.SearchData(_nameCrc);
	return pBit ? pBit->bit : 0;
}

///-------------------------------------------------------------------------------------------------
/// パスシェーダ取得
const PassShader*	TecShader::GetPassShader(u16 _bit) const
{
	const COMBO* pCombo = m_comboList.SearchData(_bit);
	return pCombo ? pCombo->pPass : NULL;
}


///-------------------------------------------------------------------------------------------------
/// 生成
b8		TecShader::Create(const INIT_DESC& _desc)
{
	// リスト生成
	if (_desc.attrNum > 0)
	{
		if (!m_attrList.Initialize(CShaderUtility::GetMemHdl(), _desc.attrNum))
		{
			return false;
		}
	}
	if (!m_comboList.Initialize(CShaderUtility::GetMemHdl(), _desc.comboNum))
	{
		return false;
	}

	// 属性ビットリスト設定
	for (u16 i = 0; i < _desc.attrNum; ++i)
	{
		m_attrList.PushBack(_desc.pAttrs[i]);
	}

	// パスシェーダリスト設定
	for (u16 i = 0; i < _desc.comboNum; ++i)
	{
		m_comboList.PushBack(_desc.pCombos[i]);
	}

	return true;
}

///-------------------------------------------------------------------------------------------------
/// 破棄
void	TecShader::Release()
{
	m_attrList.Finalize();
	m_comboList.Finalize();
}



///*************************************************************************************************
///	エフェクトシェーダクラス
///*************************************************************************************************
FxShader::FxShader()
{
}
FxShader::~FxShader()
{
}

///-------------------------------------------------------------------------------------------------
/// 有効かどうか
b8		FxShader::IsEnable() const
{
	return (m_tecList.GetUseNum() != 0);
}

///-------------------------------------------------------------------------------------------------
/// テクニックシェーダ取得
const TecShader*	FxShader::GetTecShader(hash32 _nameCrc) const
{
	// 名前指定がなければ最初のテクニックシェーダを取得
	if (_nameCrc == hash32::Invalid) {
		return m_tecList.GetDataFromIdx(0);
	}
	const TecShader* const* ppShader = m_tecList.SearchData(_nameCrc);
	return ppShader ? (*ppShader) : NULL;
}

///-------------------------------------------------------------------------------------------------
/// 生成
b8		FxShader::Create(const INIT_DESC& _desc, const c8* _pName)
{
	// リスト生成
	if (!m_tecList.Initialize(CShaderUtility::GetMemHdl(), _desc.tecNum))
	{
		return false;
	}

	// コンボシェーダリスト設定
	for (u16 i = 0; i < _desc.tecNum; i++)
	{
		m_tecList.PushBack(&_desc.pTecShader[i]);
	}

	// 名前保持
	strcpy_s(m_name, _pName);

	return true;
}

///-------------------------------------------------------------------------------------------------
/// 破棄
void	FxShader::Release()
{
	m_tecList.Finalize();
}

///-------------------------------------------------------------------------------------------------
/// ハッシュ値計算
hash32	FxShader::ConvAttrCrc(hash32 _nameCrc, const VTX_DECL* _pDecl, u32 _declNum)
{
	hash32 attrName = CRC32(_pDecl, sizeof(VTX_DECL) * _declNum);
	return CRC32(&_nameCrc, sizeof(hash32), attrName);
}



POISON_END

﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "shader/shader_utility.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype



///*************************************************************************************************
///	シェーダユーティリティ
///*************************************************************************************************

static IShaderLibrary*	s_pShaderLibrary = NULL;

///-------------------------------------------------------------------------------------------------
/// シェーダライブラリ取得
IShaderLibrary*	CShaderUtility::GetLibrary()
{
	libAssert(s_pShaderLibrary);
	return s_pShaderLibrary;
}

///-------------------------------------------------------------------------------------------------
/// セットアップ
void	CShaderUtility::Setup(IShaderLibrary* _pLibrary)
{
	s_pShaderLibrary = _pLibrary;
}

///-------------------------------------------------------------------------------------------------
/// 終了
void	CShaderUtility::Finalize()
{
	if (s_pShaderLibrary)
	{
		s_pShaderLibrary->Finalize();
	}
	s_pShaderLibrary = NULL;
}

//-------------------------------------------------------------------------------------------------
//@brief	メモリハンドル取得
MEM_HANDLE	CShaderUtility::GetMemHdl()
{
	return GetLibrary()->GetMemHdl();
}

///-------------------------------------------------------------------------------------------------
///	VS検索
const VShader*	CShaderUtility::GetVShader(u16 _idx)
{
	return GetLibrary()->GetVShader(_idx);
}
const VShader*	CShaderUtility::SearchVShader(hash32 _nameCrc)
{
	return GetLibrary()->SearchVShader(_nameCrc);
}

///-------------------------------------------------------------------------------------------------
///	GS検索
const GShader*	CShaderUtility::GetGShader(u16 _idx)
{
	return GetLibrary()->GetGShader(_idx);
}
const GShader*	CShaderUtility::SearchGShader(hash32 _nameCrc)
{
	return GetLibrary()->SearchGShader(_nameCrc);
}

///-------------------------------------------------------------------------------------------------
///	PS検索
const PShader*	CShaderUtility::GetPShader(u16 _idx)
{
	return GetLibrary()->GetPShader(_idx);
}
const PShader*	CShaderUtility::SearchPShader(hash32 _nameCrc)
{
	return GetLibrary()->SearchPShader(_nameCrc);
}

///-------------------------------------------------------------------------------------------------
///	Fx検索
const FxShader*	CShaderUtility::GetFxShader(u16 _idx)
{
	return GetLibrary()->GetFxShader(_idx);
}
const FxShader*	CShaderUtility::SearchAndCreateFxShader(hash32 _nameCrc, const VTX_DECL* _pDecl, u32 _declNum)
{
	return GetLibrary()->SearchAndCreateFxShader(_nameCrc, _pDecl, _declNum);
}

///-------------------------------------------------------------------------------------------------
///	エラーシェーダ取得
const PassShader*	CShaderUtility::GetError()
{
	return GetLibrary()->GetError();
}


POISON_END

﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "device/gfx_device.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///*************************************************************************************************
///	GFXデバイス
///*************************************************************************************************

CGfxDevice::CGfxDevice()
	: m_pDrawContext(NULL)
	, m_width(0)
	, m_height(0)
	, m_swapNum(0)
	, m_refreshRate(60)
{
}

CGfxDevice::~CGfxDevice()
{
}

///-------------------------------------------------------------------------------------------------
/// スクリーンビューポート更新
b8		CGfxDevice::UpdateScreenViewprt(u32 _width, u32 _height, SCREEN_ALIGN _align, SCREEN_STRETCH _stretch)
{
	// 画面ストレッチ関数
	typedef void(*STRETCH_FUNC)(CRect& _rect, u32 _width, u32 _height, u32 _texWidth, u32 _texHeight);
	STRETCH_FUNC StretchFitFunc = [](CRect& _rect, u32 _width, u32 _height, u32 _texWidth, u32 _texHeight)
	{
		f32 aspect = SCast<f32>(_height) / SCast<f32>(_width);
		f32 texAspect = SCast<f32>(_texHeight) / SCast<f32>(_texWidth);
		// 水平合わせ
		if (aspect >= texAspect)
		{
			_rect.width = _width;
			_rect.height = SCast<u32>(_width * texAspect);
		}
		// 垂直合わせ
		else
		{
			_rect.height = _height;
			_rect.width = SCast<u32>(_height / texAspect);
		}
	};
	STRETCH_FUNC StretchHorizontalFunc = [](CRect& _rect, u32 _width, u32 _height, u32 _texWidth, u32 _texHeight)
	{
		f32 texAspect = SCast<f32>(_texHeight) / SCast<f32>(_texWidth);
		_rect.width = _width;
		_rect.height = SCast<u32>(_width * texAspect);
	};
	STRETCH_FUNC StretchVerticalFunc = [](CRect& _rect, u32 _width, u32 _height, u32 _texWidth, u32 _texHeight)
	{
		f32 invTexAspect = SCast<f32>(_texWidth) / SCast<f32>(_texHeight);
		_rect.height = _height;
		_rect.width = SCast<u32>(_height * invTexAspect);
	};
	STRETCH_FUNC StretchOverScanFunc = [](CRect& _rect, u32 _width, u32 _height, u32 _texWidth, u32 _texHeight)
	{
		f32 aspect = SCast<f32>(_height) / SCast<f32>(_width);
		f32 texAspect = SCast<f32>(_texHeight) / SCast<f32>(_texWidth);
		// 水平合わせ
		if (aspect <= texAspect)
		{
			_rect.width = _width;
			_rect.height = SCast<u32>(_width * texAspect);
		}
		// 垂直合わせ
		else
		{
			_rect.height = _height;
			_rect.width = SCast<u32>(_height / texAspect);
		}
	};
	STRETCH_FUNC StretchDirectFunc = [](CRect& _rect, u32 _width, u32 _height, u32 _texWidth, u32 _texHeight)
	{
		_rect.width = _texWidth;
		_rect.height = _texHeight;
	};
	static const STRETCH_FUNC s_stretchFuncs[] =
	{
		StretchFitFunc,
		StretchHorizontalFunc,
		StretchVerticalFunc,
		StretchOverScanFunc,
		StretchDirectFunc,
	};
	StaticAssert(ARRAYOF(s_stretchFuncs) == SCREEN_STRETCH_TYPE_NUM);

	// 画面左右位置
	typedef void(*HALIGN_FUNC)(CRect& _rect, u32 _width);
	HALIGN_FUNC HalignLeftFunc = [](CRect& _rect, u32 _width)
	{
		_rect.posX = 0;
	};
	HALIGN_FUNC HalignCenterFunc = [](CRect& _rect, u32 _width)
	{
		_rect.posX = (_width / 2) - (_rect.width / 2);
	};
	HALIGN_FUNC HalignRightFunc = [](CRect& _rect, u32 _width)
	{
		_rect.posX = _width - _rect.width;
	};
	static const HALIGN_FUNC s_halignFuncs[] =
	{
		HalignLeftFunc,
		HalignCenterFunc,
		HalignRightFunc,
	};
	StaticAssert(ARRAYOF(s_halignFuncs) == (SCREEN_HALIGN_RIGHT >> SCREEN_HALIGN_SHIFT));

	// 画面上下位置
	typedef void(*VALIGN_FUNC)(CRect& _rect, u32 _height);
	VALIGN_FUNC ValignTopFunc = [](CRect& _rect, u32 _height)
	{
#if defined( OPENGL_VIEWPORT )
		_rect.posY = _height - _rect.height;
#elif defined( DIRECTX_VIEWPORT )
		_rect.posY = 0;
#endif
	};
	VALIGN_FUNC ValignMiddleFunc = [](CRect& _rect, u32 _height)
	{
		_rect.posY = (_height / 2) - (_rect.height / 2);
	};
	VALIGN_FUNC ValignBottomFunc = [](CRect& _rect, u32 _height)
	{
#if defined( OPENGL_VIEWPORT )
		_rect.posY = 0;
#elif defined( DIRECTX_VIEWPORT )
		_rect.posY = _height - _rect.height;
#endif
	};
	static const VALIGN_FUNC s_valignFuncs[] =
	{
		ValignTopFunc,
		ValignMiddleFunc,
		ValignBottomFunc,
	};
	StaticAssert(ARRAYOF(s_valignFuncs) == (SCREEN_VALIGN_BOTTOM >> SCREEN_VALIGN_SHIFT));


	CRect rect;
	// 画面ストレッチ
	s_stretchFuncs[_stretch](rect, m_width, m_height, _width, _height);
	// 画面左右位置
	s_halignFuncs[((_align & SCREEN_HALIGN_MASK) >> SCREEN_HALIGN_SHIFT) - 1](rect, m_width);
	// 画面上下位置
	s_valignFuncs[((_align & SCREEN_VALIGN_MASK) >> SCREEN_VALIGN_SHIFT) - 1](rect, m_height);
	b8 bRet = (m_viewport != rect);
	m_viewport = rect;
	return bRet;
}


POISON_END


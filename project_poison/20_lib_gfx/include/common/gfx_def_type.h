﻿//#pragma once
#ifndef __GFX_DEF_TYPE_H__
#define __GFX_DEF_TYPE_H__

//-------------------------------------------------------------------------------------------------
// define
#define LIB_GFX_OPENGL
//#define LIB_GFX_DX11


//-------------------------------------------------------------------------------------------------
// include
#if defined( LIB_GFX_OPENGL )

#include "gl/def/def_gl.h"

#elif defined( LIB_GFX_DX11 )

#include "dx11/def/def_dx11.h"

#endif // LIB_GFX_OPENGL


//-------------------------------------------------------------------------------------------------
// prototype



#endif	// __GFX_DEF_TYPE_H__

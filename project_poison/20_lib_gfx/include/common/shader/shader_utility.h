﻿//#pragma once
#ifndef	__SHADER_UTILITY_H__
#define	__SHADER_UTILITY_H__

//-------------------------------------------------------------------------------------------------
// include
#include "shader.h"


POISON_BGN

//*************************************************************************************************
//@brief	シェーダライブラリ
//*************************************************************************************************
class IShaderLibrary : public CDontCopy
{
public:
	static const u16	INVALID_IDX = 0xffff;	///< 無効インデックス

public:
	IShaderLibrary() {}
	virtual ~IShaderLibrary() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	virtual void	Finalize() = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	メモリハンドル取得
	virtual MEM_HANDLE	GetMemHdl() const = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	VS取得・検索
	virtual const VShader*	GetVShader(u16 _idx) const = 0;
	virtual const VShader*	SearchVShader(hash32 _nameCrc) const = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	GS取得・検索
	virtual const GShader*	GetGShader(u16 _idx) const = 0;
	virtual const GShader*	SearchGShader(hash32 _nameCrc) const = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	PS取得・検索
	virtual const PShader*	GetPShader(u16 _idx) const = 0;
	virtual const PShader*	SearchPShader(hash32 _nameCrc) const = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	Fx取得・検索＆生成
	virtual const FxShader*	GetFxShader(u16 _idx) const = 0;
	virtual const FxShader*	SearchAndCreateFxShader(hash32 _nameCrc, const VTX_DECL* _pDecl, u32 _declNum) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	エラーシェーダ取得
	virtual const PassShader* GetError() const = 0;

};



//*************************************************************************************************
//@brief	シェーダユーティリティ
//*************************************************************************************************
class CShaderUtility
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	シェーダライブラリ取得
	static IShaderLibrary*	GetLibrary();

	//-------------------------------------------------------------------------------------------------
	//@brief	セットアップ
	static void		Setup(IShaderLibrary* _pLibrary);

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	static void		Finalize();

	//-------------------------------------------------------------------------------------------------
	//@brief	メモリハンドル取得
	static MEM_HANDLE	GetMemHdl();

	//-------------------------------------------------------------------------------------------------
	//@brief	VS検索
	static const VShader*		GetVShader(u16 _idx);
	static const VShader*		SearchVShader(hash32 _nameCrc);

	//-------------------------------------------------------------------------------------------------
	//@brief	GS検索
	static const GShader*		GetGShader(u16 _idx);
	static const GShader*		SearchGShader(hash32 _nameCrc);

	//-------------------------------------------------------------------------------------------------
	//@brief	PS検索
	static const PShader*		GetPShader(u16 _idx);
	static const PShader*		SearchPShader(hash32 _nameCrc);
	
	//-------------------------------------------------------------------------------------------------
	//@brief	Fx検索＆生成
	static const FxShader*		GetFxShader(u16 _idx);
	static const FxShader*		SearchAndCreateFxShader(hash32 _nameCrc, const VTX_DECL* _pDecl, u32 _declNum);

	//-------------------------------------------------------------------------------------------------
	//@brief	エラーシェーダ取得
	static const PassShader*		GetError();

};



POISON_END


#endif	// __SHADER_UTILITY_H__

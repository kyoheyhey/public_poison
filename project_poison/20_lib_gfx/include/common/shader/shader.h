﻿//#pragma once
#ifndef	__SHADER_H__
#define	__SHADER_H__


//-------------------------------------------------------------------------------------------------
// define
#if defined( LIB_GFX_OPENGL )
//#define USE_LINK_PROGRAM
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
#endif // LIB_GFX_DX11


//-------------------------------------------------------------------------------------------------
// include
#include "format/gfx_format.h"


POISON_BGN

//*************************************************************************************************
//@brief	頂点シェーダオブジェクト
//*************************************************************************************************
struct VERTEX_SHADER_OBJECT
{
#if defined( LIB_GFX_OPENGL )
	u32		shaderID = 0;		///< シェーダID
	u32		programID = 0;		///< プログラムID
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
	ID3D11VertexShader*	pShader = NULL;
#endif // LIB_GFX_DX11
};

//*************************************************************************************************
//@brief	ジオメトリシェーダオブジェクト
//*************************************************************************************************
struct GEOMETRY_SHADER_OBJECT
{
#if defined( LIB_GFX_OPENGL )
	u32		shaderID = 0;		///< シェーダID
	u32		programID = 0;		///< プログラムID
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
	ID3D11GeometryShader*	pShader = NULL;
#endif // LIB_GFX_DX11
};

//*************************************************************************************************
//@brief	ピクセルシェーダオブジェクト
//*************************************************************************************************
struct PIXEL_SHADER_OBJECT
{
#if defined( LIB_GFX_OPENGL )
	u32		shaderID = 0;		///< シェーダID
	u32		programID = 0;		///< プログラムID
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
	ID3D11PixelShader*	pShader = NULL;
#endif // LIB_GFX_DX11
};

//*************************************************************************************************
//@brief	レイアウトオブジェクト
//*************************************************************************************************
struct LAYOUT_OBJECT
{
#if defined( LIB_GFX_OPENGL )
	u32		programID = 0;		///< プログラムID
	u32		pipelineID = 0;		///< パイプラインID
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
	ID3D11InputLayout*	pLayout = NULL;	///< レイアウト
#endif // LIB_GFX_DX11
};

//*************************************************************************************************
//@brief	頂点属性宣言構造体
//*************************************************************************************************
struct VTX_DECL
{
	VERTEX_ATTRIBUTE	attr = VTX_ATTR_INTERLEAVED;	///< 頂点属性
	u8					offset = 0;						///< 先頭からのオフセット
	GFX_VALUE			valType = GFX_VALUE_F32;		///< データ型
	u8					elemNum = 0;					///< 要素数
	u32					slot = VTX_ATTR_INTERLEAVED;	///< 頂点スロット

	VTX_DECL() {}
	VTX_DECL(VERTEX_ATTRIBUTE _attr, u32 _offset, GFX_VALUE _valType, u32 _elemNum, u32 _slot = VTX_ATTR_INTERLEAVED)
		: attr(_attr)
		, offset(_offset)
		, valType(_valType)
		, elemNum(_elemNum)
		, slot(_slot)
	{}
};

#ifndef POISON_RELEASE
//*************************************************************************************************
//@brief	シェーダスロットチェックサム構造体
//*************************************************************************************************
struct ShaderSlotCheckSum
{
	u32	texAndRoBufMask = 0;	///< テクスチャ＆Ｒｏバッファマスク
	u16	samplerMask = 0;		///< サンプラーマスク
	u16	uniformBlockMask = 0;	///< ユニフォームブロックマスク
	u8	uavBufMask = 0;			///< ＵＡＶバッファマスク
	u8	padding[7] = {};
};
#endif//!POISON_RELEASE



//*************************************************************************************************
//@brief	頂点シェーダ
//*************************************************************************************************
class VShader
{
public:
	VShader() {}
	~VShader() {}
	
	//-------------------------------------------------------------------------------------------------
	//@brief	名前取得・設定
	const c8*		GetName() const { return m_name; }
	void			SetName(const c8* _name);

	//-------------------------------------------------------------------------------------------------
	//@brief	取得
	const VERTEX_SHADER_OBJECT&	GetObject() const { return m_object; }
#ifndef POISON_RELEASE
	const ShaderSlotCheckSum&	GetCheckSum() const { return m_checkSum; }
#endif//!POISON_RELEASE

	//-------------------------------------------------------------------------------------------------
	//@brief	有効かどうか
	b8		IsEnable() const;

#if defined( LIB_GFX_DX11 )
	//-------------------------------------------------------------------------------------------------
	//@brief	ソース取得
	const c8*		GetSrcBuf() const { return m_pSrcBuf; }
	u32				GetSrcBufSize() const { return m_srcBufSize; }
#endif // LIB_GFX_DX11

	//-------------------------------------------------------------------------------------------------
	//@brief	生成
	b8		Create(const void* _pBuf, u32 _size, const c8* _pName = NULL);

	//-------------------------------------------------------------------------------------------------
	//@brief	コンパイル
	b8		Compile(const c8* _pSrc, u32 _size, const c8* _pName = NULL);

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄
	void	Release();

protected:
	VERTEX_SHADER_OBJECT	m_object;			///< オブジェクト
	c8						m_name[64] = {};	///< 名前
#if defined( LIB_GFX_DX11 )
	c8*						m_pSrcBuf = NULL;	///< ソースバッファ
	u32						m_srcBufSize = 0;	///< ソースバッファサイズ
	c8						m_paddingDX11[4] = {};
#endif // LIB_GFX_DX11
#ifndef POISON_RELEASE
	ShaderSlotCheckSum		m_checkSum;			///< チェックサム
#endif//!POISON_RELEASE
};

//*************************************************************************************************
//@brief	ジオメトリシェーダ
//*************************************************************************************************
class GShader
{
public:
	GShader() {}
	~GShader() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	名前取得・設定
	const c8*		GetName() const { return m_name; }
	void			SetName(const c8* _name);

	//-------------------------------------------------------------------------------------------------
	//@brief	取得
	const GEOMETRY_SHADER_OBJECT&	GetObject() const { return m_object; }
#ifndef POISON_RELEASE
	const ShaderSlotCheckSum&		GetCheckSum() const { return m_checkSum; }
#endif//!POISON_RELEASE

	//-------------------------------------------------------------------------------------------------
	//@brief	有効かどうか
	b8		IsEnable() const;

	//-------------------------------------------------------------------------------------------------
	//@brief	生成
	b8		Create(const void* _pBuf, u32 _size, const c8* _pName = NULL);

	//-------------------------------------------------------------------------------------------------
	//@brief	コンパイル
	b8		Compile(const c8* _pSrc, u32 _size, const c8* _pName = NULL);

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄
	void	Release();

protected:
	GEOMETRY_SHADER_OBJECT	m_object;			///< オブジェクト
	c8						m_name[64] = {};	///< 名前
#ifndef POISON_RELEASE
	ShaderSlotCheckSum		m_checkSum;			///< チェックサム
#endif//!POISON_RELEASE
};

//*************************************************************************************************
//@brief	ピクセルシェーダ
//*************************************************************************************************
class PShader
{
public:
	PShader() {}
	~PShader() {}

	//-------------------------------------------------------------------------------------------------
	//@brief	名前取得・設定
	const c8*		GetName() const { return m_name; }
	void			SetName(const c8* _name);

	//-------------------------------------------------------------------------------------------------
	//@brief	取得
	const PIXEL_SHADER_OBJECT&	GetObject() const { return m_object; }
#ifndef POISON_RELEASE
	const ShaderSlotCheckSum&	GetCheckSum() const { return m_checkSum; }
#endif//!POISON_RELEASE

	//-------------------------------------------------------------------------------------------------
	//@brief	有効かどうか
	b8		IsEnable() const;

	//-------------------------------------------------------------------------------------------------
	//@brief	生成
	b8		Create(const void* _pBuf, u32 _size, const c8* _pName = NULL);

	//-------------------------------------------------------------------------------------------------
	//@brief	コンパイル
	b8		Compile(const c8* _pSrc, u32 _size, const c8* _pName = NULL);

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄
	void	Release();

protected:
	PIXEL_SHADER_OBJECT	m_object;			//< オブジェクト
	c8					m_name[64] = {};	///< 名前
#ifndef POISON_RELEASE
	ShaderSlotCheckSum	m_checkSum;			///< チェックサム
#endif//!POISON_RELEASE
};



//*************************************************************************************************
//@brief	パスシェーダ
//*************************************************************************************************
class PassShader
{
public:
	//*************************************************************************************************
	//@brief	構築情報
	struct INIT_DESC
	{
		const VShader* pVshader = NULL;
		const GShader* pGshader = NULL;
		const PShader* pPshader = NULL;
	};
	struct INIT_CRC_DESC
	{
		hash32	vsNameCrc = hash32::Invalid;
		hash32	gsNameCrc = hash32::Invalid;
		hash32	psNameCrc = hash32::Invalid;
	};

public:
	PassShader();
	~PassShader();
	
	//-------------------------------------------------------------------------------------------------
	//@brief	名前取得・設定
	const c8*		GetName() const { return m_name; }
	void			SetName(const c8* _name);

	//-------------------------------------------------------------------------------------------------
	//@brief	取得
	const LAYOUT_OBJECT&	GetObject() const { return m_object; }

	//-------------------------------------------------------------------------------------------------
	//@brief	頂点DECL取得
	const VTX_DECL*	GetVtxDecl() const { return m_vtxDecl; }
	const u32		GetDeclNum() const { return m_declNum; }

	//-------------------------------------------------------------------------------------------------
	//@brief	シェーダ取得
	const VShader*	GetVShader() const { return m_vshader; }
	const GShader*	GetGShader() const { return m_gshader; }
	const PShader*	GetPShader() const { return m_pshader; }

	//-------------------------------------------------------------------------------------------------
	//@brief	有効かどうか
	b8		IsEnable() const;

	//-------------------------------------------------------------------------------------------------
	//@brief	生成
	b8		Create(const VTX_DECL* _pDecl, u32 _declNum, const INIT_DESC& _desc, const c8* _pName = NULL);
	b8		Create(const VTX_DECL* _pDecl, u32 _declNum, const INIT_CRC_DESC& _desc, const c8* _pName = NULL);

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄
	void	Release();

protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	DECLセットアップ
	void	VtxDeclSetUp(const VTX_DECL* _pDecl, u32 _num);


protected:
	LAYOUT_OBJECT	m_object;								///< レイアウトオブジェクト
	const VShader*	m_vshader = NULL;						///< 頂点シェーダ
	const GShader*	m_gshader = NULL;						///< ジオメトリシェーダ
	const PShader*	m_pshader = NULL;						///< ピクセルシェーダ
	VTX_DECL		m_vtxDecl[VERTEX_ATTRIBUTE_TYPE_NUM];	///< 頂点属性DECL
	u32				m_declNum = 0;							///< DECL数
	u8				m_padding[4] = {};
	c8				m_name[64] = {};						///< 名前
};



//*************************************************************************************************
//@brief	テクニックシェーダ
//*************************************************************************************************
class TecShader
{
public:
	//*************************************************************************************************
	//@brief	属性ビット
	struct ATTR
	{
		hash32		GetNameCrc() const { return nameCrc; }
		void		SetNameCrc(hash32 _nameCrc) { nameCrc = _nameCrc; }

		hash32	nameCrc = hash32::Invalid;	///< 属性名Crc
		u16		bit = 0;					///< 属性ビット
	};
	typedef	CIdxSortList<ATTR, hash32>	AttrList;

	//*************************************************************************************************
	//@brief	コンボ情報
	struct COMBO
	{
		u16		GetNameCrc() const { return combobit; }
		void	SetNameCrc(u16 _bit) { combobit = _bit; }

		const PassShader*	pPass = NULL;	///< パスシェーダ
		u16					combobit = 0;	///< コンボビット
	};
	typedef	CIdxSortList<COMBO, u16>	ComboList;

	//*************************************************************************************************
	//@brief	構築情報
	struct INIT_DESC
	{
		ATTR*	pAttrs = NULL;
		u32		attrNum = 0;
		COMBO*	pCombos = NULL;
		u32		comboNum = 0;
	};

public:
	TecShader();
	~TecShader();
	
	//-------------------------------------------------------------------------------------------------
	//@brief	名前取得・設定
	const hash32	GetNameCrc() const { return m_nameCrc; }
	void			SetNameCrc(const hash32 _name) { m_nameCrc = _name; }


	//-------------------------------------------------------------------------------------------------
	//@brief	属性bit取得
	u16					GetAttrBit(hash32 _nameCrc) const;

	//-------------------------------------------------------------------------------------------------
	//@brief	パスシェーダ取得
	const PassShader*	GetPassShader(u16 _bit) const;

	//-------------------------------------------------------------------------------------------------
	//@brief	生成
	b8		Create(const INIT_DESC& _desc);

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄
	void	Release();

private:
	AttrList		m_attrList;		///< 属性ビットリスト
	ComboList		m_comboList;	///< コンボリスト
	hash32			m_nameCrc;		///< 名前Crc
};



//*************************************************************************************************
//@brief	エフェクトシェーダ
//*************************************************************************************************
class FxShader
{
public:
	//*************************************************************************************************
	//@brief	構築情報
	struct INIT_DESC
	{
		const TecShader*	pTecShader = NULL;
		u32					tecNum = 0;
	};

private:
	typedef	CIdxSortList<const TecShader*, hash32, CSortDataPtrIF<const TecShader*, hash32>>	TecShaderList;

public:
	FxShader();
	~FxShader();
	
	//-------------------------------------------------------------------------------------------------
	//@brief	名前取得・設定
	const c8*		GetName() const { return m_name; }
	void			SetName(const c8* _name) { strcpy_s(m_name, _name); }
	const hash32	GetNameCrc() const { return m_hash; }
	void			SetNameCrc(const hash32 _name) { m_hash = _name; }

	//-------------------------------------------------------------------------------------------------
	//@brief	有効かどうか
	b8		IsEnable() const;

	//-------------------------------------------------------------------------------------------------
	//@brief	テクニックシェーダ取得
	const TecShader*	GetTecShader(hash32 _nameCrc) const;

	//-------------------------------------------------------------------------------------------------
	//@brief	生成
	b8		Create(const INIT_DESC& _desc, const c8* _pName);

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄
	void	Release();

	//-------------------------------------------------------------------------------------------------
	//@brief	ハッシュ値変換
	static hash32	ConvAttrCrc(hash32 _nameCrc, const VTX_DECL* _pDecl, u32 _declNum);

protected:
	TecShaderList	m_tecList;			///< テクニックシェーダリスト

	VTX_DECL		m_vtxDecl[VERTEX_ATTRIBUTE_TYPE_NUM];	///< 頂点属性DECL
	u32				m_declNum = 0;							///< DECL数

	c8				m_name[64] = {};	///< 名前
	hash32			m_hash;				///< ハッシュ値
};



POISON_END


#endif	// __SHADER_H__

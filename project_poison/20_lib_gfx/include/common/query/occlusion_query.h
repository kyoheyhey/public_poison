﻿//#pragma once
#ifndef __OCCLUSION_QUERY_H__
#define __OCCLUSION_QUERY_H__

//-------------------------------------------------------------------------------------------------
// include
#include "gfx_query.h"
#include "format/gfx_format.h"


//-------------------------------------------------------------------------------------------------
// prototype
POISON_BGN


//*************************************************************************************************
//@brief	オクルージョンクエリー
//*************************************************************************************************
class COcclusionQuery
{
public:
	COcclusionQuery();
	~COcclusionQuery();

	//-------------------------------------------------------------------------------------------------
	//@brief	取得
	b8										IsEnable() const;
	inline GFX_PREDICATE_COUNTER&			GetCounter() { return m_counter; }
	inline const GFX_PREDICATE_COUNTER&		GetCounter() const { return CCast<COcclusionQuery*>(this)->GetCounter(); }
	inline const GFX_PREDICATE_QUERY_TYPE	GetType() const { return m_type; }


	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	b8		Initialize(GFX_PREDICATE_QUERY_TYPE _type);

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	void	Finalize();

	//-------------------------------------------------------------------------------------------------
	//@brief	オクルージョン結果取得
	b8		GetResultOcclusion() const;

	//-------------------------------------------------------------------------------------------------
	//@brief	ピクセルカウント結果取得
	u64		GetResultPixelCount() const;

private:
	GFX_PREDICATE_COUNTER		m_counter;
	GFX_PREDICATE_QUERY_TYPE	m_type;
};


POISON_END

#endif	// __OCCLUSION_QUERY_H__

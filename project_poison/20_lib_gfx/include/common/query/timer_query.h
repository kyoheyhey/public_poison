﻿//#pragma once
#ifndef __TIMER_QUERY_H__
#define __TIMER_QUERY_H__

//-------------------------------------------------------------------------------------------------
// include
#include "gfx_query.h"


//-------------------------------------------------------------------------------------------------
// prototype
POISON_BGN


//*************************************************************************************************
//@brief	タイマークエリー
//*************************************************************************************************
class CTimerQuery
{
public:
	CTimerQuery();
	~CTimerQuery();

	//-------------------------------------------------------------------------------------------------
	//@brief	取得
	b8							IsEnable() const;
	inline GFX_COUNTER&			GetCounter() { return m_counter; }
	inline const GFX_COUNTER&	GetCounter() const { return CCast<CTimerQuery*>(this)->GetCounter(); }
#ifdef LIB_GFX_DX11
	inline GFX_COUNTER&			GetEndCounter() { return m_counterEnd; }
	inline const GFX_COUNTER&	GetEndCounter() const { return CCast<CTimerQuery*>(this)->GetEndCounter(); }
#endif // LIB_GFX_DX11


	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	b8		Initialize();

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	void	Finalize();

	//-------------------------------------------------------------------------------------------------
	//@brief	タイマー取得（秒）
	f64		GetResultTimeSec() const;

	//-------------------------------------------------------------------------------------------------
	//@brief	タイマー取得（milli秒）
	f64		GetResultTimeMilliSec() const { return GetResultTimeSec() * 1000.0; }

	//-------------------------------------------------------------------------------------------------
	//@brief	タイマー取得（micro秒）
	f64		GetResultTimeMicroSec() const { return GetResultTimeMilliSec() * 1000.0; }


public:
	//-------------------------------------------------------------------------------------------------
	//@brief	周波数取得
	static f64		GetFrequency();

	//-------------------------------------------------------------------------------------------------
	//@brief	周波数計測開始
	static void		FrequencyBgn();

	//-------------------------------------------------------------------------------------------------
	//@brief	周波数計測終了
	static void		FrequencyEnd();

#ifdef LIB_GFX_DX11
	//-------------------------------------------------------------------------------------------------
	//@brief	周波数カウンター初期化
	static b8		InitFrequency();

	//-------------------------------------------------------------------------------------------------
	//@brief	周波数カウンター終了
	static void		FinFrequency();
#endif // LIB_GFX_DX11


private:
	GFX_COUNTER		m_counter;

#ifdef LIB_GFX_DX11
	GFX_COUNTER		m_counterEnd;		///< DX11用時間測定Query
#endif // LIB_GFX_DX11


};


POISON_END

#endif	// __TIMER_QUERY_H__

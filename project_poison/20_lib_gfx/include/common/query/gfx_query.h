﻿//#pragma once
#ifndef __GFX_QUERY_H__
#define __GFX_QUERY_H__

///-------------------------------------------------------------------------------------------------
/// define
#define	QUERY_PREF_NUM	4


//-------------------------------------------------------------------------------------------------
// include
#include "device/device_def.h"


//-------------------------------------------------------------------------------------------------
// prototype
POISON_BGN


//*************************************************************************************************
//@brief	グラフィックスクエリー
//*************************************************************************************************
#if defined( LIB_GFX_OPENGL )
typedef u32				GFX_QUERY;
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
typedef ID3D11Query*	GFX_QUERY;
#endif // LIB_GFX_DX11

//*************************************************************************************************
//@brief	グラフィクスカウンター
//*************************************************************************************************
struct GFX_COUNTER
{
	GFX_QUERY			query[QUERY_PREF_NUM];	///< クエリー
	u8					queryIdx = 0;			///< クエリーインデックス

	//-------------------------------------------------------------------------------------------------
	//@brief	クエリー取得
	inline GFX_QUERY&	GetQuery() { return query[queryIdx]; }

	//-------------------------------------------------------------------------------------------------
	//@brief	クエリーインデックス更新
	inline u8			CountUp()
	{
		queryIdx = CountUp(queryIdx);
		return queryIdx;
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	クエリーインデックス更新（外部で使用版）
	static u8			CountUp(u8 _queryIdx)
	{
		_queryIdx = (_queryIdx + 1) % QUERY_PREF_NUM;
		return _queryIdx;
	}
	static u8			CountDown(u8 _queryIdx)
	{
		_queryIdx = (_queryIdx - 1) % QUERY_PREF_NUM;
		return _queryIdx;
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	クエリー初期化
	inline void			Init()
	{
		memset(this, 0, sizeof(GFX_COUNTER));
	}
};


//*************************************************************************************************
//@brief	プリディケーションクエリー
//*************************************************************************************************
#if defined( LIB_GFX_OPENGL )
typedef u32					GFX_PREDICATE;
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
typedef ID3D11Predicate*	GFX_PREDICATE;
#endif // LIB_GFX_DX11

//*************************************************************************************************
//@brief	プリディケーションカウンター
//*************************************************************************************************
struct GFX_PREDICATE_COUNTER
{
	GFX_PREDICATE		predicate[QUERY_PREF_NUM];	///< プリディケーション
	u8					predicateIdx = 0;			///< プリディケーションインデックス

	//-------------------------------------------------------------------------------------------------
	//@brief	プリディケーション取得
	inline GFX_PREDICATE&	GetPredicate() { return predicate[predicateIdx]; }

	//-------------------------------------------------------------------------------------------------
	//@brief	プリディケーションインデックス更新
	inline u8			CountUp()
	{
		predicateIdx = CountUp(predicateIdx);
		return predicateIdx;
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	プリディケーションインデックス更新（外部で使用版）
	static u8			CountUp(u8 _queryIdx)
	{
		_queryIdx = (_queryIdx + 1) % QUERY_PREF_NUM;
		return _queryIdx;
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	プリディケーション初期化
	inline void			Init()
	{
		memset(this, 0, sizeof(GFX_PREDICATE_COUNTER));
	}
};


POISON_END

#endif	// __GFX_QUERY_H__

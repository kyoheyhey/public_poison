﻿//#pragma once
#ifndef __PRIMITIVE_QUERY_H__
#define __PRIMITIVE_QUERY_H__

//-------------------------------------------------------------------------------------------------
// include
#include "gfx_query.h"


//-------------------------------------------------------------------------------------------------
// prototype
POISON_BGN


//*************************************************************************************************
//@brief	プリミティブクエリー
//*************************************************************************************************
class CPrimitiveQuery
{
public:
	CPrimitiveQuery();
	~CPrimitiveQuery();

	//-------------------------------------------------------------------------------------------------
	//@brief	取得
	b8							IsEnable() const;
	inline GFX_COUNTER&			GetCounter() { return m_counter; }
	inline const GFX_COUNTER&	GetCounter() const { return CCast<CPrimitiveQuery*>(this)->GetCounter(); }


	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	b8		Initialize();

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	void	Finalize();

	//-------------------------------------------------------------------------------------------------
	//@brief	プリミティブ結果取得
	u64		GetResultPrimitive() const;

private:
	GFX_COUNTER		m_counter;
};


POISON_END

#endif	// __PRIMITIVE_QUERY_H__

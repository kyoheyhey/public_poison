﻿//#pragma once
#ifndef	__INDEX_BUFFER_H__
#define	__INDEX_BUFFER_H__

//-------------------------------------------------------------------------------------------------
// include
#include "gfx_object.h"


POISON_BGN

//*************************************************************************************************
//@brief	インデックスデータ宣言構造体
//*************************************************************************************************
struct IDX_DESC
{
	const void*		data = NULL;			//< データ
	u32				elementSize = 0;		//< 要素サイズ
	u32				elementNum = 0;			//< 要素数
	GFX_USAGE		usage = GFX_USAGE_NONE;	//< アクセス方法

	IDX_DESC() {}
	IDX_DESC(const void* _data, u32 _stride, u32 _num, GFX_USAGE _usage)
		: data(_data)
		, elementSize(_stride)
		, elementNum(_num)
		, usage(_usage)
	{}
};

//*************************************************************************************************
//@brief	インデックスバッファ
//*************************************************************************************************
class CIndexBuffer
{
public:
	CIndexBuffer();
	virtual ~CIndexBuffer();

	//-------------------------------------------------------------------------------------------------
	//@brief	取得
	b8						IsEnable() const;
	RENDER_OBJECT&			GetObject()	{ return m_object; }
	const RENDER_OBJECT&	GetObject() const { return CCast<CIndexBuffer*>(this)->GetObject(); }
	const u32				GetElemSize() const { return m_elemSize; }
	const u32				GetIdxNum() const { return m_elemNum; }

	//-------------------------------------------------------------------------------------------------
	//@brief	更新
	void	Update(u32 _size, const void* _data, DRAW_CONTEXT* _pDrawContext = NULL);

	//-------------------------------------------------------------------------------------------------
	//@brief	生成
	b8		Create(const IDX_DESC& _desc, DRAW_CONTEXT* _pDrawContext = NULL);

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄
	void	Release();


private:
	RENDER_OBJECT	m_object;	///< レンダーオブジェクト
	u32				m_elemSize;	///< 要素サイズ
	u32				m_elemNum;	///< 要素数

};


POISON_END

#endif	// __INDEX_BUFFER_H__

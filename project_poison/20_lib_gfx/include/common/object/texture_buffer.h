﻿//#pragma once
#ifndef	__TEXTURE_BUFFER_H__
#define	__TEXTURE_BUFFER_H__

//-------------------------------------------------------------------------------------------------
// include
#include "gfx_object.h"


POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype
class CColorBuffer;
class CDepthBuffer;


//*************************************************************************************************
//@brief	データ構造体
//*************************************************************************************************
struct TEX_DATA_DESC
{
	const void*			data = NULL;					//< データ
	u32					size = 0;						//< データサイズ
};

//*************************************************************************************************
//@brief	初期化構造体
//*************************************************************************************************
struct TEX_DESC
{
	u32					width = 0;						//< 幅
	u32					height = 0;						//< 高さ
	u32					depth = 0;						//< 奥行
	GFX_IMAGE_FORMAT	format = GFX_FORMAT_UNKNOWN;	//< フォーマット
	TEX_DATA_DESC*		pData = NULL;					//< データ（arrayNum*mipLevels）
	u32					arrayNum = 1;					//< 配列数
	u32					mipLevels = 1;					//< ミップマップレベル
	GFX_USAGE			usage = GFX_USAGE_NONE;			//< アクセス
	GFX_BUFFER_ATTR		attr = GFX_BUFFER_ATTR_RO;		//< バッファ属性
};

//*************************************************************************************************
//@brief	テクスチャバッファ
//*************************************************************************************************
class CTextureBuffer
{
public:
	CTextureBuffer();
	~CTextureBuffer();

	//-------------------------------------------------------------------------------------------------
	//@brief	取得
	b8						IsEnable() const;
	TEXTURE_OBJECT&			GetObject()	{ return m_object; }
	const TEXTURE_OBJECT&	GetObject() const { return CCast<CTextureBuffer*>(this)->GetObject(); }
	GFX_TEXTURE_TYPE		GetTexType() const { return m_textureType; }
	u32						GetWidth(u32 _level = 0) const;
	u32						GetWidth(GFX_CUBEMAP_FACE _face, u32 _level = 0) const;
	u32						GetHeight(u32 _level = 0) const;
	u32						GetHeight(GFX_CUBEMAP_FACE _face, u32 _level = 0) const;
	GFX_IMAGE_FORMAT		GetFormat(u32 _level = 0) const;
	GFX_IMAGE_FORMAT		GetFormat(GFX_CUBEMAP_FACE _face, u32 _level = 0) const;

	//-------------------------------------------------------------------------------------------------
	//@brief	更新
	void	Update(u32 _array = 0, u32 _level = 0, u32 _xoff = 0, u32 _yoff = 0, u32 _zoff = 0, const void* _pData = NULL, DRAW_CONTEXT* _pDrawContext = NULL);
	void	Update(GFX_CUBEMAP_FACE _face, u32 _level = 0, u32 _xoff = 0, u32 _yoff = 0, u32 _zoff = 0, const void* _pData = NULL, DRAW_CONTEXT* _pDrawContext = NULL);

	//-------------------------------------------------------------------------------------------------
	//@brief	２Ｄテクスチャ生成
	b8		Create2D(const TEX_DESC& _desc);

	//-------------------------------------------------------------------------------------------------
	//@brief	キューブマップ生成
	b8		CreateCube(const TEX_DESC& _desc);

	//-------------------------------------------------------------------------------------------------
	//@brief	ターゲット参照テクスチャ生成
	b8		InitReferenceTarget(const CColorBuffer* _pClr, GFX_BUFFER_ATTR _attr = GFX_BUFFER_ATTR_RO);
	b8		InitReferenceTarget(const CDepthBuffer* _pDep, GFX_BUFFER_ATTR _attr = GFX_BUFFER_ATTR_RO);

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄
	void	Release();


protected:
	TEXTURE_OBJECT		m_object;		///< テクスチャオブジェクト
	GFX_TEXTURE_TYPE	m_textureType;	///< テクスチャタイプ
	u32					m_width;		///< 幅
	u32					m_height;		///< 高さ
	u32					m_depth;		///< 奥行
	u32					m_arrayNum;		///< テクスチャ配列数
	u32					m_mipLevels;	///< ミップマップレベル
	GFX_IMAGE_FORMAT	m_format;		///< フォーマット

};


POISON_END

#endif	// __TEXTURE_BUFFER_H__

﻿//#pragma once
#ifndef	__UNIFORM_BUFFER_H__
#define	__UNIFORM_BUFFER_H__

//-------------------------------------------------------------------------------------------------
// include
#include "gfx_object.h"


POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype


//*************************************************************************************************
//@brief	頂点データ宣言構造体
//*************************************************************************************************
struct UNIFORM_DESC
{
	const void*		data = NULL;						///< データ
	u32				elementSize = 0;					///< 要素サイズ
	u32				elementNum = 0;						///< 要素数
	GFX_USAGE		usage = GFX_USAGE_NONE;				///< アクセス方法
	GFX_BUFFER_ATTR	attr = GFX_BUFFER_ATTR_CONSTANT;	///< バッファ属性

	UNIFORM_DESC() {}
	UNIFORM_DESC(const void* _data, u32 _stride, u32 _num, GFX_USAGE _usage, GFX_BUFFER_ATTR _attr)
		: data(_data)
		, elementSize(_stride)
		, elementNum(_num)
		, usage(_usage)
		, attr(_attr)
	{}
};

//*************************************************************************************************
//@brief	定数バッファ
//*************************************************************************************************
class CUniformBuffer
{
public:
	CUniformBuffer();
	~CUniformBuffer();

	//-------------------------------------------------------------------------------------------------
	//@brief	取得
	b8						IsEnable() const;
	RENDER_OBJECT&			GetObject()	{ return m_object; }
	const RENDER_OBJECT&	GetObject() const { return CCast<CUniformBuffer*>(this)->GetObject(); }
	const u32				GetElemSize() const { return m_elemSize; }
	const u32				GetElemNum() const { return m_elemNum; }
	GFX_BUFFER_ATTR			GetAttr() const { return m_attr; }

	//-------------------------------------------------------------------------------------------------
	//@brief	更新
	void	Update(u32 _size, const void* _data, DRAW_CONTEXT* _pDrawContext = NULL);

	//-------------------------------------------------------------------------------------------------
	//@brief	生成
	b8		Create(const UNIFORM_DESC& _desc, DRAW_CONTEXT* _pDrawContext = NULL);

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄
	void	Release();


private:
	RENDER_OBJECT		m_object;	///< レンダーオブジェクト
	RENDER_RO_OBJECT	m_roObject;	///< 読み込み用オブジェクト
	RENDER_RW_OBJECT	m_rwObject;	///< 読み込み用オブジェクト
	u32					m_elemSize;	///< 要素サイズ
	u32					m_elemNum;	///< 要素数
	GFX_BUFFER_ATTR		m_attr;		///< 属性
};


POISON_END

#endif	// __UNIFORM_BUFFER_H__

﻿//#pragma once
#ifndef __RASTERIZER_STATE_H__
#define __RASTERIZER_STATE_H__

//-------------------------------------------------------------------------------------------------
// include
#include "gfx_object.h"


//-------------------------------------------------------------------------------------------------
// prototype
POISON_BGN


///*************************************************************************************************
//@brief	ラスタライザーステート宣言構造体
///*************************************************************************************************
struct RASTERIZER_DESC
{
	GFX_FRONT_FACE		frontFace = GFX_FRONT_FACE_CCW;	///< 全面タイプ
	GFX_CULL_FACE		cullFace = GFX_CULL_FACE_NONE;	///< カリング面
	f32					bepthBias = 0.0f;				///< バイアス値
	f32					bepthBiasClamp = 0.0f;			///< バイアス最大値
	f32					slopeScaledDepthBias = 0.0f;	///< 傾斜勾配バイアススケール値
	b8					isMSAA = false;					///< MSAA有効かどうか

#if defined( LIB_GFX_DX11 )
	GFX_IMAGE_FORMAT	depthFormat = GFX_FORMAT_D24_UNORM_S8_UINT;	///< デプスステンシルフォーマット
#endif // LIB_GFX_DX11

};

///*************************************************************************************************
//@brief	ラスタライザーステート
///*************************************************************************************************
class CRasterizer
{
public:
	CRasterizer();
	~CRasterizer();

	//-------------------------------------------------------------------------------------------------
	//@brief	取得
	b8							IsEnable() const;
	RASTERIZER_OBJECT&			GetObject()	{ return m_object; }
	const RASTERIZER_OBJECT&	GetObject() const { return CCast<CRasterizer*>(this)->GetObject(); }
	RASTERIZER_DESC&			GetDesc()	{ return m_desc; }
	const RASTERIZER_DESC&		GetDesc() const { return CCast<CRasterizer*>(this)->GetDesc(); }

	//-------------------------------------------------------------------------------------------------
	//@brief	生成
	b8		Create(const RASTERIZER_DESC& _desc);

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄
	void	Release();


protected:
	RASTERIZER_OBJECT	m_object;	///< ラスタライザーオブジェクト
	RASTERIZER_DESC		m_desc;		///< ラスタライザーDESC
};


POISON_END

#endif	// __RASTERIZER_STATE_H__

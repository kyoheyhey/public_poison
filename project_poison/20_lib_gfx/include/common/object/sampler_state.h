﻿//#pragma once
#ifndef	__SAMPLER_STATE_H__
#define	__SAMPLER_STATE_H__

//-------------------------------------------------------------------------------------------------
// include
#include "gfx_object.h"


POISON_BGN

//*************************************************************************************************
//@brief	サンプラー宣言構造体
//*************************************************************************************************
struct SAMPLER_DESC
{
	GFX_SAMPLER_WRAP	wrapX;			//< X軸ラップ指定
	GFX_SAMPLER_WRAP	wrapY;			//< Y軸ラップ指定
	GFX_SAMPLER_WRAP	wrapZ;			//< Z軸ラップ指定
	GFX_SAMPLER_FILTER	minFilter;		//< 縮小時フィルター指定
	GFX_SAMPLER_FILTER	magFilter;		//< 拡大時フィルター指定
	GFX_SAMPLER_FILTER	mipFilter;		//< mipmapフィルター指定
	u32					anisotropy;		//< 異方性フィルター数(1以上で有効化)
	f32					LODMin;			//< LOD最小値
	f32					LODMax;			//< LOD最大値
	f32					LODBias;		//< LODバイアス
	GFX_TEST			testFunc;		//< 比較計算式
	f32					borderColor[4];	//< ボーダーカラー

	SAMPLER_DESC()
		: wrapX(GFX_SAMPLER_WRAP_UNKNOWN)
		, wrapY(GFX_SAMPLER_WRAP_UNKNOWN)
		, wrapZ(GFX_SAMPLER_WRAP_UNKNOWN)
		, minFilter(GFX_SAMPLER_FILTER_POINT)
		, magFilter(GFX_SAMPLER_FILTER_POINT)
		, mipFilter(GFX_SAMPLER_FILTER_POINT)
		, anisotropy(0)
		, LODMin(0.0f)
		, LODMax(1000.0f)
		, LODBias(0.0f)
		, testFunc(GFX_TEST_NONE)
	{
		memset(borderColor, 0, sizeof(borderColor));
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	デフォルト設定
	void	SetDefault()
	{
		wrapX = GFX_SAMPLER_WRAP_CLAMP;
		wrapY = GFX_SAMPLER_WRAP_CLAMP;
		wrapZ = GFX_SAMPLER_WRAP_CLAMP;
		minFilter = GFX_SAMPLER_FILTER_LINEAR;
		magFilter = GFX_SAMPLER_FILTER_LINEAR;
		mipFilter = GFX_SAMPLER_FILTER_LINEAR;
		anisotropy = 0;
		LODMin = 0.0f;
		LODMax = 100.0f;
		LODBias = 0.0f;
		testFunc = GFX_TEST_ALWAYS;
		memset(borderColor, 0, sizeof(borderColor));
	}
};

//*************************************************************************************************
//@brief	サンプラーステート
//*************************************************************************************************
class CSampler
{
public:
	CSampler();
	~CSampler();

	//-------------------------------------------------------------------------------------------------
	//@brief	取得
	b8						IsEnable() const;
	SAMPLER_OBJECT&			GetObject()	{ return m_object; }
	const SAMPLER_OBJECT&	GetObject() const { return CCast<CSampler*>(this)->GetObject(); }

	//-------------------------------------------------------------------------------------------------
	//@brief	生成
	b8		Create(const SAMPLER_DESC& _desc, DRAW_CONTEXT* _pDrawContext = NULL);

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄
	void	Release();


protected:
	SAMPLER_OBJECT		m_object;	///< サンプラーオブジェクト
};


POISON_END

#endif	// __SAMPLER_STATE_H__

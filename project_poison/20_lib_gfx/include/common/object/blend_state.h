﻿//#pragma once
#ifndef __BLEND_STATE_H__
#define __BLEND_STATE_H__

//-------------------------------------------------------------------------------------------------
// include
#include "gfx_object.h"


//-------------------------------------------------------------------------------------------------
// prototype
POISON_BGN


///*************************************************************************************************
//@brief	ブレンドステート宣言構造体
///*************************************************************************************************
struct BLEND_STATE_DESC
{
	b8				enable = false;							///< ブレンドが有効かどうか
	GFX_BLEND_OP	colorOp = GFX_BLEND_OP_UNKNOWN;			///< カラーブレンドオペレーション
	GFX_BLEND_FUNC	colorSrcFunc = GFX_BLEND_FUNC_UNKNOWN;	///< カラーブレンド関数
	GFX_BLEND_FUNC	colorDstFunc = GFX_BLEND_FUNC_UNKNOWN;	///< カラーブレンド関数
	GFX_BLEND_OP	alphaOp = GFX_BLEND_OP_UNKNOWN;			///< アルファブレンドオペレーション
	GFX_BLEND_FUNC	alphaSrcFunc = GFX_BLEND_FUNC_UNKNOWN;	///< カラーブレンド関数
	GFX_BLEND_FUNC	alphaDstFunc = GFX_BLEND_FUNC_UNKNOWN;	///< カラーブレンド関数
	GFX_COLOR_MASK	colorMask = GFX_COLOR_MASK_RGBA;		///< カラーマスク

};

///*************************************************************************************************
//@brief	ブレンドステート
///*************************************************************************************************
class CBlendState
{
public:
	CBlendState();
	~CBlendState();

	//-------------------------------------------------------------------------------------------------
	//@brief	取得
	b8							IsEnable() const;
	BLEND_STATE_OBJECT&			GetObject()	{ return m_object; }
	const BLEND_STATE_OBJECT&	GetObject() const { return CCast<CBlendState*>(this)->GetObject(); }
	BLEND_STATE_DESC&					GetDesc(RENDER_BUFFER_SLOT _slot)	{ return m_desc[_slot]; }
	const BLEND_STATE_DESC&			GetDesc(RENDER_BUFFER_SLOT _slot) const { return CCast<CBlendState*>(this)->GetDesc(_slot); }

	//-------------------------------------------------------------------------------------------------
	//@brief	生成
	b8		Create(const BLEND_STATE_DESC& _desc);
	b8		Create(const BLEND_STATE_DESC(&_desc)[RENDER_BUFFER_SLOT_NUM]);

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄
	void	Release();


protected:
	BLEND_STATE_OBJECT	m_object;						///< ブレンドステートオブジェクト
	BLEND_STATE_DESC	m_desc[RENDER_BUFFER_SLOT_NUM];	///< ブレンドDESC
};


POISON_END

#endif	// __BLEND_STATE_H__

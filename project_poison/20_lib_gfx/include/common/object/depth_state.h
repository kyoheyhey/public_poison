﻿//#pragma once
#ifndef __DEPTH_STATE_H__
#define __DEPTH_STATE_H__

//-------------------------------------------------------------------------------------------------
// include
#include "gfx_object.h"


//-------------------------------------------------------------------------------------------------
// prototype
POISON_BGN


///*************************************************************************************************
//@brief	デプスステンシルステート宣言構造体
///*************************************************************************************************
struct DEPTH_STATE_DESC
{
	b8				isDepthTest = false;						///< デプステストするか
	b8				isDepthWrite = false;						///< デプス書き込みするか
	GFX_TEST		depthTest = GFX_TEST_NONE;					///< デプス比較式

	b8				isStencilTest = false;						///< ステンシルテストするか
	b8				isStencilWrite = false;						///< ステンシル書き込みするか
	u8				stencilReadMask = 0xFF;						///< ステンシル読み込みマスク
	u8				stencilWriteMask = 0xFF;					///< ステンシル書き込みマスク
	GFX_TEST		stencilTest = GFX_TEST_NONE;				///< ステンシル比較式
	GFX_STENCIL_OP	stencilFailOp = GFX_STENCIL_OP_UNKNOWN;		///< ステンシルテストに失敗した時の処理
	GFX_STENCIL_OP	stencilZFailOp = GFX_STENCIL_OP_UNKNOWN;	///< 深度テストに失敗した時の処理
	GFX_STENCIL_OP	stencilPassOp = GFX_STENCIL_OP_UNKNOWN;		///< ステンシルテストに成功した時に実行する処理

};

///*************************************************************************************************
//@brief	デプスステンシルステート
///*************************************************************************************************
class CDepthState
{
public:
	CDepthState();
	~CDepthState();

	//-------------------------------------------------------------------------------------------------
	//@brief	取得
	b8							IsEnable() const;
	DEPTH_STATE_OBJECT&			GetObject()	{ return m_object; }
	const DEPTH_STATE_OBJECT&	GetObject() const { return CCast<CDepthState*>(this)->GetObject(); }
	DEPTH_STATE_DESC&			GetDesc()	{ return m_desc; }
	const DEPTH_STATE_DESC&		GetDesc() const { return CCast<CDepthState*>(this)->GetDesc(); }

	//-------------------------------------------------------------------------------------------------
	//@brief	生成
	b8		Create(const DEPTH_STATE_DESC& _desc, u32 _stencilRef = 0);

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄
	void	Release();


protected:
	DEPTH_STATE_OBJECT	m_object;	///< デプスステンシルステートオブジェクト
	DEPTH_STATE_DESC	m_desc;		///< デプスステンシルステートDESC
};


POISON_END

#endif	// __DEPTH_STATE_H__

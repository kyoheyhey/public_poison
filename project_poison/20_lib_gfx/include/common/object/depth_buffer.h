﻿//#pragma once
#ifndef	__DEPTH_BUFFER_H__
#define	__DEPTH_BUFFER_H__

//-------------------------------------------------------------------------------------------------
// include
#include "gfx_object.h"


POISON_BGN


//*************************************************************************************************
//@brief	デプスバッファ
//*************************************************************************************************
class CDepthBuffer
{
public:
	friend class CRenderTarget;

public:
	CDepthBuffer();
	~CDepthBuffer();

	//-------------------------------------------------------------------------------------------------
	//@brief	取得
	b8						IsEnable() const;
	DEPTH_OBJECT&			GetObject()	{ return m_object; }
	const DEPTH_OBJECT&		GetObject() const { return CCast<CDepthBuffer*>(this)->GetObject(); }
	u16						GetWidth() const { return m_width; }
	u16						GetHeight() const { return m_height; }
	GFX_IMAGE_FORMAT		GetFormat() const { return m_format; }
	GFX_ANTI_ALIASE			GetAA() const { return m_aa; }
	u16						GetArrayNum() const { return m_arrayNum; }

	//-------------------------------------------------------------------------------------------------
	//@brief	生成
	b8		Create(u16 _width, u16 _height, GFX_IMAGE_FORMAT _format, GFX_ANTI_ALIASE _aa, u32 _num = 1, GFX_USAGE _usage = GFX_USAGE_NONE, GFX_BUFFER_ATTR _attr = GFX_BUFFER_ATTR_RO);

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄
	void	Release();

protected:
	DEPTH_OBJECT			m_object;	///< デプスオブジェクト
	u16						m_width;	///< 幅
	u16						m_height;	///< 高さ
	u16						m_arrayNum;	///< 枚数
	GFX_IMAGE_FORMAT		m_format;	///< フォーマット
	GFX_ANTI_ALIASE			m_aa;		///< アンチエイリアス
};


POISON_END

#endif	// __DEPTH_BUFFER_H__

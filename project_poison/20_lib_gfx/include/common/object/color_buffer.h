﻿//#pragma once
#ifndef	__COLOR_BUFFER_H__
#define	__COLOR_BUFFER_H__

//-------------------------------------------------------------------------------------------------
// include
#include "gfx_object.h"


POISON_BGN


//*************************************************************************************************
//@brief	カラーバッファ
//*************************************************************************************************
class CColorBuffer
{
public:
	friend class CRenderTarget;

public:
	CColorBuffer();
	~CColorBuffer();

	//-------------------------------------------------------------------------------------------------
	//@brief	取得
	b8						IsEnable() const;
	COLOR_OBJECT&			GetObject()	{ return m_object; }
	const COLOR_OBJECT&		GetObject() const { return CCast<CColorBuffer*>(this)->GetObject(); }
	u16						GetWidth() const { return m_width; }
	u16						GetHeight() const { return m_height; }
	GFX_IMAGE_FORMAT		GetFormat() const { return m_format; }
	GFX_ANTI_ALIASE			GetAA() const { return m_aa; }

	//-------------------------------------------------------------------------------------------------
	//@brief	生成
	b8		Create(u16 _width, u16 _height, GFX_IMAGE_FORMAT _format, GFX_ANTI_ALIASE _aa, GFX_USAGE _usage = GFX_USAGE_NONE, GFX_BUFFER_ATTR _attr = GFX_BUFFER_ATTR_RO);

	//-------------------------------------------------------------------------------------------------
	//@brief	スワップバッファから生成
	b8		InitReferenceSwap(SWAP_OBJECT& _swap, u32 _swapIdx, u16 _width, u16 _height, GFX_IMAGE_FORMAT _format, GFX_ANTI_ALIASE _aa);

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄
	void	Release();

protected:
	COLOR_OBJECT			m_object;	///< カラーオブジェクト
	u16						m_width;	///< 幅
	u16						m_height;	///< 高さ
	GFX_IMAGE_FORMAT		m_format;	///< フォーマット
	GFX_ANTI_ALIASE			m_aa;		///< アンチエイリアス
};


POISON_END

#endif	// __COLOR_BUFFER_H__

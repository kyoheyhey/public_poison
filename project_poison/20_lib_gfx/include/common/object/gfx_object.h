﻿//#pragma once
#ifndef __GFX_OBJECT_H__
#define __GFX_OBJECT_H__


//-------------------------------------------------------------------------------------------------
// include
#include "format/gfx_format.h"
#include "device/device_def.h"


POISON_BGN


//*************************************************************************************************
//@brief	レンダーオブジェクト
//*************************************************************************************************
struct RENDER_OBJECT
{
#if defined( LIB_GFX_OPENGL )
	u32		objectID = 0;		///< オブジェクトID
	u32		target = 0;			///< バッファーターゲット
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
	ID3D11Buffer*	pBuf = NULL;	///< レンダーバッファ
#endif // LIB_GFX_DX11
};

//*************************************************************************************************
//@brief	読み込みレンダーオブジェクト
//*************************************************************************************************
struct RENDER_RO_OBJECT
{
#if defined( LIB_GFX_OPENGL )
	u32		objectID = 0;		///< オブジェクトID
	u32		target = 0;			///< バッファーターゲット
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
	ID3D11ShaderResourceView*	pBuf = NULL;	///< レンダーバッファ
#endif // LIB_GFX_DX11
};

//*************************************************************************************************
//@brief	読み書きレンダーオブジェクト
//*************************************************************************************************
struct RENDER_RW_OBJECT
{
#if defined( LIB_GFX_OPENGL )
	u32		objectID = 0;		///< オブジェクトID
	u32		target = 0;			///< バッファーターゲット
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
	ID3D11UnorderedAccessView*	pBuf = NULL;	///< レンダーバッファ
#endif // LIB_GFX_DX11
};


//*************************************************************************************************
//@brief	テクスチャオブジェクト
//*************************************************************************************************
struct TEXTURE_OBJECT
{
#if defined( LIB_GFX_OPENGL )
	u32		objectID = 0;		///< オブジェクトID
	u32		target = 0;			///< バッファーターゲット
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
	ID3D11Texture2D*			pTex = NULL;	///< テクスチャ
	ID3D11ShaderResourceView*	pSRV = NULL;	///< シェーダーリソースビュー
	ID3D11UnorderedAccessView*	pUAV = NULL;	///< アンオーダードアクセスビュー
#endif // LIB_GFX_DX11
};


//*************************************************************************************************
//@brief	カラーオブジェクト
//*************************************************************************************************
struct COLOR_OBJECT
{
#if defined( LIB_GFX_OPENGL )
	u32		objectID = 0;		///< オブジェクトID
	u32		target = 0;			///< バッファーターゲット
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
	ID3D11Texture2D*			pTex = NULL;	///< テクスチャ
	ID3D11RenderTargetView*		pView = NULL;	///< レンダーターゲットビュー
#endif // LIB_GFX_DX11
};


//*************************************************************************************************
//@brief	デプスオブジェクト
//*************************************************************************************************
struct DEPTH_OBJECT
{
#if defined( LIB_GFX_OPENGL )
	u32		objectID = 0;		///< オブジェクトID
	u32		target = 0;			///< バッファーターゲット
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
	ID3D11Texture2D*			pTex = NULL;	///< テクスチャ
	ID3D11DepthStencilView*		pView = NULL;	///< デプスステンシルビュー
#endif // LIB_GFX_DX11
};


//*************************************************************************************************
//@brief	フレームオブジェクト
//*************************************************************************************************
struct FRAME_OBJECT
{
#if defined( LIB_GFX_OPENGL )
	u32		objectID = 0;		///< オブジェクトID
	u32		target = 0;			///< バッファーターゲット
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
#endif // LIB_GFX_DX11
};


//*************************************************************************************************
//@brief	スワップオブジェクト
//*************************************************************************************************
struct SWAP_OBJECT
{
#if defined( LIB_GFX_OPENGL )
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
	IDXGISwapChain*				pBuf = NULL;	///< スワップバッファ
	HWND						param;			///< スワップパラメータ
#endif // LIB_GFX_DX11
};


//*************************************************************************************************
//@brief	サンプラーオブジェクト
//*************************************************************************************************
struct SAMPLER_OBJECT
{
#if defined( LIB_GFX_OPENGL )
	u32		objectID = 0;		///< オブジェクトID
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
	ID3D11SamplerState*			pState = NULL;	///< ステート
#endif // LIB_GFX_DX11
};


//*************************************************************************************************
//@brief	ラストライザーオブジェクト
//*************************************************************************************************
struct RASTERIZER_OBJECT
{
#if defined( LIB_GFX_OPENGL )
	u32		frontFace = 0;			///< 全面
	u32		cullFace = 0;			///< カリング面
	f32		depthBias = 0.0f;		///< デプスバイアス
	f32		slopeScaledDepthBias;	///< 遮蔽勾配スケール
	b8		isMSAA = false;			///< MSAA有効かどうか
	b8		isEnable = false;		///< 有効かどうか
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
	ID3D11RasterizerState*		pState = NULL;	///< ステート
#endif // LIB_GFX_DX11
};


//*************************************************************************************************
//@brief	ブレンドステートオブジェクト
//*************************************************************************************************
struct BLEND_STATE_OBJECT
{
#if defined( LIB_GFX_OPENGL )
	struct TRG_STATE
	{
		u32	colorOp = 0;		///< カラーブレンドオペレーション
		u32	colorSrcFunc = 0;	///< カラーブレンド関数
		u32	colorDstFunc = 0;	///< カラーブレンド関数
		u32	alphaOp = 0;		///< アルファブレンドオペレーション
		u32	alphaSrcFunc = 0;	///< カラーブレンド関数
		u32	alphaDstFunc = 0;	///< カラーブレンド関数
		b8	enable = false;		///< ブレンドが有効かどうか
		b8	colorMask[4];		///< カラーマスク
		TRG_STATE()
		{
			memset(colorMask, 1, sizeof(colorMask));
		}
	};
	TRG_STATE	state[RENDER_BUFFER_SLOT_NUM];	///< ターゲットごとのステート
	b8			isIndependent = false;			///< 同時処理するか
	b8			isEnable = false;				///< 有効かどうか
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
	ID3D11BlendState*		pState = NULL;	///< ステート
#endif // LIB_GFX_DX11
};


//*************************************************************************************************
//@brief	デプスステンシルステートオブジェクト
//*************************************************************************************************
struct DEPTH_STATE_OBJECT
{
#if defined( LIB_GFX_OPENGL )
	u32		depthTest = 0;			///< デプステスト
	b8		isDepthTest = false;	///< デプステストするか
	b8		isDepthWrite = false;	///< デプス書き込みするか

	u32		stencilTest = 0;		///< ステンシル比較式
	u32		stencilFailOp = 0;		///< ステンシルテストに失敗した時の処理
	u32		stencilZFailOp = 0;		///< 深度テストに失敗した時の処理
	u32		stencilPassOp = 0;		///< ステンシルテストに成功した時に実行する処理
	u8		stencilReadMask = 0xFF;	///< ステンシル読み込みマスク
	u8		stencilWriteMask = 0xFF;///< ステンシル書き込みマスク
	b8		isStencilTest = false;	///< ステンシルテストするか
	b8		isStencilWrite = false;	///< ステンシル書き込みするか
	b8		isEnable = false;		///< 有効かどうか
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
	ID3D11DepthStencilState*	pState = NULL;	///< ステート
#endif // LIB_GFX_DX11
};


POISON_END

#endif	// __GFX_OBJECT_H__

﻿//#pragma once
#ifndef	__GFX_FORMAT_H__
#define	__GFX_FORMAT_H__


POISON_BGN

//-------------------------------------------------------------------------------------------------
//@brief	シェーダタイプ
enum GFX_SHADER : u8
{
	GFX_SHADER_VERTEX = 0,
	GFX_SHADER_GEOMETRY,
	//GFX_SHADER_DOMAIN,
	//GFX_SHADER_HULL,
	GFX_SHADER_PIXEL,
	GFX_SHADER_COMPUTE,

	GFX_SHADER_GRAPHICS_TYPE_NUM = GFX_SHADER_PIXEL + 1,
	GFX_SHADER_TYPE_NUM,
};

//-------------------------------------------------------------------------------------------------
//@brief	バッファタイプ
enum GFX_BUFFER : u8
{
	GFX_BUFFER_UNKNOWN = 0,
	GFX_BUFFER_VERTEX,
	GFX_BUFFER_INDEX,
	GFX_BUFFER_UNIFORM,
	GFX_BUFFER_TEXTURE,
};


//-------------------------------------------------------------------------------------------------
//@brief	バッファ属性タイプ
enum GFX_BUFFER_ATTR : u8
{
	GFX_BUFFER_ATTR_CONSTANT = 0,	///< コンスタントなデータバッファ
	GFX_BUFFER_ATTR_RO,				///< 読み込み専用なデータバッファ
	GFX_BUFFER_ATTR_RW,				///< 読み書き可能なデータバッファ

	GFX_BUFFER_ATTR_TYPE_NUM
};

//-------------------------------------------------------------------------------------------------
//@brief	バッファCPUアクセスタイプ
enum GFX_USAGE : u8
{
	GFX_USAGE_NONE = 0,
	GFX_USAGE_STATIC,
	GFX_USAGE_DYNAMIC,
	GFX_USAGE_STAGING,

	GFX_USAGE_TYPE_NUM
};

//-------------------------------------------------------------------------------------------------
//@brief	GFXデータ型タイプ
enum GFX_VALUE : u8
{
	GFX_VALUE_UNKNOWN = 0,
	GFX_VALUE_S8,
	GFX_VALUE_U8,
	GFX_VALUE_S16,
	GFX_VALUE_U16,
	GFX_VALUE_S32,
	GFX_VALUE_U32,
	GFX_VALUE_F32,
	GFX_VALUE_S64,
	GFX_VALUE_U64,
	GFX_VALUE_F64,

	GFX_VALUE_TYPE_NUM
};

//-------------------------------------------------------------------------------------------------
//@brief	イメージフォーマット
enum GFX_IMAGE_FORMAT : u8
{
	GFX_FORMAT_UNKNOWN = 0,
	GFX_FORMAT_R32G32B32A32_TYPELESS,
	GFX_FORMAT_R32G32B32A32_FLOAT,
	GFX_FORMAT_R32G32B32A32_UINT,
	GFX_FORMAT_R32G32B32A32_SINT,
	GFX_FORMAT_R32G32B32_TYPELESS,
	GFX_FORMAT_R32G32B32_FLOAT,
	GFX_FORMAT_R32G32B32_UINT,
	GFX_FORMAT_R32G32B32_SINT,
	GFX_FORMAT_R16G16B16A16_TYPELESS,
	GFX_FORMAT_R16G16B16A16_FLOAT,
	GFX_FORMAT_R16G16B16A16_UNORM,
	GFX_FORMAT_R16G16B16A16_UINT,
	GFX_FORMAT_R16G16B16A16_SNORM,
	GFX_FORMAT_R16G16B16A16_SINT,
	GFX_FORMAT_R32G32_TYPELESS,
	GFX_FORMAT_R32G32_FLOAT,
	GFX_FORMAT_R32G32_UINT,
	GFX_FORMAT_R32G32_SINT,
	GFX_FORMAT_R32G8X24_TYPELESS,
	GFX_FORMAT_D32_FLOAT_S8X24_UINT,
	GFX_FORMAT_R32_FLOAT_X8X24_TYPELESS,
	GFX_FORMAT_X32_TYPELESS_G8X24_UINT,
	GFX_FORMAT_R10G10B10A2_TYPELESS,
	GFX_FORMAT_R10G10B10A2_UNORM,
	GFX_FORMAT_R10G10B10A2_UINT,
	GFX_FORMAT_R11G11B10_FLOAT,
	GFX_FORMAT_R8G8B8A8_TYPELESS,
	GFX_FORMAT_R8G8B8A8_UNORM,
	GFX_FORMAT_R8G8B8A8_UNORM_SRGB,
	GFX_FORMAT_R8G8B8A8_UINT,
	GFX_FORMAT_R8G8B8A8_SNORM,
	GFX_FORMAT_R8G8B8A8_SINT,
	GFX_FORMAT_R16G16_TYPELESS,
	GFX_FORMAT_R16G16_FLOAT,
	GFX_FORMAT_R16G16_UNORM,
	GFX_FORMAT_R16G16_UINT,
	GFX_FORMAT_R16G16_SNORM,
	GFX_FORMAT_R16G16_SINT,
	GFX_FORMAT_R32_TYPELESS,
	GFX_FORMAT_D32_FLOAT,
	GFX_FORMAT_R32_FLOAT,
	GFX_FORMAT_R32_UINT,
	GFX_FORMAT_R32_SINT,
	GFX_FORMAT_R24G8_TYPELESS,
	GFX_FORMAT_D24_UNORM_S8_UINT,
	GFX_FORMAT_R24_UNORM_G8_TYPELESS,
	GFX_FORMAT_X24_TYPELESS_G8_UINT,
	GFX_FORMAT_R8G8_TYPELESS,
	GFX_FORMAT_R8G8_UNORM,
	GFX_FORMAT_R8G8_UINT,
	GFX_FORMAT_R8G8_SNORM,
	GFX_FORMAT_R8G8_SINT,
	GFX_FORMAT_R16_TYPELESS,
	GFX_FORMAT_R16_FLOAT,
	GFX_FORMAT_D16_UNORM,
	GFX_FORMAT_R16_UNORM,
	GFX_FORMAT_R16_UINT,
	GFX_FORMAT_R16_SNORM,
	GFX_FORMAT_R16_SINT,
	GFX_FORMAT_R8_TYPELESS,
	GFX_FORMAT_R8_UNORM,
	GFX_FORMAT_R8_UINT,
	GFX_FORMAT_R8_SNORM,
	GFX_FORMAT_R8_SINT,
	GFX_FORMAT_A8_UNORM,

	GFX_FORMAT_TYPE_NUM
};

//-------------------------------------------------------------------------------------------------
//@brief	アンチエイリアス
enum GFX_ANTI_ALIASE : u8
{
	GFX_AA_UNKNOWN = 0,
	GFX_AA_1X,
	GFX_AA_2X,
	GFX_AA_4X,
	GFX_AA_8X,

	GFX_AA_TYPE_NUM,
};


//-------------------------------------------------------------------------------------------------
//@brief	テクスチャタイプ
enum GFX_TEXTURE_TYPE : u8
{
	GFX_TEXTURE_UNKNOWN = 0,
	GFX_TEXTURE_1D,
	GFX_TEXTURE_2D,
	GFX_TEXTURE_3D,
	GFX_TEXTURE_CUBEMAP,
	GFX_TEXTURE_2DARRAY,
	GFX_TEXTURE_CUBEMAPARRAY,

	GFX_TEXTURE_RES_TYPE_NUM,

	GFX_TEXTURE_COLORBUFFER = GFX_TEXTURE_RES_TYPE_NUM,
	GFX_TEXTURE_DEPTHBUFFER,

	GFX_TEXTURE_TYPE_NUM,
};

//-------------------------------------------------------------------------------------------------
//@brief	キューブマップ面タイプ
enum GFX_CUBEMAP_FACE : u8
{
	GFX_CUBEMAP_LEFT = 0,	///< +X
	GFX_CUBEMAP_RIGHT,		///< -X
	GFX_CUBEMAP_UP,			///< +Y
	GFX_CUBEMAP_DOWN,		///< -Y
	GFX_CUBEMAP_FRONT,		///< +Z
	GFX_CUBEMAP_BACK,		///< -Z

	GFX_CUBEMAP_FACE_TYPE_NUM
};


//-------------------------------------------------------------------------------------------------
//@brief	テスト比較式
enum GFX_TEST : u8
{
	GFX_TEST_NONE = 0,
	GFX_TEST_NEVER,			///< 常に無効
	GFX_TEST_LESS,			///< ref未満
	GFX_TEST_EQUAL,			///< refと同じ
	GFX_TEST_LEQUAL,		///< ref以下
	GFX_TEST_GREATER,		///< refより大きい
	GFX_TEST_GEQUAL,		///< ref以上
	GFX_TEST_NOTEQUAL,		///< ref以外
	GFX_TEST_ALWAYS,		///< 常に有効

	GFX_TEST_TYPE_NUM
};


//-------------------------------------------------------------------------------------------------
//@brief	サンプラーラップモード
enum GFX_SAMPLER_WRAP : u8
{
	GFX_SAMPLER_WRAP_UNKNOWN = 0,
	GFX_SAMPLER_WRAP_CLAMP,			///< クランプ
	GFX_SAMPLER_WRAP_REPEAT,		///< リピート
	GFX_SAMPLER_WRAP_MIRROR,		///< ミラー
	GFX_SAMPLER_WRAP_MIRROR_ONCE,	///< ミラーワンス
	GFX_SAMPLER_WRAP_BORDER,		///< ボーダー

	GFX_SAMPLER_WRAP_TYPE_NUM
};

//-------------------------------------------------------------------------------------------------
//@brief	サンプラーフィルタタイプ
enum GFX_SAMPLER_FILTER : u8
{
	GFX_SAMPLER_FILTER_POINT = 0,
	GFX_SAMPLER_FILTER_LINEAR,

	GFX_SAMPLER_FILTER_TYPE_NUM
};


//-------------------------------------------------------------------------------------------------
//@brief	ブレンドオペレーション
enum GFX_BLEND_OP : u8
{
	GFX_BLEND_OP_UNKNOWN,
	GFX_BLEND_OP_ADD,	///< 加算
	GFX_BLEND_OP_SUB,	///< 減算DST-SRC
	GFX_BLEND_OP_RSUB,	///< 逆減算SRC-DST
	GFX_BLEND_OP_MIN,	///< 最小値
	GFX_BLEND_OP_MAX,	///< 最大値
//	GFX_BLEND_OP_MUL,	///< 乗算

	GFX_BLEND_OP_TYPE_NUM
};

//-------------------------------------------------------------------------------------------------
//@brief	ブレンド式
enum GFX_BLEND_FUNC : u8
{
	GFX_BLEND_FUNC_UNKNOWN = 0,
	GFX_BLEND_FUNC_ZERO,					///< 0
	GFX_BLEND_FUNC_ONE,						///< 1
	GFX_BLEND_FUNC_SRC_COLOR,				///< SRCカラー
	GFX_BLEND_FUNC_INV_SRC_COLOR,			///< 1-SRCカラー
	GFX_BLEND_FUNC_SRC_ALPHA,				///< SRCアルファ
	GFX_BLEND_FUNC_INV_SRC_ALPHA,			///< 1-SRCアルファ
	GFX_BLEND_FUNC_DST_COLOR,				///< DSTカラー
	GFX_BLEND_FUNC_INV_DST_COLOR,			///< 1-DSTカラー
	GFX_BLEND_FUNC_DST_ALPHA,				///< DSTアルファ
	GFX_BLEND_FUNC_INV_DST_ALPHA,			///< 1-DSTアルファ

	GFX_BLEND_FUNC_SRC_ALPHA_SATURATE,		///< SRCアルファー0～1クランプ

	GFX_BLEND_FUNC_CONSTANT_COLOR,			///< コンスタントカラー
	GFX_BLEND_FUNC_INV_CONSTANT_COLOR,		///< 1-コンスタントカラー
	GFX_BLEND_FUNC_CONSTANT_ALPHA,			///< コンスタントアルファ
	GFX_BLEND_FUNC_INV_CONSTANT_ALPHA,		///< 1-コンスタントアルファ

	GFX_BLEND_FUNC_SRC1_COLOR,				///< SRC1カラー
	GFX_BLEND_FUNC_INV_SRC1_COLOR,			///< 1-SRC1カラー
	GFX_BLEND_FUNC_SRC1_ALPHA,				///< SRC1アルファ
	GFX_BLEND_FUNC_INV_SRC1_ALPHA,			///< 1-SRC1アルファ

	GFX_BLEND_FUNC_TYPE_NUM
};

//-------------------------------------------------------------------------------------------------
//@brief	カラーマスクビット
enum GFX_COLOR_MASK : u8
{
	GFX_COLOR_MASK_NONE	= 0,
	GFX_COLOR_MASK_R	= (1 << 0),
	GFX_COLOR_MASK_G	= (1 << 1),
	GFX_COLOR_MASK_B	= (1 << 2),
	GFX_COLOR_MASK_A	= (1 << 3),
	GFX_COLOR_MASK_RGBA	= (GFX_COLOR_MASK_R | GFX_COLOR_MASK_G | GFX_COLOR_MASK_B | GFX_COLOR_MASK_A),
};


//-------------------------------------------------------------------------------------------------
//@brief	ステンシル演算オペレータ
enum GFX_STENCIL_OP : u8
{
	GFX_STENCIL_OP_UNKNOWN = 0,
	GFX_STENCIL_OP_KEEP,			//	dest = src
	GFX_STENCIL_OP_ZERO,			//	dest = 0x00
	GFX_STENCIL_OP_OPVALUE,			//	dest = opVal
	GFX_STENCIL_OP_ADD_CLAMP,		//	dest = src + opVal (clamp)
	GFX_STENCIL_OP_SUB_CLAMP,		//	dest = src - opVal (clamp)
	GFX_STENCIL_OP_ADD_WRAP,		//	dest = src + opVal (wrap)
	GFX_STENCIL_OP_SUB_WRAP,		//	dest = src - opVal (wrap)
	GFX_STENCIL_OP_INVERT,			//	dest = ~src
//	GFX_STENCIL_OP_ONES,			//	dest = 0xFF
//	GFX_STENCIL_OP_TESTVALUE,		//	dest = testVal
//	GFX_STENCIL_OP_AND,				//	dest = src & opVal
//	GFX_STENCIL_OP_OR,				//	dest = src | opVal
//	GFX_STENCIL_OP_XOR,				//	dest = src ^ opVal
//	GFX_STENCIL_OP_NAND,			//	dest = ~(src & opVal)
//	GFX_STENCIL_OP_NOR,				//	dest = ~(src | opVal)
//	GFX_STENCIL_OP_NXOR,			//	dest = ~(src ^ opVal)

	GFX_STENCIL_OP_TYPE_NUM
};

//-------------------------------------------------------------------------------------------------
//@brief	前面指定
enum GFX_FRONT_FACE : u8
{
	GFX_FRONT_FACE_CCW,		///< 反時計回り
	GFX_FRONT_FACE_CW,		///< 時計回り

	GFX_FRONT_FACE_TYPE_NUM
};

//-------------------------------------------------------------------------------------------------
//@brief	カリング面指定
enum GFX_CULL_FACE : u8
{
	GFX_CULL_FACE_NONE = 0,	///< なし
	GFX_CULL_FACE_FRONT,	///< 表面
	GFX_CULL_FACE_BACK,		///< 裏面

	GFX_CULL_FACE_TYPE_NUM
};

//-------------------------------------------------------------------------------------------------
//@brief	プリミティブタイプ
enum GFX_PRIMITIVE : u8
{
	GFX_PRIMITIVE_UNKNOWN = 0,
	GFX_PRIMITIVE_POINTS,					///< 点
	GFX_PRIMITIVE_LINES,					///< ライン
	GFX_PRIMITIVE_LINE_LOOP,				///< ラインループ
	GFX_PRIMITIVE_LINE_STRIP,				///< ラインストリップ
	GFX_PRIMITIVE_TRIANGLES,				///< 三角形
	GFX_PRIMITIVE_TRIANGLE_FAN,				///< 三角形ファン(DirectX11がない)
	GFX_PRIMITIVE_TRIANGLE_STRIP,			///< 三角形ストリップ
	GFX_PRIMITIVE_QUADS,					///< 四角形
	GFX_PRIMITIVE_QUAD_STRIP,				///< 四角形ストリップ

	GFX_PRIMITIVE_LINES_ADJACENCY,				///<
	GFX_PRIMITIVE_LINE_STRIP_ADJACENCY,			///<
	GFX_PRIMITIVE_TRIANGLES_ADJACENCY,			///<
	GFX_PRIMITIVE_TRIANGLE_STRIP_ADJACENCY,		///<

	GFX_PRIMITIVE_TYPE_NUM,
};

//-------------------------------------------------------------------------------------------------
//@brief	プリディケーションクエリータイプ
enum GFX_PREDICATE_QUERY_TYPE : u8
{
	GFX_PREDICATE_OCCL_CHEKER = 0,
	GFX_PREDICATE_PIXEL_COUNTER,

	GFX_PREDICATE_QUERY_TYPE_NUM,
};


//-------------------------------------------------------------------------------------------------
//@brief	レンダーバッファスロット
enum RENDER_BUFFER_SLOT : u8
{
	RENDER_BUFFER_SLOT_0 = 0,
	RENDER_BUFFER_SLOT_1,
	RENDER_BUFFER_SLOT_2,
	RENDER_BUFFER_SLOT_3,
	RENDER_BUFFER_SLOT_4,
	RENDER_BUFFER_SLOT_5,
	RENDER_BUFFER_SLOT_6,
	RENDER_BUFFER_SLOT_7,

	RENDER_BUFFER_SLOT_NUM,
};

//-------------------------------------------------------------------------------------------------
//@brief	レンダーバッファビット
typedef u16		RENDER_BUFFER_BIT;
enum RENDER_BUFFER_BIT_TYPE : RENDER_BUFFER_BIT
{
	RENDER_BUFFER_BIT_COLOR_0 =	(1u << 0),
	RENDER_BUFFER_BIT_COLOR_1 =	(1u << 1),
	RENDER_BUFFER_BIT_COLOR_2 =	(1u << 2),
	RENDER_BUFFER_BIT_COLOR_3 =	(1u << 3),
	RENDER_BUFFER_BIT_COLOR_4 =	(1u << 4),
	RENDER_BUFFER_BIT_COLOR_5 =	(1u << 5),
	RENDER_BUFFER_BIT_COLOR_6 =	(1u << 6),
	RENDER_BUFFER_BIT_COLOR_7 =	(1u << 7),
	RENDER_BUFFER_BIT_DEPTH	 =	(1u << 8),
	RENDER_BUFFER_BIT_STENCIL =	(1u << 9),

	RENDER_BUFFER_BIT_COLOR_ALL	=		RENDER_BUFFER_BIT_COLOR_0 | RENDER_BUFFER_BIT_COLOR_1 | RENDER_BUFFER_BIT_COLOR_2 | RENDER_BUFFER_BIT_COLOR_3 |
										RENDER_BUFFER_BIT_COLOR_4 | RENDER_BUFFER_BIT_COLOR_5 | RENDER_BUFFER_BIT_COLOR_6 | RENDER_BUFFER_BIT_COLOR_7,
	RENDER_BUFFER_BIT_DEPTH_STENCIL =	RENDER_BUFFER_BIT_DEPTH | RENDER_BUFFER_BIT_STENCIL,
	RENDER_BUFFER_BIT_ALL =				RENDER_BUFFER_BIT_COLOR_ALL | RENDER_BUFFER_BIT_DEPTH_STENCIL,
};

//-------------------------------------------------------------------------------------------------
//@brief	カラーバッファビット
typedef u64		COLOR_BIT;

//-------------------------------------------------------------------------------------------------
//@brief	無効デプスインデックス
#define INVALID_RT_IDX	0xff

//-------------------------------------------------------------------------------------------------
//@brief	クリア値
#define	CLEAR_DEPTH		1.0f	// デプスクリア値
#define	CLEAR_STENCIL	0		// ステンシルクリア値


//-------------------------------------------------------------------------------------------------
//@brief	頂点属性タイプ
enum VERTEX_ATTRIBUTE : u8
{
	VTX_ATTR_INTERLEAVED = 0,	///< インタリーブ
	VTX_ATTR_POS = 0,			///< 座標
	VTX_ATTR_NRM,				///< 法線
	VTX_ATTR_TAN,				///< タンジェント
	VTX_ATTR_BIN,				///< バイノーマル
	VTX_ATTR_BLEND_WEIGHT0,		///< ブレンドウェイト0
	VTX_ATTR_BLEND_WEIGHT1,		///< ブレンドウェイト1
	VTX_ATTR_BLEND_INDEX0,		///< ブレンドインデックス0
	VTX_ATTR_BLEND_INDEX1,		///< ブレンドインデックス1
	VTX_ATTR_CLR0,				///< カラー0
	VTX_ATTR_CLR1,				///< カラー1
	VTX_ATTR_UV0,				///< UV0
	VTX_ATTR_UV1,				///< UV1
	VTX_ATTR_UV2,				///< UV2
	VTX_ATTR_UV3,				///< UV3

	VERTEX_ATTRIBUTE_TYPE_NUM,
};

//-------------------------------------------------------------------------------------------------
//@brief	ユニフォームスロット
enum UNIFORM_SLOT : u8
{
	UNIFORM_SLOT_00 = 0,
	UNIFORM_SLOT_01,
	UNIFORM_SLOT_02,
	UNIFORM_SLOT_03,
	UNIFORM_SLOT_04,
	UNIFORM_SLOT_05,
	UNIFORM_SLOT_06,
	UNIFORM_SLOT_07,
	UNIFORM_SLOT_08,
	UNIFORM_SLOT_09,
	UNIFORM_SLOT_10,
	UNIFORM_SLOT_11,
	UNIFORM_SLOT_12,
	UNIFORM_SLOT_13,
	UNIFORM_SLOT_14,
	UNIFORM_SLOT_15,

	UNIFORM_SLOT_NUM,
};

//-------------------------------------------------------------------------------------------------
//@brief	テクスチャスロット
enum TEXTURE_SLOT : u8
{
	TEXTURE_SLOT_00 = 0,
	TEXTURE_SLOT_01,
	TEXTURE_SLOT_02,
	TEXTURE_SLOT_03,
	TEXTURE_SLOT_04,
	TEXTURE_SLOT_05,
	TEXTURE_SLOT_06,
	TEXTURE_SLOT_07,
	TEXTURE_SLOT_08,
	TEXTURE_SLOT_09,
	TEXTURE_SLOT_10,
	TEXTURE_SLOT_11,
	TEXTURE_SLOT_12,
	TEXTURE_SLOT_13,
	TEXTURE_SLOT_14,
	TEXTURE_SLOT_15,

	TEXTURE_SLOT_NUM = 16,	///< 本来は128
};

//-------------------------------------------------------------------------------------------------
//@brief	サンプラースロット
enum SAMPLER_SLOT : u8
{
	SAMPLER_SLOT_00 = 0,
	SAMPLER_SLOT_01,
	SAMPLER_SLOT_02,
	SAMPLER_SLOT_03,
	SAMPLER_SLOT_04,
	SAMPLER_SLOT_05,
	SAMPLER_SLOT_06,
	SAMPLER_SLOT_07,
	SAMPLER_SLOT_08,
	SAMPLER_SLOT_09,
	SAMPLER_SLOT_10,
	SAMPLER_SLOT_11,
	SAMPLER_SLOT_12,
	SAMPLER_SLOT_13,
	SAMPLER_SLOT_14,
	SAMPLER_SLOT_15,

	SAMPLER_SLOT_NUM,
};

//-------------------------------------------------------------------------------------------------
//@brief	読み書きスロット
enum UAV_SLOT : u8
{
	UAV_SLOT_00 = 0,
	UAV_SLOT_01,
	UAV_SLOT_02,
	UAV_SLOT_03,
	UAV_SLOT_04,
	UAV_SLOT_05,
	UAV_SLOT_06,
	UAV_SLOT_07,
	UAV_SLOT_08,
	UAV_SLOT_09,
	UAV_SLOT_10,
	UAV_SLOT_11,
	UAV_SLOT_12,
	UAV_SLOT_13,
	UAV_SLOT_14,
	UAV_SLOT_15,

	UAV_SLOT_NUM,
};


//-------------------------------------------------------------------------------------------------
//@brief	画面位置
enum SCREEN_ALIGN : u8
{
	SCREEN_HALIGN_SHIFT =	0,		///< 水平方向のシフト量
	SCREEN_HALIGN_LEFT =	(1 << SCREEN_HALIGN_SHIFT),	///< 左
	SCREEN_HALIGN_CENTER =	(2 << SCREEN_HALIGN_SHIFT),	///< 中
	SCREEN_HALIGN_RIGHT =	(3 << SCREEN_HALIGN_SHIFT),	///< 右

	SCREEN_VALIGN_SHIFT =	2,		///< 垂直方向のシフト量
	SCREEN_VALIGN_TOP =		(1 << SCREEN_VALIGN_SHIFT),	///< 上
	SCREEN_VALIGN_MIDDLE =	(2 << SCREEN_VALIGN_SHIFT),	///< 中段
	SCREEN_VALIGN_BOTTOM =	(3 << SCREEN_VALIGN_SHIFT),	///< 下

	SCREEN_HALIGN_MASK =	(0x03 << SCREEN_HALIGN_SHIFT),	///< 水平マスク
	SCREEN_VALIGN_MASK =	(0x03 << SCREEN_VALIGN_SHIFT),	///< 垂直マスク

	SCREEN_ALIGN_LEFT_TOP =		(SCREEN_HALIGN_LEFT | SCREEN_VALIGN_TOP),		///< 左上
	SCREEN_ALIGN_LEFT_MIDDLE =	(SCREEN_HALIGN_LEFT | SCREEN_VALIGN_MIDDLE),	///< 左中段
	SCREEN_ALIGN_LEFT_BOTTOM =	(SCREEN_HALIGN_LEFT | SCREEN_VALIGN_BOTTOM),	///< 左下

	SCREEN_ALIGN_CENTER_TOP =	(SCREEN_HALIGN_CENTER | SCREEN_VALIGN_TOP),		///< 中上
	SCREEN_ALIGN_CENTER_MIDDLE =(SCREEN_HALIGN_CENTER | SCREEN_VALIGN_MIDDLE),	///< 中心
	SCREEN_ALIGN_CENTER_BOTTOM =(SCREEN_HALIGN_CENTER | SCREEN_VALIGN_BOTTOM),	///< 中下

	SCREEN_ALIGN_RIGHT_TOP =	(SCREEN_HALIGN_RIGHT | SCREEN_VALIGN_TOP),		///< 右上
	SCREEN_ALIGN_RIGHT_MIDDLE =	(SCREEN_HALIGN_RIGHT | SCREEN_VALIGN_MIDDLE),	///< 右中段
	SCREEN_ALIGN_RIGHT_BOTTOM =	(SCREEN_HALIGN_RIGHT | SCREEN_VALIGN_BOTTOM),	///< 右下
};

//-------------------------------------------------------------------------------------------------
//@brief	画面ストレッチ
enum SCREEN_STRETCH : u8
{
	SCREEN_STRETCH_FIT = 0,		///< フィット(小さい方にストレッチ)
	SCREEN_STRETCH_HORIZONTAL,	///< 水平ストレッチ
	SCREEN_STRETCH_VERTICAL,	///< 垂直ストレッチ
	SCREEN_STRETCH_OVERSCAN,	///< オーバースキャン(大きい方にストレッチ)
	SCREEN_STRETCH_DIRECT,		///< ダイレクト(ストレッチなし)

	SCREEN_STRETCH_TYPE_NUM,
};


//-------------------------------------------------------------------------------------------------
//@brief	行列タイプ
enum MATRIX_TYPE : u8
{
	MATRIX_VIEW_PROJ = 0,	///< ビュー⇒プロジェクション行列
	MATRIX_WORLD_VIEW,		///< ワールド⇒ビュー行列
	MATRIX_LOCAL_WORLD,		///< ローカル⇒ワールド行列

	MATRIX_OLD_VIEW_PROJ,	///< 前回ビュー⇒プロジェクション行列
	MATRIX_OLD_WORLD_VIEW,	///< 前回ワールド⇒ビュー行列
	MATRIX_OLD_LOCAL_WORLD,	///< 前回ローカル⇒ワールド行列

	MATRIX_BASE_NUM,							///< 基本行列の数
	MATRIX_AUTO_CALC_BEGIN = MATRIX_BASE_NUM,	///< これ以降は自動計算行列

	MATRIX_WORLD_PROJ = MATRIX_AUTO_CALC_BEGIN,	///< ワールド⇒プロジェクション行列
	MATRIX_LOCAL_VIEW,							///< ローカル⇒ビュー行列
	MATRIX_LOCAL_PROJ,							///< ローカル⇒プロジェクション行列
	MATRIX_OLD_WORLD_PROJ,						///< 前回ワールド⇒プロジェクション行列
	MATRIX_OLD_LOCAL_VIEW,						///< 前回ローカル⇒のビュー行列
	MATRIX_OLD_LOCAL_PROJ,						///< 前回ローカル⇒のプロジェクション行列

	MATRIX_PROJ_VIEW,		///< プロジェクション⇒ビュー(ビュープロジェクションの逆行列)
	MATRIX_VIEW_WORLD,		///< ビュー⇒ワールド(ワールドビューの逆行列)
	MATRIX_PROJ_WORLD,		///< プロジェクション⇒ワールド(ワールドプロジェの逆行列)

	MATRIX_WORLD_LOCAL,		///< ワールド⇒ローカル(ローカルワールドの逆行列)
	MATRIX_VIEW_BILLBOARD,	///< ビューのビルボード行列

	MATRIX_TYPE_NUM,		///< 行列タイプ個数

	MATRIX_AUTO_CALC_NUM = MATRIX_TYPE_NUM - MATRIX_BASE_NUM,	///< 計算行列の数
	MATRIX_FLUSH_NUM = MATRIX_WORLD_LOCAL,						///< フラッシュ行列個数
};


POISON_END


#endif	// __GFX_FORMAT_H__

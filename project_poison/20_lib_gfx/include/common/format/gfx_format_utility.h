﻿//#pragma once
#ifndef __GFX_FORMAT_UTILITY_H__
#define __GFX_FORMAT_UTILITY_H__

//-------------------------------------------------------------------------------------------------
// include
#include "format/gfx_format.h"


POISON_BGN


//*************************************************************************************************
//@brief	グラフィクスフォーマットユーティリティ
//*************************************************************************************************
class CGfxFormatUtility
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	値フォーマットサイズ取得
	static u32	GetValueSize(GFX_VALUE _varType);

	//-------------------------------------------------------------------------------------------------
	//@brief	サイズから値フォーマット取得
	static GFX_VALUE	GetValueFormatS(u32 _size);
	static GFX_VALUE	GetValueFormatU(u32 _size);
	static GFX_VALUE	GetValueFormatF(u32 _size);

	//-------------------------------------------------------------------------------------------------
	//@brief	型から値フォーマット取得
	template<typename _VAL>
	static GFX_VALUE	GetValueFormat(_VAL& _val);

	//-------------------------------------------------------------------------------------------------
	//@brief	シェーダステージ名取得
	static const c8*	GetShaderStageName(GFX_SHADER _shader);

	//-------------------------------------------------------------------------------------------------
	//@brief	イメージフォーマットサイズ取得
	static u32	GetImageFormatSize(GFX_IMAGE_FORMAT _format);

	//-------------------------------------------------------------------------------------------------
	//@brief	イメージフォーマット変換
	static GFX_IMAGE_FORMAT	ConvertImageFormat(GFX_VALUE _val, u32 _elemNum);

	//-------------------------------------------------------------------------------------------------
	//@brief	型無しイメージフォーマット変換
	static GFX_IMAGE_FORMAT	ConvertTypelessFormat(GFX_IMAGE_FORMAT _format);

	//-------------------------------------------------------------------------------------------------
	//@brief	デプスからカラーイメージフォーマット変換
	static GFX_IMAGE_FORMAT	ConvertDepthToColorFormat(GFX_IMAGE_FORMAT _format);

	//-------------------------------------------------------------------------------------------------
	//@brief	イメージフォーマットからデプス・ステンシルのビット数取得
	static void		GetDepthStencilFormat(u32& _depthBit, u32& _stencilBit, GFX_IMAGE_FORMAT _format);

	//-------------------------------------------------------------------------------------------------
	//@brief	イメージフォーマットがデプス・ステンシル対応かチェック
	static void		CheckDepthStencilFormat(b8& _isDepth, b8& _isStencil, GFX_IMAGE_FORMAT _format);

	//-------------------------------------------------------------------------------------------------
	//@brief	AA変換
	static u32		ConvertAA(GFX_ANTI_ALIASE _AA);

};


POISON_END


#endif	// __GFX_FORMAT_UTILITY_H__

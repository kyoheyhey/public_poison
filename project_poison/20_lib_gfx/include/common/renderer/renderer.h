﻿//#pragma once
#ifndef __RENDERER_H__
#define __RENDERER_H__

//-------------------------------------------------------------------------------------------------
// define
#ifndef POISON_RELEASE
// レンダラーマーカー設定
#define		PUSH_RENDERER_MARKER(__renderer, __name)	__renderer->PushMarker(__name)
// レンダラーマーカー解除
#define		POP_RENDERER_MARKER(__renderer)				__renderer->PopMarker()
#else
// デバッグなしは空
#define		PUSH_RENDERER_MARKER(__renderer, __name)
#define		POP_RENDERER_MARKER(__renderer)
#endif//!POISON_RELEASE

//-------------------------------------------------------------------------------------------------
// include
#include "renderer_list.h"
#include "format/gfx_format.h"
#include "device/device_def.h"

// スレッド
#include "thread/thread.h"


POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype
struct VTX_DECL;
class VShader;
class GShader;
class PShader;
class PassShader;
class TecShader;
class FxShader;

class CColorBuffer;
class CDepthBuffer;
class CRenderTarget;

class CVertexBuffer;
class CIndexBuffer;
class CUniformBuffer;
class CTextureBuffer;

class CSampler;
class CRasterizer;
class CBlendState;
class CDepthState;

class CGfxUtility;
class CRendererUtility;

struct RASTERIZER_DESC;
struct BLEND_STATE_DESC;
struct DEPTH_STATE_DESC;
struct SAMPLER_DESC;

class CVertexCacheBuilder;
class CIndexCacheBuilder;
class CUniformCacheBuilder;
class CRasterizerCacheBuilder;
class CBlendStateCacheBuilder;
class CDepthStateCacheBuilder;
class CSamplerCacheBuilder;

class COcclusionQuery;
class CPrimitiveQuery;
class CTimerQuery;


//*************************************************************************************************
//@brief	描画クラス
//*************************************************************************************************
class CRenderer
{
public:
	friend class CGfxUtility;
	friend class CRendererUtility;


protected:
	enum { STACK_NUM = 4 };	//< スタック数
	enum { UNIFORM_MTX_SLOT = 1 };	//< 行列ユニフォームブロックバッファスロット

	//-------------------------------------------------------------------------------------------------
	//@brief	ステート更新ビット
	typedef u32	UPDATE_STATE_BIT;
	enum UPDATE_STATE_BIT_TYPE : UPDATE_STATE_BIT
	{
		UPDATE_VIEWPORT			= (1 << 0),
		UPDATE_SCISSOR			= (1 << 1),
		UPDATE_RASTERRIZER		= (1 << 2),
		UPDATE_BLEND_STATE		= (1 << 3),
		UPDATE_DEPTH_STATE		= (1 << 4),

		UPDATE_SHADER			= (1 << 5),
		UPDATE_INDEX			= (1 << 6),

		UPDATE_MATRIX			= (1 << 7),	///< 行列更新
		UPDATE_OLD_MATRIX		= (1 << 8),	///< 過去行列更新
	};

	//-------------------------------------------------------------------------------------------------
	//@brief	レンダラ―バッファーデータ
	struct RENDERER_BUFFER_DATA
	{
		const void*			pBuffer = NULL;						///< 設定するデータのポインタ
		u32					elemSize = 0;						///< 設定するデータの要素数サイズ
		u32					elemNum = 0;						///< 設定するデータの要素数
		GFX_IMAGE_FORMAT	dataFormat = GFX_FORMAT_UNKNOWN;	///< データフォーマット(GFX_FORMAT_UNKNOWNのときテクスチャバッファ、!= 0のとき、データバッファ。)
		GFX_TEXTURE_TYPE	type = GFX_TEXTURE_UNKNOWN;			///< テクスチャタイプ
	};


public:
	CRenderer();
	virtual ~CRenderer();

	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	virtual b8		Initialize(IAllocator* _pAllocator, const c8* _pName);

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	virtual void	Finalize();

	//-------------------------------------------------------------------------------------------------
	//@brief	描画コマンド待機
	b8		WaitDrawCommand(b8 _noWait);

protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	描画開始
	virtual void	DrawBegin();

	//-------------------------------------------------------------------------------------------------
	//@brief	描画終了
	virtual void	DrawEnd();

	//-------------------------------------------------------------------------------------------------
	//@brief	描画
	void	Draw();

	//-------------------------------------------------------------------------------------------------
	//@brief	スレッド描画関数
	static THREAD_RESULT	ThreadDraw(void* _pRenderer);

	//-------------------------------------------------------------------------------------------------
	//@brief	描画実行
	void	ExcecuteRenderer(DRAW_OBJ* _pDrawBegin, DRAW_OBJ* _pDrawEnd, b8 _thread = true);

	//-------------------------------------------------------------------------------------------------
	//@brief	描画時ベント実行
	void	RunRenderEvent();

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	名前取得/設定
	const c8*	GetName() const { return m_pName; }
	void		SetName(const c8* _pName) { m_pName = _pName; }

	//-------------------------------------------------------------------------------------------------
	//@brief	スレッドの優先度取得/設定
	s32		GetThreadPriority() const { return m_threadPriority; }
	void	SetThreadPriority(s32 _val) { m_threadPriority = _val; }

	//-------------------------------------------------------------------------------------------------
	//@brief	スレッドのアフィニティ取得/設定
	u32		GetThreadAffinityMask() const { return m_threadAffinityMask; }
	void	SetThreadAffinityMask(u32 _val) { m_threadAffinityMask = _val; }

	//-------------------------------------------------------------------------------------------------
	//@brief	描画関数用アロケータ取得
	IAllocator*	GetAllocator() const { return m_pAllocator; }

	//-------------------------------------------------------------------------------------------------
	//@brief	描画関数用アロケータ取得
	DRAW_CONTEXT*	GetContext() { return m_pDrawContext; }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	キャッシュビルダー設定
	void	SetCacheBuilder(CVertexCacheBuilder* _pCacheBuilder) { m_pVertexCacheBuilder = _pCacheBuilder; }
	void	SetCacheBuilder(CIndexCacheBuilder* _pCacheBuilder) { m_pIndexCacheBuilder = _pCacheBuilder; }
	void	SetCacheBuilder(CUniformCacheBuilder* _pCacheBuilder) { m_pUniformCacheBuilder = _pCacheBuilder; }
	void	SetCacheBuilder(CRasterizerCacheBuilder* _pCacheBuilder) { m_pRasterizerCacheBuilder = _pCacheBuilder; }
	void	SetCacheBuilder(CBlendStateCacheBuilder* _pCacheBuilder) { m_pBlendStateCacheBuilder = _pCacheBuilder; }
	void	SetCacheBuilder(CDepthStateCacheBuilder* _pCacheBuilder) { m_pDepthStateCacheBuilder = _pCacheBuilder; }
	void	SetCacheBuilder(CSamplerCacheBuilder* _pCacheBuilder) { m_pSamplerCacheBuilder = _pCacheBuilder; }

	//-------------------------------------------------------------------------------------------------
	//@brief	頂点キャッシュビルド
	const CVertexBuffer*	CreateAndCacheVertex(u32 _elemSize, u32 _elemNum, const void* _data = NULL);
	//-------------------------------------------------------------------------------------------------
	//@brief	インデックスキャッシュビルド
	const CIndexBuffer*		CreateAndCacheIndex(u32 _elemSize, u32 _elemNum, const void* _data = NULL);
	//-------------------------------------------------------------------------------------------------
	//@brief	ユニフォームキャッシュビルド
	const CUniformBuffer*	CreateAndCacheUniform(u32 _elemSize, u32 _elemNum, const void* _data = NULL);
	//-------------------------------------------------------------------------------------------------
	//@brief	ラスタライザーキャッシュビルド
	const CRasterizer*		CreateAndCacheRasterizer(const RASTERIZER_DESC& _desc);
	//-------------------------------------------------------------------------------------------------
	//@brief	ブレンドステートキャッシュビルド
	const CBlendState*		CreateAndCacheBlendState(const BLEND_STATE_DESC& _desc);
	//-------------------------------------------------------------------------------------------------
	//@brief	デプスステンシルステートキャッシュビルド
	const CDepthState*		CreateAndCacheDepthState(const DEPTH_STATE_DESC& _desc);
	//-------------------------------------------------------------------------------------------------
	//@brief	サンプラーキャッシュビルド
	const CSampler*			CreateAndCacheSampler(const SAMPLER_DESC& _desc);

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	レンダーターゲット設定
	void	SetRenderTarget(const CRenderTarget* _pRenderTarget, COLOR_BIT _clrBit, u32 _depIdx);

	//-------------------------------------------------------------------------------------------------
	//@brief	レンダーターゲットプッシュ・ポップ
	void	PushRenderTarget();
	void	PopRenderTarget();

	//-------------------------------------------------------------------------------------------------
	//@brief	ビューポート設定
	void	SetViewport(const CRect& _rect);

	//-------------------------------------------------------------------------------------------------
	//@brief	シザリング設定
	void	SetScissorRect(const CRect& _rect);

	//-------------------------------------------------------------------------------------------------
	//@brief	レンダーターゲットにサイズにビューポートを設定
	void	ResetViewport();

	//-------------------------------------------------------------------------------------------------
	//@brief	レンダーターゲットにサイズにシザリングを設定
	void	ResetScissorRect();


	//-------------------------------------------------------------------------------------------------
	//@brief	シェーダ設定
	void	SetShader(const PassShader* _pPassShader);
	void	SetShader(const FxShader* _pFxShader, b8 _isError = true);

	//-------------------------------------------------------------------------------------------------
	//@brief	テクニックシェーダ設定
	void	SetTechnic(hash32 _tecNameCrc);

	//-------------------------------------------------------------------------------------------------
	//@brief	属性シェーダ設定
	void	SetAttribute(hash32 _attrNameCrc);

	//-------------------------------------------------------------------------------------------------
	//@brief	頂点設定
	void	SetVertex(const CVertexBuffer* _pVtxBuf, VERTEX_ATTRIBUTE _slot = VTX_ATTR_INTERLEAVED);

	//-------------------------------------------------------------------------------------------------
	//@brief	インデックス設定
	void	SetIndex(const CIndexBuffer* _pIdxBuf);

	//-------------------------------------------------------------------------------------------------
	//@brief	ユニフォーム設定
	void	SetUniform(GFX_SHADER _shader, UNIFORM_SLOT _slot, const CUniformBuffer* _pUniBuf);

	//-------------------------------------------------------------------------------------------------
	//@brief	テクスチャ設定
	void	SetTexture(GFX_SHADER _shader, TEXTURE_SLOT _slot, const CTextureBuffer* _pTexBuf);


	//-------------------------------------------------------------------------------------------------
	//@brief	サンプラー設定
	void	SetSampler(GFX_SHADER _shader, SAMPLER_SLOT _slot, const CSampler* _pSampler);

	//-------------------------------------------------------------------------------------------------
	//@brief	サンプラープッシュ・ポップ
	void	PushSampler();
	void	PopSampler();

	//-------------------------------------------------------------------------------------------------
	//@brief	ラスタライザー設定
	void	SetRasterizer(const CRasterizer* _pRasterizer);

	//-------------------------------------------------------------------------------------------------
	//@brief	ラスタライザープッシュ・ポップ
	void	PushRasterizer();
	void	PopRasterizer();

	//-------------------------------------------------------------------------------------------------
	//@brief	ブレンドステート設定
	void	SetBlendState(const CBlendState* _pBlendState);

	//-------------------------------------------------------------------------------------------------
	//@brief	ブレンドステートプッシュ・ポップ
	void	PushBlendState();
	void	PopBlendState();

	//-------------------------------------------------------------------------------------------------
	//@brief	デプスステンシルステート設定
	void	SetDepthState(const CDepthState* _pDepthState);

	//-------------------------------------------------------------------------------------------------
	//@brief	デプスステンシルステートプッシュ・ポップ
	void	PushDepthState();
	void	PopDepthState();

	//-------------------------------------------------------------------------------------------------
	//@brief	リセットリソース
	void	ResetResource();


public:
	//-------------------------------------------------------------------------------------------------
	//@brief	タイプに合わせた行列設定
	void	LoadMatrix(const MATRIX_TYPE _type, const CMtx44& _mtx);

	//-------------------------------------------------------------------------------------------------
	//@brief	タイプに合わせた現在の行列に掛け算
	void	MultMatrix(const MATRIX_TYPE _type, const CMtx44& _mtx);

	//-------------------------------------------------------------------------------------------------
	//@brief	タイプに合わせた行列取得
	const CMtx44&	GetMatrix(MATRIX_TYPE _type);

	//-------------------------------------------------------------------------------------------------
	///@brief	行列のプッシュ
	void	PushMatrix();

	//-------------------------------------------------------------------------------------------------
	//@brief	行列のポップ
	void	PopMatrix();


public:
	//-------------------------------------------------------------------------------------------------
	//@brief	シェーダ設定（直接）
	virtual void	SetDirectShader(const PassShader* _pPassShader) {}

	//-------------------------------------------------------------------------------------------------
	//@brief	頂点設定（直接）
	virtual void	SetDirectVertex(const CVertexBuffer* _pVtxBuf, VERTEX_ATTRIBUTE _slot = VTX_ATTR_INTERLEAVED) {}

	//-------------------------------------------------------------------------------------------------
	//@brief	インデックス設定（直接）
	virtual void	SetDirectIndex(const CIndexBuffer* _pIdxBuf) {}

	//-------------------------------------------------------------------------------------------------
	//@brief	ユニフォーム設定（直接）
	virtual void	SetDirectUniform(GFX_SHADER _shader, UNIFORM_SLOT _slot, const CUniformBuffer* _pUniBuf) {}

	//-------------------------------------------------------------------------------------------------
	//@brief	テクスチャ設定（直接）
	virtual void	SetDirectTexture(GFX_SHADER _shader, TEXTURE_SLOT _slot, const CTextureBuffer* _pTexBuf) {}

	//-------------------------------------------------------------------------------------------------
	//@brief	サンプラー設定（直接）
	virtual void	SetDirectSampler(SAMPLER_SLOT _slot, const CSampler* _pSampBuf) {}


public:
	//-------------------------------------------------------------------------------------------------
	//@brief	バッファクリア
	virtual void	ClearSurface(const CVec4& _clr, RENDER_BUFFER_BIT _bit = RENDER_BUFFER_BIT_ALL) {}
	virtual void	ClearSurface(const CVec4& _clr, const CRenderTarget* _pRenderTarget, COLOR_BIT _clrBit, u32 _depIdx) {}

	//-------------------------------------------------------------------------------------------------
	//@brief	頂点配列描画
	virtual void	DrawPrim(GFX_PRIMITIVE _primType, u32 _num, u32 _offset = 0) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	インデックス描画
	virtual void	DrawIndexPrim(GFX_PRIMITIVE _primType, u32 _num, u32 _vtxOffset = 0, u32 _idxOffset = 0) = 0;


public:
	//-------------------------------------------------------------------------------------------------
	//@brief	クエリー開始
	virtual void	BeginQuery(const COcclusionQuery* _pQuery) = 0;
	virtual void	BeginQuery(const CPrimitiveQuery* _pQuery) = 0;
	virtual void	BeginQuery(const CTimerQuery* _pQuery) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	クエリー終了
	virtual void	EndQuery(const COcclusionQuery* _pQuery) = 0;
	virtual void	EndQuery(const CPrimitiveQuery* _pQuery) = 0;
	virtual void	EndQuery(const CTimerQuery* _pQuery) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	事前描画遮蔽カリング設定
	virtual void	SetDrawingPredictionQuery(const COcclusionQuery* _pQuery) = 0;


public:
	//-------------------------------------------------------------------------------------------------
	//@brief	マーカープッシュ設定
	virtual void	PushMarker(const c8* _pMessage) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	マーカーポップ設定
	virtual void	PopMarker() = 0;


protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	テクスチャとレンダーターゲットが被ってたらクリア
	b8		ClearTextureRenderTarget(const void* _pRtTex);

	//-------------------------------------------------------------------------------------------------
	//@brief	パスシェーダフラッシュ
	const PassShader*	FlushPassShader();

	//-------------------------------------------------------------------------------------------------
	//@brief	描画フラッシュ開始
	virtual void	FlushDrawBegin();

	//-------------------------------------------------------------------------------------------------
	//@brief	描画フラッシュ終了
	virtual void	FlushDrawEnd();

	//-------------------------------------------------------------------------------------------------
	//@brief	行列計算と行列ユニフォームブロックのフラッシュ
	virtual void	FlushCalcUniformBlockMatrix();

	//-------------------------------------------------------------------------------------------------
	//@brief	レンダーターゲットフラッシュ
	virtual void	FlushRenderTarget() = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	シェーダフラッシュ
	virtual void	FlushShader(const PassShader* _pPassShader) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	頂点フラッシュ
	virtual void	FlushVertex() = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	インデックスフラッシュ
	virtual void	FlushIndex() = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	ユニフォームフラッシュ
	virtual void	FlushUniform() = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	テクスチャフラッシュ
	virtual void	FlushTexture() = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	サンプラーステートフラッシュ
	virtual void	FlushSamplerState() = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	ビューポートフラッシュ
	virtual void	FlushViewport() = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	シザリングフラッシュ
	virtual void	FlushScissorRect() = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	ラストライザステートフラッシュ
	virtual void	FlushRasterrizerState() = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	ブレンドステートフラッシュ
	virtual void	FlushBrendState() = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	デプスステンシルステートフラッシュ
	virtual void	FlushDepthStencilState() = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	事前描画プリディケーションフラッシュ開始
	virtual void	FlushDrawingPredictionQueryBegin() = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	事前描画プリディケーションフラッシュ終了
	virtual void	FlushDrawingPredictionQueryEnd() = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	コマンドキューフラッシュ
	virtual void	FlushCommandQueue() = 0;

#ifndef POISON_RELEASE
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	プリミティブクエリー設定・取得
	void				SetPrimitiveQuery(CPrimitiveQuery* _pQuery) { m_pPrimitiveQuery = _pQuery; }
	CPrimitiveQuery*	GetPrimitiveQuery() const { return m_pPrimitiveQuery; };

	//-------------------------------------------------------------------------------------------------
	//@brief	タイマークエリー設定・取得
	void				SetTimerQuery(CTimerQuery* _pQuery) { m_pTimerQuery = _pQuery; }
	CTimerQuery*		GetTimerQuery() const { return m_pTimerQuery; };

#endif // !POISON_RELEASE


protected:
	IAllocator*				m_pAllocator = NULL;	///< 描画１フレアロケータ
	DRAW_OBJ*				m_pDrawObjBgn = NULL;	///< 描画開始データ
	DRAW_OBJ*				m_pDrawObjEnd = NULL;	///< 描画終了データ

	DRAW_CONTEXT*			m_pDrawContext = NULL;	///< 描画コンテキスト

	CVertexCacheBuilder*		m_pVertexCacheBuilder = NULL;			///< 頂点キャッシュビルダー
	CIndexCacheBuilder*			m_pIndexCacheBuilder = NULL;			///< インデックスキャッシュビルダー
	CUniformCacheBuilder*		m_pUniformCacheBuilder = NULL;			///< ユニフォームキャッシュビルダー
	CRasterizerCacheBuilder*	m_pRasterizerCacheBuilder = NULL;		///< ラスタライザーキャッシュビルダー
	CBlendStateCacheBuilder*	m_pBlendStateCacheBuilder = NULL;		///< ブレンドステートキャッシュビルダー
	CDepthStateCacheBuilder*	m_pDepthStateCacheBuilder = NULL;		///< デプスステンシルステートキャッシュビルダー
	CSamplerCacheBuilder*		m_pSamplerCacheBuilder = NULL;			///< サンプラーキャッシュビルダー

	const CRenderTarget*	m_pCacheRenderTarget[STACK_NUM] = {};		///< キャッシュレンダーターゲット
	COLOR_BIT				m_pCacheColorBufferIdxBit[STACK_NUM] = {};	///< キャッシュカラーバッファインデックスビット
	u8						m_pCacheDepthBufferIdx[STACK_NUM] = {};		///< キャッシュデプスバッファインデックス
	u32						m_currentRenderTargetIdx = 0;				///< レンダーターゲットカレントインデックス
	CRect					m_cacheViewport;							///< キャッシュビューポート
	CRect					m_cacheScissor;								///< キャッシュシザリング

	const VShader*			m_pCacheVShader = NULL;						///< キャッシュ頂点シェーダ
	const GShader*			m_pCacheGShader = NULL;						///< キャッシュジオメトリシェーダ
	const PShader*			m_pCachePShader = NULL;						///< キャッシュピクセルシェーダ
	const PassShader*		m_pCachePassShader = NULL;					///< キャッシュパスシェーダ
	const TecShader*		m_pCacheTecShader = NULL;					///< キャッシュテクニックシェーダ
	const FxShader*			m_pCacheFxShader = NULL;					///< キャッシュエフェクトシェーダ
	hash32					m_tecNameCrc;								///< テクニック名
	hash32					m_attrNameCrcList[16] = {};					///< 属性名リスト（属性ビットは16）
	u16						m_attrCurrentIdx = 0;						///< 属性リストカレントインデックス
	b8						m_isError = false;							///< エラーシェーダ

	const CVertexBuffer*	m_pCacheVertex[VERTEX_ATTRIBUTE_TYPE_NUM] = {};	///< キャッシュ頂点
	const CIndexBuffer*		m_pCacheIndex = NULL;							///< キャッシュインデックス
	RENDERER_BUFFER_DATA	m_cacheUniform[GFX_SHADER_GRAPHICS_TYPE_NUM][UNIFORM_SLOT_NUM];		///< キャッシュユニフォーム
	RENDERER_BUFFER_DATA	m_cacheTexAndRoBuf[GFX_SHADER_GRAPHICS_TYPE_NUM][UNIFORM_SLOT_NUM];	///< キャッシュテクスチャ＆ROバッファ

	const CSampler*			m_pCacheSampler[STACK_NUM][GFX_SHADER_GRAPHICS_TYPE_NUM][SAMPLER_SLOT_NUM];	///< キャッシュサンプラー
	u32						m_currentSamplerIdx = 0;													///< サンプラーカレントインデックス

	const CRasterizer*		m_pCacheRasterizer[STACK_NUM] = {};			///< キャッシュラスタライザー
	u32						m_currentRasterizerIdx = 0;					///< ラスタライザーカレントインデックス

	const CBlendState*		m_pCacheBlendState[STACK_NUM] = {};			///< キャッシュブレンドステート
	u32						m_currentBlendStateIdx = 0;					///< ブレンドステートカレントインデックス

	const CDepthState*		m_pCacheDepthState[STACK_NUM] = {};			///< キャッシュデプスステンシルステート
	u32						m_currentDepthStateIdx = 0;					///< デプスステンシルステートカレントインデックス
	
	const COcclusionQuery*	m_pDrawingPredication = NULL;				///< 事前描画プリディケーション

	CMtx44					m_drawMtx[STACK_NUM][MATRIX_TYPE_NUM];		///< 描画行列
	CMtx44					m_flushMtx[MATRIX_FLUSH_NUM];				///< フラッシュ行列
	u32						m_currentDrawMtxIdx = 0;					///< 描画行列カレントインデックス

	/// 更新チェックビット
	RENDER_BUFFER_BIT		m_updateRenderTargetBit = 0;							///< レンダーターゲット更新フラグ
	u32						m_updateVertexBit = 0;									///< 頂点バッファ更新フラグ
	u32						m_updateUniformBit[GFX_SHADER_GRAPHICS_TYPE_NUM] = {};	///< ユニフォーム更新フラグ
	u32						m_updateTextureBit[GFX_SHADER_GRAPHICS_TYPE_NUM] = {};	///< テクスチャ更新フラグ
	u32						m_updateSamplerBit[GFX_SHADER_GRAPHICS_TYPE_NUM] = {};	///< サンプラー更新フラグ
	u32						m_updateDrawMtxBit[STACK_NUM] = {};						///< 描画行列更新フラグ
	UPDATE_STATE_BIT		m_updateRanderStateBit = 0;								///< UPDATE_STATE_BITにあるステートの更新フラグ



protected:
	const c8*				m_pName = NULL;					///< 名前
	b8						m_isDrawing = false;			///< 描画中かどうか
	b8						m_isDrawingThread = false;		///< スレッド描画中かどうか
	b8						m_isRunRenderEvent = false;		///< 描画イベント実行したか
	CThread					m_thread;						///< 描画スレッド
	THREAD_EVENT			m_eventBgnHdl;					///< 描画開始同期ハンドル			
	THREAD_EVENT			m_eventEndHdl;					///< 描画処理終了同期ハンドル
	s32						m_threadPriority = 0;			///< スレッドのプライオリティ
	u32						m_threadAffinityMask = 0;		///< スレッドのCPUアフィニティ

#ifndef POISON_RELEASE
protected:
	CPrimitiveQuery*		m_pPrimitiveQuery = NULL;
	CTimerQuery*			m_pTimerQuery = NULL;
#endif // !POISON_RELEASE
};

POISON_END


#endif	// __RENDERER_H__

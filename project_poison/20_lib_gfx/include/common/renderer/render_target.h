﻿//#pragma once
#ifndef	__RENDER_TARGET_H__
#define	__RENDER_TARGET_H__

//-------------------------------------------------------------------------------------------------
// include
#include "object/gfx_object.h"


POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype
class CColorBuffer;
class CDepthBuffer;
class CTextureBuffer;


//*************************************************************************************************
//@brief	カラーバッファ宣言構造体
//*************************************************************************************************
struct COLOR_DESC
{
	GFX_IMAGE_FORMAT	format = GFX_FORMAT_R8G8B8A8_UNORM;
	GFX_ANTI_ALIASE		aa = GFX_AA_UNKNOWN;
	GFX_USAGE			usage = GFX_USAGE_NONE;
	GFX_BUFFER_ATTR		attr = GFX_BUFFER_ATTR_RO;
};

//*************************************************************************************************
//@brief	デプスバッファ宣言構造体
//*************************************************************************************************
struct DEPTH_DESC
{
	GFX_IMAGE_FORMAT	format = GFX_FORMAT_D24_UNORM_S8_UINT;
	GFX_ANTI_ALIASE		aa = GFX_AA_UNKNOWN;
	u32					num = 1;
	GFX_USAGE			usage = GFX_USAGE_NONE;
	GFX_BUFFER_ATTR		attr = GFX_BUFFER_ATTR_RO;
};

//*************************************************************************************************
//@brief	レンダーターゲット宣言構造体
//*************************************************************************************************
struct RENDER_TARGET_DESC
{
	const c8*	name = NULL;
	MEM_HANDLE	memHdl = MEM_HDL_INVALID;
	u16			width = 0;
	u16			height = 0;
	COLOR_DESC*	pColor = NULL;
	u32			clrNum = 0;
	DEPTH_DESC*	pDepth = NULL;
	u32			depNum = 0;
};


//*************************************************************************************************
//@brief	レンダーターゲット
//*************************************************************************************************
class CRenderTarget
{
public:
	static const u32	MAX_RT_NUM = 0x40;		///< 最大レンダーバッファ数(64)

public:
	CRenderTarget();
	virtual ~CRenderTarget();

	//-------------------------------------------------------------------------------------------------
	//@brief	オブジェクト取得
	FRAME_OBJECT&		GetObject()	{ return m_object; }
	const FRAME_OBJECT&	GetObject() const { return CCast<CRenderTarget*>(this)->GetObject(); }

	//-------------------------------------------------------------------------------------------------
	//@brief	レンダーバッファ取得
	CColorBuffer*		GetColorRender(u32 _idx);
	const CColorBuffer*	GetColorRender(u32 _idx) const	{ return CCast<CRenderTarget*>(this)->GetColorRender(_idx); }
	u32					GetColorRenderNum() const { return m_colorNum; }
	CDepthBuffer*		GetDepthRender(u32 _idx);
	const CDepthBuffer*	GetDepthRender(u32 _idx) const	{ return CCast<CRenderTarget*>(this)->GetDepthRender(_idx); }
	u32					GetDepthRenderNum() const { return m_depthNum; }

	//-------------------------------------------------------------------------------------------------
	//@brief	テクスチャバッファ取得
	CTextureBuffer*			GetColorTexture(u32 _idx);
	const CTextureBuffer*	GetColorTexture(u32 _idx) const	{ return CCast<CRenderTarget*>(this)->GetColorTexture(_idx); }
	CTextureBuffer*			GetDepthTexture(u32 _idx);
	const CTextureBuffer*	GetDepthTexture(u32 _idx) const	{ return CCast<CRenderTarget*>(this)->GetDepthTexture(_idx); }

	//-------------------------------------------------------------------------------------------------
	//@brief	各種取得
	const c8*			GetName() const { return m_name; }
	MEM_HANDLE			GetMemHdl() const { return m_memHdl; }
	u16					GetWidth() const { return m_width; }
	u16					GetHeight() const { return m_height; }

	//-------------------------------------------------------------------------------------------------
	//@brief	バインドするかどうか
	virtual b8		IsBind() const { return true; }

	//-------------------------------------------------------------------------------------------------
	//@brief	生成
	b8		Create(const RENDER_TARGET_DESC& _desc);

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄
	virtual void	Release();

	//-------------------------------------------------------------------------------------------------
	//@brief	ターゲットのリサイズ
	//@param[in]	_width	幅
	//@param[in]	_height	高さ
	virtual b8		ResizeTarget(u16 _width, u16 _height);

protected:
	c8				m_name[32];	///< 名前
	FRAME_OBJECT	m_object;	///< フレームオブジェクト
	CColorBuffer*	m_pColor;	///< カラーバッファ
	CDepthBuffer*	m_pDepth;	///< デプスバッファ
	CTextureBuffer*	m_pClrTex;	///< カラーテクスチャバッファ(m_colorNum)
	CTextureBuffer*	m_pDepTex;	///< デプステクスチャバッファ(m_depthNum)
	u32				m_colorNum;	///< カラーバッファ数(上限64)
	u32				m_depthNum;	///< デプスバッファ数(上限64)
	MEM_HANDLE		m_memHdl;	///< メモリハンドル
	u16				m_width;	///< 幅
	u16				m_height;	///< 高さ
};


POISON_END

#endif	// __RENDER_TARGET_H__

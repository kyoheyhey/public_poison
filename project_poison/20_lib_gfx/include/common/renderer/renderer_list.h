﻿//#pragma once
#ifndef __RENDERER_LIST_H__
#define __RENDERER_LIST_H__


POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype
class CRenderer;


//-------------------------------------------------------------------------------------------------
//@brief	描画関数
typedef b8(*RENDER_FUNC)(CRenderer*, const void*, const void*);


//-------------------------------------------------------------------------------------------------
//@brief	描画オブジェクト
LINK_NODE(DRAW_OBJ)
{
public:
	RENDER_FUNC		renderFunc = NULL;	//< 描画関数
	const void*		p0 = NULL;			//< ユーザーポインタ0(ユーザーが実態を保証してください)
	const void*		p1 = NULL;			//< ユーザーポインタ1(ユーザーが実態を保証してください)
};


//-------------------------------------------------------------------------------------------------
//@brief	描画リスト
typedef CLinkNode<DRAW_OBJ>	DRAW_LIST;


POISON_END

#endif	// __RENDERER_LIST_H__

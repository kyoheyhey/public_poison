﻿//#pragma once
#ifndef __GFX_UTILITY_H__
#define __GFX_UTILITY_H__

//-------------------------------------------------------------------------------------------------
// include
#include "format/gfx_format.h"
#include "renderer/renderer_list.h"


POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype
class PassShader;
class CVertexBuffer;
class CTextureBuffer;
class CRenderer;
class CRenderTarget;
class CRasterizer;
class CBlendState;
class CDepthState;
class CSampler;


//*************************************************************************************************
//@brief	グラフィクスユーティリティクラス
//*************************************************************************************************
class CGfxUtility
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	レンダラー取得
	static CRenderer&		GetRenderer(u32 _idx);

	//-------------------------------------------------------------------------------------------------
	//@brief	レンダラー数取得
	static u32				GetRendererNum();

	//-------------------------------------------------------------------------------------------------
	//@brief	レンダーターゲット取得
	//@param[in]	_idx	レンダーターゲットインデックス
	static CRenderTarget&	GetRenderTarget(u32 _idx);

	//-------------------------------------------------------------------------------------------------
	//@brief	コピーシェーダ取得
	static const PassShader*	GetCopyShader();

	//-------------------------------------------------------------------------------------------------
	//@brief	スクリーン頂点バッファ取得
	static const CVertexBuffer&	GetScreenVtx();

	//-------------------------------------------------------------------------------------------------
	//@brief	デフォルトパラメータ取得
	static const CRasterizer&	GetDefaultRasterizer();
	static const CBlendState&	GetDefaultBlendState();
	static const CDepthState&	GetDefaultDepthState();
	static const CSampler&		GetDefaultSampler();

	//-------------------------------------------------------------------------------------------------
	//@brief	スクリーンビューポート取得
	static const CRect&	GetScreenViewport();

	//-------------------------------------------------------------------------------------------------
	//@brief	ウィンドウ(クライアント)座標⇒スクリーン座標変換
	static void			ConvertWindowToScreenPos(CVec2& _screenPos, const CVec2& _windowPos);

	//-------------------------------------------------------------------------------------------------
	//@brief	セットアップ
	//@param[in]	_pRenderer		レンダラー配列
	//@param[in]	_num			レンダラー配列数
	static void		SetUpRenderer(CRenderer* _pRenderer, u32 _num);

	//@param[in]	pRenderTarget	レンダーターゲット配列(0番目はポストレンダーターゲット前提)
	//@param[in]	_rtNum			レンダーターゲット配列数
	static void		SetUpRenderTarget(CRenderTarget* pRenderTarget, u32 _num);

	//-------------------------------------------------------------------------------------------------
	//@brief	描画スレッド取得・設定
	static b8		IsThreadDraw();
	static void		SetThreadDraw(b8 _enable);

	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	static b8		Initialize(IAllocator* _pAllocator);

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	static void		Finalize();

	//-------------------------------------------------------------------------------------------------
	//@brief	描画開始
	static void		DrawBegin();

	//-------------------------------------------------------------------------------------------------
	//@brief	描画終了
	static void		DrawEnd();

	//-------------------------------------------------------------------------------------------------
	//@brief	描画コマンド登録
	static void		AddDraw(const RENDER_FUNC _renderFunc, const void* _p0, const void* _p1);

	//-------------------------------------------------------------------------------------------------
	//@brief	描画コマンド登録(テンプレート引数版、内部でキャスト)
	template< typename _FUNC, typename _T0, typename _T1 >
	static void		AddDraw(const _FUNC& _pFunc, const _T0 _pParam0, const _T1 _pParam1)
	{
		AddDraw(RCast<const RENDER_FUNC>(_pFunc), RCast<const void*>(_pParam0), RCast<const void*>(_pParam1));
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	描画
	static void		DrawFlush();

	//-------------------------------------------------------------------------------------------------
	//@brief	描画コマンド待機
	//@param[in]	_noWait			同期待ちするかどうか
	static b8		WaitDrawCommand(b8 _noWait = false);

	//-------------------------------------------------------------------------------------------------
	//@brief	スワップバッファ
	//@param[in]	_pTexture		スワップさせるテクスチャ(デフォルトは0番目レンダーターゲット)
	//@param[in]	_align			画面位置
	//@param[in]	_stretch		画面ストレッチ
	static void		SwapBuffer(const CTextureBuffer* _pTexture = NULL, SCREEN_ALIGN _align = SCREEN_ALIGN_CENTER_MIDDLE, SCREEN_STRETCH _stretch = SCREEN_STRETCH_FIT);

	//-------------------------------------------------------------------------------------------------
	//@brief	垂直同期
	//@param[in]	_syncInterval	垂直同期インターバル数(0=同期無し)
	static b8		WaitVsync(u32 _syncInterval);

	//-------------------------------------------------------------------------------------------------
	//@brief	型指定でインデックスをbit変換
	template<typename Type, typename... Types>
	static u32	RtClrBit(Type _rt, Types... _types)
	{
		return RtClrBitBase(_rt, _types...);
	}

#ifndef POISON_RELEASE
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	レンダラー描画プリミティブ数取得
	static u64	GetRenderPrimitiveCount(u32 _idx);

	//-------------------------------------------------------------------------------------------------
	//@brief	レンダラー描画時間取得
	static f64	GetRenderTimeMilliSec(u32 _idx);
#endif // !POISON_RELEASE


private:
	//-------------------------------------------------------------------------------------------------
	//@brief	型指定でインデックスをbit変換ベース
	template<typename Type>
	static u32	RtClrBitBase(Type _type)
	{
		return (1u << _type);
	}
	template<typename Type, typename... Types>
	static u32	RtClrBitBase(Type _type, Types... _types)
	{
		return RtClrBitBase(_types...) | RtClrBitBase(_type);
	}

};


POISON_END

#endif	// __GFX_UTILITY_H__

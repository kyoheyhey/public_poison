﻿//#pragma once
#ifndef __UNIFORM_CACHE_BUILDER_H__
#define __UNIFORM_CACHE_BUILDER_H__

//-------------------------------------------------------------------------------------------------
// include
#include "object/uniform_buffer.h"
#include "cache_list.h"


//-------------------------------------------------------------------------------------------------
// prototype
POISON_BGN


//*************************************************************************************************
//@brief	ユニフォームバッファキャッシュビルダー
//*************************************************************************************************
class CUniformCacheBuilder
{
private:
	//*************************************************************************************************
	//@brief	キャッシュオブジェクト
	//*************************************************************************************************
	struct CACHE_OBJECT
	{
		u32		GetNameCrc() const { return size; }
		void	SetNameCrc(u32 _nameCrc) { size = _nameCrc; }
		b8		IsUsed() const { return isUsed; }
		void	SetUsed(b8 _used) { isUsed = _used; }
		CUniformBuffer	object;
		u32				size = 0;
		b8				isUsed = false;
	};
	typedef CCacheSearchList<CACHE_OBJECT, u32>	CACHE_SEARCH_LIST;

public:
	CUniformCacheBuilder();
	~CUniformCacheBuilder();

	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	b8		Initialize(MEM_HANDLE _mem, u16 _num, GFX_BUFFER_ATTR _attr);

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	void	Finalize();

	//-------------------------------------------------------------------------------------------------
	//@brief	キャッシュビルド
	const CUniformBuffer*	Create(u32 _elemSize, u32 _elemNum, const void* _data = NULL, DRAW_CONTEXT* _pDrawContext = NULL);

	//-------------------------------------------------------------------------------------------------
	//@brief	キャッシュクリア
	void	Clear();


private:
	CACHE_SEARCH_LIST	m_cacheList;	///< キャッシュ検索リスト
	GFX_BUFFER_ATTR		m_attr;			///< バッファ属性

};


POISON_END

#endif	// __UNIFORM_CACHE_BUILDER_H__

﻿//#pragma once
#ifndef __CACHE_LIST_H__
#define __CACHE_LIST_H__

//-------------------------------------------------------------------------------------------------
// include
#include "collection/idx_sort_list.h"


POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype


//*************************************************************************************************
//@brief	ソートデータインターフェース
//*************************************************************************************************
template <typename __DATA, typename __KEY, b8 IS_ENABLE_SAME_NAME = true>
class CCacheSortDataIF
{
private:
	//-------------------------------------------------------------------------------------------------
	//@brief	型定義
	typedef	__DATA		DATA;
	typedef __KEY		KEY;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	検索用キー取得
	static inline	KEY		GetKey(const DATA& _data)			{ return _data.GetNameCrc(); }
	//-------------------------------------------------------------------------------------------------
	//@brief	検索用キーセット
	static inline	void	SetKey(DATA& _data, KEY _uName)		{ _data.SetNameCrc(_uName); }
	//-------------------------------------------------------------------------------------------------
	//@brief	データコピー
	static inline	void	Copy(DATA& _dst, const DATA& _src)	{ _dst = _src; }
	//-------------------------------------------------------------------------------------------------
	//@brief	未使用領域の初期化
	static inline	void	Clear(DATA& _data)					{ new(&_data) DATA(); }
	//-------------------------------------------------------------------------------------------------
	//@brief	同名登録の許可
	static inline b8		IsEnableSameName()					{ return IS_ENABLE_SAME_NAME; }
	//-------------------------------------------------------------------------------------------------
	//@brief	使用中かどうかフラグ取得
	static inline	b8		IsUsed(const DATA& _data)			{ return _data.IsUsed(); }
	//-------------------------------------------------------------------------------------------------
	//@brief	使用中かどうかフラグセット
	static inline	void	SetUsed(DATA& _data, b8 _used)		{ _data.SetUsed(_used); }
};

//*************************************************************************************************
//@brief	キャッシュ検索リスト
//*************************************************************************************************
template <typename __DATA, typename __KEY, b8 IS_ENABLE_SAME_NAME = true>
class CCacheSearchList : public CIdxSortList<__DATA, __KEY, CCacheSortDataIF<__DATA, __KEY, IS_ENABLE_SAME_NAME>>
{
private:
	//-------------------------------------------------------------------------------------------------
	//@brief	型定義
	typedef __DATA													DATA;
	typedef __KEY													KEY;
	typedef CCacheSortDataIF<__DATA, __KEY, IS_ENABLE_SAME_NAME>	INTERFACE;
	typedef CBaseIdxList											BASE;
	typedef CKeySort												SORT;
	typedef CIdxSortList											IDX_SORT_LIST;

public:
	CCacheSearchList() {}
	~CCacheSearchList() {}

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	キャッシュがあれば利用。なければ生成
	//@param[in]	_key		キー値
	DATA*	CacheAndCreate(const KEY& _key);

	//-------------------------------------------------------------------------------------------------
	//@brief	使用情報リセット
	void	UsedReset();

};


POISON_END

#include "cache_list.inl"

#endif	// __CACHE_LIST_H__

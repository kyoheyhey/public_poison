﻿//#pragma once
#ifndef __RASTERIZER_CACHE_BUILDER_H__
#define __RASTERIZER_CACHE_BUILDER_H__

//-------------------------------------------------------------------------------------------------
// include
#include "object/rasterrizer_state.h"

//-------------------------------------------------------------------------------------------------
// prototype
POISON_BGN


//*************************************************************************************************
//@brief	ラスタライザーキャッシュビルダー
//*************************************************************************************************
class CRasterizerCacheBuilder
{
private:
	//*************************************************************************************************
	//@brief	キャッシュオブジェクト
	//*************************************************************************************************
	struct CACHE_OBJECT
	{
		u32		GetNameCrc() const { return hash; }
		void	SetNameCrc(u32 _nameCrc) { hash = _nameCrc; }
		CRasterizer	object;
		hash32		hash = hash32::Invalid;
	};
	typedef CIdxSortList<CACHE_OBJECT, hash32, CSortDataIF<CACHE_OBJECT, hash32, false>>	CACHE_LIST;

public:
	CRasterizerCacheBuilder();
	~CRasterizerCacheBuilder();

	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	b8		Initialize(MEM_HANDLE _mem, u16 _num);

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	void	Finalize();

	//-------------------------------------------------------------------------------------------------
	//@brief	キャッシュビルド
	const CRasterizer*	Create(const RASTERIZER_DESC& _desc);

	//-------------------------------------------------------------------------------------------------
	//@brief	キャッシュクリア
	void	Clear();


private:
	CACHE_LIST		m_cacheList;	///< キャッシュリスト

};


POISON_END

#endif	// __RASTERIZER_CACHE_BUILDER_H__

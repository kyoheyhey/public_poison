﻿//#pragma once
#ifndef __DEPTH_STATE_CACHE_BUILDER_H__
#define __DEPTH_STATE_CACHE_BUILDER_H__

//-------------------------------------------------------------------------------------------------
// include
#include "object/depth_state.h"


//-------------------------------------------------------------------------------------------------
// prototype
POISON_BGN


//*************************************************************************************************
//@brief	デプスステンシルステートキャッシュビルダー
//*************************************************************************************************
class CDepthStateCacheBuilder
{
private:
	//*************************************************************************************************
	//@brief	キャッシュオブジェクト
	//*************************************************************************************************
	struct CACHE_OBJECT
	{
		u32		GetNameCrc() const { return hash; }
		void	SetNameCrc(u32 _nameCrc) { hash = _nameCrc; }
		CDepthState	object;
		hash32		hash = hash32::Invalid;
	};
	typedef CIdxSortList<CACHE_OBJECT, hash32, CSortDataIF<CACHE_OBJECT, hash32, false>>	CACHE_LIST;

public:
	CDepthStateCacheBuilder();
	~CDepthStateCacheBuilder();

	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	b8		Initialize(MEM_HANDLE _mem, u16 _num);

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	void	Finalize();

	//-------------------------------------------------------------------------------------------------
	//@brief	キャッシュビルド
	const CDepthState*	Create(const DEPTH_STATE_DESC& _desc);

	//-------------------------------------------------------------------------------------------------
	//@brief	キャッシュクリア
	void	Clear();


private:
	CACHE_LIST		m_cacheList;	///< キャッシュリスト

};


POISON_END

#endif	// __DEPTH_STATE_CACHE_BUILDER_H__

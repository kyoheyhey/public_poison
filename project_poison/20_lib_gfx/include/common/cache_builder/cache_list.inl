﻿//#pragma once
#ifndef __CACHE_LIST_INL__
#define __CACHE_LIST_INL__


POISON_BGN

///*************************************************************************************************
///	キャッシュ検索リスト
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// テンプレート宣言部分の省略
#define CACHE_LIST_TEMPLATE_HEAD	template <typename __DATA, typename __KEY, b8 IS_ENABLE_SAME_NAME>
#define CACHE_LIST_CLASS_HEAD		CCacheSearchList< __DATA, __KEY, IS_ENABLE_SAME_NAME >

///-------------------------------------------------------------------------------------------------
//@brief	キャッシュがあれば利用。なければ生成
//@param[in]	_key		キー値
CACHE_LIST_TEMPLATE_HEAD
typename CACHE_LIST_CLASS_HEAD::DATA*	CACHE_LIST_CLASS_HEAD::CacheAndCreate(const KEY& _key)
{
	DATA* pData = NULL;
	u16 sortIdx = 0;
	u16 useNum = IDX_SORT_LIST::GetUseNum();
	if (SORT::SearchFrontSortIdx(sortIdx, _key, useNum) && sortIdx < useNum)
	{
		// 同キーの末尾インデックスまで検索
		u16 endIdx = useNum - 1;
		while (sortIdx < endIdx && SORT::GetKey(sortIdx + 1) == _key)
		{
			DATA& rData = IDX_SORT_LIST::GetDataFromSortIdx(sortIdx);
			// 空きを見つけたら
			if (!INTERFACE::IsUsed(rData))
			{
				pData = &rData;
				break;
			}
			sortIdx++;
		}
	}
	if (pData == NULL && useNum < BASE::GetMaxNum())
	{
		ITR* pItr = IDX_SORT_LIST::PushBack(_key);
		pData = &IDX_SORT_LIST::GetDataFromItr(pItr);
	}

	if (pData)
	{
		// 使用フラグ設定
		INTERFACE::SetUsed(*pData, true);
	}
	return pData;
}

///-------------------------------------------------------------------------------------------------
//@brief	使用情報リセット
CACHE_LIST_TEMPLATE_HEAD
void	CACHE_LIST_CLASS_HEAD::UsedReset()
{
	for (u16 sortIdx = 0; sortIdx < GetUseNum(); ++sortIdx)
	{
		DATA& rData = IDX_SORT_LIST::GetDataFromSortIdx(sortIdx);
		INTERFACE::SetUsed(rData, false);
	}
}

POISON_END

#endif	// __CACHE_LIST_INL__

﻿//#pragma once
#ifndef __BLEND_STATE_CACHE_BUILDER_H__
#define __BLEND_STATE_CACHE_BUILDER_H__

//-------------------------------------------------------------------------------------------------
// include
#include "object/blend_state.h"


//-------------------------------------------------------------------------------------------------
// prototype
POISON_BGN


//*************************************************************************************************
//@brief	ブレンドステートキャッシュビルダー
//*************************************************************************************************
class CBlendStateCacheBuilder
{
private:
	//*************************************************************************************************
	//@brief	キャッシュオブジェクト
	//*************************************************************************************************
	struct CACHE_OBJECT
	{
		u32		GetNameCrc() const { return hash; }
		void	SetNameCrc(u32 _nameCrc) { hash = _nameCrc; }
		CBlendState	object;
		hash32		hash = hash32::Invalid;
	};
	typedef CIdxSortList<CACHE_OBJECT, hash32, CSortDataIF<CACHE_OBJECT, hash32, false>>	CACHE_LIST;

public:
	CBlendStateCacheBuilder();
	~CBlendStateCacheBuilder();

	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	b8		Initialize(MEM_HANDLE _mem, u16 _num);

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	void	Finalize();

	//-------------------------------------------------------------------------------------------------
	//@brief	キャッシュビルド
	const CBlendState*	Create(const BLEND_STATE_DESC& _desc);

	//-------------------------------------------------------------------------------------------------
	//@brief	キャッシュクリア
	void	Clear();


private:
	CACHE_LIST		m_cacheList;	///< キャッシュリスト

};

POISON_END

#endif	// __BLEND_STATE_CACHE_BUILDER_H__

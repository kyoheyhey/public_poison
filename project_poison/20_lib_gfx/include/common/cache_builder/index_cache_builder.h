﻿//#pragma once
#ifndef __INDEX_CACHE_BUILDER_H__
#define __INDEX_CACHE_BUILDER_H__

//-------------------------------------------------------------------------------------------------
// include
#include "object/index_buffer.h"
#include "cache_list.h"


//-------------------------------------------------------------------------------------------------
// prototype
POISON_BGN


//*************************************************************************************************
//@brief	
//*************************************************************************************************
class CIndexCacheBuilder
{
private:
	//*************************************************************************************************
	//@brief	キャッシュオブジェクト
	//*************************************************************************************************
	struct CACHE_OBJECT
	{
		u32		GetNameCrc() const { return size; }
		void	SetNameCrc(u32 _nameCrc) { size = _nameCrc; }
		b8		IsUsed() const { return isUsed; }
		void	SetUsed(b8 _used) { isUsed = _used; }
		CIndexBuffer	object;
		u32				size = 0;
		b8				isUsed = false;
	};
	typedef CCacheSearchList<CACHE_OBJECT, u32>	CACHE_SEARCH_LIST;

public:
	CIndexCacheBuilder();
	~CIndexCacheBuilder();

	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	b8		Initialize(MEM_HANDLE _mem, u16 _num);

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	void	Finalize();

	//-------------------------------------------------------------------------------------------------
	//@brief	バッファ生成
	const CIndexBuffer*	Create(u32 _elemSize, u32 _elemNum, const void* _data = NULL, DRAW_CONTEXT* _pDrawContext = NULL);

	//-------------------------------------------------------------------------------------------------
	//@brief	キャッシュリセット
	void	Clear();


private:
	CACHE_SEARCH_LIST	m_cacheList;	///< キャッシュ検索リスト
};


POISON_END

#endif	// __INDEX_CACHE_BUILDER_H__

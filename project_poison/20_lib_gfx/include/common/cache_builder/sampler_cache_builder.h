﻿//#pragma once
#ifndef __SAMPLER_CACHE_BUILDER_H__
#define __SAMPLER_CACHE_BUILDER_H__

//-------------------------------------------------------------------------------------------------
// include
#include "object/sampler_state.h"


//-------------------------------------------------------------------------------------------------
// prototype
POISON_BGN


//*************************************************************************************************
//@brief	サンプラーキャッシュビルダー
//*************************************************************************************************
class CSamplerCacheBuilder
{
private:
	//*************************************************************************************************
	//@brief	キャッシュオブジェクト
	//*************************************************************************************************
	struct CACHE_OBJECT
	{
		u32		GetNameCrc() const { return hash; }
		void	SetNameCrc(u32 _nameCrc) { hash = _nameCrc; }
		CSampler	object;
		hash32		hash = hash32::Invalid;
	};
	typedef CIdxSortList<CACHE_OBJECT, hash32, CSortDataIF<CACHE_OBJECT, hash32, false>>	CACHE_LIST;

public:
	CSamplerCacheBuilder();
	~CSamplerCacheBuilder();

	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	b8		Initialize(MEM_HANDLE _mem, u16 _num);

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	void	Finalize();

	//-------------------------------------------------------------------------------------------------
	//@brief	キャッシュビルド
	const CSampler*		Create(const SAMPLER_DESC& _desc, DRAW_CONTEXT* _pDrawContext = NULL);

	//-------------------------------------------------------------------------------------------------
	//@brief	キャッシュクリア
	void	Clear();


private:
	CACHE_LIST		m_cacheList;	///< キャッシュリスト

};

POISON_END

#endif	// __SAMPLER_CACHE_BUILDER_H__

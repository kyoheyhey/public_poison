﻿//#pragma once
#ifndef __GFX_DEVICE_H__
#define __GFX_DEVICE_H__

//-------------------------------------------------------------------------------------------------
// include
#include "format/gfx_format.h"
#include "object/gfx_object.h"


//-------------------------------------------------------------------------------------------------
// prototype
POISON_BGN
class CTextureBuffer;


//*************************************************************************************************
//@brief	キャンパス宣言構造体
//*************************************************************************************************
struct CANVAS_DESC
{
	u32					width = 0;
	u32					height = 0;
	GFX_IMAGE_FORMAT	clrFormat = GFX_FORMAT_R8G8B8A8_UNORM;
	GFX_IMAGE_FORMAT	depFormat = GFX_FORMAT_UNKNOWN;
	GFX_ANTI_ALIASE		aa = GFX_AA_UNKNOWN;
	u32					swapNum = 0;
	u32					refreshRate = 60;
	CVec4				clearColor = CVec4::Black;
};

//*************************************************************************************************
//@brief	GFXデバイス
//*************************************************************************************************
class CGfxDevice
{
public:
	CGfxDevice();
	virtual ~CGfxDevice();

	//-------------------------------------------------------------------------------------------------
	//@brief	オブジェクト取得
	SWAP_OBJECT&		GetObject()	{ return m_object; }
	const SWAP_OBJECT&	GetObject() const { return CCast<CGfxDevice*>(this)->GetObject(); }

	//-------------------------------------------------------------------------------------------------
	//@brief	描画コンテキスト取得
	DRAW_CONTEXT*		GetContext() const { return m_pDrawContext; }

	//-------------------------------------------------------------------------------------------------
	//@brief	各種取得
	u32				GetWidth() const { return m_width; }
	u32				GetHeight() const { return m_height; }
	u32				GetSwapNum() const { return m_swapNum; }
	u32				GetRefrashRate() const { return m_refreshRate; }
	const CRect&	GetViewport() const { return m_viewport; }
	const CVec4&	GetClearColor() const { return m_clearClr; }

	//-------------------------------------------------------------------------------------------------
	//@brief	各種設定
	void			SetRefrashRate(u32 _refrashRate) { m_refreshRate = m_refreshRate; }
	void			SetClearColor(const CVec4& _clearClr) { m_clearClr = _clearClr; }
	
	//-------------------------------------------------------------------------------------------------
	//@brief	キャンパス生成
	//@param[in]	_desc		構築情報
	virtual b8		CreateCanvas(const CANVAS_DESC& _desc) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	キャンパスバインド
	virtual void	BindCanvas() = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	描画開始
	virtual void	DrawBegin() = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	描画終了
	virtual void	DrawEnd() = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	クリアサーフェイス
	//@param[in]	_clearClr	クリアカラー
	virtual void	ClearSurface(const CVec4& _clearClr) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	スワップバッファ
	//@param[in]	_pTexture	テクスチャ
	//@param[in]	_align		画面位置
	//@param[in]	_stretch	画面ストレッチ
	virtual void	SwapBuffer(const CTextureBuffer* _pTexture, SCREEN_ALIGN _align = SCREEN_ALIGN_CENTER_MIDDLE, SCREEN_STRETCH _stretch = SCREEN_STRETCH_FIT) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	垂直同期
	virtual b8		WaitVsync(u32 _syncInterval) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	ターゲットのリサイズ
	//@param[in]	_width		幅
	//@param[in]	_height		高さ
	virtual b8		ResizeTarget(u32 _width, u32 _height) = 0;

	//-------------------------------------------------------------------------------------------------
	//@brief	スクリーンビューポート更新
	//@param[in]	_width		幅
	//@param[in]	_height		高さ
	//@param[in]	_align		画面位置
	//@param[in]	_stretch	画面ストレッチ
	//@return					更新したか
	b8		UpdateScreenViewprt(u32 _width, u32 _height, SCREEN_ALIGN _align, SCREEN_STRETCH _stretch);


protected:
	SWAP_OBJECT		m_object;		///< スワップオブジェクト
	DRAW_CONTEXT*	m_pDrawContext;	///< 描画コンテキスト
	CRect			m_viewport;		///< ビューポート
	CVec4			m_clearClr;		///< クリアカラー
	u32				m_width;		///< 幅
	u32				m_height;		///< 高さ
	u32				m_swapNum;		///< スワップ数
	u32				m_refreshRate;	///< フレッシュレート
};


POISON_END

#endif	// __GFX_DEVICE_H__

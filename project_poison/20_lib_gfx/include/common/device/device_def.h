﻿//#pragma once
#ifndef __DEVICE_DEF_H__
#define __DEVICE_DEF_H__


//-------------------------------------------------------------------------------------------------
// define
#define USE_SUBDATA_BUFFER


//-------------------------------------------------------------------------------------------------
// include


//-------------------------------------------------------------------------------------------------
// prototype


POISON_BGN


//*************************************************************************************************
//@brief	描画コンテキスト
//*************************************************************************************************
#if defined( LIB_GFX_OPENGL )
typedef	CDrawContext		DRAW_CONTEXT;
#endif // LIB_GFX_OPENGL
#if defined( LIB_GFX_DX11 )
typedef	ID3D11DeviceContext	DRAW_CONTEXT;
#endif // LIB_GFX_DX11


POISON_END

#endif	// __DEVICE_DEF_H__

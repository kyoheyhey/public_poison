﻿//#pragma once
#ifndef __DISPLAY_H__
#define __DISPLAY_H__

//-------------------------------------------------------------------------------------------------
// include
#include "renderer/render_target.h"


POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype



//*************************************************************************************************
//@brief	ディスプレイ宣言構造体
//*************************************************************************************************
struct DISPLAY_DESC
{
	const c8*	name = NULL;
	MEM_HANDLE	memHdl = MEM_HDL_INVALID;
	u16			width = 0;
	u16			height = 0;
	COLOR_DESC	color;
	u32			swapNum = 0;
	DEPTH_DESC*	pDepth;
	u32			refreshRate = 60;
};

//*************************************************************************************************
//@brief	ディスプレイクラス
//*************************************************************************************************
class CDisplay : public CRenderTarget
{
public:
	static const u32	MAX_RT_NUM = RENDER_BUFFER_SLOT_NUM;		// 最大スワップバッファ数

public:
	CDisplay();
	virtual ~CDisplay();

	//-------------------------------------------------------------------------------------------------
	//@brief	オブジェクト取得
	SWAP_OBJECT&		GetObject()	{ return m_object; }
	const SWAP_OBJECT&	GetObject() const { return CCast<CDisplay*>(this)->GetObject(); }

	//-------------------------------------------------------------------------------------------------
	//@brief	バインドするかどうか
	virtual b8		IsBind() const override;

	//-------------------------------------------------------------------------------------------------
	//@brief	生成
	b8		Create(const DISPLAY_DESC& _desc);

	//-------------------------------------------------------------------------------------------------
	//@brief	破棄
	virtual void	Release() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	ターゲットのリサイズ
	//@param[in]	_width	幅
	//@param[in]	_height	高さ
	virtual b8		ResizeTarget(u16 _width, u16 _height) override;

	//-------------------------------------------------------------------------------------------------
	//@brief	垂直同期
	b8		WaitVsync(u32 _syncInterval);

protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	スワップバッファ生成
	b8		CreateSwap(CColorBuffer* _pRetClrBuf, u16 _width, u16 _height, const COLOR_DESC& _clrDesc, u32 _swapNum, u32 _refreshRate);

	//-------------------------------------------------------------------------------------------------
	//@brief	カラーテクスチャバッファ取得を無効化
	CTextureBuffer*			GetColorTexture(u32 _idx) { return NULL; }
	const CTextureBuffer*	GetColorTexture(u32 _idx) const	{ return CCast<CDisplay*>(this)->GetColorTexture(_idx); }

protected:
	SWAP_OBJECT		m_object;			///< スワップオブジェクト
	u32				m_refreshRate;		///< デフォルトフレッシュレート
};


POISON_END

#endif	// __DISPLAY_H__

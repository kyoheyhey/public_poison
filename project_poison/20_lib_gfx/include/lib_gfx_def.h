﻿//#pragma once
#ifndef __LIB_GFX_DEF_H__
#define __LIB_GFX_DEF_H__


//-------------------------------------------------------------------------------------------------
// include
#include "gfx_def_type.h"

#include "renderer/renderer.h"

#include "device/gfx_device.h"
#include "utility/gfx_utility.h"

#endif	// __LIB_GFX_DEF_H__

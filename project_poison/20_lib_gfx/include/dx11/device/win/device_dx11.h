﻿//#pragma once
#ifndef	__DEVICE_DX11_H__
#define	__DEVICE_DX11_H__

//-------------------------------------------------------------------------------------------------
// include
#include "device/device_win.h"
#include "device/gfx_device.h"


POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype
class CColorBuffer;
class CDepthBuffer;


///*************************************************************************************************
//@brief	グラフィックデバイスクラス
///*************************************************************************************************
class CDeviceDX11 : public CDeviceWin, public CGfxDevice
{
public:
	CDeviceDX11();
	virtual ~CDeviceDX11();

	//-------------------------------------------------------------------------------------------------
	//@brief	初期化開始
	virtual b8		InitBegin(MEM_HANDLE _memHdl) override;

	//-------------------------------------------------------------------------------------------------
	//@brief	初期化終了
	virtual b8		InitEnd() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	virtual void	Finalize() override;


public:
	//-------------------------------------------------------------------------------------------------
	//@brief	キャンパス生成
	virtual b8		CreateCanvas(const CANVAS_DESC& _desc) override;

	//-------------------------------------------------------------------------------------------------
	//@brief	キャンパスバインド
	virtual void	BindCanvas() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	クリアサーフェイス
	virtual void	ClearSurface(const CVec4& _clearClr) override;

	//-------------------------------------------------------------------------------------------------
	//@brief	描画開始
	virtual void	DrawBegin() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	描画終了
	virtual void	DrawEnd() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	スワップバッファ
	virtual void	SwapBuffer(const CTextureBuffer* _pTexture, SCREEN_ALIGN _align = SCREEN_ALIGN_CENTER_MIDDLE, SCREEN_STRETCH _stretch = SCREEN_STRETCH_FIT) override;

	//-------------------------------------------------------------------------------------------------
	//@brief	垂直同期
	virtual b8		WaitVsync(u32 _syncInterval) override;

	//-------------------------------------------------------------------------------------------------
	//@brief	ターゲットのリサイズ
	//@param[in]	_width	幅
	//@param[in]	_height	高さ
	virtual b8		ResizeTarget(u32 _width, u32 _height) override;


public:
	//-------------------------------------------------------------------------------------------------
	//@brief	ファクトリー
	IDXGIFactory*	GetFactory() { return m_pDXGIFactory; }

	//-------------------------------------------------------------------------------------------------
	//@brief	アダプター
	IDXGIAdapter*	GetAdapter() { return m_pDXGIAdapter; }

	//-------------------------------------------------------------------------------------------------
	//@brief	デバイス
	ID3D11Device*	GetDevice() { return m_pD3D11Device; }

	//-------------------------------------------------------------------------------------------------
	//@brief	ディファードコンテキスト生成
	//@note		外部で必ず破棄してください。
	ID3D11DeviceContext*	CDeviceDX11::CreateDeferredContext();

	//-------------------------------------------------------------------------------------------------
	//@brief	マルチサンプリングクオリティレベルチェック
	void	CheckMultisampleQualityLevels(DXGI_SAMPLE_DESC& _retDesc, DXGI_FORMAT _format, u32 _aa);

#ifndef POISON_RELEASE
	//-------------------------------------------------------------------------------------------------
	//@brief	PrintError
	static void	PrintError(const HRESULT& _result, const c8* _pFuncName, const c8* _format = NULL);
#endif // !POISON_RELEASE

private:
	//-------------------------------------------------------------------------------------------------
	//@brief	GPUメモリチェック
	void	CheckGPU();

	//-------------------------------------------------------------------------------------------------
	//@brief	DirectX11の初期化
	b8		InitDX11();

	//-------------------------------------------------------------------------------------------------
	//@brief	スワップバッファ生成
	b8		CreateSwap(CColorBuffer* _pRetClrBuf, u16 _width, u16 _height, GFX_IMAGE_FORMAT _format, GFX_ANTI_ALIASE _aa, u32 _swapNum, u32 _refreshRate);


private:
	// DXGI interfaces
	IDXGIFactory*			m_pDXGIFactory;
	IDXGIAdapter*			m_pDXGIAdapter;
	IDXGIDevice*			m_pDXGIDevice;

	// D3D11
	ID3D11Device*			m_pD3D11Device;
	ID3D11DeviceContext*	m_pD3D11ImmediateContext;	// Immediate context
	
	// D3D11_1
	ID3D11Device1*			m_pD3D11Device1;
	ID3D11DeviceContext1*	m_pD3D11ImmediateContext1;	// Immediate context

	// D3D11_2
	ID3D11Device2*			m_pD3D11Device2;
	ID3D11DeviceContext2*	m_pD3D11ImmediateContext2;	// Immediate context

	// D3D11_3
	ID3D11Device3*			m_pD3D11Device3;
	ID3D11DeviceContext3*	m_pD3D11ImmediateContext3;	// Immediate context

	CColorBuffer*			m_pColor;	///< キャンパスカラーバッファ
	CDepthBuffer*			m_pDepth;	///< キャンパスデプスバッファ
};

POISON_END


#endif	// __DEVICE_DX11_H__

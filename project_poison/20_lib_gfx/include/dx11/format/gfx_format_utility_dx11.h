﻿//#pragma once
#ifndef __GFX_FORMAT_UTILITY_DX11_H__
#define __GFX_FORMAT_UTILITY_DX11_H__

//-------------------------------------------------------------------------------------------------
// include
#include "format/gfx_format.h"


POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype
struct VTX_DECL;


//*************************************************************************************************
//@brief	グラフィクスフォーマットユーティリティDX11
//*************************************************************************************************
class CGfxFormatUtilityDX11
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	データアクセス変換
	static void		ConvertUsage(D3D11_USAGE& _retUsage, GFX_USAGE _usage);

	//-------------------------------------------------------------------------------------------------
	//@brief	頂点用データアクセス変換
	static void		ConvertVertexUsage(
		D3D11_USAGE& _retUsage, D3D11_BIND_FLAG& _retBind, D3D11_CPU_ACCESS_FLAG& _retCPU, D3D11_RESOURCE_MISC_FLAG& _retMisc,
		GFX_USAGE _usage
		);

	//-------------------------------------------------------------------------------------------------
	//@brief	インデックス用データアクセス変換
	static void		ConvertIndexUsage(
		D3D11_USAGE& _retUsage, D3D11_BIND_FLAG& _retBind, D3D11_CPU_ACCESS_FLAG& _retCPU, D3D11_RESOURCE_MISC_FLAG& _retMisc,
		GFX_USAGE _usage
		);

	//-------------------------------------------------------------------------------------------------
	//@brief	ユニフォーム用データアクセス変換
	static void		ConvertUniformUsage(
		D3D11_USAGE& _retUsage, D3D11_BIND_FLAG& _retBind, D3D11_CPU_ACCESS_FLAG& _retCPU, D3D11_RESOURCE_MISC_FLAG& _retMisc,
		GFX_USAGE _usage, GFX_BUFFER_ATTR _attr
		);

	//-------------------------------------------------------------------------------------------------
	//@brief	テクスチャ用データアクセス変換
	static void		ConvertTextureUsage(
		D3D11_USAGE& _retUsage, D3D11_BIND_FLAG& _retBind, D3D11_CPU_ACCESS_FLAG& _retCPU, D3D11_RESOURCE_MISC_FLAG& _retMisc,
		GFX_USAGE _usage
		);

	//-------------------------------------------------------------------------------------------------
	//@brief	カラーバッファ用データアクセス変換
	static void		ConvertColorBufferUsage(
		D3D11_USAGE& _retUsage, D3D11_BIND_FLAG& _retBind, D3D11_CPU_ACCESS_FLAG& _retCPU, D3D11_RESOURCE_MISC_FLAG& _retMisc,
		GFX_USAGE _usage, b8 _isMSAA
		);

	//-------------------------------------------------------------------------------------------------
	//@brief	デプスバッファ用データアクセス変換
	static void		ConvertDepthBufferUsage(
		D3D11_USAGE& _retUsage, D3D11_BIND_FLAG& _retBind, D3D11_CPU_ACCESS_FLAG& _retCPU, D3D11_RESOURCE_MISC_FLAG& _retMisc,
		GFX_USAGE _usage, b8 _isMSAA
		);

	//-------------------------------------------------------------------------------------------------
	//@brief	フォーマット変換
	static void		ConvertFormat(DXGI_FORMAT& _retFormat, GFX_IMAGE_FORMAT _format);
	static void		ConvertFormat(GFX_IMAGE_FORMAT& _retFormat, DXGI_FORMAT _format);

	//-------------------------------------------------------------------------------------------------
	//@brief	サンプラーラップ変換
	static void		ConvertSamplerWrap(D3D11_TEXTURE_ADDRESS_MODE& _retWrap, GFX_SAMPLER_WRAP _wrap);

	//-------------------------------------------------------------------------------------------------
	//@brief	サンプラーフィルター変換
	static void		ConvertSamplerFilter(D3D11_FILTER& _retFilter, GFX_SAMPLER_FILTER _min, GFX_SAMPLER_FILTER _mag, GFX_SAMPLER_FILTER _mip, b8 _isAniso);

	//-------------------------------------------------------------------------------------------------
	//@brief	比較式変換
	static void		ConvertTestFunc(D3D11_COMPARISON_FUNC& _retTest, GFX_TEST _test);

	//-------------------------------------------------------------------------------------------------
	//@brief	頂点レイアウト変換
	static void		ConvertVertexDecl(D3D11_INPUT_ELEMENT_DESC(&_retDecl)[VERTEX_ATTRIBUTE_TYPE_NUM], const VTX_DECL* _pDecl, u32 _num);

	//-------------------------------------------------------------------------------------------------
	//@brief	プリミティブ変換
	static void		ConvertPrimitive(D3D11_PRIMITIVE_TOPOLOGY& _retPrim, GFX_PRIMITIVE _prim);

	//-------------------------------------------------------------------------------------------------
	//@brief	カリング面変換
	static void		ConvertCullFace(D3D11_CULL_MODE& _retFace, GFX_CULL_FACE _face);

	//-------------------------------------------------------------------------------------------------
	//@brief	ブレンドオペレーション変換
	static void		ConvertBlendOp(D3D11_BLEND_OP& _retBlendOp, GFX_BLEND_OP _blendOp);

	//-------------------------------------------------------------------------------------------------
	//@brief	ブレンド式変換
	static void		ConvertBlendFunc(D3D11_BLEND& _retBlendFunc, GFX_BLEND_FUNC _blendFunc);

	//-------------------------------------------------------------------------------------------------
	//@brief	カラーマスク変換
	static void		ConvertColorMask(D3D11_COLOR_WRITE_ENABLE& _retColorMask, GFX_COLOR_MASK _colorMask);

	//-------------------------------------------------------------------------------------------------
	//@brief	ステンシルオペレーション変換
	static void		ConvertStencilOp(D3D11_STENCIL_OP& _retStencilOp, GFX_STENCIL_OP _stencilOp);

};



POISON_END

#endif	// __GFX_FORMAT_UTILITY_DX11_H__

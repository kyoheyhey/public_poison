﻿//#pragma once
#ifndef __DEF_DX11_H__
#define __DEF_DX11_H__


//-------------------------------------------------------------------------------------------------
// pragma
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")


//-------------------------------------------------------------------------------------------------
// include
#include <dxgi.h>
#include <d3dCommon.h>
#include <d3d11.h>
#include <d3d11_1.h>
#include <d3d11_2.h>
#include <d3d11_3.h>
#include <d3d11shader.h>


// undef（ファッキンDirectX）
#undef INTERFACE


#endif	// __DEF_DX11_H__

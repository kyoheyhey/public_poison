﻿//#pragma once
#ifndef __GFX_FORMAT_UTILITY_GL_H__
#define __GFX_FORMAT_UTILITY_GL_H__

//-------------------------------------------------------------------------------------------------
// include
#include "format/gfx_format.h"


POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype
struct VTX_DECL;


//*************************************************************************************************
//@brief	グラフィクスフォーマットユーティリティGL
//*************************************************************************************************
class CGfxFormatUtilityGL
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	論理型変換
	static void		ConvertBool(u32& _retBool, b8 _bool);

	//-------------------------------------------------------------------------------------------------
	//@brief	データアクセス変換
	static void		ConvertUsage(u32& _retUsage, GFX_USAGE _usage);

	//-------------------------------------------------------------------------------------------------
	//@brief	バッファ属性変換
	static void		ConvertBufferAttr(u32& _retAttr, GFX_BUFFER_ATTR _attr);

	//-------------------------------------------------------------------------------------------------
	//@brief	値タイプ変換
	static void		ConvertValueType(u32& _retValType, GFX_VALUE _valType);

	//-------------------------------------------------------------------------------------------------
	//@brief	フォーマット変換
	static void		ConvertFormat(u32& _retFormat, GFX_IMAGE_FORMAT _format);
	static void		ConvertFormat(GFX_IMAGE_FORMAT& _retFormat, u32 _format);

	//-------------------------------------------------------------------------------------------------
	//@brief	フォーマットからテクスチャフォーマット変換
	static void		ConvertFormatToTexFormat(u32& _retTexFormat, GFX_IMAGE_FORMAT _format);

	//-------------------------------------------------------------------------------------------------
	//@brief	フォーマットから値タイプ変換
	static void		ConvertFormatToValueType(u32& _retValType, GFX_IMAGE_FORMAT _format);

	//-------------------------------------------------------------------------------------------------
	//@brief	テクスチャタイプ変換
	static void		ConvertTextureType(u32& _retTexType, GFX_TEXTURE_TYPE _texType);

	//-------------------------------------------------------------------------------------------------
	//@brief	キューブマップ面変換
	static void		ConvertCubemapFace(u32& _retCubemapFace, GFX_CUBEMAP_FACE _cubemapFace);

	//-------------------------------------------------------------------------------------------------
	//@brief	サンプラーラップ変換
	static void		ConvertSamplerWrap(u32& _retWrap, GFX_SAMPLER_WRAP _wrap);

	//-------------------------------------------------------------------------------------------------
	//@brief	サンプラーフィルター変換
	static void		ConvertSamplerFilter(u32& _retMinFilter, u32& _retMaxFilter, GFX_SAMPLER_FILTER _min, GFX_SAMPLER_FILTER _mag, GFX_SAMPLER_FILTER _mip);

	//-------------------------------------------------------------------------------------------------
	//@brief	プリミティブ変換
	static void		ConvertPrimitive(u32& _retPrim, GFX_PRIMITIVE _prim);

	//-------------------------------------------------------------------------------------------------
	//@brief	前面変換
	static void		ConvertFrontFace(u32& _retFace, GFX_FRONT_FACE _face);

	//-------------------------------------------------------------------------------------------------
	//@brief	カリング面変換
	static void		ConvertCullFace(u32& _retFace, GFX_CULL_FACE _face);

	//-------------------------------------------------------------------------------------------------
	//@brief	比較式変換
	static void		ConvertTestFunc(u32& _retTest, GFX_TEST _test);

	//-------------------------------------------------------------------------------------------------
	//@brief	ブレンドオペレーション変換
	static void		ConvertBlendOp(u32& _retBlendOp, GFX_BLEND_OP _blendOp);

	//-------------------------------------------------------------------------------------------------
	//@brief	ブレンド式変換
	static void		ConvertBlendFunc(u32& _retBlendFunc, GFX_BLEND_FUNC _blendFunc);

	//-------------------------------------------------------------------------------------------------
	//@brief	カラーマスク変換
	static void		ConvertColorMask(b8 (&_rgba)[4], GFX_COLOR_MASK _colorMask);

	//-------------------------------------------------------------------------------------------------
	//@brief	ステンシルオペレーション変換
	static void		ConvertStencilOp(u32& _retStencilOp, GFX_STENCIL_OP _stencilOp);

	//-------------------------------------------------------------------------------------------------
	//@brief	クリアレンダーターゲットビット変換
	static void		ConvertClearRenderTargetBit(u32& _retClearRtBit, RENDER_BUFFER_BIT _rtBit);

	//-------------------------------------------------------------------------------------------------
	//@brief	レンダーターゲットスロット変換
	static void		ConvertRenderTargetSlot(u32& _retRtSlot, u32 _rtSlot);

	//-------------------------------------------------------------------------------------------------
	//@brief	テクスチャスロット変換
	static void		ConvertTextureSlot(u32& _retTexSlot, u32 _texSlot);

	//-------------------------------------------------------------------------------------------------
	//@brief	シェーダタイプ変換
	static void		ConvertShaderType(u32& _retShaderType, GFX_SHADER _shaderType);

	//-------------------------------------------------------------------------------------------------
	//@brief	頂点シェーダロケーション名取得
	static const c8*	GetVertexLocationName(VERTEX_ATTRIBUTE _vtxAttr);

	//-------------------------------------------------------------------------------------------------
	//@brief	フラグメントシェーダロケーション名取得
	static const c8*	GetFragmentLocationName(RENDER_BUFFER_SLOT _outSlot);

	//-------------------------------------------------------------------------------------------------
	//@brief	シェーダユニフォームロケーション名取得
	static const c8*	GetUniformLocationName(UNIFORM_SLOT _uniSlot);

	//-------------------------------------------------------------------------------------------------
	//@brief	シェーダテクスチャロケーション名取得
	static const c8*	GetTextureLocationName(TEXTURE_SLOT _texSlot);
};


POISON_END

#endif	// __GFX_FORMAT_UTILITY_GL_H__

﻿//#pragma once
#ifndef __IMMEDIATE_DRAW_CONTEXT_H__
#define __IMMEDIATE_DRAW_CONTEXT_H__

//-------------------------------------------------------------------------------------------------
// include
#include "draw_context.h"

//-------------------------------------------------------------------------------------------------
// prototype


POISON_BGN


//*************************************************************************************************
//@brief	描画コンテキスト
//*************************************************************************************************
class CImmediateContext : public CDrawContext
{
public:
	CImmediateContext();
	virtual ~CImmediateContext();

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	描画コマンド関数
	virtual void	Enable(u32 _cap) override;
	virtual void	Enablei(u32 _slot, u32 _cap) override;
	virtual void	Disable(u32 _cap) override;
	virtual void	Disablei(u32 _slot, u32 _cap) override;

	virtual void	BindBuffer(u32 _target, const u32* _pObjectID) override;
	virtual void	BindBufferBase(u32 _target, u32 _slot, const u32* _pObjectID) override;
	virtual void	BindFramebuffer(u32 _target, const u32* _pObjectID) override;
	virtual void	BindRenderbuffer(u32 _target, const u32* _pObjectID) override;
	virtual void	BindTexture(u32 _target, const u32* _pObjectID) override;
	virtual void	ActiveTexture(u32 _slot) override;
	virtual void	BindSampler(u32 _slot, const u32* _pObjectID) override;

	virtual void	UnBindBuffer(u32 _target) override;
	virtual void	UnBindFramebuffer(u32 _target) override;
	virtual void	UnBindRenderbuffer(u32 _target) override;
	virtual void	UnBindTexture(u32 _target) override;

	virtual void	GenBuffers(u32 _num, u32* _pObjectID) override;
	virtual void	GenFramebuffers(u32 _num, u32* _pObjectID) override;
	virtual void	GenRenderbuffers(u32 _num, u32* _pObjectID) override;
	virtual void	GenTextures(u32 _num, u32* _pObjectID) override;
	virtual void	GenSamplers(u32 _num, u32* _pObjectID) override;
	virtual void	GenQueries(u32 _num, u32* _pQueryID) override;

	virtual void	DeleteBuffers(u32 _num, u32* _pObjectID) override;
	virtual void	DeleteFramebuffers(u32 _num, u32* _pObjectID) override;
	virtual void	DeleteRenderbuffers(u32 _num, u32* _pObjectID) override;
	virtual void	DeleteTextures(u32 _num, u32* _pObjectID) override;
	virtual void	DeleteSamplers(u32 _num, u32* _pObjectID) override;
	virtual void	DeleteQueries(u32 _num, u32* _pQueryID) override;

	virtual void	BufferData(u32 _target, u32 _size, const void* _pData, u32 _usage) override;
	virtual void	RenderbufferStorage(u32 _target, u32 _format, u32 _width, u32 _height) override;
	virtual void	RenderbufferStorageMultisample(u32 _target, u32 _aa, u32 _format, u32 _width, u32 _height) override;
	virtual void	TexImage2D(u32 _target, u32 _level, u32 _format, u32 _width, u32 _height, u32 _texFormat, u32 _valType, const void* _pData) override;
	virtual void	TexImage2DMultisample(u32 _target, u32 _aa, u32 _format, u32 _width, u32 _height) override;
	virtual void	TexImage3D(u32 _target, u32 _level, u32 _format, u32 _width, u32 _height, u32 _depth, u32 _texFormat, u32 _valType, const void* _pData) override;
	virtual void	TexStorage2D(u32 _target, u32 _level, u32 _format, u32 _width, u32 _height) override;
	virtual void	TexStorage2DMultisample(u32 _target, u32 _aa, u32 _format, u32 _width, u32 _height) override;
	virtual void	TexStorage3D(u32 _target, u32 _level, u32 _format, u32 _width, u32 _height, u32 _depth) override;

	virtual void	MapBuffer(u32 _target, u32 _size, const void* _pData) override;
	virtual void	TexSubImage2D(u32 _target, u32 _level, u32 _xoff, u32 _yoff, u32 _mipWidth, u32 _mipHeight, u32 _texFormat, u32 _valType, const void* _pData) override;
	virtual void	TexSubImage3D(u32 _target, u32 _level, u32 _xoff, u32 _yoff, u32 _zoff, u32 _mipWidth, u32 _mipHeight, u32 _mipDepth, u32 _texFormat, u32 _valType, const void* _pData) override;

	virtual void	CheckFramebufferStatus(const u32* _pObjectID) override;
	virtual void	GetTexLevelParameteriv(u32 _target, u32 _level, u32 _name, s32* _pParam) override;
	virtual void	GetQueryObjectiv(const u32* _pQueryID, u32 _name, s32* _pParam) override;
	virtual void	GetQueryObjectuiv(const u32* _pQueryID, u32 _name, u32* _pParam) override;
	virtual void	GetQueryObjecti64v(const u32* _pQueryID, u32 _name, s64* _pParam) override;
	virtual void	GetQueryObjectui64v(const u32* _pQueryID, u32 _name, u64* _pParam) override;

	virtual void	FramebufferRenderbuffer(u32 _frameTrg, u32 _slot, u32 _colorTrg, const u32* _pObjectID) override;
	virtual void	FramebufferTexture2D(u32 _frameTrg, u32 _slot, u32 _textureTrg, const u32* _pObjectID, u32 _level) override;
	virtual void	DrawBuffers(u32 _num, const u32* _pColorTrg) override;

	virtual void	ClearBufferColor(f32 _r, f32 _g, f32 _b, f32 _a) override;
	virtual void	ClearBufferDepth(f32 _depth) override;
	virtual void	ClearBufferStencil(s32 _stencil) override;
	virtual void	ClearColor(f32 _r, f32 _g, f32 _b, f32 _a) override;
	virtual void	ClearDepth(f32 _depth) override;
	virtual void	ClearStencil(s32 _stencil) override;
	virtual void	Clear(u32 _bit) override;

	virtual void	Viewport(s32 _posX, s32 _posY, u32 _width, u32 _height) override;
	virtual void	Scissor(s32 _posX, s32 _posY, u32 _width, u32 _height) override;
	virtual void	DepthRange(f32 _near, f32 _far) override;
	virtual void	FrontFace(u32 _faceType) override;
	virtual void	CullFace(u32 _cullType) override;
	virtual void	PolygonMode(u32 _face, u32 _mode) override;
	virtual void	PolygonOffset(f32 _slopeScale, f32 _bias) override;

	virtual void	BlendEquationSeparate(u32 _colorOp, u32 _alphaOp) override;
	virtual void	BlendEquationSeparatei(u32 _slot, u32 _colorOp, u32 _alphaOp) override;
	virtual void	BlendFuncSeparate(u32 _colorSrcFunc, u32 _colorDstFunc, u32 _alphaSrcFunc, u32 _alphaDstFunc) override;
	virtual void	BlendFuncSeparatei(u32 _slot, u32 _colorSrcFunc, u32 _colorDstFunc, u32 _alphaSrcFunc, u32 _alphaDstFunc) override;
	virtual void	ColorMask(b8 _r, b8 _g, b8 _b, b8 _a) override;
	virtual void	ColorMaski(u32 _slot, b8 _r, b8 _g, b8 _b, b8 _a) override;

	virtual void	DepthMask(b8 _isWrite) override;
	virtual void	DepthFunc(u32 _depthTest) override;
	virtual void	StencilMaskSeparate(u32 _face, u32 _mask) override;
	virtual void	StencilFuncSeparate(u32 _face, u32 _test, u32 _ref, u32 _readMask) override;
	virtual void	StencilOpSeparate(u32 _face, u32 _failOp, u32 _ZFailOp, u32 _passOp) override;

	virtual void	SamplerParameteri(const u32* _pObjectID, u32 _name, u32 _param) override;
	virtual void	SamplerParameterf(const u32* _pObjectID, u32 _name, f32 _param) override;
	virtual void	SamplerParameterfv(const u32* _pObjectID, u32 _name, const f32* _param) override;

	virtual void	BindProgram(u32 _programID) override;
	virtual void	BindProgramPipeline(u32 _pipelineID) override;
	virtual void	ActiveShaderProgram(u32 _pipelineID, u32 _programID) override;

	virtual void	EnableVertexAttribArray(u32 _attr) override;
	virtual void	VertexAttribPointer(u32 _attr, u32 _elemNum, u32 _valType, b8 _normalized, u32 _stride, u32 _offset) override;
	virtual void	DrawArrays(u32 _primType, u32 _vtxOffset, u32 _vtxNum) override;
	virtual void	DrawIndexed(u32 _primType, u32 _valType, u32 _idxOffset, u32 _vtxOffset, u32 _idxNum) override;

	virtual void	Flush() override;

	virtual void	BeginQuery(u32 _target, const u32* _pQueryID) override;
	virtual void	EndQuery(u32 _target) override;
	virtual void	BeginConditionalRender(const u32* _pQueryID, u32 _mode) override;
	virtual void	EndConditionalRender() override;
	virtual void	PushDebugGroup(u32 _source, const u32* _pQueryID, u32 _srtLen, const c8* _pStr) override;
	virtual void	PopDebugGroup() override;

};


POISON_END

#endif	// __IMMEDIATE_DRAW_CONTEXT_H__

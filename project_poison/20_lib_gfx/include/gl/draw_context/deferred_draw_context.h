﻿//#pragma once
#ifndef __DEFERRED_DRAW_CONTEXT_H__
#define __DEFERRED_DRAW_CONTEXT_H__

//-------------------------------------------------------------------------------------------------
// include
#include "draw_context.h"


//-------------------------------------------------------------------------------------------------
// prototype


POISON_BGN


//*************************************************************************************************
//@brief	描画コンテキスト
//*************************************************************************************************
class CDeferredContext : public CDrawContext
{
public:
	CDeferredContext();
	virtual ~CDeferredContext();

	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	b8		Initialize(IAllocator* _pAllocator);

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	void	Finalize();

	//-------------------------------------------------------------------------------------------------
	//@brief	コマンドフラッシュ
	void	ExecuteCommandList();

	//-------------------------------------------------------------------------------------------------
	//@brief	登録コマンドリスト数
	u32		GetCommandListNum() const { return m_list.GetNum(); }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	描画コマンド関数
	virtual void	Enable(u32 _cap) override;
	virtual void	Enablei(u32 _slot, u32 _cap) override;
	virtual void	Disable(u32 _cap) override;
	virtual void	Disablei(u32 _slot, u32 _cap) override;

	virtual void	BindBuffer(u32 _target, const u32* _pObjectID) override;
	virtual void	BindBufferBase(u32 _target, u32 _slot, const u32* _pObjectID) override;
	virtual void	BindFramebuffer(u32 _target, const u32* _pObjectID) override;
	virtual void	BindRenderbuffer(u32 _target, const u32* _pObjectID) override;
	virtual void	BindTexture(u32 _target, const u32* _pObjectID) override;
	virtual void	ActiveTexture(u32 _slot) override;
	virtual void	BindSampler(u32 _slot, const u32* _pObjectID) override;

	virtual void	UnBindBuffer(u32 _target) override;
	virtual void	UnBindFramebuffer(u32 _target) override;
	virtual void	UnBindRenderbuffer(u32 _target) override;
	virtual void	UnBindTexture(u32 _target) override;

	virtual void	GenBuffers(u32 _num, u32* _pObjectID) override;
	virtual void	GenFramebuffers(u32 _num, u32* _pObjectID) override;
	virtual void	GenRenderbuffers(u32 _num, u32* _pObjectID) override;
	virtual void	GenTextures(u32 _num, u32* _pObjectID) override;
	virtual void	GenSamplers(u32 _num, u32* _pObjectID) override;
	virtual void	GenQueries(u32 _num, u32* _pQueryID) override;

	virtual void	DeleteBuffers(u32 _num, u32* _pObjectID) override;
	virtual void	DeleteFramebuffers(u32 _num, u32* _pObjectID) override;
	virtual void	DeleteRenderbuffers(u32 _num, u32* _pObjectID) override;
	virtual void	DeleteTextures(u32 _num, u32* _pObjectID) override;
	virtual void	DeleteSamplers(u32 _num, u32* _pObjectID) override;
	virtual void	DeleteQueries(u32 _num, u32* _pQueryID) override;

	virtual void	BufferData(u32 _target, u32 _size, const void* _pData, u32 _usage) override;
	virtual void	RenderbufferStorage(u32 _target, u32 _format, u32 _width, u32 _height) override;
	virtual void	RenderbufferStorageMultisample(u32 _target, u32 _aa, u32 _format, u32 _width, u32 _height) override;
	virtual void	TexImage2D(u32 _target, u32 _level, u32 _format, u32 _width, u32 _height, u32 _texFormat, u32 _valType, const void* _pData) override;
	virtual void	TexImage2DMultisample(u32 _target, u32 _aa, u32 _format, u32 _width, u32 _height) override;
	virtual void	TexImage3D(u32 _target, u32 _level, u32 _format, u32 _width, u32 _height, u32 _depth, u32 _texFormat, u32 _valType, const void* _pData) override;
	virtual void	TexStorage2D(u32 _target, u32 _level, u32 _format, u32 _width, u32 _height) override;
	virtual void	TexStorage2DMultisample(u32 _target, u32 _aa, u32 _format, u32 _width, u32 _height) override;
	virtual void	TexStorage3D(u32 _target, u32 _level, u32 _format, u32 _width, u32 _height, u32 _depth) override;

	virtual void	MapBuffer(u32 _target, u32 _size, const void* _pData) override;
	virtual void	TexSubImage2D(u32 _target, u32 _level, u32 _xoff, u32 _yoff, u32 _mipWidth, u32 _mipHeight, u32 _texFormat, u32 _valType, const void* _pData) override;
	virtual void	TexSubImage3D(u32 _target, u32 _level, u32 _xoff, u32 _yoff, u32 _zoff, u32 _mipWidth, u32 _mipHeight, u32 _mipDepth, u32 _texFormat, u32 _valType, const void* _pData) override;

	virtual void	CheckFramebufferStatus(const u32* _pObjectID) override;
	virtual void	GetTexLevelParameteriv(u32 _target, u32 _level, u32 _name, s32* _pParam) override;
	virtual void	GetQueryObjectiv(const u32* _pQueryID, u32 _name, s32* _pParam) override;
	virtual void	GetQueryObjectuiv(const u32* _pQueryID, u32 _name, u32* _pParam) override;
	virtual void	GetQueryObjecti64v(const u32* _pQueryID, u32 _name, s64* _pParam) override;
	virtual void	GetQueryObjectui64v(const u32* _pQueryID, u32 _name, u64* _pParam) override;

	virtual void	FramebufferRenderbuffer(u32 _frameTrg, u32 _slot, u32 _colorTrg, const u32* _pObjectID) override;
	virtual void	FramebufferTexture2D(u32 _frameTrg, u32 _slot, u32 _textureTrg, const u32* _pObjectID, u32 _level) override;
	virtual void	DrawBuffers(u32 _num, const u32* _pColorTrg) override;

	virtual void	ClearBufferColor(f32 _r, f32 _g, f32 _b, f32 _a) override;
	virtual void	ClearBufferDepth(f32 _depth) override;
	virtual void	ClearBufferStencil(s32 _stencil) override;
	virtual void	ClearColor(f32 _r, f32 _g, f32 _b, f32 _a) override;
	virtual void	ClearDepth(f32 _depth) override;
	virtual void	ClearStencil(s32 _stencil) override;
	virtual void	Clear(u32 _bit) override;

	virtual void	Viewport(s32 _posX, s32 _posY, u32 _width, u32 _height) override;
	virtual void	Scissor(s32 _posX, s32 _posY, u32 _width, u32 _height) override;
	virtual void	DepthRange(f32 _near, f32 _far) override;
	virtual void	FrontFace(u32 _faceType) override;
	virtual void	CullFace(u32 _cullType) override;
	virtual void	PolygonMode(u32 _face, u32 _mode) override;
	virtual void	PolygonOffset(f32 _slopeScale, f32 _bias) override;

	virtual void	BlendEquationSeparate(u32 _colorOp, u32 _alphaOp) override;
	virtual void	BlendEquationSeparatei(u32 _slot, u32 _colorOp, u32 _alphaOp) override;
	virtual void	BlendFuncSeparate(u32 _colorSrcFunc, u32 _colorDstFunc, u32 _alphaSrcFunc, u32 _alphaDstFunc) override;
	virtual void	BlendFuncSeparatei(u32 _slot, u32 _colorSrcFunc, u32 _colorDstFunc, u32 _alphaSrcFunc, u32 _alphaDstFunc) override;
	virtual void	ColorMask(b8 _r, b8 _g, b8 _b, b8 _a) override;
	virtual void	ColorMaski(u32 _slot, b8 _r, b8 _g, b8 _b, b8 _a) override;

	virtual void	DepthMask(b8 _isWrite) override;
	virtual void	DepthFunc(u32 _depthTest) override;
	virtual void	StencilMaskSeparate(u32 _face, u32 _mask) override;
	virtual void	StencilFuncSeparate(u32 _face, u32 _test, u32 _ref, u32 _readMask) override;
	virtual void	StencilOpSeparate(u32 _face, u32 _failOp, u32 _ZFailOp, u32 _passOp) override;

	virtual void	SamplerParameteri(const u32* _pObjectID, u32 _name, u32 _param) override;
	virtual void	SamplerParameterf(const u32* _pObjectID, u32 _name, f32 _param) override;
	virtual void	SamplerParameterfv(const u32* _pObjectID, u32 _name, const f32* _param) override;

	virtual void	BindProgram(u32 _programID) override;
	virtual void	BindProgramPipeline(u32 _pipelineID) override;
	virtual void	ActiveShaderProgram(u32 _pipelineID, u32 _programID) override;

	virtual void	EnableVertexAttribArray(u32 _attr) override;
	virtual void	VertexAttribPointer(u32 _attr, u32 _elemNum, u32 _valType, b8 _normalized, u32 _stride, u32 _offset) override;
	virtual void	DrawArrays(u32 _primType, u32 _vtxOffset, u32 _vtxNum) override;
	virtual void	DrawIndexed(u32 _primType, u32 _valType, u32 _idxOffset, u32 _vtxOffset, u32 _idxNum) override;

	virtual void	Flush() override;

	virtual void	BeginQuery(u32 _target, const u32* _pQueryID) override;
	virtual void	EndQuery(u32 _target) override;
	virtual void	BeginConditionalRender(const u32* _pQueryID, u32 _mode) override;
	virtual void	EndConditionalRender() override;
	virtual void	PushDebugGroup(u32 _source, const u32* _pQueryID, u32 _srtLen, const c8* _pStr) override;
	virtual void	PopDebugGroup() override;

	
private:
	//-------------------------------------------------------------------------------------------------
	//@brief	描画コマンド関数
	static void	Cmd_Enable(u32 _argc, const void* _argv[]);
	static void	Cmd_Enablei(u32 _argc, const void* _argv[]);
	static void	Cmd_Disable(u32 _argc, const void* _argv[]);
	static void	Cmd_Disablei(u32 _argc, const void* _argv[]);

	static void	Cmd_BindBuffer(u32 _argc, const void* _argv[]);
	static void	Cmd_BindBufferBase(u32 _argc, const void* _argv[]);
	static void	Cmd_BindFramebuffer(u32 _argc, const void* _argv[]);
	static void	Cmd_BindRenderbuffer(u32 _argc, const void* _argv[]);
	static void	Cmd_BindTexture(u32 _argc, const void* _argv[]);
	static void	Cmd_ActiveTexture(u32 _argc, const void* _argv[]);
	static void	Cmd_BindSampler(u32 _argc, const void* _argv[]);

	static void	Cmd_UnBindBuffer(u32 _argc, const void* _argv[]);
	static void	Cmd_UnBindFramebuffer(u32 _argc, const void* _argv[]);
	static void	Cmd_UnBindRenderbuffer(u32 _argc, const void* _argv[]);
	static void	Cmd_UnBindTexture(u32 _argc, const void* _argv[]);

	static void	Cmd_GenBuffers(u32 _argc, const void* _argv[]);
	static void	Cmd_GenFramebuffers(u32 _argc, const void* _argv[]);
	static void	Cmd_GenRenderbuffers(u32 _argc, const void* _argv[]);
	static void	Cmd_GenTextures(u32 _argc, const void* _argv[]);
	static void	Cmd_GenSamplers(u32 _argc, const void* _argv[]);
	static void	Cmd_GenQueries(u32 _argc, const void* _argv[]);

	static void	Cmd_DeleteBuffers(u32 _argc, const void* _argv[]);
	static void	Cmd_DeleteFramebuffers(u32 _argc, const void* _argv[]);
	static void	Cmd_DeleteRenderbuffers(u32 _argc, const void* _argv[]);
	static void	Cmd_DeleteTextures(u32 _argc, const void* _argv[]);
	static void	Cmd_DeleteSamplers(u32 _argc, const void* _argv[]);
	static void	Cmd_DeleteQueries(u32 _argc, const void* _argv[]);

	static void	Cmd_BufferData(u32 _argc, const void* _argv[]);
	static void	Cmd_RenderbufferStorage(u32 _argc, const void* _argv[]);
	static void	Cmd_RenderbufferStorageMultisample(u32 _argc, const void* _argv[]);
	static void	Cmd_TexImage2D(u32 _argc, const void* _argv[]);
	static void	Cmd_TexImage2DMultisample(u32 _argc, const void* _argv[]);
	static void	Cmd_TexImage3D(u32 _argc, const void* _argv[]);
	static void	Cmd_TexStorage2D(u32 _argc, const void* _argv[]);
	static void	Cmd_TexStorage2DMultisample(u32 _argc, const void* _argv[]);
	static void	Cmd_TexStorage3D(u32 _argc, const void* _argv[]);

	static void	Cmd_MapBuffer(u32 _argc, const void* _argv[]);
	static void	Cmd_TexSubImage2D(u32 _argc, const void* _argv[]);
	static void	Cmd_TexSubImage3D(u32 _argc, const void* _argv[]);

	static void	Cmd_CheckFramebufferStatus(u32 _argc, const void* _argv[]);

	static void	Cmd_FramebufferRenderbuffer(u32 _argc, const void* _argv[]);
	static void	Cmd_FramebufferTexture2D(u32 _argc, const void* _argv[]);
	static void	Cmd_DrawBuffers(u32 _argc, const void* _argv[]);

	static void	Cmd_ClearBufferColor(u32 _argc, const void* _argv[]);
	static void	Cmd_ClearBufferDepth(u32 _argc, const void* _argv[]);
	static void	Cmd_ClearBufferStencil(u32 _argc, const void* _argv[]);
	static void	Cmd_ClearColor(u32 _argc, const void* _argv[]);
	static void	Cmd_ClearDepth(u32 _argc, const void* _argv[]);
	static void	Cmd_ClearStencil(u32 _argc, const void* _argv[]);
	static void	Cmd_Clear(u32 _argc, const void* _argv[]);

	static void	Cmd_Viewport(u32 _argc, const void* _argv[]);
	static void	Cmd_Scissor(u32 _argc, const void* _argv[]);
	static void	Cmd_DepthRange(u32 _argc, const void* _argv[]);
	static void	Cmd_FrontFace(u32 _argc, const void* _argv[]);
	static void	Cmd_CullFace(u32 _argc, const void* _argv[]);
	static void	Cmd_PolygonMode(u32 _argc, const void* _argv[]);
	static void	Cmd_PolygonOffset(u32 _argc, const void* _argv[]);

	static void	Cmd_BlendEquationSeparate(u32 _argc, const void* _argv[]);
	static void	Cmd_BlendEquationSeparatei(u32 _argc, const void* _argv[]);
	static void	Cmd_BlendFuncSeparate(u32 _argc, const void* _argv[]);
	static void	Cmd_BlendFuncSeparatei(u32 _argc, const void* _argv[]);
	static void	Cmd_ColorMask(u32 _argc, const void* _argv[]);
	static void	Cmd_ColorMaski(u32 _argc, const void* _argv[]);

	static void	Cmd_DepthMask(u32 _argc, const void* _argv[]);
	static void	Cmd_DepthFunc(u32 _argc, const void* _argv[]);
	static void	Cmd_StencilMaskSeparate(u32 _argc, const void* _argv[]);
	static void	Cmd_StencilFuncSeparate(u32 _argc, const void* _argv[]);
	static void	Cmd_StencilOpSeparate(u32 _argc, const void* _argv[]);

	static void	Cmd_SamplerParameteri(u32 _argc, const void* _argv[]);
	static void	Cmd_SamplerParameterf(u32 _argc, const void* _argv[]);
	static void	Cmd_SamplerParameterfv(u32 _argc, const void* _argv[]);

	static void	Cmd_BindProgram(u32 _argc, const void* _argv[]);
	static void	Cmd_BindProgramPipeline(u32 _argc, const void* _argv[]);
	static void	Cmd_ActiveShaderProgram(u32 _argc, const void* _argv[]);

	static void	Cmd_EnableVertexAttribArray(u32 _argc, const void* _argv[]);
	static void	Cmd_VertexAttribPointer(u32 _argc, const void* _argv[]);
	static void	Cmd_DrawArrays(u32 _argc, const void* _argv[]);
	static void	Cmd_DrawIndexed(u32 _argc, const void* _argv[]);

	static void	Cmd_Flush(u32 _argc, const void* _argv[]);

	static void	Cmd_BeginQuery(u32 _argc, const void* _argv[]);
	static void	Cmd_EndQuery(u32 _argc, const void* _argv[]);
	static void	Cmd_BeginConditionalRender(u32 _argc, const void* _argv[]);
	static void	Cmd_EndConditionalRender(u32 _argc, const void* _argv[]);
	static void	Cmd_PushDebugGroup(u32 _argc, const void* _argv[]);
	static void	Cmd_PopDebugGroup(u32 _argc, const void* _argv[]);


private:
	//-------------------------------------------------------------------------------------------------
	//@brief	描画コマンド関数
	typedef void(*CMD_DRAW_FUNC)(u32 _argc, const void* _argv[]);

	//-------------------------------------------------------------------------------------------------
	//@brief	描画コマンドパケット
	LINK_NODE(CMD_DRAW_PACKET)
	{
	public:
		CMD_DRAW_FUNC	func = NULL;
		const void**	argv = NULL;
		u32				argc = 0;
	};
	typedef CLinkNode<CMD_DRAW_PACKET>	CMD_DRAW_PACKET_LIST;

private:
	IAllocator*				m_pAllocator = NULL;
	CMD_DRAW_PACKET_LIST	m_list;

};


POISON_END

#endif	// __DEFERRED_DRAW_CONTEXT_H__

﻿//#pragma once
#ifndef __DRAW_CONTEXT_H__
#define __DRAW_CONTEXT_H__

//-------------------------------------------------------------------------------------------------
// include


//-------------------------------------------------------------------------------------------------
// prototype


POISON_BGN


//*************************************************************************************************
//@brief	描画コンテキスト
//*************************************************************************************************
class CDrawContext
{
public:
	CDrawContext() {}
	virtual ~CDrawContext() {}


public:
	//-------------------------------------------------------------------------------------------------
	//@brief	描画コマンド関数
	virtual void	Enable(u32 _cap) = 0;
	virtual void	Enablei(u32 _slot, u32 _cap) = 0;
	virtual void	Disable(u32 _cap) = 0;
	virtual void	Disablei(u32 _slot, u32 _cap) = 0;

	virtual void	BindBuffer(u32 _target, const u32* _pObjectID) = 0;
	virtual void	BindBufferBase(u32 _target, u32 _slot, const u32* _pObjectID) = 0;
	virtual void	BindFramebuffer(u32 _target, const u32* _pObjectID) = 0;
	virtual void	BindRenderbuffer(u32 _target, const u32* _pObjectID) = 0;
	virtual void	BindTexture(u32 _target, const u32* _pObjectID) = 0;
	virtual void	ActiveTexture(u32 _slot) = 0;
	virtual void	BindSampler(u32 _slot, const u32* _pObjectID) = 0;

	virtual void	UnBindBuffer(u32 _target) = 0;
	virtual void	UnBindFramebuffer(u32 _target) = 0;
	virtual void	UnBindRenderbuffer(u32 _target) = 0;
	virtual void	UnBindTexture(u32 _target) = 0;

	virtual void	GenBuffers(u32 _num, u32* _pObjectID) = 0;
	virtual void	GenFramebuffers(u32 _num, u32* _pObjectID) = 0;
	virtual void	GenRenderbuffers(u32 _num, u32* _pObjectID) = 0;
	virtual void	GenTextures(u32 _num, u32* _pObjectID) = 0;
	virtual void	GenSamplers(u32 _num, u32* _pObjectID) = 0;
	virtual void	GenQueries(u32 _num, u32* _pQueryID) = 0;

	virtual void	DeleteBuffers(u32 _num, u32* _pObjectID) = 0;
	virtual void	DeleteFramebuffers(u32 _num, u32* _pObjectID) = 0;
	virtual void	DeleteRenderbuffers(u32 _num, u32* _pObjectID) = 0;
	virtual void	DeleteTextures(u32 _num, u32* _pObjectID) = 0;
	virtual void	DeleteSamplers(u32 _num, u32* _pObjectID) = 0;
	virtual void	DeleteQueries(u32 _num, u32* _pQueryID) = 0;

	virtual void	BufferData(u32 _target, u32 _size, const void* _pData, u32 _usage) = 0;
	virtual void	RenderbufferStorage(u32 _target, u32 _format, u32 _width, u32 _height) = 0;
	virtual void	RenderbufferStorageMultisample(u32 _target, u32 _aa, u32 _format, u32 _width, u32 _height) = 0;
	virtual void	TexImage2D(u32 _target, u32 _level, u32 _format, u32 _width, u32 _height, u32 _texFormat, u32 _valType, const void* _pData) = 0;
	virtual void	TexImage2DMultisample(u32 _target, u32 _aa, u32 _format, u32 _width, u32 _height) = 0;
	virtual void	TexImage3D(u32 _target, u32 _level, u32 _format, u32 _width, u32 _height, u32 _depth, u32 _texFormat, u32 _valType, const void* _pData) = 0;
	virtual void	TexStorage2D(u32 _target, u32 _level, u32 _format, u32 _width, u32 _height) = 0;
	virtual void	TexStorage2DMultisample(u32 _target, u32 _aa, u32 _format, u32 _width, u32 _height) = 0;
	virtual void	TexStorage3D(u32 _target, u32 _level, u32 _format, u32 _width, u32 _height, u32 _depth) = 0;

	virtual void	MapBuffer(u32 _target, u32 _size, const void* _pData) = 0;
	virtual void	TexSubImage2D(u32 _target, u32 _level, u32 _xoff, u32 _yoff, u32 _mipWidth, u32 _mipHeight, u32 _texFormat, u32 _valType, const void* _pData) = 0;
	virtual void	TexSubImage3D(u32 _target, u32 _level, u32 _xoff, u32 _yoff, u32 _zoff, u32 _mipWidth, u32 _mipHeight, u32 _mipDepth, u32 _texFormat, u32 _valType, const void* _pData) = 0;

	virtual void	CheckFramebufferStatus(const u32* _pObjectID) = 0;
	virtual void	GetTexLevelParameteriv(u32 _target, u32 _level, u32 _name, s32* _pParam) = 0;
	virtual void	GetQueryObjectiv(const u32* _pQueryID, u32 _name, s32* _pParam) = 0;
	virtual void	GetQueryObjectuiv(const u32* _pQueryID, u32 _name, u32* _pParam) = 0;
	virtual void	GetQueryObjecti64v(const u32* _pQueryID, u32 _name, s64* _pParam) = 0;
	virtual void	GetQueryObjectui64v(const u32* _pQueryID, u32 _name, u64* _pParam) = 0;

	virtual void	FramebufferRenderbuffer(u32 _frameTrg, u32 _slot, u32 _colorTrg, const u32* _pObjectID) = 0;
	virtual void	FramebufferTexture2D(u32 _frameTrg, u32 _slot, u32 _textureTrg, const u32* _pObjectID, u32 _level) = 0;
	virtual void	DrawBuffers(u32 _num, const u32* _pColorTrg) = 0;

	virtual void	ClearBufferColor(f32 _r, f32 _g, f32 _b, f32 _a) = 0;
	virtual void	ClearBufferDepth(f32 _depth) = 0;
	virtual void	ClearBufferStencil(s32 _stencil) = 0;
	virtual void	ClearColor(f32 _r, f32 _g, f32 _b, f32 _a) = 0;
	virtual void	ClearDepth(f32 _depth) = 0;
	virtual void	ClearStencil(s32 _stencil) = 0;
	virtual void	Clear(u32 _bit) = 0;

	virtual void	Viewport(s32 _posX, s32 _posY, u32 _width, u32 _height) = 0;
	virtual void	Scissor(s32 _posX, s32 _posY, u32 _width, u32 _height) = 0;
	virtual void	DepthRange(f32 _near, f32 _far) = 0;
	virtual void	FrontFace(u32 _faceType) = 0;
	virtual void	CullFace(u32 _cullType) = 0;
	virtual void	PolygonMode(u32 _face, u32 _mode) = 0;
	virtual void	PolygonOffset(f32 _slopeScale, f32 _bias) = 0;

	virtual void	BlendEquationSeparate(u32 _colorOp, u32 _alphaOp) = 0;
	virtual void	BlendEquationSeparatei(u32 _slot, u32 _colorOp, u32 _alphaOp) = 0;
	virtual void	BlendFuncSeparate(u32 _colorSrcFunc, u32 _colorDstFunc, u32 _alphaSrcFunc, u32 _alphaDstFunc) = 0;
	virtual void	BlendFuncSeparatei(u32 _slot, u32 _colorSrcFunc, u32 _colorDstFunc, u32 _alphaSrcFunc, u32 _alphaDstFunc) = 0;
	virtual void	ColorMask(b8 _r, b8 _g, b8 _b, b8 _a) = 0;
	virtual void	ColorMaski(u32 _slot, b8 _r, b8 _g, b8 _b, b8 _a) = 0;

	virtual void	DepthMask(b8 _isWrite) = 0;
	virtual void	DepthFunc(u32 _depthTest) = 0;
	virtual void	StencilMaskSeparate(u32 _face, u32 _mask) = 0;
	virtual void	StencilFuncSeparate(u32 _face, u32 _test, u32 _ref, u32 _readMask) = 0;
	virtual void	StencilOpSeparate(u32 _face, u32 _failOp, u32 _ZFailOp, u32 _passOp) = 0;

	virtual void	SamplerParameteri(const u32* _pObjectID, u32 _name, u32 _param) = 0;
	virtual void	SamplerParameterf(const u32* _pObjectID, u32 _name, f32 _param) = 0;
	virtual void	SamplerParameterfv(const u32* _pObjectID, u32 _name, const f32* _param) = 0;

	virtual void	BindProgram(u32 _programID) = 0;
	virtual void	BindProgramPipeline(u32 _pipelineID) = 0;
	virtual void	ActiveShaderProgram(u32 _pipelineID, u32 _programID) = 0;

	virtual void	EnableVertexAttribArray(u32 _attr) = 0;
	virtual void	VertexAttribPointer(u32 _attr, u32 _elemNum, u32 _valType, b8 _normalized, u32 _stride, u32 _offset) = 0;
	virtual void	DrawArrays(u32 _primType, u32 _vtxOffset, u32 _vtxNum) = 0;
	virtual void	DrawIndexed(u32 _primType, u32 _valType, u32 _idxOffset, u32 _vtxOffset, u32 _idxNum) = 0;

	virtual void	Flush() = 0;

	virtual void	BeginQuery(u32 _target, const u32* _pQueryID) = 0;
	virtual void	EndQuery(u32 _target) = 0;
	virtual void	BeginConditionalRender(const u32* _pQueryID, u32 _mode) = 0;
	virtual void	EndConditionalRender() = 0;
	virtual void	PushDebugGroup(u32 _source, const u32* _pQueryID, u32 _srtLen, const c8* _pStr) = 0;
	virtual void	PopDebugGroup() = 0;

};


POISON_END

#endif	// __DRAW_CONTEXT_H__

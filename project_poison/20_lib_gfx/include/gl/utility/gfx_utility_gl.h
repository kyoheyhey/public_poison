﻿//#pragma once
#ifndef __GFX_UTILITY_GL_H__
#define __GFX_UTILITY_GL_H__

//-------------------------------------------------------------------------------------------------
// include
#include "format/gfx_format.h"


POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype


//*************************************************************************************************
//@brief	OpenGLユーティリティ
//*************************************************************************************************
class CGfxUtilityGL
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	OpenGL関数ロード
	static b8		LoadProc();

	//-------------------------------------------------------------------------------------------------
	//@brief	OpenGLデバッグチェックコールバック設定
	static void		SetupDebugCallback();

	//-------------------------------------------------------------------------------------------------
	//@brief	デフォルトテクスチャパラメータ設定
	static b8		SetDefaultTexParameter(u32 _target, GFX_IMAGE_FORMAT _format, u32 _mipLevel);

};



POISON_END

#endif	// __GFX_UTILITY_GL_H__

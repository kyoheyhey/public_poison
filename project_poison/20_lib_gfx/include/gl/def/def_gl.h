﻿//#pragma once
#ifndef __DEF_GL_H__
#define __DEF_GL_H__


///-------------------------------------------------------------------------------------------------
/// define
#define USE_OPNEGL_PROC_GLEW
//#define USE_OPNEGL_PROC_GL_POISON


//-------------------------------------------------------------------------------------------------
// include
#if defined(USE_OPNEGL_PROC_GLEW)
#include "glew.h"
#include "wglew.h"

#elif defined(USE_OPNEGL_PROC_GL_POISON)
#include "gl_poison.h"

#endif // USE_OPNEGL_PROC_GL_POISON

// 描画コンテキスト
#include "gl/draw_context/draw_context.h"


#endif	// __DEF_GL_H__

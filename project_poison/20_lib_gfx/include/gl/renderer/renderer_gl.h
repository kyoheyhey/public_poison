﻿//#pragma once
#ifndef __RENDERER_GL_H__
#define __RENDERER_GL_H__

//-------------------------------------------------------------------------------------------------
// define
//#define USE_IMMEDIATE


//-------------------------------------------------------------------------------------------------
// include
#include "renderer/renderer.h"
#ifdef USE_IMMEDIATE
#include "gl/draw_context/immediate_draw_context.h"
#else
#include "gl/draw_context/deferred_draw_context.h"
#endif // USE_IMMEDIATE


POISON_BGN

//*************************************************************************************************
//@brief	描画クラス
//*************************************************************************************************
class CRendererGL : public CRenderer
{
public:
	CRendererGL();
	virtual ~CRendererGL();

	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	virtual b8		Initialize(IAllocator* _pAllocator, const c8* _pName) override;

	//-------------------------------------------------------------------------------------------------
	//@brief	終了
	virtual void	Finalize();


protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	描画開始
	//virtual void	DrawBegin();

	//-------------------------------------------------------------------------------------------------
	//@brief	描画終了
	//virtual void	DrawEnd() override;


public:
	//-------------------------------------------------------------------------------------------------
	//@brief	バッファクリア
	virtual void	ClearSurface(const CVec4& _clr, RENDER_BUFFER_BIT _bit = RENDER_BUFFER_BIT_ALL) override;
	virtual void	ClearSurface(const CVec4& _clr, const CRenderTarget* _pRenderTarget, COLOR_BIT _clrBit, u32 _depIdx) override;

	//-------------------------------------------------------------------------------------------------
	//@brief	頂点配列描画
	virtual void	DrawPrim(GFX_PRIMITIVE _primType, u32 _num, u32 _offset = 0) override;

	//-------------------------------------------------------------------------------------------------
	//@brief	インデックス描画
	virtual void	DrawIndexPrim(GFX_PRIMITIVE _primType, u32 _num, u32 _vtxOffset = 0, u32 _idxOffset = 0) override;


public:
	//-------------------------------------------------------------------------------------------------
	//@brief	クエリー開始
	virtual void	BeginQuery(const COcclusionQuery* _pQuery);
	virtual void	BeginQuery(const CPrimitiveQuery* _pQuery);
	virtual void	BeginQuery(const CTimerQuery* _pQuery);

	//-------------------------------------------------------------------------------------------------
	//@brief	クエリー終了
	virtual void	EndQuery(const COcclusionQuery* _pQuery);
	virtual void	EndQuery(const CPrimitiveQuery* _pQuery);
	virtual void	EndQuery(const CTimerQuery* _pQuery);

	//-------------------------------------------------------------------------------------------------
	//@brief	事前描画遮蔽カリング設定
	virtual void	SetDrawingPredictionQuery(const COcclusionQuery* _pQuery);


public:
	//-------------------------------------------------------------------------------------------------
	//@brief	マーカープッシュ設定
	virtual void	PushMarker(const c8* _pMessage) override;

	//-------------------------------------------------------------------------------------------------
	//@brief	マーカーポップ設定
	virtual void	PopMarker() override;


protected:
	//-------------------------------------------------------------------------------------------------
	//@brief	レンダーターゲットフラッシュ
	virtual void	FlushRenderTarget() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	シェーダフラッシュ
	virtual void	FlushShader(const PassShader* _pPassShader) override;

	//-------------------------------------------------------------------------------------------------
	//@brief	頂点フラッシュ
	virtual void	FlushVertex() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	インデックスフラッシュ
	virtual void	FlushIndex() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	ユニフォームフラッシュ
	virtual void	FlushUniform() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	テクスチャフラッシュ
	virtual void	FlushTexture() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	サンプラーステートフラッシュ
	virtual void	FlushSamplerState() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	ビューポートフラッシュ
	virtual void	FlushViewport() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	シザリングフラッシュ
	virtual void	FlushScissorRect() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	ラストライザステートフラッシュ
	virtual void	FlushRasterrizerState() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	ブレンドステートフラッシュ
	virtual void	FlushBrendState() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	デプスステンシルステートフラッシュ
	virtual void	FlushDepthStencilState() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	事前描画プリディケーションフラッシュ開始
	virtual void	FlushDrawingPredictionQueryBegin() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	事前描画プリディケーションフラッシュ終了
	virtual void	FlushDrawingPredictionQueryEnd() override;

	//-------------------------------------------------------------------------------------------------
	//@brief	キューフラッシュ
	virtual void	FlushCommandQueue() override;


protected:
#ifdef USE_IMMEDIATE
	CImmediateContext	m_drawContext;		//< 描画コンテキスト(実体)
#else
	CDeferredContext	m_drawContext;		//< 描画コンテキスト(実体)
#endif // USE_IMMEDIATE
};

POISON_END


#endif	// __RENDERER_GL_H__

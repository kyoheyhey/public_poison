﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "render_prim/render_prim.h"

// シェーダ
#include "shader/shader.h"
#include "shader/shader_utility.h"
#include "resource/shader_library.h"

// バッファオブジェクト
#include "object/color_buffer.h"
#include "object/depth_buffer.h"
#include "object/texture_buffer.h"
#include "object/uniform_buffer.h"
#include "object/vertex_buffer.h"
#include "object/index_buffer.h"
#include "object/sampler_state.h"
#include "object/rasterrizer_state.h"
#include "object/blend_state.h"
#include "object/depth_state.h"

// カメラ
//#include ""


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant

// シングルトンのためのポインタ
static CRenderPrim* s_pRenPrim = NULL;


///-------------------------------------------------------------------------------------------------
/// コンストラクタ
CRenderPrim::CRenderPrim()
{
}

///-------------------------------------------------------------------------------------------------
/// デストラクタ
CRenderPrim::~CRenderPrim()
{
}


///-------------------------------------------------------------------------------------------------
/// 初期化
b8		CRenderPrim::Initialize(CRenderPrimElm _elemAry[], const u32 _elemNum, const hash32 _shaderName)
{
	// インスタンス
	static CRenderPrim s_renderPrim;
	s_pRenPrim = &s_renderPrim;

	s_pRenPrim->m_pElm = _elemAry;
	s_pRenPrim->m_uMaxNum = SCast<u16>(_elemNum);

	// 頂点DECL
	VTX_DECL vtxDecl[] =
	{
		{ VTX_ATTR_POS, 0, GFX_VALUE_F32, 3, },
		{ VTX_ATTR_CLR0, sizeof(f32) * 3, GFX_VALUE_F32, 4, },
	};
	s_pRenPrim->m_pShader = CShaderUtility::SearchAndCreateFxShader(_shaderName, vtxDecl, ARRAYOF(vtxDecl));
	return (s_pRenPrim->m_pShader != NULL);
}

///-------------------------------------------------------------------------------------------------
/// 後処理
void	CRenderPrim::Finalize()
{
	if (!s_pRenPrim) { return; }

	if (s_pRenPrim->m_pElm)
	{
		::memset(s_pRenPrim->m_pElm, 0, sizeof(CRenderPrimElm) * s_pRenPrim->m_uMaxNum);
	}
	s_pRenPrim = NULL;
}

///-------------------------------------------------------------------------------------------------
/// 塗りつぶし用のデプスレンダーターゲット設定
void	CRenderPrim::SetDepthTex(const CTextureBuffer* _depthTex)
{
	s_pRenPrim->m_pDepthTex = _depthTex;
}

///-------------------------------------------------------------------------------------------------
/// 線のメッシュマージ描画
b8	CRenderPrim::IsMeshMergeElmType(CRenderPrimElm::PRIM _elmType)
{
	return (
		_elmType == CRenderPrimElm::PRIM_ELM_POINT ||
		_elmType == CRenderPrimElm::PRIM_ELM_LINE ||
		_elmType == CRenderPrimElm::PRIM_ELM_TRIANGLE ||
		_elmType == CRenderPrimElm::PRIM_ELM_BOX ||
		_elmType == CRenderPrimElm::PRIM_ELM_PIPE ||
		_elmType == CRenderPrimElm::PRIM_ELM_SPHERE ||
		_elmType == CRenderPrimElm::PRIM_ELM_CAPSULE ||
		_elmType == CRenderPrimElm::PRIM_ELM_ELLIPSE ||
		_elmType == CRenderPrimElm::PRIM_ELM_FAN ||
		_elmType == CRenderPrimElm::PRIM_ELM_AXIS ||
		_elmType == CRenderPrimElm::PRIM_ELM_SPRITE ||
		_elmType == CRenderPrimElm::PRIM_ELM_FONT ||
		_elmType == CRenderPrimElm::PRIM_ELM_DIR ||
		_elmType == CRenderPrimElm::PRIM_ELM_BOX_AREA_FILL ||
		_elmType == CRenderPrimElm::PRIM_ELM_SPHERE_AREA_FILL ||
		_elmType == CRenderPrimElm::PRIM_ELM_CAPSULE_AREA_FILL ||
		_elmType == CRenderPrimElm::PRIM_ELM_PIPE_AREA_FILL ||
		_elmType == CRenderPrimElm::PRIM_ELM_FAN_AREA_FILL
		);
}


///-------------------------------------------------------------------------------------------------
/// 描画コールバック
b8		CRenderPrim::RenderFunc(CRenderer* _pRenderer, const void* _p0, const void* _p1)
{
	const CRenderPrim* _pRenPrim = PCast<const CRenderPrim*>(_p0);
	const u32 _flag = *PCast<const u32*>(_p1);
	// 登録なし
	if (_pRenPrim->m_uDrawBgnIndex == _pRenPrim->m_uDrawEndIndex) { return false; }

	PUSH_RENDERER_MARKER(_pRenderer, "RenderPrim");
	_pRenderer->PushRasterizer();
	_pRenderer->PushDepthState();
	_pRenderer->PushBlendState();

	// 開始インデックス取得、開始は次の位置から
	u32 uStartIndex = _pRenPrim->GetNextIndex(_pRenPrim->m_uDrawBgnIndex);

	// 最大値を取る
	u32 uNextMax = _pRenPrim->m_uDrawEndIndex;
	// 開始の方が大きい → 一周している
	if (uNextMax < uStartIndex)
	{
		// 最大値まで
		uNextMax = _pRenPrim->m_uMaxNum - 1;
	}

	RASTERIZER_DESC rasterizerDesc;
	DEPTH_STATE_DESC depthDesc;
	BLEND_STATE_DESC blendDesc;
	// レンダーステート設定
	{
		rasterizerDesc.cullFace = GFX_CULL_FACE_BACK;	// カリングする
		depthDesc.isDepthTest = true;					// Zテストする
		depthDesc.isDepthWrite = false;					// Z書き込みなし
		depthDesc.depthTest = GFX_TEST_GREATER;			// refより大きい
		blendDesc.enable = true;						// ブレンド有効
		blendDesc.colorSrcFunc = GFX_BLEND_FUNC_SRC_ALPHA;
		blendDesc.colorDstFunc = GFX_BLEND_FUNC_INV_SRC_ALPHA;
		blendDesc.colorOp = GFX_BLEND_OP_ADD;
		blendDesc.alphaOp = GFX_BLEND_OP_ADD;
	}

	// シェーダーセット
	{
		// シェーダー設定
		_pRenderer->SetShader(_pRenPrim->m_pShader);
	}

	// ローカルマトリクス初期化
	{
		_pRenderer->LoadMatrix(MATRIX_LOCAL_WORLD, CMtx44::Identity);
	}

	f32 fClrRate[] = { 0.7f, 1.f };
	f32 fAlphaRate[] = { 0.5f, 1.f };

#if 1	// 一括式

	// 2回描画する(一回目は半透明のZ無視、2回目は不透明のZあり)
	for (u32 iLoop = 0; iLoop < 2; ++iLoop)
	{
		// 2回目はZテストを行う
		if (iLoop > 0)
		{
			// Zテストする
			depthDesc.depthTest = GFX_TEST_LEQUAL;	// ref以下
		}


		// 塗りつぶし描画
#pragma region Draw Merge Fill Mesh

		// ポリゴンオフセット
		rasterizerDesc.bepthBias = -1.0f;

		// カリング描画するかどうか
		b8 cullDraw = false;

		do
		{
			if (cullDraw)
			{
				rasterizerDesc.cullFace = GFX_CULL_FACE_BACK;	// カリングする
			}
			else
			{
				rasterizerDesc.cullFace = GFX_CULL_FACE_NONE;	// カリングする
			}

			// 頂点数、インデックス数の取得
			u32 vtxNum = 0;
			u32 idxNum = 0;
			const CRenderPrimElm* pElm = NULL;
			{
				pElm = _pRenPrim->m_pElm + uStartIndex;
				for (u32 i = uStartIndex; i <= uNextMax; ++i, ++pElm)
				{
					// フラグが一致したら
					if (iLoop == 0 && pElm->GetZTest() == CRenderPrimElm::ZTEST_ENABLE) { continue; }
					if (iLoop == 1 && pElm->GetZTest() == CRenderPrimElm::ZTEST_NONE) { continue; }

					if (pElm->GetFlag() & _flag && pElm->IsFillCullFace() == cullDraw)
					{
						// 頂点数、インデックス数をカウント
						vtxNum += pElm->GetFillVertexNum();
						idxNum += pElm->GetFillIndexNum();
					}
					// 例外的にAreaFill処理***********************************************************S
					if (_pRenPrim->m_pDepthTex && _pRenPrim->m_pDepthTex->IsEnable() && iLoop == 0 && pElm->IsFillCullFace() == cullDraw &&
						(
							pElm->GetType() == CRenderPrimElm::PRIM_ELM_BOX_AREA_FILL ||
							pElm->GetType() == CRenderPrimElm::PRIM_ELM_SPHERE_AREA_FILL ||
							pElm->GetType() == CRenderPrimElm::PRIM_ELM_CAPSULE_AREA_FILL ||
							pElm->GetType() == CRenderPrimElm::PRIM_ELM_PIPE_AREA_FILL ||
							pElm->GetType() == CRenderPrimElm::PRIM_ELM_FAN_AREA_FILL)
						)
					{
						// 領域塗りつぶしパラメーター
						CMtx44 areaFill;
						pElm->GetAreaFillParam(areaFill);
						// ピクセル定数バッファ設定
						const CUniformBuffer* pAreaFillUB = _pRenderer->CreateAndCacheUniform(sizeof(areaFill), 1, &areaFill);
						libAssert(pAreaFillUB);
						_pRenderer->SetUniform(GFX_SHADER_PIXEL, UNIFORM_SLOT_00, pAreaFillUB);
						// 描画
						pElm->RenderFuncAreaFill(_pRenderer, pElm->GetFaceColor(), _pRenPrim->m_pDepthTex);
						continue;
					}
					// 例外的にAreaFill処理***********************************************************E
				}

				// 最大値と描画終了位置が違う
				if (uNextMax != _pRenPrim->m_uDrawEndIndex)
				{
					// 頭からまたループでカウント
					pElm = _pRenPrim->m_pElm;
					for (u32 i = 0; i <= _pRenPrim->m_uDrawEndIndex; ++i, ++pElm)
					{
						// フラグが一致したら
						if (iLoop == 0 && pElm->GetZTest() == CRenderPrimElm::ZTEST_ENABLE) { continue; }
						if (iLoop == 1 && pElm->GetZTest() == CRenderPrimElm::ZTEST_NONE) { continue; }

						if (pElm->GetFlag() & _flag && pElm->IsFillCullFace() == cullDraw)
						{
							// 頂点数、インデックス数をカウント
							vtxNum += pElm->GetFillVertexNum();
							idxNum += pElm->GetFillIndexNum();
						}
					}
				}
			}
			// 個数があれば
			if (vtxNum > 0)
			{
				// メモリ確保
				CRenderPrimElm::VERTEX* pVertex = PCast<CRenderPrimElm::VERTEX*>(MEM_MALLOC(_pRenderer->GetAllocator(), sizeof(CRenderPrimElm::VERTEX) * vtxNum));
				u32* pIndex = PCast<u32*>(MEM_MALLOC(_pRenderer->GetAllocator(), sizeof(u32) * idxNum));

				// メッシュの準備
				u32 cnt_vtx = 0;
				u32 cnt_ind = 0;
				{
					pElm = _pRenPrim->m_pElm + uStartIndex;
					for (u32 i = uStartIndex; i <= uNextMax; ++i, ++pElm)
					{
						// フラグが一致したら描画
						if (iLoop == 0 && pElm->GetZTest() == CRenderPrimElm::ZTEST_ENABLE) { continue; }
						if (iLoop == 1 && pElm->GetZTest() == CRenderPrimElm::ZTEST_NONE) { continue; }

						if (pElm->GetFlag() & _flag && pElm->IsFillCullFace() == cullDraw)
						{
							// メッシュの準備
							pElm->FillMeshSetFunc(pVertex, pIndex, cnt_vtx, cnt_ind, fClrRate[iLoop], fAlphaRate[iLoop]);
							cnt_vtx += pElm->GetFillVertexNum();
							cnt_ind += pElm->GetFillIndexNum();
						}
					}

					// 最大値と描画終了位置が違う
					if (uNextMax != _pRenPrim->m_uDrawEndIndex)
					{
						// 頭からまたループでカウント
						pElm = _pRenPrim->m_pElm;
						for (u32 i = 0; i <= _pRenPrim->m_uDrawEndIndex; ++i, ++pElm)
						{
							// フラグが一致したら描画
							if (iLoop == 0 && pElm->GetZTest() == CRenderPrimElm::ZTEST_ENABLE) { continue; }
							if (iLoop == 1 && pElm->GetZTest() == CRenderPrimElm::ZTEST_NONE) { continue; }

							if (pElm->GetFlag() & _flag && pElm->IsFillCullFace() == cullDraw)
							{
								// メッシュの準備
								pElm->FillMeshSetFunc(pVertex, pIndex, cnt_vtx, cnt_ind, fClrRate[iLoop], fAlphaRate[iLoop]);
								cnt_vtx += pElm->GetFillVertexNum();
								cnt_ind += pElm->GetFillIndexNum();
							}
						}
					}
				}

				// ラスタライザー設定
				{
					const CRasterizer* pRasterizer = _pRenderer->CreateAndCacheRasterizer(rasterizerDesc);
					Assert(pRasterizer);
					_pRenderer->SetRasterizer(pRasterizer);
				}
				// デプスステンシルステート設定
				{
					const CDepthState* pDepthState = _pRenderer->CreateAndCacheDepthState(depthDesc);
					libAssert(pDepthState);
					_pRenderer->SetDepthState(pDepthState);
				}
				// ブレンドステート設定
				{
					const CBlendState* pBlendState = _pRenderer->CreateAndCacheBlendState(blendDesc);
					libAssert(pBlendState);
					_pRenderer->SetBlendState(pBlendState);
				}

				// 頂点データ設定
				const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(pVertex[0]), vtxNum, pVertex);
				_pRenderer->SetVertex(pVtxBuf);

				// インデックスデータ設定
				const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(pIndex[0]), idxNum, pIndex);
				_pRenderer->SetIndex(pIdxBuf);

				//描画
				_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_TRIANGLES, idxNum);
			}

			// 入れ替え
			cullDraw = !cullDraw;
		} while (cullDraw);

		// ポリゴンオフセット戻す
		rasterizerDesc.bepthBias = 0.0f;

#pragma endregion


		// 線のメッシュマージ描画
#pragma region Draw Merge Line Mesh 
		do	// break用
		{
			// 頂点数、インデックス数の取得
			u32 vtxNum = 0;
			u32 idxNum = 0;
			const CRenderPrimElm* pElm = NULL;
			{
				pElm = _pRenPrim->m_pElm + uStartIndex;
				for (u32 i = uStartIndex; i <= uNextMax; ++i, ++pElm)
				{
					// フラグが一致したら
					if (iLoop == 0 &&
						(pElm->GetZTest() == CRenderPrimElm::ZTEST_ENABLE
							|| !IsMeshMergeElmType(pElm->GetType()))
						) {
						continue;
					}
					if (iLoop == 1 && pElm->GetZTest() == CRenderPrimElm::ZTEST_NONE) { continue; }

					if (pElm->GetFlag() & _flag)
					{
						// 頂点数、インデックス数をカウント
						vtxNum += pElm->GetLineVertexNum();
						idxNum += pElm->GetLineIndexNum();
					}
				}

				// 最大値と描画終了位置が違う
				if (uNextMax != _pRenPrim->m_uDrawEndIndex)
				{
					// 頭からまたループでカウント
					pElm = _pRenPrim->m_pElm;
					for (u32 i = 0; i <= _pRenPrim->m_uDrawEndIndex; ++i, ++pElm)
					{
						// フラグが一致したら描画
						if (iLoop == 0 &&
							(pElm->GetZTest() == CRenderPrimElm::ZTEST_ENABLE
								|| !IsMeshMergeElmType(pElm->GetType()))
							) {
							continue;
						}
						if (iLoop == 1 && pElm->GetZTest() == CRenderPrimElm::ZTEST_NONE) { continue; }

						if (pElm->GetFlag() & _flag)
						{
							// 頂点数、インデックス数をカウント
							vtxNum += pElm->GetLineVertexNum();
							idxNum += pElm->GetLineIndexNum();
						}
					}
				}
			}

			if (vtxNum <= 0) { break; }

			// メモリ確保
			CRenderPrimElm::VERTEX* pVertex = PCast<CRenderPrimElm::VERTEX*>(MEM_MALLOC(_pRenderer->GetAllocator(), sizeof(CRenderPrimElm::VERTEX) * vtxNum));
			if (pVertex == NULL) { break; }
			u32* pIndex = PCast<u32*>(MEM_MALLOC(_pRenderer->GetAllocator(), sizeof(u32) * idxNum));
			if (pIndex == NULL) { break; }

			// メッシュの準備
			u32 cnt_vtx = 0;
			u32 cnt_ind = 0;
			{
				pElm = _pRenPrim->m_pElm + uStartIndex;
				for (u32 i = uStartIndex; i <= uNextMax; ++i, ++pElm)
				{
					// メッシュの準備
					if (iLoop == 0 &&
						(pElm->GetZTest() == CRenderPrimElm::ZTEST_ENABLE
							|| !IsMeshMergeElmType(pElm->GetType()))
						) {
						continue;
					}
					if (iLoop == 1 && pElm->GetZTest() == CRenderPrimElm::ZTEST_NONE) { continue; }

					pElm->LineMeshSetFunc(pVertex, pIndex, cnt_vtx, cnt_ind, fClrRate[iLoop], fAlphaRate[iLoop]);
					cnt_vtx += pElm->GetLineVertexNum();
					cnt_ind += pElm->GetLineIndexNum();
				}

				// 最大値と描画終了位置が違う
				if (uNextMax != _pRenPrim->m_uDrawEndIndex)
				{
					// 頭からまたループでカウント
					pElm = _pRenPrim->m_pElm;
					for (u32 i = 0; i <= _pRenPrim->m_uDrawEndIndex; ++i, ++pElm)
					{
						// フラグが一致したら描画
						if (iLoop == 0 &&
							(pElm->GetZTest() == CRenderPrimElm::ZTEST_ENABLE
								|| !IsMeshMergeElmType(pElm->GetType()))
							) {
							continue;
						}
						if (iLoop == 1 && pElm->GetZTest() == CRenderPrimElm::ZTEST_NONE) { continue; }

						if (pElm->GetFlag() & _flag)
						{
							// メッシュの準備
							pElm->LineMeshSetFunc(pVertex, pIndex, cnt_vtx, cnt_ind, fClrRate[iLoop], fAlphaRate[iLoop]);
							cnt_vtx += pElm->GetLineVertexNum();
							cnt_ind += pElm->GetLineIndexNum();
						}
					}
				}
			}

			// ラスタライザー設定
			{
				const CRasterizer* pRasterizer = _pRenderer->CreateAndCacheRasterizer(rasterizerDesc);
				Assert(pRasterizer);
				_pRenderer->SetRasterizer(pRasterizer);
			}
			// デプスステンシルステート設定
			{
				const CDepthState* pDepthState = _pRenderer->CreateAndCacheDepthState(depthDesc);
				libAssert(pDepthState);
				_pRenderer->SetDepthState(pDepthState);
			}
			// ブレンドステート設定
			{
				const CBlendState* pBlendState = _pRenderer->CreateAndCacheBlendState(blendDesc);
				libAssert(pBlendState);
				_pRenderer->SetBlendState(pBlendState);
			}

			// 頂点データ設定
			const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(pVertex[0]), vtxNum, pVertex);
			_pRenderer->SetVertex(pVtxBuf);

			// インデックスデータ設定
			const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(pIndex[0]), idxNum, pIndex);
			_pRenderer->SetIndex(pIdxBuf);

			//描画
			_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_LINES, idxNum);
		} while (false);
#pragma endregion

		// 例外的にマージせず描画
		{
			const CRenderPrimElm* pElm = NULL;
			pElm = _pRenPrim->m_pElm + uStartIndex;
			for (u32 i = uStartIndex; i <= uNextMax; ++i, ++pElm)
			{
				// タイプとフラグが一致したら
				if (pElm->GetType() == CRenderPrimElm::PRIM_ELM_DIR
					&& pElm->GetFlag() & _flag)
				{
					pElm->RenderFunc(_pRenderer, fClrRate[iLoop], fAlphaRate[iLoop], 0);
				}
			}

			// 最大値と描画終了位置が違う
			if (uNextMax != _pRenPrim->m_uDrawEndIndex)
			{
				// 頭からまたループでカウント
				pElm = _pRenPrim->m_pElm;
				for (u32 i = 0; i <= _pRenPrim->m_uDrawEndIndex; ++i, ++pElm)
				{
					// タイプとフラグが一致したら
					if (pElm->GetType() == CRenderPrimElm::PRIM_ELM_DIR
						&& pElm->GetFlag() & _flag)
					{
						pElm->RenderFunc(_pRenderer, fClrRate[iLoop], fAlphaRate[iLoop], 0);
					}
				}
			}
		}
	}

#else	// 個別Draw式

	// 2回描画する(一回目は半透明のZ無視、2回目は不透明のZあり)
	for (u32 iLoop = 0; iLoop < 2; ++iLoop)
	{
		// 2回目はZテストを行う
		if (iLoop > 0)
		{
			// Zテストする
			_pRenderer->SetRenderState(RS_DEPTH_FUNC, RS_LESS);		// Zテスト式
			_pRenderer->SetRenderState(RS_BLEND_ENABLE, RS_FALSE);	// ブレンド有効
		}

		// 描画ループ
		const CRenderPrimElm* pElm = _pRenPrim->m_pElm + uStartIndex;
		for (u32 i = uStartIndex; i <= uNextMax; ++i, ++pElm)
		{
			// フラグが一致したら描画
			if (pElm->GetFlag() & _flag)
			{
				pElm->RenderFunc(_pRenderer, fClrRate[iLoop], fAlphaRate[iLoop], 0);
			}
		}

		// 最大値と描画終了位置が違う
		if (uNextMax != _pRenPrim->m_uDrawEndIndex)
		{
			// 頭からまたループで描画
			pElm = _pRenPrim->m_pElm;
			for (u32 i = 0; i <= _pRenPrim->m_uDrawEndIndex; ++i, ++pElm)
			{
				// フラグが一致したら描画
				if (pElm->GetFlag() & _flag)
				{
					pElm->RenderFunc(_pRenderer, fClrRate[iLoop], fAlphaRate[iLoop], 0);
				}
			}
		}

	}
#endif

	_pRenderer->PopBlendState();
	_pRenderer->PopDepthState();
	_pRenderer->PopRasterizer();
	POP_RENDERER_MARKER(_pRenderer);

	return true;
}

///-------------------------------------------------------------------------------------------------
/// 描画
void	CRenderPrim::Draw(const u32 _flag)
{
	static u32 s_flag = _flag;
	// 描画の開始インデックスを更新
	s_pRenPrim->m_uDrawBgnIndex = s_pRenPrim->m_uBgnIndex;
	// 終了インデックスも更新
	s_pRenPrim->m_uDrawEndIndex = s_pRenPrim->m_uEndIndex;

	// 描画呼び出し
	CGfxUtility::AddDraw(RenderFunc, s_pRenPrim, &s_flag);
}


///-------------------------------------------------------------------------------------------------
/// 描画終了、次の描画のための準備 
void	CRenderPrim::DrawEnd()
{
	// 登録なし
	if (s_pRenPrim->m_uBgnIndex == s_pRenPrim->m_uEndIndex) { return; }

	if (s_pRenPrim->m_uNum >= s_pRenPrim->m_uMaxNum)
	{
		PRINT_ERROR("***** [ERROR] CRenderPrim Over Num[%4d] Max[%4d]\n", s_pRenPrim->m_uNum, s_pRenPrim->m_uMaxNum);
	}

	// 終了位置を開始位置に
	s_pRenPrim->m_uBgnIndex = s_pRenPrim->m_uEndIndex;
	s_pRenPrim->m_uNum = 0;
}


///-------------------------------------------------------------------------------------------------
/// 要素を登録
b8	CRenderPrim::Add(CRenderPrimElm& _elm)
{
	// 終了インデックスの次
	u16 nextIndex = s_pRenPrim->GetNextIndex(s_pRenPrim->m_uEndIndex);

	// 描画中
	if (s_pRenPrim->m_uDrawBgnIndex != s_pRenPrim->m_uDrawEndIndex)
	{
		// 次の描画の領域は超えれない
		if (nextIndex == s_pRenPrim->m_uDrawBgnIndex) { return false; }
	}

	// バッファの先頭まで来た(最大値)なら無理
	if (nextIndex == s_pRenPrim->m_uBgnIndex)
	{
		//	PRINT_ERROR("***** [ERROR] CRenderPrim::Add() Over Num[%4d]\n", s_pRenPrim->m_uNum );
		//	libAssert( s_pRenPrim->m_uNum == s_pRenPrim->m_uMaxNum ); 

		s_pRenPrim->m_uNum++;	// カウントだけしてみる…
		return false;
	}

	// 登録
	s_pRenPrim->m_pElm[nextIndex] = _elm;
	s_pRenPrim->m_uEndIndex = nextIndex;
	s_pRenPrim->m_uNum++;
	libAssert(s_pRenPrim->m_uNum <= s_pRenPrim->m_uMaxNum);

	return true;
}


///-------------------------------------------------------------------------------------------------
/// AddLines
b8	CRenderPrim::AddLines(const CVec3* _pPosAry, const u32 _posNum, const f32 _clr[4])
{
	if (!_pPosAry || _posNum <= 1) { return false; }

	CRenderPrimElm elm;
	// カラー設定
	elm.SetColor(_clr[0], _clr[1], _clr[2], _clr[3]);

	// 座標ループ
	const CVec3* p = _pPosAry;
	for (u32 i = 0; i < _posNum - 1; ++i, ++p)
	{
		// ライン
		elm.SetAsLine((*p), p[1]);
		if (!Add(elm)) {
			return false;
		}
	}

	return true;
}

///-------------------------------------------------------------------------------------------------
/// AddPoints
b8	CRenderPrim::AddPoints(const CVec3* _pPosAry, const u32 _posNum, const f32 _clr[4], const f32 _size)
{
	if (!_pPosAry || _posNum <= 1) { return false; }

	CRenderPrimElm elm;
	// カラー設定
	elm.SetColor(_clr[0], _clr[1], _clr[2], _clr[3]);

	// 座標ループ
	const CVec3* p = _pPosAry;
	for (u32 i = 0; i < _posNum; ++i, ++p)
	{
		// ボックス(仮)
		elm.SetAsBox((*p), _size);
		if (!Add(elm)) {
			return false;
		}
	}

	return true;
}

///-------------------------------------------------------------------------------------------------
/// 線分を追加
void	CRenderPrimUtility::AddLine(const CVec3& _start, const CVec3& _end, const CVec3& _color)
{
	CRenderPrimElm elm;

	elm.SetAsLine(_start, _end);
	elm.SetColor(_color);
	CRenderPrim::Add(elm);
}

///-------------------------------------------------------------------------------------------------
/// 網の目状の地面をレンダープリムに追加
void	CRenderPrimUtility::AddSplitGround(f32 _size, u32 _split)
{
	if (_size <= 0.0f || _split <= 0) { return; }

	_split = (_split % 2) ? _split + 1 : _split;		// 分割数を偶数化
	f32 halfSize = _size / 2.0f;					// 半分のサイズ
	f32 add_value = _size / _split;		// 分割幅

	CRenderPrimElm elm;

	f32 posX = -halfSize;		// x始点
	f32 posZ = -halfSize;		// z始点
	for (u32 cnt_x = 0; cnt_x < _split + 1; ++cnt_x)
	{
		// 線分追加
		CRenderPrimUtility::AddLine(CVec3(posX, 0.0f, posZ), CVec3(posX, 0.0f, posZ + _size));
		posX += add_value;
	}

	posX = -halfSize;
	posZ = -halfSize;
	for (u32 cnt_z = 0; cnt_z < _split + 1; ++cnt_z)
	{
		// 線分追加
		CRenderPrimUtility::AddLine(CVec3(posX, 0.0f, posZ), CVec3(posX + _size, 0.0f, posZ));
		posZ += add_value;
	}
}

POISON_END


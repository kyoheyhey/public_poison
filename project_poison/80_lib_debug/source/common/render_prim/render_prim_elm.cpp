﻿
///-------------------------------------------------------------------------------------------------
/// define
#define RENDER_PRIM_DIV	8

//#define RENDER_PRIM_FILL


///-------------------------------------------------------------------------------------------------
/// include
#include "render_prim/render_prim_elm.h"

#include "renderer/renderer.h"
// バッファオブジェクト
#include "object/color_buffer.h"
#include "object/depth_buffer.h"
#include "object/texture_buffer.h"
#include "object/uniform_buffer.h"
#include "object/vertex_buffer.h"
#include "object/index_buffer.h"
#include "object/sampler_state.h"
#include "object/rasterrizer_state.h"
#include "object/blend_state.h"
#include "object/depth_state.h"

POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant

//	構築チェック
static b8		render_prim_built = false;

//	円柱
static CVec4	render_prim_pipe_vtx_lst[2 * 2 * RENDER_PRIM_DIV];
static u32		render_prim_pipe_vtx_num;

static u16		render_prim_pipe_idx_lst[2 * (3 * 2 * RENDER_PRIM_DIV)];
static u32		render_prim_pipe_idx_num;

//	球
static CVec4	render_prim_sphere_vtx_lst[2 * (RENDER_PRIM_DIV + 1) * RENDER_PRIM_DIV];
static u32		render_prim_sphere_vtx_num;

static u16		render_prim_sphere_idx_lst[2 * ((2 * RENDER_PRIM_DIV * (RENDER_PRIM_DIV - 1)) + (2 * RENDER_PRIM_DIV * RENDER_PRIM_DIV))];
static u32		render_prim_sphere_idx_num;

//	カプセル
static CVec4	render_prim_capsule_vtx_lst[2 * (RENDER_PRIM_DIV + 2) * RENDER_PRIM_DIV];
static u32		render_prim_capsule_vtx_num;

static u16		render_prim_capsule_idx_lst[2 * ((2 * RENDER_PRIM_DIV * RENDER_PRIM_DIV) + (2 * RENDER_PRIM_DIV * (RENDER_PRIM_DIV + 1)))];
static u32		render_prim_capsule_idx_num;

//	扇
static CVec4	render_prim_fan_vtx_lst[2 * (2 * RENDER_PRIM_DIV + 1)];
static u32		render_prim_fan_vtx_num;

static u16		render_prim_fan_idx_lst[2 * (3 * (2 * RENDER_PRIM_DIV + 1))];
static u32		render_prim_fan_idx_num;

// 構築タイプ
enum RENDER_PRIM_BUILD_TYPE
{
	NONE = 0,
	PIPE,		// 円柱
	SPHERE,		// 球
	CAPSULE,	// カプセル

	ALL,		// 全て			
};

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma region Build

void	render_prim_build(const RENDER_PRIM_BUILD_TYPE _build_type)
{
	if (render_prim_built) { return; }

	if (_build_type == PIPE || _build_type == ALL)
		//	円柱
	{
		//	座標
		{
			render_prim_pipe_vtx_num = 0;

			const f32	offset[2] = { 1.f, -1.f };

			//	上面・底面
			for (u32 rh = 0; rh < 2; rh++)
			{
				for (u32 rv = 0; rv < 2 * RENDER_PRIM_DIV; rv++)
				{
					f32	v_rad = PI * rv / (f32)RENDER_PRIM_DIV - PI;
					f32	v_sin = sinf(v_rad);
					f32	v_cos = cosf(v_rad);

					render_prim_pipe_vtx_lst[render_prim_pipe_vtx_num] = CVec4(v_sin, offset[rh], v_cos, 1.f);
					render_prim_pipe_vtx_num++;
				}
			}
		}

		//	インデックス
		{
			render_prim_pipe_idx_num = 0;

			//	上面・底面
			{
				const int	offset[2] = { 0, 2 * RENDER_PRIM_DIV };
				for (u32 rh = 0; rh < 2; rh++)
				{
					render_prim_pipe_idx_lst[render_prim_pipe_idx_num] = SCast<u16>(offset[rh] + 2 * RENDER_PRIM_DIV - 1);
					render_prim_pipe_idx_num++;
					render_prim_pipe_idx_lst[render_prim_pipe_idx_num] = SCast<u16>(offset[rh] + 0);
					render_prim_pipe_idx_num++;

					for (u32 rv = 1; rv < 2 * RENDER_PRIM_DIV; rv++)
					{
						render_prim_pipe_idx_lst[render_prim_pipe_idx_num] = SCast<u16>(offset[rh] + rv - 1);
						render_prim_pipe_idx_num++;
						render_prim_pipe_idx_lst[render_prim_pipe_idx_num] = SCast<u16>(offset[rh] + rv);
						render_prim_pipe_idx_num++;
					}
				}
			}

			//	側面
			{
				for (u32 rv = 0; rv < 2 * RENDER_PRIM_DIV; rv++)
				{
					render_prim_pipe_idx_lst[render_prim_pipe_idx_num] = SCast<u16>(rv);
					render_prim_pipe_idx_num++;
					render_prim_pipe_idx_lst[render_prim_pipe_idx_num] = SCast<u16>(rv + 2 * RENDER_PRIM_DIV);
					render_prim_pipe_idx_num++;
				}
			}
		}
	}

	//	扇
	{
		// 頂点数
		render_prim_fan_vtx_num = ARRAYOF(render_prim_fan_vtx_lst);
		render_prim_fan_vtx_lst[0] = CVec4::Up;
		render_prim_fan_vtx_lst[2 * RENDER_PRIM_DIV + 1] = CVec4(0.f, -1.f, 0.f, 1.f);

		// インデックス
		render_prim_fan_idx_num = 0;
		//	上面・底面
		{
			const u32	offset[2] = { 0, 2 * RENDER_PRIM_DIV + 1 };
			for (u32 rh = 0; rh < 2; ++rh)
			{
				render_prim_fan_idx_lst[render_prim_fan_idx_num] = SCast<u16>(offset[rh] + 2 * RENDER_PRIM_DIV);
				++render_prim_fan_idx_num;
				render_prim_fan_idx_lst[render_prim_fan_idx_num] = SCast<u16>(offset[rh] + 0);
				++render_prim_fan_idx_num;

				for (u32 rv = 1; rv < 2 * RENDER_PRIM_DIV + 1; ++rv)
				{
					render_prim_fan_idx_lst[render_prim_fan_idx_num] = SCast<u16>(offset[rh] + rv - 1);
					++render_prim_fan_idx_num;
					render_prim_fan_idx_lst[render_prim_fan_idx_num] = SCast<u16>(offset[rh] + rv);
					++render_prim_fan_idx_num;
				}
			}
		}

		//	側面
		{
			for (u32 rv = 0; rv < 2 * RENDER_PRIM_DIV + 1; ++rv)
			{
				render_prim_fan_idx_lst[render_prim_fan_idx_num] = SCast<u16>(rv);
				++render_prim_fan_idx_num;
				render_prim_fan_idx_lst[render_prim_fan_idx_num] = SCast<u16>(rv + (2 * RENDER_PRIM_DIV + 1));
				++render_prim_fan_idx_num;
			}
		}
	}

	if (_build_type == SPHERE || _build_type == ALL)
		//	球
	{
		//	座標
		{
			render_prim_sphere_vtx_num = 0;

			for (u32 rh = 0; rh <= RENDER_PRIM_DIV; rh++)
			{
				f32	h_rad = PI * rh / (f32)RENDER_PRIM_DIV;
				f32	h_sin = sinf(h_rad);
				f32	h_cos = cosf(h_rad);

				for (u32 rv = 0; rv < 2 * RENDER_PRIM_DIV; rv++)
				{
					f32	v_rad = PI * rv / (f32)RENDER_PRIM_DIV - PI;
					f32	v_sin = sinf(v_rad);
					f32	v_cos = cosf(v_rad);

					render_prim_sphere_vtx_lst[render_prim_sphere_vtx_num] = CVec4(h_sin * v_sin, h_cos, h_sin * v_cos, 1.f);
					render_prim_sphere_vtx_num++;
				}
			}
		}

		//	インデックス
		{
			render_prim_sphere_idx_num = 0;

			for (u32 rh = 1; rh < RENDER_PRIM_DIV; rh++)
			{
				render_prim_sphere_idx_lst[render_prim_sphere_idx_num] = SCast<u16>(2 * RENDER_PRIM_DIV * rh + 2 * RENDER_PRIM_DIV - 1);
				render_prim_sphere_idx_num++;
				render_prim_sphere_idx_lst[render_prim_sphere_idx_num] = SCast<u16>(2 * RENDER_PRIM_DIV * rh);
				render_prim_sphere_idx_num++;

				for (u32 rv = 1; rv < 2 * RENDER_PRIM_DIV; rv++)
				{
					render_prim_sphere_idx_lst[render_prim_sphere_idx_num] = SCast<u16>(2 * RENDER_PRIM_DIV * rh + rv - 1);
					render_prim_sphere_idx_num++;
					render_prim_sphere_idx_lst[render_prim_sphere_idx_num] = SCast<u16>(2 * RENDER_PRIM_DIV * rh + rv);
					render_prim_sphere_idx_num++;
				}
			}

			for (u32 rv = 0; rv < 2 * RENDER_PRIM_DIV; rv++)
			{
				for (u32 rh = 0; rh < RENDER_PRIM_DIV; rh++)
				{
					render_prim_sphere_idx_lst[render_prim_sphere_idx_num] = SCast<u16>(2 * RENDER_PRIM_DIV * (rh)+rv);
					render_prim_sphere_idx_num++;
					render_prim_sphere_idx_lst[render_prim_sphere_idx_num] = SCast<u16>(2 * RENDER_PRIM_DIV * (rh + 1) + rv);
					render_prim_sphere_idx_num++;
				}
			}
		}
	}

	if (_build_type == CAPSULE || _build_type == ALL)
		//	カプセル
	{
		//	座標
		{
			render_prim_capsule_vtx_num = 0;

			for (u32 rh = 0; rh <= RENDER_PRIM_DIV + 1; rh++)
			{
				u32	val = rh;
				if (2 * rh > RENDER_PRIM_DIV) { val--; }
				f32	h_rad = PI * val / (f32)RENDER_PRIM_DIV;
				f32	h_sin = sinf(h_rad);
				f32	h_cos = cosf(h_rad);

				for (u32 rv = 0; rv < 2 * RENDER_PRIM_DIV; rv++)
				{
					f32	v_rad = PI * rv / (f32)RENDER_PRIM_DIV - PI;
					f32	v_sin = sinf(v_rad);
					f32	v_cos = cosf(v_rad);

					render_prim_capsule_vtx_lst[render_prim_capsule_vtx_num] = CVec4(h_sin * v_sin, h_cos, h_sin * v_cos, 1.f);
					render_prim_capsule_vtx_num++;
				}
			}
		}

		//	インデックス
		{
			render_prim_capsule_idx_num = 0;

			for (u32 rh = 1; rh < RENDER_PRIM_DIV + 1; rh++)
			{
				render_prim_capsule_idx_lst[render_prim_capsule_idx_num] = SCast<u16>(2 * RENDER_PRIM_DIV * rh + 2 * RENDER_PRIM_DIV - 1);
				render_prim_capsule_idx_num++;
				render_prim_capsule_idx_lst[render_prim_capsule_idx_num] = SCast<u16>(2 * RENDER_PRIM_DIV * rh);
				render_prim_capsule_idx_num++;

				for (u32 rv = 1; rv < 2 * RENDER_PRIM_DIV; rv++)
				{
					render_prim_capsule_idx_lst[render_prim_capsule_idx_num] = SCast<u16>(2 * RENDER_PRIM_DIV * rh + rv - 1);
					render_prim_capsule_idx_num++;
					render_prim_capsule_idx_lst[render_prim_capsule_idx_num] = SCast<u16>(2 * RENDER_PRIM_DIV * rh + rv);
					render_prim_capsule_idx_num++;
				}
			}

			for (u32 rv = 0; rv < 2 * RENDER_PRIM_DIV; rv++)
			{
				for (u32 rh = 0; rh <= RENDER_PRIM_DIV; rh++)
				{
					render_prim_capsule_idx_lst[render_prim_capsule_idx_num] = SCast<u16>(2 * RENDER_PRIM_DIV * (rh)+rv);
					render_prim_capsule_idx_num++;
					render_prim_capsule_idx_lst[render_prim_capsule_idx_num] = SCast<u16>(2 * RENDER_PRIM_DIV * (rh + 1) + rv);
					render_prim_capsule_idx_num++;
				}
			}
		}
	}

	//	構築済み
	render_prim_built = true;
}

#pragma endregion

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	線分
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma region Line

//	描画
void	CRenderPrimElm::CLine::RenderFunc(CRenderer* _pRenderer, const CVec4& _color)	const
{
	VERTEX vertex[2];
	u32 index[2];

	// メッシュ設定
	LineMeshSetFunc(vertex, index, 0, 0, _color);

	// 頂点データ設定
	const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
	_pRenderer->SetVertex(pVtxBuf);

	//描画
	_pRenderer->DrawPrim(GFX_PRIMITIVE_LINES, GetLineIndexNum());
}

//  メッシュを設定する処理
void	CRenderPrimElm::CLine::LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	VERTEX vertex[2] =
	{
		{	pos[0].GetX(), pos[0].GetY(), pos[0].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	pos[1].GetX(), pos[1].GetY(), pos[1].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
	};
	_vertex_list[0 + _vertex_start_idx] = vertex[0];
	_vertex_list[1 + _vertex_start_idx] = vertex[1];

	_idx_list[0 + _idx_list_start_idx] = 0 + _vertex_start_idx;
	_idx_list[1 + _idx_list_start_idx] = 1 + _vertex_start_idx;
}

#pragma endregion

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	三角形
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma region Triangle

//	描画
void	CRenderPrimElm::CTriangle::RenderFunc(CRenderer* _pRenderer, const CVec4& _color)	const
{
#if 1

	VERTEX vertex[3];
	u32 index[6];

	LineMeshSetFunc(vertex, index, 0, 0, _color);

	// 頂点データ設定
	const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
	_pRenderer->SetVertex(pVtxBuf);

	// インデックスデータ設定
	const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(index[0]), ARRAYOF(index), index);
	_pRenderer->SetIndex(pIdxBuf);

	//描画
	_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_LINES, ARRAYOF(index));
#endif
}

//  メッシュを設定する処理
void	CRenderPrimElm::CTriangle::LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	VERTEX vertex[] =
	{
		{	pos[0].GetX(), pos[0].GetY(), pos[0].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	pos[1].GetX(), pos[1].GetY(), pos[1].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	pos[2].GetX(), pos[2].GetY(), pos[2].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
	};

	u16 index[] =
	{
		0, 1,  1, 2,  2, 0
	};

	memcpy(&_vertex_list[_vertex_start_idx], vertex, sizeof(vertex));
	for (u32 i = 0; i < ARRAYOF(index); ++i)
	{
		_idx_list[i + _idx_list_start_idx] = index[i] + _vertex_start_idx;
	}
}
#pragma endregion

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	箱
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma region Box

//	描画
void	CRenderPrimElm::CBox::RenderFunc(CRenderer* _pRenderer, const CVec4& _color)	const
{
#if 1
	VERTEX vertex[8];
	u32 index[24];

	// メッシュデータ設定
	LineMeshSetFunc(vertex, index, 0, 0, _color);

	// 頂点データ設定
	const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
	_pRenderer->SetVertex(pVtxBuf);

	// インデックスデータ設定
	const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(index[0]), ARRAYOF(index), index);
	_pRenderer->SetIndex(pIdxBuf);

	//描画
	_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_LINES, ARRAYOF(index));

	// シェーダ切り替えて
	//_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_LINES, ARRAYOF(index), index);
#endif
}

//  メッシュを設定する処理
void	CRenderPrimElm::CBox::LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	if (_vertex_list == NULL) { return; }
	if (_idx_list == NULL) { return; }

	// 座標
	CVec4 vtx[8] =
	{
		pst * CVec4(+1.f, +1.f, +1.f, 1.f),	// 右上前
		pst * CVec4(-1.f, +1.f, +1.f, 1.f),	// 左上前
		pst * CVec4(-1.f, +1.f, -1.f, 1.f),	// 左上後
		pst * CVec4(+1.f, +1.f, -1.f, 1.f),	// 右上後
		pst * CVec4(+1.f, -1.f, +1.f, 1.f),	// 右下前
		pst * CVec4(-1.f, -1.f, +1.f, 1.f),	// 左下前
		pst * CVec4(-1.f, -1.f, -1.f, 1.f),	// 左下後
		pst * CVec4(+1.f, -1.f, -1.f, 1.f),	// 右下後
	};

	for (u32 i = 0; i < 8; ++i)
	{
		// 座標コピー
		_vertex_list[i + _vertex_start_idx].vx = vtx[i].GetX();
		_vertex_list[i + _vertex_start_idx].vy = vtx[i].GetY();
		_vertex_list[i + _vertex_start_idx].vz = vtx[i].GetZ();

		// カラーコピー
		Vec::Copy(&(_vertex_list[i + _vertex_start_idx].cr), _color);
	}

	static const  u16 index[] =
	{
		0, 1,	1, 2,	2, 3,	3, 0,
		4, 5,	5, 6,	6, 7,	7, 4,
		0, 4,	1, 5,	2, 6,	3, 7,
	};

	for (u32 i = 0; i < ARRAYOF(index); ++i)
	{
		_idx_list[i + _idx_list_start_idx] = index[i] + _vertex_start_idx;
	}
}
#pragma endregion

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	円柱
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma region PIPE

//	描画
void	CRenderPrimElm::CPipe::RenderFunc(CRenderer* _pRenderer, const CVec4& _color)	const
{
#if 1
	VERTEX	vertex[sizeof(render_prim_pipe_vtx_lst) / sizeof(const CVec4&)];
	for (u32 n = 0; n < render_prim_pipe_vtx_num; n++)
	{
		CVec4	vtx = pst * render_prim_pipe_vtx_lst[n];
		vertex[n].vx = vtx[0];
		vertex[n].vy = vtx[1];
		vertex[n].vz = vtx[2];
		vertex[n].cr = _color.GetX();
		vertex[n].cg = _color.GetY();
		vertex[n].cb = _color.GetZ();
		vertex[n].ca = _color.GetW();
	}

	// 頂点データ設定
	const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
	_pRenderer->SetVertex(pVtxBuf);

	// インデックスデータ設定
	const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(render_prim_pipe_idx_lst[0]), ARRAYOF(render_prim_pipe_idx_lst), render_prim_pipe_idx_lst);
	_pRenderer->SetIndex(pIdxBuf);

	//描画
	_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_LINES, render_prim_pipe_idx_num);
#endif
}

//  メッシュを設定する処理
void	CRenderPrimElm::CPipe::LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	VERTEX	vertex[sizeof(render_prim_pipe_vtx_lst) / sizeof(const CVec4&)];
	for (u32 n = 0; n < render_prim_pipe_vtx_num; n++)
	{
		CVec4	vtx = pst * render_prim_pipe_vtx_lst[n];
		vertex[n].vx = vtx[0];
		vertex[n].vy = vtx[1];
		vertex[n].vz = vtx[2];
		vertex[n].cr = _color.GetX();
		vertex[n].cg = _color.GetY();
		vertex[n].cb = _color.GetZ();
		vertex[n].ca = _color.GetW();
	}

	memcpy(&_vertex_list[_vertex_start_idx], vertex, sizeof(vertex));
	for (u32 i = 0; i < ARRAYOF(render_prim_pipe_idx_lst); ++i)
	{
		_idx_list[i + _idx_list_start_idx] = render_prim_pipe_idx_lst[i] + _vertex_start_idx;
	}
}

// 頂点数取得
u32		CRenderPrimElm::CPipe::GetLineVertexNum() const
{
	return ARRAYOF(render_prim_pipe_vtx_lst);
}

// インデックス数取得
u32		CRenderPrimElm::CPipe::GetLineIndexNum() const
{
	return ARRAYOF(render_prim_pipe_idx_lst);
}

#pragma endregion

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	球
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma region Sphere

//	描画
void	CRenderPrimElm::CSphere::RenderFunc(CRenderer* _pRenderer, const CVec4& _color)	const
{
#if 1
	VERTEX	vertex[sizeof(render_prim_sphere_vtx_lst) / sizeof(const CVec4&)];
	for (u32 n = 0; n < render_prim_sphere_vtx_num; n++)
	{
		CVec4	vtx = pst * render_prim_sphere_vtx_lst[n];
		vertex[n].vx = vtx[0];
		vertex[n].vy = vtx[1];
		vertex[n].vz = vtx[2];
		vertex[n].cr = _color.GetX();
		vertex[n].cg = _color.GetY();
		vertex[n].cb = _color.GetZ();
		vertex[n].ca = _color.GetW();
	}

	// 頂点データ設定
	const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
	_pRenderer->SetVertex(pVtxBuf);

	// インデックスデータ設定
	const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(render_prim_sphere_idx_lst[0]), ARRAYOF(render_prim_sphere_idx_lst), render_prim_sphere_idx_lst);
	_pRenderer->SetIndex(pIdxBuf);

	//描画
	_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_LINES, render_prim_sphere_idx_num);
#endif
}

//  メッシュを設定する処理
void	CRenderPrimElm::CSphere::LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	VERTEX	vertex[sizeof(render_prim_sphere_vtx_lst) / sizeof(const CVec4&)];
	for (u32 n = 0; n < render_prim_sphere_vtx_num; n++)
	{
		CVec4	vtx = pst * render_prim_sphere_vtx_lst[n];
		vertex[n].vx = vtx[0];
		vertex[n].vy = vtx[1];
		vertex[n].vz = vtx[2];
		vertex[n].cr = _color.GetX();
		vertex[n].cg = _color.GetY();
		vertex[n].cb = _color.GetZ();
		vertex[n].ca = _color.GetW();
	}

	memcpy(&_vertex_list[_vertex_start_idx], vertex, sizeof(vertex));
	for (u32 i = 0; i < ARRAYOF(render_prim_sphere_idx_lst); ++i)
	{
		_idx_list[i + _idx_list_start_idx] = render_prim_sphere_idx_lst[i] + _vertex_start_idx;
	}

}

// 頂点数取得
u32		CRenderPrimElm::CSphere::GetLineVertexNum() const
{
	return render_prim_sphere_vtx_num;
}

// インデックス数取得
u32		CRenderPrimElm::CSphere::GetLineIndexNum() const
{
	return render_prim_sphere_idx_num;
}

#pragma endregion



//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	楕円
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma region Ellipse

//	描画
void	CRenderPrimElm::CEllipse::RenderFunc(CRenderer* _pRenderer, const CVec4& _color)	const
{
#if 1
	VERTEX	vertex[sizeof(render_prim_sphere_vtx_lst) / sizeof(const CVec4&)];
	for (u32 n = 0; n < render_prim_sphere_vtx_num; n++)
	{
		CVec4	vtx = pst * render_prim_sphere_vtx_lst[n];
		vertex[n].vx = vtx[0];
		vertex[n].vy = vtx[1];
		vertex[n].vz = vtx[2];
		vertex[n].cr = _color.GetX();
		vertex[n].cg = _color.GetY();
		vertex[n].cb = _color.GetZ();
		vertex[n].ca = _color.GetW();
	}

	// 頂点データ設定
	const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
	_pRenderer->SetVertex(pVtxBuf);

	// インデックスデータ設定
	const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(render_prim_sphere_idx_lst[0]), ARRAYOF(render_prim_sphere_idx_lst), render_prim_sphere_idx_lst);
	_pRenderer->SetIndex(pIdxBuf);

	//描画
	_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_LINES, render_prim_sphere_idx_num);
#endif
}

//  メッシュを設定する処理
void	CRenderPrimElm::CEllipse::LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	VERTEX	vertex[sizeof(render_prim_sphere_vtx_lst) / sizeof(const CVec4&)];
	for (u32 n = 0; n < render_prim_sphere_vtx_num; n++)
	{
		CVec4	vtx = pst * render_prim_sphere_vtx_lst[n];
		vertex[n].vx = vtx[0];
		vertex[n].vy = vtx[1];
		vertex[n].vz = vtx[2];
		vertex[n].cr = _color.GetX();
		vertex[n].cg = _color.GetY();
		vertex[n].cb = _color.GetZ();
		vertex[n].ca = _color.GetW();
	}

	memcpy(&_vertex_list[_vertex_start_idx], vertex, sizeof(vertex));
	for (u32 i = 0; i < ARRAYOF(render_prim_sphere_idx_lst); ++i)
	{
		_idx_list[i + _idx_list_start_idx] = render_prim_sphere_idx_lst[i] + _vertex_start_idx;
	}

}

// 頂点数取得
u32		CRenderPrimElm::CEllipse::GetLineVertexNum() const
{
	return render_prim_sphere_vtx_num;
}

// インデックス数取得
u32		CRenderPrimElm::CEllipse::GetLineIndexNum() const
{
	return render_prim_sphere_idx_num;
}

#pragma endregion



//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	カプセル
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma region Capsule

//	描画
void	CRenderPrimElm::CCapsule::RenderFunc(CRenderer* _pRenderer, const CVec4& _color)	const
{
#if 1

	VERTEX	vertex[ARRAYOF(render_prim_capsule_vtx_lst)];
	u32		index[ARRAYOF(render_prim_capsule_idx_lst)];

	// メッシュ準備
	LineMeshSetFunc(vertex, index, 0, 0, _color);

	// 頂点データ設定
	const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
	_pRenderer->SetVertex(pVtxBuf);

	// インデックスデータ設定
	const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(render_prim_capsule_idx_lst[0]), ARRAYOF(render_prim_capsule_idx_lst), render_prim_capsule_idx_lst);
	_pRenderer->SetIndex(pIdxBuf);

	//描画
	_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_LINES, render_prim_capsule_idx_num);

#endif
}

//  メッシュを設定する処理
void	CRenderPrimElm::CCapsule::LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	CMtx44	mtx = CMtx44::Identity;
	mtx[3] = CVec4((pos[0] + pos[1]).GetXYZ() * 0.5f, 1.f);

	if (Vec::Length2(pos[1] - mtx[3]) > 0.f) {
		mtx[1] = Vec::Normalize(CVec4((pos[1] - mtx[3]).GetXYZ(), 0.f)) * rad;
	}
	else {
		mtx[1][1] *= rad;
	}
	if ((mtx[1][0] == 0.f) && (mtx[1][2] == 0.f))
	{
		mtx[0] = CVec4(rad, 0.f, 0.f, 0.f);
		mtx[2] = CVec4(0.f, 0.f, rad, 0.f);
	}
	else {
		mtx[2] = Vec::Normalize(CVec4(Vec::Cross(CVec3(0.f, 1.f, 0.f), mtx[1].GetXYZ()), 0.f)) * rad;
		mtx[0] = Vec::Normalize(CVec4(Vec::Cross(mtx[1].GetXYZ(), mtx[2].GetXYZ()), 0.f)) * rad;
	}

	CMtx44	pst[2] = { mtx, mtx };
	pst[0][3] = pos[1];
	pst[1][3] = pos[0];

#ifdef MATRIX_COLUMN_MAJOR
	// 列優先のため転置しとく
	Mtx::Transpose(pst[0]);
	Mtx::Transpose(pst[1]);
#endif // MATRIX_COLUMN_MAJOR

	VERTEX vertex[sizeof(render_prim_capsule_vtx_lst) / sizeof(const CVec4&)];
	for (u32 n = 0; n < render_prim_capsule_vtx_num; n++)
	{
		CVec4 vtx = pst[(2 * n) >= render_prim_capsule_vtx_num] * render_prim_capsule_vtx_lst[n];

		vertex[n].vx = vtx[0];
		vertex[n].vy = vtx[1];
		vertex[n].vz = vtx[2];
		vertex[n].cr = _color.GetX();
		vertex[n].cg = _color.GetY();
		vertex[n].cb = _color.GetZ();
		vertex[n].ca = _color.GetW();
	}

	memcpy(&_vertex_list[_vertex_start_idx], vertex, sizeof(vertex));
	for (u32 i = 0; i < ARRAYOF(render_prim_capsule_idx_lst); ++i)
	{
		_idx_list[i + _idx_list_start_idx] = render_prim_capsule_idx_lst[i] + _vertex_start_idx;
	}
}

// 頂点数取得
u32		CRenderPrimElm::CCapsule::GetLineVertexNum() const
{
	return render_prim_capsule_vtx_num;
}

// インデックス数取得
u32		CRenderPrimElm::CCapsule::GetLineIndexNum() const
{
	return render_prim_capsule_idx_num;
}

#pragma endregion

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	扇
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma region Fan

//	描画
void	CRenderPrimElm::CFan::RenderFunc(CRenderer* _pRenderer, const CVec4& _color)	const
{
#if 1
	VERTEX	vertex[sizeof(render_prim_fan_vtx_lst) / sizeof(const CVec4&)];
	for (u32 n = 0; n < render_prim_fan_vtx_num; n++)
	{
		CVec4	vtx = pst * render_prim_fan_vtx_lst[n];
		vertex[n].vx = vtx[0];
		vertex[n].vy = vtx[1];
		vertex[n].vz = vtx[2];
		vertex[n].cr = _color.GetX();
		vertex[n].cg = _color.GetY();
		vertex[n].cb = _color.GetZ();
		vertex[n].ca = _color.GetW();
	}

	// 頂点データ設定
	const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
	_pRenderer->SetVertex(pVtxBuf);

	// インデックスデータ設定
	const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(render_prim_fan_idx_lst[0]), ARRAYOF(render_prim_fan_idx_lst), render_prim_fan_idx_lst);
	_pRenderer->SetIndex(pIdxBuf);

	//描画
	_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_LINES, render_prim_fan_idx_num);
#endif
}

//  メッシュを設定する処理
void	CRenderPrimElm::CFan::LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	VERTEX	vertex[sizeof(render_prim_fan_vtx_lst) / sizeof(const CVec4&)];

	const f32	rad = pst[3].GetX();	// 角度
	//	上面・底面
	u32	count = 0;
	const u32	offset[2] = { 0, 2 * RENDER_PRIM_DIV + 1 };
	const f32	offsetHeight[2] = { 1.f, -1.f };
	for (u32 rh = 0; rh < 2; ++rh)
	{
		CVec4	vtx;
		vtx = pst * render_prim_fan_vtx_lst[offset[rh]];
		vertex[count].vx = vtx[0];
		vertex[count].vy = vtx[1];
		vertex[count].vz = vtx[2];
		vertex[count].cr = _color.GetX();
		vertex[count].cg = _color.GetY();
		vertex[count].cb = _color.GetZ();
		vertex[count].ca = _color.GetW();
		++count;
		for (u32 rv = 0; rv < 2 * RENDER_PRIM_DIV; ++rv)
		{
			f32	v_rad = (rad * rv / ((f32)RENDER_PRIM_DIV * 2.f - 1.f)) - (rad * 0.5f);
			f32	v_sin = sinf(v_rad);
			f32	v_cos = cosf(v_rad);

			vtx = pst * CVec4(v_sin, offsetHeight[rh], v_cos, 1.f);
			vertex[count].vx = vtx[0];
			vertex[count].vy = vtx[1];
			vertex[count].vz = vtx[2];
			vertex[count].cr = _color.GetX();
			vertex[count].cg = _color.GetY();
			vertex[count].cb = _color.GetZ();
			vertex[count].ca = _color.GetW();
			++count;
		}
	}

	memcpy(&_vertex_list[_vertex_start_idx], vertex, sizeof(vertex));
	for (u32 i = 0; i < ARRAYOF(render_prim_fan_idx_lst); ++i)
	{
		_idx_list[i + _idx_list_start_idx] = render_prim_fan_idx_lst[i] + _vertex_start_idx;
	}
}

// 頂点数取得
u32		CRenderPrimElm::CFan::GetLineVertexNum() const
{
	return ARRAYOF(render_prim_fan_vtx_lst);
}

// インデックス数取得
u32		CRenderPrimElm::CFan::GetLineIndexNum() const
{
	return ARRAYOF(render_prim_fan_idx_lst);
}

#pragma endregion

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	空間軸
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma region Axis

//	描画
void	CRenderPrimElm::CAxis::RenderFunc(CRenderer* _pRenderer, const CVec4& _color)	const
{
#if 1

	VERTEX vertex[6];
	u32 index[6];

	LineMeshSetFunc(vertex, index, 0, 0, _color);

	// 頂点データ設定
	const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
	_pRenderer->SetVertex(pVtxBuf);

	// インデックスデータ設定
	const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(index[0]), ARRAYOF(index), index);
	_pRenderer->SetIndex(pIdxBuf);

	//描画
	_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_LINES, ARRAYOF(index));
#endif
}

//  メッシュを設定する処理
void	CRenderPrimElm::CAxis::LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	CVec4	vX = pst * CVec4(1.f, 0.f, 0.f, 1.f);
	CVec4	vY = pst * CVec4(0.f, 1.f, 0.f, 1.f);
	CVec4	vZ = pst * CVec4(0.f, 0.f, 1.f, 1.f);
	CVec3	trans;
	pst.GetTrans(trans);

	VERTEX vertex[] =
	{
		{	vX.GetX(), vX.GetY(), vX.GetZ(),				1.f, 0.f, 0.f, _color.GetW(),	},
		{	trans.GetX(), trans.GetY(), trans.GetZ(),		1.f, 0.f, 0.f, _color.GetW(),	},
		{	vY.GetX(), vY.GetY(), vY.GetZ(),				0.f, 1.f, 0.f, _color.GetW(),	},
		{	trans.GetX(), trans.GetY(), trans.GetZ(),		0.f, 1.f, 0.f, _color.GetW(),	},
		{	vZ.GetX(), vZ.GetY(), vZ.GetZ(),				0.f, 0.f, 1.f, _color.GetW(),	},
		{	trans.GetX(), trans.GetY(), trans.GetZ(),		0.f, 0.f, 1.f, _color.GetW(),	},
	};

	u16 index[] =
	{
		0, 1, 2, 3, 4, 5,
	};

	memcpy(&_vertex_list[_vertex_start_idx], vertex, sizeof(vertex));
	for (u32 i = 0; i < ARRAYOF(index); ++i)
	{
		_idx_list[i + _idx_list_start_idx] = index[i] + _vertex_start_idx;
	}
}

#pragma endregion

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	三角形（塗りつぶし）
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma region Triangle Fill

//	描画
void	CRenderPrimElm::CTriangleFill::RenderFunc(CRenderer* _pRenderer, const CVec4& _color)	const
{
#if 1
	// 塗りつぶし描画
	{
		VERTEX vertex[3];
		u32 index[3];

		// メッシュ準備
		FillMeshSetFunc(vertex, index, 0, 0, _color);

		// 頂点データ設定
		const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
		_pRenderer->SetVertex(pVtxBuf);

		// インデックスデータ設定
		const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(index[0]), ARRAYOF(index), index);
		_pRenderer->SetIndex(pIdxBuf);

		//描画
		_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_TRIANGLES, ARRAYOF(index));
	}

	// 枠線描画
	if (m_color_edge.GetW() > 0.0f)
	{
		VERTEX vertex[3];
		u32 index[6];

		// メッシュ準備
		LineMeshSetFunc(vertex, index, 0, 0, _color);

		// 頂点データ設定
		const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
		_pRenderer->SetVertex(pVtxBuf);

		// インデックスデータ設定
		const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(index[0]), ARRAYOF(index), index);
		_pRenderer->SetIndex(pIdxBuf);

		//描画
		_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_LINES, ARRAYOF(index));
	}
#endif
}

//  メッシュを設定する処理
void	CRenderPrimElm::CTriangleFill::LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	if (m_color_edge.GetW() > 0.0f)
	{
		VERTEX vertex[] =
		{
			{	pos[0].GetX(), pos[0].GetY(), pos[0].GetZ(),	m_color_edge.GetX(), m_color_edge.GetY(), m_color_edge.GetZ(), m_color_edge.GetW(),	},
			{	pos[1].GetX(), pos[1].GetY(), pos[1].GetZ(),	m_color_edge.GetX(), m_color_edge.GetY(), m_color_edge.GetZ(), m_color_edge.GetW(),	},
			{	pos[2].GetX(), pos[2].GetY(), pos[2].GetZ(),	m_color_edge.GetX(), m_color_edge.GetY(), m_color_edge.GetZ(), m_color_edge.GetW(),	},
		};

		u16 index[] =
		{
			0, 1,  1, 2,  2, 0
		};

		for (u32 i = 0; i < ARRAYOF(vertex); ++i)
		{
			_vertex_list[i + _vertex_start_idx] = vertex[i];
		}

		for (u32 i = 0; i < ARRAYOF(index); ++i)
		{
			_idx_list[i + _idx_list_start_idx] = index[i] + _vertex_start_idx;
		}
	}
}

// 塗りつぶしメッシュ設定
void	CRenderPrimElm::CTriangleFill::FillMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	VERTEX vertex[] =
	{
		{	pos[0].GetX(), pos[0].GetY(), pos[0].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	pos[1].GetX(), pos[1].GetY(), pos[1].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	pos[2].GetX(), pos[2].GetY(), pos[2].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
	};

	u16 index[] =
	{
		0, 1, 2,
	};

	memcpy(&_vertex_list[_vertex_start_idx], vertex, sizeof(vertex));
	for (u32 i = 0; i < ARRAYOF(index); ++i)
	{
		_idx_list[i + _idx_list_start_idx] = index[i] + _vertex_start_idx;
	}
}

#pragma endregion

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	箱（塗りつぶし）
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma region BoxFill

//	描画
void	CRenderPrimElm::CBoxFill::RenderFunc(CRenderer* _pRenderer, const CVec4& _color)	const
{

#if 1

	// 塗りつぶし
	{
		VERTEX vertex[VTX_NUM];
		u32 index[FILL_IND_NUM];

		// メッシュ準備
		FillMeshSetFunc(vertex, index, 0, 0, _color);

		// 頂点データ設定
		const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
		_pRenderer->SetVertex(pVtxBuf);

		// インデックスデータ設定
		const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(index[0]), ARRAYOF(index), index);
		_pRenderer->SetIndex(pIdxBuf);

		//描画
		_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_TRIANGLES, ARRAYOF(index));
	}

	// 線の描画
	{
		VERTEX vertex[VTX_NUM];
		u32 index[LINE_IND_NUM];

		// メッシュ準備
		LineMeshSetFunc(vertex, index, 0, 0, _color);

		// 頂点データ設定
		const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
		_pRenderer->SetVertex(pVtxBuf);

		// インデックスデータ設定
		const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(index[0]), ARRAYOF(index), index);
		_pRenderer->SetIndex(pIdxBuf);

		//描画
		_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_LINES, ARRAYOF(index));
	}
#endif
}

// ラインメッシュ設定
void	CRenderPrimElm::CBoxFill::LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	CVec4	vtx[8] =
	{
		pst * CVec4(+1.f, +1.f, +1.f, 1.f),
		pst * CVec4(-1.f, +1.f, +1.f, 1.f),
		pst * CVec4(-1.f, +1.f, -1.f, 1.f),
		pst * CVec4(+1.f, +1.f, -1.f, 1.f),
		pst * CVec4(+1.f, -1.f, +1.f, 1.f),
		pst * CVec4(-1.f, -1.f, +1.f, 1.f),
		pst * CVec4(-1.f, -1.f, -1.f, 1.f),
		pst * CVec4(+1.f, -1.f, -1.f, 1.f),
	};

	VERTEX vertex[] =
	{
		{	vtx[0].GetX(), vtx[0].GetY(), vtx[0].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	vtx[1].GetX(), vtx[1].GetY(), vtx[1].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	vtx[2].GetX(), vtx[2].GetY(), vtx[2].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	vtx[3].GetX(), vtx[3].GetY(), vtx[3].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	vtx[4].GetX(), vtx[4].GetY(), vtx[4].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	vtx[5].GetX(), vtx[5].GetY(), vtx[5].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	vtx[6].GetX(), vtx[6].GetY(), vtx[6].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	vtx[7].GetX(), vtx[7].GetY(), vtx[7].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
	};

	u16 index[] =
	{
		0, 1,  1, 2,  2, 3,  3, 0,
		7, 6,  6, 5,  5, 4,  4, 7,
		0, 4,  1, 5,  2, 6,  3, 7,
	};

	memcpy(&_vertex_list[_vertex_start_idx], vertex, sizeof(vertex));
	for (u32 i = 0; i < ARRAYOF(index); ++i)
	{
		_idx_list[i + _idx_list_start_idx] = index[i] + _vertex_start_idx;
	}
}

// 塗りつぶしメッシュ設定
void	CRenderPrimElm::CBoxFill::FillMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	CVec4	vtx[8] =
	{
		pst * CVec4(+1.f, +1.f, +1.f, 1.f),
		pst * CVec4(-1.f, +1.f, +1.f, 1.f),
		pst * CVec4(-1.f, +1.f, -1.f, 1.f),
		pst * CVec4(+1.f, +1.f, -1.f, 1.f),
		pst * CVec4(+1.f, -1.f, +1.f, 1.f),
		pst * CVec4(-1.f, -1.f, +1.f, 1.f),
		pst * CVec4(-1.f, -1.f, -1.f, 1.f),
		pst * CVec4(+1.f, -1.f, -1.f, 1.f),
	};

	VERTEX vertex[] =
	{
		{	vtx[0].GetX(), vtx[0].GetY(), vtx[0].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	vtx[1].GetX(), vtx[1].GetY(), vtx[1].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	vtx[2].GetX(), vtx[2].GetY(), vtx[2].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	vtx[3].GetX(), vtx[3].GetY(), vtx[3].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	vtx[4].GetX(), vtx[4].GetY(), vtx[4].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	vtx[5].GetX(), vtx[5].GetY(), vtx[5].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	vtx[6].GetX(), vtx[6].GetY(), vtx[6].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	vtx[7].GetX(), vtx[7].GetY(), vtx[7].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
	};

	u16 index[] =
	{
		0, 1, 2,	2, 3, 0,
		7, 6, 5,	5, 4, 7,
		4, 5, 1,	1, 0, 4,
		5, 6, 2,	2, 1, 5,
		6, 7, 3,	3, 2, 6,
		7, 4, 0,	0, 3, 7,
	};

	memcpy(&_vertex_list[_vertex_start_idx], vertex, sizeof(vertex));
	for (u32 i = 0; i < ARRAYOF(index); ++i)
	{
		_idx_list[i + _idx_list_start_idx] = index[i] + _vertex_start_idx;
	}
}
#pragma endregion

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	スプライト）
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma region Sprite

//	描画
void	CRenderPrimElm::CSprite::RenderFunc(CRenderer* _pRenderer, const CVec4& _color)	const
{
#if 0
	///行列の取得
	CMtx44 old_mat = CSystemUtility::GetClearScreen()->GetDrawClearProp().mtxView;
	CMtx44 old_projmat = CSystemUtility::GetClearScreen()->GetDrawClearProp().mtxProj;


	CVec3 vOffs(0.f);
	if (posFlag & POS_LEFT) { vOffs += CVec3(1.0f, 0.0f, 0.f); }
	if (posFlag & POS_RIGHT) { vOffs += CVec3(-1.0f, 0.0f, 0.f); }
	if (posFlag & POS_TOP) { vOffs += CVec3(0.0f, -1.0f, 0.f); }
	if (posFlag & POS_BOTTOM) { vOffs += CVec3(0.0f, 1.0f, 0.f); }

	// ベースポリゴン設定
	CVec3 basePolyPos[] =
	{
		CVec3(-1.0f, -1.0f, 0.f) + vOffs,
		CVec3(-1.0f,  1.0f, 0.f) + vOffs,
		CVec3(1.0f, -1.0f, 0.f) + vOffs,
		CVec3(1.0f,  1.0f, 0.f) + vOffs,
	};

	// マトリクス計算
	{
		CMtx44 mtx = CMtx44::Identity;
		CVec3 vScale = scale;
		// 回転あり
		if (rot != 0.f) {
			mtx = CMtx44::rotationZ(rot);
			const SYSTEM_INFO& info = CSystemUtility::GetSystemInfo();
			vScale = meMulPerElem(vScale, CVec3(info.fScreenWidth, info.fScreenHeight, 1.f));
			// スケール合成
			mtx = appendScale(mtx, vScale);

			// 回転後にスケールを戻す
			mtx = CMtx44::scale(CVec3(1.f / info.fScreenWidth, 1.f / info.fScreenHeight, 1.f)) * mtx;
		}
		else {
			mtx = appendScale(mtx, vScale);
		}

		// 描画位置
		CVec4 vDrawPos = old_projmat * old_mat * CVec4(pos, 1.f);
		CVec4 vDrawPos2 = old_mat * CVec4(pos, 1.f) + CVec4(0.f, 0.f, zOffs, 0.f);

		//vDrawPos = old_projmat * vDrawPos;
		vDrawPos2 = old_projmat * vDrawPos2;

		vDrawPos.setXYZ(vDrawPos.GetXYZ() / vDrawPos.GetW() + CVec3(xOffs, -yOffs, 0.f));
		// ゼット値だけ別途書き換え
		vDrawPos.setZ(vDrawPos2.GetZ() / vDrawPos2.GetW());
		mtx.setTranslation(vDrawPos.GetXYZ());

		// マトリクス初期化
		_pRenderer->LoadMatrix(ME_WORLD_VIEW, mtx);
		_pRenderer->LoadMatrix(ME_VIEW_PROJ, CMtx44::Identity);
		_pRenderer->FlushWorldMatrix();
		_pRenderer->FlushLocalMatrix();
	}

	VERTEX vertex[] =
	{
		{	basePolyPos[0].GetX(), basePolyPos[0].GetY(), basePolyPos[0].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	basePolyPos[1].GetX(), basePolyPos[1].GetY(), basePolyPos[1].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	basePolyPos[2].GetX(), basePolyPos[2].GetY(), basePolyPos[2].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
		{	basePolyPos[3].GetX(), basePolyPos[3].GetY(), basePolyPos[3].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(),	},
	};

	u16 index[] =
	{
		0, 1, 2, 3
	};


	//頂点データ メモリ指定
	_pRenderer->SetVertexData(vertex, sizeof(vertex), sizeof(VERTEX), render_prim_dec);

	//描画
	_pRenderer->DrawIndexedPrimitive(ME_TRIANGLE_STRIP, index, ME_INDEX16, sizeof(index) / sizeof(u16));

	_pRenderer->LoadMatrix(ME_WORLD_VIEW, old_mat);
	_pRenderer->LoadMatrix(ME_VIEW_PROJ, old_projmat);
	_pRenderer->FlushWorldMatrix();
	_pRenderer->FlushLocalMatrix();
#endif
}

#pragma endregion

//-------------------------------------------------------------------------------------------------
/// 頂点作成
static void	MakeVertex(const f32 _rad, const CMtx44& _rMtx, const CVec4& _vClr, CRenderPrimElm::VERTEX* _vertex, u16* _pIndex, u32& _retVtxNum, u32& _retIdxNum)
{
	u32 prim_num0 = SCast<s32>(fabsf(SCast<float>(toDegree(_rad) / 3.f))) + 2;
	u16 ct = 0;

	CRenderPrimElm::VERTEX* pVtx = _vertex;

	_retIdxNum = 0;

	CVec4 v(0.f, 0.f, 0.f, 1.f);
	v = _rMtx * v;
	pVtx->vx = v[0];
	pVtx->vy = v[1];
	pVtx->vz = v[2];
	pVtx->cr = _vClr[0];
	pVtx->cg = _vClr[1];
	pVtx->cb = _vClr[2];
	pVtx->ca = _vClr[3];
	++pVtx;

	v = _rMtx * CVec4(1.f, 0.f, 0.f, 1.f);
	pVtx->vx = v[0];
	pVtx->vy = v[1];
	pVtx->vz = v[2];
	pVtx->cr = _vClr[0];
	pVtx->cg = _vClr[1];
	pVtx->cb = _vClr[2];
	pVtx->ca = _vClr[3];
	++pVtx;

	f32 sign = _rad > 0 ? 1.f : -1.f;
	for (ct = 2; ct < prim_num0 - 1; ++ct)
	{
		f32 rad = toRadian(SCast<f32>(ct * 3) * sign);
		v = _rMtx * CVec4(cosf(rad), 0.f, sinf(rad), 1.f);
		pVtx->vx = v[0];
		pVtx->vy = v[1];
		pVtx->vz = v[2];
		pVtx->cr = _vClr[0];
		pVtx->cg = _vClr[1];
		pVtx->cb = _vClr[2];
		pVtx->ca = _vClr[3];
		++pVtx;

		(*_pIndex) = 0;
		++_pIndex;
		(*_pIndex) = ct;
		++_pIndex;
		(*_pIndex) = ct - 1;
		++_pIndex;
		_retIdxNum += 3;
	}

	v = _rMtx * CVec4(cosf(_rad), 0.f, sinf(_rad), 1.f);
	pVtx->vx = v[0];
	pVtx->vy = v[1];
	pVtx->vz = v[2];
	pVtx->cr = _vClr[0];
	pVtx->cg = _vClr[1];
	pVtx->cb = _vClr[2];
	pVtx->ca = _vClr[3];
	++pVtx;

	(*_pIndex) = 0;
	++_pIndex;
	(*_pIndex) = ct;
	++_pIndex;
	(*_pIndex) = ct - 1;
	++_pIndex;
	_retIdxNum += 3;

	_retVtxNum = prim_num0 + 1;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 方向
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma region Dir

//-------------------------------------------------------------------------------------------------
/// 方向 RenderFunc
void	CRenderPrimElm::CDir::RenderFunc(CRenderer* _pRenderer, const CVec4& _color) const
{
	CMtx44 lw = CMtx44::Identity;
	const f32 fScale = Vec::Length(dir);
	if (fScale <= 0.f) { return; }

	Mtx::Scale(lw, CVec3(fScale));
	lw.SetTrans(pos);

	CVec4 vClr = CVec4(1.f, 0, 1.f, 100.f / 255.f) * _color;
	VERTEX vertex[180 / 3 + 3];
	u16	index[180 / 3 * 3];

	// Y軸の回転角
	f32 azimuth = atan2f(dir[2], dir[0]);

	// ラスタライザー設定
	{
		RASTERIZER_DESC desc;
		desc.cullFace = GFX_CULL_FACE_NONE;
		const CRasterizer* pRasterizer = _pRenderer->CreateAndCacheRasterizer(desc);
		Assert(pRasterizer);
		_pRenderer->SetRasterizer(pRasterizer);
	}

	// ZX平面での回転ポリゴン
	{
		u32 vtxNum = 0;
		u32 idxNum = 0;
		MakeVertex(azimuth, lw, vClr, vertex, index, vtxNum, idxNum);
		libAssert(vtxNum <= ARRAYOF(vertex) && idxNum <= ARRAYOF(index));
		// 頂点データ設定
		const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), vtxNum, vertex);
		_pRenderer->SetVertex(pVtxBuf);
		// インデックスデータ設定
		const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(index[0]), idxNum, index);
		_pRenderer->SetIndex(pIdxBuf);
		// 描画
		_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_TRIANGLES, idxNum);
	}

	// XY平面での回転ポリゴン
	{
		// Y軸の回転角
		f32 f = dir[1] / fScale;
		f32 altitude = PI_H;
		if (f >= 1.f)
		{
			altitude = PI_H;
		}
		else if (f <= -1.f)
		{
			altitude = -PI_H;
		}
		else
		{
			altitude = asinf(f);
		}

		vClr[1] = _color[1]; vClr[2] = 0.f;
		u32 vtxNum = 0;
		u32 idxNum = 0;

		CMtx44 mtxRotX;
		Mtx::RotateX(mtxRotX, -PI_H);
		CMtx44 mtxRotY;
		Mtx::RotateY(mtxRotY, -azimuth);
		mtxRotX = lw * mtxRotY * mtxRotX;

		MakeVertex(altitude, mtxRotX, vClr, vertex, index, vtxNum, idxNum);
		libAssert(vtxNum <= ARRAYOF(vertex) && idxNum <= ARRAYOF(index));
		// 頂点データ設定
		const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), vtxNum, vertex);
		_pRenderer->SetVertex(pVtxBuf);
		// インデックスデータ設定
		const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(index[0]), idxNum, index);
		_pRenderer->SetIndex(pIdxBuf);
		// 描画
		_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_TRIANGLES, idxNum);
	}

	//三軸表示
#if 0
	{
		engine->LoadMatrix(ME_LOCAL_WORLD, lw);
		engine->FlushLocalMatrix();
		engine->SetVertexDataArray(ME_ATTR_POSITION, GetImpl(m_resAxis), sizeof(DLV_VERTEX), 3, ME_f32, 0);
		engine->SetVertexDataArray(ME_ATTR_COLOR0, GetImpl(m_resAxis), sizeof(DLV_VERTEX), 4, ME_NR_UBYTE, 12);
		engine->DrawPrimitiveArray(ME_LINES, 0, 6);
	}
#endif

	//指定したベクトル位置に球表示
	{
		VERTEX dir_vertex[2];

		CVec4 vPos(0.f, 0.f, 0.f, 1.f);
		vPos = lw * vPos;
		dir_vertex[0].vx = vPos[0];
		dir_vertex[0].vy = vPos[1];
		dir_vertex[0].vz = vPos[2];
		dir_vertex[0].cr = 1.f;
		dir_vertex[0].cg = 1.f;
		dir_vertex[0].cb = 0.f;
		dir_vertex[0].ca = 1.f;

		vPos = lw * CVec4(dir / fScale, 1.f);
		dir_vertex[1].vx = vPos[0];
		dir_vertex[1].vy = vPos[1];
		dir_vertex[1].vz = vPos[2];
		dir_vertex[1].cr = 1.f;
		dir_vertex[1].cg = 1.f;
		dir_vertex[1].cb = 0.f;
		dir_vertex[1].ca = 1.f;

		// 頂点データ設定
		const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(dir_vertex[0]), ARRAYOF(dir_vertex), dir_vertex);
		_pRenderer->SetVertex(pVtxBuf);
		// 描画
		_pRenderer->DrawPrim(GFX_PRIMITIVE_LINES, ARRAYOF(dir_vertex));
	}

#if 0
	DLV_VERTEX pt_vertex[] =
	{
		{ (f32)m_elems[i].dir.GetX(), (f32)m_elems[i].dir.GetY(), (f32)m_elems[i].dir.GetZ(), 255, 255, 0, 255 },
	};
	engine->SetVertexData(pt_vertex, sizeof(pt_vertex), sizeof(DLV_VERTEX), decl);
	cell::Gcm::Inline::cellGcmSetPointSize(5.0f);//エンジンに追加したら消します
	engine->DrawPrimitiveArray(ME_POINTS, 0, 1);
#endif
}

#pragma endregion

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	箱 (領域に入ったポリゴン加算塗りつぶし)
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma region BoxAreaFill

//	描画
void	CRenderPrimElm::CBoxAreaFill::RenderFunc(CRenderer* _pRenderer, const CVec4& _color, const CTextureBuffer* _pDepthTex) const
{
#ifdef RENDER_PRIM_FILL
#if 0
	// 線の描画
	{
		VERTEX vertex[VTX_NUM];
		u32 index[LINE_IND_NUM];

		// メッシュ準備
		LineMeshSetFunc(vertex, index, 0, 0, _color);

		// 頂点データ設定
		const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
		_pRenderer->SetVertex(pVtxBuf);
		// インデックスデータ設定
		const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(index[0]), ARRAYOF(index), index);
		_pRenderer->SetIndex(pIdxBuf);
		//描画
		_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_LINES, ARRAYOF(index));
	}
#endif

	// 塗りつぶし
	{
		const CRenderTarget* pRT = _pRenderer->GetCurrentRenderTarget();
		if (pRT == NULL) { return; }

		// メッシュ準備
		VERTEX vertex[VTX_NUM];
		u32 index[FILL_IND_NUM];
		FillMeshSetFunc(vertex, index, 0, 0, _color);

		// 頂点データ設定
		const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
		_pRenderer->SetVertex(pVtxBuf);
		// インデックスデータ設定
		const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(index[0]), ARRAYOF(index), index);
		_pRenderer->SetIndex(pIdxBuf);

		// シェーダ属性切り替え
		_pRenderer->SetAttribute(SCRC32("BoxAreaFill"));
		_pRenderer->PushRenderState();
		_pRenderer->PushRenderTarget(pRT, 1 << 0, false);
		{
			_pRenderer->SetRenderState(RS_DEPTH_TEST, RS_FALSE);
			_pRenderer->SetRenderState(RS_DEPTH_WRITE, RS_FALSE);
			_pRenderer->SetRenderState(RS_CULL_FACE, RS_CULL_BACK);
			// 加算合成
			_pRenderer->SetRenderState(RS_BLEND_ENABLE, RS_TRUE);
			_pRenderer->SetRenderState(RS_ALPHA_TEST, RS_FALSE);
			_pRenderer->SetRenderState(RS_BLEND_OP_COLOR, RS_ADD);
			_pRenderer->SetRenderState(RS_BLEND_FUNC_SRC_COLOR, RS_SRC_ALPHA);
			_pRenderer->SetRenderState(RS_BLEND_FUNC_DST_COLOR, RS_INV_SRC_ALPHA);
			_pRenderer->SetRenderState(RS_BLEND_FUNC_SRC_ALPHA, RS_ZERO);
			_pRenderer->SetRenderState(RS_BLEND_FUNC_DST_ALPHA, RS_ONE);
			// デプステクスチャ設定
			libAssert(_pDepthTex);
			_pRenderer->SetTexture(GFX_SHADER_PIXEL, TEXTURE_SLOT_00, _pDepthTex);
			//描画
			_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_TRIANGLES, ARRAYOF(index));
		}
		_pRenderer->PopRenderTarget();
		_pRenderer->PopRenderState();
	}
#endif // RENDER_PRIM_FILL
}

// ラインメッシュ設定
void	CRenderPrimElm::CBoxAreaFill::LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	if (_vertex_list == NULL) { return; }
	if (_idx_list == NULL) { return; }

	// 座標
	CVec4 vtx[8] =
	{
		pst * CVec4(+1.f, +1.f, +1.f, 1.f),	// 右上前
		pst * CVec4(-1.f, +1.f, +1.f, 1.f),	// 左上前
		pst * CVec4(-1.f, +1.f, -1.f, 1.f),	// 左上後
		pst * CVec4(+1.f, +1.f, -1.f, 1.f),	// 右上後
		pst * CVec4(+1.f, -1.f, +1.f, 1.f),	// 右下前
		pst * CVec4(-1.f, -1.f, +1.f, 1.f),	// 左下前
		pst * CVec4(-1.f, -1.f, -1.f, 1.f),	// 左下後
		pst * CVec4(+1.f, -1.f, -1.f, 1.f),	// 右下後
	};

	for (u32 i = 0; i < 8; ++i)
	{
		// 座標コピー
		_vertex_list[i + _vertex_start_idx].vx = vtx[i].GetX();
		_vertex_list[i + _vertex_start_idx].vy = vtx[i].GetY();
		_vertex_list[i + _vertex_start_idx].vz = vtx[i].GetZ();

		// カラーコピー
		Vec::Copy(&(_vertex_list[i + _vertex_start_idx].cr), _color);
	}

	static const  u16 index[] =
	{
		0, 1,	1, 2,	2, 3,	3, 0,
		4, 5,	5, 6,	6, 7,	7, 4,
		0, 4,	1, 5,	2, 6,	3, 7,
	};

	for (u32 i = 0; i < ARRAYOF(index); ++i)
	{
		_idx_list[i + _idx_list_start_idx] = index[i] + _vertex_start_idx;
	}
}

void	CRenderPrimElm::CBoxAreaFill::FillMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	CVec4	vtx[8] =
	{
		pst * CVec4(+1.f, +1.f, +1.f, 1.f),
		pst * CVec4(-1.f, +1.f, +1.f, 1.f),
		pst * CVec4(-1.f, +1.f, -1.f, 1.f),
		pst * CVec4(+1.f, +1.f, -1.f, 1.f),
		pst * CVec4(+1.f, -1.f, +1.f, 1.f),
		pst * CVec4(-1.f, -1.f, +1.f, 1.f),
		pst * CVec4(-1.f, -1.f, -1.f, 1.f),
		pst * CVec4(+1.f, -1.f, -1.f, 1.f),
	};
	VERTEX vertex[] =
	{
		{ vtx[0].GetX(), vtx[0].GetY(), vtx[0].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[1].GetX(), vtx[1].GetY(), vtx[1].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[2].GetX(), vtx[2].GetY(), vtx[2].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[3].GetX(), vtx[3].GetY(), vtx[3].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[4].GetX(), vtx[4].GetY(), vtx[4].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[5].GetX(), vtx[5].GetY(), vtx[5].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[6].GetX(), vtx[6].GetY(), vtx[6].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[7].GetX(), vtx[7].GetY(), vtx[7].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
	};
	u16 index[] =
	{
		0, 1, 2,	2, 3, 0,
		7, 6, 5,	5, 4, 7,
		4, 5, 1,	1, 0, 4,
		5, 6, 2,	2, 1, 5,
		6, 7, 3,	3, 2, 6,
		7, 4, 0,	0, 3, 7,
	};
	memcpy(&_vertex_list[_vertex_start_idx], vertex, sizeof(vertex));
	for (u32 i = 0; i < ARRAYOF(index); ++i)
	{
		_idx_list[i + _idx_list_start_idx] = index[i] + _vertex_start_idx;
	}
}

// 領域塗りつぶし用ユニフォームデータ取得
void	CRenderPrimElm::CBoxAreaFill::GetAreaFillParam(CMtx44& _dst) const
{
	Mtx::Inverse(_dst, pst);
}
#pragma endregion

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	球 (領域に入ったポリゴン加算塗りつぶし)
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma region SphereAreaFill

//	描画
void	CRenderPrimElm::CSphereAreaFill::RenderFunc(CRenderer* _pRenderer, const CVec4& _color, const CTextureBuffer* _pDepthTex) const
{
#ifdef RENDER_PRIM_FILL
#if 0
	// 線の描画
	{
		VERTEX	vertex[sizeof(render_prim_sphere_vtx_lst) / sizeof(const CVec4&)];
		for (u32 n = 0; n < render_prim_sphere_vtx_num; n++)
		{
			CVec4	vtx = pst * render_prim_sphere_vtx_lst[n];
			vertex[n].vx = vtx[0];
			vertex[n].vy = vtx[1];
			vertex[n].vz = vtx[2];
			vertex[n].cr = _color.GetX();
			vertex[n].cg = _color.GetY();
			vertex[n].cb = _color.GetZ();
			vertex[n].ca = _color.GetW();
		}
		// 頂点データ設定
		const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
		_pRenderer->SetVertex(pVtxBuf);
		// インデックスデータ設定
		const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(render_prim_sphere_idx_lst[0]), ARRAYOF(render_prim_sphere_idx_lst), render_prim_sphere_idx_lst);
		_pRenderer->SetIndex(pIdxBuf);
		//描画
		_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_LINES, render_prim_sphere_idx_num);
	}
#endif

	// 塗りつぶし
	{
		const CRenderTarget* pRT = _pRenderer->GetCurrentRenderTarget();
		if (pRT == NULL) { return; }

		// メッシュ準備
		VERTEX vertex[VTX_NUM];
		u32 index[FILL_IND_NUM];
		FillMeshSetFunc(vertex, index, 0, 0, _color);

		// 頂点データ設定
		const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
		_pRenderer->SetVertex(pVtxBuf);
		// インデックスデータ設定
		const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(index[0]), ARRAYOF(index), index);
		_pRenderer->SetIndex(pIdxBuf);

		// シェーダ属性切り替え
		_pRenderer->SetAttribute(SCRC32("SphereAreaFill"));
		_pRenderer->PushRenderState();
		_pRenderer->PushRenderTarget(pRT, 1 << 0, false);
		{
			_pRenderer->SetRenderState(RS_DEPTH_TEST, RS_FALSE);
			_pRenderer->SetRenderState(RS_DEPTH_WRITE, RS_FALSE);
			_pRenderer->SetRenderState(RS_CULL_FACE, RS_CULL_BACK);
			// 加算合成
			_pRenderer->SetRenderState(RS_BLEND_ENABLE, RS_TRUE);
			_pRenderer->SetRenderState(RS_ALPHA_TEST, RS_FALSE);
			_pRenderer->SetRenderState(RS_BLEND_OP_COLOR, RS_ADD);
			_pRenderer->SetRenderState(RS_BLEND_FUNC_SRC_COLOR, RS_SRC_ALPHA);
			_pRenderer->SetRenderState(RS_BLEND_FUNC_DST_COLOR, RS_INV_SRC_ALPHA);
			_pRenderer->SetRenderState(RS_BLEND_FUNC_SRC_ALPHA, RS_ZERO);
			_pRenderer->SetRenderState(RS_BLEND_FUNC_DST_ALPHA, RS_ONE);
			// デプステクスチャ設定
			libAssert(_pDepthTex);
			_pRenderer->SetTexture(GFX_SHADER_PIXEL, TEXTURE_SLOT_00, _pDepthTex);
			//描画
			_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_TRIANGLES, ARRAYOF(index));
		}
		_pRenderer->PopRenderTarget();
		_pRenderer->PopRenderState();
	}
#endif // RENDER_PRIM_FILL
}

//  メッシュを設定する処理
void	CRenderPrimElm::CSphereAreaFill::LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	VERTEX	vertex[sizeof(render_prim_sphere_vtx_lst) / sizeof(const CVec4&)];
	for (u32 n = 0; n < render_prim_sphere_vtx_num; n++)
	{
		CVec4	vtx = pst * render_prim_sphere_vtx_lst[n];
		vertex[n].vx = vtx[0];
		vertex[n].vy = vtx[1];
		vertex[n].vz = vtx[2];
		vertex[n].cr = _color.GetX();
		vertex[n].cg = _color.GetY();
		vertex[n].cb = _color.GetZ();
		vertex[n].ca = _color.GetW();
	}

	memcpy(&_vertex_list[_vertex_start_idx], vertex, sizeof(vertex));
	for (u32 i = 0; i < ARRAYOF(render_prim_sphere_idx_lst); ++i)
	{
		_idx_list[i + _idx_list_start_idx] = render_prim_sphere_idx_lst[i] + _vertex_start_idx;
	}

}

// 頂点数取得
u32		CRenderPrimElm::CSphereAreaFill::GetLineVertexNum() const
{
	return render_prim_sphere_vtx_num;
}

// インデックス数取得
u32		CRenderPrimElm::CSphereAreaFill::GetLineIndexNum() const
{
	return render_prim_sphere_idx_num;
}

// 塗りつぶしメッシュ設定
void	CRenderPrimElm::CSphereAreaFill::FillMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	CVec4	vtx[8] =
	{
		pst * CVec4(+1.f, +1.f, +1.f, 1.f),
		pst * CVec4(-1.f, +1.f, +1.f, 1.f),
		pst * CVec4(-1.f, +1.f, -1.f, 1.f),
		pst * CVec4(+1.f, +1.f, -1.f, 1.f),
		pst * CVec4(+1.f, -1.f, +1.f, 1.f),
		pst * CVec4(-1.f, -1.f, +1.f, 1.f),
		pst * CVec4(-1.f, -1.f, -1.f, 1.f),
		pst * CVec4(+1.f, -1.f, -1.f, 1.f),
	};
	VERTEX vertex[] =
	{
		{ vtx[0].GetX(), vtx[0].GetY(), vtx[0].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[1].GetX(), vtx[1].GetY(), vtx[1].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[2].GetX(), vtx[2].GetY(), vtx[2].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[3].GetX(), vtx[3].GetY(), vtx[3].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[4].GetX(), vtx[4].GetY(), vtx[4].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[5].GetX(), vtx[5].GetY(), vtx[5].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[6].GetX(), vtx[6].GetY(), vtx[6].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[7].GetX(), vtx[7].GetY(), vtx[7].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
	};
	u16 index[] =
	{
		0, 1, 2,	2, 3, 0,
		7, 6, 5,	5, 4, 7,
		4, 5, 1,	1, 0, 4,
		5, 6, 2,	2, 1, 5,
		6, 7, 3,	3, 2, 6,
		7, 4, 0,	0, 3, 7,
	};
	memcpy(&_vertex_list[_vertex_start_idx], vertex, sizeof(vertex));
	for (u32 i = 0; i < ARRAYOF(index); ++i)
	{
		_idx_list[i + _idx_list_start_idx] = index[i] + _vertex_start_idx;
	}
}

// 領域塗りつぶし用ユニフォームデータ取得
void	CRenderPrimElm::CSphereAreaFill::GetAreaFillParam(CMtx44& _dst) const
{
	_dst = pst;
}
#pragma endregion


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	カプセル (領域に入ったポリゴン加算塗りつぶし)
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma region CapsuleAreaFill

//	描画
void	CRenderPrimElm::CCapsuleAreaFill::RenderFunc(CRenderer* _pRenderer, const CVec4& _color, const CTextureBuffer* _pDepthTex) const
{
#ifdef RENDER_PRIM_FILL
#if 0
	// 線の描画
	{
		VERTEX	vertex[ARRAYOF(render_prim_capsule_vtx_lst)];
		u32		index[ARRAYOF(render_prim_capsule_idx_lst)];

		// メッシュ準備
		LineMeshSetFunc(vertex, index, 0, 0, _color);

		// 頂点データ設定
		const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
		_pRenderer->SetVertex(pVtxBuf);

		// インデックスデータ設定
		const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(render_prim_capsule_idx_lst[0]), ARRAYOF(render_prim_capsule_idx_lst), render_prim_capsule_idx_lst);
		_pRenderer->SetIndex(pIdxBuf);

		//描画
		_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_LINES, render_prim_capsule_idx_num);
	}
#endif // 0

	// 塗りつぶし
	{
		const CRenderTarget* pRT = _pRenderer->GetCurrentRenderTarget();
		if (pRT == NULL) { return; }

		// メッシュ準備
		VERTEX vertex[VTX_NUM];
		u32 index[FILL_IND_NUM];
		FillMeshSetFunc(vertex, index, 0, 0, _color);

		// 頂点データ設定
		const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
		_pRenderer->SetVertex(pVtxBuf);
		// インデックスデータ設定
		const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(index[0]), ARRAYOF(index), index);
		_pRenderer->SetIndex(pIdxBuf);

		// シェーダ属性切り替え
		_pRenderer->SetAttribute(SCRC32("CapsuleAreaFill"));
		_pRenderer->PushRenderState();
		_pRenderer->PushRenderTarget(pRT, 1 << 0, false);
		{
			_pRenderer->SetRenderState(RS_DEPTH_TEST, RS_FALSE);
			_pRenderer->SetRenderState(RS_DEPTH_WRITE, RS_FALSE);
			_pRenderer->SetRenderState(RS_CULL_FACE, RS_CULL_BACK);
			// 加算合成
			_pRenderer->SetRenderState(RS_BLEND_ENABLE, RS_TRUE);
			_pRenderer->SetRenderState(RS_ALPHA_TEST, RS_FALSE);
			_pRenderer->SetRenderState(RS_BLEND_OP_COLOR, RS_ADD);
			_pRenderer->SetRenderState(RS_BLEND_FUNC_SRC_COLOR, RS_SRC_ALPHA);
			_pRenderer->SetRenderState(RS_BLEND_FUNC_DST_COLOR, RS_INV_SRC_ALPHA);
			_pRenderer->SetRenderState(RS_BLEND_FUNC_SRC_ALPHA, RS_ZERO);
			_pRenderer->SetRenderState(RS_BLEND_FUNC_DST_ALPHA, RS_ONE);
			// デプステクスチャ設定
			libAssert(_pDepthTex);
			_pRenderer->SetTexture(GFX_SHADER_PIXEL, TEXTURE_SLOT_00, _pDepthTex);
			//描画
			_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_TRIANGLES, ARRAYOF(index));
		}
		_pRenderer->PopRenderTarget();
		_pRenderer->PopRenderState();
	}
#endif // RENDER_PRIM_FILL
}

//  メッシュを設定する処理
void	CRenderPrimElm::CCapsuleAreaFill::LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	CMtx44	mtx = CMtx44::Identity;
	mtx[3] = CVec4((pos[0] + pos[1]).GetXYZ() * 0.5f, 1.f);

	if (Vec::Length2(pos[1] - mtx[3]) > 0.f) {
		mtx[1] = Vec::Normalize(CVec4((pos[1] - mtx[3]).GetXYZ(), 0.f)) * rad;
	}
	else {
		mtx[1][1] *= rad;
	}
	if ((mtx[1][0] == 0.f) && (mtx[1][2] == 0.f))
	{
		mtx[0] = CVec4(rad, 0.f, 0.f, 0.f);
		mtx[2] = CVec4(0.f, 0.f, rad, 0.f);
	}
	else {
		mtx[2] = Vec::Normalize(CVec4(Vec::Cross(CVec3(0.f, 1.f, 0.f), mtx[1].GetXYZ()), 0.f)) * rad;
		mtx[0] = Vec::Normalize(CVec4(Vec::Cross(mtx[1].GetXYZ(), mtx[2].GetXYZ()), 0.f)) * rad;
	}

	CMtx44	pst[2] = { mtx, mtx };
	pst[0][3] = pos[1];
	pst[1][3] = pos[0];

#ifdef MATRIX_COLUMN_MAJOR
	// 列優先のため転置しとく
	Mtx::Transpose(pst[0]);
	Mtx::Transpose(pst[1]);
#endif // MATRIX_COLUMN_MAJOR

	VERTEX	vertex[sizeof(render_prim_capsule_vtx_lst) / sizeof(const CVec4&)];
	for (u32 n = 0; n < render_prim_capsule_vtx_num; n++)
	{
		CVec4	vtx = pst[(2 * n) >= render_prim_capsule_vtx_num] * render_prim_capsule_vtx_lst[n];

		vertex[n].vx = vtx[0];
		vertex[n].vy = vtx[1];
		vertex[n].vz = vtx[2];
		vertex[n].cr = _color.GetX();
		vertex[n].cg = _color.GetY();
		vertex[n].cb = _color.GetZ();
		vertex[n].ca = _color.GetW();
	}

	memcpy(&_vertex_list[_vertex_start_idx], vertex, sizeof(vertex));
	for (u32 i = 0; i < ARRAYOF(render_prim_capsule_idx_lst); ++i)
	{
		_idx_list[i + _idx_list_start_idx] = render_prim_capsule_idx_lst[i] + _vertex_start_idx;
	}
}

// 頂点数取得
u32		CRenderPrimElm::CCapsuleAreaFill::GetLineVertexNum() const
{
	return render_prim_capsule_vtx_num;
}

// インデックス数取得
u32		CRenderPrimElm::CCapsuleAreaFill::GetLineIndexNum() const
{
	return render_prim_capsule_idx_num;
}

// 塗りつぶしメッシュ設定
void	CRenderPrimElm::CCapsuleAreaFill::FillMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	CMtx44 mtx = CMtx44::Identity;
	CVec3 v0To1 = pos[1].GetXYZ() - pos[0].GetXYZ();
	f32 l0To1 = Vec::Length(v0To1);
	CVec3 nrm = v0To1 / l0To1;
	CVec3 center;
	Vec::Lerp(center, pos[0].GetXYZ(), pos[1].GetXYZ(), 0.5f);
	CVec3 scl(rad, l0To1 + 2.0f * rad, rad);
	CVec3 cross;
	Vec::Cross(cross, CVec3::Up, nrm);
	CQuat quat = CQuat::Identity;
	Rot::Rotate(quat, Vec::Normalize(cross), acosf(Vec::Dot(CVec3::Up, nrm)));
	CSqt sqt(scl, quat, center);
	sqt.GetCalcMtx(mtx);

	CVec4	vtx[8] =
	{
		mtx * CVec4(+1.f, +1.f, +1.f, 1.f),
		mtx * CVec4(-1.f, +1.f, +1.f, 1.f),
		mtx * CVec4(-1.f, +1.f, -1.f, 1.f),
		mtx * CVec4(+1.f, +1.f, -1.f, 1.f),
		mtx * CVec4(+1.f, -1.f, +1.f, 1.f),
		mtx * CVec4(-1.f, -1.f, +1.f, 1.f),
		mtx * CVec4(-1.f, -1.f, -1.f, 1.f),
		mtx * CVec4(+1.f, -1.f, -1.f, 1.f),
	};
	VERTEX vertex[] =
	{
		{ vtx[0].GetX(), vtx[0].GetY(), vtx[0].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[1].GetX(), vtx[1].GetY(), vtx[1].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[2].GetX(), vtx[2].GetY(), vtx[2].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[3].GetX(), vtx[3].GetY(), vtx[3].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[4].GetX(), vtx[4].GetY(), vtx[4].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[5].GetX(), vtx[5].GetY(), vtx[5].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[6].GetX(), vtx[6].GetY(), vtx[6].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[7].GetX(), vtx[7].GetY(), vtx[7].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
	};
	u16 index[] =
	{
		0, 1, 2,	2, 3, 0,
		7, 6, 5,	5, 4, 7,
		4, 5, 1,	1, 0, 4,
		5, 6, 2,	2, 1, 5,
		6, 7, 3,	3, 2, 6,
		7, 4, 0,	0, 3, 7,
	};
	memcpy(&_vertex_list[_vertex_start_idx], vertex, sizeof(vertex));
	for (u32 i = 0; i < ARRAYOF(index); ++i)
	{
		_idx_list[i + _idx_list_start_idx] = index[i] + _vertex_start_idx;
	}
}

// 領域塗りつぶし用ユニフォームデータ取得
void	CRenderPrimElm::CCapsuleAreaFill::GetAreaFillParam(CMtx44& _dst) const
{
	//CVec3 center;
	//pst.GetTrans(center);
	//CVec3 up;
	//Mtx::GetAxisY(up, pst.getUpper());
	//CVec3 nrmUp;
	//Vec::Normalize(nrmUp, up);
	//CVec3 side;
	//Mtx::GetAxisX(side, pst.getUpper());
	//f32 radius = Vec::Length(side);

	//CMtx44 param;
	//param[0] = CVec4(center + up - nrmUp * radius);
	//param[1] = CVec4(center - up + nrmUp * radius);
	//param[2] = CVec4(radius);
	//param[3] = CVec4(radius);
	//_dst = param;
	_dst[0] = pos[0];
	_dst[1] = pos[1];
	_dst[2] = CVec4(rad);
}

#pragma endregion


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	円柱 (領域に入ったポリゴン加算塗りつぶし)
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma region PipeAreaFill

//	描画
void	CRenderPrimElm::CPipeAreaFill::RenderFunc(CRenderer* _pRenderer, const CVec4& _color, const CTextureBuffer* _pDepthTex) const
{
#ifdef RENDER_PRIM_FILL
#if 0
	// 線の描画
	{
		VERTEX	vertex[sizeof(render_prim_pipe_vtx_lst) / sizeof(const CVec4&)];
		for (u32 n = 0; n < render_prim_pipe_vtx_num; n++)
		{
			CVec4	vtx = pst * render_prim_pipe_vtx_lst[n];
			vertex[n].vx = vtx[0];
			vertex[n].vy = vtx[1];
			vertex[n].vz = vtx[2];
			vertex[n].cr = _color.GetX();
			vertex[n].cg = _color.GetY();
			vertex[n].cb = _color.GetZ();
			vertex[n].ca = _color.GetW();
		}
		// 頂点データ設定
		const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
		_pRenderer->SetVertex(pVtxBuf);
		// インデックスデータ設定
		const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(render_prim_pipe_idx_lst[0]), ARRAYOF(render_prim_pipe_idx_lst), render_prim_pipe_idx_lst);
		_pRenderer->SetIndex(pIdxBuf);
		//描画
		_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_LINES, render_prim_pipe_idx_num);
	}
#endif

	// 塗りつぶし
	{
		const CRenderTarget* pRT = _pRenderer->GetCurrentRenderTarget();
		if (pRT == NULL) { return; }

		// メッシュ準備
		VERTEX vertex[VTX_NUM];
		u32 index[FILL_IND_NUM];
		FillMeshSetFunc(vertex, index, 0, 0, _color);

		// 頂点データ設定
		const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
		_pRenderer->SetVertex(pVtxBuf);
		// インデックスデータ設定
		const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(index[0]), ARRAYOF(index), index);
		_pRenderer->SetIndex(pIdxBuf);

		// シェーダ属性切り替え
		_pRenderer->SetAttribute(SCRC32("PipeAreaFill"));
		_pRenderer->PushRenderState();
		_pRenderer->PushRenderTarget(pRT, 1 << 0, false);
		{
			_pRenderer->SetRenderState(RS_DEPTH_TEST, RS_FALSE);
			_pRenderer->SetRenderState(RS_DEPTH_WRITE, RS_FALSE);
			_pRenderer->SetRenderState(RS_CULL_FACE, RS_CULL_BACK);
			// 加算合成
			_pRenderer->SetRenderState(RS_BLEND_ENABLE, RS_TRUE);
			_pRenderer->SetRenderState(RS_ALPHA_TEST, RS_FALSE);
			_pRenderer->SetRenderState(RS_BLEND_OP_COLOR, RS_ADD);
			_pRenderer->SetRenderState(RS_BLEND_FUNC_SRC_COLOR, RS_SRC_ALPHA);
			_pRenderer->SetRenderState(RS_BLEND_FUNC_DST_COLOR, RS_INV_SRC_ALPHA);
			_pRenderer->SetRenderState(RS_BLEND_FUNC_SRC_ALPHA, RS_ZERO);
			_pRenderer->SetRenderState(RS_BLEND_FUNC_DST_ALPHA, RS_ONE);
			// デプステクスチャ設定
			libAssert(_pDepthTex);
			_pRenderer->SetTexture(GFX_SHADER_PIXEL, TEXTURE_SLOT_00, _pDepthTex);
			//描画
			_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_TRIANGLES, ARRAYOF(index));
		}
		_pRenderer->PopRenderTarget();
		_pRenderer->PopRenderState();
	}
#endif // RENDER_PRIM_FILL
}

//  メッシュを設定する処理
void	CRenderPrimElm::CPipeAreaFill::LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	VERTEX	vertex[sizeof(render_prim_pipe_vtx_lst) / sizeof(const CVec4&)];
	for (u32 n = 0; n < render_prim_pipe_vtx_num; n++)
	{
		CVec4	vtx = pst * render_prim_pipe_vtx_lst[n];
		vertex[n].vx = vtx[0];
		vertex[n].vy = vtx[1];
		vertex[n].vz = vtx[2];
		vertex[n].cr = _color.GetX();
		vertex[n].cg = _color.GetY();
		vertex[n].cb = _color.GetZ();
		vertex[n].ca = _color.GetW();
	}

	memcpy(&_vertex_list[_vertex_start_idx], vertex, sizeof(vertex));
	for (u32 i = 0; i < ARRAYOF(render_prim_pipe_idx_lst); ++i)
	{
		_idx_list[i + _idx_list_start_idx] = render_prim_pipe_idx_lst[i] + _vertex_start_idx;
	}
}

// 頂点数取得
u32		CRenderPrimElm::CPipeAreaFill::GetLineVertexNum() const
{
	return ARRAYOF(render_prim_pipe_vtx_lst);
}

// インデックス数取得
u32		CRenderPrimElm::CPipeAreaFill::GetLineIndexNum() const
{
	return ARRAYOF(render_prim_pipe_idx_lst);
}

// 塗りつぶしメッシュ設定
void	CRenderPrimElm::CPipeAreaFill::FillMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	CVec4	vtx[8] =
	{
		pst * CVec4(+1.f, +1.f, +1.f, 1.f),
		pst * CVec4(-1.f, +1.f, +1.f, 1.f),
		pst * CVec4(-1.f, +1.f, -1.f, 1.f),
		pst * CVec4(+1.f, +1.f, -1.f, 1.f),
		pst * CVec4(+1.f, -1.f, +1.f, 1.f),
		pst * CVec4(-1.f, -1.f, +1.f, 1.f),
		pst * CVec4(-1.f, -1.f, -1.f, 1.f),
		pst * CVec4(+1.f, -1.f, -1.f, 1.f),
	};
	VERTEX vertex[] =
	{
		{ vtx[0].GetX(), vtx[0].GetY(), vtx[0].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[1].GetX(), vtx[1].GetY(), vtx[1].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[2].GetX(), vtx[2].GetY(), vtx[2].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[3].GetX(), vtx[3].GetY(), vtx[3].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[4].GetX(), vtx[4].GetY(), vtx[4].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[5].GetX(), vtx[5].GetY(), vtx[5].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[6].GetX(), vtx[6].GetY(), vtx[6].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[7].GetX(), vtx[7].GetY(), vtx[7].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
	};
	u16 index[] =
	{
		0, 1, 2,	2, 3, 0,
		7, 6, 5,	5, 4, 7,
		4, 5, 1,	1, 0, 4,
		5, 6, 2,	2, 1, 5,
		6, 7, 3,	3, 2, 6,
		7, 4, 0,	0, 3, 7,
	};
	memcpy(&_vertex_list[_vertex_start_idx], vertex, sizeof(vertex));
	for (u32 i = 0; i < ARRAYOF(index); ++i)
	{
		_idx_list[i + _idx_list_start_idx] = index[i] + _vertex_start_idx;
	}
}


// 領域塗りつぶし用ユニフォームデータ取得
void	CRenderPrimElm::CPipeAreaFill::GetAreaFillParam(CMtx44& _dst) const
{
	Mtx::Inverse(_dst, pst);
}
#pragma endregion


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	扇 (領域に入ったポリゴン加算塗りつぶし)
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma region FanAreaFill

//	描画
void	CRenderPrimElm::CFanAreaFill::RenderFunc(CRenderer* _pRenderer, const CVec4& _color, const CTextureBuffer* _pDepthTex)	const
{
#ifdef RENDER_PRIM_FILL
#if 0
	// 線の描画
	{
		VERTEX	vertex[sizeof(render_prim_fan_vtx_lst) / sizeof(const CVec4&)];
		for (u32 n = 0; n < render_prim_fan_vtx_num; n++)
		{
			CVec4	vtx = pst * render_prim_fan_vtx_lst[n];
			vertex[n].vx = vtx[0];
			vertex[n].vy = vtx[1];
			vertex[n].vz = vtx[2];
			vertex[n].cr = _color.GetX();
			vertex[n].cg = _color.GetY();
			vertex[n].cb = _color.GetZ();
			vertex[n].ca = _color.GetW();
		}
		// 頂点データ設定
		const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
		_pRenderer->SetVertex(pVtxBuf);
		// インデックスデータ設定
		const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(render_prim_fan_idx_lst[0]), ARRAYOF(render_prim_fan_idx_lst), render_prim_fan_idx_lst);
		_pRenderer->SetIndex(pIdxBuf);
		//描画
		_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_LINES, render_prim_fan_idx_num);
	}
#endif

	// 塗りつぶし
	{
		const CRenderTarget* pRT = _pRenderer->GetCurrentRenderTarget();
		if (pRT == NULL) { return; }

		// メッシュ準備
		VERTEX vertex[VTX_NUM];
		u32 index[FILL_IND_NUM];
		FillMeshSetFunc(vertex, index, 0, 0, _color);

		// 頂点データ設定
		const CVertexBuffer* pVtxBuf = _pRenderer->CreateAndCacheVertex(sizeof(vertex[0]), ARRAYOF(vertex), vertex);
		_pRenderer->SetVertex(pVtxBuf);
		// インデックスデータ設定
		const CIndexBuffer* pIdxBuf = _pRenderer->CreateAndCacheIndex(sizeof(index[0]), ARRAYOF(index), index);
		_pRenderer->SetIndex(pIdxBuf);

		// シェーダ属性切り替え
		_pRenderer->SetAttribute(SCRC32("FanAreaFill"));
		_pRenderer->PushRenderState();
		_pRenderer->PushRenderTarget(pRT, 1 << 0, false);
		{
			_pRenderer->SetRenderState(RS_DEPTH_TEST, RS_FALSE);
			_pRenderer->SetRenderState(RS_DEPTH_WRITE, RS_FALSE);
			_pRenderer->SetRenderState(RS_CULL_FACE, RS_CULL_BACK);
			// 加算合成
			_pRenderer->SetRenderState(RS_BLEND_ENABLE, RS_TRUE);
			_pRenderer->SetRenderState(RS_ALPHA_TEST, RS_FALSE);
			_pRenderer->SetRenderState(RS_BLEND_OP_COLOR, RS_ADD);
			_pRenderer->SetRenderState(RS_BLEND_FUNC_SRC_COLOR, RS_SRC_ALPHA);
			_pRenderer->SetRenderState(RS_BLEND_FUNC_DST_COLOR, RS_INV_SRC_ALPHA);
			_pRenderer->SetRenderState(RS_BLEND_FUNC_SRC_ALPHA, RS_ZERO);
			_pRenderer->SetRenderState(RS_BLEND_FUNC_DST_ALPHA, RS_ONE);
			// デプステクスチャ設定
			libAssert(_pDepthTex);
			_pRenderer->SetTexture(GFX_SHADER_PIXEL, TEXTURE_SLOT_00, _pDepthTex);
			//描画
			_pRenderer->DrawIndexPrim(GFX_PRIMITIVE_TRIANGLES, ARRAYOF(index));
		}
		_pRenderer->PopRenderTarget();
		_pRenderer->PopRenderState();
	}
#endif // RENDER_PRIM_FILL
}

//  メッシュを設定する処理
void	CRenderPrimElm::CFanAreaFill::LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	VERTEX	vertex[sizeof(render_prim_fan_vtx_lst) / sizeof(const CVec4&)];

	const f32	rad = pst[3].GetX();	// 角度
	//	上面・底面
	u32	count = 0;
	const u32	offset[2] = { 0, 2 * RENDER_PRIM_DIV + 1 };
	const f32	offsetHeight[2] = { 1.f, -1.f };
	for (u32 rh = 0; rh < 2; ++rh)
	{
		CVec4	vtx;
		vtx = pst * render_prim_fan_vtx_lst[offset[rh]];
		vertex[count].vx = vtx[0];
		vertex[count].vy = vtx[1];
		vertex[count].vz = vtx[2];
		vertex[count].cr = _color.GetX();
		vertex[count].cg = _color.GetY();
		vertex[count].cb = _color.GetZ();
		vertex[count].ca = _color.GetW();
		++count;
		for (u32 rv = 0; rv < 2 * RENDER_PRIM_DIV; ++rv)
		{
			f32	v_rad = (rad * rv / ((f32)RENDER_PRIM_DIV * 2.f - 1.f)) - (rad * 0.5f);
			f32	v_sin = sinf(v_rad);
			f32	v_cos = cosf(v_rad);

			vtx = pst * CVec4(v_sin, offsetHeight[rh], v_cos, 1.f);
			vertex[count].vx = vtx[0];
			vertex[count].vy = vtx[1];
			vertex[count].vz = vtx[2];
			vertex[count].cr = _color.GetX();
			vertex[count].cg = _color.GetY();
			vertex[count].cb = _color.GetZ();
			vertex[count].ca = _color.GetW();
			++count;
		}
	}

	memcpy(&_vertex_list[_vertex_start_idx], vertex, sizeof(vertex));
	for (u32 i = 0; i < ARRAYOF(render_prim_fan_idx_lst); ++i)
	{
		_idx_list[i + _idx_list_start_idx] = render_prim_fan_idx_lst[i] + _vertex_start_idx;
	}
}

// 頂点数取得
u32		CRenderPrimElm::CFanAreaFill::GetLineVertexNum() const
{
	return ARRAYOF(render_prim_fan_vtx_lst);
}

// インデックス数取得
u32		CRenderPrimElm::CFanAreaFill::GetLineIndexNum() const
{
	return ARRAYOF(render_prim_fan_idx_lst);
}

// 塗りつぶしメッシュ設定
void	CRenderPrimElm::CFanAreaFill::FillMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const
{
	CVec4	vtx[8] =
	{
		pst * CVec4(+1.f, +1.f, +1.f, 1.f),
		pst * CVec4(-1.f, +1.f, +1.f, 1.f),
		pst * CVec4(-1.f, +1.f, -1.f, 1.f),
		pst * CVec4(+1.f, +1.f, -1.f, 1.f),
		pst * CVec4(+1.f, -1.f, +1.f, 1.f),
		pst * CVec4(-1.f, -1.f, +1.f, 1.f),
		pst * CVec4(-1.f, -1.f, -1.f, 1.f),
		pst * CVec4(+1.f, -1.f, -1.f, 1.f),
	};
	VERTEX vertex[] =
	{
		{ vtx[0].GetX(), vtx[0].GetY(), vtx[0].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[1].GetX(), vtx[1].GetY(), vtx[1].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[2].GetX(), vtx[2].GetY(), vtx[2].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[3].GetX(), vtx[3].GetY(), vtx[3].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[4].GetX(), vtx[4].GetY(), vtx[4].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[5].GetX(), vtx[5].GetY(), vtx[5].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[6].GetX(), vtx[6].GetY(), vtx[6].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
		{ vtx[7].GetX(), vtx[7].GetY(), vtx[7].GetZ(),	_color.GetX(), _color.GetY(), _color.GetZ(), _color.GetW(), },
	};
	u16 index[] =
	{
		0, 1, 2,	2, 3, 0,
		7, 6, 5,	5, 4, 7,
		4, 5, 1,	1, 0, 4,
		5, 6, 2,	2, 1, 5,
		6, 7, 3,	3, 2, 6,
		7, 4, 0,	0, 3, 7,
	};
	memcpy(&_vertex_list[_vertex_start_idx], vertex, sizeof(vertex));
	for (u32 i = 0; i < ARRAYOF(index); ++i)
	{
		_idx_list[i + _idx_list_start_idx] = index[i] + _vertex_start_idx;
	}
}

// 領域塗りつぶし用ユニフォームデータ取得
void	CRenderPrimElm::CFanAreaFill::GetAreaFillParam(CMtx44& _dst) const
{
	CMtx44	mtx = pst;
	mtx[3].SetX(0.f);
	Mtx::Inverse(_dst, mtx);
	_dst[3].SetX(cosf(pst[3].GetX() * 0.5f));	// 角度の半分のCOS
}
#pragma endregion


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	CRenderPrimElm
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//	初期化
void	CRenderPrimElm::Initialize()
{
	memset(this, 0, sizeof(CRenderPrimElm));
	m_type = PRIM_ELM_NONE;
	m_ztest = ZTEST_DEFAULT;
	m_flag = ~SCast<u8>(0u);
	m_edgeColor.Set(1.f);
	m_faceColor.Set(1.f);

	render_prim_build(ALL);
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//	色
void	CRenderPrimElm::SetColor(f32 _r, f32 _g, f32 _b, f32 _a)
{
	SetColor(CVec4(_r, _g, _b, _a));
}

void	CRenderPrimElm::SetColor(const CVec3& _clr)
{
	SetEdgeColor(_clr);
	SetFaceColor(CVec4(_clr, 0.5f));
}

void	CRenderPrimElm::SetColor(const CVec4& _clr)
{
	SetEdgeColor(_clr);
	SetFaceColor(CVec4(_clr.GetXYZ(), _clr.GetW() / 2.f));
}

//	エッジ色
void	CRenderPrimElm::SetEdgeColor(f32 _r, f32 _g, f32 _b, f32 _a)
{
	SetEdgeColor(CVec4(_r, _g, _b, _a));
}

void	CRenderPrimElm::SetEdgeColor(const CVec3& _clr)
{
	SetEdgeColor(CVec4(_clr, 1.f));
}

void	CRenderPrimElm::SetEdgeColor(const CVec4& _clr)
{
	m_edgeColor = _clr;
}

//	表面色
void	CRenderPrimElm::SetFaceColor(f32 _r, f32 _g, f32 _b, f32 _a)
{
	SetFaceColor(CVec4(_r, _g, _b, _a));
}

void	CRenderPrimElm::SetFaceColor(const CVec3& _clr)
{
	SetFaceColor(CVec4(_clr, 1.f));
}

void	CRenderPrimElm::SetFaceColor(const CVec4& _clr)
{
	m_faceColor = _clr;
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//	Zテスト
void	CRenderPrimElm::SetZTest(ZTEST ztest)
{
	m_ztest = ztest;
}


//--------------------------------------------------------------------------------------------------

//	線分
void	CRenderPrimElm::SetAsLine(f32 _x0, f32 _y0, f32 _z0, f32 _x1, f32 _y1, f32 _z1)
{
	SetAsLine(CVec3(_x0, _y0, _z0), CVec3(_x1, _y1, _z1));
}

void	CRenderPrimElm::SetAsLine(const CVec3& _p0, const CVec3& _p1)
{
	CLine* tmp = (CLine*)m_data;
	tmp->pos[0] = CVec4(_p0, 1.f);
	tmp->pos[1] = CVec4(_p1, 1.f);
	m_type = PRIM_ELM_LINE;
}

void	CRenderPrimElm::SetAsLine(const CVec4& _p0, const CVec4& _p1)
{
	SetAsLine(_p0.GetXYZ(), _p1.GetXYZ());
}

//--------------------------------------------------------------------------------------------------

//	線分
void	CRenderPrimElm::SetAsTriangle(f32 _x0, f32 _y0, f32 _z0, f32 _x1, f32 _y1, f32 _z1, f32 _x2, f32 _y2, f32 _z2)
{
	SetAsTriangle(CVec3(_x0, _y0, _z0), CVec3(_x1, _y1, _z1), CVec3(_x2, _y2, _z2));
}

void	CRenderPrimElm::SetAsTriangle(const CVec3& _p0, const CVec3& _p1, const CVec3& _p2)
{
	CTriangle* tmp = (CTriangle*)m_data;
	tmp->pos[0] = CVec4(_p0, 1.f);
	tmp->pos[1] = CVec4(_p1, 1.f);
	tmp->pos[2] = CVec4(_p2, 1.f);
	m_type = PRIM_ELM_TRIANGLE;
}

void	CRenderPrimElm::SetAsTriangle(const CVec4& _p0, const CVec4& _p1, const CVec4& _p2)
{
	SetAsTriangle(_p0.GetXYZ(), _p1.GetXYZ(), _p2.GetXYZ());
}

//--------------------------------------------------------------------------------------------------

//	箱
void	CRenderPrimElm::SetAsBox(const CQuat& _qat, const CVec3& _cnt, const CVec3& _scl)
{
	CMtx44 mtx = CMtx44::Identity;
	Mtx::Rotate(mtx, _qat);
	mtx.SetTrans(_cnt);
	SetAsBox(Mtx::Scale(mtx, _scl));
}

void	CRenderPrimElm::SetAsBox(const CQuat& _qat, const CVec3& _cnt, f32 _sx, f32 _sy, f32 _sz)
{
	CMtx44 mtx = CMtx44::Identity;
	Mtx::Rotate(mtx, _qat);
	mtx.SetTrans(_cnt);
	SetAsBox(Mtx::Scale(mtx, CVec3(_sx, _sy, _sz)));
}

void	CRenderPrimElm::SetAsBox(const CQuat& _qat, const CVec3& _cnt, f32 _scl)
{
	SetAsBox(_qat, _cnt, _scl, _scl, _scl);
}

void	CRenderPrimElm::SetAsBox(const CVec3& _cnt, f32 _sx, f32 _sy, f32 _sz)
{
	CMtx44	mtx = CMtx44::Identity;
	mtx.SetTrans(_cnt);
	SetAsBox(Mtx::Scale(mtx, CVec3(_sx, _sy, _sz)));
}

void	CRenderPrimElm::SetAsBox(const CVec3& _cnt, f32 _scl)
{
	SetAsBox(_cnt, _scl, _scl, _scl);
}

void	CRenderPrimElm::SetAsBox(const CVec3& _max, const CVec3& _min)
{
	CMtx44	mtx = CMtx44::Identity;
	mtx.SetTrans((_max + _min) * 0.5f);
	SetAsBox(Mtx::Scale(mtx, (_max - _min) * 0.5f));
}

void	CRenderPrimElm::SetAsBox(const CVec4& _max, const CVec4& _min)
{
	SetAsBox(_max.GetXYZ(), _min.GetXYZ());
}

void	CRenderPrimElm::SetAsBox(const CMtx34& _pst)
{
	CBox* tmp = (CBox*)m_data;
	tmp->pst.SetMtx34(_pst);
	m_type = PRIM_ELM_BOX;
}

void	CRenderPrimElm::SetAsBox(const CMtx44& _pst)
{
	CBox* tmp = (CBox*)m_data;
	tmp->pst = _pst;
	m_type = PRIM_ELM_BOX;
}

//--------------------------------------------------------------------------------------------------

//	円柱
void	CRenderPrimElm::SetAsPipe(const CVec3& _btm, f32 _rad, f32 _hgt)
{
	CMtx44	mtx = CMtx44::Identity;
	CVec3 pos = _btm;
	pos[1] += 0.5f * _hgt;
	mtx.SetTrans(pos);
	SetAsPipe(Mtx::Scale(mtx, CVec3(_rad, 0.5f * _hgt, _rad)));
}

void	CRenderPrimElm::SetAsPipe(const CVec4& _btm, f32 _rad, f32 _hgt)
{
	SetAsPipe(_btm.GetXYZ(), _rad, _hgt);
}

void	CRenderPrimElm::SetAsPipe(const CVec3& _p0, const CVec3& _p1, f32 _rad)
{
	CVec3 pos = (_p0 + _p1) * 0.5f;
	CVec3 vY = _p1 - pos;
	CVec3 vX, vZ;
	if ((vY[0] == 0.f) && (vY[2] == 0.f))
	{
		vX = CVec3(_rad, 0.f, 0.f);
		vZ = CVec3(0.f, 0.f, _rad);
	}
	else {
		CVec3 tmp;
		Vec::Normalize(vZ, Vec::Cross(tmp, CVec3::Up, vY));
		Vec::Normalize(vX, Vec::Cross(tmp, vY, vZ));
		vZ *= _rad;
		vX *= _rad;
	}
	CMtx44 mtx;
	mtx.GetMtx34().SetAxis(vX, vY, vZ, pos);
	SetAsPipe(mtx);
}

void	CRenderPrimElm::SetAsPipe(const CVec4& _p0, const CVec4& _p1, f32 _rad)
{
	SetAsPipe(_p0.GetXYZ(), _p1.GetXYZ(), _rad);
}

void	CRenderPrimElm::SetAsPipe(const CMtx34& _pst)
{
	CPipe* tmp = (CPipe*)m_data;
	tmp->pst.SetMtx34(_pst);
	m_type = PRIM_ELM_PIPE;
}

void	CRenderPrimElm::SetAsPipe(const CMtx44& _pst)
{
	CPipe* tmp = (CPipe*)m_data;
	tmp->pst = _pst;
	m_type = PRIM_ELM_PIPE;
}

//--------------------------------------------------------------------------------------------------

//	球
void	CRenderPrimElm::SetAsSphere(const CQuat& _qat, const CVec3& _cnt, f32 _sx, f32 _sy, f32 _sz)
{
	SetAsSphere(_qat, _cnt, CVec3(_sx, _sy, _sz));
}

void	CRenderPrimElm::SetAsSphere(const CQuat& _qat, const CVec3& _cnt, const CVec3& _scl)
{
	CMtx44	mtx = CMtx44::Identity;
	mtx.SetTrans(_cnt);
	SetAsSphere(Mtx::Scale(mtx, _scl));
}

void	CRenderPrimElm::SetAsSphere(const CVec3& _cnt, f32 _rad)
{
	CMtx44	mtx = CMtx44::Identity;
	mtx.SetTrans(_cnt);
	SetAsSphere(Mtx::Scale(mtx, CVec3(_rad, _rad, _rad)));
}

void	CRenderPrimElm::SetAsSphere(const CVec4& _cnt, f32 _rad)
{
	SetAsSphere(_cnt.GetXYZ(), _rad);
}

void	CRenderPrimElm::SetAsSphere(const CMtx34& _pst)
{
	CSphere* tmp = (CSphere*)m_data;
	tmp->pst.SetMtx34(_pst);
	m_type = PRIM_ELM_SPHERE;
}

void	CRenderPrimElm::SetAsSphere(const CMtx44& _pst)
{
	CSphere* tmp = (CSphere*)m_data;
	tmp->pst = _pst;
	m_type = PRIM_ELM_SPHERE;
}


//--------------------------------------------------------------------------------------------------

//	楕円
void	CRenderPrimElm::SetAsEllipse(const CQuat& _qat, const CVec3& _cnt, f32 _sx, f32 _sy, f32 _sz)
{
	CMtx44 mtx = CMtx44::Identity;
	Mtx::Rotate(mtx, _qat);
	mtx.SetTrans(_cnt);
	SetAsEllipse(Mtx::Scale(mtx, CVec3(_sx, _sy, _sz)));
}

void	CRenderPrimElm::SetAsEllipse(const CQuat& _qat, const CVec3& _cnt, CVec3& _scl)
{
	CMtx44 mtx = CMtx44::Identity;
	Mtx::Rotate(mtx, _qat);
	mtx.SetTrans(_cnt);
	SetAsEllipse(Mtx::Scale(mtx, _scl));
}

void	CRenderPrimElm::SetAsEllipse(const CMtx34& _pst)
{
	CBox* tmp = (CBox*)m_data;
	tmp->pst.SetMtx34(_pst);
	m_type = PRIM_ELM_ELLIPSE;
}

void	CRenderPrimElm::SetAsEllipse(const CMtx44& _pst)
{
	CBox* tmp = (CBox*)m_data;
	tmp->pst = _pst;
	m_type = PRIM_ELM_ELLIPSE;
}


//--------------------------------------------------------------------------------------------------

//	カプセル
void	CRenderPrimElm::SetAsCapsule(const CVec3& _p0, const CVec3& _p1, f32 _rad)
{
	CCapsule* tmp = (CCapsule*)m_data;
	tmp->pos[0] = CVec4(_p0, 1.f);
	tmp->pos[1] = CVec4(_p1, 1.f);
	tmp->rad = _rad;
	m_type = PRIM_ELM_CAPSULE;
}

void	CRenderPrimElm::SetAsCapsule(const CVec4& _p0, const CVec4& _p1, f32 _rad)
{
	SetAsCapsule(_p0.GetXYZ(), _p1.GetXYZ(), _rad);
}


//--------------------------------------------------------------------------------------------------

//	扇

void	CRenderPrimElm::SetAsFan(const CVec3& _btm, f32 _yRot, f32 _rad, f32 _hgt, f32 _rot)
{
	CMtx44	mtx = CMtx44::Identity;
	CMtx44	work = CMtx44::Identity;
	Mtx::Scale(work, CVec3(_rad, 0.5f * _hgt, _rad));
	mtx = work * mtx;

	work = CMtx44::Identity;
	Mtx::RotateY(work, _yRot);
	mtx = work * mtx;

	CVec3 pos = _btm;
	pos[1] += 0.5f * _hgt;
	mtx.SetTrans(pos);

	SetAsFan(mtx, _rot);
}

void	CRenderPrimElm::SetAsFan(const CVec4& _btm, f32 _yRot, f32 _rad, f32 _hgt, f32 _rot)
{
	SetAsFan(_btm.GetXYZ(), _yRot, _rad, _hgt, _rot);
}

void	CRenderPrimElm::SetAsFan(const CVec3& _btm, const CQuat& _qat, f32 _rad, f32 _hgt, f32 _rot)
{
	CMtx44	mtx = CMtx44::Identity;
	CMtx44	work = CMtx44::Identity;
	Mtx::Scale(work, CVec3(_rad, 0.5f * _hgt, _rad));
	mtx = work * mtx;

	work = CMtx44::Identity;
	Mtx::Rotate(work, _qat);
	mtx = work * mtx;

	CVec3 pos = _btm;
	pos[1] += 0.5f * _hgt;
	mtx.SetTrans(pos);

	SetAsFan(mtx, _rot);
}

void	CRenderPrimElm::SetAsFan(const CVec4& _btm, const CQuat& _qat, f32 _rad, f32 _hgt, f32 _rot)
{
	SetAsFan(_btm.GetXYZ(), _qat, _rad, _hgt, _rot);
}

void	CRenderPrimElm::SetAsFan(const CVec3& _btm, const CVec3& _vec, f32 _rad, f32 _hgt, f32 _rot)
{
	CMtx44	mtx = CMtx44::Identity;
	CMtx44	work = CMtx44::Identity;
	Mtx::Scale(work, CVec3(_rad, 0.5f * _hgt, _rad));
	mtx = work * mtx;

	work = CMtx44::Identity;
	CQuat	quat = CQuat::Identity;
	if (_vec == CVec3::Back)
	{
		Rot::RotateY(quat, PI);
	}
	else if (Vec::Length2(_vec) > 0)
	{
		Rot::Rotate(quat, Vec::Normalize(Vec::Cross(CVec3::Front, _vec)), acosf(Vec::Dot(CVec3::Front, Vec::Normalize(_vec))));
	}
	Mtx::Rotate(work, quat);
	mtx = work * mtx;

	CVec3 pos = _btm;
	pos[1] += 0.5f * _hgt;
	mtx.SetTrans(pos);

	SetAsFan(mtx, _rot);
}

void	CRenderPrimElm::SetAsFan(const CVec4& _btm, const CVec4& _vec, f32 _rad, f32 _hgt, f32 _rot)
{
	SetAsFan(_btm.GetXYZ(), _vec.GetXYZ(), _rad, _hgt, _rot);
}

void	CRenderPrimElm::SetAsFan(const CMtx34& _pst, f32 _rot)
{
	CPipe* tmp = (CPipe*)m_data;
	tmp->pst.SetMtx34(_pst);
	tmp->pst[3].SetX(_rot);		// 行列の４行目、１列目に角度設定
	m_type = PRIM_ELM_FAN;
}

void	CRenderPrimElm::SetAsFan(const CMtx44& _pst, f32 _rot)
{
	CPipe* tmp = (CPipe*)m_data;
	tmp->pst = _pst;
	tmp->pst[3].SetX(_rot);		// 行列の４行目、１列目に角度設定
	m_type = PRIM_ELM_FAN;
}


//--------------------------------------------------------------------------------------------------

//	空間軸
void	CRenderPrimElm::SetAsAxis(const CMtx34& _pst, f32 _scale)
{
	CAxis* tmp = (CAxis*)m_data;
	tmp->pst.Copy(_pst);
	if (_scale != 1.f)
	{
		Mtx::Scale(tmp->pst, CVec3(_scale));
	}
	m_type = PRIM_ELM_AXIS;
}

//	空間軸
void	CRenderPrimElm::SetAsAxis(const CMtx44& _pst, f32 _scale)
{
	CAxis* tmp = (CAxis*)m_data;
	tmp->pst = _pst;
	if (_scale != 1.f)
	{
		Mtx::Scale(tmp->pst, CVec3(_scale));
	}
	m_type = PRIM_ELM_AXIS;
}

//--------------------------------------------------------------------------------------------------

//	三角形（塗りつぶし有り）
void	CRenderPrimElm::SetAsTriangleFill(f32 _x0, f32 _y0, f32 _z0, f32 _x1, f32 _y1, f32 _z1, f32 _x2, f32 _y2, f32 _z2, const CVec4& edge_color, b8 _onlyFront)
{
	SetAsTriangleFill(CVec3(_x0, _y0, _z0), CVec3(_x1, _y1, _z1), CVec3(_x2, _y2, _z2), edge_color, _onlyFront);
}

void	CRenderPrimElm::SetAsTriangleFill(const CVec3& _p0, const CVec3& _p1, const CVec3& _p2, const CVec4& edge_color, b8 _onlyFront)
{
	CTriangleFill* tmp = (CTriangleFill*)m_data;
	tmp->pos[0] = CVec4(_p0, 1.f);
	tmp->pos[1] = CVec4(_p1, 1.f);
	tmp->pos[2] = CVec4(_p2, 1.f);
	tmp->m_color_edge = edge_color;
	m_type = _onlyFront ? PRIM_ELM_TRIANGLE_FILL_FRONT : PRIM_ELM_TRIANGLE_FILL;
}

void	CRenderPrimElm::SetAsTriangleFill(const CVec4& _p0, const CVec4& _p1, const CVec4& _p2, const CVec4& edge_color, b8 _onlyFront)
{
	SetAsTriangleFill(_p0.GetXYZ(), _p1.GetXYZ(), _p2.GetXYZ(), edge_color, _onlyFront);
}

/// SetAsTriangleFill
void	CRenderPrimElm::SetAsTriangleFill(const CVec3& _p0, const CVec3& _p1, const CVec3& _p2, b8 _onlyFront /*= false*/)
{
	CTriangleFill* tmp = (CTriangleFill*)m_data;
	tmp->pos[0] = CVec4(_p0, 1.f);
	tmp->pos[1] = CVec4(_p1, 1.f);
	tmp->pos[2] = CVec4(_p2, 1.f);
	m_type = _onlyFront ? PRIM_ELM_TRIANGLE_FILL_FRONT : PRIM_ELM_TRIANGLE_FILL;
}

void	CRenderPrimElm::SetAsTriangleFill(const CVec4& _p0, const CVec4& _p1, const CVec4& _p2, b8 _onlyFront)
{
	SetAsTriangleFill(_p0.GetXYZ(), _p1.GetXYZ(), _p2.GetXYZ(), _onlyFront);
}

//--------------------------------------------------------------------------------------------------
//	箱（塗りつぶし有り）
void	CRenderPrimElm::SetAsBoxFill(const CQuat& _qat, const CVec3& _cnt, CVec3& _scl)
{
	CMtx44 mtx = CMtx44::Identity;
	Mtx::Rotate(mtx, _qat);
	mtx.SetTrans(_cnt);
	SetAsBoxFill(Mtx::Scale(mtx, _scl));
}
void	CRenderPrimElm::SetAsBoxFill(const CQuat& _qat, const CVec3& _cnt, f32 _sx, f32 _sy, f32 _sz)
{
	CMtx44 mtx = CMtx44::Identity;
	Mtx::Rotate(mtx, _qat);
	mtx.SetTrans(_cnt);
	SetAsBoxFill(Mtx::Scale(mtx, CVec3(_sx, _sy, _sz)));
}

void	CRenderPrimElm::SetAsBoxFill(const CQuat& _qat, const CVec3& _cnt, f32 _scl)
{
	SetAsBoxFill(_qat, _cnt, _scl, _scl, _scl);
}

void	CRenderPrimElm::SetAsBoxFill(const CVec3& _cnt, f32 _sx, f32 _sy, f32 _sz)
{
	CMtx44 mtx = CMtx44::Identity;
	mtx.SetTrans(_cnt);
	SetAsBoxFill(Mtx::Scale(mtx, CVec3(_sx, _sy, _sz)));
}

void	CRenderPrimElm::SetAsBoxFill(const CVec3& _cnt, f32 _scl)
{
	SetAsBoxFill(_cnt, _scl, _scl, _scl);
}

void	CRenderPrimElm::SetAsBoxFill(const CVec3& _max, const CVec3& _min)
{
	CMtx44	mtx = CMtx44::Identity;
	mtx.SetTrans((_max + _min) * 0.5f);
	SetAsBoxFill(Mtx::Scale(mtx, (_max - _min) * 0.5f));
}

void	CRenderPrimElm::SetAsBoxFill(const CVec4& _max, const CVec4& _min)
{
	SetAsBoxFill(_max.GetXYZ(), _min.GetXYZ());
}

void	CRenderPrimElm::SetAsBoxFill(const CMtx34& _pst)
{
	CBoxFill* tmp = (CBoxFill*)m_data;
	tmp->pst.SetMtx34(_pst);
	m_type = PRIM_ELM_BOX_FILL;
}

void	CRenderPrimElm::SetAsBoxFill(const CMtx44& _pst)
{
	CBoxFill* tmp = (CBoxFill*)m_data;
	tmp->pst = _pst;
	m_type = PRIM_ELM_BOX_FILL;
}

//--------------------------------------------------------------------------------------------------

//	箱 (領域内に入ったポリゴンに加算塗りつぶし)
void	CRenderPrimElm::SetAsBoxAreaFill(const CQuat& _qat, const CVec3& _cnt, CVec3& _scl)
{
	CMtx44 mtx = CMtx44::Identity;
	Mtx::Rotate(mtx, _qat);
	mtx.SetTrans(_cnt);
	SetAsBoxAreaFill(Mtx::Scale(mtx, _scl));
}
void	CRenderPrimElm::SetAsBoxAreaFill(const CQuat& _qat, const CVec3& _cnt, f32 _sx, f32 _sy, f32 _sz)
{
	CMtx44 mtx = CMtx44::Identity;
	Mtx::Rotate(mtx, _qat);
	mtx.SetTrans(_cnt);
	SetAsBoxAreaFill(Mtx::Scale(mtx, CVec3(_sx, _sy, _sz)));
}

void	CRenderPrimElm::SetAsBoxAreaFill(const CQuat& _qat, const CVec3& _cnt, f32 _scl)
{
	SetAsBoxAreaFill(_qat, _cnt, _scl, _scl, _scl);
}

void	CRenderPrimElm::SetAsBoxAreaFill(const CVec3& _cnt, f32 _sx, f32 _sy, f32 _sz)
{
	CMtx44	mtx = CMtx44::Identity;
	mtx.SetTrans(_cnt);
	SetAsBoxAreaFill(Mtx::Scale(mtx, CVec3(_sx, _sy, _sz)));
}

void	CRenderPrimElm::SetAsBoxAreaFill(const CVec3& _cnt, f32 _scl)
{
	SetAsBoxAreaFill(_cnt, _scl, _scl, _scl);
}

void	CRenderPrimElm::SetAsBoxAreaFill(const CVec3& _max, const CVec3& _min)
{
	CMtx44	mtx = CMtx44::Identity;
	mtx.SetTrans((_max + _min) * 0.5f);
	SetAsBoxAreaFill(Mtx::Scale(mtx, (_max - _min) * 0.5f));
}

void	CRenderPrimElm::SetAsBoxAreaFill(const CVec4& _max, const CVec4& _min)
{
	SetAsBoxAreaFill(_max.GetXYZ(), _min.GetXYZ());
}

void	CRenderPrimElm::SetAsBoxAreaFill(const CMtx34& _pst)
{
	CBoxAreaFill* tmp = (CBoxAreaFill*)m_data;
	tmp->pst.SetMtx34(_pst);
	m_type = PRIM_ELM_BOX_AREA_FILL;
}

void	CRenderPrimElm::SetAsBoxAreaFill(const CMtx44& _pst)
{
	CBoxAreaFill* tmp = (CBoxAreaFill*)m_data;
	tmp->pst = _pst;
	m_type = PRIM_ELM_BOX_AREA_FILL;
}

//--------------------------------------------------------------------------------------------------

//	球 (領域内に入ったポリゴンに加算塗りつぶし)
void	CRenderPrimElm::SetAsSphereAreaFill(const CQuat& _qat, const CVec3& _cnt, f32 _sx, f32 _sy, f32 _sz)
{
	SetAsSphereAreaFill(_qat, _cnt, CVec3(_sx, _sy, _sz));
}

void	CRenderPrimElm::SetAsSphereAreaFill(const CQuat& _qat, const CVec3& _cnt, const CVec3& _scl)
{
	CMtx44	mtx = CMtx44::Identity;
	mtx.SetTrans(_cnt);
	SetAsSphereAreaFill(Mtx::Scale(mtx, _scl));
}

void	CRenderPrimElm::SetAsSphereAreaFill(const CVec3& _cnt, f32 _rad)
{
	CMtx44	mtx = CMtx44::Identity;
	mtx.SetTrans(_cnt);
	SetAsSphereAreaFill(Mtx::Scale(mtx, CVec3(_rad, _rad, _rad)));
}

void	CRenderPrimElm::SetAsSphereAreaFill(const CVec4& _cnt, f32 _rad)
{
	SetAsSphereAreaFill(_cnt.GetXYZ(), _rad);
}

void	CRenderPrimElm::SetAsSphereAreaFill(const CMtx34& _pst)
{
	CSphereAreaFill* tmp = (CSphereAreaFill*)m_data;
	tmp->pst.SetMtx34(_pst);
	m_type = PRIM_ELM_SPHERE_AREA_FILL;
}

void	CRenderPrimElm::SetAsSphereAreaFill(const CMtx44& _pst)
{
	CSphereAreaFill* tmp = (CSphereAreaFill*)m_data;
	tmp->pst = _pst;
	m_type = PRIM_ELM_SPHERE_AREA_FILL;
}

//--------------------------------------------------------------------------------------------------

//	カプセル (領域内に入ったポリゴンに加算塗りつぶし)
void	CRenderPrimElm::SetAsCapsuleAreaFill(const CVec3& _p0, const CVec3& _p1, f32 _rad)
{
	CCapsuleAreaFill* tmp = (CCapsuleAreaFill*)m_data;
	tmp->pos[0] = CVec4(_p0, 1.f);
	tmp->pos[1] = CVec4(_p1, 1.f);
	tmp->rad = _rad;
	m_type = PRIM_ELM_CAPSULE_AREA_FILL;
}

void	CRenderPrimElm::SetAsCapsuleAreaFill(const CVec4& _p0, const CVec4& _p1, f32 _rad)
{
	SetAsCapsule(_p0.GetXYZ(), _p1.GetXYZ(), _rad);
}

//--------------------------------------------------------------------------------------------------

//	円柱 (領域内に入ったポリゴンに加算塗りつぶし)
void	CRenderPrimElm::SetAsPipeAreaFill(const CVec3& _btm, f32 _rad, f32 _hgt)
{
	CMtx44	mtx = CMtx44::Identity;
	CVec3 pos = _btm;
	pos[1] += 0.5f * _hgt;
	mtx.SetTrans(pos);
	SetAsPipeAreaFill(Mtx::Scale(mtx, CVec3(_rad, 0.5f * _hgt, _rad)));
}

void	CRenderPrimElm::SetAsPipeAreaFill(const CVec4& _btm, f32 _rad, f32 _hgt)
{
	SetAsPipeAreaFill(_btm.GetXYZ(), _rad, _hgt);
}

void	CRenderPrimElm::SetAsPipeAreaFill(const CVec3& _p0, const CVec3& _p1, f32 _rad)
{
	CVec3 pos = (_p0 + _p1) * 0.5f;
	CVec3 vY = _p1 - pos;
	CVec3 vX, vZ;
	if ((vY[0] == 0.f) && (vY[2] == 0.f))
	{
		vX = CVec3(_rad, 0.f, 0.f);
		vZ = CVec3(0.f, 0.f, _rad);
	}
	else {
		CVec3 tmp;
		Vec::Normalize(vZ, Vec::Cross(tmp, CVec3::Up, vY));
		Vec::Normalize(vX, Vec::Cross(tmp, vY, vZ));
		vZ *= _rad;
		vX *= _rad;
	}
	CMtx44 mtx;
	mtx.GetMtx34().SetAxis(vX, vY, vZ, pos);
	SetAsPipeAreaFill(mtx);
}

void	CRenderPrimElm::SetAsPipeAreaFill(const CVec4& _p0, const CVec4& _p1, f32 _rad)
{
	SetAsPipeAreaFill(_p0.GetXYZ(), _p1.GetXYZ(), _rad);
}

void	CRenderPrimElm::SetAsPipeAreaFill(const CMtx34& _pst)
{
	CPipe* tmp = (CPipe*)m_data;
	tmp->pst.SetMtx34(_pst);
	m_type = PRIM_ELM_PIPE_AREA_FILL;
}

void	CRenderPrimElm::SetAsPipeAreaFill(const CMtx44& _pst)
{
	CPipe* tmp = (CPipe*)m_data;
	tmp->pst = _pst;
	m_type = PRIM_ELM_PIPE_AREA_FILL;
}

//--------------------------------------------------------------------------------------------------

//	扇 (領域内に入ったポリゴンに加算塗りつぶし)

void	CRenderPrimElm::SetAsFanAreaFill(const CVec3& _btm, f32 _yRot, f32 _rad, f32 _hgt, f32 _rot)
{
	CMtx44	mtx = CMtx44::Identity;
	CMtx44	work = CMtx44::Identity;
	Mtx::Scale(work, CVec3(_rad, 0.5f * _hgt, _rad));
	mtx = work * mtx;

	work = CMtx44::Identity;
	Mtx::RotateY(work, _yRot);
	mtx = work * mtx;

	CVec3 pos = _btm;
	pos[1] += 0.5f * _hgt;
	mtx.SetTrans(pos);

	SetAsFanAreaFill(mtx, _rot);
}

void	CRenderPrimElm::SetAsFanAreaFill(const CVec4& _btm, f32 _yRot, f32 _rad, f32 _hgt, f32 _rot)
{
	SetAsFanAreaFill(_btm.GetXYZ(), _yRot, _rad, _hgt, _rot);
}

void	CRenderPrimElm::SetAsFanAreaFill(const CVec3& _btm, const CQuat& _qat, f32 _rad, f32 _hgt, f32 _rot)
{
	CMtx44	mtx = CMtx44::Identity;
	CMtx44	work = CMtx44::Identity;
	Mtx::Scale(work, CVec3(_rad, 0.5f * _hgt, _rad));
	mtx = work * mtx;

	work = CMtx44::Identity;
	Mtx::Rotate(work, _qat);
	mtx = work * mtx;

	CVec3 pos = _btm;
	pos[1] += 0.5f * _hgt;
	mtx.SetTrans(pos);

	SetAsFanAreaFill(mtx, _rot);
}

void	CRenderPrimElm::SetAsFanAreaFill(const CVec4& _btm, const CQuat& _qat, f32 _rad, f32 _hgt, f32 _rot)
{
	SetAsFanAreaFill(_btm.GetXYZ(), _qat, _rad, _hgt, _rot);
}

void	CRenderPrimElm::SetAsFanAreaFill(const CVec3& _btm, const CVec3& _vec, f32 _rad, f32 _hgt, f32 _rot)
{
	CMtx44	mtx = CMtx44::Identity;
	CMtx44	work = CMtx44::Identity;
	Mtx::Scale(work, CVec3(_rad, 0.5f * _hgt, _rad));
	mtx = work * mtx;

	work = CMtx44::Identity;
	CQuat	quat = CQuat::Identity;
	if (_vec == CVec3::Back)
	{
		Rot::RotateY(quat, PI);
	}
	else if (Vec::Length2(_vec) > 0)
	{
		Rot::Rotate(quat, Vec::Normalize(Vec::Cross(CVec3::Front, _vec)), acosf(Vec::Dot(CVec3::Front, Vec::Normalize(_vec))));
	}
	Mtx::Rotate(work, quat);
	mtx = work * mtx;

	CVec3 pos = _btm;
	pos[1] += 0.5f * _hgt;
	mtx.SetTrans(pos);

	SetAsFanAreaFill(mtx, _rot);
}

void	CRenderPrimElm::SetAsFanAreaFill(const CVec4& _btm, const CVec4& _vec, f32 _rad, f32 _hgt, f32 _rot)
{
	SetAsFanAreaFill(_btm.GetXYZ(), _vec.GetXYZ(), _rad, _hgt, _rot);
}

void	CRenderPrimElm::SetAsFanAreaFill(const CMtx34& _pst, f32 _rot)
{
	CPipe* tmp = (CPipe*)m_data;
	tmp->pst.SetMtx34(_pst);
	tmp->pst[3].SetX(_rot);		// 行列の４行目、１列目に角度設定
	m_type = PRIM_ELM_FAN_AREA_FILL;
}

void	CRenderPrimElm::SetAsFanAreaFill(const CMtx44& _pst, f32 _rot)
{
	CPipe* tmp = (CPipe*)m_data;
	tmp->pst = _pst;
	tmp->pst[3].SetX(_rot);		// 行列の４行目、１列目に角度設定
	m_type = PRIM_ELM_FAN_AREA_FILL;
}

//--------------------------------------------------------------------------------------------------
//	スプライト
void	CRenderPrimElm::SetAsSprite(const CVec3& pos, u16 widthPixel, u16 heightPixel, f32 zOffs, f32 rot, int xPosFlag, int yPosFlag, f32 xOffs, f32 yOffs)
{
#if 0
	CSprite* tmp = (CSprite*)m_data;
	tmp->pos = pos;
	const SYSTEM_INFO& info = CSystemUtility::GetSystemInfo();
	tmp->scale = CVec3((f32)widthPixel / info.fScreenWidth, (f32)heightPixel / info.fScreenHeight, 1.f);
	tmp->rot = rot;
	tmp->xOffs = xOffs * 2.f / info.fScreenWidth;
	tmp->yOffs = yOffs * 2.f / info.fScreenHeight;
	tmp->zOffs = zOffs;
	tmp->posFlag = CSprite::POS_NONE;

	if (xPosFlag < 0) { tmp->posFlag |= CSprite::POS_LEFT; }
	else if (xPosFlag > 0) { tmp->posFlag |= CSprite::POS_RIGHT; }

	if (yPosFlag < 0) { tmp->posFlag |= CSprite::POS_TOP; }
	else if (yPosFlag > 0) { tmp->posFlag |= CSprite::POS_BOTTOM; }

	m_type = PRIM_ELM_SPRITE;
#endif
}


//--------------------------------------------------------------------------------------------------
//	フォント
void	CRenderPrimElm::SetAsFont(const CVec3& pos, const char* pString, f32 width, f32 height, f32 zOffs, b8 bDrawEdge, const CVec4& vEdgeColor, f32 xOffs, f32 yOffs)
{
#if 0
	CFont* tmp = (CFont*)m_data;
	tmp->pos = pos;
	tmp->pString = pString;
	tmp->width = width;
	tmp->height = height;
	tmp->bDrawEdge = bDrawEdge;
	tmp->edgeClr = vEdgeColor;
	tmp->xOffs = xOffs;
	tmp->yOffs = yOffs;
	tmp->zOffs = zOffs;
	tmp->bUTF8 = false;

	m_type = PRIM_ELM_FONT;
#endif
}


//--------------------------------------------------------------------------------------------------
//	フォント
void	CRenderPrimElm::SetAsFontUTF8(const CVec3& pos, const char* pString, f32 width, f32 height, f32 zOffs, b8 bDrawEdge, const CVec4& vEdgeColor, f32 xOffs, f32 yOffs)
{
	CRenderPrimElm::SetAsFont(pos, pString, width, height, zOffs, bDrawEdge, vEdgeColor, xOffs, yOffs);

	CFont* tmp = (CFont*)m_data;
	tmp->bUTF8 = true;
}

//-------------------------------------------------------------------------------------------------
/// SetAsDir
void	CRenderPrimElm::SetAsDir(const CVec3& _pos, const CVec3& _dir)
{
	CDir* pDir = CCast<CDir*>(GetAsDir());
	pDir->pos = _pos;
	pDir->dir = _dir;
	m_type = PRIM_ELM_DIR;
}

//--------------------------------------------------------------------------------------------------
//	描画
void	CRenderPrimElm::RenderFunc(CRenderer* _pRenderer, const f32 fColor, const f32 fAlpha, const s32 progress) const
{
#if 0
	if ((progress == 0) && (m_ztest != ZTEST_DEFAULT))
	{
		return;
	}
	if ((progress == 1))
	{
		if (m_ztest == ZTEST_DEFAULT)
		{
			_pRenderer->SetRenderState(ME_RS_ZTEST_ENABLE, ME_TRUE);
			_pRenderer->SetRenderState(ME_RS_ZMASK_ENABLE, ME_FALSE);
			_pRenderer->SetRenderState(ME_RS_COLOR_MASK, ME_COLOR_MASK_RGB);
		}
		else if (m_ztest == ZTEST_NONE)
		{
			_pRenderer->SetRenderState(ME_RS_ZTEST_ENABLE, ME_FALSE);
			_pRenderer->SetRenderState(ME_RS_ZMASK_ENABLE, ME_TRUE);
			//_pRenderer->SetRenderState(ME_RS_COLOR_MASK, ME_COLOR_MASK_RGBA);
			_pRenderer->SetRenderState(ME_RS_COLOR_MASK, ME_COLOR_MASK_RGB);
		}
		else if (m_ztest == ZTEST_ENABLE)
		{
			_pRenderer->SetRenderState(ME_RS_ZTEST_FUNC, ME_LEQUAL);
			_pRenderer->SetRenderState(ME_RS_ZTEST_ENABLE, ME_TRUE);
			_pRenderer->SetRenderState(ME_RS_ZMASK_ENABLE, ME_TRUE);
			//_pRenderer->SetRenderState(ME_RS_COLOR_MASK, ME_COLOR_MASK_RGBA);
			_pRenderer->SetRenderState(ME_RS_COLOR_MASK, ME_COLOR_MASK_RGB);
		}
	}
#endif

	CVec4 vClr = m_edgeColor;
	vClr[0] *= fColor;
	vClr[1] *= fColor;
	vClr[2] *= fColor;
	vClr[3] *= fAlpha;

	switch (GetType())
	{
	case PRIM_ELM_LINE:					GetAsLine()->RenderFunc(_pRenderer, vClr);			break;
	case PRIM_ELM_TRIANGLE:				GetAsTriangle()->RenderFunc(_pRenderer, vClr);		break;
	case PRIM_ELM_BOX:					GetAsBox()->RenderFunc(_pRenderer, vClr);			break;
	case PRIM_ELM_PIPE:					GetAsPipe()->RenderFunc(_pRenderer, vClr);			break;
	case PRIM_ELM_SPHERE:				GetAsSphere()->RenderFunc(_pRenderer, vClr);		break;
	case PRIM_ELM_ELLIPSE:				GetAsEllipse()->RenderFunc(_pRenderer, vClr);		break;
	case PRIM_ELM_CAPSULE:				GetAsCapsule()->RenderFunc(_pRenderer, vClr);		break;
	case PRIM_ELM_FAN:					GetAsFan()->RenderFunc(_pRenderer, vClr);			break;
	case PRIM_ELM_AXIS:					GetAsAxis()->RenderFunc(_pRenderer, vClr);			break;
	case PRIM_ELM_TRIANGLE_FILL:		GetAsTriangleFill()->RenderFunc(_pRenderer, vClr);	break;
	case PRIM_ELM_TRIANGLE_FILL_FRONT:	GetAsTriangleFill()->RenderFunc(_pRenderer, vClr);	break;
	case PRIM_ELM_BOX_FILL:				GetAsBoxFill()->RenderFunc(_pRenderer, vClr);		break;
	case PRIM_ELM_SPRITE:				GetAsSprite()->RenderFunc(_pRenderer, vClr);		break;
	case PRIM_ELM_DIR:					GetAsDir()->RenderFunc(_pRenderer, vClr);			break;
	default:	break;
	}
}

// 領域塗りつぶし用描画関数
void	CRenderPrimElm::RenderFuncAreaFill(CRenderer* _pRenderer, const CVec4& _color, const CTextureBuffer* _pDepthTex) const
{
	switch (GetType())
	{
	case PRIM_ELM_BOX_AREA_FILL:	GetAsBoxAreaFill()->RenderFunc(_pRenderer, _color, _pDepthTex);		break;
	case PRIM_ELM_SPHERE_AREA_FILL:	GetAsSphereAreaFill()->RenderFunc(_pRenderer, _color, _pDepthTex);	break;
	case PRIM_ELM_CAPSULE_AREA_FILL:GetAsCapsuleAreaFill()->RenderFunc(_pRenderer, _color, _pDepthTex);	break;
	case PRIM_ELM_PIPE_AREA_FILL:	GetAsPipeAreaFill()->RenderFunc(_pRenderer, _color, _pDepthTex);	break;
	case PRIM_ELM_FAN_AREA_FILL:	GetAsFanAreaFill()->RenderFunc(_pRenderer, _color, _pDepthTex);		break;
	default:	break;
	}
}


// メッシュを設定する処理
void	CRenderPrimElm::LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const f32 fColor, const f32 fAlpha) const
{
	CVec4 vClr = m_edgeColor;
	vClr[0] *= fColor;
	vClr[1] *= fColor;
	vClr[2] *= fColor;
	vClr[3] *= fAlpha;

	switch (GetType())
	{
	case PRIM_ELM_LINE:					GetAsLine()->LineMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);			break;
	case PRIM_ELM_TRIANGLE:				GetAsTriangle()->LineMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);		break;
	case PRIM_ELM_BOX:					GetAsBox()->LineMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);				break;
	case PRIM_ELM_PIPE:					GetAsPipe()->LineMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);			break;
	case PRIM_ELM_SPHERE:				GetAsSphere()->LineMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);			break;
	case PRIM_ELM_ELLIPSE:				GetAsEllipse()->LineMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);			break;
	case PRIM_ELM_CAPSULE:				GetAsCapsule()->LineMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);			break;
	case PRIM_ELM_FAN:					GetAsFan()->LineMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);				break;
	case PRIM_ELM_AXIS:					GetAsAxis()->LineMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);			break;
	case PRIM_ELM_TRIANGLE_FILL:		GetAsTriangleFill()->LineMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);	break;
	case PRIM_ELM_TRIANGLE_FILL_FRONT:	GetAsTriangleFill()->LineMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);	break;
	case PRIM_ELM_BOX_FILL:				GetAsBoxFill()->LineMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);			break;
	case PRIM_ELM_BOX_AREA_FILL:		GetAsBoxAreaFill()->LineMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);		break;
	case PRIM_ELM_SPHERE_AREA_FILL:		GetAsSphereAreaFill()->LineMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);	break;
	case PRIM_ELM_CAPSULE_AREA_FILL:	GetAsCapsuleAreaFill()->LineMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);	break;
	case PRIM_ELM_PIPE_AREA_FILL:		GetAsPipeAreaFill()->LineMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);	break;
	case PRIM_ELM_FAN_AREA_FILL:		GetAsFanAreaFill()->LineMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);		break;
		//	case PRIM_ELM_SPRITE:	GetAsSprite()->LineMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);			break;
	default:	break;
	}
}

// このメッシュに必要な頂点数取得
u32		CRenderPrimElm::GetLineVertexNum() const
{
	switch (GetType())
	{
	case PRIM_ELM_LINE:					return GetAsLine()->GetLineVertexNum();
	case PRIM_ELM_TRIANGLE:				return GetAsTriangle()->GetLineVertexNum();
	case PRIM_ELM_BOX:					return GetAsBox()->GetLineVertexNum();
	case PRIM_ELM_PIPE:					return GetAsPipe()->GetLineVertexNum();
	case PRIM_ELM_SPHERE:				return GetAsSphere()->GetLineVertexNum();
	case PRIM_ELM_ELLIPSE:				return GetAsEllipse()->GetLineVertexNum();
	case PRIM_ELM_CAPSULE:				return GetAsCapsule()->GetLineVertexNum();
	case PRIM_ELM_FAN:					return GetAsFan()->GetLineVertexNum();
	case PRIM_ELM_AXIS:					return GetAsAxis()->GetLineVertexNum();
	case PRIM_ELM_TRIANGLE_FILL:		return GetAsTriangleFill()->GetLineVertexNum();
	case PRIM_ELM_TRIANGLE_FILL_FRONT:	return GetAsTriangleFill()->GetLineVertexNum();
	case PRIM_ELM_BOX_FILL:				return GetAsBoxFill()->GetLineVertexNum();
	case PRIM_ELM_BOX_AREA_FILL:		return GetAsBoxAreaFill()->GetLineVertexNum();
	case PRIM_ELM_SPHERE_AREA_FILL:		return GetAsSphereAreaFill()->GetLineVertexNum();
	case PRIM_ELM_CAPSULE_AREA_FILL:	return GetAsCapsuleAreaFill()->GetLineVertexNum();
	case PRIM_ELM_PIPE_AREA_FILL:		return GetAsPipeAreaFill()->GetLineVertexNum();
	case PRIM_ELM_FAN_AREA_FILL:		return GetAsFanAreaFill()->GetLineVertexNum();
		//	case PRIM_ELM_SPRITE:	return GetAsSprite()->GetLineVertexNum();			
	default:	break;
	}

	return 0;
}

// このメッシュに必要なインデックス数取得
u32		CRenderPrimElm::GetLineIndexNum() const
{
	switch (GetType())
	{
	case PRIM_ELM_LINE:					return GetAsLine()->GetLineIndexNum();
	case PRIM_ELM_TRIANGLE:				return GetAsTriangle()->GetLineIndexNum();
	case PRIM_ELM_BOX:					return GetAsBox()->GetLineIndexNum();
	case PRIM_ELM_PIPE:					return GetAsPipe()->GetLineIndexNum();
	case PRIM_ELM_SPHERE:				return GetAsSphere()->GetLineIndexNum();
	case PRIM_ELM_ELLIPSE:				return GetAsEllipse()->GetLineIndexNum();
	case PRIM_ELM_CAPSULE:				return GetAsCapsule()->GetLineIndexNum();
	case PRIM_ELM_FAN:					return GetAsFan()->GetLineIndexNum();
	case PRIM_ELM_AXIS:					return GetAsAxis()->GetLineIndexNum();
	case PRIM_ELM_TRIANGLE_FILL:		return GetAsTriangleFill()->GetLineIndexNum();
	case PRIM_ELM_TRIANGLE_FILL_FRONT:	return GetAsTriangleFill()->GetLineIndexNum();
	case PRIM_ELM_BOX_FILL:				return GetAsBoxFill()->GetLineIndexNum();
	case PRIM_ELM_BOX_AREA_FILL:		return GetAsBoxAreaFill()->GetLineIndexNum();
	case PRIM_ELM_SPHERE_AREA_FILL:		return GetAsSphereAreaFill()->GetLineIndexNum();
	case PRIM_ELM_CAPSULE_AREA_FILL:	return GetAsCapsuleAreaFill()->GetLineIndexNum();
	case PRIM_ELM_PIPE_AREA_FILL:		return GetAsPipeAreaFill()->GetLineIndexNum();
	case PRIM_ELM_FAN_AREA_FILL:		return GetAsFanAreaFill()->GetLineIndexNum();
		//	case PRIM_ELM_SPRITE:	return GetAsSprite()->GetLineIndexNum();			
	default:	break;
	}

	return 0;
}

// 塗りつぶしメッシュを設定する処理
void	CRenderPrimElm::FillMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const f32 fColor, const f32 fAlpha) const
{
	CVec4 vClr = m_faceColor;
	vClr[0] *= fColor;
	vClr[1] *= fColor;
	vClr[2] *= fColor;
	vClr[3] *= fAlpha;

	switch (GetType())
	{
	case PRIM_ELM_TRIANGLE_FILL:		GetAsTriangleFill()->FillMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);	break;
	case PRIM_ELM_TRIANGLE_FILL_FRONT:	GetAsTriangleFill()->FillMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);	break;
	case PRIM_ELM_BOX_FILL:				GetAsBoxFill()->FillMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);			break;
		//	case PRIM_ELM_SPRITE:	GetAsSprite()->FillMeshSetFunc(_vertex_list, _idx_list, _vertex_start_idx, _idx_list_start_idx, vClr);			break;
	default:	break;
	}
}

// 塗りつぶしに必要な頂点数取得
u32		CRenderPrimElm::GetFillVertexNum() const
{
	switch (GetType())
	{
	case PRIM_ELM_TRIANGLE_FILL:		return GetAsTriangleFill()->GetFillVertexNum();
	case PRIM_ELM_TRIANGLE_FILL_FRONT:	return GetAsTriangleFill()->GetFillVertexNum();
	case PRIM_ELM_BOX_FILL:				return GetAsBoxFill()->GetFillVertexNum();
		//	case PRIM_ELM_SPRITE:	return GetAsSprite()->GetFillVertexNum();			
	default: break;
	}

	return 0;
}

// 塗りつぶしに必要なインデックス数取得
u32		CRenderPrimElm::GetFillIndexNum() const
{
	switch (GetType())
	{
	case PRIM_ELM_TRIANGLE_FILL:		return GetAsTriangleFill()->GetFillIndexNum();
	case PRIM_ELM_TRIANGLE_FILL_FRONT:	return GetAsTriangleFill()->GetFillIndexNum();
	case PRIM_ELM_BOX_FILL:				return GetAsBoxFill()->GetFillIndexNum();
		//	case PRIM_ELM_SPRITE:	return GetAsSprite()->GetFillIndexNum();			
	default: break;
	}

	return 0;
}

// 領域塗りつぶしに必要なユニフォームデータ取得
void	CRenderPrimElm::GetAreaFillParam(CMtx44& _dst) const
{
	switch (GetType())
	{
	case PRIM_ELM_BOX_AREA_FILL:	return GetAsBoxAreaFill()->GetAreaFillParam(_dst);
	case PRIM_ELM_SPHERE_AREA_FILL:	return GetAsSphereAreaFill()->GetAreaFillParam(_dst);
	case PRIM_ELM_CAPSULE_AREA_FILL:return GetAsCapsuleAreaFill()->GetAreaFillParam(_dst);
	case PRIM_ELM_PIPE_AREA_FILL:	return GetAsPipeAreaFill()->GetAreaFillParam(_dst);
	case PRIM_ELM_FAN_AREA_FILL:	return GetAsFanAreaFill()->GetAreaFillParam(_dst);
	default: break;
	}
}

//-------------------------------------------------------------------------------------------------
/// IsFillCullFace
b8	CRenderPrimElm::IsFillCullFace() const
{
	switch (GetType())
	{
	case PRIM_ELM_TRIANGLE_FILL_FRONT:	return true;
	case PRIM_ELM_BOX_AREA_FILL:		return true;
	case PRIM_ELM_SPHERE_AREA_FILL:		return true;
	case PRIM_ELM_CAPSULE_AREA_FILL:	return true;
	case PRIM_ELM_PIPE_AREA_FILL:		return true;
	case PRIM_ELM_FAN_AREA_FILL:		return true;
	default: break;
	}
	return false;
}


POISON_END

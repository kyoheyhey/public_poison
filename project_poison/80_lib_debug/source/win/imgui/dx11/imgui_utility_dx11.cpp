﻿
#ifdef LIB_GFX_DX11
//#ifdef LIB_GFX_OPENGL

///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "imgui/imgui_utility.h"

#include "imgui/imgui.h"
#include "imgui/imgui_impl_win32.h"
#include "imgui/dx11/imgui_impl_dx11.h"

#include "dx11/device/win/device_dx11.h"

POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///*************************************************************************************************
///	Imguiユーティリティ
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// 初期化
b8		CImguiUtility::Initialize()
{
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;	// Enable Keyboard Controls
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;	// Enable Gamepad Controls
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;		// Enable Docking
	io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;		// Enable Multi-Viewport / Platform Windows
	//io.ConfigViewportsNoAutoMerge = true;
	//io.ConfigViewportsNoTaskBarIcon = true;
	//io.ConfigViewportsNoDefaultParent = true;
	//io.ConfigDockingAlwaysTabBar = true;
	//io.ConfigDockingTransparentPayload = true;
#if 0
	io.ConfigFlags |= ImGuiConfigFlags_DpiEnableScaleFonts;		// FIXME-DPI: THIS CURRENTLY DOESN'T WORK AS EXPECTED. DON'T USE IN USER APP!
	io.ConfigFlags |= ImGuiConfigFlags_DpiEnableScaleViewports;	// FIXME-DPI
#endif

	// Setup Dear ImGui style
	//ImGui::StyleColorsDark();
	ImGui::StyleColorsClassic();

	// Setup Platform/Renderer bindings
	b8 bRet = true;
	bRet &= ImGui_ImplWin32_Init(pDevice->GetHWND(), NULL);
	bRet &= ImGui_ImplDX11_Init(pDevice->GetDevice(), pDevice->GetContext());
	return bRet;
}

///-------------------------------------------------------------------------------------------------
/// 終了
void	CImguiUtility::Finalize()
{
	ImGui_ImplDX11_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();
}

///-------------------------------------------------------------------------------------------------
/// 更新
void	CImguiUtility::Step()
{
	// Start the Dear ImGui frame
	ImGui_ImplDX11_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();

	ImGuiIO& io = ImGui::GetIO();
#if 0
	// マウス座標更新
	//const CDisplay* pDisplay = CDisplayUtility::GetDisplay();
	//Assert(pDisplay);
	CGfxDevice* pDevice = SDCast<CGfxDevice*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);

	CVec2 pos = pMouse->GetPointer().GetPos();
#if defined( OPENGL_VIEWPORT )
	pos.y = 1.0f - pos.y;
#elif defined( DIRECTX_VIEWPORT )
#endif
	io.DisplaySize.x = SCast<f32>(pDevice->GetWidth());
	io.DisplaySize.y = SCast<f32>(pDevice->GetHeight());
	io.MousePos = ImVec2(pos.x * io.DisplaySize.x, pos.y * io.DisplaySize.y);
#endif // 0

	const CMouse* pMouse = CInputUtility::GetMouse();
	libAssert(pMouse);
	// マウス入力更新
	u32 button = 0;
	memset(io.MouseDown, 0, sizeof(io.MouseDown));
	if (pMouse->GetButton(CMouse::eLeft).GetPush())
	{
		button = 1 << 0;
	}
	if (pMouse->GetButton(CMouse::eRight).GetPush())
	{
		button = 1 << 1;
	}
	if (pMouse->GetButton(CMouse::eCenter).GetPush())
	{
		button = 1 << 2;
	}
	if (button)
	{
		u32 buttonIdx = 0;
		while (button)
		{
			io.MouseDown[buttonIdx] = true;
			buttonIdx++;
			button >>= 1;
		}
	}
	f32 moveWheel = pMouse->GetWheel().GetMove();
	io.MouseWheel += moveWheel;
}

///-------------------------------------------------------------------------------------------------
/// 描画
void	CImguiUtility::Draw()
{
	// デバイス取得
	CDeviceDX11* pDevice = SDCast<CDeviceDX11*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);

	// キャンパスバインド
	pDevice->BindCanvas();

	ImGui::Render();
	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());

	ImGuiIO& io = ImGui::GetIO();
	// Update and Render additional Platform Windows
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
	{
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault();
	}
}

POISON_END

#endif // LIB_GFX_DX11
//#endif // LIB_GFX_OPENGL

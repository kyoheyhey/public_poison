﻿
#ifdef LIB_GFX_OPENGL

///-------------------------------------------------------------------------------------------------
/// define
#define GLSL_VERSION	"#version 130"

///-------------------------------------------------------------------------------------------------
/// include
#include "imgui/imgui_utility.h"

#include "imgui/imgui.h"
#include "imgui/imgui_impl_win32.h"
#include "imgui/gl/imgui_impl_opengl3.h"

#include "gl/device/win/device_gl.h"

POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant


///*************************************************************************************************
///	Imguiユーティリティ
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// 初期化
b8		CImguiUtility::Initialize()
{
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;		// Enable Keyboard Controls
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;		// Enable Gamepad Controls
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;			// Enable Docking
	io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;			// Enable Multi-Viewport / Platform Windows
	//io.ConfigViewportsNoAutoMerge = true;
	//io.ConfigViewportsNoTaskBarIcon = true;
	//io.ConfigViewportsNoDefaultParent = true;
	//io.ConfigDockingAlwaysTabBar = true;
	//io.ConfigDockingTransparentPayload = true;
#if 0
	io.ConfigFlags |= ImGuiConfigFlags_DpiEnableScaleFonts;		// FIXME-DPI: THIS CURRENTLY DOESN'T WORK AS EXPECTED. DON'T USE IN USER APP!
	io.ConfigFlags |= ImGuiConfigFlags_DpiEnableScaleViewports;	// FIXME-DPI
#endif

	// Setup Dear ImGui style
	//ImGui::StyleColorsDark();
	ImGui::StyleColorsClassic();

	// When viewports are enabled we tweak WindowRounding/WindowBg so platform windows can look identical to regular ones.
	//ImGuiStyle& style = ImGui::GetStyle();
	//if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
	//{
	//	style.WindowRounding = 0.0f;
	//	style.Colors[ImGuiCol_WindowBg].w = 1.0f;
	//}

	// Setup Platform/Renderer bindings
	b8 bRet = true;
	bRet &= ImGui_ImplWin32_Init(pDevice->GetHWND(), pDevice->GetHGLRC());
	bRet &= ImGui_ImplOpenGL3_Init(GLSL_VERSION);
	return bRet;
}

///-------------------------------------------------------------------------------------------------
/// 終了
void	CImguiUtility::Finalize()
{
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();
}

///-------------------------------------------------------------------------------------------------
/// 更新
void	CImguiUtility::Step()
{
	// Start the Dear ImGui frame
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();

	ImGuiIO& io = ImGui::GetIO();
	const CMouse* pMouse = CInputUtility::GetMouse();
#if 0
	// マウス座標更新
	CVec2 pos = pMouse->GetPointer().GetPos();
#if defined( OPENGL_VIEWPORT )
	pos.y = 1.0f - pos.y;
#elif defined( DIRECTX_VIEWPORT )
#endif
	CGfxDevice* pDevice = SDCast<CGfxDevice*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);
	io.DisplaySize.x = SCast<f32>(pDevice->GetWidth());
	io.DisplaySize.y = SCast<f32>(pDevice->GetHeight());
	io.MousePos = ImVec2(pos.x * io.DisplaySize.x, pos.y * io.DisplaySize.y);
#endif // 0

	// マウス入力更新
	u32 button = 0;
	memset(io.MouseDown, 0, sizeof(io.MouseDown));
	if (pMouse->GetButton(CMouse::eLeft).GetPush())
	{
		button = 1 << 0;
	}
	if (pMouse->GetButton(CMouse::eRight).GetPush())
	{
		button = 1 << 1;
	}
	if (pMouse->GetButton(CMouse::eCenter).GetPush())
	{
		button = 1 << 2;
	}
	if (button)
	{
		u32 buttonIdx = 0;
		while (button)
		{
			io.MouseDown[buttonIdx] = true;
			buttonIdx++;
			button >>= 1;
		}
	}
	f32 moveWheel = pMouse->GetWheel().GetMove();
	io.MouseWheel += moveWheel;
}

///-------------------------------------------------------------------------------------------------
/// 描画
void	CImguiUtility::Draw()
{
	// デバイス取得
	CDeviceGL* pDevice = SDCast<CDeviceGL*>(CDeviceUtility::GetDevice());
	libAssert(pDevice);

	// キャンパスバインド
	pDevice->BindCanvas();

	ImGui::Render();
	//ImGuiIO& io = ImGui::GetIO();
	//glViewport(0, 0, (int)io.DisplaySize.x, (int)io.DisplaySize.y);
	//glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
	//glClear(GL_COLOR_BUFFER_BIT);
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

	ImGuiIO& io = ImGui::GetIO();
	// Update and Render additional Platform Windows
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
	{
		pDevice->PushDevice();
		{
			ImGui::UpdatePlatformWindows();
			ImGui::RenderPlatformWindowsDefault();
		}
		pDevice->PopDevice();
	}
}

POISON_END

#endif // LIB_GFX_OPENGL

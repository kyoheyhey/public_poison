﻿//#pragma once
#ifndef __RENDER_PRIM_H__
#define __RENDER_PRIM_H__

//-------------------------------------------------------------------------------------------------
// include
#include "render_prim_elm.h"


POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype
class CCamera;
class CTextureBuffer;
class FxShader;
class CCamera;

	
//*************************************************************************************************
//@brief	レンダープリミティブ
//	
//	固定のプリミティブを描画します。主にデバッグ用です。
//	@test	レンダープリミティブによるプリミティブ登録方法
//	@code
//	{
//		CRenderPrimElm elm;	// ローカルに変数を作り、描画したいタイプとカラーを指定
//		elm.SetAsShpere( 5.f );
//		elm.SetColor( 1.f, 0.f, 0.f, 1.f );
//		
//		// エレメント追加
//		CRenderPrim::Add( elm );
//	}
//	@endcode
//*************************************************************************************************
class CRenderPrim
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コンストラクタ・デストラクタ
	CRenderPrim();
	~CRenderPrim();

public:

	//-------------------------------------------------------------------------------------------------
	//@brief	初期化、エレメント配列設定
	// @param[in] _elemAry
	// @param[in] _elemNum
	// @param[in] _temp_memHdl		自動破棄するテンポラリメモリハンドルを指定してください
	static b8		Initialize(CRenderPrimElm _elemAry[], const u32 _elemNum, const hash32 _shaderName);

	//-------------------------------------------------------------------------------------------------
	//@brief	後処理
	static void		Finalize();

	//-------------------------------------------------------------------------------------------------
	//@brief	領域内ポリゴン塗りつぶし用のデプステクスチャ設定
	//@param[in]	_depthTex	デプスレンダーターゲット
	//@note		設定しなければカレントのデプスレンダーターゲットで設定します。
	static void		SetDepthTex(const CTextureBuffer* _depthTex);

	//-------------------------------------------------------------------------------------------------
	//@brief	 エレメント描画
	//@param[in]	_flag	フラグで指定したビットのエレメントだけ描画します

	//@note	Addされたエレメントすべてを描画登録します。\n
	//		Addされたエレメントは描画を終えるとすべて消去されます。\n
	static void		Draw(const u32 _flag = ~0);

	//-------------------------------------------------------------------------------------------------
	///@brief	 描画終了、次の描画のための準備 
	static void		DrawEnd();

private:
	//-------------------------------------------------------------------------------------------------
	//@brief	描画コールバック
	static b8		RenderFunc(CRenderer* _pRenderer, const void* _p0, const void* _p1);

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	エレメント追加
	static b8		Add(CRenderPrimElm& _elm);

	//-------------------------------------------------------------------------------------------------
	//@brief	連続したライン追加
	//@return	成功か失敗か
	//@param[in]	_pPosAry	ラインでつなぐ座標の配列
	//@param[in]	_posNum		座標配列の個数
	//@param[in]	_clr		カラーRGBA(0～1.f)
	static b8		AddLines( const CVec3* _pPosAry, const u32 _posNum, const f32 _clr[4] );
	//-------------------------------------------------------------------------------------------------
	//@brief	点の複数追加
	//@return	成功か失敗か
	//@param[in]	_pPosAry	点の座標の配列
	//@param[in]	_posNum		座標配列の個数
	//@param[in]	_clr		カラーRGBA(0～1.f)
	//@param[in]	_size
	static b8		AddPoints( const CVec3* _pPosAry, const u32 _posNum, const f32 _clr[4], const f32 _size );

private:
	//-------------------------------------------------------------------------------------------------
	//@brief	次のインデックスを返す
	u16				GetNextIndex( const u16 _index ) const  { u16 ret = _index + 1; if( ret >= m_uMaxNum ){ ret = 0; } return ret; }

	//-------------------------------------------------------------------------------------------------
	//@brief	線のメッシュマージ描画
	static b8		IsMeshMergeElmType(CRenderPrimElm::PRIM _elmType);

private:
	
	CRenderPrimElm*			m_pElm = NULL;
	u16						m_uBgnIndex = 0;		///< 開始インデックス
	u16						m_uEndIndex = 0;		///< 終了インデックス
	u16						m_uNum = 0;				///< 登録個数
	u16						m_uMaxNum = 0;			///< 最大個数

	u16						m_uDrawBgnIndex = 0;	///< 描画の開始インデックス
	u16						m_uDrawEndIndex = 0;	///< 描画の終了インデックス
	
	const FxShader*			m_pShader = NULL;		///< シェーダーハンドル
	const CTextureBuffer*	m_pDepthTex = NULL;		///< 指定デプステクスチャ

};



//*************************************************************************************************
//@brief	レンダープリミティブのUtility
//*************************************************************************************************
class CRenderPrimUtility
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	線分を追加
	//@param[in]	_start	始点
	//@param[in]	_end	終点
	//@param[in]	_color	色
	static void		AddLine(const CVec3& _start, const CVec3& _end, const CVec3& _color = CVec3::One);

	//-------------------------------------------------------------------------------------------------
	//@brief	網の目状の地面をレンダープリムに追加
	//@param[in]	_size	幅
	//@param[in]	_split	分割数
	static void		AddSplitGround(f32 _size = 64.0f, u32 _split = 64);
};

POISON_END
	
#endif	// __RENDER_PRIM_H__


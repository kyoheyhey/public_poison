﻿//#pragma once
#ifndef __RENDER_PRIM_ELEMENT_H__
#define __RENDER_PRIM_ELEMENT_H__


//-------------------------------------------------------------------------------------------------
// include
#include "math/matrix.h"
#include "math/srt.h"

POISON_BGN
//-------------------------------------------------------------------------------------------------
// prototype
class CRenderer;
class CTextureBuffer;


//*************************************************************************************************
//@brief	レンダープリミティブエレメント
//*************************************************************************************************
class CRenderPrimElm
{
	friend class CRenderPrim;

protected:
	enum PRIM
	{
		PRIM_ELM_NONE = 0,				///< 無効

		PRIM_ELM_POINT,					///< 点
		PRIM_ELM_LINE,					///< 線分
		PRIM_ELM_TRIANGLE,				///< 三角形
		PRIM_ELM_BOX,					///< 箱
		PRIM_ELM_PIPE,					///< 円柱
		PRIM_ELM_SPHERE,				///< 球
		PRIM_ELM_ELLIPSE,				///< 楕円
		PRIM_ELM_CAPSULE,				///< カプセル
		PRIM_ELM_FAN,					///< 扇
		PRIM_ELM_AXIS,					///< 空間軸
		PRIM_ELM_TRIANGLE_FILL,			///< 三角形（塗りつぶし）
		PRIM_ELM_TRIANGLE_FILL_FRONT,	///< 三角形（表面のみ塗りつぶし）
		PRIM_ELM_SPRITE,				///< スプライト
		PRIM_ELM_FONT,					///< フォント
		PRIM_ELM_BOX_FILL,				///< 箱(塗りつぶし)
		PRIM_ELM_DIR,					///< 方向
		PRIM_ELM_BOX_AREA_FILL	,		///< 箱（領域に入ったポリゴンに加算塗りつぶし）
		PRIM_ELM_SPHERE_AREA_FILL,		///< 球（領域に入ったポリゴンに加算塗りつぶし）
		PRIM_ELM_CAPSULE_AREA_FILL,		///< カプセル（領域に入ったポリゴンに加算塗りつぶし）
		PRIM_ELM_PIPE_AREA_FILL,		///< 円柱（領域に入ったポリゴンに加算塗りつぶし）
		PRIM_ELM_FAN_AREA_FILL,			///< 扇（領域に入ったポリゴンに加算塗りつぶし）

		PRIM_ELM_NUM,
	};

public:
	//*************************************************************************************************
	//@brief	静的なデータ
	struct VERTEX
	{
		VERTEX()
			: vx(0.0f), vy(0.0f), vz(0.0f)
			, cr(0.0f), cg(0.0f), cb(0.0f), ca(0.0f)
		{}
		VERTEX(f32 _vx, f32 _vy, f32 _vz, f32 _cr, f32 _cg, f32 _cb, f32 _ca)
			: vx(_vx), vy(_vy), vz(_vz)
			, cr(_cr), cg(_cg), cb(_cb), ca(_ca)
		{}
		f32 vx, vy, vz;
		f32 cr, cg, cb, ca;
	};

protected:
	//*************************************************************************************************
	//@brief	線分
	class CLine
	{
	public:
		CVec4	pos[2];	///< 頂点

		//-------------------------------------------------------------------------------------------------
		//@brief	描画
		void	RenderFunc(CRenderer* _pRenderer, const CVec4& _color) const;

		//-------------------------------------------------------------------------------------------------
		//@brief	メッシュを設定する処理
		void	LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;

		//-------------------------------------------------------------------------------------------------
		//@brief	頂点数取得
		u32		GetLineVertexNum() const { return 2; }
		//-------------------------------------------------------------------------------------------------
		//@brief	インデックス数取得
		u32		GetLineIndexNum() const { return 2; }
	};

	//*************************************************************************************************
	//@brief	三角形
	class CTriangle
	{
	public:
		CVec4	pos[3];	///< 頂点

		//-------------------------------------------------------------------------------------------------
		//@brief	描画
		void	RenderFunc(CRenderer* _pRenderer, const CVec4& _color) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	メッシュを設定する処理
		void	LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;

		//-------------------------------------------------------------------------------------------------
		//@brief	頂点数取得
		u32		GetLineVertexNum() const { return 3; }
		//-------------------------------------------------------------------------------------------------
		//@brief	インデックス数取得
		u32		GetLineIndexNum() const { return 6; }
	};

	//*************************************************************************************************
	//@brief	箱
	class CBox
	{
		enum { VTX_NUM = 8 };
		enum { LINE_IND_NUM = 24 };

	public:
		CMtx44	pst;	///< 姿勢

		//-------------------------------------------------------------------------------------------------
		//@brief	描画
		void	RenderFunc(CRenderer* _pRenderer, const CVec4& _color) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	メッシュを設定する処理
		void	LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;

		//-------------------------------------------------------------------------------------------------
		//@brief	頂点数取得
		u32		GetLineVertexNum() const { return VTX_NUM; }
		//-------------------------------------------------------------------------------------------------
		//@brief	インデックス数取得
		u32		GetLineIndexNum() const { return LINE_IND_NUM; }
	};

	//*************************************************************************************************
	//@brief	円柱
	class CPipe
	{
	public:
		CMtx44	pst;	///< 姿勢

		//-------------------------------------------------------------------------------------------------
		//@brief	描画
		void	RenderFunc(CRenderer* _pRenderer, const CVec4& _color) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	 メッシュを設定する処理
		void	LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;

		//-------------------------------------------------------------------------------------------------
		//@brief	頂点数取得
		u32		GetLineVertexNum() const;
		//-------------------------------------------------------------------------------------------------
		//@brief	インデックス数取得
		u32		GetLineIndexNum() const;
	};

	//*************************************************************************************************
	//@brief	球
	class CSphere
	{
	public:
		CMtx44	pst;	///< 姿勢

		//-------------------------------------------------------------------------------------------------
		//@brief	描画
		void	RenderFunc(CRenderer* _pRenderer, const CVec4& _color) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	メッシュを設定する処理
		void	LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;

		//-------------------------------------------------------------------------------------------------
		//@brief	頂点数取得
		u32		GetLineVertexNum() const;
		//-------------------------------------------------------------------------------------------------
		//@brief	インデックス数取得
		u32		GetLineIndexNum() const;
	};

	//*************************************************************************************************
	//@brief	楕円
	class CEllipse
	{
	public:
		CMtx44	pst;	///< 姿勢

		//-------------------------------------------------------------------------------------------------
		//@brief	描画
		void	RenderFunc(CRenderer* _pRenderer, const CVec4& _color) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	メッシュを設定する処理
		void	LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;

		//-------------------------------------------------------------------------------------------------
		//@brief	頂点数取得
		u32		GetLineVertexNum() const;
		//-------------------------------------------------------------------------------------------------
		//@brief	インデックス数取得
		u32		GetLineIndexNum() const;
	};

	//*************************************************************************************************
	//@brief	カプセル
	class CCapsule
	{
	public:
		CVec4	pos[2];	//< 頂点
		f32		rad;	//< 半径

		//-------------------------------------------------------------------------------------------------
		//@brief	描画
		void	RenderFunc(CRenderer* _pRenderer, const CVec4& _color) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	メッシュを設定する処理
		void	LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;

		//-------------------------------------------------------------------------------------------------
		//@brief	頂点数取得
		u32		GetLineVertexNum() const;
		//-------------------------------------------------------------------------------------------------
		//@brief	インデックス数取得
		u32		GetLineIndexNum() const;
	};

	//*************************************************************************************************
	//@brief	扇
	class CFan
	{
	public:
		CMtx44	pst;	///< 姿勢

		//-------------------------------------------------------------------------------------------------
		//@brief	描画
		void	RenderFunc(CRenderer* _pRenderer, const CVec4& _color) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	メッシュを設定する処理
		void	LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;

		//-------------------------------------------------------------------------------------------------
		//@brief	頂点数取得
		u32		GetLineVertexNum() const;
		//-------------------------------------------------------------------------------------------------
		//@brief	インデックス数取得
		u32		GetLineIndexNum() const;
	};

	//*************************************************************************************************
	//@brief	空間軸
	class CAxis
	{
	public:
		CMtx44	pst;	///< 姿勢

		//-------------------------------------------------------------------------------------------------
		//@brief	描画
		void	RenderFunc(CRenderer* _pRenderer, const CVec4& _color) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	メッシュを設定する処理
		void	LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;

		//-------------------------------------------------------------------------------------------------
		//@brief	頂点数取得
		u32		GetLineVertexNum() const { return 6; }
		//-------------------------------------------------------------------------------------------------
		//@brief	インデックス数取得
		u32		GetLineIndexNum() const { return 6; }
	};

	//*************************************************************************************************
	//@brief	三角形 塗りつぶし
	class CTriangleFill
	{
	public:
		CVec4	pos[3];			//< 頂点
		CVec4	m_color_edge;	//< 色

		//-------------------------------------------------------------------------------------------------
		//@brief	描画
		void	RenderFunc(CRenderer* _pRenderer, const CVec4& _color) const;

		//-------------------------------------------------------------------------------------------------
		//@brief	メッシュを設定する処理
		void	LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	頂点数取得
		u32		GetLineVertexNum() const { return 3; }
		//-------------------------------------------------------------------------------------------------
		//@brief	インデックス数取得
		u32		GetLineIndexNum() const { return 6; }

		//-------------------------------------------------------------------------------------------------
		//@brief	塗りつぶしメッシュ設定
		void	FillMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	塗りつぶし頂点数取得
		u32		GetFillVertexNum() const { return 3; }
		//-------------------------------------------------------------------------------------------------
		//@brief	塗りつぶしインデックス数取得
		u32		GetFillIndexNum() const { return 3; }
	};

	//*************************************************************************************************
	//@brief	箱 塗りつぶし
	class CBoxFill
	{
	public:
		CMtx44	pst;	///< 姿勢

		enum { VTX_NUM = 8 };
		enum { LINE_IND_NUM = 24 };
		enum { FILL_IND_NUM = 36 };

		//-------------------------------------------------------------------------------------------------
		//@brief	描画
		void	RenderFunc(CRenderer* _pRenderer, const CVec4& _color) const;

		//-------------------------------------------------------------------------------------------------
		//@brief	 メッシュを設定する処理
		void	LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	頂点数取得
		u32		GetLineVertexNum() const { return VTX_NUM; }
		//-------------------------------------------------------------------------------------------------
		//@brief	インデックス数取得
		u32		GetLineIndexNum() const { return LINE_IND_NUM; }

		//-------------------------------------------------------------------------------------------------
		//@brief	塗りつぶしメッシュ設定
		void	FillMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	塗りつぶし頂点数取得
		u32		GetFillVertexNum() const { return VTX_NUM; }
		//-------------------------------------------------------------------------------------------------
		//@brief	塗りつぶしインデックス数取得
		u32		GetFillIndexNum() const { return FILL_IND_NUM; }
	};

	//*************************************************************************************************
	//@brief	スプライト
	class CSprite
	{
	public:
		enum POS_FLAG
		{
			POS_NONE	= 0,
			POS_LEFT	= (1<<0),
			POS_RIGHT	= (1<<1),
			POS_TOP		= (1<<2),
			POS_BOTTOM	= (1<<3),
		};

	public:
		CVec3	scale;		//< 幅、高さ
		CVec3	pos;		//< 3D座標
		f32		rot;		//< Z軸回転
		f32		xOffs;		//< Xオフセット
		f32		yOffs;		//< Yオフセット
		f32		zOffs;		//< Zオフセット
		u8		posFlag;	//< 座標位置(POS_FLAGで基準の位置を決める)

		//-------------------------------------------------------------------------------------------------
		//@brief	描画
		void	RenderFunc(CRenderer* _pRenderer, const CVec4& _color) const;
	};

	//*************************************************************************************************
	//@brief	フォント
	class CFont
	{
	public:
		CVec3		pos;		//< 3D座標
		CVec4		edgeClr;	//< 縁カラー
		const c8*	pString;	//< 文字
		f32			xOffs;		//< Xオフセット
		f32			yOffs;		//< Yオフセット
		f32			zOffs;		//< Zオフセット
		f32			width;		//< 幅
		f32			height;		//< 高さ
		b8			bDrawEdge;	//< 縁あり
		b8			bUTF8;		//< UTF8
	};

	//*************************************************************************************************
	//@brief	方向
	class CDir
	{
	public:
		CVec3	pos;	///< 位置
		CVec3	dir;	///< 方向

		//-------------------------------------------------------------------------------------------------
		//@brief	描画
		void	RenderFunc(CRenderer* _pRenderer, const CVec4& _color) const;
	};

	//*************************************************************************************************
	//@brief	箱 (領域に入ったポリゴン加算塗りつぶし)
	class CBoxAreaFill
	{
	public:
		CMtx44	pst;	///< 姿勢

		enum { VTX_NUM = 8 };
		enum { FILL_IND_NUM = 36 };
		enum { LINE_IND_NUM = 24 };

		//-------------------------------------------------------------------------------------------------
		//@brief	描画
		//@param[in] _pRenderer
		//@param[in] _color
		//@param[in] _pDepthTex	デプステクスチャを指定してください。
		void	RenderFunc(CRenderer* _pRenderer, const CVec4& _color, const CTextureBuffer* _pDepthTex) const;

		//-------------------------------------------------------------------------------------------------
		//@brief	メッシュを設定する処理
		void	LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	頂点数取得
		u32		GetLineVertexNum() const { return VTX_NUM; }
		//-------------------------------------------------------------------------------------------------
		//@brief	インデックス数取得
		u32		GetLineIndexNum() const { return LINE_IND_NUM; }

		//-------------------------------------------------------------------------------------------------
		//@brief	塗りつぶしメッシュ設定
		void	FillMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	塗りつぶしインデックス数取得
		u32		GetFillIndexNum() const { return FILL_IND_NUM; }

		//-------------------------------------------------------------------------------------------------
		//@brief	領域塗りつぶし用ユニフォームデータ取得
		void	GetAreaFillParam(CMtx44& _dst) const;
	};

	//*************************************************************************************************
	//@brief	球 (領域に入ったポリゴン加算塗りつぶし)
	class CSphereAreaFill
	{
	public:
		CMtx44	pst;	///< 姿勢

		enum { VTX_NUM = 8 };
		enum { FILL_IND_NUM = 36 };

		//-------------------------------------------------------------------------------------------------
		//@brief	描画
		//@param[in] _pRenderer
		//@param[in] _color
		//@param[in] _pDepthTex	デプステクスチャを指定してください。
		void	RenderFunc(CRenderer* _pRenderer, const CVec4& _color, const CTextureBuffer* _pDepthTex) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	メッシュを設定する処理
		void	LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;

		//-------------------------------------------------------------------------------------------------
		//@brief	頂点数取得
		u32		GetLineVertexNum() const;
		//-------------------------------------------------------------------------------------------------
		//@brief	インデックス数取得
		u32		GetLineIndexNum() const;

		//-------------------------------------------------------------------------------------------------
		//@brief	塗りつぶしメッシュ設定
		void	FillMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	領域塗りつぶし用ユニフォームデータ取得
		void	GetAreaFillParam(CMtx44& _dst) const;
	};

	//*************************************************************************************************
	//@brief	カプセル (領域に入ったポリゴン加算塗りつぶし)
	class CCapsuleAreaFill
	{
	public:
		//CMtx44	pst;	//< 姿勢
		CVec4		pos[2];	//< 頂点
		f32			rad;	//< 半径

		enum { VTX_NUM = 8 };
		enum { FILL_IND_NUM = 36 };

		//-------------------------------------------------------------------------------------------------
		//@brief	描画
		//@param[in] _pRenderer
		//@param[in] _color
		//@param[in] _pDepthTex	デプステクスチャを指定してください。
		void	RenderFunc(CRenderer* _pRenderer, const CVec4& _color, const CTextureBuffer* _pDepthTex) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	メッシュを設定する処理
		void	LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;

		//-------------------------------------------------------------------------------------------------
		//@brief	頂点数取得
		u32		GetLineVertexNum() const;
		//-------------------------------------------------------------------------------------------------
		//@brief	インデックス数取得
		u32		GetLineIndexNum() const;

		//-------------------------------------------------------------------------------------------------
		//@brief	塗りつぶしメッシュ設定
		void	FillMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	領域塗りつぶし用ユニフォームデータ取得
		void	GetAreaFillParam(CMtx44& _dst) const;
	};

	//*************************************************************************************************
	//@brief	円柱 (領域に入ったポリゴン加算塗りつぶし)
	class CPipeAreaFill
	{
	public:
		CMtx44	pst;	///< 姿勢

		enum { VTX_NUM = 8 };
		enum { FILL_IND_NUM = 36 };

		//-------------------------------------------------------------------------------------------------
		//@brief	描画
		//@param[in] _pRenderer
		//@param[in] _color
		//@param[in] _pDepthTex	デプステクスチャを指定してください。
		void	RenderFunc(CRenderer* _pRenderer, const CVec4& _color, const CTextureBuffer* _pDepthTex) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	メッシュを設定する処理
		void	LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;

		//-------------------------------------------------------------------------------------------------
		//@brief	頂点数取得
		u32		GetLineVertexNum() const;
		//-------------------------------------------------------------------------------------------------
		//@brief	インデックス数取得
		u32		GetLineIndexNum() const;

		//-------------------------------------------------------------------------------------------------
		//@brief	塗りつぶしメッシュ設定
		void	FillMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	領域塗りつぶし用ユニフォームデータ取得
		void	GetAreaFillParam(CMtx44& _dst) const;
	};

	//*************************************************************************************************
	//@brief	扇 (領域に入ったポリゴン加算塗りつぶし)
	class CFanAreaFill
	{
	public:
		CMtx44	pst;	///< 姿勢

		enum { VTX_NUM = 8 };
		enum { FILL_IND_NUM = 36 };

		//-------------------------------------------------------------------------------------------------
		//@brief	描画
		//@param[in] _pRenderer
		//@param[in] _color
		//@param[in] _pDepthTex	デプステクスチャを指定してください。
		void	RenderFunc(CRenderer* _pRenderer, const CVec4& _color, const CTextureBuffer* _pDepthTex) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	メッシュを設定する処理
		void	LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;

		//-------------------------------------------------------------------------------------------------
		//@brief	頂点数取得
		u32		GetLineVertexNum() const;
		//-------------------------------------------------------------------------------------------------
		//@brief	インデックス数取得
		u32		GetLineIndexNum() const;

		//-------------------------------------------------------------------------------------------------
		//@brief	塗りつぶしメッシュ設定
		void	FillMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const CVec4& _color) const;
		//-------------------------------------------------------------------------------------------------
		//@brief	領域塗りつぶし用ユニフォームデータ取得
		void	GetAreaFillParam(CMtx44& _dst) const;
	};


public:
	//-------------------------------------------------------------------------------------------------
	//@brief	構築と消滅
	CRenderPrimElm() { Initialize(); }
	~CRenderPrimElm()	{}

	//-------------------------------------------------------------------------------------------------
	//@brief	初期化
	void	Initialize();

	//-------------------------------------------------------------------------------------------------
	//@brief	取得
	PRIM					GetType() const				{ return m_type; }
	const CLine*			GetAsLine()	const			{ return PCast<const CLine*>(m_data); }
	const CTriangle*		GetAsTriangle() const		{ return PCast<const CTriangle*>(m_data); }
	const CBox*				GetAsBox() const			{ return PCast<const CBox*>(m_data); }
	const CPipe*			GetAsPipe()	const			{ return PCast<const CPipe*>(m_data); }
	const CSphere*			GetAsSphere() const			{ return PCast<const CSphere*>(m_data); }
	const CEllipse*			GetAsEllipse() const		{ return PCast<const CEllipse*>(m_data); }
	const CCapsule*			GetAsCapsule() const		{ return PCast<const CCapsule*>(m_data); }
	const CFan*				GetAsFan() const			{ return PCast<const CFan*>(m_data); }
	const CAxis*			GetAsAxis() const			{ return PCast<const CAxis*>(m_data); }
	const CTriangleFill*	GetAsTriangleFill() const	{ return PCast<const CTriangleFill*>(m_data); }
	const CBoxFill*			GetAsBoxFill() const		{ return PCast<const CBoxFill*>(m_data); }
	const CSprite*			GetAsSprite() const			{ return PCast<const CSprite*>(m_data); }
	const CFont*			GetAsFont() const			{ return PCast<const CFont*>(m_data); }
	const CDir*				GetAsDir() const			{ return PCast<const CDir*>(m_data); }
	const CBoxAreaFill*		GetAsBoxAreaFill() const	{ return PCast<const CBoxAreaFill*>(m_data); }
	const CSphereAreaFill*	GetAsSphereAreaFill() const	{ return PCast<const CSphereAreaFill*>(m_data); }
	const CCapsuleAreaFill*	GetAsCapsuleAreaFill() const{ return PCast<const CCapsuleAreaFill*>(m_data); }
	const CPipeAreaFill*	GetAsPipeAreaFill() const	{ return PCast<const CPipeAreaFill*>(m_data); }
	const CFanAreaFill*		GetAsFanAreaFill() const	{ return PCast<const CFanAreaFill*>(m_data); }

	//-------------------------------------------------------------------------------------------------
	//@brief	色
	//@note	エッジカラーをそのまま、フェイスカラーをアルファ値の1/2で設定します。
	void	SetColor(f32 _r, f32 _g, f32 _b, f32 _a = 1.f);
	void	SetColor(const CVec3& _clr);
	void	SetColor(const CVec4& _clr);

	void	SetEdgeColor(f32 _r, f32 _g, f32 _b, f32 _a = 1.f);
	void	SetEdgeColor(const CVec3& _clr);
	void	SetEdgeColor(const CVec4& _clr);

	void	SetFaceColor(f32 _r, f32 _g, f32 _b, f32 _a);
	void	SetFaceColor(const CVec3& _clr);
	void	SetFaceColor(const CVec4& _clr);

	//-------------------------------------------------------------------------------------------------
	//@brief	Zテスト
	enum	ZTEST
	{
		ZTEST_DEFAULT	= -1,		// 通常描画（ZTest無しで薄く描画＋ZTest有りで濃く描画）
		ZTEST_NONE		= 0,			// Zテスト無しで１回描画
		ZTEST_ENABLE	= 1,			// Zテスト有りで１回描画
	};	
	void	SetZTest(ZTEST ztest);

	//-------------------------------------------------------------------------------------------------
	//@brief	線分
	void	SetAsLine(f32 _x0, f32 _y0, f32 _z0, f32 _x1, f32 _y1, f32 _z1);
	void	SetAsLine(const CVec3& _p0, const CVec3& _p1);
	void	SetAsLine(const CVec4& _p0, const CVec4& _p1);

	//-------------------------------------------------------------------------------------------------
	//@brief	三角形
	void	SetAsTriangle(f32 _x0, f32 _y0, f32 _z0, f32 _x1, f32 _y1, f32 _z1, f32 _x2, f32 _y2, f32 _z2);
	void	SetAsTriangle(const CVec3& _p0, const CVec3& _p1, const CVec3& _p2);
	void	SetAsTriangle(const CVec4& _p0, const CVec4& _p1, const CVec4& _p2);

	//-------------------------------------------------------------------------------------------------
	//@brief	箱
	void	SetAsBox(const CQuat& _qat, const CVec3& _cnt, const CVec3& _scl);
	void	SetAsBox(const CQuat& _qat, const CVec3& _cnt, f32 _sx, f32 _sy, f32 _sz);
	void	SetAsBox(const CQuat& _qat, const CVec3& _cnt, f32 _scl);
	void	SetAsBox(const CVec3& _cnt, f32 _sx, f32 _sy, f32 _sz);
	void	SetAsBox(const CVec3& _cnt, f32 _scl);
	void	SetAsBox(const CVec3& _max, const CVec3& _min);
	void	SetAsBox(const CVec4& _max, const CVec4& _min);
	void	SetAsBox(const CMtx34& _pst);
	void	SetAsBox(const CMtx44& _pst);

	//-------------------------------------------------------------------------------------------------
	//@brief	円柱
	void	SetAsPipe(const CVec3& _btm, f32 _rad, f32 _hgt);
	void	SetAsPipe(const CVec4& _btm, f32 _rad, f32 _hgt);
	void	SetAsPipe(const CVec3& _p0, const CVec3& _p1, f32 _rad);
	void	SetAsPipe(const CVec4& _p0, const CVec4& _p1, f32 _rad);
	void	SetAsPipe(const CMtx34& _pst);
	void	SetAsPipe(const CMtx44& _pst);

	//-------------------------------------------------------------------------------------------------
	//@brief	球
	void	SetAsSphere(const CQuat& _qat, const CVec3& _cnt, f32 _sx, f32 _sy, f32 _sz);
	void	SetAsSphere(const CQuat& _qat, const CVec3& _cnt, const CVec3& _scl);
	void	SetAsSphere(const CVec3& _cnt, f32 _rad);
	void	SetAsSphere(const CVec4& _cnt, f32 _rad);
	void	SetAsSphere(const CMtx34& _pst);
	void	SetAsSphere(const CMtx44& _pst);

	//-------------------------------------------------------------------------------------------------
	//@brief	楕円
	void	SetAsEllipse(const CQuat& _qat, const CVec3& _cnt, f32 _sx, f32 _sy, f32 _sz);
	void	SetAsEllipse(const CQuat& _qat, const CVec3& _cnt, CVec3& _scl);
	void	SetAsEllipse(const CMtx34& _pst);
	void	SetAsEllipse(const CMtx44& _pst);

	//-------------------------------------------------------------------------------------------------
	//@brief	カプセル
	void	SetAsCapsule(const CVec3& _p0, const CVec3& _p1, f32 _rad);
	void	SetAsCapsule(const CVec4& _p0, const CVec4& _p1, f32 _rad);

	//-------------------------------------------------------------------------------------------------
	//@brief	扇
	//@param[in] _btm
	//@param[in] _yRot
	//@param[in] _rad
	//@param[in] _hgt
	//@param[in] _rot		扇のラジアンを指定してください。
	void	SetAsFan(const CVec3& _btm, f32 _yRot, f32 _rad, f32 _hgt, f32 _rot);
	void	SetAsFan(const CVec4& _btm, f32 _yRot, f32 _rad, f32 _hgt, f32 _rot);
	void	SetAsFan(const CVec3& _btm, const CQuat& _qat, f32 _rad, f32 _hgt, f32 _rot);
	void	SetAsFan(const CVec4& _btm, const CQuat& _qat, f32 _rad, f32 _hgt, f32 _rot);
	void	SetAsFan(const CVec3& _btm, const CVec3& _vec, f32 _rad, f32 _hgt, f32 _rot);
	void	SetAsFan(const CVec4& _btm, const CVec4& _vec, f32 _rad, f32 _hgt, f32 _rot);
	void	SetAsFan(const CMtx34& _pst, f32 _rot);
	void	SetAsFan(const CMtx44& _pst, f32 _rot);

	//-------------------------------------------------------------------------------------------------
	//@brief	空間軸
	void	SetAsAxis(const CMtx34& _pst, f32 _scale = 1.f);
	void	SetAsAxis(const CMtx44& _pst, f32 _scale = 1.f);

	//-------------------------------------------------------------------------------------------------
	//@brief	三角形
	void	SetAsTriangleFill(f32 _x0, f32 _y0, f32 _z0, f32 _x1, f32 _y1, f32 _z1, f32 _x2, f32 _y2, f32 _z2, const CVec4& edge_color, b8 _onlyFront=false);
	void	SetAsTriangleFill(const CVec3& _p0, const CVec3& _p1, const CVec3& _p2, const CVec4& edge_color, b8 _onlyFront=false);
	void	SetAsTriangleFill(const CVec4& _p0, const CVec4& _p1, const CVec4& _p2, const CVec4& edge_color, b8 _onlyFront = false);
	void	SetAsTriangleFill(const CVec3& _p0, const CVec3& _p1, const CVec3& _p2, b8 _onlyFront = false);
	void	SetAsTriangleFill(const CVec4& _p0, const CVec4& _p1, const CVec4& _p2, b8 _onlyFront = false);

	//-------------------------------------------------------------------------------------------------
	//@brief	箱（枠描画は行わない。（dataのサイズがconst CMtx44&のため、これ以上増やせない）枠描画をしたい場合は、別途行うこと
	void	SetAsBoxFill(const CQuat& _qat, const CVec3& _cnt, CVec3& _scl);
	void	SetAsBoxFill(const CQuat& _qat, const CVec3& _cnt, f32 _sx, f32 _sy, f32 _sz);
	void	SetAsBoxFill(const CQuat& _qat, const CVec3& _cnt, f32 _scl);
	void	SetAsBoxFill(const CVec3& _cnt, f32 _sx, f32 _sy, f32 _sz);
	void	SetAsBoxFill(const CVec3& _cnt, f32 _scl);
	void	SetAsBoxFill(const CVec3& _max, const CVec3& _min);
	void	SetAsBoxFill(const CVec4& _max, const CVec4& _min);
	void	SetAsBoxFill(const CMtx34& _pst);
	void	SetAsBoxFill(const CMtx44& _pst);

	//-------------------------------------------------------------------------------------------------
	//@brief	箱 (領域内に入ったポリゴンに加算塗りつぶし)
	void	SetAsBoxAreaFill(const CQuat& _qat, const CVec3& _cnt, CVec3& _scl);
	void	SetAsBoxAreaFill(const CQuat& _qat, const CVec3& _cnt, f32 _sx, f32 _sy, f32 _sz);
	void	SetAsBoxAreaFill(const CQuat& _qat, const CVec3& _cnt, f32 _scl);
	void	SetAsBoxAreaFill(const CVec3& _cnt, f32 _sx, f32 _sy, f32 _sz);
	void	SetAsBoxAreaFill(const CVec3& _cnt, f32 _scl);
	void	SetAsBoxAreaFill(const CVec3& _max, const CVec3& _min);
	void	SetAsBoxAreaFill(const CVec4& _max, const CVec4& _min);
	void	SetAsBoxAreaFill(const CMtx34& _pst);
	void	SetAsBoxAreaFill(const CMtx44& _pst);

	//-------------------------------------------------------------------------------------------------
	//@brief	球 (領域内に入ったポリゴンに加算塗りつぶし)
	void	SetAsSphereAreaFill(const CQuat& _qat, const CVec3& _cnt, f32 _sx, f32 _sy, f32 _sz);
	void	SetAsSphereAreaFill(const CQuat& _qat, const CVec3& _cnt, const CVec3& _scl);
	void	SetAsSphereAreaFill(const CVec3& _cnt, f32 _rad);
	void	SetAsSphereAreaFill(const CVec4& _cnt, f32 _rad);
	void	SetAsSphereAreaFill(const CMtx34& _pst);
	void	SetAsSphereAreaFill(const CMtx44& _pst);

	//-------------------------------------------------------------------------------------------------
	//@brief	カプセル (領域内に入ったポリゴンに加算塗りつぶし)
	void	SetAsCapsuleAreaFill(const CVec3& _p0, const CVec3& _p1, f32 _rad);
	void	SetAsCapsuleAreaFill(const CVec4& _p0, const CVec4& _p1, f32 _rad);

	//-------------------------------------------------------------------------------------------------
	//@brief	円柱 (領域内に入ったポリゴンに加算塗りつぶし)
	void	SetAsPipeAreaFill(const CVec3& _btm, f32 _rad, f32 _hgt);
	void	SetAsPipeAreaFill(const CVec4& _btm, f32 _rad, f32 _hgt);
	void	SetAsPipeAreaFill(const CVec3& _p0, const CVec3& _p1, f32 _rad);
	void	SetAsPipeAreaFill(const CVec4& _p0, const CVec4& _p1, f32 _rad);
	void	SetAsPipeAreaFill(const CMtx34& _pst);
	void	SetAsPipeAreaFill(const CMtx44& _pst);

	//-------------------------------------------------------------------------------------------------
	//@brief	扇 (領域内に入ったポリゴンに加算塗りつぶし)
	//@param[in] _btm
	//@param[in] _yRot
	//@param[in] _rad
	//@param[in] _hgt
	//@param[in] _rot		扇のラジアンを指定してください。
	void	SetAsFanAreaFill(const CVec3& _btm, f32 _yRot, f32 _rad, f32 _hgt, f32 _rot);
	void	SetAsFanAreaFill(const CVec4& _btm, f32 _yRot, f32 _rad, f32 _hgt, f32 _rot);
	void	SetAsFanAreaFill(const CVec3& _btm, const CQuat& _qat, f32 _rad, f32 _hgt, f32 _rot);
	void	SetAsFanAreaFill(const CVec4& _btm, const CQuat& _qat, f32 _rad, f32 _hgt, f32 _rot);
	void	SetAsFanAreaFill(const CVec3& _btm, const CVec3& _vec, f32 _rad, f32 _hgt, f32 _rot);
	void	SetAsFanAreaFill(const CVec4& _btm, const CVec4& _vec, f32 _rad, f32 _hgt, f32 _rot);
	void	SetAsFanAreaFill(const CMtx34& _pst, f32 _rot);
	void	SetAsFanAreaFill(const CMtx44& _pst, f32 _rot);

	//-------------------------------------------------------------------------------------------------
	//@brief	スプライト(xPosFlagがマイナスなら左あわせ、プラスなら右あわせ、yPosFlagがマイナスなら上あわせ、プラスなら下あわせ)
	void	SetAsSprite(const CVec3& pos, u16 widthPixel, u16 heightPixel, f32 zOffs = 0.f, f32 rot = 0.f, s32 xPosFlag = 0, s32 yPosFlag = 0, f32 xOffs = 0, f32 yOffs = 0 );

	//-------------------------------------------------------------------------------------------------
	//@brief	フォント描画
	void	SetAsFont(const CVec3& pos, const c8* pString, f32 width, f32 height, f32 zOffs = 0.f, b8 bDrawEdge = true, const CVec4& vEdgeColor = CVec4( 0.f, 0.f, 0.f, 1.f ), f32 xOffs = 0, f32 yOffs = 0 );
	void	SetAsFontUTF8(const CVec3& pos, const c8* pString, f32 width, f32 height, f32 zOffs = 0.f, b8 bDrawEdge = true, const CVec4& vEdgeColor = CVec4( 0.f, 0.f, 0.f, 1.f ), f32 xOffs = 0, f32 yOffs = 0 );

	//-------------------------------------------------------------------------------------------------
	//@brief	方向描画
	void	SetAsDir( const CVec3& _pos, const CVec3& _dir );


	//-------------------------------------------------------------------------------------------------
	//@brief	描画
	void	RenderFunc(CRenderer* _pRenderer, const f32 fColor, const f32 fAlpha, const s32 progress) const;
	void	RenderFuncAreaFill(CRenderer* _pRenderer, const CVec4& _color, const CTextureBuffer* _pDepthTex) const;

	//-------------------------------------------------------------------------------------------------
	//@brief	メッシュを設定する処理
	void	LineMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const f32 fColor, const f32 fAlpha) const;
	//-------------------------------------------------------------------------------------------------
	//@brief	このメッシュに必要な頂点数取得
	u32		GetLineVertexNum() const;
	//-------------------------------------------------------------------------------------------------
	//@brief	このメッシュに必要なインデックス数取得
	u32		GetLineIndexNum() const;

	//-------------------------------------------------------------------------------------------------
	//@brief	塗りつぶしメッシュを設定する処理
	void	FillMeshSetFunc(VERTEX* _vertex_list, u32* _idx_list, u32 _vertex_start_idx, u32 _idx_list_start_idx, const f32 fColor, const f32 fAlpha) const;
	//-------------------------------------------------------------------------------------------------
	//@brief	塗りつぶしに必要な頂点数取得
	u32		GetFillVertexNum() const;
	//-------------------------------------------------------------------------------------------------
	//@brief	塗りつぶしに必要なインデックス数取得
	u32		GetFillIndexNum() const;
	//-------------------------------------------------------------------------------------------------
	//@brief	領域塗りつぶしに必要なユニフォームデータ取得
	void	GetAreaFillParam(CMtx44& _dst) const;

	//-------------------------------------------------------------------------------------------------
	//@brief	塗りつぶしでカリング描画するか
	b8		IsFillCullFace() const;

	//-------------------------------------------------------------------------------------------------
	//@brief	カラー取得
	const CVec4& GetColor() const { return m_edgeColor; }
	const CVec4& GetFaceColor() const { return m_faceColor; }

	//-------------------------------------------------------------------------------------------------
	//@brief	フラグ取得
	u8		GetFlag() const {return m_flag; }
	//-------------------------------------------------------------------------------------------------
	//@brief	フラグ設定
	void	SetFlag(u8 _flag) { m_flag = _flag; }

	//-------------------------------------------------------------------------------------------------
	//@brief	ZTEST フラグ取得
	ZTEST	GetZTest() const { return m_ztest; }

private:
	// 色
	CVec4	m_edgeColor;
	CVec4	m_faceColor;

	// データ
	u8		m_data[sizeof(CMtx44)];

	// タイプ
	PRIM	m_type:8;
	ZTEST	m_ztest:8;

	u8		m_flag;
};

POISON_END
	

#endif // __RENDER_PRIM_ELEMENT_H__

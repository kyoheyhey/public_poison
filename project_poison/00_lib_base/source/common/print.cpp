﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "lib_base_def.h"
#include "print.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant

/// メモリハンドルをアトリビュート変換関数
CPrintUtility::FUNC_PRINT		CPrintUtility::s_funcPrint = NULL;
CPrintUtility::FUNC_MEM_TO_ATTR	CPrintUtility::s_funcMemToAttr = NULL;


///-------------------------------------------------------------------------------------------------
/// コールバックを使用してプリント表示
void	CPrintUtility::Print(const u32 _attr, const c8* _func, const c8* _file, const u32 _line, const c8* _format, ...)
{
	c8 buf[2048];
	va_list vlist;
	va_start(vlist, _format);
	vsprintf_safe(buf, sizeof(buf), _format, vlist);
	va_end(vlist);
	if (s_funcPrint == NULL) { CPrintUtility::PrintConsole(buf); }
	else if ((*s_funcPrint)(_attr, buf, _func, _file, _line)) { CPrintUtility::PrintConsole(buf); }
}

///-------------------------------------------------------------------------------------------------
/// コンソール画面に文字列描画
void CPrintUtility::PrintConsole(const c8* _str)
{
	__PRINT_FUNC_UTF8(_str);
}


POISON_END

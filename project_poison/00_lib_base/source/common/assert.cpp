﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "lib_base_def.h"
#include "assert.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant
static b8	s_enable_assert_debug_break = true;


b8		IsEnableAssertDebugBreak() { return s_enable_assert_debug_break; }
void	SetEnableAssertDebugBreak(const b8 _flag) { s_enable_assert_debug_break = _flag; }

POISON_END

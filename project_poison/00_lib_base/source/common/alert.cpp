﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "lib_base_def.h"
#include "alert.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant



///-------------------------------------------------------------------------------------------------
/// バックトレース文字列取得
inline const c8*	GetBackTraceString(c8 _buf[], u32 _size, const c8* _function)
{
#ifndef POISON_RELEASE
	// 元の文字列よりサイズがない
	u32 len = SCast<u32>(strlen(_function));
	if (_size <= len + 1)
	{
		return _function;
	}

	SYMBOL_INFO info[8];
	u32 num = GetBackTrace(info, ARRAYOF(info), 3);
	if (num <= 0) { return _function; }

	// 最初の関数はそのままコピー
	memcpy(_buf, _function, len);

	c8* p = _buf + len;
	(*p) = '\n';
	++p;

	s32 size = _size - SCast<s32>(p - _buf);

	for (u32 i = 0; i < num; i++)
	{
		s32 offs = 0;
		// 文字列なし
		if (info[i].name[0] == 0)
		{
			offs = sprintf_safe(p, size, "\t[%d]: 0x%p", i, info[i].address);
		}
		// 文字列あり
		else
		{
			offs = sprintf_safe(p, size, "\t[%d]: %s", i, info[i].name);
		}

		p += offs;
		size -= offs;
		if (size <= 1)
		{
			break;
		}
		if (i + 1 < num)
		{
			(*p) = '\n';
			++p;
			--size;
		}
	}
	return _buf;
#else
	return _function;
#endif // POISON_RELEASE
}



///*************************************************************************************************
/// アラート
///*************************************************************************************************

///-------------------------------------------------------------------------------------------------
/// PrintAlert
void	CAlertUtility::ShowAlertArgs(const u32 _attr, const c8* _func, const c8* _file, const u32 _line, u8* _ptr_state, const c8* _format, ...)
{
	CAlertUtility::INFO _info(_attr, _func, _file, _line, NULL, _ptr_state);

	c8 buf[2048];
	s32 offs = 0;
	{
		va_list vlist;
		va_start(vlist, _format);
		offs = vsprintf_safe(buf, sizeof(buf), _format, vlist);
		va_end(vlist);
		// 差し替え
		_info.pMessage = buf;
	}

	// 関数をバックトレースに置き換え
	{
		_info.pFuncName = GetBackTraceString(buf + offs + 1, sizeof(buf)-offs - 1, _info.pFuncName);
	}

	ShowAlert(_info);
}


///-------------------------------------------------------------------------------------------------
/// PrintWarning
void	CAlertUtility::ShowWarningArgs(const u32 _attr, const c8* _func, const c8* _file, const u32 _line, u8* _ptr_state, const c8* _format, ...)
{
	CAlertUtility::INFO _info(_attr, _func, _file, _line, NULL, _ptr_state);

	c8 buf[2048];
	s32 offs = 0;
	{
		va_list vlist;
		va_start(vlist, _format);
		offs = vsprintf_safe(buf, sizeof(buf), _format, vlist);
		va_end(vlist);
		// 差し替え
		_info.pMessage = buf;
	}

	// 関数をバックトレースに置き換え
	{
		_info.pFuncName = GetBackTraceString(buf + offs + 1, sizeof(buf)-offs - 1, _info.pFuncName);
	}

	ShowWarning(_info);
}


///-------------------------------------------------------------------------------------------------
/// プリントアラート
static inline void	PrintAlertWarning(const c8* _head, const u32 _attr, const c8* _str, const c8* _func, const c8* _file, const u32 _line)
{
	PRINT_ASSERT("*****[%s][%d]: %s\n", _head, _attr, _str);
	PRINT_ASSERT("  File: %s\n", _file);
	PRINT_ASSERT("  Line: %d\n", _line);
	PRINT_ASSERT("  Func: %s\n", _func);
}


///-------------------------------------------------------------------------------------------------
void	CAlertUtility::PrintAlert(const INFO& _info)
{
	PrintAlertWarning("Alert", _info.attr, _info.pMessage, _info.pFuncName, _info.pFileName, _info.fileLine);
}


///-------------------------------------------------------------------------------------------------
void	CAlertUtility::PrintWarning(const INFO& _info)
{
	PrintAlertWarning("Warning", _info.attr, _info.pMessage, _info.pFuncName, _info.pFileName, _info.fileLine);
}


CAlertUtility::FUNC_ALERT	CAlertUtility::s_funcAlert = CAlertUtility::PrintAlert;
CAlertUtility::FUNC_ALERT	CAlertUtility::s_funcWarning = CAlertUtility::PrintWarning;


POISON_END

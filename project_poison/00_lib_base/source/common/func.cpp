﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "lib_base_def.h"
#include "func.h"

#ifndef POISON_RELEASE
#include <imagehlp.h>
#pragma comment(lib, "imagehlp.lib")
#endif // !POISON_RELEASE


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant



///-------------------------------------------------------------------------------------------------
/// GetFileName
const c8*	GetFileName(const c8* _pPath)
{
	const u32 len = SCast<u32>(strlen(_pPath));
	return GetFileName(_pPath, len);
}
///-------------------------------------------------------------------------------------------------
/// GetFileName
const c8*	GetFileName(const c8* _pPath, const u32 _fileLen)
{
	//Assert(_pPath);
	if (_fileLen <= 2){ return _pPath; }   // 2文字以下はどうあがいても先頭

	const c8* p = _pPath + _fileLen;
	p -= 2;
	while (p != _pPath)
	{
		c8 c = (*p);
		// パス区切り文字
		if (c == '/' || c == '\\')
		{
			return p + 1;
		}
		--p;
	}

	return _pPath;
}

///-------------------------------------------------------------------------------------------------
/// GetFileExt
const c8*	GetFileExt(const c8* _pPath)
{
	const u32 len = SCast<u32>(strlen(_pPath));
	return GetFileExt(_pPath, len);
}
///-------------------------------------------------------------------------------------------------
/// GetFileExt
const c8*	GetFileExt(const c8* _pPath, const u32 _fileLen)
{
	//Assert(_pPath);
	if (_fileLen <= 1){ return NULL; }     // 1文字以下はどうあがいてもない

	const c8* p = _pPath + _fileLen;
	--p;
	// 最後の文字がピリオド
	if ((*p) == '.'){ return NULL; }
	--p;

	while (p != _pPath)
	{
		// ドットをさがす
		if ((*p) == '.')
		{
			return p + 1;
		}
		--p;
	}
	return NULL;
}

///-------------------------------------------------------------------------------------------------
/// GetConnectFilePath
void	GetConnectFilePath(c8* _pRetPath, const c8* _pPath0, const c8* _pPath1, const c8 _s /*= '/' */)
{
	GetConnectFilePath(_pRetPath, _pPath0, _pPath1, strlen(_pPath0), strlen(_pPath1), _s);
}



///-------------------------------------------------------------------------------------------------
/// GetConnectFilePath
void	GetConnectFilePath(c8* _pRetPath, const c8* _pPath0, const c8* _pPath1, const size_t _pathLen0, const size_t _pathLen1, const c8 _s /*= '/' */)
{
	u32 offs = 0;
	if (_pathLen0 > 0)
	{
		// コピー
		memcpy(_pRetPath, _pPath0, _pathLen0);
		// 末端の文字をチェック
		const c8 uEnd = _pRetPath[_pathLen0 - 1];
		// スラッシュがなかったらつける
		if (uEnd != '/' && uEnd != '\\')
		{
			_pRetPath[_pathLen0] = _s;
			++offs;
		}
	}
	// 文字列コピー
	memcpy(_pRetPath + _pathLen0 + offs, _pPath1, _pathLen1);
	// 末端
	_pRetPath[_pathLen0 + offs + _pathLen1] = 0;
}


#ifndef POISON_RELEASE

///-------------------------------------------------------------------------------------------------
/// バックトレース取得
u32 GetBackTrace(SYMBOL_INFO _ret[], u32 _retMaxNum, u32 _traceOffset)
{
#ifdef LIB_SYSTEM_WIN
	// プロセス取得
	HANDLE process = ::GetCurrentProcess();
	// シンボルハンドル初期化
	::SymInitialize(process, NULL, TRUE);

	void** stack = PCast<void**>(alloca(sizeof(void*)* _retMaxNum));
	if (!stack){ return 0; }

	u32 frames = CaptureStackBackTrace(_traceOffset + 1, _retMaxNum, stack, NULL);
	u8 buf[sizeof(::SYMBOL_INFO) + 256] = { 0 };
	::SYMBOL_INFO* pSymbol = PCast<::SYMBOL_INFO*>(buf);
	pSymbol->MaxNameLen = 255;
	pSymbol->SizeOfStruct = sizeof(::SYMBOL_INFO);

	for (u32 i = 0; i < frames; i++)
	{
		if (SymFromAddr(process, RCast<DWORD64>(stack[i]), 0, pSymbol))
		{
			_ret[i].address = RCast<const void*>(pSymbol->Address);
			strcpy_s(_ret[i].name, pSymbol->Name);
		}
		else
		{
			_ret[i].address = stack[i];
			_ret[i].name[0] = 0;
		}
	}
	return frames;
#endif // LIB_SYSTEM_WIN


#if 0
	SceDbgCallFrame* frame = PCast<SceDbgCallFrame*>(alloca(sizeof(SceDbgCallFrame)* _retMaxNum));
	u32 num = 0;
	if (SCE_OK == sceDbgBacktraceSelf(frame, _retMaxNum, &num, SCE_DBG_BACKTRACE_MODE_DONT_EXCEED))
	{
		for (u32 i = 0; i < num; i++)
		{
			_ret[i].address = RCast<const void*>(frame[i].sp);
			_ret[i].name[0] = 0;
		}
		return num;
	}
	return 0;
#endif
}


#endif // !POISON_RELEASE




POISON_END

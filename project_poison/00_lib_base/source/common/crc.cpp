﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "lib_base_def.h"
#include "crc.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant
const hash32	hash32::Invalid = INVALID_HASH32;	///< 無効値
const hash64	hash64::Invalid = INVALID_HASH64;	///< 無効値


///-------------------------------------------------------------------------------------------------
/// CRC変換
hash32		CCrcUtility::BaseCrc32(const c8* _str)
{
	hash32 crc = 0xffffffff;

	const u8* p = PCast<const u8*>(_str);

	while (true)
	{
		const u8 code = *(p++);
		if (!code)
		{
			break;
		}
		crc = (s_CCrcUtility_crcTable32[SCast<u8>(crc & 0xFF) ^ code] ^ (crc >> 8));
	}

	return (~crc);
}

///-------------------------------------------------------------------------------------------------
/// CRC変換
hash32		CCrcUtility::BaseCrc32(const void* _p, const u32 _size, const hash32 _base)
{
	hash32 crc = ~_base;

	const u8* p = PCast<const u8*>(_p);

	for (u32 i = 0; i < _size; i++)
	{
		const u8 code = *(p++);

		crc = (s_CCrcUtility_crcTable32[SCast<u8>(crc & 0xFF) ^ code] ^ (crc >> 8));
	}

	return (~crc);
}


POISON_END

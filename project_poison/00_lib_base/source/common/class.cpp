﻿
///-------------------------------------------------------------------------------------------------
/// define


///-------------------------------------------------------------------------------------------------
/// include
#include "lib_base_def.h"
#include "class.h"


POISON_BGN
///-------------------------------------------------------------------------------------------------
/// prototype


///-------------------------------------------------------------------------------------------------
/// constant
const s64	TypeMinMax<s8>::minValue = -128;
const u64	TypeMinMax<s8>::maxValue = 127;

const s64	TypeMinMax<s16>::minValue = -32768;
const u64	TypeMinMax<s16>::maxValue = 32767;

const s64	TypeMinMax<s32>::minValue = -2147483648ll;
const u64	TypeMinMax<s32>::maxValue = 2147483647ull;

const s64	TypeMinMax<s64>::minValue = -9223372036854775807ll - 1;
const u64	TypeMinMax<s64>::maxValue = 9223372036854775807ull;
//const s64	TypeMinMax<s64>::minValue = LONG_MIN;
//const u64	TypeMinMax<s64>::maxValue = LONG_MAX;

const s64	TypeMinMax<f32>::minValue = -9223372036854775807ll - 1;	// 浮動小数のオーバーフローのため、s64の最小値
const u64	TypeMinMax<f32>::maxValue = 9223372036854775807ull;		// 浮動小数のオーバーフローのため、u64の最大値
//const s64	TypeMinMax<f32>::minValue = -16777215;
//const u64	TypeMinMax<f32>::maxValue = 16777215;

const s64	TypeMinMax<f64>::minValue = -9223372036854775807ll - 1;	// 浮動小数のオーバーフローのため、s64の最小値
const u64	TypeMinMax<f64>::maxValue = 9223372036854775807ull;		// 浮動小数のオーバーフローのため、u64の最大値
//const s64	TypeMinMax<f64>::minValue = (s64)-9007199254740991;
//const u64	TypeMinMax<f64>::maxValue = (u64)9007199254740991;



//-------------------------------------------------------------------------------------------------
/// 整数取得
s32	CValue::GetInt(u32 _idx/*= 0*/) const
{
	libAssert(!IsPtr() && !IsStr());
	if (_idx == 0 && m_paramNum <= 1)
	{
		if (IsFloat())
		{
			return SCast<s32>(m_param.f);
		}
		else
		{
			return m_param.i;
		}
	}
	libAssert(_idx < m_paramNum);

	if (IsValuePtr())
	{
		return GetValuePtr()[_idx].GetInt();
	}
	else if (IsFloat())
	{
		return SCast<s32>(*(PCast<f32*>(m_param.p) + _idx));
	}
	else
	{
		return *(PCast<s32*>(m_param.p) + _idx);
	}
}

//-------------------------------------------------------------------------------------------------
/// GetUInt
u32	CValue::GetUInt(u32 _idx/*= 0*/) const
{
	libAssert(!IsPtr() && !IsStr());
	if (_idx == 0 && m_paramNum <= 1)
	{
		if (IsFloat())
		{
			return SCast<s32>(m_param.f);
		}
		else
		{
			return m_param.u;
		}
	}

	libAssert(_idx < m_paramNum);
	if (IsValuePtr())
	{
		return GetValuePtr()[_idx].GetUInt();
	}
	else if (IsFloat())
	{
		return SCast<u32>(*(PCast<f32*>(m_param.p) + _idx));
	}
	else
	{
		return *(PCast<u32*>(m_param.p) + _idx);
	}
}

///-------------------------------------------------------------------------------------------------
/// 小数取得
f32	CValue::GetFloat(u32 _idx/*= 0*/) const
{
	libAssert(!IsPtr() && !IsStr());
	if (_idx == 0 && m_paramNum <= 1)
	{
		if (IsFloat())
		{
			return m_param.f;
		}
		else
		{
			return SCast<f32>(m_param.i);
		}
	}
	libAssert(_idx < m_paramNum);

	if (IsValuePtr())
	{
		return GetValuePtr()[_idx].GetFloat();
	}
	else if (IsFloat())
	{
		return *(PCast<f32*>(m_param.p) + _idx);
	}
	else
	{
		return SCast<f32>(*(PCast<s32*>(m_param.p) + _idx));
	}
}

///-------------------------------------------------------------------------------------------------
/// 文字列取得
const c8*	CValue::GetString() const
{
	libAssert(IsStr());
	return m_param.s;
}

///-------------------------------------------------------------------------------------------------
/// ポインタ取得
void*	CValue::GetPtr() const
{
	libAssert(IsPtr() | IsValuePtr());
	return m_param.p;
}

///-------------------------------------------------------------------------------------------------
/// CValueポインタ取得
const CValue*	CValue::GetValuePtr() const
{
	libAssert(IsValuePtr());
	return m_param.v;
}



//-------------------------------------------------------------------------------------------------
/// クラスをコンストラクタで取得
CClassName::CClassName(const std::type_info& _type_info, b8 _namespace_error)
{
	m_pBuf = NULL;
	m_pName = _type_info.name();

	static const u32 lib_name_len = SCast<u32>(::strlen(POISON_NAME_STR));

#ifdef _MSC_BUILD	// VCのみ
	m_pName += 6;

	// Poison::の部分があれば外す
	const c8* pPoison = POISON_NAME_STR;
	// 頭がPoison
	if ((*PCast<const u32*>(m_pName)) == (*PCast<const u32*>(pPoison)) &&
		(*PCast<const u16*>(m_pName + 4)) == (*PCast<const u16*>(pPoison + 4)))
	{
		m_pName += lib_name_len + 2;
		PRINT("*****[WARNING] CClassName() Poisonのクラスを自動でクラス名登録しました[%s]\n", m_pName);
	}
	// 他に:があったらNG
	libAssert(!_namespace_error || strstr(m_pName, ":") == NULL);

#else	// それ以外
	// Poisonがないか検索
	const c8* p = ::strstr(m_pName, POISON_NAME_STR);
	if (p != NULL)
	{
		m_pName = p + lib_name_len;
	}

	// 数字がなくなるまで進める
	while ((*m_pName) >= '0' && (*m_pName) <= '9')
	{
		++m_pName;
	}
	// 文字の長さ取得
	size_t len = strlen(m_pName);
	if (m_pName[len - 1] == 'E')
	{
		// 末尾のE文字を削除して文字の長さを計算する
		while (m_pName[len - 1] == 'E')
		{
			--len;
		}
		// コピーする
		c8* pTmp = PCast<c8*>(malloc(len + 1));
		if (!pTmp)
		{
			libAssert(pTmp);
			return;
		}
		memcpy(pTmp, m_pName, len);
		pTmp[len] = 0;

		m_pBuf = pTmp;
		m_pName = pTmp;

		PRINT("*****[WARNING] CClassName() use malloc class name[%s]\n", m_pName);
	}
#endif
}

///-------------------------------------------------------------------------------------------------
// デストラクタ
CClassName::~CClassName()
{
	if (m_pBuf){ free(m_pBuf); m_pBuf = NULL; }
	m_pName = NULL;
}

///-------------------------------------------------------------------------------------------------
/// 定数置き場




POISON_END

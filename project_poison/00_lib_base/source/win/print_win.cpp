﻿
#include "lib_base_def.h"
#include "print.h"

#include <malloc.h>

POISON_BGN


///-------------------------------------------------------------------------------------------------
/// 共通化するけど一旦Win用
u32	ConvMultiByteString(c8* _ret, u32 _maxNum, const c8* _pStr, u32 _srcType, u32 _dstType)
{
	// Convert SJIS -> UTF-16
	const s32 nSizeUTF16 = ::MultiByteToWideChar(_srcType, 0, _pStr, -1, NULL, 0);

	LPWSTR buffUtf16 = PCast<LPWSTR>(alloca(nSizeUTF16 * 2 + 2));
	::MultiByteToWideChar(_srcType, 0, _pStr, -1, buffUtf16, nSizeUTF16 + 1);

	// Convert UTF-16 -> UTF-8
	u32 size = SCast<u32>(::WideCharToMultiByte(_dstType, 0, buffUtf16, nSizeUTF16, NULL, 0, NULL, NULL));
	if (!_ret || size <= 0) {
		return size;
	}
	// バッファ最大数チェック
	if (size >= _maxNum)
	{
		size = _maxNum - 1;
	}
	// 戻り値を優先
	size = SCast<u32>(::WideCharToMultiByte(_dstType, 0, buffUtf16, nSizeUTF16, _ret, size + 1, NULL, NULL));
	_ret[size] = 0;

	return size;
}

///-------------------------------------------------------------------------------------------------
/// SJIS→UTF8
u32	ConvSJIStoUTF8(c8* _ret, u32 _maxNum, const c8* _pStr)
{
	return ConvMultiByteString(_ret, _maxNum, _pStr, CP_ACP, CP_UTF8);
}

///-------------------------------------------------------------------------------------------------
/// UTF8→SJIS
u32	ConvUTF8toSJIS(c8* _ret, u32 _maxNum, const c8* _pStr)
{
	return ConvMultiByteString(_ret, _maxNum, _pStr, CP_UTF8, CP_ACP);
}

///-------------------------------------------------------------------------------------------------
/// SJISプリント
void	PrintSJIS(const c8* _format, ...)
{
#ifndef POISON_RELEASE
	c8 buf[2048];
	{
		va_list vlist;
		va_start(vlist, _format);
		vsprintf_safe(buf, sizeof(buf), _format, vlist);
		va_end(vlist);
	}
	::printf(buf);
	::OutputDebugStringA(buf);
#endif // POISON_RELEASE
}

///-------------------------------------------------------------------------------------------------
/// UTF8プリント
void	PrintUTF8(const c8* _format, ...)
{
#ifndef POISON_RELEASE
	c8 buf[2048];
	{
		c8 tmpFormat[2048];
		{
			va_list vlist;
			va_start(vlist, _format);
			vsprintf_safe(tmpFormat, sizeof(tmpFormat), _format, vlist);
			va_end(vlist);
		}
		// SJIS化
		ConvUTF8toSJIS(buf, sizeof(buf), tmpFormat);
	}
	::printf(buf);
	::OutputDebugStringA(buf);
#endif // POISON_RELEASE
}


POISON_END

﻿//#pragma once
#ifndef	__LIB_BASE_DEF_H__
#define	__LIB_BASE_DEF_H__


#include "types.h"
#include "lib_def.h"

#include "define.h"
#include "print.h"
#include "assert.h"
#include "alert.h"
#include "crc.h"
#include "inline.h"
#include "endian.h"
#include "func.h"
#include "class.h"


#endif	// __LIB_BASE_DEF_H__

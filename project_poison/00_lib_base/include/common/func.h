﻿//#pragma once
#ifndef __FUNC_H__
#define __FUNC_H__



POISON_BGN

//-------------------------------------------------------------------------------------------------
//@brief	ファイルパスからファイル名取得
extern const c8*	GetFileName(const c8* _pPath);
extern const c8*	GetFileName(const c8* _pPath, u32 _fileLen);

//-------------------------------------------------------------------------------------------------
//@brief	ファイルパスから拡張子取得(拡張子がない場合、NULLを返す)
extern const c8*	GetFileExt(const c8* _pPath);
extern const c8*	GetFileExt(const c8* _pPath, u32 _fileLen);

//-------------------------------------------------------------------------------------------------
//@brief	ファイルパスの連結
extern void	GetConnectFilePath(c8* _pRetPath, const c8* _pPath0, const c8* _pPath1, const c8 _s = '/');
extern void	GetConnectFilePath(c8* _pRetPath, const c8* _pPath0, const c8* _pPath1, const size_t _pathLen0, const size_t _pathLen1, const c8 _s = '/');


#ifndef POISON_RELEASE
//-------------------------------------------------------------------------------------------------
//@brief	シンボル情報
struct SYMBOL_INFO
{
	const void*	address;
	c8			name[256];
};

//-------------------------------------------------------------------------------------------------
//@brief	バックトレースの取得
extern u32	GetBackTrace(SYMBOL_INFO _ret[], u32 _retMaxNum, u32 _traceOffset = 0);
#endif // !POISON_RELEASE



POISON_END

#endif	// __FUNC_H__

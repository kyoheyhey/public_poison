﻿//#pragma once
#ifndef __LIB_DEF_H__
#define __LIB_DEF_H__


//-------------------------------------------------------------------------------------------------
// ネームスペース定義
#define POISON		Poison
#define POISON_BGN	namespace POISON{
#define POISON_END	}
#define POISON_NAME_STR "Poison"


//-------------------------------------------------------------------------------------------------
// ライブラリビルド名
#ifdef POISON_RELEASE
#define POISON_CONFIGURATION	"Release"
#elif defined(POISON_DEVELOPMENT)
#define POISON_CONFIGURATION	"Development"
#elif defined(POISON_DEBUG)
#define POISON_CONFIGURATION	"Debug"
#endif


#include <stdio.h>

//-------------------------------------------------------------------------------------------------
// Windows
#if defined(WIN32)		// Windows

#define LIB_SYSTEM_WIN	//< Winシステム使用

#if defined(WIN64)
#define LIB_ARC_64BIT	//< 64bitアーキテクチャ
#else
#define LIB_ARC_32BIT	//< 32bitアーキテクチャ
#endif

#define LIB_ARC_X86		//< x86アーキテクチャ

#define LIB_LITTLE_ENDIAN	//< エンディアンはLITTLE使用

#include <string.h>
#include <assert.h>
#include <stddef.h>
#include <typeinfo>
#include <stdarg.h>
#include <windows.h>
#include <winbase.h>
#include <math.h>
#include <process.h>

// ヘッダーからあまり使われない関数を除く
#define WIN32_LEAN_AND_MEAN

//-------------------------------------------------------------------------------------------------
// メモリリーク発生時にデバッガに出力する内容をわかりやすくする
#if defined(DEBUG) || defined(_DEBUG)
#define _CRTDBG_MAP_ALLOC							//< mallocによるメモリリーク検出でCPPファイル名と行数出力指定
#define NEW	new(_NORMAL_BLOCK, __FILE__, __LINE__)	//< new によるメモリリーク検出でCPPファイル名と行数出力指定
#else
#define NEW	new
#endif

//-------------------------------------------------------------------------------------------------
// ブレーク
#ifndef POISON_RELEASE
#define FUNC_DEBUG_BREAK()	{ ::DebugBreak(); }
#endif // !POISON_RELEASE

//-------------------------------------------------------------------------------------------------
// ソース中の文字列はUTF8に
#pragma execution_character_set("utf-8")

//-------------------------------------------------------------------------------------------------
// アライメント指定
#define ALIGN(_n)		__declspec(align(_n))
// 変数アライメント指定
#define ALIGN_DECL( __var, __n )	ALIGN(__n) __var

// 強制インライン展開
#define FORCE_INLINE	__forceinline

// 最適化なし
#define OPTNONE

// 非推奨
#define DEPRECATED(__mes)

// 関数定義名
#define __PRETTY_FUNCTION__		__FUNCTION__

//-------------------------------------------------------------------------------------------------
// ワーニング除去
// シフト数が負の値であるか、大きすぎます。定義されていない動作です
#pragma warning(disable : 4293)
// structのnew時に引数なしのコンストラクタの()をつける必要がないワーニング
#pragma warning(disable : 4345)
// sprintfなど古い関数を使用している時のワーニング
//#pragma warning(disable : 4996)
// warning C4324: 'クラス名' : __declspec(align()) のために構造体がパッドされました。
#pragma warning(disable : 4324)
// 戻り値がないはエラー
#pragma warning(error : 4715)
// enumの基本型指定（C++11の機能）が非標準というワーニング
#pragma warning(disable : 4480)
// warning C4312: 'reinterpret_cast': 'const int' からより大きいサイズの 'const void *' へ変換します。
#pragma warning(disable : 4312)
// warning C4819: ファイルは、現在のコード ページ (0) で表示できない文字を含んでいます。データの損失を防ぐために、ファイルを Unicode 形式で保存してください。(UTF8で保存しているが文字によって言われる)
#pragma warning(disable : 4819)
// warning C6255: _alloca は、スタック オーバーフロー例外を発生させて失敗を示します。_malloca を使用してください。
#pragma warning(disable : 6255)
// warning C28251 : 'new' に対する整合性のない注釈 : このインスタンスには 注釈なし が含まれます。g : \nk2\program\libprot\program\10_lib_platform\project\predefined c++ types(compiler internal)(20) を参照してください。この組み込み関数の 1 つ目のユーザーに指定された注釈は行 c : \program files(x86)\microsoft visual studio 12.0\vc\include\new(54) にあります。
//#pragma warning(disable : 28251)

//-------------------------------------------------------------------------------------------------
// メモリコピー・セット
#define memcpy16	memcpy
#define memcpy32	memcpy
#define memset16	memset
#define memset32	memset

//-------------------------------------------------------------------------------------------------
// メモリ用スロー
#define MEM_THROW()


// undef、死ねWindows
#undef GetObject
#undef CreateObject
#undef GetClassName
#undef SendMessage
#undef SearchPath
#undef GetCurrentTime
#undef INTERFACE

#endif // defined(WIN32)


//-------------------------------------------------------------------------------------------------
// エンディアン定義
#if !defined(LIB_BIG_ENDIAN) && !defined(LIB_LITTLE_ENDIAN)

// 自動判定してみる
#if defined(__BIG_ENDIAN__)
#define LIB_BIG_ENDIAN			//< エンディアンはBIG使用
#elif defined(__LITTLE_ENDIAN__)
#define LIB_LITTLE_ENDIAN		//< エンディアンはLITTLE使用
#else
// エンディアン未定義
#endif

#endif //!defined(LIB_BIG_ENDIAN) && !defined(LIB_LITTLE_ENDIAN)


#endif	// __LIB_DEF_H__

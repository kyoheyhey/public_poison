﻿//#pragma once
#ifndef __DEFINE_H__
#define __DEFINE_H__



POISON_BGN


//-------------------------------------------------------------------------------------------------
// 定数定義
#define DCM_MIN		(1.1754943508222875079687365372222e-38f)
#define DCM_MAX		(0.999999940395355224609375f)

#define PI			3.1415926535897932384626433832795f	//< π
#define PI_2		6.283185307179586476925286766559f	//< 2π
#define PI_H		1.5707963267948966192313216916398f	//< π/2
#define PI_Q		0.78539816339744830961566084581988f	//< π/4
#define LOG_2		0.69314718055994530941723212145818f

#define INV_PI		0.31830988618379067153776752674503f	//< 1/π
#define INV_PI_2	0.15915494309189533576888376337251f	//< 1/2π
#define INV_PI_H	0.63661977236758134307553505349006f	//< 1/π/2
#define INV_PI_Q	1.2732395447351626861510701069801f	//< 1/π/4
#define INV_LOG_2	1.4426950408889634073599246810019f	//< 1/log2

#define SQRT_2		1.4142135623730950488016887242097f	//< √2
#define SQRT_3		1.7320508075688772935274463415059f	//< √3
#define INV_SQRT_2	0.70710678118654752440084436210485f	//< 1/√2
#define INV_SQRT_3	0.57735026918962576450914878050196f	//< 1/√3

#define EXP			2.7182818284590452353602874713527f	//< exp(1.0)
#define DEG_TO_RAD	0.017453292519943295769f			//< 角度⇒ラジアン
#define RAD_TO_DEG	57.29577951308232087679f			//< ラジアン⇒角度

#define toRadian(degree)	( (degree) * DEG_TO_RAD )	//< デグリー角をラジアンに変換
#define toDegree(radian)	( (radian) * RAD_TO_DEG )	//< ラジアン角をデグリーに変換

#if 0
#ifndef FLT_MAX
#define FLT_MAX		3.402823466e+38f		//< float最大値
#endif // FLT_MAX

#ifndef FLT_MIN
#define FLT_MIN		1.175494351e-38f		//< float最小値
#endif // FLT_MIN
#endif // 0

#ifndef DBL_MAX
#define DBL_MAX		1.7976931348623158e+308	//< double最大値
#endif // DBL_MAX

#ifndef DBL_MIN
#define DBL_MIN		2.2250738585072014e-308	//< double最小値
#endif // DBL_MIN


//-------------------------------------------------------------------------------------------------
/// 配列数を求める
#define ARRAYOF(_list)	( sizeof(_list)/sizeof(*(_list)) )

//-------------------------------------------------------------------------------------------------
/// クラスメンバの先頭からのオフセットを求める
#define OFFSETOF(__class, __member)	(offsetof(__class, __member))
//#define OFFSETOF(__class, __member)	( (u64)( &(RCast<__class*>(NULL)->__member) ) )

//-------------------------------------------------------------------------------------------------
/// クラスのアラメイント取得
#define ALIGNOF( __class )				(AlignOf<__class>())

//-------------------------------------------------------------------------------------------------
/// サイズなどのアライメント
#define ALIGN_VAL( __val, __align )		ALIGN_PTR( __val, __align )

//-------------------------------------------------------------------------------------------------
/// アライメントしたアドレス取得
#define ALIGN_PTR(__val, __align)		(((u64)(__val)+((u64)(__align)-1))&~((u64)(__align)-1))
/// アライメントによるオフセット取得
#define ALIGN_OFFSET(__val, __align)	(u64)(ALIGN_PTR( __val, __align)-(u64)(__val) )

//-------------------------------------------------------------------------------------------------
/// クラスサイズをコンパイル時にエラーで出す
#define PRINT_SIZEOF(__class)				CPrintSizeof<__class, sizeof(__class)>()

//-------------------------------------------------------------------------------------------------
/// クラスメンバの先頭からのオフセット値をコンパイル時にエラーで出す
#define PRINT_OFFSETOF(__class, __member)	CPrintSizeof<__class, OFFSETOF(__class, __member)> __member;	//PrintSizeof(CPrintSizeof<__TYPE, OFFSETOF(__TYPE, __member)>)


//-------------------------------------------------------------------------------------------------
/// ビットフラグのセット
#define SET_BIT_FLAG( __val, __bitshift, __flag )	if( __flag ){ __val |= (1<<(__bitshift)); }else{ __val &= ~(1<<(__bitshift)); }

//-------------------------------------------------------------------------------------------------
/// セーフリリース
#ifndef SAFE_RELEASE
#define SAFE_RELEASE( __p )	{ if(__p) { (__p)->Release(); (__p)=NULL; } }
#endif // SAFE_RELEASE



POISON_END


#endif	// __DEFINE_H__

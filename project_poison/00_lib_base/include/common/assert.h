﻿//#pragma once
#ifndef __ASSERT_H__
#define __ASSERT_H__



//-------------------------------------------------------------------------------------------------
// 静的解析有効
#ifdef __clang_analyzer__
// 値を返さない関数(静的解析時のアサート通知)
void __analysis_noreturn()		__attribute__((noreturn));
#define	ANALYSIS_NORETURN()		{ __analysis_noreturn(); }
#else
#define	ANALYSIS_NORETURN()
#endif

//-------------------------------------------------------------------------------------------------
#ifdef LIB_SYSTEM_WIN
// コード解析用、式がtrueであることを保証する
#define ANALYSIS_ASSUME(__exp)	__analysis_assume(__exp)
#else
#define ANALYSIS_ASSUME(__exp)
#endif


POISON_BGN

//-------------------------------------------------------------------------------------------------
#ifndef POISON_RELEASE

// デバッグ関数宣言(直接呼び出し禁止)
b8		IsEnableAssertDebugBreak();
void	SetEnableAssertDebugBreak(const b8 _flag);

// アサート文章
#ifndef ASSERT_STRING
#define	ASSERT_STRING( __exp )	"Assert( " #__exp " )\n  Func= %s\n  File= %s\n  Line= %d\n", __PRETTY_FUNCTION__, __FILE__, __LINE__
#endif // !ASSERT_STRING

// アサート文章
#ifndef ASSERT_STRING_MSG
#define	ASSERT_STRING_MSG( __exp, __msg )	"Assert( " #__exp " )\n  Msg= %s\n  Func= %s\n  File= %s\n  Line= %d\n", __msg, __PRETTY_FUNCTION__, __FILE__, __LINE__
#endif // !ASSERT_STRING_MSG

// アサート出力
#define	DUMP_ASSERT( __exp )		PRINT_ASSERT(  ASSERT_STRING(__exp)  )
// アサート出力(ライブラリ用)
#define	LIB_DUMP_ASSERT( __exp )	PrintUTF8(  ASSERT_STRING(__exp)  )

// アサート出力
#define	DUMP_ASSERT_MSG( __exp, __msg )		PRINT_ASSERT(  ASSERT_STRING_MSG(__exp, __msg)  )
// アサート出力(ライブラリ用)
#define	LIB_DUMP_ASSERT_MSG( __exp, __msg )	PrintUTF8(  ASSERT_STRING_MSG(__exp, __msg)  )

// デバッグブレーク有効か
#define	IS_EANBLE_ASSERT_DEBUG_BREAK()			(POISON::IsEnableAssertDebugBreak())
// デバッグブレーク有効無効設定
#define	SET_ENABLE_ASSERT_DEBUG_BREAK( __flag )	(POISON::SetEnableAssertDebugBreak(__flag))


//-------------------------------------------------------------------------------------------------
// スコープ内のアサート向けの状態変数名
#define	ASSERT_SCOPE_STATE_NAME			s_isEnableAssertScopeState
// スコープ内のアサート向けの状態変数宣言
#define	ASSERT_SCOPE_STATE_DECL()		static u8 ASSERT_SCOPE_STATE_NAME = (IS_EANBLE_ASSERT_DEBUG_BREAK() ? 0 : 1)
// スコープ内のアサート向けの状態変数が有効かどうか
#define	IS_EANBLE_ASSERT_SCOPE_STATE()	(ASSERT_SCOPE_STATE_NAME == 0)


//-------------------------------------------------------------------------------------------------
// デバッグブレーク
#define	ASSERT_DEBUG_BREAK()				{ if( IS_EANBLE_ASSERT_DEBUG_BREAK() ){ FUNC_DEBUG_BREAK(); } ANALYSIS_NORETURN(); }
// スコープフラグチェックのデバッグブレーク
#define	ASSERT_DEBUG_BREAK_SCOPE_STATE()	{ if( IS_EANBLE_ASSERT_DEBUG_BREAK() && IS_EANBLE_ASSERT_SCOPE_STATE() ){ FUNC_DEBUG_BREAK(); } ANALYSIS_NORETURN(); }


//-------------------------------------------------------------------------------------------------
// アサートべース
#define	AssertBase(__exp)		{ if( !(__exp) ){ ASSERT_SCOPE_STATE_DECL(); DUMP_ASSERT( __exp ); ASSERT_DEBUG_BREAK_SCOPE_STATE(); } ANALYSIS_ASSUME(__exp); }
// ライブラリアサートベース
#define	libAssertBase(__exp)	{ if( !(__exp) ){ ASSERT_SCOPE_STATE_DECL(); LIB_DUMP_ASSERT( __exp ); ASSERT_DEBUG_BREAK_SCOPE_STATE(); } ANALYSIS_ASSUME(__exp); }


//-------------------------------------------------------------------------------------------------
// アサートメッセージ
#define	AssertBaseMsg(__exp, __msg)		{ if( !(__exp) ){ ASSERT_SCOPE_STATE_DECL(); DUMP_ASSERT_MSG( __exp, __msg ); ASSERT_DEBUG_BREAK_SCOPE_STATE(); } ANALYSIS_ASSUME(__exp); }
// ライブラリアサートベース
#define	libAssertBaseMsg(__exp, __msg)	{ if( !(__exp) ){ ASSERT_SCOPE_STATE_DECL(); LIB_DUMP_ASSERT_MSG( __exp, __msg ); ASSERT_DEBUG_BREAK_SCOPE_STATE(); } ANALYSIS_ASSUME(__exp); }


//-------------------------------------------------------------------------------------------------
// 通常アサート(ゲーム側実装)
#define	Assert(__exp)	AssertBase(__exp)
// アサートのようなアラート
//#define	AssertAlert( __exp )	{ if(!(__exp)) { ASSERT_SCOPE_STATE_DECL(); libAlertDirectState( &ASSERT_SCOPE_STATE_NAME, "Assert( " #__exp ")" ); ANALYSIS_NORETURN(); } ANALYSIS_ASSUME(__exp); }
// アサートのようなワーニング
//#define	AssertWarning( __exp )	{ if(!(__exp)) { Warning( "DebugAssert( " #__exp ")" ); ANALYSIS_NORETURN(); } ANALYSIS_ASSUME(__exp); }

// アサートのようなアラートメッセージ
//#define	AssertAlertMsg( __exp, __msg )		{ if(!(__exp)) { ASSERT_SCOPE_STATE_DECL(); libAlertDirectState( &ASSERT_SCOPE_STATE_NAME, __msg ); ANALYSIS_NORETURN(); } ANALYSIS_ASSUME(__exp); }
// アサートのようなワーニングメッセージ
//#define	AssertWarningMsg( __exp, __msg )	{ if(!(__exp)) { Warning( __msg ); ANALYSIS_NORETURN(); } ANALYSIS_ASSUME(__exp); }


#ifdef POISON_DEBUG
// ライブラリ用アサート、デバッグ時のアサート、デバッグ以外はアラートに変更
#define	libAssert(__exp)		libAssertBase(__exp)
// ライブラリ用アサート、デバッグ時のアサート、デバッグ以外はワーニングに変更
#define	libDebugAssert(__exp)	libAssertBase(__exp)
// ライブラリ用アサートメッセージ、デバッグ時のアサート、デバッグ以外はアラートに変更
#define	libAssertMsg(__exp, __msg)		libAssertBaseMsg(__exp, __msg)
// ライブラリ用アサートメッセージ、デバッグ時のアサート、デバッグ以外はワーニングに変更
#define	libDebugAssertMsg(__exp, __msg)	libAssertBaseMsg(__exp, __msg)
#else // POISON_DEBUG
// ライブラリ用アサート、デバッグ時のアサート、デバッグ以外はアラートに変更
#define	libAssert(__exp)		AssertAlert(__exp)
// ライブラリ用アサート、デバッグ時のアサート、デバッグ以外はワーニングに変更
#define	libDebugAssert(__exp)	AssertWarning(__exp)
// ライブラリ用アサートメッセージ、デバッグ時のアサート、デバッグ以外はアラートに変更
#define	libAssertMsg(__exp, __msg)		AssertAlertMsg(__exp, __msg)
// ライブラリ用アサートメッセージ、デバッグ時のアサート、デバッグ以外はワーニングに変更
#define	libDebugAssertMsg(__exp, __msg)	AssertWarningMsg(__exp, __msg)
#endif // !POISON_DEBUG

//-------------------------------------------------------------------------------------------------
template <b8 x> struct STATIC_ASSERTION_FAILURE;
template <> struct STATIC_ASSERTION_FAILURE<true> {};

//-------------------------------------------------------------------------------------------------
// Staticアサート
#define StaticAssert( __exp )	POISON::STATIC_ASSERTION_FAILURE< (b8)(__exp) >()

#else   // !POISON_RELEASE

#define ASSERT_STRING( __exp )
#define DUMP_ASSERT( __exp )
#define LIB_DUMP_ASSERT( __exp )
#define DUMP_ASSERT_MSG( __exp, __msg )
#define LIB_DUMP_ASSERT_MSG( __exp, __msg )

#define IS_EANBLE_ASSERT_DEBUG_BREAK()
#define SET_ENABLE_ASSERT_DEBUG_BREAK(__flag)
#define ASSERT_SCOPE_STATE_DECL()
#define ASSERT_DEBUG_BREAK()
#define ASSERT_DEBUG_BREAK_SCOPE_STATE()

#define AssertBase(__exp)
#define libAssertBase(__exp)
#define AssertBaseMsg(__exp, __msg)
#define libAssertBaseMsg(__exp, __msg)

#define	Assert( __exp )

#define AssertAlert( __exp )   
#define AssertWarning( __exp )
#define AssertAlertMsg( __exp, __msg ) 
#define AssertWarningMsg( __exp, __msg )

#define libAssert(__exp)
#define libDebugAssert(__exp)
#define libAssertMsg(__exp, __msg)
#define libDebugAssertMsg(__exp, __msg)
#define StaticAssert( __exp )

#endif // POISON_RELEASE


POISON_END


#endif	// __ASSERT_H__

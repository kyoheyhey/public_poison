﻿//#pragma once
#ifndef __ALERT_H__
#define __ALERT_H__



POISON_BGN


///*************************************************************************************************
//@brief	アラートユーティリティ
///*************************************************************************************************
class	CAlertUtility
{
private:
	/// コンストラクタ
	CAlertUtility(){}

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	アラート用の情報構造体
	struct INFO
	{
		//@brief	コンストラクタ
		INFO(const u32 _attr, const c8* _func, const c8* _file, const u32 _line, const c8* _message = NULL, u8* _pState = NULL)
			: pState(_pState)
			, pMessage(_message)
			, pFuncName(_func)
			, pFileName(_file)
			, fileLine(_line)
			, attr(_attr)
		{}

	public:
		u8*					pState;		//< 表示ステート(ステートの書き換えも行う、0がデフォルト(有効))
		mutable const c8*	pMessage;	//< 表示メッセージ
		mutable const c8*	pFuncName;	//< 関数名
		mutable const c8*	pFileName;	//< ファイル名
		u32					fileLine;	//< ファイル行数
		u32					attr;		//< 属性
	};

	//-------------------------------------------------------------------------------------------------
	//@brief	アラート表示型
	typedef void(*FUNC_ALERT)(const INFO& _info);

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	アラート表示、画面が停止
	static inline void	ShowAlert(const INFO& _info)
	{
		(*s_funcAlert)(_info);
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	ワーニング表示、画面は停止しない
	static inline void	ShowWarning(const INFO& _info)
	{
		(*s_funcWarning)(_info);
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	文字列書式指定のアラート表示
	static void	ShowAlertArgs(const u32 _attr, const c8* _func, const c8* _file, const u32 _line, u8* _ptr_state, const c8* _format, ...);

	//-------------------------------------------------------------------------------------------------
	//@brief	文字列書式指定のワーニング表示
	static void	ShowWarningArgs(const u32 _attr, const c8* _func, const c8* _file, const u32 _line, u8* _ptr_state, const c8* _format, ...);

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	アラートの関数ポインタ設定
	static FUNC_ALERT	SetFuncAlert(FUNC_ALERT _func) { FUNC_ALERT ret = s_funcAlert; s_funcAlert = _func; return ret; }

	//-------------------------------------------------------------------------------------------------
	//@brief	ワーニングの関数ポインタ設定
	static FUNC_ALERT	SetFuncWarning(FUNC_ALERT _func) { FUNC_ALERT ret = s_funcWarning; s_funcWarning = _func; return ret; }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	プリントのみ行うアラート、デフォルトで指定されている
	static void	PrintAlert(const INFO& _info);

	//-------------------------------------------------------------------------------------------------
	//@brief	プリントのみ行うワーニング、デフォルトで指定されている
	static void	PrintWarning(const INFO& _info);

private:
	static FUNC_ALERT	s_funcAlert;	//< アラート表示関数
	static FUNC_ALERT	s_funcWarning;	//< ワーニング表示関数
};



#ifndef POISON_RELEASE

//-------------------------------------------------------------------------------------------------
//@brief	アラートの直接呼び出し
#define libAlertDirect( ... )					libAlertDirectAttr( POISON::libPrintAttrError, __VA_ARGS__ )
//@brief	アラートの直接呼び出し(属性指定)
#define libAlertDirectAttr( __attr, ... )		libAlertDirectAttrState( _attr, NULL, __VA_ARGS__ )
//@brief	ステート条件付きアラートの直接呼び出し
#define libAlertDirectState( __ptr_state, ... )	libAlertDirectAttrState( POISON::libPrintAttrError, __ptr_state, __VA_ARGS__ )
//@brief	ステート条件付きアラートの直接呼び出し(属性指定)
#define libAlertDirectAttrState( __attr, __ptr_state, ... )	POISON::CAlertUtility::ShowAlertArgs( __attr, __PRETTY_FUNCTION__, __FILE__, __LINE__, __ptr_state, __VA_ARGS__ )

//-------------------------------------------------------------------------------------------------
//@brief	アラートベース(内部でステート変数宣言あり)
#define libAlertBase( ... )				libAlertBaseAttr( POISON::libPrintAttrError, __VA_ARGS__ )
//@brief	アラートベース(内部でステート変数宣言あり、属性指定)
#define libAlertBaseAttr( __attr, ... )	{ ASSERT_SCOPE_STATE_DECL(); libAlertDirectAttrState(__attr, &ASSERT_SCOPE_STATE_NAME, __VA_ARGS__); }

//-------------------------------------------------------------------------------------------------
//@brief	ライブラリ用アラート呼び出し
#define libAlert( ... )				libAlertAttr( POISON::libPrintAttrError, __VA_ARGS__ )
//@brief	アラート呼び出し(属性付き)
#define libAlertAttr( __attr, ... )	libAlertBaseAttr(__attr, __VA_ARGS__)

//-------------------------------------------------------------------------------------------------
//@brief	ライブラリ用ワーニング呼び出し
#define libWarning( ... )				libWarningAttr( POISON::libPrintAttrWarning, __VA_ARGS__ )
//@brief	ライブラリ用ワーニング呼び出し(属性付き)
#define libWarningAttr( __attr, ... )	POISON::CAlertUtility::ShowWarningArgs( __attr, __PRETTY_FUNCTION__, __FILE__, __LINE__, NULL, __VA_ARGS__ )

//-------------------------------------------------------------------------------------------------
//@brief	アラート呼び出し
#define Alert		libAlert
//@brief	アラート呼び出し(属性付き) 
#define AlertAttr	libAlertAttr

//-------------------------------------------------------------------------------------------------
//@brief	ワーニング呼び出し
#define Warning		libWarning
//@brief	ワーニング呼び出し(属性付き)
#define WarningAttr	libWarningAttr

#else

// リリースは無効
#define libAlertDirect( ... )
#define libAlertDirectAttr( __attr, ... )
#define libAlertDirectState( __ptr_state, ... )
#define libAlertDirectAttrState( __attr, __ptr_state, ... )
#define libAlertBase( ... )
#define libAlertBaseAttr( __attr, ... )

#define libAlert( ... )
#define libAlertAttr( __attr, ... )
#define libWarning( ... )
#define libWarningAttr( __attr, ... )

#define Alert( ... )
#define AlertAttr( __attr, ... )
#define Warning( ... )
#define WarningAttr( __attr, ... )

#endif	// POISON_RELEASE


POISON_END

#endif	// __ALERT_H__

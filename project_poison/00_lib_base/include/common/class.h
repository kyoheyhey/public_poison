﻿//#pragma once
#ifndef	__CLASS_H__
#define	__CLASS_H__



POISON_BGN

//*************************************************************************************************
//@brief	コピー禁止基底クラス
//*************************************************************************************************
class CDontCopy
{
protected:
	CDontCopy() {}
	virtual ~CDontCopy() {}

	CDontCopy(const CDontCopy &) = delete;
	CDontCopy& operator=(const CDontCopy &) = delete;
	CDontCopy(CDontCopy &&) = delete;
	CDontCopy& operator=(CDontCopy &&) = delete;
};


//*************************************************************************************************
//@brief	レクトクラス
//*************************************************************************************************
class CRect
{
public:
	s32	posX;
	s32	posY;
	u32 width;
	u32 height;

	CRect() { posX = posY = width = height = 0; }
	CRect(s32 _posX, s32 _posY, u32 _width, u32 _height)
	{
		posX = _posX; posY = _posY; width = _width; height = _height;
	}

	//@brief	一致判定
	inline b8	operator==(const CRect& _rect) const { return (posX == _rect.posX && posY == _rect.posY && width == _rect.width && height == _rect.height); }
	//@brief	不一致判定
	inline b8	operator!=(const CRect& _rect) const { return (posX != _rect.posX || posY != _rect.posY || width != _rect.width || height != _rect.height); }
};


//*************************************************************************************************
//@brief	汎用値クラス
//*************************************************************************************************
class CValue
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	汎用パラメータタイプ
	enum VALUE_TYPE
	{
		TYPE_BOOL = 0,	//< 論理型
		TYPE_INT,		//< 整数型
		TYPE_UINT,		//< 整数型（符号なし）
		TYPE_FLOAT,		//< 小数型
		TYPE_STR,		//< 文字列型
		TYPE_PTR,		//< ポインタ型
		TYPE_VALUE_PTR,	//< Valueポインタ型

		TYPE_NUM		//< 型タイプ数
	};

private:
	//-------------------------------------------------------------------------------------------------
	//@brief	汎用パラメータ
	union VALUE_PARAM
	{
		b8				b;
		s32				i;
		u32				u;
		f32				f;
		void*			p;
		const c8*		s;
		const CValue*	v;
	};

private:
	VALUE_PARAM		m_param;
	VALUE_TYPE		m_type;
	u32				m_paramNum;


public:
	CValue() { m_param.p = NULL; m_type = TYPE_NUM; m_paramNum = 1; }
	CValue(s32 _value) { SetValue(_value); }
	CValue(f32 _value) { SetValue(_value); }
	CValue(const c8* _value) { SetValue(_value); }
	CValue(void* _value) { SetValue(_value); }
	CValue(const CValue& value) { m_param = value.m_param; m_type = value.m_type; m_paramNum = value.m_paramNum; }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	判定
	inline b8	IsBool() const { return (this->m_type == TYPE_BOOL); }				//< 論理型
	inline b8	IsInt() const { return (this->m_type == TYPE_INT); }				//< 整数型
	inline b8	IsUInt() const { return (this->m_type == TYPE_UINT); }				//< 整数型
	inline b8	IsFloat() const { return (this->m_type == TYPE_FLOAT); }			//< 浮動小数型か
	inline b8	IsStr() const { return (this->m_type == TYPE_STR); }				//< 文字列型
	inline b8	IsPtr() const { return (this->m_type == TYPE_PTR); }				//< ポインタ型
	inline b8	IsValuePtr() const { return (this->m_type == TYPE_VALUE_PTR); }		//< Valueポインタ型かどうか
	inline b8	IsArray() const { return (this->m_paramNum > 1); }					//< 配列かどうか

	//-------------------------------------------------------------------------------------------------
	//@brief	他のバッファを参照しているか
	inline b8	IsRefBuffer() const { return (IsPtr() | IsValuePtr() | IsArray()); }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	値の設定
	void	SetValue(b8 _value) { m_param.b = _value; m_type = TYPE_INT; m_paramNum = 1; }						//< 論理型
	void	SetValue(s32 _value) { m_param.i = _value; m_type = TYPE_INT; m_paramNum = 1; }						//< 整数型
	void	SetValue(u32 _value) { m_param.u = _value; m_type = TYPE_UINT; m_paramNum = 1; }					//< 整数型
	void	SetValue(f32 _value) { m_param.f = _value; m_type = TYPE_FLOAT; m_paramNum = 1; }					//< 小数型
	void	SetValue(const c8* _value){ m_param.s = _value; m_type = TYPE_STR; m_paramNum = 1; }				//< 文字列型
	void	SetValue(void* _value){ m_param.p = _value; m_type = TYPE_PTR; m_paramNum = 1; }					//< 汎用ポインタ型
	void	SetValue(const CValue* _value){ m_param.v = _value; m_type = TYPE_VALUE_PTR; m_paramNum = 1; }		//< Valueポインタ型
	void	SetValue(s32* _value_array, u32 _num) { m_param.p = _value_array; m_type = TYPE_INT; m_paramNum = _num; }
	void	SetValue(f32* _value_array, u32 _num) { m_param.p = _value_array; m_type = TYPE_FLOAT; m_paramNum = _num; }
	void	SetValue(void* _value_array, u32 _num) { m_param.p = _value_array; m_type = TYPE_PTR; m_paramNum = _num; }
	void	SetValue(const CValue* _value_array, u32 _num) { m_param.v = _value_array; m_type = TYPE_VALUE_PTR; m_paramNum = _num; }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	値の取得
	b8				GetBool(u32 _idx = 0) const;	//< 論理型取得
	s32				GetInt(u32 _idx = 0) const;		//< 整数型取得
	u32				GetUInt(u32 _idx = 0) const;	//< 整数型取得
	f32				GetFloat(u32 _idx = 0) const;	//< 小数型取得
	const c8*		GetString() const;				//< 文字列型取得
	void*			GetPtr() const;					//< ポインタ型取得
	const CValue*	GetValuePtr() const;			//< Valueポインタ型取得

	u32				GetParamNum() const { return m_paramNum; }	///< パラメータ数取得
};



//*************************************************************************************************
//@brief	テンプレート版汎用ハンドルクラス
//*************************************************************************************************
template<typename BASE, u32 INVALID_VALUE = 0u>
class CHandleUtil32
{
public:
	static const u32	INVALID = INVALID_VALUE;

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	32bitコンストラクタ
	inline	CHandleUtil32(const u32 _handle = INVALID_VALUE) : handle32(_handle){}
	//-------------------------------------------------------------------------------------------------
	//@brief	16bitコンストラクタ
	inline	CHandleUtil32(const u16 _handle1, const u16 _handle2) { handle16[0] = _handle1; handle16[1] = _handle2; }
	//-------------------------------------------------------------------------------------------------
	//@brief	8bitコンストラクタ
	inline	CHandleUtil32(const u8 _handle1, const u8 _handle2, const u8 _handle3, const u8 _handle4) { handle8[0] = _handle1; handle8[1] = _handle2; handle8[2] = _handle3; handle8[3] = _handle4; }
	//-------------------------------------------------------------------------------------------------
	//@brief	8bitコンストラクタ
	inline	CHandleUtil32(const u8* _pHandle) : handle32(*PCast<const u32*>(_pHandle)) {}

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	ハンドル一致
	inline	b8		operator==(const BASE& _handle) const { return (handle32 == _handle.handle32); }
	//-------------------------------------------------------------------------------------------------
	//@brief	ハンドル不一致
	inline	b8		operator!=(const BASE& _handle) const { return (handle32 != _handle.handle32); }

	//-------------------------------------------------------------------------------------------------
	//@brief	ハンドル代入
	inline	BASE&	operator=(const BASE& _handle) { handle32 = _handle.handle32; return (*this); }
	//-------------------------------------------------------------------------------------------------
	//@brief	ハンドル代入
	inline	BASE&	operator=(const u32 _handle) { handle32 = _handle; return (*this); }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	ハンドル32bit取得
	inline	u32		GetHandle32() const	{ return handle32; }
	//-------------------------------------------------------------------------------------------------
	//@brief	ハンドル16bit取得
	inline	u16		GetHandle16(const u32 _index) const { return handle16[_index]; }
	//-------------------------------------------------------------------------------------------------
	//@brief	ハンドル8bit取得
	inline	u8		GetHandle8(const u32 _index) const { return handle8[_index]; }

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	有効ハンドルかどうか
	inline	b8		IsValid() const { return (handle32 != INVALID); }
	//-------------------------------------------------------------------------------------------------
	//@brief	無効ハンドルかどうか
	inline	b8		IsInvalid() const { return (handle32 == INVALID); }

	//-------------------------------------------------------------------------------------------------
	//@brief	ハンドルを無効にする
	inline	void	SetInvalid() { handle32 = INVALID; }

	//-------------------------------------------------------------------------------------------------
	//@brief	無効ハンドル取得
	static	inline	BASE	GetInvalid() { return INVALID; }
	//@brief	無効ハンドルの参照を取得
	static	inline	BASE&	GetRefInvalid() { static BASE invalid(INVALID_VALUE); return invalid; }

private:
	union
	{
		u32	handle32;
		u16 handle16[2];
		u8	handle8[4];
	};
};

//*************************************************************************************************
//@brief	汎用ハンドルクラス
//*************************************************************************************************
class CHandle : public CHandleUtil32<CHandle>
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	32bitコンストラクタ
	inline	CHandle(const u32 _handle = INVALID) : CHandleUtil32<CHandle>(_handle) {}
	//-------------------------------------------------------------------------------------------------
	//@brief	16bitコンストラクタ
	inline	CHandle(const u16 _handle1, const u16 _handle2) : CHandleUtil32<CHandle>(_handle1, _handle2) {}
	//-------------------------------------------------------------------------------------------------
	//@brief	8bitコンストラクタ
	inline	CHandle(const u8 _handle1, const u8 _handle2, const u8 _handle3, const u8 _handle4) : CHandleUtil32<CHandle>(_handle1, _handle2, _handle3, _handle4) {}
	//-------------------------------------------------------------------------------------------------
	//@brief	8bitコンストラクタ
	inline	CHandle(const u8* _pHandle) : CHandleUtil32<CHandle>(_pHandle) {}
};

POISON_END

#endif	// __CLASS_H__

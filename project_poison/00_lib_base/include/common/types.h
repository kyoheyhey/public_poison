﻿//#pragma once
#ifndef __TYPES_H__
#define __TYPES_H__


//-------------------------------------------------------------------------------------------------
// 変数定義
typedef bool				b8;		//< 条件判定用1byte変数型

typedef char				c8;		//< 文字列用1byte変数型
typedef wchar_t				c16;	//< 文字列用2byte変数型
typedef unsigned int		c32;	//< 文字列用4byte変数型

typedef unsigned char		u8;		//< 符号無し1byte変数型
typedef unsigned short		u16;	//< 符号無し2byte変数型
typedef unsigned int		u32;	//< 符号無し4byte変数型
typedef unsigned long long	u64;	//< 符号無し8byte変数型

typedef char				s8;		//< 符号付き1byte変数型
typedef short				s16;	//< 符号付き2byte変数型
typedef int					s32;	//< 符号付き4byte変数型
typedef long long			s64;	//< 符号付き8byte変数型

typedef float				f32;	//< 浮動小数4byte変数型
typedef double				f64;	//< 浮動小数8byte変数型

typedef s16					fx16;	//< 固定小数向け2byte変数型
typedef s32					fx32;	//< 固定小数向け4byte変数型
typedef s64					fx64;	//< 固定小数向け8byte変数型


#endif	// __TYPES_H__

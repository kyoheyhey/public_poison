﻿//#pragma once
#ifndef __INLINE_INL__
#define __INLINE_INL__


#include <type_traits>


POISON_BGN


///-------------------------------------------------------------------------------------------------
/// クラスのアライメントを求める関数
template <class T>
u32	AlignOf()
{
	struct _TMP { u8 a; T b; };
	return OFFSETOF(_TMP, b);
};

///-------------------------------------------------------------------------------------------------
/// エラー時のsizeof出力用クラス宣言(エラーを出すため実態は定義しない)
template <class T, u32 _NUM>
class CPrintSizeof;

///-------------------------------------------------------------------------------------------------
/// ポインタキャスト
template <class T>
inline T	_pointer_cast(void* _p)
{
	return static_cast<T>(_p);
}
///-------------------------------------------------------------------------------------------------
/// constポインタキャスト
template <class T>
inline T	_pointer_cast(const void* _p)
{
	return static_cast<T>(_p);
}

///-------------------------------------------------------------------------------------------------
/// カスタムstatic_castキャスト
template <typename T1, typename T2>
inline T1		_static_cast(T2* _t)
{
	return static_cast<T1>(_t);
}
///-------------------------------------------------------------------------------------------------
/// カスタムconst static_castキャスト
template <typename T1, typename T2>
inline const T1	_static_cast(const T2* _t)
{
	return static_cast<T1>(_t);
}

///-------------------------------------------------------------------------------------------------
/// static_down_castキャスト
/// リリースモードでは静的なダウンキャスト、それ以外では動的なダウンキャスト
template <typename T1, typename T2>
inline T1		_static_down_cast(T2* _t)
{
#ifdef POISON_RELEASE
	//return SCast<T1>(_t);
	return DCast<T1>(_t);
#else
	T1 ret = _t ? DCast<T1>(_t) : NULL;
	if (_t) { libAssert(ret); }
	return ret;
#endif // POISON_RELEASE
}
///-------------------------------------------------------------------------------------------------
/// const static_down_castキャスト
/// リリースモードでは静的なダウンキャスト、それ以外では動的なダウンキャスト
template <typename T1, typename T2>
inline const T1	_static_down_cast(const T2* _t)
{
#ifdef POISON_RELEASE
	//return SCast<T1>(_val);
	return DCast<T1>(_val);
#else
	T1 ret = _t ? DCast<T1>(_t) : NULL;
	if (_t) { libAssert(ret); }
	return ret;
#endif // POISON_RELEASE
}


///-------------------------------------------------------------------------------------------------
/// 符号なしの最小値最大値
template <typename T>
const s64 TypeMinMax<T>::minValue = 0;
template <typename T>
const u64 TypeMinMax<T>::maxValue = static_cast<u64>(static_cast<T>(~static_cast<T>(0)));

template <>
struct TypeMinMax<s8>
{
	static const s64 minValue;
	static const u64 maxValue;
};
template <>
struct TypeMinMax<s16>
{
	static const s64 minValue;
	static const u64 maxValue;
};
template <>
struct TypeMinMax<s32>
{
	static const s64 minValue;
	static const u64 maxValue;
};
template <>
struct TypeMinMax<s64>
{
	static const s64 minValue;
	static const u64 maxValue;
};
template <>
struct TypeMinMax<f32>
{
	static const s64 minValue;
	static const u64 maxValue;
};
template <>
struct TypeMinMax<f64>
{
	static const s64 minValue;
	static const u64 maxValue;
};


///-------------------------------------------------------------------------------------------------
/// 範囲の最大値最小値判定
template <typename T1, typename T2>
inline void	CheckMinMax(const T2& _val)
{
	// 絶対値より大きければNG
	libDebugAssert(_val < 0 || static_cast<u64>(_val) <= TypeMinMax<T1>::maxValue);
	// マイナスもはみ出てる
	libDebugAssert(_val >= 0 || static_cast<s64>(_val) >= TypeMinMax<T1>::minValue);
}


///-------------------------------------------------------------------------------------------------
/// enum 変換チェック（enum → 整数）
template <typename T1, typename T2, b8 ENABLE>
class CheckEnumCastableToIntegral
{
public:
	inline void operator()(const T2& _val) {};
};
template <typename T1, typename T2>
class CheckEnumCastableToIntegral<T1, T2, true>
{
public:
	inline void operator()(const T2& _val)
	{
		typedef typename std::underlying_type<T2>::type underlyType;
		CheckMinMax<T1, underlyType>(static_cast<underlyType>(_val));
	}
};
template <typename T1, typename T2>
class CheckEnumCastableToIntegral<T1, T2, false>
{
public:
	inline void operator()(const T2& _val) {};
};

///-------------------------------------------------------------------------------------------------
/// enum 変換チェック（整数 → enum）
template <typename T1, typename T2, b8 ENABLE>
class CheckEnumCastableToEnum
{
public:
	inline void operator()(const T2& _val) {};
};
template <typename T1, typename T2>
class CheckEnumCastableToEnum<T1, T2, true>
{
public:
	inline void operator()(const T2& _val)
	{
		typedef typename std::underlying_type<T1>::type underlyType;
		CheckMinMax<underlyType, T2>(_val);
	}
};
template <typename T1, typename T2>
class CheckEnumCastableToEnum<T1, T2, false>
{
public:
	inline void operator()(const T2& _val) {}
};

///-------------------------------------------------------------------------------------------------
/// enum 変換チェック
template <typename T1, typename T2, b8 IS_ENUM>
class CheckEnumOrIntegralCastable
{
public:
	inline void operator()(const T2& _val) {};
};
template <typename T1, typename T2>
class CheckEnumOrIntegralCastable<T1, T2, true>
{
public:
	inline void operator()(const T2& _val)
	{
		CheckEnumCastableToIntegral<T1, T2, std::is_integral<T1>::value && std::is_enum<T2>::value>()(_val);
		CheckEnumCastableToEnum<T1, T2, std::is_enum<T1>::value && std::is_integral<T2>::value>()(_val);
	}
};
template <typename T1, typename T2>
class CheckEnumOrIntegralCastable<T1, T2, false>
{
public:
	inline void operator()(const T2& _val)
	{
		CheckMinMax<T1, T2>(_val);
	}
};

///-------------------------------------------------------------------------------------------------
/// カスタムstatic_castキャスト
template <typename T1, typename T2>
inline const T1	_static_cast(const T2& _t)
{
	CheckEnumOrIntegralCastable<T1, T2, std::is_enum<T1>::value || std::is_enum<T2>::value>()(_t);
	return static_cast<T1>(_t);
}



///-------------------------------------------------------------------------------------------------
/// sprintf_safe
inline u32	sprintf_safe(c8* _ret, size_t _size, const c8* _format, ...)
{
	if (_size <= 0){ return 0; }
	va_list vlist;
	va_start(vlist, _format);
	u32 ret = vsprintf_safe(_ret, _size, _format, vlist);
	va_end(vlist);
	return ret;
}

///-------------------------------------------------------------------------------------------------
/// vsprintf_safe
inline u32	vsprintf_safe(c8* _ret, size_t _size, const c8* _format, va_list _arg_list)
{
	if (_size <= 0) { return 0; }
	s32 n = vsnprintf_s(_ret, _size, _TRUNCATE, _format, _arg_list);
	if (n < 0 || n >= _size)
	{
		_ret[_size - 1] = 0;
		return static_cast<u32>(_size);
	}
	return static_cast<u32>(n);
}

///-------------------------------------------------------------------------------------------------
/// strcpy_safe
inline u32	strcpy_safe(c8* _ret, size_t _size, const c8* _src)
{
	//libAssert(_size> 0);
	c8* ret = _ret;
	const c8* fromat = _src;
	while (ret < _ret + _size - 1 && (*fromat) != '\0')
	{
		(*ret++) = (*fromat++);
	}
	(*ret) = 0;
	return static_cast<u32>(ret - _ret);
}

///-------------------------------------------------------------------------------------------------
/// sprintf_safe
template <u32 SIZE>
inline u32	sprintf_safe(c8(&_ret)[SIZE], const c8* _format, ...)
{
	va_list vlist;
	va_start(vlist, _format);
	u32 ret = vsprintf_safe(_ret, SIZE, _format, vlist);
	va_end(vlist);
	return ret;
}

///-------------------------------------------------------------------------------------------------
/// vsprintf_safe
template <u32 SIZE>
inline u32	vsprintf_safe(c8(&_ret)[SIZE], const c8* _format, va_list _arg_list)
{
	return vsprintf_safe(_ret, SIZE, _format, _arg_list);
}

template <u32 SIZE>
inline u32	strcpy_safe(c8(&_ret)[SIZE], const c8* _src)
{
	return strcpy_safe(_ret, SIZE, _src);
}

///-------------------------------------------------------------------------------------------------
/// クラスの型からクラス名の取得
template <class T>
const c8*	GetClassName(b8 _namespace_error)
{
	static CClassName s_className(typeid(T), _namespace_error);
	return s_className.GetName();
}



POISON_END


#endif	// __INLINE_INL__

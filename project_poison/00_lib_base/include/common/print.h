﻿//#pragma once
#ifndef __PRINT_H__
#define __PRINT_H__



POISON_BGN

#define PRINT_BIT_NUM			(32)	//< アトリビュート使用ビット数
#define PRINT_SYSTEM_BIT_NUM	(8)		//< システム側ビット数

//-------------------------------------------------------------------------------------------------
// print用システムアトリビュート
enum libPrintAttr
{
	libPrintAttrDefault = 0,	//< デフォルト用
	libPrintAttrError,			//< エラー用
	libPrintAttrWarning,		//< ワーニング用
	libPrintAttrMax,
};

//*************************************************************************************************
//@brief	PRINTユーティリティクラス
//*************************************************************************************************
class CPrintUtility
{
public:
	//-------------------------------------------------------------------------------------------------
	//@brief	プリント表示型
	typedef b8(*FUNC_PRINT)(const u32 _attr, const c8* _str, const c8* _func, const c8* _file, const u32 _line);

	//-------------------------------------------------------------------------------------------------
	//@brief	メモリハンドルをアトリビュート変換型
	typedef u32(*FUNC_MEM_TO_ATTR)(u8 _memHdl);

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	コールバックを使用してプリント表示
	static void	Print(const u32 _attr, const c8* _func, const c8* _file, const u32 _line, const c8* _format, ...);

	//-------------------------------------------------------------------------------------------------
	//@brief	コンソール画面に文字列描画
	static void	PrintConsole(const c8* _str);

public:
	//-------------------------------------------------------------------------------------------------
	//@brief	プリントの関数ポインタ設定
	static void	SetFuncPrint(FUNC_PRINT _func) { s_funcPrint = _func; }

	//-------------------------------------------------------------------------------------------------
	//@brief	定数でのUserアトリビュート
	static inline const u32	GetUserAttr(const u32 _bitindex, const u32 _system_attr = 0)
	{
		return (((1 << _bitindex) << PRINT_SYSTEM_BIT_NUM) + _system_attr);
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	bitでのUserアトリビュート
	static inline const u32	GetUserAttrBit(const u32 _userbit, const u32 _system_attr = 0)
	{
		return (_userbit << PRINT_SYSTEM_BIT_NUM) + _system_attr;
	}

	//-------------------------------------------------------------------------------------------------
	//@brief	変換の関数ポインタ設定
	static void	SetFuncMemToAttr(FUNC_MEM_TO_ATTR _func) { s_funcMemToAttr = _func; }

	//-------------------------------------------------------------------------------------------------
	//@brief	メモリハンドルからアトリビュートを取得
	static inline u32	MemHandleToAttr(u8 _memHdl) { return (s_funcMemToAttr) ? (*s_funcMemToAttr)(_memHdl) : 0; }

private:
	static FUNC_PRINT		s_funcPrint;		//< プリント表示間数
	static FUNC_MEM_TO_ATTR	s_funcMemToAttr;	//< メモリハンドルをアトリビュート変換関数

};


#ifndef POISON_RELEASE

//-------------------------------------------------------------------------------------------------
//@brief	SJISプリント
//@param[in]	_format	書式
//@param[in]	...		引数
extern void	PrintSJIS(const c8* _format, ...);

//-------------------------------------------------------------------------------------------------
//@brief	UTF8プリント
//@param[in]	_format	書式
//@param[in]	...		引数
extern void	PrintUTF8(const c8* _format, ...);

//-------------------------------------------------------------------------------------------------
// SJISプリント関数define
#define __PRINT_FUNC_SJIS(...)			POISON::PrintSJIS(__VA_ARGS__)
// UTF8プリント関数define
#define __PRINT_FUNC_UTF8(...)			POISON::PrintUTF8(__VA_ARGS__)

#define __PRINT_ALERT(__attr, ...)		libAlertAttr( __attr, __VA_ARGS__ )
#define __PRINT_ERROR(__attr,...)		libWarningAttr( __attr, __VA_ARGS__ ) // エラーをワーニングで呼び出し
#define __PRINT_WARNING(__attr,...)		libWarningAttr( __attr, __VA_ARGS__ )
#define __PRINT_ATTR(__attr,...)		POISON::CPrintUtility::Print( __attr, __PRETTY_FUNCTION__, __FILE__, __LINE__, __VA_ARGS__ )

#else // !POISON_RELEASE

#define __PRINT_FUNC_SJIS(...)
#define __PRINT_FUNC_UTF8(...)

#define __PRINT_ALERT(...)
#define __PRINT_ERROR(...)
#define __PRINT_WARNING(...)
#define __PRINT_ATTR(...)

#endif // else !POISON_RELEASE

//-------------------------------------------------------------------------------------------------
// 変換関数define
#define MEM_HANDLE_TO_ATTR( __mem_hdl )		POISON::CPrintUtility::MemHandleToAttr( __mem_hdl )

//-------------------------------------------------------------------------------------------------
// 用途別プリントdefine
#define PRINT_ASSERT(...)				__PRINT_FUNC_UTF8(__VA_ARGS__)										//< アサート用
#define PRINT_ALERT(...)				__PRINT_ALERT(POISON::libPrintAttrError, __VA_ARGS__)				//< アラート呼び出し
#define PRINT_ERROR(...)				__PRINT_ERROR(POISON::libPrintAttrError,__VA_ARGS__)				//< エラー用
#define PRINT_WARNING(...)				__PRINT_WARNING(POISON::libPrintAttrWarning,__VA_ARGS__)			//< 警告用

#define PRINT(...)						__PRINT_ATTR(POISON::libPrintAttrDefault,__VA_ARGS__)				//< 通常用
#define PRINT_ATTR(__attr,...)			__PRINT_ATTR(__attr,__VA_ARGS__)									//< 定数フィルタattr
#define PRINT_WARNING_ATTR(__attr,...)	__PRINT_WARNING(__attr + POISON::libPrintAttrWarning,__VA_ARGS__)	//< 定数フィルタワーニングattr
#define PRINT_ERROR_ATTR(__attr,...)	__PRINT_ERROR(__attr + POISON::libPrintAttrError,__VA_ARGS__)		//< 定数フィルタエラーattr
#define PRINT_ALERT_ATTR(__attr,...)	__PRINT_ALERT(__attr + POISON::libPrintAttrError,__VA_ARGS__)		//< 定数フィルタアラートattr

#define PRINT_ATTR_MEM( __mem_hdl, ... )				PRINT_ATTR( MEM_HANDLE_TO_ATTR(__mem_hdl), __VA_ARGS__ )
#define PRINT_WARNING_ATTR_MEM( __mem_hdl, ... )		PRINT_WARNING_ATTR( MEM_HANDLE_TO_ATTR(__mem_hdl), __VA_ARGS__ )
#define PRINT_ERROR_ATTR_MEM( __mem_hdl, ... )			PRINT_ERROR_ATTR( MEM_HANDLE_TO_ATTR(__mem_hdl), __VA_ARGS__ )
#define PRINT_ALERT_ATTR_MEM( __mem_hdl, ... )			PRINT_ALERT_ATTR( MEM_HANDLE_TO_ATTR(__mem_hdl), __VA_ARGS__ )



POISON_END

#endif	// __PRINT_H__

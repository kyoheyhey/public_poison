﻿//#pragma once
#ifndef __ENDIAN_INL__
#define __ENDIAN_INL__


#include "print.h"
#include "assert.h"


POISON_BGN


///-------------------------------------------------------------------------------------------------
/// ビッグエンディアンかどうか
inline b8	IsBigEndian()
{
#if defined(LIB_BIG_ENDIAN)
	return true;
#elif defined(LIB_LITTLE_ENDIAN)
	return false;
#else
	return !IsLittleEndian();
#endif
}


///-------------------------------------------------------------------------------------------------
/// リトルエンディアンかどうか
inline b8	IsLittleEndian()
{
#if defined(LIB_BIG_ENDIAN)
	return false;
#elif defined(LIB_LITTLE_ENDIAN)
	return true;
#else
	static const u32 u = 1;
	return (*(PCast<const u8*>(&u))) != 0;
#endif
}


///-------------------------------------------------------------------------------------------------
/// エンディアン変換 u8
template <>
inline void	SwapEndian<u8>(u8& _ret, const u8& _val)
{
	_ret = _val;
}
template <>
inline void	SwapEndian<s8>(s8& _ret, const s8& _val)
{
	SwapEndian(*(PCast<u8*>(&_ret)), *(PCast<const u8*>(&_val)));
}
///-------------------------------------------------------------------------------------------------
/// エンディアン変換 u16
template <>
inline void	SwapEndian<u16>(u16& _ret, const u16& _val)
{
	_ret = ((_val << 8) | (_val >> 8));
}
template <>
inline void	SwapEndian<s16>(s16& _ret, const s16& _val)
{
	SwapEndian(*(PCast<u16*>(&_ret)), *(PCast<const u16*>(&_val)));
}
///-------------------------------------------------------------------------------------------------
/// エンディアン変換 u32
template <>
inline void	SwapEndian<u32>(u32& _ret, const u32& _val)
{
	_ret = ((_val << 24) | ((_val << 8) & 0x00FF0000) |
		(_val >> 24) | ((_val >> 8) & 0x0000FF00));
}
template <>
inline void	SwapEndian<s32>(s32& _ret, const s32& _val)
{
	SwapEndian(*(PCast<u32*>(&_ret)), *(PCast<const u32*>(&_val)));
}
template <>
inline void	SwapEndian<f32>(f32& _ret, const f32& _val)
{
	SwapEndian(*(PCast<u32*>(&_ret)), *(PCast<const u32*>(&_val)));
}
///-------------------------------------------------------------------------------------------------
/// エンディアン変換 u64
template <>
inline void	SwapEndian<u64>(u64& _ret, const u64& _val)
{
	_ret = ((_val << 56) | ((_val << 40) & 0x00FF000000000000) | ((_val << 24) & 0x0000FF0000000000) | ((_val << 8) & 0x000000FF00000000) |
		(_val >> 56) | ((_val >> 40) & 0x000000000000FF00) | ((_val >> 24) & 0x0000000000FF0000) | ((_val >> 8) & 0x00000000FF000000));
}
template <>
inline void	SwapEndian<s64>(s64& _ret, const s64& _val)
{
	SwapEndian(*(PCast<u64*>(&_ret)), *(PCast<const u64*>(&_val)));
}
template <>
inline void	SwapEndian<f64>(f64& _ret, const f64& _val)
{
	SwapEndian(*(PCast<u64*>(&_ret)), *(PCast<const u64*>(&_val)));
}

///-------------------------------------------------------------------------------------------------
/// バイト数指定のエンディアン変換
template <u32 BYTE_NUM>
inline void	SwapEndianByte(u8* _ret, const u8* _p)
{
	libAssert(_ret != _p);
	_ret = _ret + (BYTE_NUM - 1);
	for (u32 i = 0; i < BYTE_NUM; i++, _ret--, _p++)
	{
		(*_ret) = (*_p);
	}
}

template <>
inline void	SwapEndianByte<1>(u8* _ret, const u8* _p)
{
	(*_ret) = (*_p);
}

///-------------------------------------------------------------------------------------------------
/// 未対応のエンディアン変換
template <typename T>
inline void	SwapEndian(T& _ret, const T& _val)
{
	SwapEndianByte<sizeof(T)>(PCast<u8*>(&_ret), PCast<const u8*>(&_val));
}


///-------------------------------------------------------------------------------------------------
/// 配列のエンディアン変換
template <typename T>
inline void	SwapEndian(T* _ret, const T* _p, const u32 _num)
{
	for (u32 i = 0; i < _num; i++, _ret++, _p++)
	{
		SwapEndian((*_ret), (*_p));
	}
}

///-------------------------------------------------------------------------------------------------
/// 自身のエンディアン変換
template <>
inline void	SwapEndian<u8>(u8& _ret)
{
	// 1バイトは何もしない
}
template <>
inline void	SwapEndian<s8>(s8& _ret)
{
	// 1バイトは何もしない
}
template <typename T>
inline void	SwapEndian(T& _ret)
{
	SwapEndian(_ret, _ret);
}
template <typename T>
inline void	SwapEndian(T* _ret, const u32 _num)
{
	SwapEndian(_ret, _ret, _num);
}


///-------------------------------------------------------------------------------------------------
/// SwapEndianReturn
template <typename T>
inline T	SwapEndianReturn(T _val)
{
	SwapEndian(_val, _val);
	return _val;
}

///-------------------------------------------------------------------------------------------------
/// GetEndianBitField
template <typename T>
inline T	GetEndianBitField(const T& _val, const u8 _bitSize)
{
	return (_val & ((1 << _bitSize) - 1));
}


///-------------------------------------------------------------------------------------------------
/// GetEndianBitField
template <typename T>
inline T	GetEndianBitField(const T& _val, const u8 _bitSize, const u8 _offs)
{
	return ((_val >> (_offs)) & ((1 << _bitSize) - 1));
}



//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

///-------------------------------------------------------------------------------------------------
/// toLittleEndian
template <typename T>
inline void	toLittleEndian(T& _ret, const T& _val)
{
#ifdef LIB_LITTLE_ENDIAN
	_ret = _val;
#else
	SwapEndian(_ret, _val);
#endif
}

///-------------------------------------------------------------------------------------------------
/// toLittleEndian
template <typename T>
inline void	toLittleEndian(T* _ret, const T* _p, const u32 _num)
{
#ifdef LIB_LITTLE_ENDIAN
	memcpy(_ret, _p, _num);
#else
	SwapEndian(_ret, _p, _num);
#endif
}

///-------------------------------------------------------------------------------------------------
/// toLittleEndian
template <typename T>
inline void	toLittleEndian(T& _ret)
{
#ifdef LIB_LITTLE_ENDIAN
#else
	SwapEndian(_ret, _ret);
#endif
}

///-------------------------------------------------------------------------------------------------
/// toLittleEndian
template <typename T>
inline void	toLittleEndian(T* _ret, const u32 _num)
{
#ifdef LIB_LITTLE_ENDIAN
#else
	SwapEndian(_ret, _num);
#endif
}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


///-------------------------------------------------------------------------------------------------
/// toBigEndian
template <typename T>
inline void	toBigEndian(T& _ret, const T& _val)
{
#ifdef LIB_BIG_ENDIAN
	_ret = _val;
#else
	SwapEndian(_ret, _val);
#endif
}

///-------------------------------------------------------------------------------------------------
/// toBigEndian
template <typename T>
inline void	toBigEndian(T* _ret, const T* _p, const u32 _num)
{
#ifdef LIB_BIG_ENDIAN
	::memcpy(_ret, _p, _num);
#else
	SwapEndian(_ret, _p, _num);
#endif
}

///-------------------------------------------------------------------------------------------------
/// toBigEndian
template <typename T>
inline void	toBigEndian(T& _ret)
{
#ifdef LIB_BIG_ENDIAN
#else
	SwapEndian(_ret, _ret);
#endif
}

///-------------------------------------------------------------------------------------------------
/// toBigEndian
template <typename T>
inline void	toBigEndian(T* _ret, const u32 _num)
{
#ifdef LIB_BIG_ENDIAN
#else
	SwapEndian(_ret, _num);
#endif
}


POISON_END


#endif	// __ENDIAN_INL__

﻿//#pragma once
#ifndef __INLINE_H__
#define __INLINE_H__



POISON_BGN


//-------------------------------------------------------------------------------------------------
//@brief	ポインタキャスト
//@return	指定した型にキャストしたポインタ
//@param	_p	キャストしたい元のポインタ
template <class T>
inline T	_pointer_cast(void* _p);
template <class T>
inline T	_pointer_cast(const void* _p);

//-------------------------------------------------------------------------------------------------
//@brief	カスタムstatic_castキャスト
//@return	指定した型にキャストしたポインタ
//@param	_p	キャストしたい元のポインタ
template <typename T1, typename T2>
inline T1		_static_cast(T2* _t);
template <typename T1, typename T2>
inline const T1	_static_cast(const T2* _t);
template <typename T1, typename T2>
inline const T1	_static_cast(const T2& _t);

//-------------------------------------------------------------------------------------------------
//@brief	static_down_castキャスト
//@note		リリースモードでは静的なダウンキャスト、それ以外では検証付きの動的なダウンキャスト
template <typename T1, typename T2>
inline T1		_static_down_cast(T2* _t);
template <typename T1, typename T2>
inline const T1	_static_down_cast(const T2* _t);

#ifndef PCast
#define	PCast	_pointer_cast
#endif // !PCast

#ifndef SCast
#define	SCast	_static_cast
#endif // !SCast

#ifndef SDCast
#define SDCast	_static_down_cast
#endif // !SDCast

#ifndef CCast
#define	CCast	const_cast
#endif // !CCast

#ifndef RCast
#define	RCast	reinterpret_cast
#endif // !RCast

#ifndef DCast
#define	DCast	dynamic_cast
#endif // !DCast


//-------------------------------------------------------------------------------------------------
// 型の整数値の最大最少値情報
template <typename T>
struct TypeMinMax
{
	static const s64	minValue;
	static const u64	maxValue;
};


//-------------------------------------------------------------------------------------------------
// ポインタかどうか
template <typename T>
struct CIsPoiner
{
	static const b8 IsPointer = false;
};

template <typename T>
struct CIsPoiner<T*>
{
	static const b8 IsPointer = true;
};

template <typename T>
inline b8	IsPointer(const T& _r)
{
	return false;
}

template <typename T>
inline b8	IsPointer(const T* _p)
{
	return true;
}

//-------------------------------------------------------------------------------------------------
// 符号ありか判定
template <typename T>
inline b8	IsSign()
{
	return std::is_signed<T>::value;
}

template <>
inline b8	IsSign<b8>()
{
	return false;
}

//-------------------------------------------------------------------------------------------------
// クラスかどうかの指定付きの符号ありかの判定
template <typename T, b8 IS_CLASS>
struct CIsSignClass
{
	static inline b8	IsSign() { return POISON::IsSign<T>(); }
};
// クラスかどうかの指定付きの符号ありかの判定、クラスなら符号なし
template <typename T>
struct CIsSignClass<T, true>
{
	static inline b8	IsSign() { return false; }
};

//-------------------------------------------------------------------------------------------------
// 浮動小数かどうか
template <typename T>
inline b8	IsFloat()
{
	return false;
}

template <>
inline b8	IsFloat<f32>()
{
	return true;
}

template <>
inline b8	IsFloat<f64>()
{
	return true;
}

//-------------------------------------------------------------------------------------------------
// floatが有効かどうか(nan, infでない)
inline b8	IsEnableFloat(f32 _f)
{
	return ((*((const u32*)&_f) >> 23) & 0xFF) != 0xFF; //!isnan(_f) && !isinf(_f);
}

//-------------------------------------------------------------------------------------------------
// 割算が可能かどうか(0, nan, infでない)
inline b8	IsEnableDivision(f32 _f)
{
	return (IsEnableFloat(_f) && _f != 0.0f);
}



POISON_END

// max,min消し
#undef max
#undef min
#undef clamp

POISON_BGN

//-------------------------------------------------------------------------------------------------
// max取得
template <typename T>
inline T	max(T _a, T _b)
{
	return (_a > _b) ? _a : _b;
}

inline s32	max(s32 _a, u32 _b)
{
	return (_a > SCast<s32>(_b)) ? _a : SCast<s32>(_b);
}

inline s32	max(u32 _a, s32 _b)
{
	return (SCast<s32>(_a) > _b) ? SCast<s32>(_a) : _b;
}

//-------------------------------------------------------------------------------------------------
// min取得
template <typename T>
inline T	min(T _a, T _b)
{
	return (_a < _b) ? _a : _b;
}

inline s32	min(s32 _a, u32 _b)
{
	return (_a < SCast<s32>(_b)) ? _a : SCast<s32>(_b);
}

inline s32	min(u32 _a, s32 _b)
{
	return (SCast<s32>(_a) < _b) ? SCast<s32>(_a) : _b;
}

//-------------------------------------------------------------------------------------------------
// clamp取得
template <typename T>
inline T	clamp(T _min, T _max, T _t)
{
	return min(max(_min, _t), _max);
}

//-------------------------------------------------------------------------------------------------
// スワップ
template< typename T >
inline void	swap(T& _val0, T& _val1)
{
	T val = _val0;
	_val0 = _val1;
	_val1 = val;
}

#if 0
//-------------------------------------------------------------------------------------------------
// 指定した配列を0でクリア(inline)
template <size_t SIZE>
FORCE_INLINE void	ZeroClear(void* _p);

//-------------------------------------------------------------------------------------------------
// 構造体のzeroクリアdefine
#define ZERO_CLEAR(__struct_ptr)	POISON::ZeroClear<sizeof(*(__struct_ptr))>((__struct_ptr))
// 固定配列のzeroクリア
#define ZERO_CLEAR_ARRAY(__array)	POISON::ZeroClear<sizeof((__array))>((__array))
#else
//-------------------------------------------------------------------------------------------------
// 構造体のzeroクリアdefine
#define ZERO_CLEAR(__struct_ptr)	memset( __struct_ptr, 0, sizeof(*__struct_ptr) )
// 固定配列のzeroクリア
#define ZERO_CLEAR_ARRAY(__array)	memset( __array, 0, sizeof(__array) )
#endif


//-------------------------------------------------------------------------------------------------
//@brief	sprintf文字数制限版
//@return	_retに書き込まれた文字数が返る、_sizeを超えた文字列の場合、_sizeが返る
//@param[in]	_ret		sprintfで展開した文字列が書き込まれる、_sizeを超えた場合も必ず終端(_ret[_size-1])に0を書き込む
//@param[in]	_size		_retに書き込む最大文字数
//@param[in]	_format		sprintfで使用するフォーマット
//@note		snprintfとほぼ同じ動作をします。違いはサイズオーバー時の挙動です。\n
//			snprintfはサイズオーバー時は_size-1まで文字列を書き込みますが、この関数は終端に0を書き込んだ状態で_sizeを返します。
inline u32	sprintf_safe(c8* _ret, size_t _size, const c8* _format, ...);

//-------------------------------------------------------------------------------------------------
//@brief	vsprintf文字数制限版
//@return	_retに書き込まれた文字数が返る、_sizeを超えた文字列の場合、_sizeが返る
//@param[in]	_ret		vsprintfで展開した文字列が書き込まれる、_sizeを超えた場合も必ず終端(_ret[_size-1])に0を書き込む
//@param[in]	_size		_retに書き込む最大文字数
//@param[in]	_format		vsprintfで使用するフォーマット
//@param[in]	_arg_list	引数リスト
//@note		vsnprintfとほぼ同じ動作をします。違いはサイズオーバー時の挙動です。\n
//			vsnprintfはサイズオーバー時は_size-1まで文字列を書き込みますが、この関数は終端に0を書き込んだ状態で_sizeを返します。
inline u32	vsprintf_safe(c8* _ret, size_t _size, const c8* _format, va_list _arg_list);

//-------------------------------------------------------------------------------------------------
//@brief	strcpy文字数制限版
//@return	_retに書き込まれた文字数が返る、_sizeを超えた文字列の場合、_sizeが返る
//@param[in]	_ret		strcpyで展開した文字列が書き込まれる、_sizeを超えた場合も必ず終端(_ret[_size-1])に0を書き込む
//@param[in]	_size		_retに書き込む最大文字数
//@param[in]	_src		strcpyで使用するコピー元の文字列
//@note		strcpy_sとほぼ同じ動作をします。違いはサイズオーバー時の挙動です。\n
//			strcpyはサイズオーバー時は_size-1まで文字列を書き込みますが、この関数は終端に0を書き込んだ状態で_sizeを返します。
inline u32	strcpy_safe(c8* _ret, size_t _size, const c8* _src);

//-------------------------------------------------------------------------------------------------
//@brief	sprintf文字数制限版(配列数がテンプレート版)
//@return	_retに書き込まれた文字数が返る、_sizeを超えた文字列の場合、_sizeが返る
//@param[in]	_ret		sprintfで展開した文字列が書き込まれる、SIZEを超えた場合も必ず終端(_ret[SIZE-1])に0を書き込む
//@param[in]	_format		sprintfで使用するフォーマット
//@note		snprintfとほぼ同じ動作をします。違いはサイズオーバー時の挙動です。\n
//			snprintfはサイズオーバー時はSIZE-1まで文字列を書き込みますが、この関数は終端に0を書き込んだ状態でSIZEを返します。
template <u32 SIZE>
inline u32	sprintf_safe(c8(&_ret)[SIZE], const c8* _format, ...);

//-------------------------------------------------------------------------------------------------
//@brief	vsprintf文字数制限版(配列数がテンプレート版)
//@return	_retに書き込まれた文字数が返る、_sizeを超えた文字列の場合、_sizeが返る
//@param[in]	_ret		vsprintfで展開した文字列が書き込まれる、SIZEを超えた場合も必ず終端(_ret[SIZE-1])に0を書き込む
//@param[in]	_format		vsprintfで使用するフォーマット
//@note		vsnprintfとほぼ同じ動作をします。違いはサイズオーバー時の挙動です。\n
//			vsnprintfはサイズオーバー時はSIZE-1まで文字列を書き込みますが、この関数は終端に0を書き込んだ状態でSIZEを返します。
template <u32 SIZE>
inline u32	vsprintf_safe(c8(&_ret)[SIZE], const c8* _format, ...);

//-------------------------------------------------------------------------------------------------
//@brief	strcpy文字数制限版(配列数がテンプレート版)
//@return	_retに書き込まれた文字数が返る、_sizeを超えた文字列の場合、_sizeが返る
//@param[in]	_ret		strcpyで展開した文字列が書き込まれる、SIZEを超えた場合も必ず終端(_ret[SIZE-1])に0を書き込む
//@param[in]	_src		strcpyで使用するコピー元の文字列
//@note		strcpy_sとほぼ同じ動作をします。違いはサイズオーバー時の挙動です。\n
//			strcpyはサイズオーバー時はSIZE-1まで文字列を書き込みますが、この関数は終端に0を書き込んだ状態でSIZEを返します。
template <u32 SIZE>
inline u32	strcpy_safe(c8(&_ret)[SIZE], const c8* _src);


//-------------------------------------------------------------------------------------------------
// クラスの最適化での削除防止
template <class T>
void	ExistClass();

// Poisonクラスの存在の最適化防止登録
#define EXIST_POISON_CLASS( __class )	template <> void ExistClass< __class >() {}

// クラスの存在の最適化防止登録
#define EXIST_CLASS( __class )			template <> void POISON::ExistClass< __class >() {}


//-------------------------------------------------------------------------------------------------
// クラスの型からクラス名の取得
template <class T>
const c8*	GetClassName(b8 _namespace_error = true);

// Poisonネームスペース内でのクラスの型からクラス名の取得の登録、クラス宣言と一緒に宣言する
#define REGIST_POISON_CLASS_NAME( __class )				template <> inline const c8* GetClassName< __class >(b8) { return #__class; }

// クラスの型からクラス名の取得の登録、クラス宣言と一緒に宣言する
#define REGIST_CLASS_NAME( __class )					template <> inline const c8* POISON::GetClassName< __class >(b8) { return #__class; }

// 名前空間付きクラスの型からクラス名の取得の登録、クラス宣言と一緒に宣言する
#define REGIST_NAMED_CLASS_NAME( __prefix, __class )	template <> inline const c8* POISON::GetClassName< __prefix:: __class >(b8) { return #__class; }

// クラスの存在定義とクラスの型からクラス名の取得の登録、cppのみでのクラス宣言と一緒に宣言する
#define REGIST_CLASS( __class )							REGIST_CLASS_NAME( __class );	EXIST_CLASS( __class )

// クラスの存在定義と名前空間付きクラスの型からクラス名の取得の登録、cppのみでのクラス宣言と一緒に宣言する
#define REGIST_NAMED_CLASS( __prefix, __class )			REGIST_NAMED_CLASS_NAME( __prefix, __class );	EXIST_CLASS( __class )


//-------------------------------------------------------------------------------------------------
/// クラス名取得クラス
class CClassName
{
public:
	//-------------------------------------------------------------------------------------------------
	/// クラス名をコンストラクタで取得
	CClassName(const std::type_info& _type_info, b8 _namespace_error = true);
	/// デストラクタ
	~CClassName();

	//-------------------------------------------------------------------------------------------------
	/// クラス名取得
	inline const c8*	GetName() const { return m_pName; }

private:
	const c8*	m_pName;
	void*		m_pBuf;
};


POISON_END

#include "inline.inl"


#endif	// __INLINE_H__

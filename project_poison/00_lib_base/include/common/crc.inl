﻿//#pragma once
#ifndef __CRC_INL__
#define __CRC_INL__



POISON_BGN

///-------------------------------------------------------------------------------------------------
/// 文字列の静的展開CRC変換
template <size_t NUM> inline hash32 CCrcUtility::BaseStaticCrc32(const c8(&_str)[NUM])
{
	return ~BaseInnerStaticCrc32(reinterpret_cast<const c8(&)[NUM - 1]>(_str));
}

///-------------------------------------------------------------------------------------------------
/// 文字列の静的展開CRC内部呼び出し用
template <size_t NUM> inline hash32 CCrcUtility::BaseInnerStaticCrc32(const c8(&_str)[NUM])
{
	const u32 crc = BaseInnerStaticCrc32(reinterpret_cast<const c8(&)[NUM - 1]>(_str));

	return (_str[NUM - 1] != 0) ? (s_CCrcUtility_crcTable32[static_cast<u8>(crc & 0xFF) ^ reinterpret_cast<const u8*>(_str)[NUM - 1]] ^ (crc >> 8)) : crc;
}

///-------------------------------------------------------------------------------------------------
/// 文字列の静的展開CRC内部呼び出し（再帰終端、変換開始）
template <> inline hash32 CCrcUtility::BaseInnerStaticCrc32(const c8(&_str)[1])
{
	const hash32 crc = 0xffffffffu;

	return (_str[0] != 0) ? static_cast<hash32>(s_CCrcUtility_crcTable32[static_cast<u8>(crc & 0xFF) ^ (*reinterpret_cast<const u8*>(_str))] ^ (crc >> 8)) : crc;
}

POISON_END


#endif	// __CRC_INL__

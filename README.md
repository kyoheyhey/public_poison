■ライブラリ
00_lib_base
 ・コーディング規約系
10_lib_core
 ・コアシステム
15_lib_core_posix
 ・posixライブラリ
20_lib_gfx
 ・描画関連
25_lib_gfx_gl
 ・openglライブラリ
30_lib_system
 ・ゲームよりなシステムライブラリ
80_lib_debug
 ・デバッグ

■アプリケーション
90_lib_work
 ・90_lib_work.slnをビルドすれば
 　\public_poison\project_poison\90_lib_work\exeにlib_work.exeが出力されます。
 ・\public_poison\project_poison\90_lib_work\shader_link.batを叩けばshader_worksの出力データのシンボリックリンクを
 　\public_poison\project_poison\90_lib_work\dataに生成します。

■シェーダツール
shader_works
 ・vs, gs, ps, cs, fxcfg, cfgをコンパイルすれば
 　\public_poison\project_poison\shader_works\outに出力されます。

■ツール
tools
 ・コンバートexe, batなどの置き場所

